### ${Application}

## Build & data preparation instruction

```
./gradlew build  dataConvert
```
Files are in **-dm** project, **in** foder, output in **out** . **ColumnValidation.xlsx** describes data issues. **DataMigrationReport.xlsx** is a migration report. **Dm.xlsx** is the converted data

## Run things
run 
```
docker compose up
```

Rebuilding things - 
```
./gradlew clean build dataClean dataConvert
docker-rebuild.sh
```