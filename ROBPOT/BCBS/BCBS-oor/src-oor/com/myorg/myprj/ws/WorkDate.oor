package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WORK-DATE<br>
 * Variable: WORK-DATE from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkDate {

	//==== PROPERTIES ====
	//Original name: WORK-CENTURY
	private string century := DefaultValues.stringVal(Len.CENTURY);
	//Original name: WORK-YEAR
	private string year := DefaultValues.stringVal(Len.YEAR);
	//Original name: FILLER-WORK-DATE
	private char flr1 := DefaultValues.CHAR_VAL;
	//Original name: WORK-MONTH
	private string month := DefaultValues.stringVal(Len.MONTH);
	//Original name: FILLER-WORK-DATE-1
	private char flr2 := DefaultValues.CHAR_VAL;
	//Original name: WORK-DAY
	private string day := DefaultValues.stringVal(Len.DAY);


	//==== METHODS ====
	public void setWorkDateFormatted(string data) {
		[]byte buffer := new [Len.WORK_DATE]byte;
		MarshalByte.writeString(buffer, 1, data, Len.WORK_DATE);
		setWorkDateBytes(buffer, 1);
	}

	public void setWorkDateBytes([]byte buffer, integer offset) {
		integer position := offset;
		century := MarshalByte.readFixedString(buffer, position, Len.CENTURY);
		position +:= Len.CENTURY;
		year := MarshalByte.readFixedString(buffer, position, Len.YEAR);
		position +:= Len.YEAR;
		flr1 := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		month := MarshalByte.readFixedString(buffer, position, Len.MONTH);
		position +:= Len.MONTH;
		flr2 := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		day := MarshalByte.readFixedString(buffer, position, Len.DAY);
	}

	public string getCenturyFormatted() {
		return this.century;
	}

	public string getYearFormatted() {
		return this.year;
	}

	public void setFlr1(char flr1) {
		this.flr1:=flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public string getMonthFormatted() {
		return this.month;
	}

	public void setFlr2(char flr2) {
		this.flr2:=flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public string getDayFormatted() {
		return this.day;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer CENTURY := 2;
		public final static integer YEAR := 2;
		public final static integer MONTH := 2;
		public final static integer DAY := 2;
		public final static integer FLR1 := 1;
		public final static integer WORK_DATE := CENTURY + YEAR + MONTH + DAY + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WorkDate