package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-DB2-ERR-KEY<br>
 * Variable: WS-DB2-ERR-KEY from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsDb2ErrKey {

	//==== PROPERTIES ====
	//Original name: WS-ERR-ID
	private string id := DefaultValues.stringVal(Len.ID);
	//Original name: FILLER-WS-DB2-ERR-KEY
	private string flr1 := " /";
	//Original name: WS-ERR-SFX-ID
	private char sfxId := DefaultValues.CHAR_VAL;
	//Original name: FILLER-WS-DB2-ERR-KEY-1
	private string flr2 := " /";
	//Original name: WS-ERR-PMT-ID
	private string pmtId := DefaultValues.stringVal(Len.PMT_ID);


	//==== METHODS ====
	public string getWsDb2ErrKeyFormatted() {
		return MarshalByteExt.bufferToStr(getWsDb2ErrKeyBytes());
	}

	public []byte getWsDb2ErrKeyBytes() {
		[]byte buffer := new [Len.WS_DB2_ERR_KEY]byte;
		return getWsDb2ErrKeyBytes(buffer, 1);
	}

	public []byte getWsDb2ErrKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, id, Len.ID);
		position +:= Len.ID;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeChar(buffer, position, sfxId);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, pmtId, Len.PMT_ID);
		return buffer;
	}

	public void setId(string id) {
		this.id:=Functions.subString(id, Len.ID);
	}

	public string getId() {
		return this.id;
	}

	public string getFlr1() {
		return this.flr1;
	}

	public void setSfxId(char sfxId) {
		this.sfxId:=sfxId;
	}

	public char getSfxId() {
		return this.sfxId;
	}

	public string getFlr2() {
		return this.flr2;
	}

	public void setPmtIdFormatted(string pmtId) {
		this.pmtId:=Trunc.toUnsignedNumeric(pmtId, Len.PMT_ID);
	}

	public short getPmtId() {
		return NumericDisplay.asShort(this.pmtId);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ID := 12;
		public final static integer PMT_ID := 2;
		public final static integer FLR1 := 3;
		public final static integer SFX_ID := 1;
		public final static integer WS_DB2_ERR_KEY := ID + SFX_ID + PMT_ID + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsDb2ErrKey