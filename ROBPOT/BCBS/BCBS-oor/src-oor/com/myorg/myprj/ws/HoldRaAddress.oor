package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: HOLD-RA-ADDRESS<br>
 * Variable: HOLD-RA-ADDRESS from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class HoldRaAddress {

	//==== PROPERTIES ====
	//Original name: HOLD-RA-ADDR1
	private string addr1 := DefaultValues.stringVal(Len.ADDR1);
	//Original name: HOLD-RA-ADDR2
	private string addr2 := DefaultValues.stringVal(Len.ADDR2);
	//Original name: HOLD-RA-ADDR3-CITY
	private string addr3City := DefaultValues.stringVal(Len.ADDR3_CITY);
	//Original name: HOLD-RA-ADDR3-ST
	private string addr3St := DefaultValues.stringVal(Len.ADDR3_ST);
	//Original name: HOLD-RA-ADDR3-ZIP
	private string addr3Zip := DefaultValues.stringVal(Len.ADDR3_ZIP);


	//==== METHODS ====
	public string getHoldRaAddressFormatted() {
		return MarshalByteExt.bufferToStr(getHoldRaAddressBytes());
	}

	public []byte getHoldRaAddressBytes() {
		[]byte buffer := new [Len.HOLD_RA_ADDRESS]byte;
		return getHoldRaAddressBytes(buffer, 1);
	}

	public []byte getHoldRaAddressBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, addr1, Len.ADDR1);
		position +:= Len.ADDR1;
		MarshalByte.writeString(buffer, position, addr2, Len.ADDR2);
		position +:= Len.ADDR2;
		getAddr3Bytes(buffer, position);
		return buffer;
	}

	public void setAddr1(string addr1) {
		this.addr1:=Functions.subString(addr1, Len.ADDR1);
	}

	public string getAddr1() {
		return this.addr1;
	}

	public void setAddr2(string addr2) {
		this.addr2:=Functions.subString(addr2, Len.ADDR2);
	}

	public string getAddr2() {
		return this.addr2;
	}

	/**Original name: HOLD-RA-ADDR3<br>*/
	public []byte getAddr3Bytes() {
		[]byte buffer := new [Len.ADDR3]byte;
		return getAddr3Bytes(buffer, 1);
	}

	public []byte getAddr3Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, addr3City, Len.ADDR3_CITY);
		position +:= Len.ADDR3_CITY;
		MarshalByte.writeString(buffer, position, addr3St, Len.ADDR3_ST);
		position +:= Len.ADDR3_ST;
		MarshalByte.writeString(buffer, position, addr3Zip, Len.ADDR3_ZIP);
		return buffer;
	}

	public void initAddr3Spaces() {
		addr3City := "";
		addr3St := "";
		addr3Zip := "";
	}

	public void setAddr3City(string addr3City) {
		this.addr3City:=Functions.subString(addr3City, Len.ADDR3_CITY);
	}

	public string getAddr3City() {
		return this.addr3City;
	}

	public void setAddr3St(string addr3St) {
		this.addr3St:=Functions.subString(addr3St, Len.ADDR3_ST);
	}

	public string getAddr3St() {
		return this.addr3St;
	}

	public void setAddr3Zip(string addr3Zip) {
		this.addr3Zip:=Functions.subString(addr3Zip, Len.ADDR3_ZIP);
	}

	public string getAddr3Zip() {
		return this.addr3Zip;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ADDR1 := 25;
		public final static integer ADDR2 := 25;
		public final static integer ADDR3_CITY := 25;
		public final static integer ADDR3_ST := 2;
		public final static integer ADDR3_ZIP := 10;
		public final static integer ADDR3 := ADDR3_CITY + ADDR3_ST + ADDR3_ZIP;
		public final static integer HOLD_RA_ADDRESS := ADDR1 + ADDR2 + ADDR3;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//HoldRaAddress