package com.myorg.myprj.copy;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.commons.data.to.IS02718sa;

/**Original name: DCLS02718SA<br>
 * Variable: DCLS02718SA from copybook S02718SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02718sa implements IS02718sa {

	//==== PROPERTIES ====
	//Original name: SV201-PROD-SRV-ID
	private string sv201ProdSrvId := DefaultValues.stringVal(Len.SV201_PROD_SRV_ID);
	//Original name: PROD-SRV-ID-QLF-CD
	private string prodSrvIdQlfCd := DefaultValues.stringVal(Len.PROD_SRV_ID_QLF_CD);
	//Original name: SV2022-PROD-SRV-ID
	private string sv2022ProdSrvId := DefaultValues.stringVal(Len.SV2022_PROD_SRV_ID);
	//Original name: SV2023-PROC-MOD-CD
	private string sv2023ProcModCd := DefaultValues.stringVal(Len.SV2023_PROC_MOD_CD);
	//Original name: SV2024-PROC-MOD-CD
	private string sv2024ProcModCd := DefaultValues.stringVal(Len.SV2024_PROC_MOD_CD);
	//Original name: SV2025-PROC-MOD-CD
	private string sv2025ProcModCd := DefaultValues.stringVal(Len.SV2025_PROC_MOD_CD);
	//Original name: SV2026-PROC-MOD-CD
	private string sv2026ProcModCd := DefaultValues.stringVal(Len.SV2026_PROC_MOD_CD);
	//Original name: SV203-MNTRY-AM
	private decimal(11,2) sv203MntryAm := DefaultValues.DEC_VAL;
	//Original name: QNTY-CT
	private decimal(7,2) qntyCt := DefaultValues.DEC_VAL;


	//==== METHODS ====
	@Override
	public void setSv201ProdSrvId(string sv201ProdSrvId) {
		this.sv201ProdSrvId:=Functions.subString(sv201ProdSrvId, Len.SV201_PROD_SRV_ID);
	}

	@Override
	public string getSv201ProdSrvId() {
		return this.sv201ProdSrvId;
	}

	@Override
	public void setProdSrvIdQlfCd(string prodSrvIdQlfCd) {
		this.prodSrvIdQlfCd:=Functions.subString(prodSrvIdQlfCd, Len.PROD_SRV_ID_QLF_CD);
	}

	@Override
	public string getProdSrvIdQlfCd() {
		return this.prodSrvIdQlfCd;
	}

	@Override
	public void setSv2022ProdSrvId(string sv2022ProdSrvId) {
		this.sv2022ProdSrvId:=Functions.subString(sv2022ProdSrvId, Len.SV2022_PROD_SRV_ID);
	}

	@Override
	public string getSv2022ProdSrvId() {
		return this.sv2022ProdSrvId;
	}

	@Override
	public void setSv2023ProcModCd(string sv2023ProcModCd) {
		this.sv2023ProcModCd:=Functions.subString(sv2023ProcModCd, Len.SV2023_PROC_MOD_CD);
	}

	@Override
	public string getSv2023ProcModCd() {
		return this.sv2023ProcModCd;
	}

	@Override
	public void setSv2024ProcModCd(string sv2024ProcModCd) {
		this.sv2024ProcModCd:=Functions.subString(sv2024ProcModCd, Len.SV2024_PROC_MOD_CD);
	}

	@Override
	public string getSv2024ProcModCd() {
		return this.sv2024ProcModCd;
	}

	@Override
	public void setSv2025ProcModCd(string sv2025ProcModCd) {
		this.sv2025ProcModCd:=Functions.subString(sv2025ProcModCd, Len.SV2025_PROC_MOD_CD);
	}

	@Override
	public string getSv2025ProcModCd() {
		return this.sv2025ProcModCd;
	}

	@Override
	public void setSv2026ProcModCd(string sv2026ProcModCd) {
		this.sv2026ProcModCd:=Functions.subString(sv2026ProcModCd, Len.SV2026_PROC_MOD_CD);
	}

	@Override
	public string getSv2026ProcModCd() {
		return this.sv2026ProcModCd;
	}

	@Override
	public void setSv203MntryAm(decimal(11,2) sv203MntryAm) {
		this.sv203MntryAm:=sv203MntryAm;
	}

	@Override
	public decimal(11,2) getSv203MntryAm() {
		return this.sv203MntryAm;
	}

	@Override
	public void setQntyCt(decimal(7,2) qntyCt) {
		this.qntyCt:=qntyCt;
	}

	@Override
	public decimal(7,2) getQntyCt() {
		return this.qntyCt;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer SV201_PROD_SRV_ID := 48;
		public final static integer PROD_SRV_ID_QLF_CD := 2;
		public final static integer SV2022_PROD_SRV_ID := 48;
		public final static integer SV2023_PROC_MOD_CD := 2;
		public final static integer SV2024_PROC_MOD_CD := 2;
		public final static integer SV2025_PROC_MOD_CD := 2;
		public final static integer SV2026_PROC_MOD_CD := 2;
		public final static integer CLM_CNTL_ID := 12;
		public final static integer LOOP_ID := 6;
		public final static integer SEG_ID := 3;
		public final static integer SV2027_DS_TX := 80;
		public final static integer UNIT_BSS_MEAS_CD := 2;
		public final static integer INFO_CHG_ID := 8;
		public final static integer INFO_CHG_DT := 10;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Dcls02718sa