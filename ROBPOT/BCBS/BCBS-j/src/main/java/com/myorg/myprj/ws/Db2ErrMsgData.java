/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DB2-ERR-MSG-DATA<br>
 * Variable: DB2-ERR-MSG-DATA from copybook NUDBERDD<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Db2ErrMsgData extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int G_MAXOCCURS = 10;
	//Original name: DB2-ERR-MSG-LG
	private short lg = ((short) 790);
	//Original name: DB2-ERR-MSG
	private String[] g = new String[G_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public Db2ErrMsgData() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.ERR_MSG_DATA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setErrMsgDataBytes(buf);
	}

	public void init() {
		for (int gIdx = 1; gIdx <= G_MAXOCCURS; gIdx++) {
			setG(gIdx, DefaultValues.stringVal(Len.G));
		}
	}

	public String getErrMsgDataFormatted() {
		byte[] buffer = new byte[Len.ERR_MSG_DATA];
		return MarshalByteExt.bufferToStr(getErrMsgDataBytes(buffer, 1));
	}

	public void setErrMsgDataBytes(byte[] buffer) {
		setErrMsgDataBytes(buffer, 1);
	}

	public byte[] getErrMsgDataBytes() {
		byte[] buffer = new byte[Len.ERR_MSG_DATA];
		return getErrMsgDataBytes(buffer, 1);
	}

	public void setErrMsgDataBytes(byte[] buffer, int offset) {
		int position = offset;
		lg = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		for (int idx = 1; idx <= G_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setG(idx, MarshalByte.readString(buffer, position, Len.G));
				position += Len.G;
			} else {
				setG(idx, "");
				position += Len.G;
			}
		}
	}

	public byte[] getErrMsgDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, lg);
		position += Types.SHORT_SIZE;
		for (int idx = 1; idx <= G_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getG(idx), Len.G);
			position += Len.G;
		}
		return buffer;
	}

	public void setLg(short lg) {
		this.lg = lg;
	}

	public short getLg() {
		return this.lg;
	}

	public void setG(int gIdx, String g) {
		this.g[gIdx - 1] = Functions.subString(g, Len.G);
	}

	public String getG(int gIdx) {
		return this.g[gIdx - 1];
	}

	public String getGFormatted(int gIdx) {
		return Functions.padBlanks(getG(gIdx), Len.G);
	}

	@Override
	public byte[] serialize() {
		return getErrMsgDataBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LG = 2;
		public static final int G = 79;
		public static final int ERR_MSG_DATA = LG + Db2ErrMsgData.G_MAXOCCURS * G;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
