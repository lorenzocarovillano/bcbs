/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import com.myorg.myprj.commons.data.to.IS02993sa;

/**
 * Data Access Object(DAO) for table [S02993SA]
 * 
 */
public class S02993saDao extends BaseSqlDao<IS02993sa> {

	private Cursor nonPrtRmks;
	private final IRowMapper<IS02993sa> fetchNonPrtRmksRm = buildNamedRowMapper(IS02993sa.class, "dnlRmrkCd");

	public S02993saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02993sa> getToClass() {
		return IS02993sa.class;
	}

	public DbAccessStatus openNonPrtRmks() {
		nonPrtRmks = buildQuery("openNonPrtRmks").open();
		return dbStatus;
	}

	public DbAccessStatus closeNonPrtRmks() {
		return closeCursor(nonPrtRmks);
	}

	public IS02993sa fetchNonPrtRmks(IS02993sa iS02993sa) {
		return fetch(nonPrtRmks, iS02993sa, fetchNonPrtRmksRm);
	}

	public char selectByDb2LiRmrkCd(String db2LiRmrkCd, char dft) {
		return buildQuery("selectByDb2LiRmrkCd").bind("db2LiRmrkCd", db2LiRmrkCd).scalarResultChar(dft);
	}
}
