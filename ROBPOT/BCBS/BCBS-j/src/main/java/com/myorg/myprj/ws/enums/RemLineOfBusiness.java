/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: REM-LINE-OF-BUSINESS<br>
 * Variable: REM-LINE-OF-BUSINESS from copybook NF07RMIT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class RemLineOfBusiness {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.REM_LINE_OF_BUSINESS);
	public static final String BCS = "BCS";
	public static final String FEP = "FEP";
	public static final String KANSAS_SOLUTIONS = "KSS";

	//==== METHODS ====
	public void setRemLineOfBusiness(String remLineOfBusiness) {
		this.value = Functions.subString(remLineOfBusiness, Len.REM_LINE_OF_BUSINESS);
	}

	public String getRemLineOfBusiness() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REM_LINE_OF_BUSINESS = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
