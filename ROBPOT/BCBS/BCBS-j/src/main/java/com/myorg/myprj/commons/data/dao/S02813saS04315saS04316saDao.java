/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS02813saS04315saS04316sa;

/**
 * Data Access Object(DAO) for tables [S02813SA, S04315SA, S04316SA]
 * 
 */
public class S02813saS04315saS04316saDao extends BaseSqlDao<IS02813saS04315saS04316sa> {

	public S02813saS04315saS04316saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02813saS04315saS04316sa> getToClass() {
		return IS02813saS04315saS04316sa.class;
	}

	public DbAccessStatus updateRec(String cntlId, char cntlSfxId, short pmtId) {
		return buildQuery("updateRec").bind("cntlId", cntlId).bind("cntlSfxId", String.valueOf(cntlSfxId)).bind("pmtId", pmtId).executeUpdate();
	}
}
