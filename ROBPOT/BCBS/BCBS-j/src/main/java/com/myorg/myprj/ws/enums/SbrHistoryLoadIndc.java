/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SBR-HISTORY-LOAD-INDC<br>
 * Variable: SBR-HISTORY-LOAD-INDC from copybook NF05SUBR<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SbrHistoryLoadIndc {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NOT_HIST_LOAD = ' ';
	public static final char HST_LOAD_WITHOUT_CHECK = 'A';
	public static final char HST_LOAD_PERFORM = 'D';
	public static final char HST_LOAD_DRUGS = 'D';
	public static final char HST_LOAD_INTERPLAN_BANK = 'I';
	public static final char HST_LOAD_SOK_CLINIC = 'K';
	public static final char HST_LOAD_WITH_TV_CHECK = 'L';
	public static final char HST_LOAD_MEDICARE = 'M';
	public static final char HST_LOAD_RECIPROCITY = 'R';

	//==== METHODS ====
	public void setHistoryLoadIndc(char historyLoadIndc) {
		this.value = historyLoadIndc;
	}

	public char getHistoryLoadIndc() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HISTORY_LOAD_INDC = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
