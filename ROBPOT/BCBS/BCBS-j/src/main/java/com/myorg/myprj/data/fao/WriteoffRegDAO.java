/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.data.fao;

import com.bphx.ctu.af.io.file.AbstractInputOutputDAO;
import com.bphx.ctu.af.io.file.FileAccessStatus;
import com.myorg.myprj.data.fto.WriteoffRegTO;

/**Original name: WRITEOFF-REG/RWRITOFF/RWRITOFF[NF0533ML]<br>*/
public class WriteoffRegDAO extends AbstractInputOutputDAO<WriteoffRegTO> {

	//==== PROPERTIES ====
	public static final int RECORD_SIZE = 650;

	//==== CONSTRUCTORS ====
	public WriteoffRegDAO(FileAccessStatus fileStatus) {
		super(fileStatus);
	}

	//==== METHODS ====
	@Override
	public String getName() {
		return "RWRITOFF";
	}

	@Override
	public int getRecordSize() {
		return RECORD_SIZE;
	}

	@Override
	public WriteoffRegTO createTo() {
		return new WriteoffRegTO();
	}

	@Override
	public WriteoffRegTO read() {
		WriteoffRegTO writeoffRegTO = new WriteoffRegTO();
		return read(writeoffRegTO);
	}
}
