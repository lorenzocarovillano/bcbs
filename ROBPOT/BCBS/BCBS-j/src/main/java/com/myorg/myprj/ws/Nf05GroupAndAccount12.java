/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.enums.Nf05GroupAndAccount3;

/**Original name: NF05-GROUP-AND-ACCOUNT-12<br>
 * Variable: NF05-GROUP-AND-ACCOUNT-12 from program NF0533ML<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class Nf05GroupAndAccount12 {

	//==== PROPERTIES ====
	//Original name: NF05-GROUP-AND-ACCOUNT-3
	private Nf05GroupAndAccount3 account3 = new Nf05GroupAndAccount3();
	//Original name: NF05-GROUP-AND-ACCOUNT-9
	private String account9 = DefaultValues.stringVal(Len.ACCOUNT9);

	//==== METHODS ====
	public void setNf05GroupAndAccountX(String nf05GroupAndAccountX) {
		int position = 1;
		byte[] buffer = getNf05GroupAndAccount12Bytes();
		MarshalByte.writeString(buffer, position, nf05GroupAndAccountX, Len.NF05_GROUP_AND_ACCOUNT_X);
		setNf05GroupAndAccount12Bytes(buffer);
	}

	/**Original name: NF05-GROUP-AND-ACCOUNT-X<br>*/
	public String getNf05GroupAndAccountX() {
		int position = 1;
		return MarshalByte.readString(getNf05GroupAndAccount12Bytes(), position, Len.NF05_GROUP_AND_ACCOUNT_X);
	}

	public void setNf05GroupAndAccount12Formatted(String data) {
		byte[] buffer = new byte[Len.NF05_GROUP_AND_ACCOUNT12];
		MarshalByte.writeString(buffer, 1, data, Len.NF05_GROUP_AND_ACCOUNT12);
		setNf05GroupAndAccount12Bytes(buffer, 1);
	}

	public void setNf05GroupAndAccount12Bytes(byte[] buffer) {
		setNf05GroupAndAccount12Bytes(buffer, 1);
	}

	/**Original name: NF05-GROUP-AND-ACCOUNT-12<br>*/
	public byte[] getNf05GroupAndAccount12Bytes() {
		byte[] buffer = new byte[Len.NF05_GROUP_AND_ACCOUNT12];
		return getNf05GroupAndAccount12Bytes(buffer, 1);
	}

	public void setNf05GroupAndAccount12Bytes(byte[] buffer, int offset) {
		int position = offset;
		account3.setAccount3(MarshalByte.readString(buffer, position, Nf05GroupAndAccount3.Len.ACCOUNT3));
		position += Nf05GroupAndAccount3.Len.ACCOUNT3;
		account9 = MarshalByte.readString(buffer, position, Len.ACCOUNT9);
	}

	public byte[] getNf05GroupAndAccount12Bytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, account3.getAccount3(), Nf05GroupAndAccount3.Len.ACCOUNT3);
		position += Nf05GroupAndAccount3.Len.ACCOUNT3;
		MarshalByte.writeString(buffer, position, account9, Len.ACCOUNT9);
		return buffer;
	}

	public void setAccount9(String account9) {
		this.account9 = Functions.subString(account9, Len.ACCOUNT9);
	}

	public String getAccount9() {
		return this.account9;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACCOUNT9 = 9;
		public static final int NF05_GROUP_AND_ACCOUNT12 = Nf05GroupAndAccount3.Len.ACCOUNT3 + ACCOUNT9;
		public static final int NF05_GROUP_AND_ACCOUNT_X = 12;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int NF05_GROUP_AND_ACCOUNT_X = 12;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
