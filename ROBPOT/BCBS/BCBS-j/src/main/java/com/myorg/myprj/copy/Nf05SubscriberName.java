/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: NF05-SUBSCRIBER-NAME<br>
 * Variable: NF05-SUBSCRIBER-NAME from copybook NF05EOB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Nf05SubscriberName {

	//==== PROPERTIES ====
	//Original name: NF05-SUBS-NAME-LAST
	private String last = DefaultValues.stringVal(Len.LAST);
	//Original name: NF05-SUBS-NAME-TITLE
	private String title = DefaultValues.stringVal(Len.TITLE);
	//Original name: NF05-SUBS-NAME-FIRST
	private String first = DefaultValues.stringVal(Len.FIRST);
	//Original name: NF05-SUBS-NAME-MI
	private char mi = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setNf05PatientNameFormatted(String data) {
		byte[] buffer = new byte[Len.DETL_PATIENT_NAME];
		MarshalByte.writeString(buffer, 1, data, Len.DETL_PATIENT_NAME);
		setDetlPatientNameBytes(buffer, 1);
	}

	public void setDetlPatientNameBytes(byte[] buffer, int offset) {
		int position = offset;
		last = MarshalByte.readString(buffer, position, Len.LAST);
		position += Len.LAST;
		title = MarshalByte.readString(buffer, position, Len.TITLE);
		position += Len.TITLE;
		first = MarshalByte.readString(buffer, position, Len.FIRST);
		position += Len.FIRST;
		mi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getDetlPatientNameBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, last, Len.LAST);
		position += Len.LAST;
		MarshalByte.writeString(buffer, position, title, Len.TITLE);
		position += Len.TITLE;
		MarshalByte.writeString(buffer, position, first, Len.FIRST);
		position += Len.FIRST;
		MarshalByte.writeChar(buffer, position, mi);
		return buffer;
	}

	public void initNf05PatientNameSpaces() {
		last = "";
		title = "";
		first = "";
		mi = Types.SPACE_CHAR;
	}

	public void setLast(String last) {
		this.last = Functions.subString(last, Len.LAST);
	}

	public String getLast() {
		return this.last;
	}

	public void setTitle(String title) {
		this.title = Functions.subString(title, Len.TITLE);
	}

	public String getTitle() {
		return this.title;
	}

	public void setFirst(String first) {
		this.first = Functions.subString(first, Len.FIRST);
	}

	public String getFirst() {
		return this.first;
	}

	public void setMi(char mi) {
		this.mi = mi;
	}

	public void setMiFormatted(String mi) {
		setMi(Functions.charAt(mi, Types.CHAR_SIZE));
	}

	public char getMi() {
		return this.mi;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LAST = 20;
		public static final int TITLE = 3;
		public static final int FIRST = 15;
		public static final int MI = 1;
		public static final int DETL_PATIENT_NAME = LAST + TITLE + FIRST + MI;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
