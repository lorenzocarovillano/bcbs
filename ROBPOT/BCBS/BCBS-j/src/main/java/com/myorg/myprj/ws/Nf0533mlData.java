/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.myorg.myprj.S02805saS02809saS02811saS02813saS02952saTrigger1Nf0533ml;
import com.myorg.myprj.S02814saNf0533ml;
import com.myorg.myprj.commons.data.to.IS02813sa;
import com.myorg.myprj.copy.Dcls02801sa;
import com.myorg.myprj.copy.Dcls02809sa;
import com.myorg.myprj.copy.Dcls02813sa;
import com.myorg.myprj.copy.Dcls02814sa;
import com.myorg.myprj.copy.Dcls02993sa;
import com.myorg.myprj.copy.Dcls03085sa;
import com.myorg.myprj.copy.Dcls03086sa;
import com.myorg.myprj.copy.Dcls04315sa;
import com.myorg.myprj.copy.Dclsr01451;
import com.myorg.myprj.copy.EobDb2CursorArea;
import com.myorg.myprj.copy.ExpenseRecord;
import com.myorg.myprj.copy.Nf05EobRecord;
import com.myorg.myprj.copy.Nf07ErrorMessages;
import com.myorg.myprj.copy.VoidInformation;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program NF0533ML<br>
 * Generated as a class for rule WS.<br>*/
public class Nf0533mlData implements IS02813sa {

	//==== PROPERTIES ====
	private S02805saS02809saS02811saS02813saS02952saTrigger1Nf0533ml s02805saS02809saS02811saS02813saS02952saTrigger1Nf0533ml = new S02805saS02809saS02811saS02813saS02952saTrigger1Nf0533ml(
			this);
	private S02814saNf0533ml s02814saNf0533ml = new S02814saNf0533ml(this);
	//Original name: NU0201ML
	private String nu0201ml = "NU0201ML";
	//Original name: WS-DATE-FIELDS
	private WsDateFields wsDateFields = new WsDateFields();
	//Original name: WS-NON-PRT-RMKS-VARIABLES
	private WsNonPrtRmksVariables wsNonPrtRmksVariables = new WsNonPrtRmksVariables();
	//Original name: WS-PRV-STOP-PAY-VARIABLES
	private WsPrvStopPayVariables wsPrvStopPayVariables = new WsPrvStopPayVariables();
	//Original name: GREGORN
	private Gregorn gregorn = new Gregorn();
	//Original name: FORMATTED-TIME
	private FormattedTime formattedTime = new FormattedTime();
	//Original name: WS-CONSTANTS
	private WsConstants wsConstants = new WsConstants();
	//Original name: MSG-SUB
	private int msgSub = DefaultValues.INT_VAL;
	//Original name: WS-ALL-COUNTERS
	private WsAllCounters wsAllCounters = new WsAllCounters();
	//Original name: WS-SWITCHES
	private WsSwitches wsSwitches = new WsSwitches();
	//Original name: WS-DISPLAY-WORK-AREA
	private WsDisplayWorkArea wsDisplayWorkArea = new WsDisplayWorkArea();
	//Original name: WS-HOLD-NOTE-AREA
	private WsHoldNoteArea wsHoldNoteArea = new WsHoldNoteArea();
	//Original name: WS-NF0533-WORK-AREA
	private WsNf0533WorkArea wsNf0533WorkArea = new WsNf0533WorkArea();
	//Original name: ABEND-CODE
	private int abendCode = 0;
	//Original name: DB2-NPI-CD
	private char db2NpiCd = DefaultValues.CHAR_VAL;
	//Original name: WS-RESTART-STUFF
	private WsRestartStuff wsRestartStuff = new WsRestartStuff();
	//Original name: EOB-HLD-CURSOR-AREA
	private String eobHldCursorArea = DefaultValues.stringVal(Len.EOB_HLD_CURSOR_AREA);
	//Original name: EOB-CUR-CURSOR-AREA
	private String eobCurCursorArea = DefaultValues.stringVal(Len.EOB_CUR_CURSOR_AREA);
	//Original name: WS-ROLL-LOGIC-WORK
	private WsRollLogicWork wsRollLogicWork = new WsRollLogicWork();
	//Original name: WS-PROMPT-PAY-WORK-FIELDS
	private WsPromptPayWorkFields wsPromptPayWorkFields = new WsPromptPayWorkFields();
	//Original name: WORK-FIELDS
	private WorkFields workFields = new WorkFields();
	//Original name: WS-EXP-WORK-ITEMS
	private WsExpWorkItems wsExpWorkItems = new WsExpWorkItems();
	//Original name: CLAIM-FORM-WK-AREA
	private ClaimFormWkArea claimFormWkArea = new ClaimFormWkArea();
	//Original name: WS-BLUE-CROSS-ADDRESS-STUFF
	private WsBlueCrossAddressStuff wsBlueCrossAddressStuff = new WsBlueCrossAddressStuff();
	//Original name: WS-CLAIM-ID
	private WsClaimId wsClaimId = new WsClaimId();
	//Original name: NF07-ERROR-MESSAGES
	private Nf07ErrorMessages nf07ErrorMessages = new Nf07ErrorMessages();
	//Original name: NF05-EOB-RECORD
	private Nf05EobRecord nf05EobRecord = new Nf05EobRecord();
	//Original name: WS-SUBROUTINE
	private WsSubroutine wsSubroutine = new WsSubroutine();
	//Original name: EXPENSE-RECORD
	private ExpenseRecord expenseRecord = new ExpenseRecord();
	//Original name: PROV-PASSED-INDIC-REC
	private ProvPassedIndicRec provPassedIndicRec = new ProvPassedIndicRec();
	//Original name: EOB-DB2-CURSOR-AREA
	private EobDb2CursorArea eobDb2CursorArea = new EobDb2CursorArea();
	//Original name: VOID-INFORMATION
	private VoidInformation voidInformation = new VoidInformation();
	//Original name: DCLS02801SA
	private Dcls02801sa dcls02801sa = new Dcls02801sa();
	//Original name: DCLS02809SA
	private Dcls02809sa dcls02809sa = new Dcls02809sa();
	//Original name: DCLS02813SA
	private Dcls02813sa dcls02813sa = new Dcls02813sa();
	//Original name: DCLS04315SA
	private Dcls04315sa dcls04315sa = new Dcls04315sa();
	//Original name: DCLS02814SA
	private Dcls02814sa dcls02814sa = new Dcls02814sa();
	//Original name: DCLS02993SA
	private Dcls02993sa dcls02993sa = new Dcls02993sa();
	//Original name: DCLS03085SA
	private Dcls03085sa dcls03085sa = new Dcls03085sa();
	//Original name: DCLS03086SA
	private Dcls03086sa dcls03086sa = new Dcls03086sa();
	//Original name: DCLSR01451
	private Dclsr01451 dclsr01451 = new Dclsr01451();

	//==== METHODS ====
	public String getNu0201ml() {
		return this.nu0201ml;
	}

	public void setMsgSub(int msgSub) {
		this.msgSub = msgSub;
	}

	public int getMsgSub() {
		return this.msgSub;
	}

	public void setAbendCode(int abendCode) {
		this.abendCode = abendCode;
	}

	public void setAbendCodeFromBuffer(byte[] buffer) {
		abendCode = MarshalByte.readBinaryInt(buffer, 1);
	}

	public int getAbendCode() {
		return this.abendCode;
	}

	public String getAbendCodeFormatted() {
		return PicFormatter.display(new PicParams("S9(5)").setUsage(PicUsage.BINARY)).format(getAbendCode()).toString();
	}

	public char getDb2NpiCd() {
		return this.db2NpiCd;
	}

	public void setEobHldCursorArea(String eobHldCursorArea) {
		this.eobHldCursorArea = Functions.subString(eobHldCursorArea, Len.EOB_HLD_CURSOR_AREA);
	}

	public String getEobHldCursorArea() {
		return this.eobHldCursorArea;
	}

	public String getEobHldCursorAreaFormatted() {
		return Functions.padBlanks(getEobHldCursorArea(), Len.EOB_HLD_CURSOR_AREA);
	}

	public void setEobCurCursorArea(String eobCurCursorArea) {
		this.eobCurCursorArea = Functions.subString(eobCurCursorArea, Len.EOB_CUR_CURSOR_AREA);
	}

	public String getEobCurCursorArea() {
		return this.eobCurCursorArea;
	}

	public String getEobCurCursorAreaFormatted() {
		return Functions.padBlanks(getEobCurCursorArea(), Len.EOB_CUR_CURSOR_AREA);
	}

	public String getWsExpFileRecFormatted() {
		return expenseRecord.getExpenseRecordFormatted();
	}

	@Override
	public AfDecimal getAcrPrmtPaIntAm() {
		return dcls02813sa.getAcrPrmtPaIntAm();
	}

	@Override
	public void setAcrPrmtPaIntAm(AfDecimal acrPrmtPaIntAm) {
		this.dcls02813sa.setAcrPrmtPaIntAm(acrPrmtPaIntAm.copy());
	}

	@Override
	public String getCkDt() {
		return dcls02813sa.getCkDt();
	}

	@Override
	public void setCkDt(String ckDt) {
		this.dcls02813sa.setCkDt(ckDt);
	}

	@Override
	public String getCkId() {
		return dcls02813sa.getCkId();
	}

	@Override
	public void setCkId(String ckId) {
		this.dcls02813sa.setCkId(ckId);
	}

	public ClaimFormWkArea getClaimFormWkArea() {
		return claimFormWkArea;
	}

	@Override
	public String getClmCntlId() {
		return dcls02813sa.getClmCntlId();
	}

	@Override
	public void setClmCntlId(String clmCntlId) {
		this.dcls02813sa.setClmCntlId(clmCntlId);
	}

	@Override
	public char getClmCntlSfxId() {
		return dcls02813sa.getClmCntlSfxId();
	}

	@Override
	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.dcls02813sa.setClmCntlSfxId(clmCntlSfxId);
	}

	@Override
	public short getClmPmtId() {
		return dcls02813sa.getClmPmtId();
	}

	@Override
	public void setClmPmtId(short clmPmtId) {
		this.dcls02813sa.setClmPmtId(clmPmtId);
	}

	@Override
	public String getDb2ClmCntlId() {
		return eobDb2CursorArea.getS02801Info().getClmCntlId();
	}

	@Override
	public void setDb2ClmCntlId(String db2ClmCntlId) {
		this.eobDb2CursorArea.getS02801Info().setClmCntlId(db2ClmCntlId);
	}

	@Override
	public char getDb2ClmCntlSfxId() {
		return eobDb2CursorArea.getS02801Info().getClmCntlSfxId();
	}

	@Override
	public void setDb2ClmCntlSfxId(char db2ClmCntlSfxId) {
		this.eobDb2CursorArea.getS02801Info().setClmCntlSfxId(db2ClmCntlSfxId);
	}

	public Dcls02801sa getDcls02801sa() {
		return dcls02801sa;
	}

	public Dcls02809sa getDcls02809sa() {
		return dcls02809sa;
	}

	public Dcls02813sa getDcls02813sa() {
		return dcls02813sa;
	}

	public Dcls02814sa getDcls02814sa() {
		return dcls02814sa;
	}

	public Dcls02993sa getDcls02993sa() {
		return dcls02993sa;
	}

	public Dcls03085sa getDcls03085sa() {
		return dcls03085sa;
	}

	public Dcls03086sa getDcls03086sa() {
		return dcls03086sa;
	}

	public Dcls04315sa getDcls04315sa() {
		return dcls04315sa;
	}

	public Dclsr01451 getDclsr01451() {
		return dclsr01451;
	}

	public EobDb2CursorArea getEobDb2CursorArea() {
		return eobDb2CursorArea;
	}

	public ExpenseRecord getExpenseRecord() {
		return expenseRecord;
	}

	public FormattedTime getFormattedTime() {
		return formattedTime;
	}

	public Gregorn getGregorn() {
		return gregorn;
	}

	public Nf05EobRecord getNf05EobRecord() {
		return nf05EobRecord;
	}

	public Nf07ErrorMessages getNf07ErrorMessages() {
		return nf07ErrorMessages;
	}

	@Override
	public AfDecimal getPrmptPayIntAm() {
		return dcls02813sa.getPrmptPayIntAm();
	}

	@Override
	public void setPrmptPayIntAm(AfDecimal prmptPayIntAm) {
		this.dcls02813sa.setPrmptPayIntAm(prmptPayIntAm.copy());
	}

	public ProvPassedIndicRec getProvPassedIndicRec() {
		return provPassedIndicRec;
	}

	public S02805saS02809saS02811saS02813saS02952saTrigger1Nf0533ml getS02805saS02809saS02811saS02813saS02952saTrigger1Nf0533ml() {
		return s02805saS02809saS02811saS02813saS02952saTrigger1Nf0533ml;
	}

	public S02814saNf0533ml getS02814saNf0533ml() {
		return s02814saNf0533ml;
	}

	@Override
	public short getVoidClmPmtId() {
		return voidInformation.getVoidS02813Info().getClmPmtId();
	}

	@Override
	public void setVoidClmPmtId(short voidClmPmtId) {
		this.voidInformation.getVoidS02813Info().setClmPmtId(voidClmPmtId);
	}

	public VoidInformation getVoidInformation() {
		return voidInformation;
	}

	public WorkFields getWorkFields() {
		return workFields;
	}

	public WsAllCounters getWsAllCounters() {
		return wsAllCounters;
	}

	public WsBlueCrossAddressStuff getWsBlueCrossAddressStuff() {
		return wsBlueCrossAddressStuff;
	}

	public WsClaimId getWsClaimId() {
		return wsClaimId;
	}

	public WsConstants getWsConstants() {
		return wsConstants;
	}

	public WsDateFields getWsDateFields() {
		return wsDateFields;
	}

	public WsDisplayWorkArea getWsDisplayWorkArea() {
		return wsDisplayWorkArea;
	}

	public WsExpWorkItems getWsExpWorkItems() {
		return wsExpWorkItems;
	}

	@Override
	public String getWsGlOfstOrigCd() {
		return wsExpWorkItems.getWsGlOfstOrigCd();
	}

	@Override
	public void setWsGlOfstOrigCd(String wsGlOfstOrigCd) {
		this.wsExpWorkItems.setWsGlOfstOrigCd(wsGlOfstOrigCd);
	}

	@Override
	public String getWsGlOfstVoidCd() {
		return wsExpWorkItems.getWsGlOfstVoidCd();
	}

	@Override
	public void setWsGlOfstVoidCd(String wsGlOfstVoidCd) {
		this.wsExpWorkItems.setWsGlOfstVoidCd(wsGlOfstVoidCd);
	}

	@Override
	public short getWsGlSoteOrigCd() {
		return wsExpWorkItems.getWsGlSoteOrigCd();
	}

	@Override
	public void setWsGlSoteOrigCd(short wsGlSoteOrigCd) {
		this.wsExpWorkItems.setWsGlSoteOrigCd(wsGlSoteOrigCd);
	}

	@Override
	public short getWsGlSoteVoidCd() {
		return wsExpWorkItems.getWsGlSoteVoidCd();
	}

	@Override
	public void setWsGlSoteVoidCd(short wsGlSoteVoidCd) {
		this.wsExpWorkItems.setWsGlSoteVoidCd(wsGlSoteVoidCd);
	}

	public WsHoldNoteArea getWsHoldNoteArea() {
		return wsHoldNoteArea;
	}

	public WsNf0533WorkArea getWsNf0533WorkArea() {
		return wsNf0533WorkArea;
	}

	public WsNonPrtRmksVariables getWsNonPrtRmksVariables() {
		return wsNonPrtRmksVariables;
	}

	public WsPromptPayWorkFields getWsPromptPayWorkFields() {
		return wsPromptPayWorkFields;
	}

	public WsPrvStopPayVariables getWsPrvStopPayVariables() {
		return wsPrvStopPayVariables;
	}

	public WsRestartStuff getWsRestartStuff() {
		return wsRestartStuff;
	}

	public WsRollLogicWork getWsRollLogicWork() {
		return wsRollLogicWork;
	}

	public WsSubroutine getWsSubroutine() {
		return wsSubroutine;
	}

	public WsSwitches getWsSwitches() {
		return wsSwitches;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HOLD_PAY_PROV_TOTAL = 1400;
		public static final int HOLD_PAY_PROV_LINE = 1400;
		public static final int EOB_HLD_CURSOR_AREA = 1400;
		public static final int EOB_CUR_CURSOR_AREA = 1400;
		public static final int FLR2 = 79;
		public static final int FLR3 = 3;
		public static final int WS_ADDRESS_ZIP = 9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
