/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.ws.enums.ExpAssignedIndc;
import com.myorg.myprj.ws.enums.ExpBankIndicator;
import com.myorg.myprj.ws.enums.ExpBillProvLob;
import com.myorg.myprj.ws.enums.ExpExaminerAction;
import com.myorg.myprj.ws.enums.ExpReportIndc;
import com.myorg.myprj.ws.enums.ExpWrapIndc;

/**Original name: EXPENSE-RECORD<br>
 * Variable: EXPENSE-RECORD from copybook NF06EXP<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class ExpenseRecord {

	//==== PROPERTIES ====
	//Original name: EXP-ACCOUNT-NUM
	private String expAccountNum = DefaultValues.stringVal(Len.EXP_ACCOUNT_NUM);
	//Original name: EXP-CLAIM-ID
	private String expClaimId = DefaultValues.stringVal(Len.EXP_CLAIM_ID);
	//Original name: EXP-CLAIM-ID-SUFFIX
	private char expClaimIdSuffix = DefaultValues.CHAR_VAL;
	//Original name: EXP-INS-ID
	private String expInsId = DefaultValues.stringVal(Len.EXP_INS_ID);
	//Original name: EXP-BILL-PROV-ID
	private String expBillProvId = DefaultValues.stringVal(Len.EXP_BILL_PROV_ID);
	//Original name: EXP-BILL-PROV-LOB
	private ExpBillProvLob expBillProvLob = new ExpBillProvLob();
	//Original name: EXP-BANK-INDICATOR
	private ExpBankIndicator expBankIndicator = new ExpBankIndicator();
	//Original name: EXP-ASSIGNED-INDC
	private ExpAssignedIndc expAssignedIndc = new ExpAssignedIndc();
	//Original name: EXP-AMOUNT-PAID
	private AfDecimal expAmountPaid = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: EXP-REPORT-INDC
	private ExpReportIndc expReportIndc = new ExpReportIndc();
	//Original name: EXP-TEAM
	private String expTeam = DefaultValues.stringVal(Len.EXP_TEAM);
	//Original name: EXP-ID
	private String expId = DefaultValues.stringVal(Len.EXP_ID);
	//Original name: EXP-WRAP-INDC
	private ExpWrapIndc expWrapIndc = new ExpWrapIndc();
	//Original name: EXP-WRAP-DATE-CCYY-MM-DD
	private Nf05RecieptDate expWrapDateCcyyMmDd = new Nf05RecieptDate();
	//Original name: EXP-EXAMINER-ACTION
	private ExpExaminerAction expExaminerAction = new ExpExaminerAction();
	//Original name: EXP-NPI-PROV-NO
	private String expNpiProvNo = DefaultValues.stringVal(Len.EXP_NPI_PROV_NO);
	//Original name: EXP-GEN-LDGR-OFFSET
	private String expGenLdgrOffset = DefaultValues.stringVal(Len.EXP_GEN_LDGR_OFFSET);
	//Original name: EXP-DIVISION-CODE
	private char expDivisionCode = DefaultValues.CHAR_VAL;
	//Original name: EXP-PRODUCT-CODE
	private String expProductCode = DefaultValues.stringVal(Len.EXP_PRODUCT_CODE);
	//Original name: EXP-PROGRAM-AREA
	private String expProgramArea = DefaultValues.stringVal(Len.EXP_PROGRAM_AREA);
	//Original name: EXP-ENROLLMENT-CLASS
	private String expEnrollmentClass = DefaultValues.stringVal(Len.EXP_ENROLLMENT_CLASS);
	//Original name: EXP-FINANCIAL-CODE
	private String expFinancialCode = DefaultValues.stringVal(Len.EXP_FINANCIAL_CODE);
	//Original name: EXP-CONVRTD-TYP-GRP-CD
	private char expConvrtdTypGrpCd = DefaultValues.CHAR_VAL;
	//Original name: EXP-ADJUSTMENT-TYPE
	private char expAdjustmentType = DefaultValues.CHAR_VAL;
	//Original name: EXP-HISTORY-LOAD-IND
	private char expHistoryLoadInd = DefaultValues.CHAR_VAL;
	//Original name: EXP-ITS-IND
	private char expItsInd = DefaultValues.CHAR_VAL;
	//Original name: EXP-REIMBURSE-IND
	private String expReimburseInd = DefaultValues.stringVal(Len.EXP_REIMBURSE_IND);
	//Original name: EXP-EXPLANATION-CODE
	private char expExplanationCode = DefaultValues.CHAR_VAL;
	//Original name: EXP-NETWORK-CODE
	private String expNetworkCode = DefaultValues.stringVal(Len.EXP_NETWORK_CODE);
	//Original name: EXP-MEMBER-ID
	private String expMemberId = DefaultValues.stringVal(Len.EXP_MEMBER_ID);
	//Original name: EXP-PAYMENT-NUM
	private String expPaymentNum = DefaultValues.stringVal(Len.EXP_PAYMENT_NUM);
	//Original name: EXP-LINE-NUM
	private String expLineNum = DefaultValues.stringVal(Len.EXP_LINE_NUM);
	//Original name: EXP-CALC-TYPE
	private char expCalcType = DefaultValues.CHAR_VAL;
	//Original name: EXP-TYP-GRP-CD
	private String expTypGrpCd = DefaultValues.stringVal(Len.EXP_TYP_GRP_CD);
	//Original name: EXP-CLM-SYST-VER-ID
	private String expClmSystVerId = DefaultValues.stringVal(Len.EXP_CLM_SYST_VER_ID);
	//Original name: EXP-CLM-INS-LN-CD
	private String expClmInsLnCd = DefaultValues.stringVal(Len.EXP_CLM_INS_LN_CD);
	//Original name: FILLER-X
	private String fillerX = DefaultValues.stringVal(Len.FILLER_X);

	//==== METHODS ====
	public String getExpenseRecordFormatted() {
		return MarshalByteExt.bufferToStr(getExpenseRecordBytes());
	}

	public byte[] getExpenseRecordBytes() {
		byte[] buffer = new byte[Len.EXPENSE_RECORD];
		return getExpenseRecordBytes(buffer, 1);
	}

	public byte[] getExpenseRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, expAccountNum, Len.EXP_ACCOUNT_NUM);
		position += Len.EXP_ACCOUNT_NUM;
		getExpClaimIdAndSuffixBytes(buffer, position);
		position += Len.EXP_CLAIM_ID_AND_SUFFIX;
		MarshalByte.writeString(buffer, position, expInsId, Len.EXP_INS_ID);
		position += Len.EXP_INS_ID;
		getExpBillingProviderDataBytes(buffer, position);
		position += Len.EXP_BILLING_PROVIDER_DATA;
		MarshalByte.writeChar(buffer, position, expBankIndicator.getExpBankIndicator());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expAssignedIndc.getExpAssignedIndc(), ExpAssignedIndc.Len.EXP_ASSIGNED_INDC);
		position += ExpAssignedIndc.Len.EXP_ASSIGNED_INDC;
		MarshalByte.writeDecimalAsPacked(buffer, position, expAmountPaid.copy());
		position += Len.EXP_AMOUNT_PAID;
		MarshalByte.writeString(buffer, position, expReportIndc.value, ExpReportIndc.Len.EXP_REPORT_INDC);
		position += ExpReportIndc.Len.EXP_REPORT_INDC;
		MarshalByte.writeString(buffer, position, expTeam, Len.EXP_TEAM);
		position += Len.EXP_TEAM;
		MarshalByte.writeString(buffer, position, expId, Len.EXP_ID);
		position += Len.EXP_ID;
		MarshalByte.writeChar(buffer, position, expWrapIndc.getExpWrapIndc());
		position += Types.CHAR_SIZE;
		expWrapDateCcyyMmDd.getExpWrapDateCcyyMmDdBytes(buffer, position);
		position += Nf05RecieptDate.Len.NF05_RECIEPT_DATE;
		MarshalByte.writeChar(buffer, position, expExaminerAction.getExpExaminerAction());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expNpiProvNo, Len.EXP_NPI_PROV_NO);
		position += Len.EXP_NPI_PROV_NO;
		MarshalByte.writeString(buffer, position, expGenLdgrOffset, Len.EXP_GEN_LDGR_OFFSET);
		position += Len.EXP_GEN_LDGR_OFFSET;
		MarshalByte.writeChar(buffer, position, expDivisionCode);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expProductCode, Len.EXP_PRODUCT_CODE);
		position += Len.EXP_PRODUCT_CODE;
		MarshalByte.writeString(buffer, position, expProgramArea, Len.EXP_PROGRAM_AREA);
		position += Len.EXP_PROGRAM_AREA;
		MarshalByte.writeString(buffer, position, expEnrollmentClass, Len.EXP_ENROLLMENT_CLASS);
		position += Len.EXP_ENROLLMENT_CLASS;
		MarshalByte.writeString(buffer, position, expFinancialCode, Len.EXP_FINANCIAL_CODE);
		position += Len.EXP_FINANCIAL_CODE;
		MarshalByte.writeChar(buffer, position, expConvrtdTypGrpCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, expAdjustmentType);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, expHistoryLoadInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, expItsInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expReimburseInd, Len.EXP_REIMBURSE_IND);
		position += Len.EXP_REIMBURSE_IND;
		MarshalByte.writeChar(buffer, position, expExplanationCode);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expNetworkCode, Len.EXP_NETWORK_CODE);
		position += Len.EXP_NETWORK_CODE;
		MarshalByte.writeString(buffer, position, expMemberId, Len.EXP_MEMBER_ID);
		position += Len.EXP_MEMBER_ID;
		MarshalByte.writeString(buffer, position, expPaymentNum, Len.EXP_PAYMENT_NUM);
		position += Len.EXP_PAYMENT_NUM;
		MarshalByte.writeString(buffer, position, expLineNum, Len.EXP_LINE_NUM);
		position += Len.EXP_LINE_NUM;
		MarshalByte.writeChar(buffer, position, expCalcType);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expTypGrpCd, Len.EXP_TYP_GRP_CD);
		position += Len.EXP_TYP_GRP_CD;
		MarshalByte.writeString(buffer, position, expClmSystVerId, Len.EXP_CLM_SYST_VER_ID);
		position += Len.EXP_CLM_SYST_VER_ID;
		MarshalByte.writeString(buffer, position, expClmInsLnCd, Len.EXP_CLM_INS_LN_CD);
		position += Len.EXP_CLM_INS_LN_CD;
		MarshalByte.writeString(buffer, position, fillerX, Len.FILLER_X);
		return buffer;
	}

	public void setExpAccountNum(String expAccountNum) {
		this.expAccountNum = Functions.subString(expAccountNum, Len.EXP_ACCOUNT_NUM);
	}

	public String getExpAccountNum() {
		return this.expAccountNum;
	}

	public byte[] getExpClaimIdAndSuffixBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, expClaimId, Len.EXP_CLAIM_ID);
		position += Len.EXP_CLAIM_ID;
		MarshalByte.writeChar(buffer, position, expClaimIdSuffix);
		return buffer;
	}

	public void setExpClaimId(String expClaimId) {
		this.expClaimId = Functions.subString(expClaimId, Len.EXP_CLAIM_ID);
	}

	public String getExpClaimId() {
		return this.expClaimId;
	}

	public void setExpClaimIdSuffix(char expClaimIdSuffix) {
		this.expClaimIdSuffix = expClaimIdSuffix;
	}

	public char getExpClaimIdSuffix() {
		return this.expClaimIdSuffix;
	}

	public void setExpInsId(String expInsId) {
		this.expInsId = Functions.subString(expInsId, Len.EXP_INS_ID);
	}

	public String getExpInsId() {
		return this.expInsId;
	}

	public byte[] getExpBillingProviderDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, expBillProvId, Len.EXP_BILL_PROV_ID);
		position += Len.EXP_BILL_PROV_ID;
		MarshalByte.writeChar(buffer, position, expBillProvLob.getExpBillProvLob());
		return buffer;
	}

	public void setExpBillProvId(String expBillProvId) {
		this.expBillProvId = Functions.subString(expBillProvId, Len.EXP_BILL_PROV_ID);
	}

	public String getExpBillProvId() {
		return this.expBillProvId;
	}

	public void setExpAmountPaid(AfDecimal expAmountPaid) {
		this.expAmountPaid.assign(expAmountPaid);
	}

	public AfDecimal getExpAmountPaid() {
		return this.expAmountPaid.copy();
	}

	public void setExpTeam(String expTeam) {
		this.expTeam = Functions.subString(expTeam, Len.EXP_TEAM);
	}

	public String getExpTeam() {
		return this.expTeam;
	}

	public void setExpId(String expId) {
		this.expId = Functions.subString(expId, Len.EXP_ID);
	}

	public String getExpId() {
		return this.expId;
	}

	public void setExpNpiProvNo(String expNpiProvNo) {
		this.expNpiProvNo = Functions.subString(expNpiProvNo, Len.EXP_NPI_PROV_NO);
	}

	public String getExpNpiProvNo() {
		return this.expNpiProvNo;
	}

	public void setExpGenLdgrOffset(String expGenLdgrOffset) {
		this.expGenLdgrOffset = Functions.subString(expGenLdgrOffset, Len.EXP_GEN_LDGR_OFFSET);
	}

	public String getExpGenLdgrOffset() {
		return this.expGenLdgrOffset;
	}

	public void setExpDivisionCode(char expDivisionCode) {
		this.expDivisionCode = expDivisionCode;
	}

	public char getExpDivisionCode() {
		return this.expDivisionCode;
	}

	public void setExpProductCode(String expProductCode) {
		this.expProductCode = Functions.subString(expProductCode, Len.EXP_PRODUCT_CODE);
	}

	public String getExpProductCode() {
		return this.expProductCode;
	}

	public void setExpProgramArea(String expProgramArea) {
		this.expProgramArea = Functions.subString(expProgramArea, Len.EXP_PROGRAM_AREA);
	}

	public String getExpProgramArea() {
		return this.expProgramArea;
	}

	public void setExpEnrollmentClass(String expEnrollmentClass) {
		this.expEnrollmentClass = Functions.subString(expEnrollmentClass, Len.EXP_ENROLLMENT_CLASS);
	}

	public String getExpEnrollmentClass() {
		return this.expEnrollmentClass;
	}

	public void setExpFinancialCode(String expFinancialCode) {
		this.expFinancialCode = Functions.subString(expFinancialCode, Len.EXP_FINANCIAL_CODE);
	}

	public String getExpFinancialCode() {
		return this.expFinancialCode;
	}

	public void setExpConvrtdTypGrpCd(char expConvrtdTypGrpCd) {
		this.expConvrtdTypGrpCd = expConvrtdTypGrpCd;
	}

	public void setExpConvrtdTypGrpCdFormatted(String expConvrtdTypGrpCd) {
		setExpConvrtdTypGrpCd(Functions.charAt(expConvrtdTypGrpCd, Types.CHAR_SIZE));
	}

	public char getExpConvrtdTypGrpCd() {
		return this.expConvrtdTypGrpCd;
	}

	public void setExpAdjustmentType(char expAdjustmentType) {
		this.expAdjustmentType = expAdjustmentType;
	}

	public char getExpAdjustmentType() {
		return this.expAdjustmentType;
	}

	public void setExpHistoryLoadInd(char expHistoryLoadInd) {
		this.expHistoryLoadInd = expHistoryLoadInd;
	}

	public char getExpHistoryLoadInd() {
		return this.expHistoryLoadInd;
	}

	public void setExpItsInd(char expItsInd) {
		this.expItsInd = expItsInd;
	}

	public char getExpItsInd() {
		return this.expItsInd;
	}

	public void setExpReimburseInd(String expReimburseInd) {
		this.expReimburseInd = Functions.subString(expReimburseInd, Len.EXP_REIMBURSE_IND);
	}

	public String getExpReimburseInd() {
		return this.expReimburseInd;
	}

	public void setExpExplanationCode(char expExplanationCode) {
		this.expExplanationCode = expExplanationCode;
	}

	public char getExpExplanationCode() {
		return this.expExplanationCode;
	}

	public void setExpNetworkCode(String expNetworkCode) {
		this.expNetworkCode = Functions.subString(expNetworkCode, Len.EXP_NETWORK_CODE);
	}

	public String getExpNetworkCode() {
		return this.expNetworkCode;
	}

	public void setExpMemberId(String expMemberId) {
		this.expMemberId = Functions.subString(expMemberId, Len.EXP_MEMBER_ID);
	}

	public String getExpMemberId() {
		return this.expMemberId;
	}

	public void setExpPaymentNum(short expPaymentNum) {
		this.expPaymentNum = NumericDisplay.asString(expPaymentNum, Len.EXP_PAYMENT_NUM);
	}

	public void setExpPaymentNumFormatted(String expPaymentNum) {
		this.expPaymentNum = Trunc.toUnsignedNumeric(expPaymentNum, Len.EXP_PAYMENT_NUM);
	}

	public short getExpPaymentNum() {
		return NumericDisplay.asShort(this.expPaymentNum);
	}

	public void setExpLineNum(short expLineNum) {
		this.expLineNum = NumericDisplay.asString(expLineNum, Len.EXP_LINE_NUM);
	}

	public void setExpLineNumFormatted(String expLineNum) {
		this.expLineNum = Trunc.toUnsignedNumeric(expLineNum, Len.EXP_LINE_NUM);
	}

	public short getExpLineNum() {
		return NumericDisplay.asShort(this.expLineNum);
	}

	public void setExpCalcType(char expCalcType) {
		this.expCalcType = expCalcType;
	}

	public void setExpCalcTypeFormatted(String expCalcType) {
		setExpCalcType(Functions.charAt(expCalcType, Types.CHAR_SIZE));
	}

	public char getExpCalcType() {
		return this.expCalcType;
	}

	public void setExpTypGrpCd(String expTypGrpCd) {
		this.expTypGrpCd = Functions.subString(expTypGrpCd, Len.EXP_TYP_GRP_CD);
	}

	public String getExpTypGrpCd() {
		return this.expTypGrpCd;
	}

	public void setExpClmSystVerId(String expClmSystVerId) {
		this.expClmSystVerId = Functions.subString(expClmSystVerId, Len.EXP_CLM_SYST_VER_ID);
	}

	public String getExpClmSystVerId() {
		return this.expClmSystVerId;
	}

	public void setExpClmInsLnCd(String expClmInsLnCd) {
		this.expClmInsLnCd = Functions.subString(expClmInsLnCd, Len.EXP_CLM_INS_LN_CD);
	}

	public String getExpClmInsLnCd() {
		return this.expClmInsLnCd;
	}

	public void setFillerX(String fillerX) {
		this.fillerX = Functions.subString(fillerX, Len.FILLER_X);
	}

	public String getFillerX() {
		return this.fillerX;
	}

	public ExpAssignedIndc getExpAssignedIndc() {
		return expAssignedIndc;
	}

	public ExpBankIndicator getExpBankIndicator() {
		return expBankIndicator;
	}

	public ExpBillProvLob getExpBillProvLob() {
		return expBillProvLob;
	}

	public ExpExaminerAction getExpExaminerAction() {
		return expExaminerAction;
	}

	public ExpReportIndc getExpReportIndc() {
		return expReportIndc;
	}

	public Nf05RecieptDate getExpWrapDateCcyyMmDd() {
		return expWrapDateCcyyMmDd;
	}

	public ExpWrapIndc getExpWrapIndc() {
		return expWrapIndc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EXP_ACCOUNT_NUM = 8;
		public static final int EXP_CLAIM_ID = 12;
		public static final int EXP_INS_ID = 14;
		public static final int EXP_BILL_PROV_ID = 10;
		public static final int EXP_TEAM = 5;
		public static final int EXP_ID = 3;
		public static final int EXP_NPI_PROV_NO = 10;
		public static final int EXP_GEN_LDGR_OFFSET = 2;
		public static final int EXP_PRODUCT_CODE = 2;
		public static final int EXP_PROGRAM_AREA = 3;
		public static final int EXP_ENROLLMENT_CLASS = 3;
		public static final int EXP_FINANCIAL_CODE = 3;
		public static final int EXP_REIMBURSE_IND = 2;
		public static final int EXP_NETWORK_CODE = 5;
		public static final int EXP_MEMBER_ID = 14;
		public static final int EXP_PAYMENT_NUM = 2;
		public static final int EXP_LINE_NUM = 3;
		public static final int EXP_TYP_GRP_CD = 3;
		public static final int EXP_CLM_SYST_VER_ID = 6;
		public static final int EXP_CLM_INS_LN_CD = 3;
		public static final int FILLER_X = 5;
		public static final int EXP_CLAIM_ID_SUFFIX = 1;
		public static final int EXP_CLAIM_ID_AND_SUFFIX = EXP_CLAIM_ID + EXP_CLAIM_ID_SUFFIX;
		public static final int EXP_BILLING_PROVIDER_DATA = EXP_BILL_PROV_ID + ExpBillProvLob.Len.EXP_BILL_PROV_LOB;
		public static final int EXP_AMOUNT_PAID = 6;
		public static final int EXP_DIVISION_CODE = 1;
		public static final int EXP_CONVRTD_TYP_GRP_CD = 1;
		public static final int EXP_ADJUSTMENT_TYPE = 1;
		public static final int EXP_HISTORY_LOAD_IND = 1;
		public static final int EXP_ITS_IND = 1;
		public static final int EXP_EXPLANATION_CODE = 1;
		public static final int EXP_CALC_TYPE = 1;
		public static final int EXPENSE_RECORD = EXP_ACCOUNT_NUM + EXP_CLAIM_ID_AND_SUFFIX + EXP_INS_ID + EXP_BILLING_PROVIDER_DATA
				+ ExpBankIndicator.Len.EXP_BANK_INDICATOR + ExpAssignedIndc.Len.EXP_ASSIGNED_INDC + EXP_AMOUNT_PAID
				+ ExpReportIndc.Len.EXP_REPORT_INDC + EXP_TEAM + EXP_ID + ExpWrapIndc.Len.EXP_WRAP_INDC + Nf05RecieptDate.Len.NF05_RECIEPT_DATE
				+ ExpExaminerAction.Len.EXP_EXAMINER_ACTION + EXP_NPI_PROV_NO + EXP_GEN_LDGR_OFFSET + EXP_DIVISION_CODE + EXP_PRODUCT_CODE
				+ EXP_PROGRAM_AREA + EXP_ENROLLMENT_CLASS + EXP_FINANCIAL_CODE + EXP_CONVRTD_TYP_GRP_CD + EXP_ADJUSTMENT_TYPE + EXP_HISTORY_LOAD_IND
				+ EXP_ITS_IND + EXP_REIMBURSE_IND + EXP_EXPLANATION_CODE + EXP_NETWORK_CODE + EXP_MEMBER_ID + EXP_PAYMENT_NUM + EXP_LINE_NUM
				+ EXP_CALC_TYPE + EXP_TYP_GRP_CD + EXP_CLM_SYST_VER_ID + EXP_CLM_INS_LN_CD + FILLER_X;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int EXP_AMOUNT_PAID = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int EXP_AMOUNT_PAID = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
