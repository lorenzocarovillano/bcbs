/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.commons.data.to.IS02993sa;

/**Original name: DCLS02993SA<br>
 * Variable: DCLS02993SA from copybook S02993SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02993sa implements IS02993sa {

	//==== PROPERTIES ====
	//Original name: DNL-RMRK-CD
	private String dnlRmrkCd = DefaultValues.stringVal(Len.DNL_RMRK_CD);
	//Original name: DNL-RMRK-USG-CD
	private char dnlRmrkUsgCd = DefaultValues.CHAR_VAL;
	//Original name: PRMPT-PAY-DNL-IN
	private char prmptPayDnlIn = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	@Override
	public void setDnlRmrkCd(String dnlRmrkCd) {
		this.dnlRmrkCd = Functions.subString(dnlRmrkCd, Len.DNL_RMRK_CD);
	}

	@Override
	public String getDnlRmrkCd() {
		return this.dnlRmrkCd;
	}

	public void setDnlRmrkUsgCd(char dnlRmrkUsgCd) {
		this.dnlRmrkUsgCd = dnlRmrkUsgCd;
	}

	public char getDnlRmrkUsgCd() {
		return this.dnlRmrkUsgCd;
	}

	public void setPrmptPayDnlIn(char prmptPayDnlIn) {
		this.prmptPayDnlIn = prmptPayDnlIn;
	}

	public void setPrmptPayDnlInFormatted(String prmptPayDnlIn) {
		setPrmptPayDnlIn(Functions.charAt(prmptPayDnlIn, Types.CHAR_SIZE));
	}

	public char getPrmptPayDnlIn() {
		return this.prmptPayDnlIn;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DNL_RMRK_CD = 3;
		public static final int DNL_RMRK_DS_TEXT = 500;
		public static final int DNL_RMRK_INT_NT_TX_TEXT = 700;
		public static final int INFO_EFF_DT = 10;
		public static final int INFO_TRM_DT = 10;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_TS = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
