/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: CSR-VERSION-FORMAT-ID<br>
 * Variable: CSR-VERSION-FORMAT-ID from copybook NF07AREA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CsrVersionFormatId {

	//==== PROPERTIES ====
	private String value = "";
	public static final String HIPAA4010 = "4010";
	public static final String HIPAA5010 = "5010";
	public static final String NCPDP = "NCPDP";

	//==== METHODS ====
	public void setVersionFormatId(String versionFormatId) {
		this.value = Functions.subString(versionFormatId, Len.VERSION_FORMAT_ID);
	}

	public String getVersionFormatId() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VERSION_FORMAT_ID = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
