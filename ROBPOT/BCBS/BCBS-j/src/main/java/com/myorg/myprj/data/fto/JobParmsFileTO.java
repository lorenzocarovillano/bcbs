/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.data.fto;

import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: JOB-PARMS-FILE<br>
 * File: JOB-PARMS-FILE from program NF0735ML<br>
 * Generated as a class for rule FTO.<br>*/
public class JobParmsFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: FILLER-WS-JOB-PARMS
	private String flr1 = "";
	//Original name: WS-JOB-NAME
	private String name = "";
	//Original name: FILLER-WS-JOB-PARMS-1
	private String flr2 = "";
	//Original name: WS-JOB-STEP
	private String step = "";
	//Original name: FILLER-WS-JOB-PARMS-2
	private String flr3 = "";
	//Original name: WS-JOB-SEQ
	private String seq = "000";
	//Original name: FILLER-WS-JOB-PARMS-3
	private String flr4 = "";

	//==== METHODS ====
	public void setParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
		position += Len.FLR1;
		name = MarshalByte.readString(buffer, position, Len.NAME);
		position += Len.NAME;
		flr2 = MarshalByte.readString(buffer, position, Len.FLR2);
		position += Len.FLR2;
		step = MarshalByte.readString(buffer, position, Len.STEP);
		position += Len.STEP;
		flr3 = MarshalByte.readString(buffer, position, Len.FLR1);
		position += Len.FLR1;
		seq = MarshalByte.readFixedString(buffer, position, Len.SEQ);
		position += Len.SEQ;
		flr4 = MarshalByte.readString(buffer, position, Len.FLR4);
	}

	public byte[] getParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, name, Len.NAME);
		position += Len.NAME;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, step, Len.STEP);
		position += Len.STEP;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, seq, Len.SEQ);
		position += Len.SEQ;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		return buffer;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setName(String name) {
		this.name = Functions.subString(name, Len.NAME);
	}

	public String getName() {
		return this.name;
	}

	public String getNameFormatted() {
		return Functions.padBlanks(getName(), Len.NAME);
	}

	public void setFlr2(String flr2) {
		this.flr2 = Functions.subString(flr2, Len.FLR2);
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setStep(String step) {
		this.step = Functions.subString(step, Len.STEP);
	}

	public String getStep() {
		return this.step;
	}

	public String getStepFormatted() {
		return Functions.padBlanks(getStep(), Len.STEP);
	}

	public void setFlr3(String flr3) {
		this.flr3 = Functions.subString(flr3, Len.FLR1);
	}

	public String getFlr3() {
		return this.flr3;
	}

	public short getSeq() {
		return NumericDisplay.asShort(this.seq);
	}

	public String getSeqFormatted() {
		return this.seq;
	}

	public String getSeqAsString() {
		return getSeqFormatted();
	}

	public void setFlr4(String flr4) {
		this.flr4 = Functions.subString(flr4, Len.FLR4);
	}

	public String getFlr4() {
		return this.flr4;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getParmsBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setParmsBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.PARMS;
	}

	@Override
	public IBuffer copy() {
		JobParmsFileTO copyTO = new JobParmsFileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 8;
		public static final int NAME = 8;
		public static final int FLR2 = 9;
		public static final int STEP = 8;
		public static final int SEQ = 3;
		public static final int FLR4 = 36;
		public static final int PARMS = NAME + STEP + SEQ + 2 * FLR1 + FLR2 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
