/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-DISPLAY-WORK-AREA<br>
 * Variable: WS-DISPLAY-WORK-AREA from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsDisplayWorkArea {

	//==== PROPERTIES ====
	//Original name: WS-SQLCODE-SIGN
	private char wsSqlcodeSign = DefaultValues.CHAR_VAL;
	//Original name: WS-SQLCODE
	private String wsSqlcode = DefaultValues.stringVal(Len.WS_SQLCODE);
	//Original name: ERROR-MESSAGE
	private ErrorMessage errorMessage = new ErrorMessage();
	//Original name: ERROR-TEXT-LEN
	private int errorTextLen = 78;
	//Original name: DB2-ERR-KEY
	private String db2ErrKey = DefaultValues.stringVal(Len.DB2_ERR_KEY);
	//Original name: DB2-ERR-TABLE
	private String db2ErrTable = DefaultValues.stringVal(Len.DB2_ERR_TABLE);
	//Original name: DB2-ERR-PARA
	private String db2ErrPara = DefaultValues.stringVal(Len.DB2_ERR_PARA);
	//Original name: DB2-ERR-LAST-CALL
	private String db2ErrLastCall = DefaultValues.stringVal(Len.DB2_ERR_LAST_CALL);
	//Original name: WS-DB2-ERR-KEY
	private WsDb2ErrKey wsDb2ErrKey = new WsDb2ErrKey();
	//Original name: DISP-INTEREST-PAID
	private String dispInterestPaid = DefaultValues.stringVal(Len.DISP_INTEREST_PAID);
	//Original name: WS-DISP-PROG-STATUS
	private String wsDispProgStatus = DefaultValues.stringVal(Len.WS_DISP_PROG_STATUS);
	//Original name: WS-DISP-DB-STATUS-SIGN
	private char wsDispDbStatusSign = DefaultValues.CHAR_VAL;
	//Original name: WS-DISP-DATABASE-STATUS
	private String wsDispDatabaseStatus = DefaultValues.stringVal(Len.WS_DISP_DATABASE_STATUS);
	//Original name: WS-DISP-ABENDING-PARAGRAPH
	private String wsDispAbendingParagraph = DefaultValues.stringVal(Len.WS_DISP_ABENDING_PARAGRAPH);
	//Original name: WS-DISP-ERR-MSG-SIGN
	private char wsDispErrMsgSign = DefaultValues.CHAR_VAL;
	//Original name: WS-DISP-ERR-MSG-LG
	private String wsDispErrMsgLg = DefaultValues.stringVal(Len.WS_DISP_ERR_MSG_LG);
	//Original name: WS-DISP-MSG-LG
	private String wsDispMsgLg = DefaultValues.stringVal(Len.WS_DISP_MSG_LG);
	//Original name: WS-DISP-CMT-CT
	private String wsDispCmtCt = DefaultValues.stringVal(Len.WS_DISP_CMT_CT);

	//==== METHODS ====
	public String getWsSqlcodeUnpackedFormatted() {
		return MarshalByteExt.bufferToStr(getWsSqlcodeUnpackedBytes());
	}

	/**Original name: WS-SQLCODE-UNPACKED<br>*/
	public byte[] getWsSqlcodeUnpackedBytes() {
		byte[] buffer = new byte[Len.WS_SQLCODE_UNPACKED];
		return getWsSqlcodeUnpackedBytes(buffer, 1);
	}

	public byte[] getWsSqlcodeUnpackedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, wsSqlcodeSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, wsSqlcode, Len.WS_SQLCODE);
		return buffer;
	}

	public void setWsSqlcodeSign(char wsSqlcodeSign) {
		this.wsSqlcodeSign = wsSqlcodeSign;
	}

	public void setWsSqlcodeSignFormatted(String wsSqlcodeSign) {
		setWsSqlcodeSign(Functions.charAt(wsSqlcodeSign, Types.CHAR_SIZE));
	}

	public char getWsSqlcodeSign() {
		return this.wsSqlcodeSign;
	}

	public void setWsSqlcode(int wsSqlcode) {
		this.wsSqlcode = NumericDisplay.asString(wsSqlcode, Len.WS_SQLCODE);
	}

	public int getWsSqlcode() {
		return NumericDisplay.asInt(this.wsSqlcode);
	}

	public void setErrorTextLen(int errorTextLen) {
		this.errorTextLen = errorTextLen;
	}

	public void setErrorTextLenFromBuffer(byte[] buffer) {
		errorTextLen = MarshalByte.readBinaryInt(buffer, 1);
	}

	public int getErrorTextLen() {
		return this.errorTextLen;
	}

	public String getErrorTextLenFormatted() {
		return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.BINARY)).format(getErrorTextLen()).toString();
	}

	public void setDb2ErrKey(String db2ErrKey) {
		this.db2ErrKey = Functions.subString(db2ErrKey, Len.DB2_ERR_KEY);
	}

	public String getDb2ErrKey() {
		return this.db2ErrKey;
	}

	public String getDb2ErrKeyFormatted() {
		return Functions.padBlanks(getDb2ErrKey(), Len.DB2_ERR_KEY);
	}

	public void setDb2ErrTable(String db2ErrTable) {
		this.db2ErrTable = Functions.subString(db2ErrTable, Len.DB2_ERR_TABLE);
	}

	public String getDb2ErrTable() {
		return this.db2ErrTable;
	}

	public String getDb2ErrTableFormatted() {
		return Functions.padBlanks(getDb2ErrTable(), Len.DB2_ERR_TABLE);
	}

	public void setDb2ErrPara(String db2ErrPara) {
		this.db2ErrPara = Functions.subString(db2ErrPara, Len.DB2_ERR_PARA);
	}

	public String getDb2ErrPara() {
		return this.db2ErrPara;
	}

	public String getDb2ErrParaFormatted() {
		return Functions.padBlanks(getDb2ErrPara(), Len.DB2_ERR_PARA);
	}

	public void setDb2ErrLastCall(String db2ErrLastCall) {
		this.db2ErrLastCall = Functions.subString(db2ErrLastCall, Len.DB2_ERR_LAST_CALL);
	}

	public String getDb2ErrLastCall() {
		return this.db2ErrLastCall;
	}

	public void setDispInterestPaid(AfDecimal dispInterestPaid) {
		this.dispInterestPaid = PicFormatter.display("Z,Z(3),Z(2)9.9(2)").format(dispInterestPaid.copy()).toString();
	}

	public AfDecimal getDispInterestPaid() {
		return PicParser.display("Z,Z(3),Z(2)9.9(2)").parseDecimal(Len.Int.DISP_INTEREST_PAID + Len.Fract.DISP_INTEREST_PAID,
				Len.Fract.DISP_INTEREST_PAID, this.dispInterestPaid);
	}

	public String getDispInterestPaidFormatted() {
		return this.dispInterestPaid;
	}

	public String getDispInterestPaidAsString() {
		return getDispInterestPaidFormatted();
	}

	public void setWsDispProgStatus(String wsDispProgStatus) {
		this.wsDispProgStatus = Functions.subString(wsDispProgStatus, Len.WS_DISP_PROG_STATUS);
	}

	public String getWsDispProgStatus() {
		return this.wsDispProgStatus;
	}

	public String getWsDispProgStatusFormatted() {
		return Functions.padBlanks(getWsDispProgStatus(), Len.WS_DISP_PROG_STATUS);
	}

	public void setWsDispDbStatusSign(char wsDispDbStatusSign) {
		this.wsDispDbStatusSign = wsDispDbStatusSign;
	}

	public void setWsDispDbStatusSignFormatted(String wsDispDbStatusSign) {
		setWsDispDbStatusSign(Functions.charAt(wsDispDbStatusSign, Types.CHAR_SIZE));
	}

	public char getWsDispDbStatusSign() {
		return this.wsDispDbStatusSign;
	}

	public void setWsDispDatabaseStatus(int wsDispDatabaseStatus) {
		this.wsDispDatabaseStatus = NumericDisplay.asString(wsDispDatabaseStatus, Len.WS_DISP_DATABASE_STATUS);
	}

	public int getWsDispDatabaseStatus() {
		return NumericDisplay.asInt(this.wsDispDatabaseStatus);
	}

	public String getWsDispDatabaseStatusFormatted() {
		return this.wsDispDatabaseStatus;
	}

	public String getWsDispDatabaseStatusAsString() {
		return getWsDispDatabaseStatusFormatted();
	}

	public void setWsDispAbendingParagraph(String wsDispAbendingParagraph) {
		this.wsDispAbendingParagraph = Functions.subString(wsDispAbendingParagraph, Len.WS_DISP_ABENDING_PARAGRAPH);
	}

	public String getWsDispAbendingParagraph() {
		return this.wsDispAbendingParagraph;
	}

	public String getWsDispAbendingParagraphFormatted() {
		return Functions.padBlanks(getWsDispAbendingParagraph(), Len.WS_DISP_ABENDING_PARAGRAPH);
	}

	public void setWsDispErrMsgSign(char wsDispErrMsgSign) {
		this.wsDispErrMsgSign = wsDispErrMsgSign;
	}

	public void setWsDispErrMsgSignFormatted(String wsDispErrMsgSign) {
		setWsDispErrMsgSign(Functions.charAt(wsDispErrMsgSign, Types.CHAR_SIZE));
	}

	public char getWsDispErrMsgSign() {
		return this.wsDispErrMsgSign;
	}

	public void setWsDispErrMsgLg(short wsDispErrMsgLg) {
		this.wsDispErrMsgLg = NumericDisplay.asString(wsDispErrMsgLg, Len.WS_DISP_ERR_MSG_LG);
	}

	public short getWsDispErrMsgLg() {
		return NumericDisplay.asShort(this.wsDispErrMsgLg);
	}

	public String getWsDispErrMsgLgFormatted() {
		return this.wsDispErrMsgLg;
	}

	public String getWsDispErrMsgLgAsString() {
		return getWsDispErrMsgLgFormatted();
	}

	public void setWsDispMsgLg(int wsDispMsgLg) {
		this.wsDispMsgLg = NumericDisplay.asString(wsDispMsgLg, Len.WS_DISP_MSG_LG);
	}

	public int getWsDispMsgLg() {
		return NumericDisplay.asInt(this.wsDispMsgLg);
	}

	public String getWsDispMsgLgFormatted() {
		return this.wsDispMsgLg;
	}

	public String getWsDispMsgLgAsString() {
		return getWsDispMsgLgFormatted();
	}

	public void setWsDispCmtCt(int wsDispCmtCt) {
		this.wsDispCmtCt = NumericDisplay.asString(wsDispCmtCt, Len.WS_DISP_CMT_CT);
	}

	public int getWsDispCmtCt() {
		return NumericDisplay.asInt(this.wsDispCmtCt);
	}

	public String getWsDispCmtCtFormatted() {
		return this.wsDispCmtCt;
	}

	public String getWsDispCmtCtAsString() {
		return getWsDispCmtCtFormatted();
	}

	public ErrorMessage getErrorMessage() {
		return errorMessage;
	}

	public WsDb2ErrKey getWsDb2ErrKey() {
		return wsDb2ErrKey;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SQLCODE = 9;
		public static final int DB2_ERR_KEY = 60;
		public static final int DB2_ERR_TABLE = 60;
		public static final int DB2_ERR_PARA = 60;
		public static final int DB2_ERR_LAST_CALL = 60;
		public static final int DISP_INTEREST_PAID = 12;
		public static final int WS_DISP_DATE_PARM = 10;
		public static final int WS_DISP_PROG_STATUS = 3;
		public static final int WS_DISP_DATABASE_STATUS = 9;
		public static final int WS_DISP_ABENDING_PARAGRAPH = 30;
		public static final int WS_DISP_ERR_MSG_LG = 4;
		public static final int WS_DISP_MSG_LG = 9;
		public static final int WS_DISP_CMT_CT = 9;
		public static final int WS_SQLCODE_SIGN = 1;
		public static final int WS_SQLCODE_UNPACKED = WS_SQLCODE_SIGN + WS_SQLCODE;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int DISP_INTEREST_PAID = 7;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int DISP_INTEREST_PAID = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
