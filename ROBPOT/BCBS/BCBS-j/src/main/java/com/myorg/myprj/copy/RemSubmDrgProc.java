/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: REM-SUBM-DRG-PROC<br>
 * Variable: REM-SUBM-DRG-PROC from copybook NF07RMIT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class RemSubmDrgProc {

	//==== PROPERTIES ====
	//Original name: REM-SUBM-PROC-CODE
	private String code = DefaultValues.stringVal(Len.CODE);
	//Original name: REM-SUBM-PROC-MOD1
	private String mod1 = DefaultValues.stringVal(Len.MOD1);
	//Original name: REM-SUBM-PROC-MOD2
	private String mod2 = DefaultValues.stringVal(Len.MOD2);

	//==== METHODS ====
	public void setSubmDrgProcBytes(byte[] buffer, int offset) {
		int position = offset;
		code = MarshalByte.readString(buffer, position, Len.CODE);
		position += Len.CODE;
		mod1 = MarshalByte.readString(buffer, position, Len.MOD1);
		position += Len.MOD1;
		mod2 = MarshalByte.readString(buffer, position, Len.MOD2);
	}

	public byte[] getSubmDrgProcBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, code, Len.CODE);
		position += Len.CODE;
		MarshalByte.writeString(buffer, position, mod1, Len.MOD1);
		position += Len.MOD1;
		MarshalByte.writeString(buffer, position, mod2, Len.MOD2);
		return buffer;
	}

	public void initSubmDrgProcSpaces() {
		code = "";
		mod1 = "";
		mod2 = "";
	}

	public void setCode(String code) {
		this.code = Functions.subString(code, Len.CODE);
	}

	public String getCode() {
		return this.code;
	}

	public void setMod1(String mod1) {
		this.mod1 = Functions.subString(mod1, Len.MOD1);
	}

	public String getMod1() {
		return this.mod1;
	}

	public void setMod2(String mod2) {
		this.mod2 = Functions.subString(mod2, Len.MOD2);
	}

	public String getMod2() {
		return this.mod2;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CODE = 5;
		public static final int MOD1 = 2;
		public static final int MOD2 = 2;
		public static final int SUBM_DRG_PROC = CODE + MOD1 + MOD2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
