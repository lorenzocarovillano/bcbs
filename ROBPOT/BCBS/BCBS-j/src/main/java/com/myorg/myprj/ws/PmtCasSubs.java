/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: PMT-CAS-SUBS<br>
 * Variable: PMT-CAS-SUBS from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class PmtCasSubs {

	//==== PROPERTIES ====
	//Original name: CASCO-SUB
	private short cascoSub = ((short) 0);
	//Original name: CASPR-SUB
	private short casprSub = ((short) 0);
	//Original name: CASOA-SUB
	private short casoaSub = ((short) 0);
	//Original name: CASPI-SUB
	private short caspiSub = ((short) 0);

	//==== METHODS ====
	public void setCascoSub(short cascoSub) {
		this.cascoSub = cascoSub;
	}

	public short getCascoSub() {
		return this.cascoSub;
	}

	public String getCascoSubFormatted() {
		return PicFormatter.display(new PicParams("9(4)").setUsage(PicUsage.PACKED)).format(getCascoSub()).toString();
	}

	public String getCascoSubAsString() {
		return getCascoSubFormatted();
	}

	public void setCasprSub(short casprSub) {
		this.casprSub = casprSub;
	}

	public short getCasprSub() {
		return this.casprSub;
	}

	public String getCasprSubFormatted() {
		return PicFormatter.display(new PicParams("9(4)").setUsage(PicUsage.PACKED)).format(getCasprSub()).toString();
	}

	public String getCasprSubAsString() {
		return getCasprSubFormatted();
	}

	public void setCasoaSub(short casoaSub) {
		this.casoaSub = casoaSub;
	}

	public short getCasoaSub() {
		return this.casoaSub;
	}

	public String getCasoaSubFormatted() {
		return PicFormatter.display(new PicParams("9(4)").setUsage(PicUsage.PACKED)).format(getCasoaSub()).toString();
	}

	public String getCasoaSubAsString() {
		return getCasoaSubFormatted();
	}

	public void setCaspiSub(short caspiSub) {
		this.caspiSub = caspiSub;
	}

	public short getCaspiSub() {
		return this.caspiSub;
	}

	public String getCaspiSubFormatted() {
		return PicFormatter.display(new PicParams("9(4)").setUsage(PicUsage.PACKED)).format(getCaspiSub()).toString();
	}

	public String getCaspiSubAsString() {
		return getCaspiSubFormatted();
	}
}
