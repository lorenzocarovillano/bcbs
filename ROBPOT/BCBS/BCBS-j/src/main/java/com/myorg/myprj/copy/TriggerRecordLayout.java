/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TRIGGER-RECORD-LAYOUT<br>
 * Variable: TRIGGER-RECORD-LAYOUT from copybook NF05TRIG<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class TriggerRecordLayout {

	//==== PROPERTIES ====
	//Original name: TRG-PROD-IND
	private String prodInd = DefaultValues.stringVal(Len.PROD_IND);
	//Original name: TRG-KEY
	private TrgKey key = new TrgKey();
	//Original name: TRG-STAT-CATG-CD
	private String statCatgCd = DefaultValues.stringVal(Len.STAT_CATG_CD);
	//Original name: TRG-FNL-BUS-DT
	private String fnlBusDt = DefaultValues.stringVal(Len.FNL_BUS_DT);
	//Original name: TRG-INFO-SSYST-ID
	private String infoSsystId = DefaultValues.stringVal(Len.INFO_SSYST_ID);
	//Original name: TRG-BI-PROV-ID
	private String biProvId = DefaultValues.stringVal(Len.BI_PROV_ID);
	//Original name: TRG-BI-PROV-LOB
	private char biProvLob = DefaultValues.CHAR_VAL;
	//Original name: TRG-BI-SPEC-CD
	private String biSpecCd = DefaultValues.stringVal(Len.BI_SPEC_CD);
	//Original name: TRG-PE-PROV-ID
	private String peProvId = DefaultValues.stringVal(Len.PE_PROV_ID);
	//Original name: TRG-PE-PROV-LOB
	private char peProvLob = DefaultValues.CHAR_VAL;
	//Original name: TRG-ALPH-PRFX-CD
	private String alphPrfxCd = DefaultValues.stringVal(Len.ALPH_PRFX_CD);
	//Original name: TRG-INS-ID
	private String insId = DefaultValues.stringVal(Len.INS_ID);
	//Original name: TRG-MEMBER-ID
	private String memberId = DefaultValues.stringVal(Len.MEMBER_ID);
	//Original name: TRG-CORP-RCV-DT
	private String corpRcvDt = DefaultValues.stringVal(Len.CORP_RCV_DT);
	//Original name: TRG-ADJD-DT
	private String adjdDt = DefaultValues.stringVal(Len.ADJD_DT);
	//Original name: TRG-PMT-FRM-DT
	private String pmtFrmDt = DefaultValues.stringVal(Len.PMT_FRM_DT);
	//Original name: TRG-PMT-THR-DT
	private String pmtThrDt = DefaultValues.stringVal(Len.PMT_THR_DT);
	//Original name: TRG-BILL-FRM-DT
	private String billFrmDt = DefaultValues.stringVal(Len.BILL_FRM_DT);
	//Original name: TRG-BILL-THR-DT
	private String billThrDt = DefaultValues.stringVal(Len.BILL_THR_DT);
	//Original name: TRG-ASG-CD
	private String asgCd = DefaultValues.stringVal(Len.ASG_CD);
	//Original name: TRG-CLM-PD-AM
	private AfDecimal clmPdAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: TRG-ADJ-TYP-CD
	private char adjTypCd = DefaultValues.CHAR_VAL;
	//Original name: TRG-KCAPS-TEAM-NM
	private String kcapsTeamNm = DefaultValues.stringVal(Len.KCAPS_TEAM_NM);
	//Original name: TRG-KCAPS-USE-ID
	private String kcapsUseId = DefaultValues.stringVal(Len.KCAPS_USE_ID);
	//Original name: TRG-HIST-LOAD-CD
	private char histLoadCd = DefaultValues.CHAR_VAL;
	//Original name: TRG-PMT-BK-PROD-CD
	private char pmtBkProdCd = DefaultValues.CHAR_VAL;
	//Original name: TRG-DRG
	private String drg = DefaultValues.stringVal(Len.DRG);
	//Original name: TRG-OI-CD
	private char oiCd = DefaultValues.CHAR_VAL;
	//Original name: TRG-PAT-ACT-ID
	private String patActId = DefaultValues.stringVal(Len.PAT_ACT_ID);
	//Original name: TRG-ITS-CLM-TYP-CD
	private char itsClmTypCd = DefaultValues.CHAR_VAL;
	//Original name: TRG-PGM-AREA-CD
	private String pgmAreaCd = DefaultValues.stringVal(Len.PGM_AREA_CD);
	//Original name: TRG-FIN-CD
	private String finCd = DefaultValues.stringVal(Len.FIN_CD);
	//Original name: TRG-ADJD-PROV-STAT
	private char adjdProvStat = DefaultValues.CHAR_VAL;
	//Original name: TRG-PRMPT-PAY-DAY
	private String prmptPayDay = DefaultValues.stringVal(Len.PRMPT_PAY_DAY);
	//Original name: TRG-PRMPT-PAY-OVRD
	private char prmptPayOvrd = DefaultValues.CHAR_VAL;
	//Original name: TRG-GRP-ID
	private String grpId = DefaultValues.stringVal(Len.GRP_ID);
	//Original name: TRG-NPI-CD
	private char npiCd = DefaultValues.CHAR_VAL;
	//Original name: TRG-LOB-CD
	private char lobCd = DefaultValues.CHAR_VAL;
	//Original name: TRG-TYP-GRP-CD
	private String typGrpCd = DefaultValues.stringVal(Len.TYP_GRP_CD);
	//Original name: TRG-NTWRK-CD
	private String ntwrkCd = DefaultValues.stringVal(Len.NTWRK_CD);
	//Original name: TRG-BASE-CN-ARNG-CD
	private String baseCnArngCd = DefaultValues.stringVal(Len.BASE_CN_ARNG_CD);
	//Original name: FILLER-TRIGGER-RECORD-LAYOUT
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: TRG-PRIM-CN-ARNG-CD
	private String primCnArngCd = DefaultValues.stringVal(Len.PRIM_CN_ARNG_CD);
	//Original name: TRG-PRC-INST-REIMB-CD
	private String prcInstReimbCd = DefaultValues.stringVal(Len.PRC_INST_REIMB_CD);
	//Original name: TRG-PRC-PROF-REIMB-CD
	private String prcProfReimbCd = DefaultValues.stringVal(Len.PRC_PROF_REIMB_CD);
	//Original name: TRG-ELIG-INST-REIMB-CD
	private String eligInstReimbCd = DefaultValues.stringVal(Len.ELIG_INST_REIMB_CD);
	//Original name: TRG-ELIG-PROF-REIMB-CD
	private String eligProfReimbCd = DefaultValues.stringVal(Len.ELIG_PROF_REIMB_CD);
	//Original name: TRG-ENR-CL-CD
	private String enrClCd = DefaultValues.stringVal(Len.ENR_CL_CD);
	//Original name: TRG-PAT-RESP
	private AfDecimal patResp = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: TRG-PWO-AMT
	private AfDecimal pwoAmt = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: TRG-NCOV-AMT
	private AfDecimal ncovAmt = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: TRG-PATIENT-NAME
	private String patientName = DefaultValues.stringVal(Len.PATIENT_NAME);
	//Original name: TRG-PERFORMING-PROVIDER-NAME
	private String performingProviderName = DefaultValues.stringVal(Len.PERFORMING_PROVIDER_NAME);
	//Original name: TRG-BILLING-PROVIDER-NAME
	private String billingProviderName = DefaultValues.stringVal(Len.BILLING_PROVIDER_NAME);
	//Original name: TRG-CLM-SYST-VER-ID
	private String clmSystVerId = DefaultValues.stringVal(Len.CLM_SYST_VER_ID);
	//Original name: TRG-IRC-CD
	private String ircCd = DefaultValues.stringVal(Len.IRC_CD);
	//Original name: TRG-BEN-PLN-ID
	private String benPlnId = DefaultValues.stringVal(Len.BEN_PLN_ID);
	//Original name: TRG-CLM-INS-LN-CD
	private String clmInsLnCd = DefaultValues.stringVal(Len.CLM_INS_LN_CD);
	//Original name: TRG-ALT-INS-ID
	private String altInsId = DefaultValues.stringVal(Len.ALT_INS_ID);
	//Original name: FILLER-TRIGGER-RECORD-LAYOUT-1
	private String flr2 = DefaultValues.stringVal(Len.FLR2);

	//==== METHODS ====
	public void setTriggerRecordLayoutBytes(byte[] buffer, int offset) {
		int position = offset;
		prodInd = MarshalByte.readString(buffer, position, Len.PROD_IND);
		position += Len.PROD_IND;
		key.setKeyBytes(buffer, position);
		position += TrgKey.Len.KEY;
		statCatgCd = MarshalByte.readString(buffer, position, Len.STAT_CATG_CD);
		position += Len.STAT_CATG_CD;
		fnlBusDt = MarshalByte.readString(buffer, position, Len.FNL_BUS_DT);
		position += Len.FNL_BUS_DT;
		infoSsystId = MarshalByte.readString(buffer, position, Len.INFO_SSYST_ID);
		position += Len.INFO_SSYST_ID;
		biProvId = MarshalByte.readString(buffer, position, Len.BI_PROV_ID);
		position += Len.BI_PROV_ID;
		biProvLob = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		biSpecCd = MarshalByte.readString(buffer, position, Len.BI_SPEC_CD);
		position += Len.BI_SPEC_CD;
		peProvId = MarshalByte.readString(buffer, position, Len.PE_PROV_ID);
		position += Len.PE_PROV_ID;
		peProvLob = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		alphPrfxCd = MarshalByte.readString(buffer, position, Len.ALPH_PRFX_CD);
		position += Len.ALPH_PRFX_CD;
		insId = MarshalByte.readString(buffer, position, Len.INS_ID);
		position += Len.INS_ID;
		memberId = MarshalByte.readString(buffer, position, Len.MEMBER_ID);
		position += Len.MEMBER_ID;
		corpRcvDt = MarshalByte.readString(buffer, position, Len.CORP_RCV_DT);
		position += Len.CORP_RCV_DT;
		adjdDt = MarshalByte.readString(buffer, position, Len.ADJD_DT);
		position += Len.ADJD_DT;
		pmtFrmDt = MarshalByte.readString(buffer, position, Len.PMT_FRM_DT);
		position += Len.PMT_FRM_DT;
		pmtThrDt = MarshalByte.readString(buffer, position, Len.PMT_THR_DT);
		position += Len.PMT_THR_DT;
		billFrmDt = MarshalByte.readString(buffer, position, Len.BILL_FRM_DT);
		position += Len.BILL_FRM_DT;
		billThrDt = MarshalByte.readString(buffer, position, Len.BILL_THR_DT);
		position += Len.BILL_THR_DT;
		asgCd = MarshalByte.readString(buffer, position, Len.ASG_CD);
		position += Len.ASG_CD;
		clmPdAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.CLM_PD_AM, Len.Fract.CLM_PD_AM));
		position += Len.CLM_PD_AM;
		adjTypCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		kcapsTeamNm = MarshalByte.readString(buffer, position, Len.KCAPS_TEAM_NM);
		position += Len.KCAPS_TEAM_NM;
		kcapsUseId = MarshalByte.readString(buffer, position, Len.KCAPS_USE_ID);
		position += Len.KCAPS_USE_ID;
		histLoadCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pmtBkProdCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		drg = MarshalByte.readString(buffer, position, Len.DRG);
		position += Len.DRG;
		oiCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		patActId = MarshalByte.readString(buffer, position, Len.PAT_ACT_ID);
		position += Len.PAT_ACT_ID;
		itsClmTypCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pgmAreaCd = MarshalByte.readString(buffer, position, Len.PGM_AREA_CD);
		position += Len.PGM_AREA_CD;
		finCd = MarshalByte.readString(buffer, position, Len.FIN_CD);
		position += Len.FIN_CD;
		adjdProvStat = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		prmptPayDay = MarshalByte.readString(buffer, position, Len.PRMPT_PAY_DAY);
		position += Len.PRMPT_PAY_DAY;
		prmptPayOvrd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		grpId = MarshalByte.readString(buffer, position, Len.GRP_ID);
		position += Len.GRP_ID;
		npiCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		lobCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		typGrpCd = MarshalByte.readString(buffer, position, Len.TYP_GRP_CD);
		position += Len.TYP_GRP_CD;
		ntwrkCd = MarshalByte.readString(buffer, position, Len.NTWRK_CD);
		position += Len.NTWRK_CD;
		baseCnArngCd = MarshalByte.readString(buffer, position, Len.BASE_CN_ARNG_CD);
		position += Len.BASE_CN_ARNG_CD;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
		position += Len.FLR1;
		primCnArngCd = MarshalByte.readString(buffer, position, Len.PRIM_CN_ARNG_CD);
		position += Len.PRIM_CN_ARNG_CD;
		prcInstReimbCd = MarshalByte.readString(buffer, position, Len.PRC_INST_REIMB_CD);
		position += Len.PRC_INST_REIMB_CD;
		prcProfReimbCd = MarshalByte.readString(buffer, position, Len.PRC_PROF_REIMB_CD);
		position += Len.PRC_PROF_REIMB_CD;
		eligInstReimbCd = MarshalByte.readString(buffer, position, Len.ELIG_INST_REIMB_CD);
		position += Len.ELIG_INST_REIMB_CD;
		eligProfReimbCd = MarshalByte.readString(buffer, position, Len.ELIG_PROF_REIMB_CD);
		position += Len.ELIG_PROF_REIMB_CD;
		enrClCd = MarshalByte.readString(buffer, position, Len.ENR_CL_CD);
		position += Len.ENR_CL_CD;
		patResp.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PAT_RESP, Len.Fract.PAT_RESP));
		position += Len.PAT_RESP;
		pwoAmt.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PWO_AMT, Len.Fract.PWO_AMT));
		position += Len.PWO_AMT;
		ncovAmt.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.NCOV_AMT, Len.Fract.NCOV_AMT));
		position += Len.NCOV_AMT;
		patientName = MarshalByte.readString(buffer, position, Len.PATIENT_NAME);
		position += Len.PATIENT_NAME;
		performingProviderName = MarshalByte.readString(buffer, position, Len.PERFORMING_PROVIDER_NAME);
		position += Len.PERFORMING_PROVIDER_NAME;
		billingProviderName = MarshalByte.readString(buffer, position, Len.BILLING_PROVIDER_NAME);
		position += Len.BILLING_PROVIDER_NAME;
		clmSystVerId = MarshalByte.readString(buffer, position, Len.CLM_SYST_VER_ID);
		position += Len.CLM_SYST_VER_ID;
		ircCd = MarshalByte.readString(buffer, position, Len.IRC_CD);
		position += Len.IRC_CD;
		benPlnId = MarshalByte.readString(buffer, position, Len.BEN_PLN_ID);
		position += Len.BEN_PLN_ID;
		clmInsLnCd = MarshalByte.readString(buffer, position, Len.CLM_INS_LN_CD);
		position += Len.CLM_INS_LN_CD;
		altInsId = MarshalByte.readString(buffer, position, Len.ALT_INS_ID);
		position += Len.ALT_INS_ID;
		flr2 = MarshalByte.readString(buffer, position, Len.FLR2);
	}

	public byte[] getTriggerRecordLayoutBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, prodInd, Len.PROD_IND);
		position += Len.PROD_IND;
		key.getKeyBytes(buffer, position);
		position += TrgKey.Len.KEY;
		MarshalByte.writeString(buffer, position, statCatgCd, Len.STAT_CATG_CD);
		position += Len.STAT_CATG_CD;
		MarshalByte.writeString(buffer, position, fnlBusDt, Len.FNL_BUS_DT);
		position += Len.FNL_BUS_DT;
		MarshalByte.writeString(buffer, position, infoSsystId, Len.INFO_SSYST_ID);
		position += Len.INFO_SSYST_ID;
		MarshalByte.writeString(buffer, position, biProvId, Len.BI_PROV_ID);
		position += Len.BI_PROV_ID;
		MarshalByte.writeChar(buffer, position, biProvLob);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, biSpecCd, Len.BI_SPEC_CD);
		position += Len.BI_SPEC_CD;
		MarshalByte.writeString(buffer, position, peProvId, Len.PE_PROV_ID);
		position += Len.PE_PROV_ID;
		MarshalByte.writeChar(buffer, position, peProvLob);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, alphPrfxCd, Len.ALPH_PRFX_CD);
		position += Len.ALPH_PRFX_CD;
		MarshalByte.writeString(buffer, position, insId, Len.INS_ID);
		position += Len.INS_ID;
		MarshalByte.writeString(buffer, position, memberId, Len.MEMBER_ID);
		position += Len.MEMBER_ID;
		MarshalByte.writeString(buffer, position, corpRcvDt, Len.CORP_RCV_DT);
		position += Len.CORP_RCV_DT;
		MarshalByte.writeString(buffer, position, adjdDt, Len.ADJD_DT);
		position += Len.ADJD_DT;
		MarshalByte.writeString(buffer, position, pmtFrmDt, Len.PMT_FRM_DT);
		position += Len.PMT_FRM_DT;
		MarshalByte.writeString(buffer, position, pmtThrDt, Len.PMT_THR_DT);
		position += Len.PMT_THR_DT;
		MarshalByte.writeString(buffer, position, billFrmDt, Len.BILL_FRM_DT);
		position += Len.BILL_FRM_DT;
		MarshalByte.writeString(buffer, position, billThrDt, Len.BILL_THR_DT);
		position += Len.BILL_THR_DT;
		MarshalByte.writeString(buffer, position, asgCd, Len.ASG_CD);
		position += Len.ASG_CD;
		MarshalByte.writeDecimalAsPacked(buffer, position, clmPdAm.copy());
		position += Len.CLM_PD_AM;
		MarshalByte.writeChar(buffer, position, adjTypCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, kcapsTeamNm, Len.KCAPS_TEAM_NM);
		position += Len.KCAPS_TEAM_NM;
		MarshalByte.writeString(buffer, position, kcapsUseId, Len.KCAPS_USE_ID);
		position += Len.KCAPS_USE_ID;
		MarshalByte.writeChar(buffer, position, histLoadCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, pmtBkProdCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, drg, Len.DRG);
		position += Len.DRG;
		MarshalByte.writeChar(buffer, position, oiCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, patActId, Len.PAT_ACT_ID);
		position += Len.PAT_ACT_ID;
		MarshalByte.writeChar(buffer, position, itsClmTypCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pgmAreaCd, Len.PGM_AREA_CD);
		position += Len.PGM_AREA_CD;
		MarshalByte.writeString(buffer, position, finCd, Len.FIN_CD);
		position += Len.FIN_CD;
		MarshalByte.writeChar(buffer, position, adjdProvStat);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, prmptPayDay, Len.PRMPT_PAY_DAY);
		position += Len.PRMPT_PAY_DAY;
		MarshalByte.writeChar(buffer, position, prmptPayOvrd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, grpId, Len.GRP_ID);
		position += Len.GRP_ID;
		MarshalByte.writeChar(buffer, position, npiCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, lobCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, typGrpCd, Len.TYP_GRP_CD);
		position += Len.TYP_GRP_CD;
		MarshalByte.writeString(buffer, position, ntwrkCd, Len.NTWRK_CD);
		position += Len.NTWRK_CD;
		MarshalByte.writeString(buffer, position, baseCnArngCd, Len.BASE_CN_ARNG_CD);
		position += Len.BASE_CN_ARNG_CD;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, primCnArngCd, Len.PRIM_CN_ARNG_CD);
		position += Len.PRIM_CN_ARNG_CD;
		MarshalByte.writeString(buffer, position, prcInstReimbCd, Len.PRC_INST_REIMB_CD);
		position += Len.PRC_INST_REIMB_CD;
		MarshalByte.writeString(buffer, position, prcProfReimbCd, Len.PRC_PROF_REIMB_CD);
		position += Len.PRC_PROF_REIMB_CD;
		MarshalByte.writeString(buffer, position, eligInstReimbCd, Len.ELIG_INST_REIMB_CD);
		position += Len.ELIG_INST_REIMB_CD;
		MarshalByte.writeString(buffer, position, eligProfReimbCd, Len.ELIG_PROF_REIMB_CD);
		position += Len.ELIG_PROF_REIMB_CD;
		MarshalByte.writeString(buffer, position, enrClCd, Len.ENR_CL_CD);
		position += Len.ENR_CL_CD;
		MarshalByte.writeDecimalAsPacked(buffer, position, patResp.copy());
		position += Len.PAT_RESP;
		MarshalByte.writeDecimalAsPacked(buffer, position, pwoAmt.copy());
		position += Len.PWO_AMT;
		MarshalByte.writeDecimalAsPacked(buffer, position, ncovAmt.copy());
		position += Len.NCOV_AMT;
		MarshalByte.writeString(buffer, position, patientName, Len.PATIENT_NAME);
		position += Len.PATIENT_NAME;
		MarshalByte.writeString(buffer, position, performingProviderName, Len.PERFORMING_PROVIDER_NAME);
		position += Len.PERFORMING_PROVIDER_NAME;
		MarshalByte.writeString(buffer, position, billingProviderName, Len.BILLING_PROVIDER_NAME);
		position += Len.BILLING_PROVIDER_NAME;
		MarshalByte.writeString(buffer, position, clmSystVerId, Len.CLM_SYST_VER_ID);
		position += Len.CLM_SYST_VER_ID;
		MarshalByte.writeString(buffer, position, ircCd, Len.IRC_CD);
		position += Len.IRC_CD;
		MarshalByte.writeString(buffer, position, benPlnId, Len.BEN_PLN_ID);
		position += Len.BEN_PLN_ID;
		MarshalByte.writeString(buffer, position, clmInsLnCd, Len.CLM_INS_LN_CD);
		position += Len.CLM_INS_LN_CD;
		MarshalByte.writeString(buffer, position, altInsId, Len.ALT_INS_ID);
		position += Len.ALT_INS_ID;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		return buffer;
	}

	public void setProdInd(String prodInd) {
		this.prodInd = Functions.subString(prodInd, Len.PROD_IND);
	}

	public String getProdInd() {
		return this.prodInd;
	}

	public void setStatCatgCd(String statCatgCd) {
		this.statCatgCd = Functions.subString(statCatgCd, Len.STAT_CATG_CD);
	}

	public String getStatCatgCd() {
		return this.statCatgCd;
	}

	public void setFnlBusDt(String fnlBusDt) {
		this.fnlBusDt = Functions.subString(fnlBusDt, Len.FNL_BUS_DT);
	}

	public String getFnlBusDt() {
		return this.fnlBusDt;
	}

	public void setInfoSsystId(String infoSsystId) {
		this.infoSsystId = Functions.subString(infoSsystId, Len.INFO_SSYST_ID);
	}

	public String getInfoSsystId() {
		return this.infoSsystId;
	}

	public void setBiProvId(String biProvId) {
		this.biProvId = Functions.subString(biProvId, Len.BI_PROV_ID);
	}

	public String getBiProvId() {
		return this.biProvId;
	}

	public void setBiProvLob(char biProvLob) {
		this.biProvLob = biProvLob;
	}

	public char getBiProvLob() {
		return this.biProvLob;
	}

	public void setBiSpecCd(String biSpecCd) {
		this.biSpecCd = Functions.subString(biSpecCd, Len.BI_SPEC_CD);
	}

	public String getBiSpecCd() {
		return this.biSpecCd;
	}

	public void setPeProvId(String peProvId) {
		this.peProvId = Functions.subString(peProvId, Len.PE_PROV_ID);
	}

	public String getPeProvId() {
		return this.peProvId;
	}

	public void setPeProvLob(char peProvLob) {
		this.peProvLob = peProvLob;
	}

	public char getPeProvLob() {
		return this.peProvLob;
	}

	public void setAlphPrfxCd(String alphPrfxCd) {
		this.alphPrfxCd = Functions.subString(alphPrfxCd, Len.ALPH_PRFX_CD);
	}

	public String getAlphPrfxCd() {
		return this.alphPrfxCd;
	}

	public void setInsId(String insId) {
		this.insId = Functions.subString(insId, Len.INS_ID);
	}

	public String getInsId() {
		return this.insId;
	}

	public void setMemberId(String memberId) {
		this.memberId = Functions.subString(memberId, Len.MEMBER_ID);
	}

	public String getMemberId() {
		return this.memberId;
	}

	public void setCorpRcvDt(String corpRcvDt) {
		this.corpRcvDt = Functions.subString(corpRcvDt, Len.CORP_RCV_DT);
	}

	public String getCorpRcvDt() {
		return this.corpRcvDt;
	}

	public void setAdjdDt(String adjdDt) {
		this.adjdDt = Functions.subString(adjdDt, Len.ADJD_DT);
	}

	public String getAdjdDt() {
		return this.adjdDt;
	}

	public void setPmtFrmDt(String pmtFrmDt) {
		this.pmtFrmDt = Functions.subString(pmtFrmDt, Len.PMT_FRM_DT);
	}

	public String getPmtFrmDt() {
		return this.pmtFrmDt;
	}

	public void setPmtThrDt(String pmtThrDt) {
		this.pmtThrDt = Functions.subString(pmtThrDt, Len.PMT_THR_DT);
	}

	public String getPmtThrDt() {
		return this.pmtThrDt;
	}

	public void setBillFrmDt(String billFrmDt) {
		this.billFrmDt = Functions.subString(billFrmDt, Len.BILL_FRM_DT);
	}

	public String getBillFrmDt() {
		return this.billFrmDt;
	}

	public void setBillThrDt(String billThrDt) {
		this.billThrDt = Functions.subString(billThrDt, Len.BILL_THR_DT);
	}

	public String getBillThrDt() {
		return this.billThrDt;
	}

	public void setAsgCd(String asgCd) {
		this.asgCd = Functions.subString(asgCd, Len.ASG_CD);
	}

	public String getAsgCd() {
		return this.asgCd;
	}

	public void setClmPdAm(AfDecimal clmPdAm) {
		this.clmPdAm.assign(clmPdAm);
	}

	public AfDecimal getClmPdAm() {
		return this.clmPdAm.copy();
	}

	public void setAdjTypCd(char adjTypCd) {
		this.adjTypCd = adjTypCd;
	}

	public char getAdjTypCd() {
		return this.adjTypCd;
	}

	public void setKcapsTeamNm(String kcapsTeamNm) {
		this.kcapsTeamNm = Functions.subString(kcapsTeamNm, Len.KCAPS_TEAM_NM);
	}

	public String getKcapsTeamNm() {
		return this.kcapsTeamNm;
	}

	public void setKcapsUseId(String kcapsUseId) {
		this.kcapsUseId = Functions.subString(kcapsUseId, Len.KCAPS_USE_ID);
	}

	public String getKcapsUseId() {
		return this.kcapsUseId;
	}

	public void setHistLoadCd(char histLoadCd) {
		this.histLoadCd = histLoadCd;
	}

	public char getHistLoadCd() {
		return this.histLoadCd;
	}

	public void setPmtBkProdCd(char pmtBkProdCd) {
		this.pmtBkProdCd = pmtBkProdCd;
	}

	public char getPmtBkProdCd() {
		return this.pmtBkProdCd;
	}

	public void setDrg(String drg) {
		this.drg = Functions.subString(drg, Len.DRG);
	}

	public String getDrg() {
		return this.drg;
	}

	public void setOiCd(char oiCd) {
		this.oiCd = oiCd;
	}

	public char getOiCd() {
		return this.oiCd;
	}

	public void setPatActId(String patActId) {
		this.patActId = Functions.subString(patActId, Len.PAT_ACT_ID);
	}

	public String getPatActId() {
		return this.patActId;
	}

	public void setItsClmTypCd(char itsClmTypCd) {
		this.itsClmTypCd = itsClmTypCd;
	}

	public char getItsClmTypCd() {
		return this.itsClmTypCd;
	}

	public void setPgmAreaCd(String pgmAreaCd) {
		this.pgmAreaCd = Functions.subString(pgmAreaCd, Len.PGM_AREA_CD);
	}

	public String getPgmAreaCd() {
		return this.pgmAreaCd;
	}

	public void setFinCd(String finCd) {
		this.finCd = Functions.subString(finCd, Len.FIN_CD);
	}

	public String getFinCd() {
		return this.finCd;
	}

	public void setAdjdProvStat(char adjdProvStat) {
		this.adjdProvStat = adjdProvStat;
	}

	public char getAdjdProvStat() {
		return this.adjdProvStat;
	}

	public void setPrmptPayDay(String prmptPayDay) {
		this.prmptPayDay = Functions.subString(prmptPayDay, Len.PRMPT_PAY_DAY);
	}

	public String getPrmptPayDay() {
		return this.prmptPayDay;
	}

	public void setPrmptPayOvrd(char prmptPayOvrd) {
		this.prmptPayOvrd = prmptPayOvrd;
	}

	public char getPrmptPayOvrd() {
		return this.prmptPayOvrd;
	}

	public void setGrpId(String grpId) {
		this.grpId = Functions.subString(grpId, Len.GRP_ID);
	}

	public String getGrpId() {
		return this.grpId;
	}

	public void setNpiCd(char npiCd) {
		this.npiCd = npiCd;
	}

	public char getNpiCd() {
		return this.npiCd;
	}

	public void setLobCd(char lobCd) {
		this.lobCd = lobCd;
	}

	public char getLobCd() {
		return this.lobCd;
	}

	public void setTypGrpCd(String typGrpCd) {
		this.typGrpCd = Functions.subString(typGrpCd, Len.TYP_GRP_CD);
	}

	public String getTypGrpCd() {
		return this.typGrpCd;
	}

	public void setNtwrkCd(String ntwrkCd) {
		this.ntwrkCd = Functions.subString(ntwrkCd, Len.NTWRK_CD);
	}

	public String getNtwrkCd() {
		return this.ntwrkCd;
	}

	public void setBaseCnArngCd(String baseCnArngCd) {
		this.baseCnArngCd = Functions.subString(baseCnArngCd, Len.BASE_CN_ARNG_CD);
	}

	public String getBaseCnArngCd() {
		return this.baseCnArngCd;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setPrimCnArngCd(String primCnArngCd) {
		this.primCnArngCd = Functions.subString(primCnArngCd, Len.PRIM_CN_ARNG_CD);
	}

	public String getPrimCnArngCd() {
		return this.primCnArngCd;
	}

	public void setPrcInstReimbCd(String prcInstReimbCd) {
		this.prcInstReimbCd = Functions.subString(prcInstReimbCd, Len.PRC_INST_REIMB_CD);
	}

	public String getPrcInstReimbCd() {
		return this.prcInstReimbCd;
	}

	public void setPrcProfReimbCd(String prcProfReimbCd) {
		this.prcProfReimbCd = Functions.subString(prcProfReimbCd, Len.PRC_PROF_REIMB_CD);
	}

	public String getPrcProfReimbCd() {
		return this.prcProfReimbCd;
	}

	public void setEligInstReimbCd(String eligInstReimbCd) {
		this.eligInstReimbCd = Functions.subString(eligInstReimbCd, Len.ELIG_INST_REIMB_CD);
	}

	public String getEligInstReimbCd() {
		return this.eligInstReimbCd;
	}

	public void setEligProfReimbCd(String eligProfReimbCd) {
		this.eligProfReimbCd = Functions.subString(eligProfReimbCd, Len.ELIG_PROF_REIMB_CD);
	}

	public String getEligProfReimbCd() {
		return this.eligProfReimbCd;
	}

	public void setEnrClCd(String enrClCd) {
		this.enrClCd = Functions.subString(enrClCd, Len.ENR_CL_CD);
	}

	public String getEnrClCd() {
		return this.enrClCd;
	}

	public void setPatResp(AfDecimal patResp) {
		this.patResp.assign(patResp);
	}

	public AfDecimal getPatResp() {
		return this.patResp.copy();
	}

	public void setPwoAmt(AfDecimal pwoAmt) {
		this.pwoAmt.assign(pwoAmt);
	}

	public AfDecimal getPwoAmt() {
		return this.pwoAmt.copy();
	}

	public void setNcovAmt(AfDecimal ncovAmt) {
		this.ncovAmt.assign(ncovAmt);
	}

	public AfDecimal getNcovAmt() {
		return this.ncovAmt.copy();
	}

	public void setPatientName(String patientName) {
		this.patientName = Functions.subString(patientName, Len.PATIENT_NAME);
	}

	public String getPatientName() {
		return this.patientName;
	}

	public void setPerformingProviderName(String performingProviderName) {
		this.performingProviderName = Functions.subString(performingProviderName, Len.PERFORMING_PROVIDER_NAME);
	}

	public String getPerformingProviderName() {
		return this.performingProviderName;
	}

	public void setBillingProviderName(String billingProviderName) {
		this.billingProviderName = Functions.subString(billingProviderName, Len.BILLING_PROVIDER_NAME);
	}

	public String getBillingProviderName() {
		return this.billingProviderName;
	}

	public void setClmSystVerId(String clmSystVerId) {
		this.clmSystVerId = Functions.subString(clmSystVerId, Len.CLM_SYST_VER_ID);
	}

	public String getClmSystVerId() {
		return this.clmSystVerId;
	}

	public void setIrcCd(String ircCd) {
		this.ircCd = Functions.subString(ircCd, Len.IRC_CD);
	}

	public String getIrcCd() {
		return this.ircCd;
	}

	public void setBenPlnId(String benPlnId) {
		this.benPlnId = Functions.subString(benPlnId, Len.BEN_PLN_ID);
	}

	public String getBenPlnId() {
		return this.benPlnId;
	}

	public void setClmInsLnCd(String clmInsLnCd) {
		this.clmInsLnCd = Functions.subString(clmInsLnCd, Len.CLM_INS_LN_CD);
	}

	public String getClmInsLnCd() {
		return this.clmInsLnCd;
	}

	public void setAltInsId(String altInsId) {
		this.altInsId = Functions.subString(altInsId, Len.ALT_INS_ID);
	}

	public String getAltInsId() {
		return this.altInsId;
	}

	public void setFlr2(String flr2) {
		this.flr2 = Functions.subString(flr2, Len.FLR2);
	}

	public String getFlr2() {
		return this.flr2;
	}

	public TrgKey getKey() {
		return key;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROD_IND = 3;
		public static final int STAT_CATG_CD = 3;
		public static final int FNL_BUS_DT = 10;
		public static final int INFO_SSYST_ID = 4;
		public static final int BI_PROV_ID = 10;
		public static final int BI_PROV_LOB = 1;
		public static final int BI_SPEC_CD = 4;
		public static final int PE_PROV_ID = 10;
		public static final int PE_PROV_LOB = 1;
		public static final int ALPH_PRFX_CD = 3;
		public static final int INS_ID = 12;
		public static final int MEMBER_ID = 14;
		public static final int CORP_RCV_DT = 10;
		public static final int ADJD_DT = 10;
		public static final int PMT_FRM_DT = 10;
		public static final int PMT_THR_DT = 10;
		public static final int BILL_FRM_DT = 10;
		public static final int BILL_THR_DT = 10;
		public static final int ASG_CD = 2;
		public static final int CLM_PD_AM = 6;
		public static final int ADJ_TYP_CD = 1;
		public static final int KCAPS_TEAM_NM = 5;
		public static final int KCAPS_USE_ID = 4;
		public static final int HIST_LOAD_CD = 1;
		public static final int PMT_BK_PROD_CD = 1;
		public static final int DRG = 4;
		public static final int OI_CD = 1;
		public static final int PAT_ACT_ID = 25;
		public static final int ITS_CLM_TYP_CD = 1;
		public static final int PGM_AREA_CD = 3;
		public static final int FIN_CD = 3;
		public static final int ADJD_PROV_STAT = 1;
		public static final int PRMPT_PAY_DAY = 2;
		public static final int PRMPT_PAY_OVRD = 1;
		public static final int GRP_ID = 12;
		public static final int NPI_CD = 1;
		public static final int LOB_CD = 1;
		public static final int TYP_GRP_CD = 3;
		public static final int NTWRK_CD = 3;
		public static final int BASE_CN_ARNG_CD = 5;
		public static final int FLR1 = 5;
		public static final int PRIM_CN_ARNG_CD = 5;
		public static final int PRC_INST_REIMB_CD = 2;
		public static final int PRC_PROF_REIMB_CD = 2;
		public static final int ELIG_INST_REIMB_CD = 2;
		public static final int ELIG_PROF_REIMB_CD = 2;
		public static final int ENR_CL_CD = 3;
		public static final int PAT_RESP = 6;
		public static final int PWO_AMT = 6;
		public static final int NCOV_AMT = 6;
		public static final int PATIENT_NAME = 39;
		public static final int PERFORMING_PROVIDER_NAME = 25;
		public static final int BILLING_PROVIDER_NAME = 25;
		public static final int CLM_SYST_VER_ID = 6;
		public static final int IRC_CD = 2;
		public static final int BEN_PLN_ID = 4;
		public static final int CLM_INS_LN_CD = 3;
		public static final int ALT_INS_ID = 12;
		public static final int FLR2 = 9;
		public static final int TRIGGER_RECORD_LAYOUT = PROD_IND + TrgKey.Len.KEY + STAT_CATG_CD + FNL_BUS_DT + INFO_SSYST_ID + BI_PROV_ID
				+ BI_PROV_LOB + BI_SPEC_CD + PE_PROV_ID + PE_PROV_LOB + ALPH_PRFX_CD + INS_ID + MEMBER_ID + CORP_RCV_DT + ADJD_DT + PMT_FRM_DT
				+ PMT_THR_DT + BILL_FRM_DT + BILL_THR_DT + ASG_CD + CLM_PD_AM + ADJ_TYP_CD + KCAPS_TEAM_NM + KCAPS_USE_ID + HIST_LOAD_CD
				+ PMT_BK_PROD_CD + DRG + OI_CD + PAT_ACT_ID + ITS_CLM_TYP_CD + PGM_AREA_CD + FIN_CD + ADJD_PROV_STAT + PRMPT_PAY_DAY + PRMPT_PAY_OVRD
				+ GRP_ID + NPI_CD + LOB_CD + TYP_GRP_CD + NTWRK_CD + BASE_CN_ARNG_CD + PRIM_CN_ARNG_CD + PRC_INST_REIMB_CD + PRC_PROF_REIMB_CD
				+ ELIG_INST_REIMB_CD + ELIG_PROF_REIMB_CD + ENR_CL_CD + PAT_RESP + PWO_AMT + NCOV_AMT + PATIENT_NAME + PERFORMING_PROVIDER_NAME
				+ BILLING_PROVIDER_NAME + CLM_SYST_VER_ID + IRC_CD + BEN_PLN_ID + CLM_INS_LN_CD + ALT_INS_ID + FLR1 + FLR2;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int CLM_PD_AM = 9;
			public static final int PAT_RESP = 9;
			public static final int PWO_AMT = 9;
			public static final int NCOV_AMT = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int CLM_PD_AM = 2;
			public static final int PAT_RESP = 2;
			public static final int PWO_AMT = 2;
			public static final int NCOV_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
