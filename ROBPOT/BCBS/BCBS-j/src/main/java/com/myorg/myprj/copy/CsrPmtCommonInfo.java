/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.ws.enums.CsrAsgCd;
import com.myorg.myprj.ws.enums.CsrBillProvIdQlfCd;
import com.myorg.myprj.ws.enums.CsrClaimLobCd;
import com.myorg.myprj.ws.enums.CsrExmnActnCd;
import com.myorg.myprj.ws.enums.CsrGmisIndicator;
import com.myorg.myprj.ws.enums.CsrPmtBkProdCd;
import com.myorg.myprj.ws.enums.CsrPrmptPayOvrd;
import com.myorg.myprj.ws.enums.CsrProdInd;
import com.myorg.myprj.ws.enums.CsrRecordType;
import com.myorg.myprj.ws.enums.CsrVersionFormatId;

/**Original name: CSR-PMT-COMMON-INFO<br>
 * Variable: CSR-PMT-COMMON-INFO from copybook NF07AREA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CsrPmtCommonInfo {

	//==== PROPERTIES ====
	//Original name: CSR-PROD-IND
	private CsrProdInd prodInd = new CsrProdInd();
	//Original name: CSR-BILLING-PROVIDER
	private CsrBillingProvider billingProvider = new CsrBillingProvider();
	//Original name: CSR-CLM-LOT-NUMBER
	private String clmLotNumber = "";
	//Original name: FILLER-CSR-CLM-CNTL-ID
	private String flr1 = "";
	//Original name: CSR-CLM-CNTL-SFX-ID
	private char clmCntlSfxId = Types.SPACE_CHAR;
	//Original name: CSR-PMT-ID
	private String pmtId = "00";
	//Original name: CSR-LI-ID
	private String liId = "000";
	//Original name: CSR-PROV-SBMT-LN-NO-ID
	private String provSbmtLnNoId = "000";
	//Original name: CSR-ADDNL-RMT-LI-ID
	private String addnlRmtLiId = "000";
	//Original name: CSR-CLM-FNLZD-PMT
	private String clmFnlzdPmt = "00";
	//Original name: CSR-GMIS-INDICATOR
	private CsrGmisIndicator gmisIndicator = new CsrGmisIndicator();
	//Original name: CSR-RECORD-TYPE
	private CsrRecordType recordType = new CsrRecordType();
	//Original name: CSR-VERSION-FORMAT-ID
	private CsrVersionFormatId versionFormatId = new CsrVersionFormatId();
	//Original name: CSR-CLAIM-LOB-CD
	private CsrClaimLobCd claimLobCd = new CsrClaimLobCd();
	//Original name: CSR-EXMN-ACTN-CD
	private CsrExmnActnCd exmnActnCd = new CsrExmnActnCd();
	//Original name: CSR-DNL-RMRK-CD
	private String dnlRmrkCd = "";
	//Original name: CSR-DNL-RMRK-USG-CD
	private char dnlRmrkUsgCd = Types.SPACE_CHAR;
	//Original name: CSR-PERFORMING-PROVIDER
	private CsrPerformingProvider performingProvider = new CsrPerformingProvider();
	//Original name: CSR-BILL-PROV-ID-QLF-CD
	private CsrBillProvIdQlfCd billProvIdQlfCd = new CsrBillProvIdQlfCd();
	//Original name: CSR-BILL-PROV-SPEC
	private String billProvSpec = "";
	//Original name: CSR-INSURED-PREFIX
	private String insuredPrefix = "";
	//Original name: CSR-INSURED-NUMBER
	private String insuredNumber = "";
	//Original name: CSR-MEMBER-ID
	private String memberId = "";
	//Original name: CSR-CORP-RCV-DT
	private CsrCorpRcvDt corpRcvDt = new CsrCorpRcvDt();
	//Original name: CSR-PMT-ADJD-DT
	private CsrCorpRcvDt pmtAdjdDt = new CsrCorpRcvDt();
	//Original name: CSR-PMT-FRM-SERV-DT
	private CsrCorpRcvDt pmtFrmServDt = new CsrCorpRcvDt();
	//Original name: CSR-PMT-THR-SERV-DT
	private CsrCorpRcvDt pmtThrServDt = new CsrCorpRcvDt();
	//Original name: CSR-BILL-FRM-DT
	private CsrCorpRcvDt billFrmDt = new CsrCorpRcvDt();
	//Original name: CSR-BILL-THR-DT
	private CsrCorpRcvDt billThrDt = new CsrCorpRcvDt();
	//Original name: CSR-DATE-COVERAGE-LAPSED
	private CsrCorpRcvDt dateCoverageLapsed = new CsrCorpRcvDt();
	//Original name: CSR-ASG-CD
	private CsrAsgCd asgCd = new CsrAsgCd();
	//Original name: CSR-CLM-PD-AM
	private AfDecimal clmPdAm = new AfDecimal("0", 11, 2);
	//Original name: CSR-ADJ-TYP-CD
	private char adjTypCd = Types.SPACE_CHAR;
	//Original name: CSR-KCAPS-TEAM-NM
	private String kcapsTeamNm = "";
	//Original name: CSR-KCAPS-USE-ID
	private String kcapsUseId = "";
	//Original name: CSR-HIST-LOAD-CD
	private char histLoadCd = Types.SPACE_CHAR;
	//Original name: CSR-PMT-BK-PROD-CD
	private CsrPmtBkProdCd pmtBkProdCd = new CsrPmtBkProdCd();
	//Original name: CSR-DRG-CD
	private String drgCd = "";
	//Original name: CSR-SCHDL-DRG-ALW-AM
	private AfDecimal schdlDrgAlwAm = new AfDecimal("0", 11, 2);
	//Original name: CSR-ALT-DRG-ALW-AM
	private AfDecimal altDrgAlwAm = new AfDecimal("0", 11, 2);
	//Original name: CSR-OVER-DRG-ALW-AM
	private AfDecimal overDrgAlwAm = new AfDecimal("0", 11, 2);
	//Original name: CSR-OI-CD
	private char oiCd = Types.SPACE_CHAR;
	//Original name: CSR-OI-PAY-NM
	private String oiPayNm = "";
	//Original name: CSR-PAT-ACT-MED-REC
	private String patActMedRec = "";
	//Original name: CSR-PGM-AREA-CD
	private String pgmAreaCd = "";
	//Original name: CSR-FIN-CD
	private String finCd = "";
	//Original name: CSR-ADJD-PROV-STAT
	private char adjdProvStat = Types.SPACE_CHAR;
	//Original name: CSR-ITS-INFORMATION
	private CsrItsInformation itsInformation = new CsrItsInformation();
	//Original name: CSR-PRMPT-PAY-DAY-CD
	private String prmptPayDayCd = "";
	//Original name: CSR-PRMPT-PAY-OVRD
	private CsrPrmptPayOvrd prmptPayOvrd = new CsrPrmptPayOvrd();
	//Original name: CSR-ACCRUED-PROMPT-PAY-INT
	private AfDecimal accruedPromptPayInt = new AfDecimal("0", 11, 2);
	//Original name: CSR-PROV-UNWRP-DT
	private CsrCorpRcvDt provUnwrpDt = new CsrCorpRcvDt();
	//Original name: CSR-GRP-ID
	private String grpId = "";
	//Original name: CSR-NPI-CD
	private char npiCd = Types.SPACE_CHAR;
	public static final char FEDERAL_EMPLOYEE_PROGRAM = 'F';
	//Original name: CSR-RATE-CD
	private String rateCd = "";
	private static final String[] RATE_BLUE_CHOICE = new String[] { "BG", "BH", "BI", "BJ", "BK", "BL", "BR", "BS", "BT", "BU", "B8", "GK" };
	//Original name: CSR-NTWRK-CD
	private String ntwrkCd = "";
	//Original name: CSR-FILLER
	private String filler = "";
	//Original name: CSR-BASE-CN-ARNG-CD
	private String baseCnArngCd = "";
	//Original name: CSR-PRIM-CN-ARNG-CD
	private String primCnArngCd = "";
	//Original name: CSR-PRC-INST-REIMB-1
	private char prcInstReimb1 = Types.SPACE_CHAR;
	//Original name: FILLER-CSR-PRC-INST-REIMB
	private char flr2 = Types.SPACE_CHAR;
	//Original name: CSR-PRC-PROF-REIMB-1
	private char prcProfReimb1 = Types.SPACE_CHAR;
	//Original name: FILLER-CSR-PRC-PROF-REIMB
	private char flr3 = Types.SPACE_CHAR;
	//Original name: CSR-ELIG-INST-REIMB
	private String eligInstReimb = "";
	//Original name: CSR-ELIG-PROF-REIMB
	private String eligProfReimb = "";
	//Original name: CSR-ENR-CL-CD
	private String enrClCd = "";
	//Original name: CSR-STAT-ADJ-PREV
	private char statAdjPrev = Types.SPACE_CHAR;
	//Original name: CSR-EFT-IND
	private char eftInd = Types.SPACE_CHAR;
	//Original name: CSR-EFT-ACCOUNT-TYPE
	private char eftAccountType = Types.SPACE_CHAR;
	//Original name: CSR-EFT-ACCT
	private String eftAcct = "";
	//Original name: CSR-EFT-TRANS
	private String eftTrans = "";
	//Original name: CSR-TYP-GRP-CD
	private String typGrpCd = "";
	//Original name: CSR-LI-FRM-SERV-DT
	private CsrCorpRcvDt liFrmServDt = new CsrCorpRcvDt();
	//Original name: CSR-LI-THR-SERV-DT
	private CsrCorpRcvDt liThrServDt = new CsrCorpRcvDt();
	//Original name: CSR-BEN-TYP-CD
	private String benTypCd = "";
	//Original name: CSR-PRMPT-PAY-DNL-IN
	private char prmptPayDnlIn = Types.SPACE_CHAR;
	//Original name: CSR-LI-EXPLN-CD
	private char liExplnCd = Types.SPACE_CHAR;
	//Original name: CSR-RVW-BY-CD
	private String rvwByCd = "";
	//Original name: CSR-ADJUDICATED-CODES
	private CsrAdjudicatedCodes adjudicatedCodes = new CsrAdjudicatedCodes();
	//Original name: CSR-REV-CD
	private String revCd = "";
	//Original name: CSR-UNIT-CT
	private AfDecimal unitCt = new AfDecimal("0", 7, 2);
	//Original name: CSR-ORIG-SUB-UNIT-CT
	private AfDecimal origSubUnitCt = new AfDecimal("0", 7, 2);
	//Original name: CSR-PLACE-OF-SERVICE
	private String placeOfService = "";
	//Original name: CSR-TYPE-OF-SERVICE
	private String typeOfService = "";
	//Original name: CSR-ORIG-SBMT-CHG-AM
	private AfDecimal origSbmtChgAm = new AfDecimal("0", 11, 2);
	//Original name: CSR-CHG-AM
	private AfDecimal chgAm = new AfDecimal("0", 11, 2);
	//Original name: CSR-LI-ALCT-OI-COV-AM
	private AfDecimal liAlctOiCovAm = new AfDecimal("0", 11, 2);
	//Original name: CSR-LI-ALCT-OI-PD-AM
	private AfDecimal liAlctOiPdAm = new AfDecimal("0", 11, 2);
	//Original name: CSR-LI-ALCT-OI-SAVE-AM
	private AfDecimal liAlctOiSaveAm = new AfDecimal("0", 11, 2);
	//Original name: FILLER-CSR-PMT-COMMON-INFO
	private char flr4 = Types.SPACE_CHAR;
	//Original name: CSR-ADJD-PROV-STAT-CD
	private char adjdProvStatCd = Types.SPACE_CHAR;
	//Original name: FILLER-CSR-PMT-COMMON-INFO-1
	private String flr5 = "";
	//Original name: CSR-LI-ALW-CHG-AM
	private AfDecimal liAlwChgAm = new AfDecimal("0", 11, 2);
	//Original name: CSR-GL-OFST-CD
	private String glOfstCd = "";
	//Original name: CSR-GL-SOTE-CD
	private String glSoteCd = "00";
	//Original name: CSR-FEP-EDT-OVRD1-CD
	private String fepEdtOvrd1Cd = "";
	//Original name: CSR-FEP-EDT-OVRD2-CD
	private String fepEdtOvrd2Cd = "";
	//Original name: CSR-FEP-EDT-OVRD3-CD
	private String fepEdtOvrd3Cd = "";
	//Original name: CSR-ADJD-OVRD1-CD
	private String adjdOvrd1Cd = "";
	//Original name: CSR-ADJD-OVRD2-CD
	private String adjdOvrd2Cd = "";
	//Original name: CSR-ADJD-OVRD3-CD
	private String adjdOvrd3Cd = "";
	//Original name: CSR-ADJD-OVRD4-CD
	private String adjdOvrd4Cd = "";
	//Original name: CSR-ADJD-OVRD5-CD
	private String adjdOvrd5Cd = "";
	//Original name: CSR-ADJD-OVRD6-CD
	private String adjdOvrd6Cd = "";
	//Original name: CSR-ADJD-OVRD7-CD
	private String adjdOvrd7Cd = "";
	//Original name: CSR-ADJD-OVRD8-CD
	private String adjdOvrd8Cd = "";
	//Original name: CSR-ADJD-OVRD9-CD
	private String adjdOvrd9Cd = "";
	//Original name: CSR-ADJD-OVRD10-CD
	private String adjdOvrd10Cd = "";
	//Original name: CSR-VOID-CD
	private char voidCd = Types.SPACE_CHAR;
	//Original name: CSR-ADJ-RESP-CD
	private char adjRespCd = Types.SPACE_CHAR;
	//Original name: CSR-ADJ-TRCK-CD
	private String adjTrckCd = "";
	//Original name: CSR-CORRECTED-PRIORITY-SUBID
	private String correctedPrioritySubid = "";
	/**Original name: CSR-REF-ID<br>
	 * <pre>      ----------------------------------------------------
	 *       -- ORIGINAL SUBMITTED INFORMATION
	 *       ----------------------------------------------------</pre>*/
	private String refId = "";
	//Original name: CSR-ORIGINAL-SUBMITTED-INFO
	private CsrOriginalSubmittedInfo originalSubmittedInfo = new CsrOriginalSubmittedInfo();
	/**Original name: FILLER-CSR-PMT-COMMON-INFO-3<br>
	 * <pre>*     10  (*)INDUS-CD                   PIC X(30) VALUE SPACES.</pre>*/
	private String flr6 = "";
	//Original name: CSR-MBR-LST-ORG-NM
	private String mbrLstOrgNm = "";
	//Original name: CSR-MBR-FRST-NM
	private String mbrFrstNm = "";
	//Original name: CSR-MBR-MID-NM
	private String mbrMidNm = "";
	//Original name: CSR-MBR-SFX-NM
	private String mbrSfxNm = "";
	//Original name: CSR-MBR-SSN-ID
	private String mbrSsnId = "";
	//Original name: CSR-PAT-LST-ORG-NM
	private String patLstOrgNm = "";
	//Original name: CSR-PAT-FRST-NM
	private String patFrstNm = "";
	//Original name: CSR-PAT-MID-NM
	private String patMidNm = "";
	//Original name: CSR-PAT-SFX-NM
	private String patSfxNm = "";
	//Original name: CSR-PAT-SSN-ID
	private String patSsnId = "";
	//Original name: CSR-PERF-LST-ORG-NM
	private String perfLstOrgNm = "";
	//Original name: CSR-PERF-FRST-NM
	private String perfFrstNm = "";
	//Original name: CSR-PERF-MID-NM
	private String perfMidNm = "";
	//Original name: CSR-PERF-SFX-NM
	private String perfSfxNm = "";
	//Original name: CSR-PERF-ENTY-TYP-QLF-CD
	private char perfEntyTypQlfCd = Types.SPACE_CHAR;
	//Original name: CSR-BILL-LST-ORG-NM
	private String billLstOrgNm = "";
	//Original name: CSR-BILL-FRST-NM
	private String billFrstNm = "";
	//Original name: CSR-BILL-MID-NM
	private String billMidNm = "";
	//Original name: CSR-BILL-SFX-NM
	private String billSfxNm = "";
	//Original name: CSR-BILL-ENTY-TYP-QLF-CD
	private char billEntyTypQlfCd = Types.SPACE_CHAR;
	//Original name: CSR-837-INSURED-PREFIX
	private String csr837InsuredPrefix = "";
	//Original name: CSR-837-INSURED-NUMBER
	private String csr837InsuredNumber = "";
	//Original name: CSR-837-INS-LST-NM
	private String csr837InsLstNm = "";
	//Original name: CSR-837-INS-FRST-NM
	private String csr837InsFrstNm = "";
	//Original name: CSR-837-INS-MID-NM
	private String csr837InsMidNm = "";
	//Original name: CSR-837-INS-SFX-NM
	private String csr837InsSfxNm = "";
	//Original name: CSR-837-CLM-SBMT-ID
	private String csr837ClmSbmtId = "";
	//Original name: CSR-837-FAC-VL-CD
	private String csr837FacVlCd = "";
	//Original name: CSR-837-FAC-QLF-CD
	private String csr837FacQlfCd = "";
	//Original name: CSR-837-CLM-FREQ-TYP-CD
	private char csr837ClmFreqTypCd = Types.SPACE_CHAR;
	//Original name: CSR-837-BILL-PROV-NPI-ID
	private String csr837BillProvNpiId = "";
	//Original name: CSR-837-PERF-PROV-NPI-ID
	private String csr837PerfProvNpiId = "";

	//==== METHODS ====
	public byte[] getPmtCommonInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, prodInd.getProdInd(), CsrProdInd.Len.PROD_IND);
		position += CsrProdInd.Len.PROD_IND;
		billingProvider.getBillingProviderBytes(buffer, position);
		position += CsrBillingProvider.Len.BILLING_PROVIDER;
		getClaimKeyBytes(buffer, position);
		position += Len.CLAIM_KEY;
		MarshalByte.writeString(buffer, position, liId, Len.LI_ID);
		position += Len.LI_ID;
		MarshalByte.writeString(buffer, position, provSbmtLnNoId, Len.PROV_SBMT_LN_NO_ID);
		position += Len.PROV_SBMT_LN_NO_ID;
		MarshalByte.writeString(buffer, position, addnlRmtLiId, Len.ADDNL_RMT_LI_ID);
		position += Len.ADDNL_RMT_LI_ID;
		MarshalByte.writeString(buffer, position, clmFnlzdPmt, Len.CLM_FNLZD_PMT);
		position += Len.CLM_FNLZD_PMT;
		MarshalByte.writeString(buffer, position, gmisIndicator.getGmisIndicator(), CsrGmisIndicator.Len.GMIS_INDICATOR);
		position += CsrGmisIndicator.Len.GMIS_INDICATOR;
		MarshalByte.writeString(buffer, position, recordType.getRecordType(), CsrRecordType.Len.RECORD_TYPE);
		position += CsrRecordType.Len.RECORD_TYPE;
		MarshalByte.writeString(buffer, position, versionFormatId.getVersionFormatId(), CsrVersionFormatId.Len.VERSION_FORMAT_ID);
		position += CsrVersionFormatId.Len.VERSION_FORMAT_ID;
		MarshalByte.writeChar(buffer, position, claimLobCd.getClaimLobCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, exmnActnCd.getExmnActnCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dnlRmrkCd, Len.DNL_RMRK_CD);
		position += Len.DNL_RMRK_CD;
		MarshalByte.writeChar(buffer, position, dnlRmrkUsgCd);
		position += Types.CHAR_SIZE;
		performingProvider.getPerformingProviderBytes(buffer, position);
		position += CsrPerformingProvider.Len.PERFORMING_PROVIDER;
		MarshalByte.writeString(buffer, position, billProvIdQlfCd.getBillProvIdQlfCd(), CsrBillProvIdQlfCd.Len.BILL_PROV_ID_QLF_CD);
		position += CsrBillProvIdQlfCd.Len.BILL_PROV_ID_QLF_CD;
		MarshalByte.writeString(buffer, position, billProvSpec, Len.BILL_PROV_SPEC);
		position += Len.BILL_PROV_SPEC;
		getInsuredIdBytes(buffer, position);
		position += Len.INSURED_ID;
		MarshalByte.writeString(buffer, position, memberId, Len.MEMBER_ID);
		position += Len.MEMBER_ID;
		corpRcvDt.getCorpRcvDtBytes(buffer, position);
		position += CsrCorpRcvDt.Len.CORP_RCV_DT;
		pmtAdjdDt.getCorpRcvDtBytes(buffer, position);
		position += CsrCorpRcvDt.Len.CORP_RCV_DT;
		pmtFrmServDt.getCorpRcvDtBytes(buffer, position);
		position += CsrCorpRcvDt.Len.CORP_RCV_DT;
		pmtThrServDt.getCorpRcvDtBytes(buffer, position);
		position += CsrCorpRcvDt.Len.CORP_RCV_DT;
		billFrmDt.getCorpRcvDtBytes(buffer, position);
		position += CsrCorpRcvDt.Len.CORP_RCV_DT;
		billThrDt.getCorpRcvDtBytes(buffer, position);
		position += CsrCorpRcvDt.Len.CORP_RCV_DT;
		dateCoverageLapsed.getCorpRcvDtBytes(buffer, position);
		position += CsrCorpRcvDt.Len.CORP_RCV_DT;
		MarshalByte.writeString(buffer, position, asgCd.getAsgCd(), CsrAsgCd.Len.ASG_CD);
		position += CsrAsgCd.Len.ASG_CD;
		MarshalByte.writeDecimalAsPacked(buffer, position, clmPdAm.copy());
		position += Len.CLM_PD_AM;
		MarshalByte.writeChar(buffer, position, adjTypCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, kcapsTeamNm, Len.KCAPS_TEAM_NM);
		position += Len.KCAPS_TEAM_NM;
		MarshalByte.writeString(buffer, position, kcapsUseId, Len.KCAPS_USE_ID);
		position += Len.KCAPS_USE_ID;
		MarshalByte.writeChar(buffer, position, histLoadCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, pmtBkProdCd.getPmtBkProdCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, drgCd, Len.DRG_CD);
		position += Len.DRG_CD;
		MarshalByte.writeDecimalAsPacked(buffer, position, schdlDrgAlwAm.copy());
		position += Len.SCHDL_DRG_ALW_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, altDrgAlwAm.copy());
		position += Len.ALT_DRG_ALW_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, overDrgAlwAm.copy());
		position += Len.OVER_DRG_ALW_AM;
		MarshalByte.writeChar(buffer, position, oiCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, oiPayNm, Len.OI_PAY_NM);
		position += Len.OI_PAY_NM;
		MarshalByte.writeString(buffer, position, patActMedRec, Len.PAT_ACT_MED_REC);
		position += Len.PAT_ACT_MED_REC;
		MarshalByte.writeString(buffer, position, pgmAreaCd, Len.PGM_AREA_CD);
		position += Len.PGM_AREA_CD;
		MarshalByte.writeString(buffer, position, finCd, Len.FIN_CD);
		position += Len.FIN_CD;
		MarshalByte.writeChar(buffer, position, adjdProvStat);
		position += Types.CHAR_SIZE;
		itsInformation.getItsInformationBytes(buffer, position);
		position += CsrItsInformation.Len.ITS_INFORMATION;
		MarshalByte.writeString(buffer, position, prmptPayDayCd, Len.PRMPT_PAY_DAY_CD);
		position += Len.PRMPT_PAY_DAY_CD;
		MarshalByte.writeChar(buffer, position, prmptPayOvrd.getPrmptPayOvrd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeDecimalAsPacked(buffer, position, accruedPromptPayInt.copy());
		position += Len.ACCRUED_PROMPT_PAY_INT;
		provUnwrpDt.getCorpRcvDtBytes(buffer, position);
		position += CsrCorpRcvDt.Len.CORP_RCV_DT;
		MarshalByte.writeString(buffer, position, grpId, Len.GRP_ID);
		position += Len.GRP_ID;
		MarshalByte.writeChar(buffer, position, npiCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rateCd, Len.RATE_CD);
		position += Len.RATE_CD;
		MarshalByte.writeString(buffer, position, ntwrkCd, Len.NTWRK_CD);
		position += Len.NTWRK_CD;
		MarshalByte.writeString(buffer, position, filler, Len.FILLER);
		position += Len.FILLER;
		MarshalByte.writeString(buffer, position, baseCnArngCd, Len.BASE_CN_ARNG_CD);
		position += Len.BASE_CN_ARNG_CD;
		MarshalByte.writeString(buffer, position, primCnArngCd, Len.PRIM_CN_ARNG_CD);
		position += Len.PRIM_CN_ARNG_CD;
		getPrcInstReimbBytes(buffer, position);
		position += Len.PRC_INST_REIMB;
		getPrcProfReimbBytes(buffer, position);
		position += Len.PRC_PROF_REIMB;
		MarshalByte.writeString(buffer, position, eligInstReimb, Len.ELIG_INST_REIMB);
		position += Len.ELIG_INST_REIMB;
		MarshalByte.writeString(buffer, position, eligProfReimb, Len.ELIG_PROF_REIMB);
		position += Len.ELIG_PROF_REIMB;
		MarshalByte.writeString(buffer, position, enrClCd, Len.ENR_CL_CD);
		position += Len.ENR_CL_CD;
		MarshalByte.writeChar(buffer, position, statAdjPrev);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, eftInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, eftAccountType);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, eftAcct, Len.EFT_ACCT);
		position += Len.EFT_ACCT;
		MarshalByte.writeString(buffer, position, eftTrans, Len.EFT_TRANS);
		position += Len.EFT_TRANS;
		MarshalByte.writeString(buffer, position, typGrpCd, Len.TYP_GRP_CD);
		position += Len.TYP_GRP_CD;
		liFrmServDt.getCorpRcvDtBytes(buffer, position);
		position += CsrCorpRcvDt.Len.CORP_RCV_DT;
		liThrServDt.getCorpRcvDtBytes(buffer, position);
		position += CsrCorpRcvDt.Len.CORP_RCV_DT;
		MarshalByte.writeString(buffer, position, benTypCd, Len.BEN_TYP_CD);
		position += Len.BEN_TYP_CD;
		MarshalByte.writeChar(buffer, position, prmptPayDnlIn);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, liExplnCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rvwByCd, Len.RVW_BY_CD);
		position += Len.RVW_BY_CD;
		adjudicatedCodes.getAdjudicatedCodesBytes(buffer, position);
		position += CsrAdjudicatedCodes.Len.ADJUDICATED_CODES;
		MarshalByte.writeString(buffer, position, revCd, Len.REV_CD);
		position += Len.REV_CD;
		MarshalByte.writeDecimalAsPacked(buffer, position, unitCt.copy());
		position += Len.UNIT_CT;
		MarshalByte.writeDecimalAsPacked(buffer, position, origSubUnitCt.copy());
		position += Len.ORIG_SUB_UNIT_CT;
		MarshalByte.writeString(buffer, position, placeOfService, Len.PLACE_OF_SERVICE);
		position += Len.PLACE_OF_SERVICE;
		MarshalByte.writeString(buffer, position, typeOfService, Len.TYPE_OF_SERVICE);
		position += Len.TYPE_OF_SERVICE;
		MarshalByte.writeDecimalAsPacked(buffer, position, origSbmtChgAm.copy());
		position += Len.ORIG_SBMT_CHG_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, chgAm.copy());
		position += Len.CHG_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, liAlctOiCovAm.copy());
		position += Len.LI_ALCT_OI_COV_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, liAlctOiPdAm.copy());
		position += Len.LI_ALCT_OI_PD_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, liAlctOiSaveAm.copy());
		position += Len.LI_ALCT_OI_SAVE_AM;
		MarshalByte.writeChar(buffer, position, flr4);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, adjdProvStatCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeDecimalAsPacked(buffer, position, liAlwChgAm.copy());
		position += Len.LI_ALW_CHG_AM;
		MarshalByte.writeString(buffer, position, glOfstCd, Len.GL_OFST_CD);
		position += Len.GL_OFST_CD;
		MarshalByte.writeString(buffer, position, glSoteCd, Len.GL_SOTE_CD);
		position += Len.GL_SOTE_CD;
		MarshalByte.writeString(buffer, position, fepEdtOvrd1Cd, Len.FEP_EDT_OVRD1_CD);
		position += Len.FEP_EDT_OVRD1_CD;
		MarshalByte.writeString(buffer, position, fepEdtOvrd2Cd, Len.FEP_EDT_OVRD2_CD);
		position += Len.FEP_EDT_OVRD2_CD;
		MarshalByte.writeString(buffer, position, fepEdtOvrd3Cd, Len.FEP_EDT_OVRD3_CD);
		position += Len.FEP_EDT_OVRD3_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd1Cd, Len.ADJD_OVRD1_CD);
		position += Len.ADJD_OVRD1_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd2Cd, Len.ADJD_OVRD2_CD);
		position += Len.ADJD_OVRD2_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd3Cd, Len.ADJD_OVRD3_CD);
		position += Len.ADJD_OVRD3_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd4Cd, Len.ADJD_OVRD4_CD);
		position += Len.ADJD_OVRD4_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd5Cd, Len.ADJD_OVRD5_CD);
		position += Len.ADJD_OVRD5_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd6Cd, Len.ADJD_OVRD6_CD);
		position += Len.ADJD_OVRD6_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd7Cd, Len.ADJD_OVRD7_CD);
		position += Len.ADJD_OVRD7_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd8Cd, Len.ADJD_OVRD8_CD);
		position += Len.ADJD_OVRD8_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd9Cd, Len.ADJD_OVRD9_CD);
		position += Len.ADJD_OVRD9_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd10Cd, Len.ADJD_OVRD10_CD);
		position += Len.ADJD_OVRD10_CD;
		MarshalByte.writeChar(buffer, position, voidCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, adjRespCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, adjTrckCd, Len.ADJ_TRCK_CD);
		position += Len.ADJ_TRCK_CD;
		MarshalByte.writeString(buffer, position, correctedPrioritySubid, Len.CORRECTED_PRIORITY_SUBID);
		position += Len.CORRECTED_PRIORITY_SUBID;
		MarshalByte.writeString(buffer, position, refId, Len.REF_ID);
		position += Len.REF_ID;
		originalSubmittedInfo.getOriginalSubmittedInfoBytes(buffer, position);
		position += CsrOriginalSubmittedInfo.Len.ORIGINAL_SUBMITTED_INFO;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, mbrLstOrgNm, Len.MBR_LST_ORG_NM);
		position += Len.MBR_LST_ORG_NM;
		MarshalByte.writeString(buffer, position, mbrFrstNm, Len.MBR_FRST_NM);
		position += Len.MBR_FRST_NM;
		MarshalByte.writeString(buffer, position, mbrMidNm, Len.MBR_MID_NM);
		position += Len.MBR_MID_NM;
		MarshalByte.writeString(buffer, position, mbrSfxNm, Len.MBR_SFX_NM);
		position += Len.MBR_SFX_NM;
		MarshalByte.writeString(buffer, position, mbrSsnId, Len.MBR_SSN_ID);
		position += Len.MBR_SSN_ID;
		MarshalByte.writeString(buffer, position, patLstOrgNm, Len.PAT_LST_ORG_NM);
		position += Len.PAT_LST_ORG_NM;
		MarshalByte.writeString(buffer, position, patFrstNm, Len.PAT_FRST_NM);
		position += Len.PAT_FRST_NM;
		MarshalByte.writeString(buffer, position, patMidNm, Len.PAT_MID_NM);
		position += Len.PAT_MID_NM;
		MarshalByte.writeString(buffer, position, patSfxNm, Len.PAT_SFX_NM);
		position += Len.PAT_SFX_NM;
		MarshalByte.writeString(buffer, position, patSsnId, Len.PAT_SSN_ID);
		position += Len.PAT_SSN_ID;
		MarshalByte.writeString(buffer, position, perfLstOrgNm, Len.PERF_LST_ORG_NM);
		position += Len.PERF_LST_ORG_NM;
		MarshalByte.writeString(buffer, position, perfFrstNm, Len.PERF_FRST_NM);
		position += Len.PERF_FRST_NM;
		MarshalByte.writeString(buffer, position, perfMidNm, Len.PERF_MID_NM);
		position += Len.PERF_MID_NM;
		MarshalByte.writeString(buffer, position, perfSfxNm, Len.PERF_SFX_NM);
		position += Len.PERF_SFX_NM;
		MarshalByte.writeChar(buffer, position, perfEntyTypQlfCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, billLstOrgNm, Len.BILL_LST_ORG_NM);
		position += Len.BILL_LST_ORG_NM;
		MarshalByte.writeString(buffer, position, billFrstNm, Len.BILL_FRST_NM);
		position += Len.BILL_FRST_NM;
		MarshalByte.writeString(buffer, position, billMidNm, Len.BILL_MID_NM);
		position += Len.BILL_MID_NM;
		MarshalByte.writeString(buffer, position, billSfxNm, Len.BILL_SFX_NM);
		position += Len.BILL_SFX_NM;
		MarshalByte.writeChar(buffer, position, billEntyTypQlfCd);
		position += Types.CHAR_SIZE;
		getCsr837InsIdBytes(buffer, position);
		position += Len.CSR837_INS_ID;
		MarshalByte.writeString(buffer, position, csr837InsLstNm, Len.CSR837_INS_LST_NM);
		position += Len.CSR837_INS_LST_NM;
		MarshalByte.writeString(buffer, position, csr837InsFrstNm, Len.CSR837_INS_FRST_NM);
		position += Len.CSR837_INS_FRST_NM;
		MarshalByte.writeString(buffer, position, csr837InsMidNm, Len.CSR837_INS_MID_NM);
		position += Len.CSR837_INS_MID_NM;
		MarshalByte.writeString(buffer, position, csr837InsSfxNm, Len.CSR837_INS_SFX_NM);
		position += Len.CSR837_INS_SFX_NM;
		MarshalByte.writeString(buffer, position, csr837ClmSbmtId, Len.CSR837_CLM_SBMT_ID);
		position += Len.CSR837_CLM_SBMT_ID;
		MarshalByte.writeString(buffer, position, csr837FacVlCd, Len.CSR837_FAC_VL_CD);
		position += Len.CSR837_FAC_VL_CD;
		MarshalByte.writeString(buffer, position, csr837FacQlfCd, Len.CSR837_FAC_QLF_CD);
		position += Len.CSR837_FAC_QLF_CD;
		MarshalByte.writeChar(buffer, position, csr837ClmFreqTypCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, csr837BillProvNpiId, Len.CSR837_BILL_PROV_NPI_ID);
		position += Len.CSR837_BILL_PROV_NPI_ID;
		MarshalByte.writeString(buffer, position, csr837PerfProvNpiId, Len.CSR837_PERF_PROV_NPI_ID);
		return buffer;
	}

	public void initPmtCommonInfoSpaces() {
		prodInd.setProdInd("");
		billingProvider.initBillingProviderSpaces();
		initClaimKeySpaces();
		liId = "";
		provSbmtLnNoId = "";
		addnlRmtLiId = "";
		clmFnlzdPmt = "";
		gmisIndicator.setGmisIndicator("");
		recordType.setRecordType("");
		versionFormatId.setVersionFormatId("");
		claimLobCd.setClaimLobCd(Types.SPACE_CHAR);
		exmnActnCd.setExmnActnCd(Types.SPACE_CHAR);
		dnlRmrkCd = "";
		dnlRmrkUsgCd = Types.SPACE_CHAR;
		performingProvider.initPerformingProviderSpaces();
		billProvIdQlfCd.setBillProvIdQlfCd("");
		billProvSpec = "";
		initInsuredIdSpaces();
		memberId = "";
		corpRcvDt.initCorpRcvDtSpaces();
		pmtAdjdDt.initCorpRcvDtSpaces();
		pmtFrmServDt.initCorpRcvDtSpaces();
		pmtThrServDt.initCorpRcvDtSpaces();
		billFrmDt.initCorpRcvDtSpaces();
		billThrDt.initCorpRcvDtSpaces();
		dateCoverageLapsed.initCorpRcvDtSpaces();
		asgCd.setAsgCd("");
		clmPdAm.setNaN();
		adjTypCd = Types.SPACE_CHAR;
		kcapsTeamNm = "";
		kcapsUseId = "";
		histLoadCd = Types.SPACE_CHAR;
		pmtBkProdCd.setPmtBkProdCd(Types.SPACE_CHAR);
		drgCd = "";
		schdlDrgAlwAm.setNaN();
		altDrgAlwAm.setNaN();
		overDrgAlwAm.setNaN();
		oiCd = Types.SPACE_CHAR;
		oiPayNm = "";
		patActMedRec = "";
		pgmAreaCd = "";
		finCd = "";
		adjdProvStat = Types.SPACE_CHAR;
		itsInformation.initItsInformationSpaces();
		prmptPayDayCd = "";
		prmptPayOvrd.setPrmptPayOvrd(Types.SPACE_CHAR);
		accruedPromptPayInt.setNaN();
		provUnwrpDt.initCorpRcvDtSpaces();
		grpId = "";
		npiCd = Types.SPACE_CHAR;
		rateCd = "";
		ntwrkCd = "";
		filler = "";
		baseCnArngCd = "";
		primCnArngCd = "";
		initPrcInstReimbSpaces();
		initPrcProfReimbSpaces();
		eligInstReimb = "";
		eligProfReimb = "";
		enrClCd = "";
		statAdjPrev = Types.SPACE_CHAR;
		eftInd = Types.SPACE_CHAR;
		eftAccountType = Types.SPACE_CHAR;
		eftAcct = "";
		eftTrans = "";
		typGrpCd = "";
		liFrmServDt.initCorpRcvDtSpaces();
		liThrServDt.initCorpRcvDtSpaces();
		benTypCd = "";
		prmptPayDnlIn = Types.SPACE_CHAR;
		liExplnCd = Types.SPACE_CHAR;
		rvwByCd = "";
		adjudicatedCodes.initAdjudicatedCodesSpaces();
		revCd = "";
		unitCt.setNaN();
		origSubUnitCt.setNaN();
		placeOfService = "";
		typeOfService = "";
		origSbmtChgAm.setNaN();
		chgAm.setNaN();
		liAlctOiCovAm.setNaN();
		liAlctOiPdAm.setNaN();
		liAlctOiSaveAm.setNaN();
		flr4 = Types.SPACE_CHAR;
		adjdProvStatCd = Types.SPACE_CHAR;
		flr5 = "";
		liAlwChgAm.setNaN();
		glOfstCd = "";
		glSoteCd = "";
		fepEdtOvrd1Cd = "";
		fepEdtOvrd2Cd = "";
		fepEdtOvrd3Cd = "";
		adjdOvrd1Cd = "";
		adjdOvrd2Cd = "";
		adjdOvrd3Cd = "";
		adjdOvrd4Cd = "";
		adjdOvrd5Cd = "";
		adjdOvrd6Cd = "";
		adjdOvrd7Cd = "";
		adjdOvrd8Cd = "";
		adjdOvrd9Cd = "";
		adjdOvrd10Cd = "";
		voidCd = Types.SPACE_CHAR;
		adjRespCd = Types.SPACE_CHAR;
		adjTrckCd = "";
		correctedPrioritySubid = "";
		refId = "";
		originalSubmittedInfo.initOriginalSubmittedInfoSpaces();
		flr6 = "";
		mbrLstOrgNm = "";
		mbrFrstNm = "";
		mbrMidNm = "";
		mbrSfxNm = "";
		mbrSsnId = "";
		patLstOrgNm = "";
		patFrstNm = "";
		patMidNm = "";
		patSfxNm = "";
		patSsnId = "";
		perfLstOrgNm = "";
		perfFrstNm = "";
		perfMidNm = "";
		perfSfxNm = "";
		perfEntyTypQlfCd = Types.SPACE_CHAR;
		billLstOrgNm = "";
		billFrstNm = "";
		billMidNm = "";
		billSfxNm = "";
		billEntyTypQlfCd = Types.SPACE_CHAR;
		initCsr837InsIdSpaces();
		csr837InsLstNm = "";
		csr837InsFrstNm = "";
		csr837InsMidNm = "";
		csr837InsSfxNm = "";
		csr837ClmSbmtId = "";
		csr837FacVlCd = "";
		csr837FacQlfCd = "";
		csr837ClmFreqTypCd = Types.SPACE_CHAR;
		csr837BillProvNpiId = "";
		csr837PerfProvNpiId = "";
	}

	public byte[] getClaimKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		getClaimNumberSuffixBytes(buffer, position);
		position += Len.CLAIM_NUMBER_SUFFIX;
		MarshalByte.writeString(buffer, position, pmtId, Len.PMT_ID);
		return buffer;
	}

	public void initClaimKeySpaces() {
		initClaimNumberSuffixSpaces();
		pmtId = "";
	}

	public byte[] getClaimNumberSuffixBytes(byte[] buffer, int offset) {
		int position = offset;
		getClmCntlIdBytes(buffer, position);
		position += Len.CLM_CNTL_ID;
		MarshalByte.writeChar(buffer, position, clmCntlSfxId);
		return buffer;
	}

	public void initClaimNumberSuffixSpaces() {
		initClmCntlIdSpaces();
		clmCntlSfxId = Types.SPACE_CHAR;
	}

	public void setClmCntlIdFormatted(String data) {
		byte[] buffer = new byte[Len.CLM_CNTL_ID];
		MarshalByte.writeString(buffer, 1, data, Len.CLM_CNTL_ID);
		setClmCntlIdBytes(buffer, 1);
	}

	public String getClmCntlIdFormatted() {
		return MarshalByteExt.bufferToStr(getClmCntlIdBytes());
	}

	/**Original name: CSR-CLM-CNTL-ID<br>*/
	public byte[] getClmCntlIdBytes() {
		byte[] buffer = new byte[Len.CLM_CNTL_ID];
		return getClmCntlIdBytes(buffer, 1);
	}

	public void setClmCntlIdBytes(byte[] buffer, int offset) {
		int position = offset;
		clmLotNumber = MarshalByte.readString(buffer, position, Len.CLM_LOT_NUMBER);
		position += Len.CLM_LOT_NUMBER;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getClmCntlIdBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clmLotNumber, Len.CLM_LOT_NUMBER);
		position += Len.CLM_LOT_NUMBER;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void initClmCntlIdSpaces() {
		clmLotNumber = "";
		flr1 = "";
	}

	public void setClmLotNumber(String clmLotNumber) {
		this.clmLotNumber = Functions.subString(clmLotNumber, Len.CLM_LOT_NUMBER);
	}

	public String getClmLotNumber() {
		return this.clmLotNumber;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.clmCntlSfxId = clmCntlSfxId;
	}

	public char getClmCntlSfxId() {
		return this.clmCntlSfxId;
	}

	public void setPmtId(short pmtId) {
		this.pmtId = NumericDisplay.asString(pmtId, Len.PMT_ID);
	}

	public void setPmtIdFormatted(String pmtId) {
		this.pmtId = Trunc.toUnsignedNumeric(pmtId, Len.PMT_ID);
	}

	public short getPmtId() {
		return NumericDisplay.asShort(this.pmtId);
	}

	public void setLiId(short liId) {
		this.liId = NumericDisplay.asString(liId, Len.LI_ID);
	}

	public void setLiIdFormatted(String liId) {
		this.liId = Trunc.toUnsignedNumeric(liId, Len.LI_ID);
	}

	public short getLiId() {
		return NumericDisplay.asShort(this.liId);
	}

	public void setProvSbmtLnNoId(short provSbmtLnNoId) {
		this.provSbmtLnNoId = NumericDisplay.asString(provSbmtLnNoId, Len.PROV_SBMT_LN_NO_ID);
	}

	public void setProvSbmtLnNoIdFormatted(String provSbmtLnNoId) {
		this.provSbmtLnNoId = Trunc.toUnsignedNumeric(provSbmtLnNoId, Len.PROV_SBMT_LN_NO_ID);
	}

	public short getProvSbmtLnNoId() {
		return NumericDisplay.asShort(this.provSbmtLnNoId);
	}

	public void setAddnlRmtLiId(short addnlRmtLiId) {
		this.addnlRmtLiId = NumericDisplay.asString(addnlRmtLiId, Len.ADDNL_RMT_LI_ID);
	}

	public void setAddnlRmtLiIdFormatted(String addnlRmtLiId) {
		this.addnlRmtLiId = Trunc.toUnsignedNumeric(addnlRmtLiId, Len.ADDNL_RMT_LI_ID);
	}

	public short getAddnlRmtLiId() {
		return NumericDisplay.asShort(this.addnlRmtLiId);
	}

	public void setClmFnlzdPmt(short clmFnlzdPmt) {
		this.clmFnlzdPmt = NumericDisplay.asString(clmFnlzdPmt, Len.CLM_FNLZD_PMT);
	}

	public void setClmFnlzdPmtFormatted(String clmFnlzdPmt) {
		this.clmFnlzdPmt = Trunc.toUnsignedNumeric(clmFnlzdPmt, Len.CLM_FNLZD_PMT);
	}

	public short getClmFnlzdPmt() {
		return NumericDisplay.asShort(this.clmFnlzdPmt);
	}

	public void setDnlRmrkCd(String dnlRmrkCd) {
		this.dnlRmrkCd = Functions.subString(dnlRmrkCd, Len.DNL_RMRK_CD);
	}

	public String getDnlRmrkCd() {
		return this.dnlRmrkCd;
	}

	public void setDnlRmrkUsgCd(char dnlRmrkUsgCd) {
		this.dnlRmrkUsgCd = dnlRmrkUsgCd;
	}

	public char getDnlRmrkUsgCd() {
		return this.dnlRmrkUsgCd;
	}

	public void setBillProvSpec(String billProvSpec) {
		this.billProvSpec = Functions.subString(billProvSpec, Len.BILL_PROV_SPEC);
	}

	public String getBillProvSpec() {
		return this.billProvSpec;
	}

	public byte[] getInsuredIdBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, insuredPrefix, Len.INSURED_PREFIX);
		position += Len.INSURED_PREFIX;
		MarshalByte.writeString(buffer, position, insuredNumber, Len.INSURED_NUMBER);
		return buffer;
	}

	public void initInsuredIdSpaces() {
		insuredPrefix = "";
		insuredNumber = "";
	}

	public void setInsuredPrefix(String insuredPrefix) {
		this.insuredPrefix = Functions.subString(insuredPrefix, Len.INSURED_PREFIX);
	}

	public String getInsuredPrefix() {
		return this.insuredPrefix;
	}

	public void setInsuredNumber(String insuredNumber) {
		this.insuredNumber = Functions.subString(insuredNumber, Len.INSURED_NUMBER);
	}

	public String getInsuredNumber() {
		return this.insuredNumber;
	}

	public void setMemberId(String memberId) {
		this.memberId = Functions.subString(memberId, Len.MEMBER_ID);
	}

	public String getMemberId() {
		return this.memberId;
	}

	public void setClmPdAm(AfDecimal clmPdAm) {
		this.clmPdAm.assign(clmPdAm);
	}

	public AfDecimal getClmPdAm() {
		return this.clmPdAm.copy();
	}

	public String getClmPdAmFormatted() {
		return PicFormatter.display(new PicParams("S9(9)V9(2)").setUsage(PicUsage.PACKED)).format(getClmPdAm()).toString();
	}

	public String getClmPdAmAsString() {
		return getClmPdAmFormatted();
	}

	public void setAdjTypCd(char adjTypCd) {
		this.adjTypCd = adjTypCd;
	}

	public char getAdjTypCd() {
		return this.adjTypCd;
	}

	public void setKcapsTeamNm(String kcapsTeamNm) {
		this.kcapsTeamNm = Functions.subString(kcapsTeamNm, Len.KCAPS_TEAM_NM);
	}

	public String getKcapsTeamNm() {
		return this.kcapsTeamNm;
	}

	public void setKcapsUseId(String kcapsUseId) {
		this.kcapsUseId = Functions.subString(kcapsUseId, Len.KCAPS_USE_ID);
	}

	public String getKcapsUseId() {
		return this.kcapsUseId;
	}

	public void setHistLoadCd(char histLoadCd) {
		this.histLoadCd = histLoadCd;
	}

	public char getHistLoadCd() {
		return this.histLoadCd;
	}

	public void setDrgCd(String drgCd) {
		this.drgCd = Functions.subString(drgCd, Len.DRG_CD);
	}

	public String getDrgCd() {
		return this.drgCd;
	}

	public void setSchdlDrgAlwAm(AfDecimal schdlDrgAlwAm) {
		this.schdlDrgAlwAm.assign(schdlDrgAlwAm);
	}

	public AfDecimal getSchdlDrgAlwAm() {
		return this.schdlDrgAlwAm.copy();
	}

	public void setAltDrgAlwAm(AfDecimal altDrgAlwAm) {
		this.altDrgAlwAm.assign(altDrgAlwAm);
	}

	public AfDecimal getAltDrgAlwAm() {
		return this.altDrgAlwAm.copy();
	}

	public void setOverDrgAlwAm(AfDecimal overDrgAlwAm) {
		this.overDrgAlwAm.assign(overDrgAlwAm);
	}

	public AfDecimal getOverDrgAlwAm() {
		return this.overDrgAlwAm.copy();
	}

	public void setOiCd(char oiCd) {
		this.oiCd = oiCd;
	}

	public char getOiCd() {
		return this.oiCd;
	}

	public void setOiPayNm(String oiPayNm) {
		this.oiPayNm = Functions.subString(oiPayNm, Len.OI_PAY_NM);
	}

	public String getOiPayNm() {
		return this.oiPayNm;
	}

	public void setPatActMedRec(String patActMedRec) {
		this.patActMedRec = Functions.subString(patActMedRec, Len.PAT_ACT_MED_REC);
	}

	public String getPatActMedRec() {
		return this.patActMedRec;
	}

	public void setPgmAreaCd(String pgmAreaCd) {
		this.pgmAreaCd = Functions.subString(pgmAreaCd, Len.PGM_AREA_CD);
	}

	public String getPgmAreaCd() {
		return this.pgmAreaCd;
	}

	public void setFinCd(String finCd) {
		this.finCd = Functions.subString(finCd, Len.FIN_CD);
	}

	public String getFinCd() {
		return this.finCd;
	}

	public void setAdjdProvStat(char adjdProvStat) {
		this.adjdProvStat = adjdProvStat;
	}

	public char getAdjdProvStat() {
		return this.adjdProvStat;
	}

	public void setPrmptPayDayCd(String prmptPayDayCd) {
		this.prmptPayDayCd = Functions.subString(prmptPayDayCd, Len.PRMPT_PAY_DAY_CD);
	}

	public String getPrmptPayDayCd() {
		return this.prmptPayDayCd;
	}

	public void setAccruedPromptPayInt(AfDecimal accruedPromptPayInt) {
		this.accruedPromptPayInt.assign(accruedPromptPayInt);
	}

	public AfDecimal getAccruedPromptPayInt() {
		return this.accruedPromptPayInt.copy();
	}

	public void setGrpId(String grpId) {
		this.grpId = Functions.subString(grpId, Len.GRP_ID);
	}

	public String getGrpId() {
		return this.grpId;
	}

	public void setNpiCd(char npiCd) {
		this.npiCd = npiCd;
	}

	public char getNpiCd() {
		return this.npiCd;
	}

	public void setRateCd(String rateCd) {
		this.rateCd = Functions.subString(rateCd, Len.RATE_CD);
	}

	public String getRateCd() {
		return this.rateCd;
	}

	public void setNtwrkCd(String ntwrkCd) {
		this.ntwrkCd = Functions.subString(ntwrkCd, Len.NTWRK_CD);
	}

	public String getNtwrkCd() {
		return this.ntwrkCd;
	}

	public void setFiller(String filler) {
		this.filler = Functions.subString(filler, Len.FILLER);
	}

	public String getFiller() {
		return this.filler;
	}

	public void setBaseCnArngCd(String baseCnArngCd) {
		this.baseCnArngCd = Functions.subString(baseCnArngCd, Len.BASE_CN_ARNG_CD);
	}

	public String getBaseCnArngCd() {
		return this.baseCnArngCd;
	}

	public void setPrimCnArngCd(String primCnArngCd) {
		this.primCnArngCd = Functions.subString(primCnArngCd, Len.PRIM_CN_ARNG_CD);
	}

	public String getPrimCnArngCd() {
		return this.primCnArngCd;
	}

	public byte[] getPrcInstReimbBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, prcInstReimb1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, flr2);
		return buffer;
	}

	public void initPrcInstReimbSpaces() {
		prcInstReimb1 = Types.SPACE_CHAR;
		flr2 = Types.SPACE_CHAR;
	}

	public void setPrcInstReimb1(char prcInstReimb1) {
		this.prcInstReimb1 = prcInstReimb1;
	}

	public char getPrcInstReimb1() {
		return this.prcInstReimb1;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public byte[] getPrcProfReimbBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, prcProfReimb1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, flr3);
		return buffer;
	}

	public void initPrcProfReimbSpaces() {
		prcProfReimb1 = Types.SPACE_CHAR;
		flr3 = Types.SPACE_CHAR;
	}

	public void setPrcProfReimb1(char prcProfReimb1) {
		this.prcProfReimb1 = prcProfReimb1;
	}

	public char getPrcProfReimb1() {
		return this.prcProfReimb1;
	}

	public char getFlr3() {
		return this.flr3;
	}

	public void setEligInstReimb(String eligInstReimb) {
		this.eligInstReimb = Functions.subString(eligInstReimb, Len.ELIG_INST_REIMB);
	}

	public String getEligInstReimb() {
		return this.eligInstReimb;
	}

	public void setEligProfReimb(String eligProfReimb) {
		this.eligProfReimb = Functions.subString(eligProfReimb, Len.ELIG_PROF_REIMB);
	}

	public String getEligProfReimb() {
		return this.eligProfReimb;
	}

	public void setEnrClCd(String enrClCd) {
		this.enrClCd = Functions.subString(enrClCd, Len.ENR_CL_CD);
	}

	public String getEnrClCd() {
		return this.enrClCd;
	}

	public void setStatAdjPrev(char statAdjPrev) {
		this.statAdjPrev = statAdjPrev;
	}

	public char getStatAdjPrev() {
		return this.statAdjPrev;
	}

	public void setEftInd(char eftInd) {
		this.eftInd = eftInd;
	}

	public char getEftInd() {
		return this.eftInd;
	}

	public void setEftAccountType(char eftAccountType) {
		this.eftAccountType = eftAccountType;
	}

	public char getEftAccountType() {
		return this.eftAccountType;
	}

	public void setEftAcct(String eftAcct) {
		this.eftAcct = Functions.subString(eftAcct, Len.EFT_ACCT);
	}

	public String getEftAcct() {
		return this.eftAcct;
	}

	public void setEftTrans(String eftTrans) {
		this.eftTrans = Functions.subString(eftTrans, Len.EFT_TRANS);
	}

	public String getEftTrans() {
		return this.eftTrans;
	}

	public void setTypGrpCd(String typGrpCd) {
		this.typGrpCd = Functions.subString(typGrpCd, Len.TYP_GRP_CD);
	}

	public String getTypGrpCd() {
		return this.typGrpCd;
	}

	public void setBenTypCd(String benTypCd) {
		this.benTypCd = Functions.subString(benTypCd, Len.BEN_TYP_CD);
	}

	public String getBenTypCd() {
		return this.benTypCd;
	}

	public void setPrmptPayDnlIn(char prmptPayDnlIn) {
		this.prmptPayDnlIn = prmptPayDnlIn;
	}

	public char getPrmptPayDnlIn() {
		return this.prmptPayDnlIn;
	}

	public void setLiExplnCd(char liExplnCd) {
		this.liExplnCd = liExplnCd;
	}

	public char getLiExplnCd() {
		return this.liExplnCd;
	}

	public void setRvwByCd(String rvwByCd) {
		this.rvwByCd = Functions.subString(rvwByCd, Len.RVW_BY_CD);
	}

	public String getRvwByCd() {
		return this.rvwByCd;
	}

	public void setRevCd(String revCd) {
		this.revCd = Functions.subString(revCd, Len.REV_CD);
	}

	public String getRevCd() {
		return this.revCd;
	}

	public void setUnitCt(AfDecimal unitCt) {
		this.unitCt.assign(unitCt);
	}

	public AfDecimal getUnitCt() {
		return this.unitCt.copy();
	}

	public void setOrigSubUnitCt(AfDecimal origSubUnitCt) {
		this.origSubUnitCt.assign(origSubUnitCt);
	}

	public AfDecimal getOrigSubUnitCt() {
		return this.origSubUnitCt.copy();
	}

	public void setPlaceOfService(String placeOfService) {
		this.placeOfService = Functions.subString(placeOfService, Len.PLACE_OF_SERVICE);
	}

	public String getPlaceOfService() {
		return this.placeOfService;
	}

	public void setTypeOfService(String typeOfService) {
		this.typeOfService = Functions.subString(typeOfService, Len.TYPE_OF_SERVICE);
	}

	public String getTypeOfService() {
		return this.typeOfService;
	}

	public void setOrigSbmtChgAm(AfDecimal origSbmtChgAm) {
		this.origSbmtChgAm.assign(origSbmtChgAm);
	}

	public AfDecimal getOrigSbmtChgAm() {
		return this.origSbmtChgAm.copy();
	}

	public void setChgAm(AfDecimal chgAm) {
		this.chgAm.assign(chgAm);
	}

	public AfDecimal getChgAm() {
		return this.chgAm.copy();
	}

	public void setLiAlctOiCovAm(AfDecimal liAlctOiCovAm) {
		this.liAlctOiCovAm.assign(liAlctOiCovAm);
	}

	public AfDecimal getLiAlctOiCovAm() {
		return this.liAlctOiCovAm.copy();
	}

	public void setLiAlctOiPdAm(AfDecimal liAlctOiPdAm) {
		this.liAlctOiPdAm.assign(liAlctOiPdAm);
	}

	public AfDecimal getLiAlctOiPdAm() {
		return this.liAlctOiPdAm.copy();
	}

	public void setLiAlctOiSaveAm(AfDecimal liAlctOiSaveAm) {
		this.liAlctOiSaveAm.assign(liAlctOiSaveAm);
	}

	public AfDecimal getLiAlctOiSaveAm() {
		return this.liAlctOiSaveAm.copy();
	}

	public char getFlr4() {
		return this.flr4;
	}

	public void setAdjdProvStatCd(char adjdProvStatCd) {
		this.adjdProvStatCd = adjdProvStatCd;
	}

	public char getAdjdProvStatCd() {
		return this.adjdProvStatCd;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setLiAlwChgAm(AfDecimal liAlwChgAm) {
		this.liAlwChgAm.assign(liAlwChgAm);
	}

	public AfDecimal getLiAlwChgAm() {
		return this.liAlwChgAm.copy();
	}

	public void setGlOfstCd(String glOfstCd) {
		this.glOfstCd = Functions.subString(glOfstCd, Len.GL_OFST_CD);
	}

	public String getGlOfstCd() {
		return this.glOfstCd;
	}

	public void setGlSoteCd(short glSoteCd) {
		this.glSoteCd = NumericDisplay.asString(glSoteCd, Len.GL_SOTE_CD);
	}

	public void setGlSoteCdFormatted(String glSoteCd) {
		this.glSoteCd = Trunc.toUnsignedNumeric(glSoteCd, Len.GL_SOTE_CD);
	}

	public short getGlSoteCd() {
		return NumericDisplay.asShort(this.glSoteCd);
	}

	public void setFepEdtOvrd1Cd(String fepEdtOvrd1Cd) {
		this.fepEdtOvrd1Cd = Functions.subString(fepEdtOvrd1Cd, Len.FEP_EDT_OVRD1_CD);
	}

	public String getFepEdtOvrd1Cd() {
		return this.fepEdtOvrd1Cd;
	}

	public void setFepEdtOvrd2Cd(String fepEdtOvrd2Cd) {
		this.fepEdtOvrd2Cd = Functions.subString(fepEdtOvrd2Cd, Len.FEP_EDT_OVRD2_CD);
	}

	public String getFepEdtOvrd2Cd() {
		return this.fepEdtOvrd2Cd;
	}

	public void setFepEdtOvrd3Cd(String fepEdtOvrd3Cd) {
		this.fepEdtOvrd3Cd = Functions.subString(fepEdtOvrd3Cd, Len.FEP_EDT_OVRD3_CD);
	}

	public String getFepEdtOvrd3Cd() {
		return this.fepEdtOvrd3Cd;
	}

	public void setAdjdOvrd1Cd(String adjdOvrd1Cd) {
		this.adjdOvrd1Cd = Functions.subString(adjdOvrd1Cd, Len.ADJD_OVRD1_CD);
	}

	public String getAdjdOvrd1Cd() {
		return this.adjdOvrd1Cd;
	}

	public void setAdjdOvrd2Cd(String adjdOvrd2Cd) {
		this.adjdOvrd2Cd = Functions.subString(adjdOvrd2Cd, Len.ADJD_OVRD2_CD);
	}

	public String getAdjdOvrd2Cd() {
		return this.adjdOvrd2Cd;
	}

	public void setAdjdOvrd3Cd(String adjdOvrd3Cd) {
		this.adjdOvrd3Cd = Functions.subString(adjdOvrd3Cd, Len.ADJD_OVRD3_CD);
	}

	public String getAdjdOvrd3Cd() {
		return this.adjdOvrd3Cd;
	}

	public void setAdjdOvrd4Cd(String adjdOvrd4Cd) {
		this.adjdOvrd4Cd = Functions.subString(adjdOvrd4Cd, Len.ADJD_OVRD4_CD);
	}

	public String getAdjdOvrd4Cd() {
		return this.adjdOvrd4Cd;
	}

	public void setAdjdOvrd5Cd(String adjdOvrd5Cd) {
		this.adjdOvrd5Cd = Functions.subString(adjdOvrd5Cd, Len.ADJD_OVRD5_CD);
	}

	public String getAdjdOvrd5Cd() {
		return this.adjdOvrd5Cd;
	}

	public void setAdjdOvrd6Cd(String adjdOvrd6Cd) {
		this.adjdOvrd6Cd = Functions.subString(adjdOvrd6Cd, Len.ADJD_OVRD6_CD);
	}

	public String getAdjdOvrd6Cd() {
		return this.adjdOvrd6Cd;
	}

	public void setAdjdOvrd7Cd(String adjdOvrd7Cd) {
		this.adjdOvrd7Cd = Functions.subString(adjdOvrd7Cd, Len.ADJD_OVRD7_CD);
	}

	public String getAdjdOvrd7Cd() {
		return this.adjdOvrd7Cd;
	}

	public void setAdjdOvrd8Cd(String adjdOvrd8Cd) {
		this.adjdOvrd8Cd = Functions.subString(adjdOvrd8Cd, Len.ADJD_OVRD8_CD);
	}

	public String getAdjdOvrd8Cd() {
		return this.adjdOvrd8Cd;
	}

	public void setAdjdOvrd9Cd(String adjdOvrd9Cd) {
		this.adjdOvrd9Cd = Functions.subString(adjdOvrd9Cd, Len.ADJD_OVRD9_CD);
	}

	public String getAdjdOvrd9Cd() {
		return this.adjdOvrd9Cd;
	}

	public void setAdjdOvrd10Cd(String adjdOvrd10Cd) {
		this.adjdOvrd10Cd = Functions.subString(adjdOvrd10Cd, Len.ADJD_OVRD10_CD);
	}

	public String getAdjdOvrd10Cd() {
		return this.adjdOvrd10Cd;
	}

	public void setVoidCd(char voidCd) {
		this.voidCd = voidCd;
	}

	public char getVoidCd() {
		return this.voidCd;
	}

	public void setAdjRespCd(char adjRespCd) {
		this.adjRespCd = adjRespCd;
	}

	public char getAdjRespCd() {
		return this.adjRespCd;
	}

	public void setAdjTrckCd(String adjTrckCd) {
		this.adjTrckCd = Functions.subString(adjTrckCd, Len.ADJ_TRCK_CD);
	}

	public String getAdjTrckCd() {
		return this.adjTrckCd;
	}

	public void setCorrectedPrioritySubid(String correctedPrioritySubid) {
		this.correctedPrioritySubid = Functions.subString(correctedPrioritySubid, Len.CORRECTED_PRIORITY_SUBID);
	}

	public String getCorrectedPrioritySubid() {
		return this.correctedPrioritySubid;
	}

	public void setRefId(String refId) {
		this.refId = Functions.subString(refId, Len.REF_ID);
	}

	public String getRefId() {
		return this.refId;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setMbrLstOrgNm(String mbrLstOrgNm) {
		this.mbrLstOrgNm = Functions.subString(mbrLstOrgNm, Len.MBR_LST_ORG_NM);
	}

	public String getMbrLstOrgNm() {
		return this.mbrLstOrgNm;
	}

	public void setMbrFrstNm(String mbrFrstNm) {
		this.mbrFrstNm = Functions.subString(mbrFrstNm, Len.MBR_FRST_NM);
	}

	public String getMbrFrstNm() {
		return this.mbrFrstNm;
	}

	public void setMbrMidNm(String mbrMidNm) {
		this.mbrMidNm = Functions.subString(mbrMidNm, Len.MBR_MID_NM);
	}

	public String getMbrMidNm() {
		return this.mbrMidNm;
	}

	public void setMbrSfxNm(String mbrSfxNm) {
		this.mbrSfxNm = Functions.subString(mbrSfxNm, Len.MBR_SFX_NM);
	}

	public String getMbrSfxNm() {
		return this.mbrSfxNm;
	}

	public void setMbrSsnId(String mbrSsnId) {
		this.mbrSsnId = Functions.subString(mbrSsnId, Len.MBR_SSN_ID);
	}

	public String getMbrSsnId() {
		return this.mbrSsnId;
	}

	public void setPatLstOrgNm(String patLstOrgNm) {
		this.patLstOrgNm = Functions.subString(patLstOrgNm, Len.PAT_LST_ORG_NM);
	}

	public String getPatLstOrgNm() {
		return this.patLstOrgNm;
	}

	public void setPatFrstNm(String patFrstNm) {
		this.patFrstNm = Functions.subString(patFrstNm, Len.PAT_FRST_NM);
	}

	public String getPatFrstNm() {
		return this.patFrstNm;
	}

	public void setPatMidNm(String patMidNm) {
		this.patMidNm = Functions.subString(patMidNm, Len.PAT_MID_NM);
	}

	public String getPatMidNm() {
		return this.patMidNm;
	}

	public void setPatSfxNm(String patSfxNm) {
		this.patSfxNm = Functions.subString(patSfxNm, Len.PAT_SFX_NM);
	}

	public String getPatSfxNm() {
		return this.patSfxNm;
	}

	public void setPatSsnId(String patSsnId) {
		this.patSsnId = Functions.subString(patSsnId, Len.PAT_SSN_ID);
	}

	public String getPatSsnId() {
		return this.patSsnId;
	}

	public void setPerfLstOrgNm(String perfLstOrgNm) {
		this.perfLstOrgNm = Functions.subString(perfLstOrgNm, Len.PERF_LST_ORG_NM);
	}

	public String getPerfLstOrgNm() {
		return this.perfLstOrgNm;
	}

	public void setPerfFrstNm(String perfFrstNm) {
		this.perfFrstNm = Functions.subString(perfFrstNm, Len.PERF_FRST_NM);
	}

	public String getPerfFrstNm() {
		return this.perfFrstNm;
	}

	public void setPerfMidNm(String perfMidNm) {
		this.perfMidNm = Functions.subString(perfMidNm, Len.PERF_MID_NM);
	}

	public String getPerfMidNm() {
		return this.perfMidNm;
	}

	public void setPerfSfxNm(String perfSfxNm) {
		this.perfSfxNm = Functions.subString(perfSfxNm, Len.PERF_SFX_NM);
	}

	public String getPerfSfxNm() {
		return this.perfSfxNm;
	}

	public void setPerfEntyTypQlfCd(char perfEntyTypQlfCd) {
		this.perfEntyTypQlfCd = perfEntyTypQlfCd;
	}

	public char getPerfEntyTypQlfCd() {
		return this.perfEntyTypQlfCd;
	}

	public void setBillLstOrgNm(String billLstOrgNm) {
		this.billLstOrgNm = Functions.subString(billLstOrgNm, Len.BILL_LST_ORG_NM);
	}

	public String getBillLstOrgNm() {
		return this.billLstOrgNm;
	}

	public void setBillFrstNm(String billFrstNm) {
		this.billFrstNm = Functions.subString(billFrstNm, Len.BILL_FRST_NM);
	}

	public String getBillFrstNm() {
		return this.billFrstNm;
	}

	public void setBillMidNm(String billMidNm) {
		this.billMidNm = Functions.subString(billMidNm, Len.BILL_MID_NM);
	}

	public String getBillMidNm() {
		return this.billMidNm;
	}

	public void setBillSfxNm(String billSfxNm) {
		this.billSfxNm = Functions.subString(billSfxNm, Len.BILL_SFX_NM);
	}

	public String getBillSfxNm() {
		return this.billSfxNm;
	}

	public void setBillEntyTypQlfCd(char billEntyTypQlfCd) {
		this.billEntyTypQlfCd = billEntyTypQlfCd;
	}

	public char getBillEntyTypQlfCd() {
		return this.billEntyTypQlfCd;
	}

	public void setCsr837InsIdFormatted(String data) {
		byte[] buffer = new byte[Len.CSR837_INS_ID];
		MarshalByte.writeString(buffer, 1, data, Len.CSR837_INS_ID);
		setCsr837InsIdBytes(buffer, 1);
	}

	public void setCsr837InsIdBytes(byte[] buffer, int offset) {
		int position = offset;
		csr837InsuredPrefix = MarshalByte.readString(buffer, position, Len.CSR837_INSURED_PREFIX);
		position += Len.CSR837_INSURED_PREFIX;
		csr837InsuredNumber = MarshalByte.readString(buffer, position, Len.CSR837_INSURED_NUMBER);
	}

	public byte[] getCsr837InsIdBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csr837InsuredPrefix, Len.CSR837_INSURED_PREFIX);
		position += Len.CSR837_INSURED_PREFIX;
		MarshalByte.writeString(buffer, position, csr837InsuredNumber, Len.CSR837_INSURED_NUMBER);
		return buffer;
	}

	public void initCsr837InsIdSpaces() {
		csr837InsuredPrefix = "";
		csr837InsuredNumber = "";
	}

	public void setCsr837InsuredPrefix(String csr837InsuredPrefix) {
		this.csr837InsuredPrefix = Functions.subString(csr837InsuredPrefix, Len.CSR837_INSURED_PREFIX);
	}

	public String getCsr837InsuredPrefix() {
		return this.csr837InsuredPrefix;
	}

	public void setCsr837InsuredNumber(String csr837InsuredNumber) {
		this.csr837InsuredNumber = Functions.subString(csr837InsuredNumber, Len.CSR837_INSURED_NUMBER);
	}

	public String getCsr837InsuredNumber() {
		return this.csr837InsuredNumber;
	}

	public void setCsr837InsLstNm(String csr837InsLstNm) {
		this.csr837InsLstNm = Functions.subString(csr837InsLstNm, Len.CSR837_INS_LST_NM);
	}

	public String getCsr837InsLstNm() {
		return this.csr837InsLstNm;
	}

	public void setCsr837InsFrstNm(String csr837InsFrstNm) {
		this.csr837InsFrstNm = Functions.subString(csr837InsFrstNm, Len.CSR837_INS_FRST_NM);
	}

	public String getCsr837InsFrstNm() {
		return this.csr837InsFrstNm;
	}

	public void setCsr837InsMidNm(String csr837InsMidNm) {
		this.csr837InsMidNm = Functions.subString(csr837InsMidNm, Len.CSR837_INS_MID_NM);
	}

	public String getCsr837InsMidNm() {
		return this.csr837InsMidNm;
	}

	public void setCsr837InsSfxNm(String csr837InsSfxNm) {
		this.csr837InsSfxNm = Functions.subString(csr837InsSfxNm, Len.CSR837_INS_SFX_NM);
	}

	public String getCsr837InsSfxNm() {
		return this.csr837InsSfxNm;
	}

	public void setCsr837ClmSbmtId(String csr837ClmSbmtId) {
		this.csr837ClmSbmtId = Functions.subString(csr837ClmSbmtId, Len.CSR837_CLM_SBMT_ID);
	}

	public String getCsr837ClmSbmtId() {
		return this.csr837ClmSbmtId;
	}

	public void setCsr837FacVlCd(String csr837FacVlCd) {
		this.csr837FacVlCd = Functions.subString(csr837FacVlCd, Len.CSR837_FAC_VL_CD);
	}

	public String getCsr837FacVlCd() {
		return this.csr837FacVlCd;
	}

	public void setCsr837FacQlfCd(String csr837FacQlfCd) {
		this.csr837FacQlfCd = Functions.subString(csr837FacQlfCd, Len.CSR837_FAC_QLF_CD);
	}

	public String getCsr837FacQlfCd() {
		return this.csr837FacQlfCd;
	}

	public void setCsr837ClmFreqTypCd(char csr837ClmFreqTypCd) {
		this.csr837ClmFreqTypCd = csr837ClmFreqTypCd;
	}

	public char getCsr837ClmFreqTypCd() {
		return this.csr837ClmFreqTypCd;
	}

	public void setCsr837BillProvNpiId(String csr837BillProvNpiId) {
		this.csr837BillProvNpiId = Functions.subString(csr837BillProvNpiId, Len.CSR837_BILL_PROV_NPI_ID);
	}

	public String getCsr837BillProvNpiId() {
		return this.csr837BillProvNpiId;
	}

	public void setCsr837PerfProvNpiId(String csr837PerfProvNpiId) {
		this.csr837PerfProvNpiId = Functions.subString(csr837PerfProvNpiId, Len.CSR837_PERF_PROV_NPI_ID);
	}

	public String getCsr837PerfProvNpiId() {
		return this.csr837PerfProvNpiId;
	}

	public CsrAdjudicatedCodes getAdjudicatedCodes() {
		return adjudicatedCodes;
	}

	public CsrAsgCd getAsgCd() {
		return asgCd;
	}

	public CsrCorpRcvDt getBillFrmDt() {
		return billFrmDt;
	}

	public CsrBillProvIdQlfCd getBillProvIdQlfCd() {
		return billProvIdQlfCd;
	}

	public CsrCorpRcvDt getBillThrDt() {
		return billThrDt;
	}

	public CsrBillingProvider getBillingProvider() {
		return billingProvider;
	}

	public CsrClaimLobCd getClaimLobCd() {
		return claimLobCd;
	}

	public CsrCorpRcvDt getCorpRcvDt() {
		return corpRcvDt;
	}

	public CsrCorpRcvDt getDateCoverageLapsed() {
		return dateCoverageLapsed;
	}

	public CsrExmnActnCd getExmnActnCd() {
		return exmnActnCd;
	}

	public CsrGmisIndicator getGmisIndicator() {
		return gmisIndicator;
	}

	public CsrItsInformation getItsInformation() {
		return itsInformation;
	}

	public CsrCorpRcvDt getLiFrmServDt() {
		return liFrmServDt;
	}

	public CsrCorpRcvDt getLiThrServDt() {
		return liThrServDt;
	}

	public CsrOriginalSubmittedInfo getOriginalSubmittedInfo() {
		return originalSubmittedInfo;
	}

	public CsrPerformingProvider getPerformingProvider() {
		return performingProvider;
	}

	public CsrCorpRcvDt getPmtAdjdDt() {
		return pmtAdjdDt;
	}

	public CsrPmtBkProdCd getPmtBkProdCd() {
		return pmtBkProdCd;
	}

	public CsrCorpRcvDt getPmtFrmServDt() {
		return pmtFrmServDt;
	}

	public CsrCorpRcvDt getPmtThrServDt() {
		return pmtThrServDt;
	}

	public CsrPrmptPayOvrd getPrmptPayOvrd() {
		return prmptPayOvrd;
	}

	public CsrProdInd getProdInd() {
		return prodInd;
	}

	public CsrCorpRcvDt getProvUnwrpDt() {
		return provUnwrpDt;
	}

	public CsrRecordType getRecordType() {
		return recordType;
	}

	public CsrVersionFormatId getVersionFormatId() {
		return versionFormatId;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LI_ID = 3;
		public static final int PROV_SBMT_LN_NO_ID = 3;
		public static final int ADDNL_RMT_LI_ID = 3;
		public static final int GL_SOTE_CD = 2;
		public static final int CLM_LOT_NUMBER = 2;
		public static final int PMT_ID = 2;
		public static final int CLM_FNLZD_PMT = 2;
		public static final int DNL_RMRK_CD = 3;
		public static final int BILL_PROV_SPEC = 4;
		public static final int INSURED_PREFIX = 3;
		public static final int INSURED_NUMBER = 12;
		public static final int MEMBER_ID = 14;
		public static final int KCAPS_TEAM_NM = 5;
		public static final int KCAPS_USE_ID = 3;
		public static final int DRG_CD = 4;
		public static final int OI_PAY_NM = 35;
		public static final int PAT_ACT_MED_REC = 25;
		public static final int PGM_AREA_CD = 3;
		public static final int FIN_CD = 3;
		public static final int PRMPT_PAY_DAY_CD = 2;
		public static final int GRP_ID = 12;
		public static final int RATE_CD = 2;
		public static final int NTWRK_CD = 3;
		public static final int FILLER = 5;
		public static final int BASE_CN_ARNG_CD = 5;
		public static final int PRIM_CN_ARNG_CD = 5;
		public static final int ELIG_INST_REIMB = 2;
		public static final int ELIG_PROF_REIMB = 2;
		public static final int ENR_CL_CD = 3;
		public static final int EFT_ACCT = 17;
		public static final int EFT_TRANS = 9;
		public static final int TYP_GRP_CD = 3;
		public static final int BEN_TYP_CD = 3;
		public static final int RVW_BY_CD = 3;
		public static final int REV_CD = 4;
		public static final int PLACE_OF_SERVICE = 2;
		public static final int TYPE_OF_SERVICE = 2;
		public static final int GL_OFST_CD = 2;
		public static final int FEP_EDT_OVRD1_CD = 4;
		public static final int FEP_EDT_OVRD2_CD = 4;
		public static final int FEP_EDT_OVRD3_CD = 4;
		public static final int ADJD_OVRD1_CD = 3;
		public static final int ADJD_OVRD2_CD = 3;
		public static final int ADJD_OVRD3_CD = 3;
		public static final int ADJD_OVRD4_CD = 3;
		public static final int ADJD_OVRD5_CD = 3;
		public static final int ADJD_OVRD6_CD = 3;
		public static final int ADJD_OVRD7_CD = 3;
		public static final int ADJD_OVRD8_CD = 3;
		public static final int ADJD_OVRD9_CD = 3;
		public static final int ADJD_OVRD10_CD = 3;
		public static final int ADJ_TRCK_CD = 2;
		public static final int CORRECTED_PRIORITY_SUBID = 30;
		public static final int REF_ID = 30;
		public static final int MBR_LST_ORG_NM = 60;
		public static final int MBR_FRST_NM = 35;
		public static final int MBR_MID_NM = 25;
		public static final int MBR_SFX_NM = 10;
		public static final int MBR_SSN_ID = 9;
		public static final int PAT_LST_ORG_NM = 60;
		public static final int PAT_FRST_NM = 35;
		public static final int PAT_MID_NM = 25;
		public static final int PAT_SFX_NM = 10;
		public static final int PAT_SSN_ID = 9;
		public static final int PERF_LST_ORG_NM = 60;
		public static final int PERF_FRST_NM = 35;
		public static final int PERF_MID_NM = 25;
		public static final int PERF_SFX_NM = 10;
		public static final int BILL_LST_ORG_NM = 60;
		public static final int BILL_FRST_NM = 35;
		public static final int BILL_MID_NM = 25;
		public static final int BILL_SFX_NM = 10;
		public static final int CSR837_INSURED_PREFIX = 3;
		public static final int CSR837_INSURED_NUMBER = 14;
		public static final int CSR837_INS_LST_NM = 35;
		public static final int CSR837_INS_FRST_NM = 25;
		public static final int CSR837_INS_MID_NM = 25;
		public static final int CSR837_INS_SFX_NM = 10;
		public static final int CSR837_CLM_SBMT_ID = 38;
		public static final int CSR837_FAC_VL_CD = 2;
		public static final int CSR837_FAC_QLF_CD = 2;
		public static final int CSR837_BILL_PROV_NPI_ID = 10;
		public static final int CSR837_PERF_PROV_NPI_ID = 10;
		public static final int FLR1 = 10;
		public static final int CLM_CNTL_ID = CLM_LOT_NUMBER + FLR1;
		public static final int CSR837_INS_ID = CSR837_INSURED_PREFIX + CSR837_INSURED_NUMBER;
		public static final int CLM_CNTL_SFX_ID = 1;
		public static final int CLAIM_NUMBER_SUFFIX = CLM_CNTL_ID + CLM_CNTL_SFX_ID;
		public static final int CLAIM_KEY = CLAIM_NUMBER_SUFFIX + PMT_ID;
		public static final int DNL_RMRK_USG_CD = 1;
		public static final int INSURED_ID = INSURED_PREFIX + INSURED_NUMBER;
		public static final int CLM_PD_AM = 6;
		public static final int ADJ_TYP_CD = 1;
		public static final int HIST_LOAD_CD = 1;
		public static final int SCHDL_DRG_ALW_AM = 6;
		public static final int ALT_DRG_ALW_AM = 6;
		public static final int OVER_DRG_ALW_AM = 6;
		public static final int OI_CD = 1;
		public static final int ADJD_PROV_STAT = 1;
		public static final int ACCRUED_PROMPT_PAY_INT = 6;
		public static final int NPI_CD = 1;
		public static final int PRC_INST_REIMB1 = 1;
		public static final int FLR2 = 1;
		public static final int PRC_INST_REIMB = PRC_INST_REIMB1 + FLR2;
		public static final int PRC_PROF_REIMB1 = 1;
		public static final int PRC_PROF_REIMB = PRC_PROF_REIMB1 + FLR2;
		public static final int STAT_ADJ_PREV = 1;
		public static final int EFT_IND = 1;
		public static final int EFT_ACCOUNT_TYPE = 1;
		public static final int PRMPT_PAY_DNL_IN = 1;
		public static final int LI_EXPLN_CD = 1;
		public static final int UNIT_CT = 4;
		public static final int ORIG_SUB_UNIT_CT = 4;
		public static final int ORIG_SBMT_CHG_AM = 6;
		public static final int CHG_AM = 6;
		public static final int LI_ALCT_OI_COV_AM = 6;
		public static final int LI_ALCT_OI_PD_AM = 6;
		public static final int LI_ALCT_OI_SAVE_AM = 6;
		public static final int ADJD_PROV_STAT_CD = 1;
		public static final int FLR5 = 2;
		public static final int LI_ALW_CHG_AM = 6;
		public static final int VOID_CD = 1;
		public static final int ADJ_RESP_CD = 1;
		public static final int FLR6 = 30;
		public static final int PERF_ENTY_TYP_QLF_CD = 1;
		public static final int BILL_ENTY_TYP_QLF_CD = 1;
		public static final int CSR837_CLM_FREQ_TYP_CD = 1;
		public static final int PMT_COMMON_INFO = CsrProdInd.Len.PROD_IND + CsrBillingProvider.Len.BILLING_PROVIDER + CLAIM_KEY + LI_ID
				+ PROV_SBMT_LN_NO_ID + ADDNL_RMT_LI_ID + CLM_FNLZD_PMT + CsrGmisIndicator.Len.GMIS_INDICATOR + CsrRecordType.Len.RECORD_TYPE
				+ CsrVersionFormatId.Len.VERSION_FORMAT_ID + CsrClaimLobCd.Len.CLAIM_LOB_CD + CsrExmnActnCd.Len.EXMN_ACTN_CD + DNL_RMRK_CD
				+ DNL_RMRK_USG_CD + CsrPerformingProvider.Len.PERFORMING_PROVIDER + CsrBillProvIdQlfCd.Len.BILL_PROV_ID_QLF_CD + BILL_PROV_SPEC
				+ INSURED_ID + MEMBER_ID + CsrCorpRcvDt.Len.CORP_RCV_DT + CsrCorpRcvDt.Len.CORP_RCV_DT + CsrCorpRcvDt.Len.CORP_RCV_DT
				+ CsrCorpRcvDt.Len.CORP_RCV_DT + CsrCorpRcvDt.Len.CORP_RCV_DT + CsrCorpRcvDt.Len.CORP_RCV_DT + CsrCorpRcvDt.Len.CORP_RCV_DT
				+ CsrAsgCd.Len.ASG_CD + CLM_PD_AM + ADJ_TYP_CD + KCAPS_TEAM_NM + KCAPS_USE_ID + HIST_LOAD_CD + CsrPmtBkProdCd.Len.PMT_BK_PROD_CD
				+ DRG_CD + SCHDL_DRG_ALW_AM + ALT_DRG_ALW_AM + OVER_DRG_ALW_AM + OI_CD + OI_PAY_NM + PAT_ACT_MED_REC + PGM_AREA_CD + FIN_CD
				+ ADJD_PROV_STAT + CsrItsInformation.Len.ITS_INFORMATION + PRMPT_PAY_DAY_CD + CsrPrmptPayOvrd.Len.PRMPT_PAY_OVRD
				+ ACCRUED_PROMPT_PAY_INT + CsrCorpRcvDt.Len.CORP_RCV_DT + GRP_ID + NPI_CD + RATE_CD + NTWRK_CD + FILLER + BASE_CN_ARNG_CD
				+ PRIM_CN_ARNG_CD + PRC_INST_REIMB + PRC_PROF_REIMB + ELIG_INST_REIMB + ELIG_PROF_REIMB + ENR_CL_CD + STAT_ADJ_PREV + EFT_IND
				+ EFT_ACCOUNT_TYPE + EFT_ACCT + EFT_TRANS + TYP_GRP_CD + CsrCorpRcvDt.Len.CORP_RCV_DT + CsrCorpRcvDt.Len.CORP_RCV_DT + BEN_TYP_CD
				+ PRMPT_PAY_DNL_IN + LI_EXPLN_CD + RVW_BY_CD + CsrAdjudicatedCodes.Len.ADJUDICATED_CODES + REV_CD + UNIT_CT + ORIG_SUB_UNIT_CT
				+ PLACE_OF_SERVICE + TYPE_OF_SERVICE + ORIG_SBMT_CHG_AM + CHG_AM + LI_ALCT_OI_COV_AM + LI_ALCT_OI_PD_AM + LI_ALCT_OI_SAVE_AM
				+ ADJD_PROV_STAT_CD + LI_ALW_CHG_AM + GL_OFST_CD + GL_SOTE_CD + FEP_EDT_OVRD1_CD + FEP_EDT_OVRD2_CD + FEP_EDT_OVRD3_CD + ADJD_OVRD1_CD
				+ ADJD_OVRD2_CD + ADJD_OVRD3_CD + ADJD_OVRD4_CD + ADJD_OVRD5_CD + ADJD_OVRD6_CD + ADJD_OVRD7_CD + ADJD_OVRD8_CD + ADJD_OVRD9_CD
				+ ADJD_OVRD10_CD + VOID_CD + ADJ_RESP_CD + ADJ_TRCK_CD + CORRECTED_PRIORITY_SUBID + REF_ID
				+ CsrOriginalSubmittedInfo.Len.ORIGINAL_SUBMITTED_INFO + MBR_LST_ORG_NM + MBR_FRST_NM + MBR_MID_NM + MBR_SFX_NM + MBR_SSN_ID
				+ PAT_LST_ORG_NM + PAT_FRST_NM + PAT_MID_NM + PAT_SFX_NM + PAT_SSN_ID + PERF_LST_ORG_NM + PERF_FRST_NM + PERF_MID_NM + PERF_SFX_NM
				+ PERF_ENTY_TYP_QLF_CD + BILL_LST_ORG_NM + BILL_FRST_NM + BILL_MID_NM + BILL_SFX_NM + BILL_ENTY_TYP_QLF_CD + CSR837_INS_ID
				+ CSR837_INS_LST_NM + CSR837_INS_FRST_NM + CSR837_INS_MID_NM + CSR837_INS_SFX_NM + CSR837_CLM_SBMT_ID + CSR837_FAC_VL_CD
				+ CSR837_FAC_QLF_CD + CSR837_CLM_FREQ_TYP_CD + CSR837_BILL_PROV_NPI_ID + CSR837_PERF_PROV_NPI_ID + FLR2 + FLR5 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int CLM_PD_AM = 9;
			public static final int SCHDL_DRG_ALW_AM = 9;
			public static final int ALT_DRG_ALW_AM = 9;
			public static final int OVER_DRG_ALW_AM = 9;
			public static final int ACCRUED_PROMPT_PAY_INT = 9;
			public static final int UNIT_CT = 5;
			public static final int ORIG_SUB_UNIT_CT = 5;
			public static final int ORIG_SBMT_CHG_AM = 9;
			public static final int CHG_AM = 9;
			public static final int LI_ALCT_OI_COV_AM = 9;
			public static final int LI_ALCT_OI_PD_AM = 9;
			public static final int LI_ALCT_OI_SAVE_AM = 9;
			public static final int LI_ALW_CHG_AM = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int CLM_PD_AM = 2;
			public static final int SCHDL_DRG_ALW_AM = 2;
			public static final int ALT_DRG_ALW_AM = 2;
			public static final int OVER_DRG_ALW_AM = 2;
			public static final int ACCRUED_PROMPT_PAY_INT = 2;
			public static final int UNIT_CT = 2;
			public static final int ORIG_SUB_UNIT_CT = 2;
			public static final int ORIG_SBMT_CHG_AM = 2;
			public static final int CHG_AM = 2;
			public static final int LI_ALCT_OI_COV_AM = 2;
			public static final int LI_ALCT_OI_PD_AM = 2;
			public static final int LI_ALCT_OI_SAVE_AM = 2;
			public static final int LI_ALW_CHG_AM = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
