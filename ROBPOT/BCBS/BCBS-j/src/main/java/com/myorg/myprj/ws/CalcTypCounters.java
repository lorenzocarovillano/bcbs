/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

/**Original name: CALC-TYP-COUNTERS<br>
 * Variable: CALC-TYP-COUNTERS from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CalcTypCounters {

	//==== PROPERTIES ====
	//Original name: CALC-CURSOR-CNT
	private long calcCursorCnt = 0;
	//Original name: TOTAL-CALC1-CNT
	private long totalCalc1Cnt = 0;
	//Original name: TOTAL-CALC2-CNT
	private long totalCalc2Cnt = 0;
	//Original name: TOTAL-CALC3-CNT
	private long totalCalc3Cnt = 0;
	//Original name: TOTAL-CALC4-CNT
	private long totalCalc4Cnt = 0;
	//Original name: TOTAL-CALC5-CNT
	private long totalCalc5Cnt = 0;
	//Original name: TOTAL-CALC6-CNT
	private long totalCalc6Cnt = 0;
	//Original name: TOTAL-CALC7-CNT
	private long totalCalc7Cnt = 0;
	//Original name: TOTAL-CALC8-CNT
	private long totalCalc8Cnt = 0;
	//Original name: TOTAL-CALC9-CNT
	private long totalCalc9Cnt = 0;

	//==== METHODS ====
	public void setCalcCursorCnt(long calcCursorCnt) {
		this.calcCursorCnt = calcCursorCnt;
	}

	public long getCalcCursorCnt() {
		return this.calcCursorCnt;
	}

	public void setTotalCalc1Cnt(long totalCalc1Cnt) {
		this.totalCalc1Cnt = totalCalc1Cnt;
	}

	public long getTotalCalc1Cnt() {
		return this.totalCalc1Cnt;
	}

	public void setTotalCalc2Cnt(long totalCalc2Cnt) {
		this.totalCalc2Cnt = totalCalc2Cnt;
	}

	public long getTotalCalc2Cnt() {
		return this.totalCalc2Cnt;
	}

	public void setTotalCalc3Cnt(long totalCalc3Cnt) {
		this.totalCalc3Cnt = totalCalc3Cnt;
	}

	public long getTotalCalc3Cnt() {
		return this.totalCalc3Cnt;
	}

	public void setTotalCalc4Cnt(long totalCalc4Cnt) {
		this.totalCalc4Cnt = totalCalc4Cnt;
	}

	public long getTotalCalc4Cnt() {
		return this.totalCalc4Cnt;
	}

	public void setTotalCalc5Cnt(long totalCalc5Cnt) {
		this.totalCalc5Cnt = totalCalc5Cnt;
	}

	public long getTotalCalc5Cnt() {
		return this.totalCalc5Cnt;
	}

	public void setTotalCalc6Cnt(long totalCalc6Cnt) {
		this.totalCalc6Cnt = totalCalc6Cnt;
	}

	public long getTotalCalc6Cnt() {
		return this.totalCalc6Cnt;
	}

	public void setTotalCalc7Cnt(long totalCalc7Cnt) {
		this.totalCalc7Cnt = totalCalc7Cnt;
	}

	public long getTotalCalc7Cnt() {
		return this.totalCalc7Cnt;
	}

	public void setTotalCalc8Cnt(long totalCalc8Cnt) {
		this.totalCalc8Cnt = totalCalc8Cnt;
	}

	public long getTotalCalc8Cnt() {
		return this.totalCalc8Cnt;
	}

	public void setTotalCalc9Cnt(long totalCalc9Cnt) {
		this.totalCalc9Cnt = totalCalc9Cnt;
	}

	public long getTotalCalc9Cnt() {
		return this.totalCalc9Cnt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CALC_CURSOR_CNT = 12;
		public static final int TOTAL_CALC1_CNT = 12;
		public static final int TOTAL_CALC2_CNT = 12;
		public static final int TOTAL_CALC3_CNT = 12;
		public static final int TOTAL_CALC4_CNT = 12;
		public static final int TOTAL_CALC5_CNT = 12;
		public static final int TOTAL_CALC6_CNT = 12;
		public static final int TOTAL_CALC7_CNT = 12;
		public static final int TOTAL_CALC8_CNT = 12;
		public static final int TOTAL_CALC9_CNT = 12;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
