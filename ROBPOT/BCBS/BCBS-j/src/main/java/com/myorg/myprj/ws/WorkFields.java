/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WORK-FIELDS<br>
 * Variable: WORK-FIELDS from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkFields {

	//==== PROPERTIES ====
	//Original name: MANUAL-PAY
	private char manualPay = 'M';
	//Original name: DAILY-INTEREST-RATE
	private AfDecimal dailyInterestRate = new AfDecimal(DefaultValues.DEC_VAL, 8, 7);
	//Original name: INTEREST-DAYS-DUE
	private int interestDaysDue = DefaultValues.INT_VAL;
	//Original name: WORK-DATE
	private WorkDate workDate = new WorkDate();

	//==== METHODS ====
	public char getManualPay() {
		return this.manualPay;
	}

	public void setDailyInterestRate(AfDecimal dailyInterestRate) {
		this.dailyInterestRate.assign(dailyInterestRate);
	}

	public AfDecimal getDailyInterestRate() {
		return this.dailyInterestRate.copy();
	}

	public void setInterestDaysDue(int interestDaysDue) {
		this.interestDaysDue = interestDaysDue;
	}

	public int getInterestDaysDue() {
		return this.interestDaysDue;
	}

	public WorkDate getWorkDate() {
		return workDate;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int INTEREST_DAYS_DUE = 7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
