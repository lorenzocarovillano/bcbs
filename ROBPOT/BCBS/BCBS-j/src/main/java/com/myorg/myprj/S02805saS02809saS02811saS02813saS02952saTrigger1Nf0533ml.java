/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.myorg.myprj.commons.data.to.IS02805saS02809saS02811saS02813saS02952saTrigger1;
import com.myorg.myprj.ws.Nf0533mlData;

/**Original name: S02805saS02809saS02811saS02813saS02952saTrigger1Nf0533ml<br>*/
public class S02805saS02809saS02811saS02813saS02952saTrigger1Nf0533ml implements IS02805saS02809saS02811saS02813saS02952saTrigger1 {

	//==== PROPERTIES ====
	private Nf0533mlData ws;

	//==== CONSTRUCTORS ====
	public S02805saS02809saS02811saS02813saS02952saTrigger1Nf0533ml(Nf0533mlData ws) {
		this.ws = ws;
	}

	//==== METHODS ====
	@Override
	public String getBillingName() {
		return ws.getWsNf0533WorkArea().getBillingName();
	}

	@Override
	public void setBillingName(String billingName) {
		ws.getWsNf0533WorkArea().setBillingName(billingName);
	}

	@Override
	public AfDecimal getDb2AcrPrmtPaIntAm() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getAcrPrmtPaIntAm();
	}

	@Override
	public void setDb2AcrPrmtPaIntAm(AfDecimal db2AcrPrmtPaIntAm) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setAcrPrmtPaIntAm(db2AcrPrmtPaIntAm.copy());
	}

	@Override
	public String getDb2BiProvId() {
		return ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvId();
	}

	@Override
	public void setDb2BiProvId(String db2BiProvId) {
		ws.getEobDb2CursorArea().getS02815ProviderInfo().setBiProvId(db2BiProvId);
	}

	@Override
	public char getDb2BiProvLob() {
		return ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvLob();
	}

	@Override
	public void setDb2BiProvLob(char db2BiProvLob) {
		ws.getEobDb2CursorArea().getS02815ProviderInfo().setBiProvLob(db2BiProvLob);
	}

	@Override
	public String getDb2BiProvSpec() {
		return ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvSpec();
	}

	@Override
	public void setDb2BiProvSpec(String db2BiProvSpec) {
		ws.getEobDb2CursorArea().getS02815ProviderInfo().setBiProvSpec(db2BiProvSpec);
	}

	@Override
	public AfDecimal getDb2C1AlwChgAm() {
		return ws.getEobDb2CursorArea().getS02811Calc1Info().getAlwChgAm();
	}

	@Override
	public void setDb2C1AlwChgAm(AfDecimal db2C1AlwChgAm) {
		ws.getEobDb2CursorArea().getS02811Calc1Info().setAlwChgAm(db2C1AlwChgAm.copy());
	}

	@Override
	public char getDb2C1CalcExplnCd() {
		return ws.getEobDb2CursorArea().getS02811Calc1Info().getCalcExplnCd();
	}

	@Override
	public void setDb2C1CalcExplnCd(char db2C1CalcExplnCd) {
		ws.getEobDb2CursorArea().getS02811Calc1Info().setCalcExplnCd(db2C1CalcExplnCd);
	}

	@Override
	public char getDb2C1CalcTypCd() {
		return ws.getEobDb2CursorArea().getS02811Calc1Info().getCalcTypCd();
	}

	@Override
	public void setDb2C1CalcTypCd(char db2C1CalcTypCd) {
		ws.getEobDb2CursorArea().getS02811Calc1Info().setCalcTypCd(db2C1CalcTypCd);
	}

	@Override
	public char getDb2C1CorpCd() {
		return ws.getEobDb2CursorArea().getS02811Calc1Info().getCorpCd();
	}

	@Override
	public void setDb2C1CorpCd(char db2C1CorpCd) {
		ws.getEobDb2CursorArea().getS02811Calc1Info().setCorpCd(db2C1CorpCd);
	}

	@Override
	public char getDb2C1CovLobCd() {
		return ws.getEobDb2CursorArea().getS02811Calc1Info().getCovLobCd();
	}

	@Override
	public void setDb2C1CovLobCd(char db2C1CovLobCd) {
		ws.getEobDb2CursorArea().getS02811Calc1Info().setCovLobCd(db2C1CovLobCd);
	}

	@Override
	public String getDb2C1GlAcctId() {
		return ws.getEobDb2CursorArea().getS02811Calc1Info().getGlAcctId();
	}

	@Override
	public void setDb2C1GlAcctId(String db2C1GlAcctId) {
		ws.getEobDb2CursorArea().getS02811Calc1Info().setGlAcctId(db2C1GlAcctId);
	}

	@Override
	public String getDb2C1InsTcCd() {
		return ws.getEobDb2CursorArea().getS02811Calc1Info().getInsTcCd();
	}

	@Override
	public void setDb2C1InsTcCd(String db2C1InsTcCd) {
		ws.getEobDb2CursorArea().getS02811Calc1Info().setInsTcCd(db2C1InsTcCd);
	}

	@Override
	public char getDb2C1PayCd() {
		return ws.getEobDb2CursorArea().getS02811Calc1Info().getPayCd();
	}

	@Override
	public void setDb2C1PayCd(char db2C1PayCd) {
		ws.getEobDb2CursorArea().getS02811Calc1Info().setPayCd(db2C1PayCd);
	}

	@Override
	public AfDecimal getDb2C1PdAm() {
		return ws.getEobDb2CursorArea().getS02811Calc1Info().getPdAm();
	}

	@Override
	public void setDb2C1PdAm(AfDecimal db2C1PdAm) {
		ws.getEobDb2CursorArea().getS02811Calc1Info().setPdAm(db2C1PdAm.copy());
	}

	@Override
	public String getDb2C1ProdCd() {
		return ws.getEobDb2CursorArea().getS02811Calc1Info().getProdCd();
	}

	@Override
	public void setDb2C1ProdCd(String db2C1ProdCd) {
		ws.getEobDb2CursorArea().getS02811Calc1Info().setProdCd(db2C1ProdCd);
	}

	@Override
	public String getDb2C1SpeciBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc1Info().getSpeciBenCd();
	}

	@Override
	public void setDb2C1SpeciBenCd(String db2C1SpeciBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc1Info().setSpeciBenCd(db2C1SpeciBenCd);
	}

	@Override
	public String getDb2C1TypBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc1Info().getTypBenCd();
	}

	@Override
	public void setDb2C1TypBenCd(String db2C1TypBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc1Info().setTypBenCd(db2C1TypBenCd);
	}

	@Override
	public AfDecimal getDb2C2AlwChgAm() {
		return ws.getEobDb2CursorArea().getS02811Calc2Info().getAlwChgAm();
	}

	@Override
	public void setDb2C2AlwChgAm(AfDecimal db2C2AlwChgAm) {
		ws.getEobDb2CursorArea().getS02811Calc2Info().setAlwChgAm(db2C2AlwChgAm.copy());
	}

	@Override
	public char getDb2C2CalcExplnCd() {
		return ws.getEobDb2CursorArea().getS02811Calc2Info().getCalcExplnCd();
	}

	@Override
	public void setDb2C2CalcExplnCd(char db2C2CalcExplnCd) {
		ws.getEobDb2CursorArea().getS02811Calc2Info().setCalcExplnCd(db2C2CalcExplnCd);
	}

	@Override
	public char getDb2C2CalcTypCd() {
		return ws.getEobDb2CursorArea().getS02811Calc2Info().getCalcTypCd();
	}

	@Override
	public void setDb2C2CalcTypCd(char db2C2CalcTypCd) {
		ws.getEobDb2CursorArea().getS02811Calc2Info().setCalcTypCd(db2C2CalcTypCd);
	}

	@Override
	public char getDb2C2CorpCd() {
		return ws.getEobDb2CursorArea().getS02811Calc2Info().getCorpCd();
	}

	@Override
	public void setDb2C2CorpCd(char db2C2CorpCd) {
		ws.getEobDb2CursorArea().getS02811Calc2Info().setCorpCd(db2C2CorpCd);
	}

	@Override
	public char getDb2C2CovLobCd() {
		return ws.getEobDb2CursorArea().getS02811Calc2Info().getCovLobCd();
	}

	@Override
	public void setDb2C2CovLobCd(char db2C2CovLobCd) {
		ws.getEobDb2CursorArea().getS02811Calc2Info().setCovLobCd(db2C2CovLobCd);
	}

	@Override
	public String getDb2C2GlAcctId() {
		return ws.getEobDb2CursorArea().getS02811Calc2Info().getGlAcctId();
	}

	@Override
	public void setDb2C2GlAcctId(String db2C2GlAcctId) {
		ws.getEobDb2CursorArea().getS02811Calc2Info().setGlAcctId(db2C2GlAcctId);
	}

	@Override
	public String getDb2C2InsTcCd() {
		return ws.getEobDb2CursorArea().getS02811Calc2Info().getInsTcCd();
	}

	@Override
	public void setDb2C2InsTcCd(String db2C2InsTcCd) {
		ws.getEobDb2CursorArea().getS02811Calc2Info().setInsTcCd(db2C2InsTcCd);
	}

	@Override
	public char getDb2C2PayCd() {
		return ws.getEobDb2CursorArea().getS02811Calc2Info().getPayCd();
	}

	@Override
	public void setDb2C2PayCd(char db2C2PayCd) {
		ws.getEobDb2CursorArea().getS02811Calc2Info().setPayCd(db2C2PayCd);
	}

	@Override
	public AfDecimal getDb2C2PdAm() {
		return ws.getEobDb2CursorArea().getS02811Calc2Info().getPdAm();
	}

	@Override
	public void setDb2C2PdAm(AfDecimal db2C2PdAm) {
		ws.getEobDb2CursorArea().getS02811Calc2Info().setPdAm(db2C2PdAm.copy());
	}

	@Override
	public String getDb2C2ProdCd() {
		return ws.getEobDb2CursorArea().getS02811Calc2Info().getProdCd();
	}

	@Override
	public void setDb2C2ProdCd(String db2C2ProdCd) {
		ws.getEobDb2CursorArea().getS02811Calc2Info().setProdCd(db2C2ProdCd);
	}

	@Override
	public String getDb2C2SpeciBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc2Info().getSpeciBenCd();
	}

	@Override
	public void setDb2C2SpeciBenCd(String db2C2SpeciBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc2Info().setSpeciBenCd(db2C2SpeciBenCd);
	}

	@Override
	public String getDb2C2TypBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc2Info().getTypBenCd();
	}

	@Override
	public void setDb2C2TypBenCd(String db2C2TypBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc2Info().setTypBenCd(db2C2TypBenCd);
	}

	@Override
	public AfDecimal getDb2C3AlwChgAm() {
		return ws.getEobDb2CursorArea().getS02811Calc3Info().getAlwChgAm();
	}

	@Override
	public void setDb2C3AlwChgAm(AfDecimal db2C3AlwChgAm) {
		ws.getEobDb2CursorArea().getS02811Calc3Info().setAlwChgAm(db2C3AlwChgAm.copy());
	}

	@Override
	public char getDb2C3CalcExplnCd() {
		return ws.getEobDb2CursorArea().getS02811Calc3Info().getCalcExplnCd();
	}

	@Override
	public void setDb2C3CalcExplnCd(char db2C3CalcExplnCd) {
		ws.getEobDb2CursorArea().getS02811Calc3Info().setCalcExplnCd(db2C3CalcExplnCd);
	}

	@Override
	public char getDb2C3CalcTypCd() {
		return ws.getEobDb2CursorArea().getS02811Calc3Info().getCalcTypCd();
	}

	@Override
	public void setDb2C3CalcTypCd(char db2C3CalcTypCd) {
		ws.getEobDb2CursorArea().getS02811Calc3Info().setCalcTypCd(db2C3CalcTypCd);
	}

	@Override
	public char getDb2C3CorpCd() {
		return ws.getEobDb2CursorArea().getS02811Calc3Info().getCorpCd();
	}

	@Override
	public void setDb2C3CorpCd(char db2C3CorpCd) {
		ws.getEobDb2CursorArea().getS02811Calc3Info().setCorpCd(db2C3CorpCd);
	}

	@Override
	public char getDb2C3CovLobCd() {
		return ws.getEobDb2CursorArea().getS02811Calc3Info().getCovLobCd();
	}

	@Override
	public void setDb2C3CovLobCd(char db2C3CovLobCd) {
		ws.getEobDb2CursorArea().getS02811Calc3Info().setCovLobCd(db2C3CovLobCd);
	}

	@Override
	public String getDb2C3GlAcctId() {
		return ws.getEobDb2CursorArea().getS02811Calc3Info().getGlAcctId();
	}

	@Override
	public void setDb2C3GlAcctId(String db2C3GlAcctId) {
		ws.getEobDb2CursorArea().getS02811Calc3Info().setGlAcctId(db2C3GlAcctId);
	}

	@Override
	public String getDb2C3InsTcCd() {
		return ws.getEobDb2CursorArea().getS02811Calc3Info().getInsTcCd();
	}

	@Override
	public void setDb2C3InsTcCd(String db2C3InsTcCd) {
		ws.getEobDb2CursorArea().getS02811Calc3Info().setInsTcCd(db2C3InsTcCd);
	}

	@Override
	public char getDb2C3PayCd() {
		return ws.getEobDb2CursorArea().getS02811Calc3Info().getPayCd();
	}

	@Override
	public void setDb2C3PayCd(char db2C3PayCd) {
		ws.getEobDb2CursorArea().getS02811Calc3Info().setPayCd(db2C3PayCd);
	}

	@Override
	public AfDecimal getDb2C3PdAm() {
		return ws.getEobDb2CursorArea().getS02811Calc3Info().getPdAm();
	}

	@Override
	public void setDb2C3PdAm(AfDecimal db2C3PdAm) {
		ws.getEobDb2CursorArea().getS02811Calc3Info().setPdAm(db2C3PdAm.copy());
	}

	@Override
	public String getDb2C3ProdCd() {
		return ws.getEobDb2CursorArea().getS02811Calc3Info().getProdCd();
	}

	@Override
	public void setDb2C3ProdCd(String db2C3ProdCd) {
		ws.getEobDb2CursorArea().getS02811Calc3Info().setProdCd(db2C3ProdCd);
	}

	@Override
	public String getDb2C3SpeciBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc3Info().getSpeciBenCd();
	}

	@Override
	public void setDb2C3SpeciBenCd(String db2C3SpeciBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc3Info().setSpeciBenCd(db2C3SpeciBenCd);
	}

	@Override
	public String getDb2C3TypBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc3Info().getTypBenCd();
	}

	@Override
	public void setDb2C3TypBenCd(String db2C3TypBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc3Info().setTypBenCd(db2C3TypBenCd);
	}

	@Override
	public AfDecimal getDb2C4AlwChgAm() {
		return ws.getEobDb2CursorArea().getS02811Calc4Info().getAlwChgAm();
	}

	@Override
	public void setDb2C4AlwChgAm(AfDecimal db2C4AlwChgAm) {
		ws.getEobDb2CursorArea().getS02811Calc4Info().setAlwChgAm(db2C4AlwChgAm.copy());
	}

	@Override
	public char getDb2C4CalcExplnCd() {
		return ws.getEobDb2CursorArea().getS02811Calc4Info().getCalcExplnCd();
	}

	@Override
	public void setDb2C4CalcExplnCd(char db2C4CalcExplnCd) {
		ws.getEobDb2CursorArea().getS02811Calc4Info().setCalcExplnCd(db2C4CalcExplnCd);
	}

	@Override
	public char getDb2C4CalcTypCd() {
		return ws.getEobDb2CursorArea().getS02811Calc4Info().getCalcTypCd();
	}

	@Override
	public void setDb2C4CalcTypCd(char db2C4CalcTypCd) {
		ws.getEobDb2CursorArea().getS02811Calc4Info().setCalcTypCd(db2C4CalcTypCd);
	}

	@Override
	public char getDb2C4CorpCd() {
		return ws.getEobDb2CursorArea().getS02811Calc4Info().getCorpCd();
	}

	@Override
	public void setDb2C4CorpCd(char db2C4CorpCd) {
		ws.getEobDb2CursorArea().getS02811Calc4Info().setCorpCd(db2C4CorpCd);
	}

	@Override
	public char getDb2C4CovLobCd() {
		return ws.getEobDb2CursorArea().getS02811Calc4Info().getCovLobCd();
	}

	@Override
	public void setDb2C4CovLobCd(char db2C4CovLobCd) {
		ws.getEobDb2CursorArea().getS02811Calc4Info().setCovLobCd(db2C4CovLobCd);
	}

	@Override
	public String getDb2C4GlAcctId() {
		return ws.getEobDb2CursorArea().getS02811Calc4Info().getGlAcctId();
	}

	@Override
	public void setDb2C4GlAcctId(String db2C4GlAcctId) {
		ws.getEobDb2CursorArea().getS02811Calc4Info().setGlAcctId(db2C4GlAcctId);
	}

	@Override
	public String getDb2C4InsTcCd() {
		return ws.getEobDb2CursorArea().getS02811Calc4Info().getInsTcCd();
	}

	@Override
	public void setDb2C4InsTcCd(String db2C4InsTcCd) {
		ws.getEobDb2CursorArea().getS02811Calc4Info().setInsTcCd(db2C4InsTcCd);
	}

	@Override
	public char getDb2C4PayCd() {
		return ws.getEobDb2CursorArea().getS02811Calc4Info().getPayCd();
	}

	@Override
	public void setDb2C4PayCd(char db2C4PayCd) {
		ws.getEobDb2CursorArea().getS02811Calc4Info().setPayCd(db2C4PayCd);
	}

	@Override
	public AfDecimal getDb2C4PdAm() {
		return ws.getEobDb2CursorArea().getS02811Calc4Info().getPdAm();
	}

	@Override
	public void setDb2C4PdAm(AfDecimal db2C4PdAm) {
		ws.getEobDb2CursorArea().getS02811Calc4Info().setPdAm(db2C4PdAm.copy());
	}

	@Override
	public String getDb2C4ProdCd() {
		return ws.getEobDb2CursorArea().getS02811Calc4Info().getProdCd();
	}

	@Override
	public void setDb2C4ProdCd(String db2C4ProdCd) {
		ws.getEobDb2CursorArea().getS02811Calc4Info().setProdCd(db2C4ProdCd);
	}

	@Override
	public String getDb2C4SpeciBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc4Info().getSpeciBenCd();
	}

	@Override
	public void setDb2C4SpeciBenCd(String db2C4SpeciBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc4Info().setSpeciBenCd(db2C4SpeciBenCd);
	}

	@Override
	public String getDb2C4TypBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc4Info().getTypBenCd();
	}

	@Override
	public void setDb2C4TypBenCd(String db2C4TypBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc4Info().setTypBenCd(db2C4TypBenCd);
	}

	@Override
	public AfDecimal getDb2C5AlwChgAm() {
		return ws.getEobDb2CursorArea().getS02811Calc5Info().getAlwChgAm();
	}

	@Override
	public void setDb2C5AlwChgAm(AfDecimal db2C5AlwChgAm) {
		ws.getEobDb2CursorArea().getS02811Calc5Info().setAlwChgAm(db2C5AlwChgAm.copy());
	}

	@Override
	public char getDb2C5CalcExplnCd() {
		return ws.getEobDb2CursorArea().getS02811Calc5Info().getCalcExplnCd();
	}

	@Override
	public void setDb2C5CalcExplnCd(char db2C5CalcExplnCd) {
		ws.getEobDb2CursorArea().getS02811Calc5Info().setCalcExplnCd(db2C5CalcExplnCd);
	}

	@Override
	public char getDb2C5CalcTypCd() {
		return ws.getEobDb2CursorArea().getS02811Calc5Info().getCalcTypCd();
	}

	@Override
	public void setDb2C5CalcTypCd(char db2C5CalcTypCd) {
		ws.getEobDb2CursorArea().getS02811Calc5Info().setCalcTypCd(db2C5CalcTypCd);
	}

	@Override
	public char getDb2C5CorpCd() {
		return ws.getEobDb2CursorArea().getS02811Calc5Info().getCorpCd();
	}

	@Override
	public void setDb2C5CorpCd(char db2C5CorpCd) {
		ws.getEobDb2CursorArea().getS02811Calc5Info().setCorpCd(db2C5CorpCd);
	}

	@Override
	public char getDb2C5CovLobCd() {
		return ws.getEobDb2CursorArea().getS02811Calc5Info().getCovLobCd();
	}

	@Override
	public void setDb2C5CovLobCd(char db2C5CovLobCd) {
		ws.getEobDb2CursorArea().getS02811Calc5Info().setCovLobCd(db2C5CovLobCd);
	}

	@Override
	public String getDb2C5GlAcctId() {
		return ws.getEobDb2CursorArea().getS02811Calc5Info().getGlAcctId();
	}

	@Override
	public void setDb2C5GlAcctId(String db2C5GlAcctId) {
		ws.getEobDb2CursorArea().getS02811Calc5Info().setGlAcctId(db2C5GlAcctId);
	}

	@Override
	public String getDb2C5InsTcCd() {
		return ws.getEobDb2CursorArea().getS02811Calc5Info().getInsTcCd();
	}

	@Override
	public void setDb2C5InsTcCd(String db2C5InsTcCd) {
		ws.getEobDb2CursorArea().getS02811Calc5Info().setInsTcCd(db2C5InsTcCd);
	}

	@Override
	public char getDb2C5PayCd() {
		return ws.getEobDb2CursorArea().getS02811Calc5Info().getPayCd();
	}

	@Override
	public void setDb2C5PayCd(char db2C5PayCd) {
		ws.getEobDb2CursorArea().getS02811Calc5Info().setPayCd(db2C5PayCd);
	}

	@Override
	public AfDecimal getDb2C5PdAm() {
		return ws.getEobDb2CursorArea().getS02811Calc5Info().getPdAm();
	}

	@Override
	public void setDb2C5PdAm(AfDecimal db2C5PdAm) {
		ws.getEobDb2CursorArea().getS02811Calc5Info().setPdAm(db2C5PdAm.copy());
	}

	@Override
	public String getDb2C5ProdCd() {
		return ws.getEobDb2CursorArea().getS02811Calc5Info().getProdCd();
	}

	@Override
	public void setDb2C5ProdCd(String db2C5ProdCd) {
		ws.getEobDb2CursorArea().getS02811Calc5Info().setProdCd(db2C5ProdCd);
	}

	@Override
	public String getDb2C5SpeciBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc5Info().getSpeciBenCd();
	}

	@Override
	public void setDb2C5SpeciBenCd(String db2C5SpeciBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc5Info().setSpeciBenCd(db2C5SpeciBenCd);
	}

	@Override
	public String getDb2C5TypBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc5Info().getTypBenCd();
	}

	@Override
	public void setDb2C5TypBenCd(String db2C5TypBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc5Info().setTypBenCd(db2C5TypBenCd);
	}

	@Override
	public AfDecimal getDb2C6AlwChgAm() {
		return ws.getEobDb2CursorArea().getS02811Calc6Info().getAlwChgAm();
	}

	@Override
	public void setDb2C6AlwChgAm(AfDecimal db2C6AlwChgAm) {
		ws.getEobDb2CursorArea().getS02811Calc6Info().setAlwChgAm(db2C6AlwChgAm.copy());
	}

	@Override
	public char getDb2C6CalcExplnCd() {
		return ws.getEobDb2CursorArea().getS02811Calc6Info().getCalcExplnCd();
	}

	@Override
	public void setDb2C6CalcExplnCd(char db2C6CalcExplnCd) {
		ws.getEobDb2CursorArea().getS02811Calc6Info().setCalcExplnCd(db2C6CalcExplnCd);
	}

	@Override
	public char getDb2C6CalcTypCd() {
		return ws.getEobDb2CursorArea().getS02811Calc6Info().getCalcTypCd();
	}

	@Override
	public void setDb2C6CalcTypCd(char db2C6CalcTypCd) {
		ws.getEobDb2CursorArea().getS02811Calc6Info().setCalcTypCd(db2C6CalcTypCd);
	}

	@Override
	public char getDb2C6CorpCd() {
		return ws.getEobDb2CursorArea().getS02811Calc6Info().getCorpCd();
	}

	@Override
	public void setDb2C6CorpCd(char db2C6CorpCd) {
		ws.getEobDb2CursorArea().getS02811Calc6Info().setCorpCd(db2C6CorpCd);
	}

	@Override
	public char getDb2C6CovLobCd() {
		return ws.getEobDb2CursorArea().getS02811Calc6Info().getCovLobCd();
	}

	@Override
	public void setDb2C6CovLobCd(char db2C6CovLobCd) {
		ws.getEobDb2CursorArea().getS02811Calc6Info().setCovLobCd(db2C6CovLobCd);
	}

	@Override
	public String getDb2C6GlAcctId() {
		return ws.getEobDb2CursorArea().getS02811Calc6Info().getGlAcctId();
	}

	@Override
	public void setDb2C6GlAcctId(String db2C6GlAcctId) {
		ws.getEobDb2CursorArea().getS02811Calc6Info().setGlAcctId(db2C6GlAcctId);
	}

	@Override
	public String getDb2C6InsTcCd() {
		return ws.getEobDb2CursorArea().getS02811Calc6Info().getInsTcCd();
	}

	@Override
	public void setDb2C6InsTcCd(String db2C6InsTcCd) {
		ws.getEobDb2CursorArea().getS02811Calc6Info().setInsTcCd(db2C6InsTcCd);
	}

	@Override
	public char getDb2C6PayCd() {
		return ws.getEobDb2CursorArea().getS02811Calc6Info().getPayCd();
	}

	@Override
	public void setDb2C6PayCd(char db2C6PayCd) {
		ws.getEobDb2CursorArea().getS02811Calc6Info().setPayCd(db2C6PayCd);
	}

	@Override
	public AfDecimal getDb2C6PdAm() {
		return ws.getEobDb2CursorArea().getS02811Calc6Info().getPdAm();
	}

	@Override
	public void setDb2C6PdAm(AfDecimal db2C6PdAm) {
		ws.getEobDb2CursorArea().getS02811Calc6Info().setPdAm(db2C6PdAm.copy());
	}

	@Override
	public String getDb2C6ProdCd() {
		return ws.getEobDb2CursorArea().getS02811Calc6Info().getProdCd();
	}

	@Override
	public void setDb2C6ProdCd(String db2C6ProdCd) {
		ws.getEobDb2CursorArea().getS02811Calc6Info().setProdCd(db2C6ProdCd);
	}

	@Override
	public String getDb2C6SpeciBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc6Info().getSpeciBenCd();
	}

	@Override
	public void setDb2C6SpeciBenCd(String db2C6SpeciBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc6Info().setSpeciBenCd(db2C6SpeciBenCd);
	}

	@Override
	public String getDb2C6TypBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc6Info().getTypBenCd();
	}

	@Override
	public void setDb2C6TypBenCd(String db2C6TypBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc6Info().setTypBenCd(db2C6TypBenCd);
	}

	@Override
	public AfDecimal getDb2C7AlwChgAm() {
		return ws.getEobDb2CursorArea().getS02811Calc7Info().getAlwChgAm();
	}

	@Override
	public void setDb2C7AlwChgAm(AfDecimal db2C7AlwChgAm) {
		ws.getEobDb2CursorArea().getS02811Calc7Info().setAlwChgAm(db2C7AlwChgAm.copy());
	}

	@Override
	public char getDb2C7CalcExplnCd() {
		return ws.getEobDb2CursorArea().getS02811Calc7Info().getCalcExplnCd();
	}

	@Override
	public void setDb2C7CalcExplnCd(char db2C7CalcExplnCd) {
		ws.getEobDb2CursorArea().getS02811Calc7Info().setCalcExplnCd(db2C7CalcExplnCd);
	}

	@Override
	public char getDb2C7CalcTypCd() {
		return ws.getEobDb2CursorArea().getS02811Calc7Info().getCalcTypCd();
	}

	@Override
	public void setDb2C7CalcTypCd(char db2C7CalcTypCd) {
		ws.getEobDb2CursorArea().getS02811Calc7Info().setCalcTypCd(db2C7CalcTypCd);
	}

	@Override
	public char getDb2C7CorpCd() {
		return ws.getEobDb2CursorArea().getS02811Calc7Info().getCorpCd();
	}

	@Override
	public void setDb2C7CorpCd(char db2C7CorpCd) {
		ws.getEobDb2CursorArea().getS02811Calc7Info().setCorpCd(db2C7CorpCd);
	}

	@Override
	public char getDb2C7CovLobCd() {
		return ws.getEobDb2CursorArea().getS02811Calc7Info().getCovLobCd();
	}

	@Override
	public void setDb2C7CovLobCd(char db2C7CovLobCd) {
		ws.getEobDb2CursorArea().getS02811Calc7Info().setCovLobCd(db2C7CovLobCd);
	}

	@Override
	public String getDb2C7GlAcctId() {
		return ws.getEobDb2CursorArea().getS02811Calc7Info().getGlAcctId();
	}

	@Override
	public void setDb2C7GlAcctId(String db2C7GlAcctId) {
		ws.getEobDb2CursorArea().getS02811Calc7Info().setGlAcctId(db2C7GlAcctId);
	}

	@Override
	public String getDb2C7InsTcCd() {
		return ws.getEobDb2CursorArea().getS02811Calc7Info().getInsTcCd();
	}

	@Override
	public void setDb2C7InsTcCd(String db2C7InsTcCd) {
		ws.getEobDb2CursorArea().getS02811Calc7Info().setInsTcCd(db2C7InsTcCd);
	}

	@Override
	public char getDb2C7PayCd() {
		return ws.getEobDb2CursorArea().getS02811Calc7Info().getPayCd();
	}

	@Override
	public void setDb2C7PayCd(char db2C7PayCd) {
		ws.getEobDb2CursorArea().getS02811Calc7Info().setPayCd(db2C7PayCd);
	}

	@Override
	public AfDecimal getDb2C7PdAm() {
		return ws.getEobDb2CursorArea().getS02811Calc7Info().getPdAm();
	}

	@Override
	public void setDb2C7PdAm(AfDecimal db2C7PdAm) {
		ws.getEobDb2CursorArea().getS02811Calc7Info().setPdAm(db2C7PdAm.copy());
	}

	@Override
	public String getDb2C7ProdCd() {
		return ws.getEobDb2CursorArea().getS02811Calc7Info().getProdCd();
	}

	@Override
	public void setDb2C7ProdCd(String db2C7ProdCd) {
		ws.getEobDb2CursorArea().getS02811Calc7Info().setProdCd(db2C7ProdCd);
	}

	@Override
	public String getDb2C7SpeciBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc7Info().getSpeciBenCd();
	}

	@Override
	public void setDb2C7SpeciBenCd(String db2C7SpeciBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc7Info().setSpeciBenCd(db2C7SpeciBenCd);
	}

	@Override
	public String getDb2C7TypBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc7Info().getTypBenCd();
	}

	@Override
	public void setDb2C7TypBenCd(String db2C7TypBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc7Info().setTypBenCd(db2C7TypBenCd);
	}

	@Override
	public AfDecimal getDb2C8AlwChgAm() {
		return ws.getEobDb2CursorArea().getS02811Calc8Info().getAlwChgAm();
	}

	@Override
	public void setDb2C8AlwChgAm(AfDecimal db2C8AlwChgAm) {
		ws.getEobDb2CursorArea().getS02811Calc8Info().setAlwChgAm(db2C8AlwChgAm.copy());
	}

	@Override
	public char getDb2C8CalcExplnCd() {
		return ws.getEobDb2CursorArea().getS02811Calc8Info().getCalcExplnCd();
	}

	@Override
	public void setDb2C8CalcExplnCd(char db2C8CalcExplnCd) {
		ws.getEobDb2CursorArea().getS02811Calc8Info().setCalcExplnCd(db2C8CalcExplnCd);
	}

	@Override
	public char getDb2C8CalcTypCd() {
		return ws.getEobDb2CursorArea().getS02811Calc8Info().getCalcTypCd();
	}

	@Override
	public void setDb2C8CalcTypCd(char db2C8CalcTypCd) {
		ws.getEobDb2CursorArea().getS02811Calc8Info().setCalcTypCd(db2C8CalcTypCd);
	}

	@Override
	public char getDb2C8CorpCd() {
		return ws.getEobDb2CursorArea().getS02811Calc8Info().getCorpCd();
	}

	@Override
	public void setDb2C8CorpCd(char db2C8CorpCd) {
		ws.getEobDb2CursorArea().getS02811Calc8Info().setCorpCd(db2C8CorpCd);
	}

	@Override
	public char getDb2C8CovLobCd() {
		return ws.getEobDb2CursorArea().getS02811Calc8Info().getCovLobCd();
	}

	@Override
	public void setDb2C8CovLobCd(char db2C8CovLobCd) {
		ws.getEobDb2CursorArea().getS02811Calc8Info().setCovLobCd(db2C8CovLobCd);
	}

	@Override
	public String getDb2C8GlAcctId() {
		return ws.getEobDb2CursorArea().getS02811Calc8Info().getGlAcctId();
	}

	@Override
	public void setDb2C8GlAcctId(String db2C8GlAcctId) {
		ws.getEobDb2CursorArea().getS02811Calc8Info().setGlAcctId(db2C8GlAcctId);
	}

	@Override
	public String getDb2C8InsTcCd() {
		return ws.getEobDb2CursorArea().getS02811Calc8Info().getInsTcCd();
	}

	@Override
	public void setDb2C8InsTcCd(String db2C8InsTcCd) {
		ws.getEobDb2CursorArea().getS02811Calc8Info().setInsTcCd(db2C8InsTcCd);
	}

	@Override
	public char getDb2C8PayCd() {
		return ws.getEobDb2CursorArea().getS02811Calc8Info().getPayCd();
	}

	@Override
	public void setDb2C8PayCd(char db2C8PayCd) {
		ws.getEobDb2CursorArea().getS02811Calc8Info().setPayCd(db2C8PayCd);
	}

	@Override
	public AfDecimal getDb2C8PdAm() {
		return ws.getEobDb2CursorArea().getS02811Calc8Info().getPdAm();
	}

	@Override
	public void setDb2C8PdAm(AfDecimal db2C8PdAm) {
		ws.getEobDb2CursorArea().getS02811Calc8Info().setPdAm(db2C8PdAm.copy());
	}

	@Override
	public String getDb2C8ProdCd() {
		return ws.getEobDb2CursorArea().getS02811Calc8Info().getProdCd();
	}

	@Override
	public void setDb2C8ProdCd(String db2C8ProdCd) {
		ws.getEobDb2CursorArea().getS02811Calc8Info().setProdCd(db2C8ProdCd);
	}

	@Override
	public String getDb2C8SpeciBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc8Info().getSpeciBenCd();
	}

	@Override
	public void setDb2C8SpeciBenCd(String db2C8SpeciBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc8Info().setSpeciBenCd(db2C8SpeciBenCd);
	}

	@Override
	public String getDb2C8TypBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc8Info().getTypBenCd();
	}

	@Override
	public void setDb2C8TypBenCd(String db2C8TypBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc8Info().setTypBenCd(db2C8TypBenCd);
	}

	@Override
	public AfDecimal getDb2C9AlwChgAm() {
		return ws.getEobDb2CursorArea().getS02811Calc9Info().getAlwChgAm();
	}

	@Override
	public void setDb2C9AlwChgAm(AfDecimal db2C9AlwChgAm) {
		ws.getEobDb2CursorArea().getS02811Calc9Info().setAlwChgAm(db2C9AlwChgAm.copy());
	}

	@Override
	public char getDb2C9CalcExplnCd() {
		return ws.getEobDb2CursorArea().getS02811Calc9Info().getCalcExplnCd();
	}

	@Override
	public void setDb2C9CalcExplnCd(char db2C9CalcExplnCd) {
		ws.getEobDb2CursorArea().getS02811Calc9Info().setCalcExplnCd(db2C9CalcExplnCd);
	}

	@Override
	public char getDb2C9CalcTypCd() {
		return ws.getEobDb2CursorArea().getS02811Calc9Info().getCalcTypCd();
	}

	@Override
	public void setDb2C9CalcTypCd(char db2C9CalcTypCd) {
		ws.getEobDb2CursorArea().getS02811Calc9Info().setCalcTypCd(db2C9CalcTypCd);
	}

	@Override
	public char getDb2C9CorpCd() {
		return ws.getEobDb2CursorArea().getS02811Calc9Info().getCorpCd();
	}

	@Override
	public void setDb2C9CorpCd(char db2C9CorpCd) {
		ws.getEobDb2CursorArea().getS02811Calc9Info().setCorpCd(db2C9CorpCd);
	}

	@Override
	public char getDb2C9CovLobCd() {
		return ws.getEobDb2CursorArea().getS02811Calc9Info().getCovLobCd();
	}

	@Override
	public void setDb2C9CovLobCd(char db2C9CovLobCd) {
		ws.getEobDb2CursorArea().getS02811Calc9Info().setCovLobCd(db2C9CovLobCd);
	}

	@Override
	public String getDb2C9GlAcctId() {
		return ws.getEobDb2CursorArea().getS02811Calc9Info().getGlAcctId();
	}

	@Override
	public void setDb2C9GlAcctId(String db2C9GlAcctId) {
		ws.getEobDb2CursorArea().getS02811Calc9Info().setGlAcctId(db2C9GlAcctId);
	}

	@Override
	public String getDb2C9InsTcCd() {
		return ws.getEobDb2CursorArea().getS02811Calc9Info().getInsTcCd();
	}

	@Override
	public void setDb2C9InsTcCd(String db2C9InsTcCd) {
		ws.getEobDb2CursorArea().getS02811Calc9Info().setInsTcCd(db2C9InsTcCd);
	}

	@Override
	public char getDb2C9PayCd() {
		return ws.getEobDb2CursorArea().getS02811Calc9Info().getPayCd();
	}

	@Override
	public void setDb2C9PayCd(char db2C9PayCd) {
		ws.getEobDb2CursorArea().getS02811Calc9Info().setPayCd(db2C9PayCd);
	}

	@Override
	public AfDecimal getDb2C9PdAm() {
		return ws.getEobDb2CursorArea().getS02811Calc9Info().getPdAm();
	}

	@Override
	public void setDb2C9PdAm(AfDecimal db2C9PdAm) {
		ws.getEobDb2CursorArea().getS02811Calc9Info().setPdAm(db2C9PdAm.copy());
	}

	@Override
	public String getDb2C9ProdCd() {
		return ws.getEobDb2CursorArea().getS02811Calc9Info().getProdCd();
	}

	@Override
	public void setDb2C9ProdCd(String db2C9ProdCd) {
		ws.getEobDb2CursorArea().getS02811Calc9Info().setProdCd(db2C9ProdCd);
	}

	@Override
	public String getDb2C9SpeciBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc9Info().getSpeciBenCd();
	}

	@Override
	public void setDb2C9SpeciBenCd(String db2C9SpeciBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc9Info().setSpeciBenCd(db2C9SpeciBenCd);
	}

	@Override
	public String getDb2C9TypBenCd() {
		return ws.getEobDb2CursorArea().getS02811Calc9Info().getTypBenCd();
	}

	@Override
	public void setDb2C9TypBenCd(String db2C9TypBenCd) {
		ws.getEobDb2CursorArea().getS02811Calc9Info().setTypBenCd(db2C9TypBenCd);
	}

	@Override
	public short getDb2CliId() {
		return ws.getEobDb2CursorArea().getS02801Info().getCliId();
	}

	@Override
	public void setDb2CliId(short db2CliId) {
		ws.getEobDb2CursorArea().getS02801Info().setCliId(db2CliId);
	}

	@Override
	public String getDb2ClmCntlId() {
		return ws.getEobDb2CursorArea().getS02801Info().getClmCntlId();
	}

	@Override
	public void setDb2ClmCntlId(String db2ClmCntlId) {
		ws.getEobDb2CursorArea().getS02801Info().setClmCntlId(db2ClmCntlId);
	}

	@Override
	public char getDb2ClmCntlSfxId() {
		return ws.getEobDb2CursorArea().getS02801Info().getClmCntlSfxId();
	}

	@Override
	public void setDb2ClmCntlSfxId(char db2ClmCntlSfxId) {
		ws.getEobDb2CursorArea().getS02801Info().setClmCntlSfxId(db2ClmCntlSfxId);
	}

	@Override
	public String getDb2ClmInsLnCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getClmInsLnCd();
	}

	@Override
	public void setDb2ClmInsLnCd(String db2ClmInsLnCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setClmInsLnCd(db2ClmInsLnCd);
	}

	@Override
	public short getDb2ClmPmtId() {
		return ws.getEobDb2CursorArea().getS02801Info().getClmPmtId();
	}

	@Override
	public void setDb2ClmPmtId(short db2ClmPmtId) {
		ws.getEobDb2CursorArea().getS02801Info().setClmPmtId(db2ClmPmtId);
	}

	@Override
	public String getDb2ClmSystVerId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getClmSystVerId();
	}

	@Override
	public void setDb2ClmSystVerId(String db2ClmSystVerId) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setClmSystVerId(db2ClmSystVerId);
	}

	@Override
	public String getDb2FnlBusDt() {
		return ws.getEobDb2CursorArea().getS02801Info().getFnlBusDt();
	}

	@Override
	public void setDb2FnlBusDt(String db2FnlBusDt) {
		ws.getEobDb2CursorArea().getS02801Info().setFnlBusDt(db2FnlBusDt);
	}

	@Override
	public String getDb2LiAdjdOvrd10Cd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd10Cd();
	}

	@Override
	public void setDb2LiAdjdOvrd10Cd(String db2LiAdjdOvrd10Cd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAdjdOvrd10Cd(db2LiAdjdOvrd10Cd);
	}

	@Override
	public String getDb2LiAdjdOvrd1Cd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd1Cd();
	}

	@Override
	public void setDb2LiAdjdOvrd1Cd(String db2LiAdjdOvrd1Cd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAdjdOvrd1Cd(db2LiAdjdOvrd1Cd);
	}

	@Override
	public String getDb2LiAdjdOvrd2Cd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd2Cd();
	}

	@Override
	public void setDb2LiAdjdOvrd2Cd(String db2LiAdjdOvrd2Cd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAdjdOvrd2Cd(db2LiAdjdOvrd2Cd);
	}

	@Override
	public String getDb2LiAdjdOvrd3Cd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd3Cd();
	}

	@Override
	public void setDb2LiAdjdOvrd3Cd(String db2LiAdjdOvrd3Cd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAdjdOvrd3Cd(db2LiAdjdOvrd3Cd);
	}

	@Override
	public String getDb2LiAdjdOvrd4Cd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd4Cd();
	}

	@Override
	public void setDb2LiAdjdOvrd4Cd(String db2LiAdjdOvrd4Cd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAdjdOvrd4Cd(db2LiAdjdOvrd4Cd);
	}

	@Override
	public String getDb2LiAdjdOvrd5Cd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd5Cd();
	}

	@Override
	public void setDb2LiAdjdOvrd5Cd(String db2LiAdjdOvrd5Cd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAdjdOvrd5Cd(db2LiAdjdOvrd5Cd);
	}

	@Override
	public String getDb2LiAdjdOvrd6Cd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd6Cd();
	}

	@Override
	public void setDb2LiAdjdOvrd6Cd(String db2LiAdjdOvrd6Cd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAdjdOvrd6Cd(db2LiAdjdOvrd6Cd);
	}

	@Override
	public String getDb2LiAdjdOvrd7Cd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd7Cd();
	}

	@Override
	public void setDb2LiAdjdOvrd7Cd(String db2LiAdjdOvrd7Cd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAdjdOvrd7Cd(db2LiAdjdOvrd7Cd);
	}

	@Override
	public String getDb2LiAdjdOvrd8Cd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd8Cd();
	}

	@Override
	public void setDb2LiAdjdOvrd8Cd(String db2LiAdjdOvrd8Cd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAdjdOvrd8Cd(db2LiAdjdOvrd8Cd);
	}

	@Override
	public String getDb2LiAdjdOvrd9Cd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd9Cd();
	}

	@Override
	public void setDb2LiAdjdOvrd9Cd(String db2LiAdjdOvrd9Cd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAdjdOvrd9Cd(db2LiAdjdOvrd9Cd);
	}

	@Override
	public AfDecimal getDb2LiAlctOiCovAm() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAlctOiCovAm();
	}

	@Override
	public void setDb2LiAlctOiCovAm(AfDecimal db2LiAlctOiCovAm) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAlctOiCovAm(db2LiAlctOiCovAm.copy());
	}

	@Override
	public AfDecimal getDb2LiAlctOiPdAm() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAlctOiPdAm();
	}

	@Override
	public void setDb2LiAlctOiPdAm(AfDecimal db2LiAlctOiPdAm) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAlctOiPdAm(db2LiAlctOiPdAm.copy());
	}

	@Override
	public AfDecimal getDb2LiAlctOiSaveAm() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAlctOiSaveAm();
	}

	@Override
	public void setDb2LiAlctOiSaveAm(AfDecimal db2LiAlctOiSaveAm) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAlctOiSaveAm(db2LiAlctOiSaveAm.copy());
	}

	@Override
	public AfDecimal getDb2LiAlwChgAm() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getAlwChgAm();
	}

	@Override
	public void setDb2LiAlwChgAm(AfDecimal db2LiAlwChgAm) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setAlwChgAm(db2LiAlwChgAm.copy());
	}

	@Override
	public String getDb2LiBenTypCd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getBenTypCd();
	}

	@Override
	public void setDb2LiBenTypCd(String db2LiBenTypCd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setBenTypCd(db2LiBenTypCd);
	}

	@Override
	public AfDecimal getDb2LiChgAm() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getChgAm();
	}

	@Override
	public void setDb2LiChgAm(AfDecimal db2LiChgAm) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setChgAm(db2LiChgAm.copy());
	}

	@Override
	public char getDb2LiExmnActnCd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd();
	}

	@Override
	public void setDb2LiExmnActnCd(char db2LiExmnActnCd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setExmnActnCd(db2LiExmnActnCd);
	}

	@Override
	public char getDb2LiExplnCd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getExplnCd();
	}

	@Override
	public void setDb2LiExplnCd(char db2LiExplnCd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setExplnCd(db2LiExplnCd);
	}

	@Override
	public String getDb2LiFrmServDt() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getFrmServDt();
	}

	@Override
	public void setDb2LiFrmServDt(String db2LiFrmServDt) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setFrmServDt(db2LiFrmServDt);
	}

	@Override
	public AfDecimal getDb2LiPatRespAm() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getPatRespAm();
	}

	@Override
	public void setDb2LiPatRespAm(AfDecimal db2LiPatRespAm) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setPatRespAm(db2LiPatRespAm.copy());
	}

	@Override
	public String getDb2LiPosCd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getPosCd();
	}

	@Override
	public void setDb2LiPosCd(String db2LiPosCd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setPosCd(db2LiPosCd);
	}

	@Override
	public String getDb2LiProcCd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd();
	}

	@Override
	public void setDb2LiProcCd(String db2LiProcCd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setProcCd(db2LiProcCd);
	}

	@Override
	public AfDecimal getDb2LiPwoAm() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getPwoAm();
	}

	@Override
	public void setDb2LiPwoAm(AfDecimal db2LiPwoAm) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setPwoAm(db2LiPwoAm.copy());
	}

	@Override
	public String getDb2LiRmrkCd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getRmrkCd();
	}

	@Override
	public void setDb2LiRmrkCd(String db2LiRmrkCd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setRmrkCd(db2LiRmrkCd);
	}

	@Override
	public char getDb2LiRvwByCd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getRvwByCd();
	}

	@Override
	public void setDb2LiRvwByCd(char db2LiRvwByCd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setRvwByCd(db2LiRvwByCd);
	}

	@Override
	public String getDb2LiThrServDt() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getThrServDt();
	}

	@Override
	public void setDb2LiThrServDt(String db2LiThrServDt) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setThrServDt(db2LiThrServDt);
	}

	@Override
	public String getDb2LiTsCd() {
		return ws.getEobDb2CursorArea().getS02809LineInfo().getTsCd();
	}

	@Override
	public void setDb2LiTsCd(String db2LiTsCd) {
		ws.getEobDb2CursorArea().getS02809LineInfo().setTsCd(db2LiTsCd);
	}

	@Override
	public String getDb2MemIdToAcumId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getMemIdToAcumId();
	}

	@Override
	public void setDb2MemIdToAcumId(String db2MemIdToAcumId) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setMemIdToAcumId(db2MemIdToAcumId);
	}

	@Override
	public String getDb2PaIdToAcumId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPaIdToAcumId();
	}

	@Override
	public void setDb2PaIdToAcumId(String db2PaIdToAcumId) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPaIdToAcumId(db2PaIdToAcumId);
	}

	@Override
	public String getDb2PatFrstNm() {
		return ws.getEobDb2CursorArea().getS02952NameDemoInfo().getPatFrstNm();
	}

	@Override
	public void setDb2PatFrstNm(String db2PatFrstNm) {
		ws.getEobDb2CursorArea().getS02952NameDemoInfo().setPatFrstNm(db2PatFrstNm);
	}

	@Override
	public String getDb2PatLstNm() {
		return ws.getEobDb2CursorArea().getS02952NameDemoInfo().getPatLstNm();
	}

	@Override
	public void setDb2PatLstNm(String db2PatLstNm) {
		ws.getEobDb2CursorArea().getS02952NameDemoInfo().setPatLstNm(db2PatLstNm);
	}

	@Override
	public String getDb2PatMidNm() {
		return ws.getEobDb2CursorArea().getS02952NameDemoInfo().getPatMidNm();
	}

	@Override
	public void setDb2PatMidNm(String db2PatMidNm) {
		ws.getEobDb2CursorArea().getS02952NameDemoInfo().setPatMidNm(db2PatMidNm);
	}

	@Override
	public String getDb2PeProvId() {
		return ws.getEobDb2CursorArea().getS02815ProviderInfo().getPeProvId();
	}

	@Override
	public void setDb2PeProvId(String db2PeProvId) {
		ws.getEobDb2CursorArea().getS02815ProviderInfo().setPeProvId(db2PeProvId);
	}

	@Override
	public char getDb2PeProvLob() {
		return ws.getEobDb2CursorArea().getS02815ProviderInfo().getPeProvLob();
	}

	@Override
	public void setDb2PeProvLob(char db2PeProvLob) {
		ws.getEobDb2CursorArea().getS02815ProviderInfo().setPeProvLob(db2PeProvLob);
	}

	@Override
	public char getDb2PmtAdjTypCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd();
	}

	@Override
	public void setDb2PmtAdjTypCd(char db2PmtAdjTypCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtAdjTypCd(db2PmtAdjTypCd);
	}

	@Override
	public String getDb2PmtAdjdDt() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjdDt();
	}

	@Override
	public void setDb2PmtAdjdDt(String db2PmtAdjdDt) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtAdjdDt(db2PmtAdjdDt);
	}

	@Override
	public char getDb2PmtAdjdProvStatCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjdProvStatCd();
	}

	@Override
	public void setDb2PmtAdjdProvStatCd(char db2PmtAdjdProvStatCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtAdjdProvStatCd(db2PmtAdjdProvStatCd);
	}

	@Override
	public String getDb2PmtAlphPrfxCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAlphPrfxCd();
	}

	@Override
	public void setDb2PmtAlphPrfxCd(String db2PmtAlphPrfxCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtAlphPrfxCd(db2PmtAlphPrfxCd);
	}

	@Override
	public String getDb2PmtAltInsId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAltInsId();
	}

	@Override
	public void setDb2PmtAltInsId(String db2PmtAltInsId) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtAltInsId(db2PmtAltInsId);
	}

	@Override
	public String getDb2PmtAsgCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd();
	}

	@Override
	public void setDb2PmtAsgCd(String db2PmtAsgCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtAsgCd(db2PmtAsgCd);
	}

	@Override
	public AfDecimal getDb2PmtBalBillAm() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtBalBillAm();
	}

	@Override
	public void setDb2PmtBalBillAm(AfDecimal db2PmtBalBillAm) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtBalBillAm(db2PmtBalBillAm.copy());
	}

	@Override
	public String getDb2PmtBenPlnId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtBenPlnId();
	}

	@Override
	public void setDb2PmtBenPlnId(String db2PmtBenPlnId) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtBenPlnId(db2PmtBenPlnId);
	}

	@Override
	public String getDb2PmtBillFrmDt() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtBillFrmDt();
	}

	@Override
	public void setDb2PmtBillFrmDt(String db2PmtBillFrmDt) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtBillFrmDt(db2PmtBillFrmDt);
	}

	@Override
	public String getDb2PmtBillThrDt() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtBillThrDt();
	}

	@Override
	public void setDb2PmtBillThrDt(String db2PmtBillThrDt) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtBillThrDt(db2PmtBillThrDt);
	}

	@Override
	public String getDb2PmtCkId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtCkId();
	}

	@Override
	public void setDb2PmtCkId(String db2PmtCkId) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtCkId(db2PmtCkId);
	}

	@Override
	public AfDecimal getDb2PmtClmPdAm() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmPdAm();
	}

	@Override
	public void setDb2PmtClmPdAm(AfDecimal db2PmtClmPdAm) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtClmPdAm(db2PmtClmPdAm.copy());
	}

	@Override
	public String getDb2PmtClmSystVerId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmSystVerId();
	}

	@Override
	public void setDb2PmtClmSystVerId(String db2PmtClmSystVerId) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtClmSystVerId(db2PmtClmSystVerId);
	}

	@Override
	public String getDb2PmtCorpRcvDt() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtCorpRcvDt();
	}

	@Override
	public void setDb2PmtCorpRcvDt(String db2PmtCorpRcvDt) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtCorpRcvDt(db2PmtCorpRcvDt);
	}

	@Override
	public String getDb2PmtDrg() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtDrg();
	}

	@Override
	public void setDb2PmtDrg(String db2PmtDrg) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtDrg(db2PmtDrg);
	}

	@Override
	public String getDb2PmtEnrClCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtEnrClCd();
	}

	@Override
	public void setDb2PmtEnrClCd(String db2PmtEnrClCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtEnrClCd(db2PmtEnrClCd);
	}

	@Override
	public String getDb2PmtFinCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtFinCd();
	}

	@Override
	public void setDb2PmtFinCd(String db2PmtFinCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtFinCd(db2PmtFinCd);
	}

	@Override
	public String getDb2PmtGrpId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtGrpId();
	}

	@Override
	public void setDb2PmtGrpId(String db2PmtGrpId) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtGrpId(db2PmtGrpId);
	}

	@Override
	public char getDb2PmtHistLoadCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd();
	}

	@Override
	public void setDb2PmtHistLoadCd(char db2PmtHistLoadCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtHistLoadCd(db2PmtHistLoadCd);
	}

	@Override
	public String getDb2PmtInsId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtInsId();
	}

	@Override
	public void setDb2PmtInsId(String db2PmtInsId) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtInsId(db2PmtInsId);
	}

	@Override
	public String getDb2PmtIrcCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtIrcCd();
	}

	@Override
	public void setDb2PmtIrcCd(String db2PmtIrcCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtIrcCd(db2PmtIrcCd);
	}

	@Override
	public char getDb2PmtItsClmTypCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd();
	}

	@Override
	public void setDb2PmtItsClmTypCd(char db2PmtItsClmTypCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtItsClmTypCd(db2PmtItsClmTypCd);
	}

	@Override
	public String getDb2PmtKcapsTeamNm() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsTeamNm();
	}

	@Override
	public void setDb2PmtKcapsTeamNm(String db2PmtKcapsTeamNm) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtKcapsTeamNm(db2PmtKcapsTeamNm);
	}

	@Override
	public String getDb2PmtKcapsUseId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsUseId();
	}

	@Override
	public void setDb2PmtKcapsUseId(String db2PmtKcapsUseId) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtKcapsUseId(db2PmtKcapsUseId);
	}

	@Override
	public char getDb2PmtLobCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtLobCd();
	}

	@Override
	public void setDb2PmtLobCd(char db2PmtLobCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtLobCd(db2PmtLobCd);
	}

	@Override
	public char getDb2PmtMdgpPlnCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtMdgpPlnCd();
	}

	@Override
	public void setDb2PmtMdgpPlnCd(char db2PmtMdgpPlnCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtMdgpPlnCd(db2PmtMdgpPlnCd);
	}

	@Override
	public String getDb2PmtMemId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtMemId();
	}

	@Override
	public void setDb2PmtMemId(String db2PmtMemId) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtMemId(db2PmtMemId);
	}

	@Override
	public char getDb2PmtNpiCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtNpiCd();
	}

	@Override
	public void setDb2PmtNpiCd(char db2PmtNpiCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtNpiCd(db2PmtNpiCd);
	}

	@Override
	public String getDb2PmtNtwrkCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtNtwrkCd();
	}

	@Override
	public void setDb2PmtNtwrkCd(String db2PmtNtwrkCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtNtwrkCd(db2PmtNtwrkCd);
	}

	@Override
	public char getDb2PmtOiCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtOiCd();
	}

	@Override
	public void setDb2PmtOiCd(char db2PmtOiCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtOiCd(db2PmtOiCd);
	}

	@Override
	public String getDb2PmtPatActId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPatActId();
	}

	@Override
	public void setDb2PmtPatActId(String db2PmtPatActId) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtPatActId(db2PmtPatActId);
	}

	@Override
	public String getDb2PmtPgmAreaCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPgmAreaCd();
	}

	@Override
	public void setDb2PmtPgmAreaCd(String db2PmtPgmAreaCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtPgmAreaCd(db2PmtPgmAreaCd);
	}

	@Override
	public char getDb2PmtPmtBkProdCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPmtBkProdCd();
	}

	@Override
	public void setDb2PmtPmtBkProdCd(char db2PmtPmtBkProdCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtPmtBkProdCd(db2PmtPmtBkProdCd);
	}

	@Override
	public String getDb2PmtPrimCnArngCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPrimCnArngCd();
	}

	@Override
	public void setDb2PmtPrimCnArngCd(String db2PmtPrimCnArngCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtPrimCnArngCd(db2PmtPrimCnArngCd);
	}

	@Override
	public String getDb2PmtPrmptPayDay() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPrmptPayDay();
	}

	@Override
	public void setDb2PmtPrmptPayDay(String db2PmtPrmptPayDay) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtPrmptPayDay(db2PmtPrmptPayDay);
	}

	@Override
	public char getDb2PmtPrmptPayOvrd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPrmptPayOvrd();
	}

	@Override
	public void setDb2PmtPrmptPayOvrd(char db2PmtPrmptPayOvrd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtPrmptPayOvrd(db2PmtPrmptPayOvrd);
	}

	@Override
	public String getDb2PmtRateCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtRateCd();
	}

	@Override
	public void setDb2PmtRateCd(String db2PmtRateCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtRateCd(db2PmtRateCd);
	}

	@Override
	public String getDb2PmtTypGrpCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd();
	}

	@Override
	public void setDb2PmtTypGrpCd(String db2PmtTypGrpCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtTypGrpCd(db2PmtTypGrpCd);
	}

	@Override
	public String getDb2ProdInd() {
		return ws.getEobDb2CursorArea().getS02801Info().getProdInd();
	}

	@Override
	public void setDb2ProdInd(String db2ProdInd) {
		ws.getEobDb2CursorArea().getS02801Info().setProdInd(db2ProdInd);
	}

	@Override
	public String getDb2ProviderName() {
		return ws.getEobDb2CursorArea().getS02952NameDemoInfo().getProviderName();
	}

	@Override
	public void setDb2ProviderName(String db2ProviderName) {
		ws.getEobDb2CursorArea().getS02952NameDemoInfo().setProviderName(db2ProviderName);
	}

	@Override
	public char getDb2SpecDrugCpnIn() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getSpecDrugCpnIn();
	}

	@Override
	public void setDb2SpecDrugCpnIn(char db2SpecDrugCpnIn) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setSpecDrugCpnIn(db2SpecDrugCpnIn);
	}

	@Override
	public String getDb2StatCatgCd() {
		return ws.getEobDb2CursorArea().getS02801Info().getStatCatgCd();
	}

	@Override
	public void setDb2StatCatgCd(String db2StatCatgCd) {
		ws.getEobDb2CursorArea().getS02801Info().setStatCatgCd(db2StatCatgCd);
	}

	@Override
	public char getDb2VbrIn() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getVbrIn();
	}

	@Override
	public void setDb2VbrIn(char db2VbrIn) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setVbrIn(db2VbrIn);
	}

	@Override
	public char getDb2VoidCd() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getVoidCd();
	}

	@Override
	public void setDb2VoidCd(char db2VoidCd) {
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setVoidCd(db2VoidCd);
	}

	@Override
	public String getPatientName() {
		return ws.getWsNf0533WorkArea().getPatientName();
	}

	@Override
	public void setPatientName(String patientName) {
		ws.getWsNf0533WorkArea().setPatientName(patientName);
	}

	@Override
	public String getPerformingName() {
		return ws.getWsNf0533WorkArea().getPerformingName();
	}

	@Override
	public void setPerformingName(String performingName) {
		ws.getWsNf0533WorkArea().setPerformingName(performingName);
	}

	@Override
	public AfDecimal getWsDb2NcovAmt() {
		return ws.getWsNf0533WorkArea().getWsDb2NcovAmt();
	}

	@Override
	public void setWsDb2NcovAmt(AfDecimal wsDb2NcovAmt) {
		ws.getWsNf0533WorkArea().setWsDb2NcovAmt(wsDb2NcovAmt.copy());
	}

	@Override
	public AfDecimal getWsDb2PatResp() {
		return ws.getWsNf0533WorkArea().getWsDb2PatResp();
	}

	@Override
	public void setWsDb2PatResp(AfDecimal wsDb2PatResp) {
		ws.getWsNf0533WorkArea().setWsDb2PatResp(wsDb2PatResp.copy());
	}

	@Override
	public AfDecimal getWsDb2PwoAmt() {
		return ws.getWsNf0533WorkArea().getWsDb2PwoAmt();
	}

	@Override
	public void setWsDb2PwoAmt(AfDecimal wsDb2PwoAmt) {
		ws.getWsNf0533WorkArea().setWsDb2PwoAmt(wsDb2PwoAmt.copy());
	}

	@Override
	public String getWsIndusCd() {
		return ws.getWsNf0533WorkArea().getWsIndusCd();
	}

	@Override
	public void setWsIndusCd(String wsIndusCd) {
		ws.getWsNf0533WorkArea().setWsIndusCd(wsIndusCd);
	}
}
