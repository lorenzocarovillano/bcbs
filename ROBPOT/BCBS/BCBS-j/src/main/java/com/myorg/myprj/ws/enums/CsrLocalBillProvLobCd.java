/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: CSR-LOCAL-BILL-PROV-LOB-CD<br>
 * Variable: CSR-LOCAL-BILL-PROV-LOB-CD from copybook NF07AREA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CsrLocalBillProvLobCd {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char CROSS = '1';
	public static final char SHIELD = '3';

	//==== METHODS ====
	public void setLocalBillProvLobCd(char localBillProvLobCd) {
		this.value = localBillProvLobCd;
	}

	public char getLocalBillProvLobCd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LOCAL_BILL_PROV_LOB_CD = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
