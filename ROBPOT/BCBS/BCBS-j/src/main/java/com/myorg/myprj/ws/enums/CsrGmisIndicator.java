/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: CSR-GMIS-INDICATOR<br>
 * Variable: CSR-GMIS-INDICATOR from copybook NF07AREA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CsrGmisIndicator {

	//==== PROPERTIES ====
	private String value = "";
	public static final String GMIS_CLAIM = "GMIS";
	public static final String NOT_A_GMIS_CLAIM = "NOTGMIS";

	//==== METHODS ====
	public void setGmisIndicator(String gmisIndicator) {
		this.value = Functions.subString(gmisIndicator, Len.GMIS_INDICATOR);
	}

	public String getGmisIndicator() {
		return this.value;
	}

	public boolean isGmisClaim() {
		return value.equals(GMIS_CLAIM);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int GMIS_INDICATOR = 7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
