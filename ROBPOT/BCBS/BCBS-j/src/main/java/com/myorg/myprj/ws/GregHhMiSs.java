/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: GREG-HH-MI-SS<br>
 * Variable: GREG-HH-MI-SS from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class GregHhMiSs {

	//==== PROPERTIES ====
	//Original name: GREG-HH
	private String hh = "00";
	//Original name: GREG-MI
	private String mi = "00";
	//Original name: GREG-SS
	private String ss = "00";

	//==== METHODS ====
	public void setHhFormatted(String hh) {
		this.hh = Trunc.toUnsignedNumeric(hh, Len.HH);
	}

	public short getHh() {
		return NumericDisplay.asShort(this.hh);
	}

	public void setMiFormatted(String mi) {
		this.mi = Trunc.toUnsignedNumeric(mi, Len.MI);
	}

	public short getMi() {
		return NumericDisplay.asShort(this.mi);
	}

	public void setSsFormatted(String ss) {
		this.ss = Trunc.toUnsignedNumeric(ss, Len.SS);
	}

	public short getSs() {
		return NumericDisplay.asShort(this.ss);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HH = 2;
		public static final int MI = 2;
		public static final int SS = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
