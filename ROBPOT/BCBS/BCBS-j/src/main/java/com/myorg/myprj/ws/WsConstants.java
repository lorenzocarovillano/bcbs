/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-CONSTANTS<br>
 * Variable: WS-CONSTANTS from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsConstants {

	//==== PROPERTIES ====
	//Original name: WS-THIS-PROGRAM
	private String wsThisProgram = "NF0533ML";
	//Original name: GL-SOTE-NUM
	private String glSoteNum = "29330100";
	//Original name: NF0531SR-PGM
	private String nf0531srPgm = "NF0531SR";
	//Original name: HOLD-GROUP-NUMBER
	private String holdGroupNumber = "";
	public static final String SPECIAL_DAILY_GROUP_1_MIN = "0781501";
	public static final String SPECIAL_DAILY_GROUP_1_MAX = "0781599";
	public static final String SPECIAL_DAILY_GROUP_2_MIN = "08999";
	public static final String SPECIAL_DAILY_GROUP_2_MAX = "0899999";
	public static final String SPECIAL_DAILY_GROUP_3_MIN = "150";
	public static final String SPECIAL_DAILY_GROUP_3_MAX = "1509999";
	public static final String SPECIAL_DAILY_GROUP_4_MIN = "151";
	public static final String SPECIAL_DAILY_GROUP_4_MAX = "1519999";
	public static final String SPECIAL_DAILY_GROUP_5 = "96019";

	//==== METHODS ====
	public String getWsThisProgram() {
		return this.wsThisProgram;
	}

	public String getGlSoteNum() {
		return this.glSoteNum;
	}

	public String getNf0531srPgm() {
		return this.nf0531srPgm;
	}

	public void setHoldGroupNumber(String holdGroupNumber) {
		this.holdGroupNumber = Functions.subString(holdGroupNumber, Len.HOLD_GROUP_NUMBER);
	}

	public String getHoldGroupNumber() {
		return this.holdGroupNumber;
	}

	public boolean isSpecialDailyGroup() {
		String fld = holdGroupNumber;
		return Conditions.isBetween(fld, SPECIAL_DAILY_GROUP_1_MIN, SPECIAL_DAILY_GROUP_1_MAX)
				|| Conditions.isBetween(fld, SPECIAL_DAILY_GROUP_2_MIN, SPECIAL_DAILY_GROUP_2_MAX)
				|| Conditions.isBetween(fld, SPECIAL_DAILY_GROUP_3_MIN, SPECIAL_DAILY_GROUP_3_MAX)
				|| Conditions.isBetween(fld, SPECIAL_DAILY_GROUP_4_MIN, SPECIAL_DAILY_GROUP_4_MAX) || fld.equals(SPECIAL_DAILY_GROUP_5);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HOLD_GROUP_NUMBER = 7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
