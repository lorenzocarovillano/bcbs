/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLS02682SA<br>
 * Variable: DCLS02682SA from copybook S02682SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02682sa {

	//==== PROPERTIES ====
	//Original name: LST-ORG-NM
	private String lstOrgNm = DefaultValues.stringVal(Len.LST_ORG_NM);
	//Original name: FRST-NM
	private String frstNm = DefaultValues.stringVal(Len.FRST_NM);
	//Original name: MID-NM
	private String midNm = DefaultValues.stringVal(Len.MID_NM);
	//Original name: SFX-NM
	private String sfxNm = DefaultValues.stringVal(Len.SFX_NM);
	//Original name: ID-CD
	private String idCd = DefaultValues.stringVal(Len.ID_CD);

	//==== METHODS ====
	public void setLstOrgNm(String lstOrgNm) {
		this.lstOrgNm = Functions.subString(lstOrgNm, Len.LST_ORG_NM);
	}

	public String getLstOrgNm() {
		return this.lstOrgNm;
	}

	public void setFrstNm(String frstNm) {
		this.frstNm = Functions.subString(frstNm, Len.FRST_NM);
	}

	public String getFrstNm() {
		return this.frstNm;
	}

	public void setMidNm(String midNm) {
		this.midNm = Functions.subString(midNm, Len.MID_NM);
	}

	public String getMidNm() {
		return this.midNm;
	}

	public void setSfxNm(String sfxNm) {
		this.sfxNm = Functions.subString(sfxNm, Len.SFX_NM);
	}

	public String getSfxNm() {
		return this.sfxNm;
	}

	public void setIdCd(String idCd) {
		this.idCd = Functions.subString(idCd, Len.ID_CD);
	}

	public String getIdCd() {
		return this.idCd;
	}

	public String getIdCdFormatted() {
		return Functions.padBlanks(getIdCd(), Len.ID_CD);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LST_ORG_NM = 60;
		public static final int FRST_NM = 35;
		public static final int MID_NM = 25;
		public static final int SFX_NM = 10;
		public static final int ID_CD = 80;
		public static final int CLM_CNTL_ID = 12;
		public static final int LOOP_ID = 6;
		public static final int SEG_ID = 3;
		public static final int ENTY_ID_CD = 3;
		public static final int ID_QLF_CD = 2;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
