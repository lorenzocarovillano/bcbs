/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: ERROR-MESSAGE<br>
 * Variable: ERROR-MESSAGE from program NF0533ML<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ErrorMessage extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int TEXT_MAXOCCURS = 8;
	//Original name: ERROR-LEN
	private short len = ((short) 624);
	//Original name: ERROR-TEXT
	private String[] text = new String[TEXT_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public ErrorMessage() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.ERROR_MESSAGE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setErrorMessageBytes(buf);
	}

	public void init() {
		for (int textIdx = 1; textIdx <= TEXT_MAXOCCURS; textIdx++) {
			setText(textIdx, DefaultValues.stringVal(Len.TEXT));
		}
	}

	public String getErrorMessageFormatted() {
		byte[] buffer = new byte[Len.ERROR_MESSAGE];
		return MarshalByteExt.bufferToStr(getErrorMessageBytes(buffer, 1));
	}

	public void setErrorMessageBytes(byte[] buffer) {
		setErrorMessageBytes(buffer, 1);
	}

	public byte[] getErrorMessageBytes() {
		byte[] buffer = new byte[Len.ERROR_MESSAGE];
		return getErrorMessageBytes(buffer, 1);
	}

	public void setErrorMessageBytes(byte[] buffer, int offset) {
		int position = offset;
		len = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		for (int idx = 1; idx <= TEXT_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setText(idx, MarshalByte.readString(buffer, position, Len.TEXT));
				position += Len.TEXT;
			} else {
				setText(idx, "");
				position += Len.TEXT;
			}
		}
	}

	public byte[] getErrorMessageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, len);
		position += Types.SHORT_SIZE;
		for (int idx = 1; idx <= TEXT_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getText(idx), Len.TEXT);
			position += Len.TEXT;
		}
		return buffer;
	}

	public void setLen(short len) {
		this.len = len;
	}

	public short getLen() {
		return this.len;
	}

	public void setText(int textIdx, String text) {
		this.text[textIdx - 1] = Functions.subString(text, Len.TEXT);
	}

	public String getText(int textIdx) {
		return this.text[textIdx - 1];
	}

	public String getTextFormatted(int textIdx) {
		return Functions.padBlanks(getText(textIdx), Len.TEXT);
	}

	@Override
	public byte[] serialize() {
		return getErrorMessageBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LEN = 2;
		public static final int TEXT = 78;
		public static final int ERROR_MESSAGE = LEN + ErrorMessage.TEXT_MAXOCCURS * TEXT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
