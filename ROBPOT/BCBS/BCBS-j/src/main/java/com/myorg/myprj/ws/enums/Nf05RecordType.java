/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: NF05-RECORD-TYPE<br>
 * Variable: NF05-RECORD-TYPE from copybook NF05EOB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Nf05RecordType {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char LINE = 'L';
	public static final char TOTAL = 'T';

	//==== METHODS ====
	public void setNf05RecordType(char nf05RecordType) {
		this.value = nf05RecordType;
	}

	public void setNf05RecordTypeFormatted(String nf05RecordType) {
		setNf05RecordType(Functions.charAt(nf05RecordType, Types.CHAR_SIZE));
	}

	public char getNf05RecordType() {
		return this.value;
	}

	public boolean isTotal() {
		return value == TOTAL;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NF05_RECORD_TYPE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
