/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-PRV-NAME<br>
 * Variable: WS-PRV-NAME from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsPrvName {

	//==== PROPERTIES ====
	//Original name: WS-PRV-LAST
	private String last = DefaultValues.stringVal(Len.LAST);
	//Original name: WS-PRV-FIRST
	private String first = DefaultValues.stringVal(Len.FIRST);
	//Original name: WS-PRV-INIT
	private char init = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public String getWsPrvNameFormatted() {
		return MarshalByteExt.bufferToStr(getWsPrvNameBytes());
	}

	public byte[] getWsPrvNameBytes() {
		byte[] buffer = new byte[Len.WS_PRV_NAME];
		return getWsPrvNameBytes(buffer, 1);
	}

	public byte[] getWsPrvNameBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, last, Len.LAST);
		position += Len.LAST;
		MarshalByte.writeString(buffer, position, first, Len.FIRST);
		position += Len.FIRST;
		MarshalByte.writeChar(buffer, position, init);
		return buffer;
	}

	public void setLast(String last) {
		this.last = Functions.subString(last, Len.LAST);
	}

	public String getLast() {
		return this.last;
	}

	public void setFirst(String first) {
		this.first = Functions.subString(first, Len.FIRST);
	}

	public String getFirst() {
		return this.first;
	}

	public void setInit(char init) {
		this.init = init;
	}

	public char getInit() {
		return this.init;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LAST = 15;
		public static final int FIRST = 9;
		public static final int INIT = 1;
		public static final int WS_PRV_NAME = LAST + FIRST + INIT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
