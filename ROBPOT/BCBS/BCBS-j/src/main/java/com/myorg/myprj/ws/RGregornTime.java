/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: R-GREGORN-TIME<br>
 * Variable: R-GREGORN-TIME from program NF0533ML<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class RGregornTime {

	//==== PROPERTIES ====
	//Original name: GREGORN-HH
	private String gregornHh = DefaultValues.stringVal(Len.GREGORN_HH);
	//Original name: GREGORN-MI
	private String gregornMi = DefaultValues.stringVal(Len.GREGORN_MI);
	//Original name: GREGORN-SS
	private String gregornSs = DefaultValues.stringVal(Len.GREGORN_SS);

	//==== METHODS ====
	public void setGregornTime(int gregornTime) {
		int position = 1;
		byte[] buffer = getrGregornTimeBytes();
		MarshalByte.writeInt(buffer, position, gregornTime, Len.Int.GREGORN_TIME, SignType.NO_SIGN);
		setrGregornTimeBytes(buffer);
	}

	/**Original name: GREGORN-TIME<br>*/
	public int getGregornTime() {
		int position = 1;
		return MarshalByte.readInt(getrGregornTimeBytes(), position, Len.Int.GREGORN_TIME, SignType.NO_SIGN);
	}

	public void setrGregornTimeBytes(byte[] buffer) {
		setrGregornTimeBytes(buffer, 1);
	}

	/**Original name: R-GREGORN-TIME<br>*/
	public byte[] getrGregornTimeBytes() {
		byte[] buffer = new byte[Len.R_GREGORN_TIME];
		return getrGregornTimeBytes(buffer, 1);
	}

	public void setrGregornTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		gregornHh = MarshalByte.readFixedString(buffer, position, Len.GREGORN_HH);
		position += Len.GREGORN_HH;
		gregornMi = MarshalByte.readFixedString(buffer, position, Len.GREGORN_MI);
		position += Len.GREGORN_MI;
		gregornSs = MarshalByte.readFixedString(buffer, position, Len.GREGORN_SS);
	}

	public byte[] getrGregornTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, gregornHh, Len.GREGORN_HH);
		position += Len.GREGORN_HH;
		MarshalByte.writeString(buffer, position, gregornMi, Len.GREGORN_MI);
		position += Len.GREGORN_MI;
		MarshalByte.writeString(buffer, position, gregornSs, Len.GREGORN_SS);
		return buffer;
	}

	public String getGregornHhFormatted() {
		return this.gregornHh;
	}

	public String getGregornMiFormatted() {
		return this.gregornMi;
	}

	public String getGregornSsFormatted() {
		return this.gregornSs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int GREGORN_HH = 2;
		public static final int GREGORN_MI = 2;
		public static final int GREGORN_SS = 2;
		public static final int R_GREGORN_TIME = GREGORN_HH + GREGORN_MI + GREGORN_SS;
		public static final int GREGORN_TIME = 6;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int GREGORN_TIME = 6;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
