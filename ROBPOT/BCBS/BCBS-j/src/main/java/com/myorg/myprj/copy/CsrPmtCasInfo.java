/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.occurs.CsrCascoReductions;
import com.myorg.myprj.ws.occurs.CsrCasoaReductions;
import com.myorg.myprj.ws.occurs.CsrCaspiReductions;

/**Original name: CSR-PMT-CAS-INFO<br>
 * Variable: CSR-PMT-CAS-INFO from copybook NF07AREA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CsrPmtCasInfo {

	//==== PROPERTIES ====
	public static final int CASCO_REDUCTIONS_MAXOCCURS = 18;
	public static final int CASOA_REDUCTIONS_MAXOCCURS = 18;
	public static final int CASPI_REDUCTIONS_MAXOCCURS = 6;
	public static final int CASPR_REDUCTIONS_MAXOCCURS = 24;
	/**Original name: CSR-CASCO-CLM-ADJ-GRP-CD<br>
	 * <pre>      ------------------------------
	 *       --   CONTRACTUAL OBILIGATION / PROVIDER WRITE OFF
	 *       ------------------------------</pre>*/
	private String cascoClmAdjGrpCd = "";
	//Original name: CSR-CASCO-REDUCTIONS
	private CsrCascoReductions[] cascoReductions = new CsrCascoReductions[CASCO_REDUCTIONS_MAXOCCURS];
	/**Original name: CSR-CASPR-CLM-ADJ-GRP-CD<br>
	 * <pre>      ------------------------------
	 *       --   PATIENT RESPONSIBILITY
	 *       ------------------------------</pre>*/
	private String casprClmAdjGrpCd = "";
	//Original name: CSR-CASPR-REDUCTIONS
	private CsrCascoReductions[] casprReductions = new CsrCascoReductions[CASPR_REDUCTIONS_MAXOCCURS];
	/**Original name: CSR-CASOA-CLM-ADJ-GRP-CD<br>
	 * <pre>      ------------------------------
	 *       --   OTHER ADJUSTMENTS
	 *       ------------------------------</pre>*/
	private String casoaClmAdjGrpCd = "";
	//Original name: CSR-CASOA-REDUCTIONS
	private CsrCasoaReductions[] casoaReductions = new CsrCasoaReductions[CASOA_REDUCTIONS_MAXOCCURS];
	/**Original name: CSR-CASPI-CLM-ADJ-GRP-CD<br>
	 * <pre>      ------------------------------
	 *       --   GROUP CODE "PI"
	 *       ------------------------------</pre>*/
	private String caspiClmAdjGrpCd = "";
	//Original name: CSR-CASPI-REDUCTIONS
	private CsrCaspiReductions[] caspiReductions = new CsrCaspiReductions[CASPI_REDUCTIONS_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public CsrPmtCasInfo() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int cascoReductionsIdx = 1; cascoReductionsIdx <= CASCO_REDUCTIONS_MAXOCCURS; cascoReductionsIdx++) {
			cascoReductions[cascoReductionsIdx - 1] = new CsrCascoReductions();
		}
		for (int casprReductionsIdx = 1; casprReductionsIdx <= CASPR_REDUCTIONS_MAXOCCURS; casprReductionsIdx++) {
			casprReductions[casprReductionsIdx - 1] = new CsrCascoReductions();
		}
		for (int casoaReductionsIdx = 1; casoaReductionsIdx <= CASOA_REDUCTIONS_MAXOCCURS; casoaReductionsIdx++) {
			casoaReductions[casoaReductionsIdx - 1] = new CsrCasoaReductions();
		}
		for (int caspiReductionsIdx = 1; caspiReductionsIdx <= CASPI_REDUCTIONS_MAXOCCURS; caspiReductionsIdx++) {
			caspiReductions[caspiReductionsIdx - 1] = new CsrCaspiReductions();
		}
	}

	public byte[] getPmtCasInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cascoClmAdjGrpCd, Len.CASCO_CLM_ADJ_GRP_CD);
		position += Len.CASCO_CLM_ADJ_GRP_CD;
		for (int idx = 1; idx <= CASCO_REDUCTIONS_MAXOCCURS; idx++) {
			cascoReductions[idx - 1].getCascoReductionsBytes(buffer, position);
			position += CsrCascoReductions.Len.CASCO_REDUCTIONS;
		}
		MarshalByte.writeString(buffer, position, casprClmAdjGrpCd, Len.CASPR_CLM_ADJ_GRP_CD);
		position += Len.CASPR_CLM_ADJ_GRP_CD;
		for (int idx = 1; idx <= CASPR_REDUCTIONS_MAXOCCURS; idx++) {
			casprReductions[idx - 1].getCascoReductionsBytes(buffer, position);
			position += CsrCascoReductions.Len.CASCO_REDUCTIONS;
		}
		MarshalByte.writeString(buffer, position, casoaClmAdjGrpCd, Len.CASOA_CLM_ADJ_GRP_CD);
		position += Len.CASOA_CLM_ADJ_GRP_CD;
		for (int idx = 1; idx <= CASOA_REDUCTIONS_MAXOCCURS; idx++) {
			casoaReductions[idx - 1].getCasoaReductionsBytes(buffer, position);
			position += CsrCasoaReductions.Len.CASOA_REDUCTIONS;
		}
		MarshalByte.writeString(buffer, position, caspiClmAdjGrpCd, Len.CASPI_CLM_ADJ_GRP_CD);
		position += Len.CASPI_CLM_ADJ_GRP_CD;
		for (int idx = 1; idx <= CASPI_REDUCTIONS_MAXOCCURS; idx++) {
			caspiReductions[idx - 1].getCaspiReductionsBytes(buffer, position);
			position += CsrCaspiReductions.Len.CASPI_REDUCTIONS;
		}
		return buffer;
	}

	public void setCascoClmAdjGrpCd(String cascoClmAdjGrpCd) {
		this.cascoClmAdjGrpCd = Functions.subString(cascoClmAdjGrpCd, Len.CASCO_CLM_ADJ_GRP_CD);
	}

	public String getCascoClmAdjGrpCd() {
		return this.cascoClmAdjGrpCd;
	}

	public void setCasprClmAdjGrpCd(String casprClmAdjGrpCd) {
		this.casprClmAdjGrpCd = Functions.subString(casprClmAdjGrpCd, Len.CASPR_CLM_ADJ_GRP_CD);
	}

	public String getCasprClmAdjGrpCd() {
		return this.casprClmAdjGrpCd;
	}

	public void setCasoaClmAdjGrpCd(String casoaClmAdjGrpCd) {
		this.casoaClmAdjGrpCd = Functions.subString(casoaClmAdjGrpCd, Len.CASOA_CLM_ADJ_GRP_CD);
	}

	public String getCasoaClmAdjGrpCd() {
		return this.casoaClmAdjGrpCd;
	}

	public void setCaspiClmAdjGrpCd(String caspiClmAdjGrpCd) {
		this.caspiClmAdjGrpCd = Functions.subString(caspiClmAdjGrpCd, Len.CASPI_CLM_ADJ_GRP_CD);
	}

	public String getCaspiClmAdjGrpCd() {
		return this.caspiClmAdjGrpCd;
	}

	public CsrCascoReductions getCascoReductions(int idx) {
		return cascoReductions[idx - 1];
	}

	public CsrCasoaReductions getCasoaReductions(int idx) {
		return casoaReductions[idx - 1];
	}

	public CsrCaspiReductions getCaspiReductions(int idx) {
		return caspiReductions[idx - 1];
	}

	public CsrCascoReductions getCasprReductions(int idx) {
		return casprReductions[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CASCO_CLM_ADJ_GRP_CD = 2;
		public static final int CASPR_CLM_ADJ_GRP_CD = 2;
		public static final int CASOA_CLM_ADJ_GRP_CD = 2;
		public static final int CASPI_CLM_ADJ_GRP_CD = 2;
		public static final int PMT_CAS_INFO = CASCO_CLM_ADJ_GRP_CD
				+ CsrPmtCasInfo.CASCO_REDUCTIONS_MAXOCCURS * CsrCascoReductions.Len.CASCO_REDUCTIONS + CASPR_CLM_ADJ_GRP_CD
				+ CsrPmtCasInfo.CASPR_REDUCTIONS_MAXOCCURS * CsrCascoReductions.Len.CASCO_REDUCTIONS + CASOA_CLM_ADJ_GRP_CD
				+ CsrPmtCasInfo.CASOA_REDUCTIONS_MAXOCCURS * CsrCasoaReductions.Len.CASOA_REDUCTIONS + CASPI_CLM_ADJ_GRP_CD
				+ CsrPmtCasInfo.CASPI_REDUCTIONS_MAXOCCURS * CsrCaspiReductions.Len.CASPI_REDUCTIONS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
