/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: PROV-XREF-AREA<br>
 * Variable: PROV-XREF-AREA from copybook NU02PROV<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ProvXrefArea {

	//==== PROPERTIES ====
	//Original name: PROV-XREF-NUM-1
	private String num1 = DefaultValues.stringVal(Len.NUM1);
	//Original name: PROV-XREF-1-LENGTH
	private String xref1Length = DefaultValues.stringVal(Len.XREF1_LENGTH);
	//Original name: PROV-XREF-NUM-2
	private String num2 = DefaultValues.stringVal(Len.NUM2);
	//Original name: PROV-XREF-2-LENGTH
	private String xref2Length = DefaultValues.stringVal(Len.XREF2_LENGTH);

	//==== METHODS ====
	public void setProvXrefAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		num1 = MarshalByte.readString(buffer, position, Len.NUM1);
		position += Len.NUM1;
		xref1Length = MarshalByte.readFixedString(buffer, position, Len.XREF1_LENGTH);
		position += Len.XREF1_LENGTH;
		num2 = MarshalByte.readString(buffer, position, Len.NUM2);
		position += Len.NUM2;
		xref2Length = MarshalByte.readFixedString(buffer, position, Len.XREF2_LENGTH);
	}

	public byte[] getProvXrefAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, num1, Len.NUM1);
		position += Len.NUM1;
		MarshalByte.writeString(buffer, position, xref1Length, Len.XREF1_LENGTH);
		position += Len.XREF1_LENGTH;
		MarshalByte.writeString(buffer, position, num2, Len.NUM2);
		position += Len.NUM2;
		MarshalByte.writeString(buffer, position, xref2Length, Len.XREF2_LENGTH);
		return buffer;
	}

	public void initProvXrefAreaSpaces() {
		num1 = "";
		xref1Length = "";
		num2 = "";
		xref2Length = "";
	}

	public void setNum1(String num1) {
		this.num1 = Functions.subString(num1, Len.NUM1);
	}

	public String getNum1() {
		return this.num1;
	}

	public void setNum2(String num2) {
		this.num2 = Functions.subString(num2, Len.NUM2);
	}

	public String getNum2() {
		return this.num2;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NUM1 = 30;
		public static final int XREF1_LENGTH = 2;
		public static final int NUM2 = 30;
		public static final int XREF2_LENGTH = 2;
		public static final int PROV_XREF_AREA = NUM1 + XREF1_LENGTH + NUM2 + XREF2_LENGTH;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
