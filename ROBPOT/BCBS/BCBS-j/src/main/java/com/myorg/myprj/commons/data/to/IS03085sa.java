/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [S03085SA]
 * 
 */
public interface IS03085sa extends BaseSqlTo {

	/**
	 * Host Variable RSTRT-IN
	 * 
	 */
	char getRstrtIn();

	void setRstrtIn(char rstrtIn);

	/**
	 * Host Variable JBSTP-RUN-CT
	 * 
	 */
	short getJbstpRunCt();

	void setJbstpRunCt(short jbstpRunCt);

	/**
	 * Host Variable JOB-NM
	 * 
	 */
	String getJobNm();

	void setJobNm(String jobNm);

	/**
	 * Host Variable JBSTP-NM
	 * 
	 */
	String getJbstpNm();

	void setJbstpNm(String jbstpNm);

	/**
	 * Host Variable JBSTP-SEQ-ID
	 * 
	 */
	short getJbstpSeqId();

	void setJbstpSeqId(short jbstpSeqId);

	/**
	 * Host Variable JBSTP-STRT-TS
	 * 
	 */
	String getJbstpStrtTs();

	void setJbstpStrtTs(String jbstpStrtTs);

	/**
	 * Host Variable JBSTP-END-TS
	 * 
	 */
	String getJbstpEndTs();

	void setJbstpEndTs(String jbstpEndTs);

	/**
	 * Host Variable INFO-CHG-ID
	 * 
	 */
	String getInfoChgId();

	void setInfoChgId(String infoChgId);

	/**
	 * Host Variable INFO-CHG-TS
	 * 
	 */
	String getInfoChgTs();

	void setInfoChgTs(String infoChgTs);
};
