/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: CSR-CLAIM-LOB-CD<br>
 * Variable: CSR-CLAIM-LOB-CD from copybook NF07AREA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CsrClaimLobCd {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char CROSS_INPAT = '1';
	public static final char CROSS_OUTPAT = '2';
	public static final char SHIELD = '3';
	public static final char ADMISSION_NOTICE = '4';
	public static final char DENTAL = '5';
	public static final char ORTHO = '6';
	public static final char PLAN65_CROSS_INPAT = '7';
	public static final char PLAN65_CROSS_OUTPAT = '8';
	public static final char PLAN65_SHIELD = '8';
	public static final char PLAN65_PLUS = '8';
	public static final char DRUGS = 'D';
	public static final char REFERRAL = 'R';
	private static final char[] INPATIENT = new char[] { '1', '7' };
	private static final char[] OUTPATIENT = new char[] { '2', '8' };

	//==== METHODS ====
	public void setClaimLobCd(char claimLobCd) {
		this.value = claimLobCd;
	}

	public char getClaimLobCd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLAIM_LOB_CD = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
