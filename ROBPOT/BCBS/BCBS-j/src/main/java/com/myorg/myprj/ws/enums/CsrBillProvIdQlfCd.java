/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: CSR-BILL-PROV-ID-QLF-CD<br>
 * Variable: CSR-BILL-PROV-ID-QLF-CD from copybook NF07AREA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CsrBillProvIdQlfCd {

	//==== PROPERTIES ====
	private String value = "";
	public static final String CROSS = "1A";
	public static final String SHIELD = "1B";

	//==== METHODS ====
	public void setBillProvIdQlfCd(String billProvIdQlfCd) {
		this.value = Functions.subString(billProvIdQlfCd, Len.BILL_PROV_ID_QLF_CD);
	}

	public String getBillProvIdQlfCd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BILL_PROV_ID_QLF_CD = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
