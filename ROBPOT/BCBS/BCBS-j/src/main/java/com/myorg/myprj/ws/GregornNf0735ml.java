/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: GREGORN<br>
 * Variable: GREGORN from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class GregornNf0735ml {

	//==== PROPERTIES ====
	//Original name: GREGORN-CC
	private String cc = "00";
	//Original name: GREGORN-YY
	private String yy = "00";
	//Original name: GREGORN-MM
	private String mm = "00";
	//Original name: GREGORN-DD
	private String dd = "00";
	//Original name: GREGORN-HH
	private String hh = "00";
	//Original name: GREGORN-MI
	private String mi = "00";
	//Original name: GREGORN-SS
	private String ss = "00";

	//==== METHODS ====
	public void setGregornFormatted(String data) {
		byte[] buffer = new byte[Len.GREGORN];
		MarshalByte.writeString(buffer, 1, data, Len.GREGORN);
		setGregornBytes(buffer, 1);
	}

	public String getSaveDateTimeFormatted() {
		return MarshalByteExt.bufferToStr(getSaveDateTimeBytes());
	}

	public void setSaveDateTimeBytes(byte[] buffer) {
		setGregornBytes(buffer, 1);
	}

	public byte[] getSaveDateTimeBytes() {
		byte[] buffer = new byte[Len.GREGORN];
		return getSaveDateTimeBytes(buffer, 1);
	}

	public void setGregornBytes(byte[] buffer, int offset) {
		int position = offset;
		cc = MarshalByte.readFixedString(buffer, position, Len.CC);
		position += Len.CC;
		yy = MarshalByte.readFixedString(buffer, position, Len.YY);
		position += Len.YY;
		mm = MarshalByte.readFixedString(buffer, position, Len.MM);
		position += Len.MM;
		dd = MarshalByte.readFixedString(buffer, position, Len.DD);
		position += Len.DD;
		hh = MarshalByte.readFixedString(buffer, position, Len.HH);
		position += Len.HH;
		mi = MarshalByte.readFixedString(buffer, position, Len.MI);
		position += Len.MI;
		ss = MarshalByte.readFixedString(buffer, position, Len.SS);
	}

	public byte[] getSaveDateTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cc, Len.CC);
		position += Len.CC;
		MarshalByte.writeString(buffer, position, yy, Len.YY);
		position += Len.YY;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position += Len.MM;
		MarshalByte.writeString(buffer, position, dd, Len.DD);
		position += Len.DD;
		MarshalByte.writeString(buffer, position, hh, Len.HH);
		position += Len.HH;
		MarshalByte.writeString(buffer, position, mi, Len.MI);
		position += Len.MI;
		MarshalByte.writeString(buffer, position, ss, Len.SS);
		return buffer;
	}

	public String getHhFormatted() {
		return this.hh;
	}

	public String getMiFormatted() {
		return this.mi;
	}

	public String getSsFormatted() {
		return this.ss;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CC = 2;
		public static final int YY = 2;
		public static final int MM = 2;
		public static final int DD = 2;
		public static final int HH = 2;
		public static final int MI = 2;
		public static final int SS = 2;
		public static final int GREGORN = CC + YY + MM + DD + HH + MI + SS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
