/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-SELECT-FIELDS<br>
 * Variable: WS-SELECT-FIELDS from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSelectFields {

	//==== PROPERTIES ====
	//Original name: WS-CLM-CNTL-ID
	private String clmCntlId = "";
	//Original name: WS-CLM-CNTL-SFX-ID
	private char clmCntlSfxId = Types.SPACE_CHAR;
	//Original name: WS-CLM-PMT-ID
	private short clmPmtId = ((short) 0);
	//Original name: WS-PROV-SBMT-LN-NO-ID
	private short provSbmtLnNoId = ((short) 0);

	//==== METHODS ====
	public void setClmCntlId(String clmCntlId) {
		this.clmCntlId = Functions.subString(clmCntlId, Len.CLM_CNTL_ID);
	}

	public String getClmCntlId() {
		return this.clmCntlId;
	}

	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.clmCntlSfxId = clmCntlSfxId;
	}

	public char getClmCntlSfxId() {
		return this.clmCntlSfxId;
	}

	public void setClmPmtId(short clmPmtId) {
		this.clmPmtId = clmPmtId;
	}

	public short getClmPmtId() {
		return this.clmPmtId;
	}

	public void setProvSbmtLnNoId(short provSbmtLnNoId) {
		this.provSbmtLnNoId = provSbmtLnNoId;
	}

	public short getProvSbmtLnNoId() {
		return this.provSbmtLnNoId;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_CNTL_ID = 12;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
