/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: EXP-BILL-PROV-LOB<br>
 * Variable: EXP-BILL-PROV-LOB from copybook NF06EXP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class ExpBillProvLob {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char CROSS = '1';
	public static final char SHIELD = '3';
	public static final char MEDA_INPAT = '4';

	//==== METHODS ====
	public void setExpBillProvLob(char expBillProvLob) {
		this.value = expBillProvLob;
	}

	public char getExpBillProvLob() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EXP_BILL_PROV_LOB = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
