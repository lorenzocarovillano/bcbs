/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: EXP-WRAP-INDC<br>
 * Variable: EXP-WRAP-INDC from copybook NF06EXP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class ExpWrapIndc {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char PAID = 'P';
	public static final char UNWRAP = 'U';
	public static final char WRAP = 'W';

	//==== METHODS ====
	public void setExpWrapIndc(char expWrapIndc) {
		this.value = expWrapIndc;
	}

	public void setExpWrapIndcFormatted(String expWrapIndc) {
		setExpWrapIndc(Functions.charAt(expWrapIndc, Types.CHAR_SIZE));
	}

	public char getExpWrapIndc() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EXP_WRAP_INDC = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
