/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.myorg.myprj.ws.Db2ErrMsgData;

/**Original name: DB2-STAT-ERR-MSG<br>
 * Variable: DB2-STAT-ERR-MSG from copybook NUDBERDD<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Db2StatErrMsg {

	//==== PROPERTIES ====
	//Original name: DB2-ERR-PROG
	private String errProg = "NF0790ML";
	//Original name: DB2-ERR-PARA
	private String errPara = "";
	//Original name: DB2-ERR-LAST-CALL
	private String errLastCall = "";
	//Original name: DB2-ERR-TABLE
	private String errTable = "";
	//Original name: DB2-ERR-KEY
	private String errKey = "";
	//Original name: DB2-ERR-MSG-DATA
	private Db2ErrMsgData errMsgData = new Db2ErrMsgData();
	//Original name: DB2-MSG-LG
	private short msgLg = ((short) 79);

	//==== METHODS ====
	public void setErrProg(String errProg) {
		this.errProg = Functions.subString(errProg, Len.ERR_PROG);
	}

	public String getErrProg() {
		return this.errProg;
	}

	public String getErrProgFormatted() {
		return Functions.padBlanks(getErrProg(), Len.ERR_PROG);
	}

	public void setErrPara(String errPara) {
		this.errPara = Functions.subString(errPara, Len.ERR_PARA);
	}

	public String getErrPara() {
		return this.errPara;
	}

	public String getErrParaFormatted() {
		return Functions.padBlanks(getErrPara(), Len.ERR_PARA);
	}

	public void setErrLastCall(String errLastCall) {
		this.errLastCall = Functions.subString(errLastCall, Len.ERR_LAST_CALL);
	}

	public String getErrLastCall() {
		return this.errLastCall;
	}

	public String getErrLastCallFormatted() {
		return Functions.padBlanks(getErrLastCall(), Len.ERR_LAST_CALL);
	}

	public void setErrTable(String errTable) {
		this.errTable = Functions.subString(errTable, Len.ERR_TABLE);
	}

	public String getErrTable() {
		return this.errTable;
	}

	public String getErrTableFormatted() {
		return Functions.padBlanks(getErrTable(), Len.ERR_TABLE);
	}

	public void setErrKey(String errKey) {
		this.errKey = Functions.subString(errKey, Len.ERR_KEY);
	}

	public String getErrKey() {
		return this.errKey;
	}

	public String getErrKeyFormatted() {
		return Functions.padBlanks(getErrKey(), Len.ERR_KEY);
	}

	public void setMsgLg(short msgLg) {
		this.msgLg = msgLg;
	}

	public void setMsgLgFromBuffer(byte[] buffer) {
		msgLg = MarshalByte.readBinaryShort(buffer, 1);
	}

	public short getMsgLg() {
		return this.msgLg;
	}

	public String getMsgLgFormatted() {
		return PicFormatter.display(new PicParams("S9(4)").setUsage(PicUsage.BINARY)).format(getMsgLg()).toString();
	}

	public Db2ErrMsgData getErrMsgData() {
		return errMsgData;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERR_PROG = 8;
		public static final int ERR_KEY = 30;
		public static final int ERR_TABLE = 27;
		public static final int ERR_LAST_CALL = 20;
		public static final int ERR_PARA = 30;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
