/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [S02813SA]
 * 
 */
public interface IS02813sa extends BaseSqlTo {

	/**
	 * Host Variable WS-GL-OFST-VOID-CD
	 * 
	 */
	String getWsGlOfstVoidCd();

	void setWsGlOfstVoidCd(String wsGlOfstVoidCd);

	/**
	 * Host Variable WS-GL-SOTE-VOID-CD
	 * 
	 */
	short getWsGlSoteVoidCd();

	void setWsGlSoteVoidCd(short wsGlSoteVoidCd);

	/**
	 * Host Variable DB2-CLM-CNTL-ID
	 * 
	 */
	String getDb2ClmCntlId();

	void setDb2ClmCntlId(String db2ClmCntlId);

	/**
	 * Host Variable DB2-CLM-CNTL-SFX-ID
	 * 
	 */
	char getDb2ClmCntlSfxId();

	void setDb2ClmCntlSfxId(char db2ClmCntlSfxId);

	/**
	 * Host Variable VOID-CLM-PMT-ID
	 * 
	 */
	short getVoidClmPmtId();

	void setVoidClmPmtId(short voidClmPmtId);

	/**
	 * Host Variable CK-ID
	 * 
	 */
	String getCkId();

	void setCkId(String ckId);

	/**
	 * Host Variable CK-DT
	 * 
	 */
	String getCkDt();

	void setCkDt(String ckDt);

	/**
	 * Host Variable PRMPT-PAY-INT-AM
	 * 
	 */
	AfDecimal getPrmptPayIntAm();

	void setPrmptPayIntAm(AfDecimal prmptPayIntAm);

	/**
	 * Host Variable ACR-PRMT-PA-INT-AM
	 * 
	 */
	AfDecimal getAcrPrmtPaIntAm();

	void setAcrPrmtPaIntAm(AfDecimal acrPrmtPaIntAm);

	/**
	 * Host Variable WS-GL-OFST-ORIG-CD
	 * 
	 */
	String getWsGlOfstOrigCd();

	void setWsGlOfstOrigCd(String wsGlOfstOrigCd);

	/**
	 * Host Variable WS-GL-SOTE-ORIG-CD
	 * 
	 */
	short getWsGlSoteOrigCd();

	void setWsGlSoteOrigCd(short wsGlSoteOrigCd);

	/**
	 * Host Variable CLM-CNTL-ID
	 * 
	 */
	String getClmCntlId();

	void setClmCntlId(String clmCntlId);

	/**
	 * Host Variable CLM-CNTL-SFX-ID
	 * 
	 */
	char getClmCntlSfxId();

	void setClmCntlSfxId(char clmCntlSfxId);

	/**
	 * Host Variable CLM-PMT-ID
	 * 
	 */
	short getClmPmtId();

	void setClmPmtId(short clmPmtId);
};
