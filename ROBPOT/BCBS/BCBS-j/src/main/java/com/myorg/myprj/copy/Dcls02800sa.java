/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: DCLS02800SA<br>
 * Variable: DCLS02800SA from copybook S02800SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02800sa {

	//==== PROPERTIES ====
	//Original name: CLM-ADJ-GRP-CD
	private String clmAdjGrpCd = DefaultValues.stringVal(Len.CLM_ADJ_GRP_CD);
	//Original name: CLM-ADJ-RSN-CD
	private String clmAdjRsnCd = DefaultValues.stringVal(Len.CLM_ADJ_RSN_CD);
	//Original name: MNTRY-AM
	private AfDecimal mntryAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: QNTY-CT
	private AfDecimal qntyCt = new AfDecimal(DefaultValues.DEC_VAL, 7, 2);

	//==== METHODS ====
	public void setClmAdjGrpCd(String clmAdjGrpCd) {
		this.clmAdjGrpCd = Functions.subString(clmAdjGrpCd, Len.CLM_ADJ_GRP_CD);
	}

	public String getClmAdjGrpCd() {
		return this.clmAdjGrpCd;
	}

	public String getClmAdjGrpCdFormatted() {
		return Functions.padBlanks(getClmAdjGrpCd(), Len.CLM_ADJ_GRP_CD);
	}

	public void setClmAdjRsnCd(String clmAdjRsnCd) {
		this.clmAdjRsnCd = Functions.subString(clmAdjRsnCd, Len.CLM_ADJ_RSN_CD);
	}

	public String getClmAdjRsnCd() {
		return this.clmAdjRsnCd;
	}

	public String getClmAdjRsnCdFormatted() {
		return Functions.padBlanks(getClmAdjRsnCd(), Len.CLM_ADJ_RSN_CD);
	}

	public void setMntryAm(AfDecimal mntryAm) {
		this.mntryAm.assign(mntryAm);
	}

	public AfDecimal getMntryAm() {
		return this.mntryAm.copy();
	}

	public String getMntryAmFormatted() {
		return PicFormatter.display(new PicParams("S9(9)V9(2)").setUsage(PicUsage.PACKED)).format(getMntryAm()).toString();
	}

	public String getMntryAmAsString() {
		return getMntryAmFormatted();
	}

	public void setQntyCt(AfDecimal qntyCt) {
		this.qntyCt.assign(qntyCt);
	}

	public AfDecimal getQntyCt() {
		return this.qntyCt.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_CNTL_ID = 12;
		public static final int INFO_CHG_TS = 26;
		public static final int CLM_ADJ_GRP_CD = 2;
		public static final int CLM_ADJ_RSN_CD = 5;
		public static final int INFO_CHG_ID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
