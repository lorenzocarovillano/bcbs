/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import com.myorg.myprj.commons.data.to.ISr01451;

/**
 * Data Access Object(DAO) for table [SR01451]
 * 
 */
public class Sr01451Dao extends BaseSqlDao<ISr01451> {

	private Cursor srsStopPayG;
	private final IRowMapper<ISr01451> fetchSrsStopPayGRm = buildNamedRowMapper(ISr01451.class, "ovId", "ovLobCd", "ovCnArngEfDt", "vCnArngTrmDt");

	public Sr01451Dao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<ISr01451> getToClass() {
		return ISr01451.class;
	}

	public DbAccessStatus openSrsStopPayG() {
		srsStopPayG = buildQuery("openSrsStopPayG").open();
		return dbStatus;
	}

	public DbAccessStatus closeSrsStopPayG() {
		return closeCursor(srsStopPayG);
	}

	public ISr01451 fetchSrsStopPayG(ISr01451 iSr01451) {
		return fetch(srsStopPayG, iSr01451, fetchSrsStopPayGRm);
	}
}
