/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SBR-ADJUSTMENT-TYPE<br>
 * Variable: SBR-ADJUSTMENT-TYPE from copybook NF05SUBR<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SbrAdjustmentType {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NOT_ADJUSTMENT = ' ';
	public static final char ADJUSTMENT = 'A';
	public static final char ADJ_REFUNDS = 'B';
	public static final char ADJ_CR_OTHER = 'B';
	public static final char ADJ_AUTO_DEDUCT = 'J';
	public static final char ADJ_REFUND_REQUEST = 'R';
	public static final char ADJ_STAT_ADJUST = 'S';

	//==== METHODS ====
	public void setAdjustmentType(char adjustmentType) {
		this.value = adjustmentType;
	}

	public char getAdjustmentType() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ADJUSTMENT_TYPE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
