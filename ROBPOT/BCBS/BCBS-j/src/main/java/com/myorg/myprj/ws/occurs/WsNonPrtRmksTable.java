/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: WS-NON-PRT-RMKS-TABLE<br>
 * Variables: WS-NON-PRT-RMKS-TABLE from program NF0533ML<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WsNonPrtRmksTable implements ICopyable<WsNonPrtRmksTable> {

	//==== PROPERTIES ====
	//Original name: WS-NON-PRT-RMKS
	private String wsNonPrtRmks = DefaultValues.stringVal(Len.WS_NON_PRT_RMKS);

	//==== CONSTRUCTORS ====
	public WsNonPrtRmksTable() {
	}

	public WsNonPrtRmksTable(WsNonPrtRmksTable nonPrtRmksTable) {
		this();
		this.wsNonPrtRmks = nonPrtRmksTable.wsNonPrtRmks;
	}

	//==== METHODS ====
	public void setWsNonPrtRmks(String wsNonPrtRmks) {
		this.wsNonPrtRmks = Functions.subString(wsNonPrtRmks, Len.WS_NON_PRT_RMKS);
	}

	public String getWsNonPrtRmks() {
		return this.wsNonPrtRmks;
	}

	@Override
	public WsNonPrtRmksTable copy() {
		return new WsNonPrtRmksTable(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_NON_PRT_RMKS = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
