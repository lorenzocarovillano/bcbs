/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: NEXT-BUS-DATE-FLAG<br>
 * Variable: NEXT-BUS-DATE-FLAG from copybook NN02DATE<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class NextBusDateFlag {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.NEXT_BUS_DATE_FLAG);
	public static final String FIRST_OF_MONTH = "FOM";
	public static final String END_OF_MONTH = "EOM";

	//==== METHODS ====
	public void setNextBusDateFlag(String nextBusDateFlag) {
		this.value = Functions.subString(nextBusDateFlag, Len.NEXT_BUS_DATE_FLAG);
	}

	public String getNextBusDateFlag() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NEXT_BUS_DATE_FLAG = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
