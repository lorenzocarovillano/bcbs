/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.enums.SbrAdjustmentType;

/**Original name: SBR-VOID-REPAY-DATA<br>
 * Variables: SBR-VOID-REPAY-DATA from copybook NF05SUBR<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class SbrVoidRepayData {

	//==== PROPERTIES ====
	//Original name: SBR-VOID-CD
	private char voidCd = DefaultValues.CHAR_VAL;
	//Original name: SBR-ADJUSTMENT-TYPE
	private SbrAdjustmentType adjustmentType = new SbrAdjustmentType();
	//Original name: SBR-TOTAL-AMOUNT-PAID
	private AfDecimal totalAmountPaid = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: FILLER-SBR-VOID-REPAY-DATA
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setSbrVoidRepayDataBytes(byte[] buffer, int offset) {
		int position = offset;
		voidCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		adjustmentType.setAdjustmentType(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		totalAmountPaid.assign(MarshalByte.readDecimal(buffer, position, Len.Int.TOTAL_AMOUNT_PAID, Len.Fract.TOTAL_AMOUNT_PAID));
		position += Len.TOTAL_AMOUNT_PAID;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getSbrVoidRepayDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, voidCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, adjustmentType.getAdjustmentType());
		position += Types.CHAR_SIZE;
		MarshalByte.writeDecimal(buffer, position, totalAmountPaid.copy());
		position += Len.TOTAL_AMOUNT_PAID;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void initSbrVoidRepayDataSpaces() {
		voidCd = Types.SPACE_CHAR;
		adjustmentType.setAdjustmentType(Types.SPACE_CHAR);
		totalAmountPaid.setNaN();
		flr1 = "";
	}

	public void setVoidCd(char voidCd) {
		this.voidCd = voidCd;
	}

	public void setVoidCdFormatted(String voidCd) {
		setVoidCd(Functions.charAt(voidCd, Types.CHAR_SIZE));
	}

	public char getVoidCd() {
		return this.voidCd;
	}

	public void setTotalAmountPaid(AfDecimal totalAmountPaid) {
		this.totalAmountPaid.assign(totalAmountPaid);
	}

	public AfDecimal getTotalAmountPaid() {
		return this.totalAmountPaid.copy();
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public SbrAdjustmentType getAdjustmentType() {
		return adjustmentType;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VOID_CD = 1;
		public static final int TOTAL_AMOUNT_PAID = 11;
		public static final int FLR1 = 20;
		public static final int SBR_VOID_REPAY_DATA = VOID_CD + SbrAdjustmentType.Len.ADJUSTMENT_TYPE + TOTAL_AMOUNT_PAID + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int TOTAL_AMOUNT_PAID = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int TOTAL_AMOUNT_PAID = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
