/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS03085sa;

/**
 * Data Access Object(DAO) for table [S03085SA]
 * 
 */
public class S03085saDao extends BaseSqlDao<IS03085sa> {

	public S03085saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS03085sa> getToClass() {
		return IS03085sa.class;
	}

	public IS03085sa selectRec(String jobName, String stepName, short jobSeqPacked, IS03085sa iS03085sa) {
		return buildQuery("selectRec").bind("jobName", jobName).bind("stepName", stepName).bind("jobSeqPacked", jobSeqPacked).singleResult(iS03085sa);
	}

	public DbAccessStatus updateRec(short bstpRunCt, String obNm, String bstpNm, short bstpSeqId) {
		return buildQuery("updateRec").bind("bstpRunCt", bstpRunCt).bind("obNm", obNm).bind("bstpNm", bstpNm).bind("bstpSeqId", bstpSeqId)
				.executeUpdate();
	}

	public DbAccessStatus updateRec1(String obNm, String bstpNm, short bstpSeqId) {
		return buildQuery("updateRec1").bind("obNm", obNm).bind("bstpNm", bstpNm).bind("bstpSeqId", bstpSeqId).executeUpdate();
	}

	public IS03085sa selectRec1(String name, String step, short seqPacked, IS03085sa iS03085sa) {
		return buildQuery("selectRec1").bind("name", name).bind("step", step).bind("seqPacked", seqPacked).singleResult(iS03085sa);
	}
}
