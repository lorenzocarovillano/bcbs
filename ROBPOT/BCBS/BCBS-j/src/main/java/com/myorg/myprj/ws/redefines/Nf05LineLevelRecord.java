/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: NF05-LINE-LEVEL-RECORD<br>
 * Variable: NF05-LINE-LEVEL-RECORD from program NF0533ML<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Nf05LineLevelRecord extends BytesAllocatingClass {

	//==== PROPERTIES ====
	private static final char[] NF05_EXAMINER_ACTION_VALUES = new char[] { 'V', 'C', 'D', ' ', 'S', 'M', 'X' };
	public static final char NF05_EXAMINER_VOID = 'V';
	public static final char NF05_EXAMINER_CANCEL = 'C';
	public static final char NF05_EXAMINER_DENIED = 'D';
	public static final char NF05_EXAMINER_PAID = ' ';
	public static final char NF05_EXAMINER_SUSPEND = 'S';
	private static final String[] NF05_TS_CROSS_INPATIENT = new String[] { "1C", "2C", "3C", "7C", "8C" };
	private static final String[] NF05_TS_CROSS_OUTPATIENT = new String[] { "4C", "5C", "6C", "9C" };
	public static final String NF05_TS_SHIELD_1_MIN = " 0";
	public static final String NF05_TS_SHIELD_1_MAX = " 9";
	public static final String NF05_TS_SHIELD_2 = " A";
	public static final String NF05_TS_SHIELD_3 = " C";
	public static final String NF05_TS_SHIELD_4 = " D";
	public static final String NF05_TS_SHIELD_5 = " F";
	public static final String NF05_TS_SHIELD_6 = " G";
	public static final String NF05_TS_SHIELD_7 = " H";
	public static final String NF05_TS_SHIELD_8 = " L";
	public static final String NF05_TS_SHIELD_9 = " M";
	public static final String NF05_TS_SHIELD_10 = " N";
	public static final String NF05_TS_SHIELD_11 = " T";
	public static final String NF05_TS_SHIELD_12 = " W";
	public static final String NF05_TS_SHIELD_13 = " X";
	public static final String NF05_TS_SHIELD_14 = " Y";
	public static final String NF05_TS_BLOOD = " 0";
	public static final String NF05_TS_MEDICAL_CARE = " 1";
	public static final String NF05_TS_SURGERY = " 2";
	public static final String NF05_TS_CONSULTATION = " 3";
	public static final String NF05_TS_XRAY = " 4";
	public static final String NF05_TS_DIAGNOSTIC_LAB = " 5";
	public static final String NF05_TS_RADIATION_THERAPY = " 6";
	public static final String NF05_TS_DENTAL = " 7";
	public static final String NF05_TS_ASSISTANT_SURGERY = " 8";
	public static final String NF05_TS_OTHER_SERVICES = " 9";
	public static final String NF05_TS_AMBULANCE = " 9";
	public static final String NF05_TS_PROSTHETICS = " 9";
	public static final String NF05_TS_SUPPLIES = " 9";
	public static final String NF05_TS_DME_PURCHASES_USED = " A";
	public static final String NF05_TS_CONDUCTIVE_ANESTHESIA = " C";
	public static final String NF05_TS_DRUGS = " D";
	public static final String NF05_TS_AMBULATORY_SURGERY_CN = " F";
	public static final String NF05_TS_FACILITY_CHARGE = " F";
	public static final String NF05_TS_ANESTHESIA = " G";
	public static final String NF05_TS_XRAY_PROFESSIONL_ONLY = " H";
	public static final String NF05_TS_LAB_PROFESSIONL_ONLY = " L";
	public static final String NF05_TS_MATERNITY = " M";
	public static final String NF05_TS_KIDNEY_TRANSPLANT_SRV = " N";
	public static final String NF05_TS_XRAY_TECHNICAL_ONLY = " T";
	public static final String NF05_TS_DME_RENTAL = " W";
	public static final String NF05_TS_DME_PURCHASED_NEW = " X";
	public static final String NF05_TS_THERAPEUTIC_RAD_PROF = " Y";
	public static final String NF05_TS_CROSS_INPAT_MEDICAL = "1C";
	public static final String NF05_TS_CROSS_INPAT_SURGERY = "2C";
	public static final String NF05_TS_CROSS_INPAT_MATERNITY = "3C";
	public static final String NF05_TS_CROSS_INPAT_PL65_MED = "7C";
	public static final String NF05_TS_CROSS_INPAT_PL65_SURG = "8C";
	public static final String NF05_TS_CROSS_OUTPAT_MEDICAL = "4C";
	public static final String NF05_TS_CROSS_OUTPAT_SURGERY = "5C";
	public static final String NF05_TS_CROSS_OUTPAT_ACCIDENT = "6C";
	public static final String NF05_TS_CROSS_OUTPATIENT_PL65 = "9C";
	private static final String[] NF05_PS_EOB_INPATIENT = new String[] { "23", "81", "84", "86", "92", "95", "96" };
	private static final String[] NF05_PS_EOB_OUTPATIENT = new String[] { "85", "88", "89", "90", "91", "93", "94" };
	public static final String NF05_URGENT_CARE_HCFA = "20";
	public static final String NF05_MEDICAL_EMERGENCY = "23";
	public static final String NF05_HOSP_INPATIENT = "81";
	public static final String NF05_HOME = "82";
	public static final String NF05_DOCTORS_OFFICE = "83";
	public static final String NF05_EXTENDED_CARE_FACILITY = "84";
	public static final String NF05_HOSP_OUTPATIENT = "85";
	public static final String NF05_INDEPENDENT_HOSP = "86";
	public static final String NF05_OTHER_PLACE_SERVICE = "87";
	public static final String NF05_NURSING_HOME = "88";
	public static final String NF05_INDEP_KIDNEY_CENTER = "89";
	public static final String NF05_PSYCH_DAY_CARE = "90";
	public static final String NF05_INDEPENDENT_LAB = "91";
	public static final String NF05_ABUSE_CNTR_RESIDENT = "92";
	public static final String NF05_ABUSE_CNTR_OUTPATIENT = "93";
	public static final String NF05_FREE_STANDING_HOSPICE = "94";
	public static final String NF05_TB_HOSPITAL = "95";
	public static final String NF05_PSYCH_HOSPITAL = "96";
	private static final String[] NF05_CANCER_IV = new String[] { "HE", "JB" };
	public static final char NF05_CALC1_LOB_NONE = ' ';
	public static final char NF05_CALC1_LOB_VALUES_1_MIN = '1';
	public static final char NF05_CALC1_LOB_VALUES_1_MAX = '6';
	public static final char NF05_CALC1_LOB_VALUES_2 = '9';
	public static final char NF05_CALC1_LOB_VALUES_3 = 'A';
	public static final char NF05_CALC1_LOB_VALUES_4 = 'B';
	public static final char NF05_CALC1_LOB_VALUES_5 = 'C';
	public static final char NF05_CALC1_LOB_VALUES_6 = 'D';
	public static final char NF05_CALC1_LOB_VALUES_7 = 'E';
	public static final char NF05_CALC1_LOB_CROSS = '1';
	public static final char NF05_CALC1_LOB_SHIELD = '2';
	public static final char NF05_CALC1_LOB_MAJOR_MED = '3';
	public static final char NF05_CALC1_LOB_SHARE_PAY = '4';
	public static final char NF05_CALC1_LOB65PLUS = '5';
	public static final char NF05_CALC1_LOB_DENTAL = '6';
	public static final char NF05_CALC1_LOB_STAND_VISION = 'A';
	public static final char NF05_CALC1_LOB_STAND_DRUG = 'B';
	public static final char NF05_CALC1_LOB_CANCER = 'C';
	public static final char NF05_CALC1_LOB_LTCARE = 'D';
	public static final char NF05_CALC1_LOB_HEARING = 'E';
	public static final char NF05_CALC1_PAY_REGISTRATION = '1';
	public static final char NF05_CALC1_PAY_HCPCS_PTS = '2';
	public static final char NF05_CALC1_PAY_HCPCS_PTS_MOD = '3';
	public static final char NF05_CALC1_PAY_REASONABLE_CHG = '4';
	public static final char NF05_CALC1_PAY_REASON_CHG_MOD = '5';
	public static final char NF05_CALC1_PAY_INDEMNITY = '6';
	public static final char NF05_CALC1_PAY_DRG_INLIER = '7';
	public static final char NF05_CALC1_PAY_DRG_OUTLIER = '8';
	public static final char NF05_CALC1_PAY_PAID_CHARGES = '9';
	public static final char NF05_CALC1_PAY_REVIEW_COMM = 'A';
	public static final char NF05_CALC1_PAY_SPCL_REVIEW_UR = 'E';
	public static final char NF05_CALC1_PAY_SPCL_REVIEW_DP = 'F';
	public static final char NF05_CALC1_PAY_MANAGEMENT_DEC = 'G';
	public static final char NF05_CALC1_PAY_PLAN65 = 'Z';
	private static final String[] NF051_TYPE_SINGLE_DENTAL = new String[] { "AA", "AC", "AD", "EA", "EC", "ED" };
	private static final String[] NF051_TYPE_CONTRACT_SINGLE = new String[] { " 1", "1", " A", "A", " E", "E", " Q", "Q", " 3", "3", " B", "B", " K",
			"K", " R", "R", " 4", "4", " C", "C", " N", "N", " T", "T", " 6", "6", " D", "D", " P", "P", " Y", "Y", "AA", "AC", "AD", "EA", "EC",
			"ED", "SL", " Z", "Z" };
	private static final String[] NF051_TYPE_FAMILY_DENTAL = new String[] { "AB", "AE", "AF", "AG", "AH", "EB", "EE", "EF", "EG", "EH", "EI", "EJ" };
	private static final String[] NF051_TYPE_CONTRACT_FAMILY = new String[] { " 2", "2", " J", "J", "AB", "AE", "AF", "AG", " 5", "5", " L", "L",
			"AH", "A7", "BA", "BB", " 7", "7", " M", "M", "BC", "B7", "EB", "EE", " 8", "8", " S", "S", "EF", "EG", "EH", "EI", " 9", "9", " U", "U",
			"EJ", "SA", "SB", "SC", " F", "F", " V", "V", "SD", "SE", "SM", "SN", " G", "G", " W", "W", "SP", "SQ", "SR", "SS", " H", "H", " X", "X",
			"ST", "SV", "SW", "SY" };
	public static final char NF05_EXPL1_DENIED = ' ';
	private static final char[] NF05_EXPL1_CONTRACTING = new char[] { 'S', 'U', 'W' };
	private static final char[] NF05_EXPL1_NON_CONTRACT = new char[] { 'V', 'T', 'X' };
	public static final char NF05_EXPL1_OUT_OF_AREA = 'D';
	public static final char NF05_EXPL1_PLAN65_SHIELD = 'E';
	public static final char NF05_EXPL1_INDEMNIFIED = 'F';
	public static final char NF05_EXPL1_SUBSCRIB_HOSP_CHC = 'H';
	public static final char NF05_EXPL165_PLUS = 'J';
	public static final char NF05_EXPL1_PLAN150 = 'L';
	public static final char NF05_EXPL1_KC_NON_PREFER_PVDR = 'N';
	public static final char NF05_EXPL1_KC_PREFERED_PVDR = 'P';
	public static final char NF05_EXPL1_CONTRACT_CPR = 'S';
	public static final char NF05_EXPL1_CONTRACT_CHC = 'S';
	public static final char NF05_EXPL1_NON_CONTRACT_CPR = 'T';
	public static final char NF05_EXPL1_NON_CONTRACT_CHC = 'T';
	public static final char NF05_EXPL1_CONTRACT_PR_CPR = 'U';
	public static final char NF05_EXPL1_CONTRACT_PR_CHC = 'U';
	public static final char NF05_EXPL1_NON_CONTRACT_P_CPR = 'V';
	public static final char NF05_EXPL1_NON_CONTRACT_P_CHC = 'V';
	public static final char NF05_EXPL1_CONTRACT_CCR = 'W';
	public static final char NF05_EXPL1_PLAN65_CROSS = 'Y';
	public static final char NF05_EXPL1_NON_CONTRACT_P_CCR = 'X';
	public static final char NF05_CALC2_LOB_NONE = ' ';
	public static final char NF05_CALC2_LOB_VALUES_1_MIN = '1';
	public static final char NF05_CALC2_LOB_VALUES_1_MAX = '6';
	public static final char NF05_CALC2_LOB_VALUES_2 = '9';
	public static final char NF05_CALC2_LOB_VALUES_3 = 'A';
	public static final char NF05_CALC2_LOB_VALUES_4 = 'B';
	public static final char NF05_CALC2_LOB_VALUES_5 = 'C';
	public static final char NF05_CALC2_LOB_VALUES_6 = 'D';
	public static final char NF05_CALC2_LOB_VALUES_7 = 'E';
	public static final char NF05_CALC2_LOB_CROSS = '1';
	public static final char NF05_CALC2_LOB_SHIELD = '2';
	public static final char NF05_CALC2_LOB_MAJOR_MED = '3';
	public static final char NF05_CALC2_LOB_SHARE_PAY = '4';
	public static final char NF05_CALC2_LOB65PLUS = '5';
	public static final char NF05_CALC2_LOB_DENTAL = '6';
	public static final char NF05_CALC2_LOB_STAND_VISION = 'A';
	public static final char NF05_CALC2_LOB_STAND_DRUG = 'B';
	public static final char NF05_CALC2_LOB_CANCER = 'C';
	public static final char NF05_CALC2_LOB_LTCARE = 'D';
	public static final char NF05_CALC2_LOB_HEARING = 'E';
	public static final char NF05_CALC2_PAY_REGISTRATION = '1';
	public static final char NF05_CALC2_PAY_HCPCS_PTS = '2';
	public static final char NF05_CALC2_PAY_HCPCS_PTS_MOD = '3';
	public static final char NF05_CALC2_PAY_REASONABLE_CHG = '4';
	public static final char NF05_CALC2_PAY_REASON_CHG_MOD = '5';
	public static final char NF05_CALC2_PAY_INDEMNITY = '6';
	public static final char NF05_CALC2_PAY_DRG_INLIER = '7';
	public static final char NF05_CALC2_PAY_DRG_OUTLIER = '8';
	public static final char NF05_CALC2_PAY_PAID_CHARGES = '9';
	public static final char NF05_CALC2_PAY_REVIEW_COMM = 'A';
	public static final char NF05_CALC2_PAY_SPCL_REVIEW_UR = 'E';
	public static final char NF05_CALC2_PAY_SPCL_REVIEW_DP = 'F';
	public static final char NF05_CALC2_PAY_MANAGEMENT_DEC = 'G';
	public static final char NF05_CALC2_PAY_PLAN65 = 'Z';
	private static final String[] NF052_TYPE_SINGLE_DENTAL = new String[] { "AA", "AC", "AD", "EA", "EC", "ED" };
	private static final String[] NF052_TYPE_CONTRACT_SINGLE = new String[] { " 1", "1", " A", "A", " E", "E", " Q", "Q", " 3", "3", " B", "B", " K",
			"K", " R", "R", " 4", "4", " C", "C", " N", "N", " T", "T", " 6", "6", " D", "D", " P", "P", " Y", "Y", "AA", "AC", "AD", "EA", "EC",
			"ED", "SL", " Z", "Z" };
	private static final String[] NF052_TYPE_FAMILY_DENTAL = new String[] { "AB", "AE", "AF", "AG", "AH", "EB", "EE", "EF", "EG", "EH", "EI", "EJ" };
	private static final String[] NF052_TYPE_CONTRACT_FAMILY = new String[] { " 2", "2", " J", "J", "AB", "AE", "AF", "AG", " 5", "5", " L", "L",
			"AH", "A7", "BA", "BB", " 7", "7", " M", "M", "BC", "B7", "EB", "EE", " 8", "8", " S", "S", "EF", "EG", "EH", "EI", " 9", "9", " U", "U",
			"EJ", "SA", "SB", "SC", " F", "F", " V", "V", "SD", "SE", "SM", "SN", " G", "G", " W", "W", "SP", "SQ", "SR", "SS", " H", "H", " X", "X",
			"ST", "SV", "SW", "SY" };
	public static final char NF05_EXPL2_DENIED = ' ';
	private static final char[] NF05_EXPL2_CONTRACTING = new char[] { 'S', 'U', 'W' };
	private static final char[] NF05_EXPL2_NON_CONTRACT = new char[] { 'V', 'T', 'X' };
	public static final char NF05_EXPL2_OUT_OF_AREA = 'D';
	public static final char NF05_EXPL2_PLAN65_SHIELD = 'E';
	public static final char NF05_EXPL2_INDEMNIFIED = 'F';
	public static final char NF05_EXPL2_SUBSCRIB_HOSP_CHC = 'H';
	public static final char NF05_EXPL265_PLUS = 'J';
	public static final char NF05_EXPL2_PLAN150 = 'L';
	public static final char NF05_EXPL2_KC_NON_PREFER_PVDR = 'N';
	public static final char NF05_EXPL2_KC_PREFERED_PVDR = 'P';
	public static final char NF05_EXPL2_CONTRACT_CPR = 'S';
	public static final char NF05_EXPL2_CONTRACT_CHC = 'S';
	public static final char NF05_EXPL2_NON_CONTRACT_CPR = 'T';
	public static final char NF05_EXPL2_NON_CONTRACT_CHC = 'T';
	public static final char NF05_EXPL2_CONTRACT_PR_CPR = 'U';
	public static final char NF05_EXPL2_CONTRACT_PR_CHC = 'U';
	public static final char NF05_EXPL2_NON_CONTRACT_P_CPR = 'V';
	public static final char NF05_EXPL2_NON_CONTRACT_P_CHC = 'V';
	public static final char NF05_EXPL2_CONTRACT_CCR = 'W';
	public static final char NF05_EXPL2_PLAN65_CROSS = 'Y';
	public static final char NF05_EXPL2_NON_CONTRACT_P_CCR = 'X';
	public static final char NF05_CALC3_LOB_NONE = ' ';
	public static final char NF05_CALC3_LOB_VALUES_1_MIN = '1';
	public static final char NF05_CALC3_LOB_VALUES_1_MAX = '6';
	public static final char NF05_CALC3_LOB_VALUES_2 = '9';
	public static final char NF05_CALC3_LOB_VALUES_3 = 'A';
	public static final char NF05_CALC3_LOB_VALUES_4 = 'B';
	public static final char NF05_CALC3_LOB_VALUES_5 = 'C';
	public static final char NF05_CALC3_LOB_VALUES_6 = 'D';
	public static final char NF05_CALC3_LOB_VALUES_7 = 'E';
	public static final char NF05_CALC3_LOB_CROSS = '1';
	public static final char NF05_CALC3_LOB_SHIELD = '2';
	public static final char NF05_CALC3_LOB_MAJOR_MED = '3';
	public static final char NF05_CALC3_LOB_SHARE_PAY = '4';
	public static final char NF05_CALC3_LOB65PLUS = '5';
	public static final char NF05_CALC3_LOB_DENTAL = '6';
	public static final char NF05_CALC3_LOB_STAND_VISION = 'A';
	public static final char NF05_CALC3_LOB_STAND_DRUG = 'B';
	public static final char NF05_CALC3_LOB_CANCER = 'C';
	public static final char NF05_CALC3_LOB_LTCARE = 'D';
	public static final char NF05_CALC3_LOB_HEARING = 'E';
	public static final char NF05_CALC3_PAY_REGISTRATION = '1';
	public static final char NF05_CALC3_PAY_HCPCS_PTS = '2';
	public static final char NF05_CALC3_PAY_HCPCS_PTS_MOD = '3';
	public static final char NF05_CALC3_PAY_REASONABLE_CHG = '4';
	public static final char NF05_CALC3_PAY_REASON_CHG_MOD = '5';
	public static final char NF05_CALC3_PAY_INDEMNITY = '6';
	public static final char NF05_CALC3_PAY_DRG_INLIER = '7';
	public static final char NF05_CALC3_PAY_DRG_OUTLIER = '8';
	public static final char NF05_CALC3_PAY_PAID_CHARGES = '9';
	public static final char NF05_CALC3_PAY_REVIEW_COMM = 'A';
	public static final char NF05_CALC3_PAY_SPCL_REVIEW_UR = 'E';
	public static final char NF05_CALC3_PAY_SPCL_REVIEW_DP = 'F';
	public static final char NF05_CALC3_PAY_MANAGEMENT_DEC = 'G';
	public static final char NF05_CALC3_PAY_PLAN65 = 'Z';
	private static final String[] NF053_TYPE_SINGLE_DENTAL = new String[] { "AA", "AC", "AD", "EA", "EC", "ED" };
	private static final String[] NF053_TYPE_CONTRACT_SINGLE = new String[] { " 1", "1", " A", "A", " E", "E", " Q", "Q", " 3", "3", " B", "B", " K",
			"K", " R", "R", " 4", "4", " C", "C", " N", "N", " T", "T", " 6", "6", " D", "D", " P", "P", " Y", "Y", "AA", "AC", "AD", "EA", "EC",
			"ED", "SL", " Z", "Z" };
	private static final String[] NF053_TYPE_FAMILY_DENTAL = new String[] { "AB", "AE", "AF", "AG", "AH", "EB", "EE", "EF", "EG", "EH", "EI", "EJ" };
	private static final String[] NF053_TYPE_CONTRACT_FAMILY = new String[] { " 2", "2", " J", "J", "AB", "AE", "AF", "AG", " 5", "5", " L", "L",
			"AH", "A7", "BA", "BB", " 7", "7", " M", "M", "BC", "B7", "EB", "EE", " 8", "8", " S", "S", "EF", "EG", "EH", "EI", " 9", "9", " U", "U",
			"EJ", "SA", "SB", "SC", " F", "F", " V", "V", "SD", "SE", "SM", "SN", " G", "G", " W", "W", "SP", "SQ", "SR", "SS", " H", "H", " X", "X",
			"ST", "SV", "SW", "SY" };
	public static final char NF05_EXPL3_DENIED = ' ';
	private static final char[] NF05_EXPL3_CONTRACTING = new char[] { 'S', 'U', 'W' };
	private static final char[] NF05_EXPL3_NON_CONTRACT = new char[] { 'V', 'T', 'X' };
	public static final char NF05_EXPL3_OUT_OF_AREA = 'D';
	public static final char NF05_EXPL3_PLAN65_SHIELD = 'E';
	public static final char NF05_EXPL3_INDEMNIFIED = 'F';
	public static final char NF05_EXPL3_SUBSCRIB_HOSP_CHC = 'H';
	public static final char NF05_EXPL365_PLUS = 'J';
	public static final char NF05_EXPL3_PLAN150 = 'L';
	public static final char NF05_EXPL3_KC_NON_PREFER_PVDR = 'N';
	public static final char NF05_EXPL3_KC_PREFERED_PVDR = 'P';
	public static final char NF05_EXPL3_CONTRACT_CPR = 'S';
	public static final char NF05_EXPL3_CONTRACT_CHC = 'S';
	public static final char NF05_EXPL3_NON_CONTRACT_CPR = 'T';
	public static final char NF05_EXPL3_NON_CONTRACT_CHC = 'T';
	public static final char NF05_EXPL3_CONTRACT_PR_CPR = 'U';
	public static final char NF05_EXPL3_CONTRACT_PR_CHC = 'U';
	public static final char NF05_EXPL3_NON_CONTRACT_P_CPR = 'V';
	public static final char NF05_EXPL3_NON_CONTRACT_P_CHC = 'V';
	public static final char NF05_EXPL3_CONTRACT_CCR = 'W';
	public static final char NF05_EXPL3_PLAN65_CROSS = 'Y';
	public static final char NF05_EXPL3_NON_CONTRACT_P_CCR = 'X';
	public static final char NF05_CALC4_LOB_NONE = ' ';
	public static final char NF05_CALC4_LOB_VALUES_1_MIN = '1';
	public static final char NF05_CALC4_LOB_VALUES_1_MAX = '6';
	public static final char NF05_CALC4_LOB_VALUES_2 = '9';
	public static final char NF05_CALC4_LOB_VALUES_3 = 'A';
	public static final char NF05_CALC4_LOB_VALUES_4 = 'B';
	public static final char NF05_CALC4_LOB_VALUES_5 = 'C';
	public static final char NF05_CALC4_LOB_VALUES_6 = 'D';
	public static final char NF05_CALC4_LOB_VALUES_7 = 'E';
	public static final char NF05_CALC4_LOB_CROSS = '1';
	public static final char NF05_CALC4_LOB_SHIELD = '2';
	public static final char NF05_CALC4_LOB_MAJOR_MED = '3';
	public static final char NF05_CALC4_LOB_SHARE_PAY = '4';
	public static final char NF05_CALC4_LOB65PLUS = '5';
	public static final char NF05_CALC4_LOB_DENTAL = '6';
	public static final char NF05_CALC4_LOB_STAND_VISION = 'A';
	public static final char NF05_CALC4_LOB_STAND_DRUG = 'B';
	public static final char NF05_CALC4_LOB_CANCER = 'C';
	public static final char NF05_CALC4_LOB_LTCARE = 'D';
	public static final char NF05_CALC4_LOB_HEARING = 'E';
	public static final char NF05_CALC4_PAY_REGISTRATION = '1';
	public static final char NF05_CALC4_PAY_HCPCS_PTS = '2';
	public static final char NF05_CALC4_PAY_HCPCS_PTS_MOD = '3';
	public static final char NF05_CALC4_PAY_REASONABLE_CHG = '4';
	public static final char NF05_CALC4_PAY_REASON_CHG_MOD = '5';
	public static final char NF05_CALC4_PAY_INDEMNITY = '6';
	public static final char NF05_CALC4_PAY_DRG_INLIER = '7';
	public static final char NF05_CALC4_PAY_DRG_OUTLIER = '8';
	public static final char NF05_CALC4_PAY_PAID_CHARGES = '9';
	public static final char NF05_CALC4_PAY_REVIEW_COMM = 'A';
	public static final char NF05_CALC4_PAY_SPCL_REVIEW_UR = 'E';
	public static final char NF05_CALC4_PAY_SPCL_REVIEW_DP = 'F';
	public static final char NF05_CALC4_PAY_MANAGEMENT_DEC = 'G';
	public static final char NF05_CALC4_PAY_PLAN65 = 'Z';
	private static final String[] NF054_TYPE_SINGLE_DENTAL = new String[] { "AA", "AC", "AD", "EA", "EC", "ED" };
	private static final String[] NF054_TYPE_CONTRACT_SINGLE = new String[] { " 1", "1", " A", "A", " E", "E", " Q", "Q", " 3", "3", " B", "B", " K",
			"K", " R", "R", " 4", "4", " C", "C", " N", "N", " T", "T", " 6", "6", " D", "D", " P", "P", " Y", "Y", "AA", "AC", "AD", "EA", "EC",
			"ED", "SL", " Z", "Z" };
	private static final String[] NF054_TYPE_FAMILY_DENTAL = new String[] { "AB", "AE", "AF", "AG", "AH", "EB", "EE", "EF", "EG", "EH", "EI", "EJ" };
	private static final String[] NF054_TYPE_CONTRACT_FAMILY = new String[] { " 2", "2", " J", "J", "AB", "AE", "AF", "AG", " 5", "5", " L", "L",
			"AH", "A7", "BA", "BB", " 7", "7", " M", "M", "BC", "B7", "EB", "EE", " 8", "8", " S", "S", "EF", "EG", "EH", "EI", " 9", "9", " U", "U",
			"EJ", "SA", "SB", "SC", " F", "F", " V", "V", "SD", "SE", "SM", "SN", " G", "G", " W", "W", "SP", "SQ", "SR", "SS", " H", "H", " X", "X",
			"ST", "SV", "SW", "SY" };
	public static final char NF05_EXPL4_DENIED = ' ';
	private static final char[] NF05_EXPL4_CONTRACTING = new char[] { 'S', 'U', 'W' };
	private static final char[] NF05_EXPL4_NON_CONTRACT = new char[] { 'V', 'T', 'X' };
	public static final char NF05_EXPL4_OUT_OF_AREA = 'D';
	public static final char NF05_EXPL4_PLAN65_SHIELD = 'E';
	public static final char NF05_EXPL4_INDEMNIFIED = 'F';
	public static final char NF05_EXPL4_SUBSCRIB_HOSP_CHC = 'H';
	public static final char NF05_EXPL465_PLUS = 'J';
	public static final char NF05_EXPL4_PLAN150 = 'L';
	public static final char NF05_EXPL4_KC_NON_PREFER_PVDR = 'N';
	public static final char NF05_EXPL4_KC_PREFERED_PVDR = 'P';
	public static final char NF05_EXPL4_CONTRACT_CPR = 'S';
	public static final char NF05_EXPL4_CONTRACT_CHC = 'S';
	public static final char NF05_EXPL4_NON_CONTRACT_CPR = 'T';
	public static final char NF05_EXPL4_NON_CONTRACT_CHC = 'T';
	public static final char NF05_EXPL4_CONTRACT_PR_CPR = 'U';
	public static final char NF05_EXPL4_CONTRACT_PR_CHC = 'U';
	public static final char NF05_EXPL4_NON_CONTRACT_P_CPR = 'V';
	public static final char NF05_EXPL4_NON_CONTRACT_P_CHC = 'V';
	public static final char NF05_EXPL4_CONTRACT_CCR = 'W';
	public static final char NF05_EXPL4_PLAN65_CROSS = 'Y';
	public static final char NF05_EXPL4_NON_CONTRACT_P_CCR = 'X';
	public static final char NF05_CALC5_LOB_NONE = ' ';
	public static final char NF05_CALC5_LOB_VALUES_1_MIN = '1';
	public static final char NF05_CALC5_LOB_VALUES_1_MAX = '6';
	public static final char NF05_CALC5_LOB_VALUES_2 = '9';
	public static final char NF05_CALC5_LOB_VALUES_3 = 'A';
	public static final char NF05_CALC5_LOB_VALUES_4 = 'B';
	public static final char NF05_CALC5_LOB_VALUES_5 = 'C';
	public static final char NF05_CALC5_LOB_VALUES_6 = 'D';
	public static final char NF05_CALC5_LOB_VALUES_7 = 'E';
	public static final char NF05_CALC5_LOB_CROSS = '1';
	public static final char NF05_CALC5_LOB_SHIELD = '2';
	public static final char NF05_CALC5_LOB_MAJOR_MED = '3';
	public static final char NF05_CALC5_LOB_SHARE_PAY = '4';
	public static final char NF05_CALC5_LOB65PLUS = '5';
	public static final char NF05_CALC5_LOB_DENTAL = '6';
	public static final char NF05_CALC5_LOB_STAND_VISION = 'A';
	public static final char NF05_CALC5_LOB_STAND_DRUG = 'B';
	public static final char NF05_CALC5_LOB_CANCER = 'C';
	public static final char NF05_CALC5_LOB_LTCARE = 'D';
	public static final char NF05_CALC5_LOB_HEARING = 'E';
	public static final char NF05_CALC5_PAY_REGISTRATION = '1';
	public static final char NF05_CALC5_PAY_HCPCS_PTS = '2';
	public static final char NF05_CALC5_PAY_HCPCS_PTS_MOD = '3';
	public static final char NF05_CALC5_PAY_REASONABLE_CHG = '4';
	public static final char NF05_CALC5_PAY_REASON_CHG_MOD = '5';
	public static final char NF05_CALC5_PAY_INDEMNITY = '6';
	public static final char NF05_CALC5_PAY_DRG_INLIER = '7';
	public static final char NF05_CALC5_PAY_DRG_OUTLIER = '8';
	public static final char NF05_CALC5_PAY_PAID_CHARGES = '9';
	public static final char NF05_CALC5_PAY_REVIEW_COMM = 'A';
	public static final char NF05_CALC5_PAY_SPCL_REVIEW_UR = 'E';
	public static final char NF05_CALC5_PAY_SPCL_REVIEW_DP = 'F';
	public static final char NF05_CALC5_PAY_MANAGEMENT_DEC = 'G';
	public static final char NF05_CALC5_PAY_PLAN65 = 'Z';
	private static final String[] NF055_TYPE_SINGLE_DENTAL = new String[] { "AA", "AC", "AD", "EA", "EC", "ED" };
	private static final String[] NF055_TYPE_CONTRACT_SINGLE = new String[] { " 1", "1", " A", "A", " E", "E", " Q", "Q", " 3", "3", " B", "B", " K",
			"K", " R", "R", " 4", "4", " C", "C", " N", "N", " T", "T", " 6", "6", " D", "D", " P", "P", " Y", "Y", "AA", "AC", "AD", "EA", "EC",
			"ED", "SL", " Z", "Z" };
	private static final String[] NF055_TYPE_FAMILY_DENTAL = new String[] { "AB", "AE", "AF", "AG", "AH", "EB", "EE", "EF", "EG", "EH", "EI", "EJ" };
	private static final String[] NF055_TYPE_CONTRACT_FAMILY = new String[] { " 2", "2", " J", "J", "AB", "AE", "AF", "AG", " 5", "5", " L", "L",
			"AH", "A7", "BA", "BB", " 7", "7", " M", "M", "BC", "B7", "EB", "EE", " 8", "8", " S", "S", "EF", "EG", "EH", "EI", " 9", "9", " U", "U",
			"EJ", "SA", "SB", "SC", " F", "F", " V", "V", "SD", "SE", "SM", "SN", " G", "G", " W", "W", "SP", "SQ", "SR", "SS", " H", "H", " X", "X",
			"ST", "SV", "SW", "SY" };
	public static final char NF05_EXPL5_DENIED = ' ';
	private static final char[] NF05_EXPL5_CONTRACTING = new char[] { 'S', 'U', 'W' };
	private static final char[] NF05_EXPL5_NON_CONTRACT = new char[] { 'V', 'T', 'X' };
	public static final char NF05_EXPL5_OUT_OF_AREA = 'D';
	public static final char NF05_EXPL5_PLAN65_SHIELD = 'E';
	public static final char NF05_EXPL5_INDEMNIFIED = 'F';
	public static final char NF05_EXPL5_SUBSCRIB_HOSP_CHC = 'H';
	public static final char NF05_EXPL565_PLUS = 'J';
	public static final char NF05_EXPL5_PLAN150 = 'L';
	public static final char NF05_EXPL5_KC_NON_PREFER_PVDR = 'N';
	public static final char NF05_EXPL5_KC_PREFERED_PVDR = 'P';
	public static final char NF05_EXPL5_CONTRACT_CPR = 'S';
	public static final char NF05_EXPL5_CONTRACT_CHC = 'S';
	public static final char NF05_EXPL5_NON_CONTRACT_CPR = 'T';
	public static final char NF05_EXPL5_NON_CONTRACT_CHC = 'T';
	public static final char NF05_EXPL5_CONTRACT_PR_CPR = 'U';
	public static final char NF05_EXPL5_CONTRACT_PR_CHC = 'U';
	public static final char NF05_EXPL5_NON_CONTRACT_P_CPR = 'V';
	public static final char NF05_EXPL5_NON_CONTRACT_P_CHC = 'V';
	public static final char NF05_EXPL5_CONTRACT_CCR = 'W';
	public static final char NF05_EXPL5_PLAN65_CROSS = 'Y';
	public static final char NF05_EXPL5_NON_CONTRACT_P_CCR = 'X';
	public static final char NF05_CALC6_LOB_NONE = ' ';
	public static final char NF05_CALC6_LOB_VALUES_1_MIN = '1';
	public static final char NF05_CALC6_LOB_VALUES_1_MAX = '6';
	public static final char NF05_CALC6_LOB_VALUES_2 = '9';
	public static final char NF05_CALC6_LOB_VALUES_3 = 'A';
	public static final char NF05_CALC6_LOB_VALUES_4 = 'B';
	public static final char NF05_CALC6_LOB_VALUES_5 = 'C';
	public static final char NF05_CALC6_LOB_VALUES_6 = 'D';
	public static final char NF05_CALC6_LOB_VALUES_7 = 'E';
	public static final char NF05_CALC6_LOB_CROSS = '1';
	public static final char NF05_CALC6_LOB_SHIELD = '2';
	public static final char NF05_CALC6_LOB_MAJOR_MED = '3';
	public static final char NF05_CALC6_LOB_SHARE_PAY = '4';
	public static final char NF05_CALC6_LOB65PLUS = '5';
	public static final char NF05_CALC6_LOB_DENTAL = '6';
	public static final char NF05_CALC6_LOB_STAND_VISION = 'A';
	public static final char NF05_CALC6_LOB_STAND_DRUG = 'B';
	public static final char NF05_CALC6_LOB_CANCER = 'C';
	public static final char NF05_CALC6_LOB_LTCARE = 'D';
	public static final char NF05_CALC6_LOB_HEARING = 'E';
	public static final char NF05_CALC6_PAY_REGISTRATION = '1';
	public static final char NF05_CALC6_PAY_HCPCS_PTS = '2';
	public static final char NF05_CALC6_PAY_HCPCS_PTS_MOD = '3';
	public static final char NF05_CALC6_PAY_REASONABLE_CHG = '4';
	public static final char NF05_CALC6_PAY_REASON_CHG_MOD = '5';
	public static final char NF05_CALC6_PAY_INDEMNITY = '6';
	public static final char NF05_CALC6_PAY_DRG_INLIER = '7';
	public static final char NF05_CALC6_PAY_DRG_OUTLIER = '8';
	public static final char NF05_CALC6_PAY_PAID_CHARGES = '9';
	public static final char NF05_CALC6_PAY_REVIEW_COMM = 'A';
	public static final char NF05_CALC6_PAY_SPCL_REVIEW_UR = 'E';
	public static final char NF05_CALC6_PAY_SPCL_REVIEW_DP = 'F';
	public static final char NF05_CALC6_PAY_MANAGEMENT_DEC = 'G';
	public static final char NF05_CALC6_PAY_PLAN65 = 'Z';
	private static final String[] NF056_TYPE_SINGLE_DENTAL = new String[] { "AA", "AC", "AD", "EA", "EC", "ED" };
	private static final String[] NF056_TYPE_CONTRACT_SINGLE = new String[] { " 1", "1", " A", "A", " E", "E", " Q", "Q", " 3", "3", " B", "B", " K",
			"K", " R", "R", " 4", "4", " C", "C", " N", "N", " T", "T", " 6", "6", " D", "D", " P", "P", " Y", "Y", "AA", "AC", "AD", "EA", "EC",
			"ED", "SL", " Z", "Z" };
	private static final String[] NF056_TYPE_FAMILY_DENTAL = new String[] { "AB", "AE", "AF", "AG", "AH", "EB", "EE", "EF", "EG", "EH", "EI", "EJ" };
	private static final String[] NF056_TYPE_CONTRACT_FAMILY = new String[] { " 2", "2", " J", "J", "AB", "AE", "AF", "AG", " 5", "5", " L", "L",
			"AH", "A7", "BA", "BB", " 7", "7", " M", "M", "BC", "B7", "EB", "EE", " 8", "8", " S", "S", "EF", "EG", "EH", "EI", " 9", "9", " U", "U",
			"EJ", "SA", "SB", "SC", " F", "F", " V", "V", "SD", "SE", "SM", "SN", " G", "G", " W", "W", "SP", "SQ", "SR", "SS", " H", "H", " X", "X",
			"ST", "SV", "SW", "SY" };
	public static final char NF05_EXPL6_DENIED = ' ';
	private static final char[] NF05_EXPL6_CONTRACTING = new char[] { 'S', 'U', 'W' };
	private static final char[] NF05_EXPL6_NON_CONTRACT = new char[] { 'V', 'T', 'X' };
	public static final char NF05_EXPL6_OUT_OF_AREA = 'D';
	public static final char NF05_EXPL6_PLAN65_SHIELD = 'E';
	public static final char NF05_EXPL6_INDEMNIFIED = 'F';
	public static final char NF05_EXPL6_SUBSCRIB_HOSP_CHC = 'H';
	public static final char NF05_EXPL665_PLUS = 'J';
	public static final char NF05_EXPL6_PLAN150 = 'L';
	public static final char NF05_EXPL6_KC_NON_PREFER_PVDR = 'N';
	public static final char NF05_EXPL6_KC_PREFERED_PVDR = 'P';
	public static final char NF05_EXPL6_CONTRACT_CPR = 'S';
	public static final char NF05_EXPL6_CONTRACT_CHC = 'S';
	public static final char NF05_EXPL6_NON_CONTRACT_CPR = 'T';
	public static final char NF05_EXPL6_NON_CONTRACT_CHC = 'T';
	public static final char NF05_EXPL6_CONTRACT_PR_CPR = 'U';
	public static final char NF05_EXPL6_CONTRACT_PR_CHC = 'U';
	public static final char NF05_EXPL6_NON_CONTRACT_P_CPR = 'V';
	public static final char NF05_EXPL6_NON_CONTRACT_P_CHC = 'V';
	public static final char NF05_EXPL6_CONTRACT_CCR = 'W';
	public static final char NF05_EXPL6_PLAN65_CROSS = 'Y';
	public static final char NF05_EXPL6_NON_CONTRACT_P_CCR = 'X';
	public static final char NF05_CALC7_LOB_NONE = ' ';
	public static final char NF05_CALC7_LOB_VALUES_1_MIN = '1';
	public static final char NF05_CALC7_LOB_VALUES_1_MAX = '6';
	public static final char NF05_CALC7_LOB_VALUES_2 = '9';
	public static final char NF05_CALC7_LOB_VALUES_3 = 'A';
	public static final char NF05_CALC7_LOB_VALUES_4 = 'B';
	public static final char NF05_CALC7_LOB_VALUES_5 = 'C';
	public static final char NF05_CALC7_LOB_VALUES_6 = 'D';
	public static final char NF05_CALC7_LOB_VALUES_7 = 'E';
	public static final char NF05_CALC7_LOB_CROSS = '1';
	public static final char NF05_CALC7_LOB_SHIELD = '2';
	public static final char NF05_CALC7_LOB_MAJOR_MED = '3';
	public static final char NF05_CALC7_LOB_SHARE_PAY = '4';
	public static final char NF05_CALC7_LOB65PLUS = '5';
	public static final char NF05_CALC7_LOB_DENTAL = '6';
	public static final char NF05_CALC7_LOB_STAND_VISION = 'A';
	public static final char NF05_CALC7_LOB_STAND_DRUG = 'B';
	public static final char NF05_CALC7_LOB_CANCER = 'C';
	public static final char NF05_CALC7_LOB_LTCARE = 'D';
	public static final char NF05_CALC7_LOB_HEARING = 'E';
	public static final char NF05_CALC7_PAY_REGISTRATION = '1';
	public static final char NF05_CALC7_PAY_HCPCS_PTS = '2';
	public static final char NF05_CALC7_PAY_HCPCS_PTS_MOD = '3';
	public static final char NF05_CALC7_PAY_REASONABLE_CHG = '4';
	public static final char NF05_CALC7_PAY_REASON_CHG_MOD = '5';
	public static final char NF05_CALC7_PAY_INDEMNITY = '6';
	public static final char NF05_CALC7_PAY_DRG_INLIER = '7';
	public static final char NF05_CALC7_PAY_DRG_OUTLIER = '8';
	public static final char NF05_CALC7_PAY_PAID_CHARGES = '9';
	public static final char NF05_CALC7_PAY_REVIEW_COMM = 'A';
	public static final char NF05_CALC7_PAY_SPCL_REVIEW_UR = 'E';
	public static final char NF05_CALC7_PAY_SPCL_REVIEW_DP = 'F';
	public static final char NF05_CALC7_PAY_MANAGEMENT_DEC = 'G';
	public static final char NF05_CALC7_PAY_PLAN65 = 'Z';
	private static final String[] NF057_TYPE_SINGLE_DENTAL = new String[] { "AA", "AC", "AD", "EA", "EC", "ED" };
	private static final String[] NF057_TYPE_CONTRACT_SINGLE = new String[] { " 1", "1", " A", "A", " E", "E", " Q", "Q", " 3", "3", " B", "B", " K",
			"K", " R", "R", " 4", "4", " C", "C", " N", "N", " T", "T", " 6", "6", " D", "D", " P", "P", " Y", "Y", "AA", "AC", "AD", "EA", "EC",
			"ED", "SL", " Z", "Z" };
	private static final String[] NF057_TYPE_FAMILY_DENTAL = new String[] { "AB", "AE", "AF", "AG", "AH", "EB", "EE", "EF", "EG", "EH", "EI", "EJ" };
	private static final String[] NF057_TYPE_CONTRACT_FAMILY = new String[] { " 2", "2", " J", "J", "AB", "AE", "AF", "AG", " 5", "5", " L", "L",
			"AH", "A7", "BA", "BB", " 7", "7", " M", "M", "BC", "B7", "EB", "EE", " 8", "8", " S", "S", "EF", "EG", "EH", "EI", " 9", "9", " U", "U",
			"EJ", "SA", "SB", "SC", " F", "F", " V", "V", "SD", "SE", "SM", "SN", " G", "G", " W", "W", "SP", "SQ", "SR", "SS", " H", "H", " X", "X",
			"ST", "SV", "SW", "SY" };
	public static final char NF05_EXPL7_DENIED = ' ';
	private static final char[] NF05_EXPL7_CONTRACTING = new char[] { 'S', 'U', 'W' };
	private static final char[] NF05_EXPL7_NON_CONTRACT = new char[] { 'V', 'T', 'X' };
	public static final char NF05_EXPL7_OUT_OF_AREA = 'D';
	public static final char NF05_EXPL7_PLAN65_SHIELD = 'E';
	public static final char NF05_EXPL7_INDEMNIFIED = 'F';
	public static final char NF05_EXPL7_SUBSCRIB_HOSP_CHC = 'H';
	public static final char NF05_EXPL765_PLUS = 'J';
	public static final char NF05_EXPL7_PLAN150 = 'L';
	public static final char NF05_EXPL7_KC_NON_PREFER_PVDR = 'N';
	public static final char NF05_EXPL7_KC_PREFERED_PVDR = 'P';
	public static final char NF05_EXPL7_CONTRACT_CPR = 'S';
	public static final char NF05_EXPL7_CONTRACT_CHC = 'S';
	public static final char NF05_EXPL7_NON_CONTRACT_CPR = 'T';
	public static final char NF05_EXPL7_NON_CONTRACT_CHC = 'T';
	public static final char NF05_EXPL7_CONTRACT_PR_CPR = 'U';
	public static final char NF05_EXPL7_CONTRACT_PR_CHC = 'U';
	public static final char NF05_EXPL7_NON_CONTRACT_P_CPR = 'V';
	public static final char NF05_EXPL7_NON_CONTRACT_P_CHC = 'V';
	public static final char NF05_EXPL7_CONTRACT_CCR = 'W';
	public static final char NF05_EXPL7_PLAN65_CROSS = 'Y';
	public static final char NF05_EXPL7_NON_CONTRACT_P_CCR = 'X';
	public static final char NF05_CALC8_LOB_NONE = ' ';
	public static final char NF05_CALC8_LOB_VALUES_1_MIN = '1';
	public static final char NF05_CALC8_LOB_VALUES_1_MAX = '6';
	public static final char NF05_CALC8_LOB_VALUES_2 = '9';
	public static final char NF05_CALC8_LOB_VALUES_3 = 'A';
	public static final char NF05_CALC8_LOB_VALUES_4 = 'B';
	public static final char NF05_CALC8_LOB_VALUES_5 = 'C';
	public static final char NF05_CALC8_LOB_VALUES_6 = 'D';
	public static final char NF05_CALC8_LOB_VALUES_7 = 'E';
	public static final char NF05_CALC8_LOB_CROSS = '1';
	public static final char NF05_CALC8_LOB_SHIELD = '2';
	public static final char NF05_CALC8_LOB_MAJOR_MED = '3';
	public static final char NF05_CALC8_LOB_SHARE_PAY = '4';
	public static final char NF05_CALC8_LOB65PLUS = '5';
	public static final char NF05_CALC8_LOB_DENTAL = '6';
	public static final char NF05_CALC8_LOB_STAND_VISION = 'A';
	public static final char NF05_CALC8_LOB_STAND_DRUG = 'B';
	public static final char NF05_CALC8_LOB_CANCER = 'C';
	public static final char NF05_CALC8_LOB_LTCARE = 'D';
	public static final char NF05_CALC8_LOB_HEARING = 'E';
	public static final char NF05_CALC8_PAY_REGISTRATION = '1';
	public static final char NF05_CALC8_PAY_HCPCS_PTS = '2';
	public static final char NF05_CALC8_PAY_HCPCS_PTS_MOD = '3';
	public static final char NF05_CALC8_PAY_REASONABLE_CHG = '4';
	public static final char NF05_CALC8_PAY_REASON_CHG_MOD = '5';
	public static final char NF05_CALC8_PAY_INDEMNITY = '6';
	public static final char NF05_CALC8_PAY_DRG_INLIER = '7';
	public static final char NF05_CALC8_PAY_DRG_OUTLIER = '8';
	public static final char NF05_CALC8_PAY_PAID_CHARGES = '9';
	public static final char NF05_CALC8_PAY_REVIEW_COMM = 'A';
	public static final char NF05_CALC8_PAY_SPCL_REVIEW_UR = 'E';
	public static final char NF05_CALC8_PAY_SPCL_REVIEW_DP = 'F';
	public static final char NF05_CALC8_PAY_MANAGEMENT_DEC = 'G';
	public static final char NF05_CALC8_PAY_PLAN65 = 'Z';
	private static final String[] NF058_TYPE_SINGLE_DENTAL = new String[] { "AA", "AC", "AD", "EA", "EC", "ED" };
	private static final String[] NF058_TYPE_CONTRACT_SINGLE = new String[] { " 1", "1", " A", "A", " E", "E", " Q", "Q", " 3", "3", " B", "B", " K",
			"K", " R", "R", " 4", "4", " C", "C", " N", "N", " T", "T", " 6", "6", " D", "D", " P", "P", " Y", "Y", "AA", "AC", "AD", "EA", "EC",
			"ED", "SL", " Z", "Z" };
	private static final String[] NF058_TYPE_FAMILY_DENTAL = new String[] { "AB", "AE", "AF", "AG", "AH", "EB", "EE", "EF", "EG", "EH", "EI", "EJ" };
	private static final String[] NF058_TYPE_CONTRACT_FAMILY = new String[] { " 2", "2", " J", "J", "AB", "AE", "AF", "AG", " 5", "5", " L", "L",
			"AH", "A7", "BA", "BB", " 7", "7", " M", "M", "BC", "B7", "EB", "EE", " 8", "8", " S", "S", "EF", "EG", "EH", "EI", " 9", "9", " U", "U",
			"EJ", "SA", "SB", "SC", " F", "F", " V", "V", "SD", "SE", "SM", "SN", " G", "G", " W", "W", "SP", "SQ", "SR", "SS", " H", "H", " X", "X",
			"ST", "SV", "SW", "SY" };
	public static final char NF05_EXPL8_DENIED = ' ';
	private static final char[] NF05_EXPL8_CONTRACTING = new char[] { 'S', 'U', 'W' };
	private static final char[] NF05_EXPL8_NON_CONTRACT = new char[] { 'V', 'T', 'X' };
	public static final char NF05_EXPL8_OUT_OF_AREA = 'D';
	public static final char NF05_EXPL8_PLAN65_SHIELD = 'E';
	public static final char NF05_EXPL8_INDEMNIFIED = 'F';
	public static final char NF05_EXPL8_SUBSCRIB_HOSP_CHC = 'H';
	public static final char NF05_EXPL865_PLUS = 'J';
	public static final char NF05_EXPL8_PLAN150 = 'L';
	public static final char NF05_EXPL8_KC_NON_PREFER_PVDR = 'N';
	public static final char NF05_EXPL8_KC_PREFERED_PVDR = 'P';
	public static final char NF05_EXPL8_CONTRACT_CPR = 'S';
	public static final char NF05_EXPL8_CONTRACT_CHC = 'S';
	public static final char NF05_EXPL8_NON_CONTRACT_CPR = 'T';
	public static final char NF05_EXPL8_NON_CONTRACT_CHC = 'T';
	public static final char NF05_EXPL8_CONTRACT_PR_CPR = 'U';
	public static final char NF05_EXPL8_CONTRACT_PR_CHC = 'U';
	public static final char NF05_EXPL8_NON_CONTRACT_P_CPR = 'V';
	public static final char NF05_EXPL8_NON_CONTRACT_P_CHC = 'V';
	public static final char NF05_EXPL8_CONTRACT_CCR = 'W';
	public static final char NF05_EXPL8_PLAN65_CROSS = 'Y';
	public static final char NF05_EXPL8_NON_CONTRACT_P_CCR = 'X';
	public static final char NF05_CALC9_LOB_NONE = ' ';
	public static final char NF05_CALC9_LOB_VALUES_1_MIN = '1';
	public static final char NF05_CALC9_LOB_VALUES_1_MAX = '6';
	public static final char NF05_CALC9_LOB_VALUES_2 = '9';
	public static final char NF05_CALC9_LOB_VALUES_3 = 'A';
	public static final char NF05_CALC9_LOB_VALUES_4 = 'B';
	public static final char NF05_CALC9_LOB_VALUES_5 = 'C';
	public static final char NF05_CALC9_LOB_VALUES_6 = 'D';
	public static final char NF05_CALC9_LOB_VALUES_7 = 'E';
	public static final char NF05_CALC9_LOB_CROSS = '1';
	public static final char NF05_CALC9_LOB_SHIELD = '2';
	public static final char NF05_CALC9_LOB_MAJOR_MED = '3';
	public static final char NF05_CALC9_LOB_SHARE_PAY = '4';
	public static final char NF05_CALC9_LOB65PLUS = '5';
	public static final char NF05_CALC9_LOB_DENTAL = '6';
	public static final char NF05_CALC9_LOB_STAND_VISION = 'A';
	public static final char NF05_CALC9_LOB_STAND_DRUG = 'B';
	public static final char NF05_CALC9_LOB_CANCER = 'C';
	public static final char NF05_CALC9_LOB_LTCARE = 'D';
	public static final char NF05_CALC9_LOB_HEARING = 'E';
	public static final char NF05_CALC9_PAY_REGISTRATION = '1';
	public static final char NF05_CALC9_PAY_HCPCS_PTS = '2';
	public static final char NF05_CALC9_PAY_HCPCS_PTS_MOD = '3';
	public static final char NF05_CALC9_PAY_REASONABLE_CHG = '4';
	public static final char NF05_CALC9_PAY_REASON_CHG_MOD = '5';
	public static final char NF05_CALC9_PAY_INDEMNITY = '6';
	public static final char NF05_CALC9_PAY_DRG_INLIER = '7';
	public static final char NF05_CALC9_PAY_DRG_OUTLIER = '8';
	public static final char NF05_CALC9_PAY_PAID_CHARGES = '9';
	public static final char NF05_CALC9_PAY_REVIEW_COMM = 'A';
	public static final char NF05_CALC9_PAY_SPCL_REVIEW_UR = 'E';
	public static final char NF05_CALC9_PAY_SPCL_REVIEW_DP = 'F';
	public static final char NF05_CALC9_PAY_MANAGEMENT_DEC = 'G';
	public static final char NF05_CALC9_PAY_PLAN65 = 'Z';
	private static final String[] NF059_TYPE_SINGLE_DENTAL = new String[] { "AA", "AC", "AD", "EA", "EC", "ED" };
	private static final String[] NF059_TYPE_CONTRACT_SINGLE = new String[] { " 1", "1", " A", "A", " E", "E", " Q", "Q", " 3", "3", " B", "B", " K",
			"K", " R", "R", " 4", "4", " C", "C", " N", "N", " T", "T", " 6", "6", " D", "D", " P", "P", " Y", "Y", "AA", "AC", "AD", "EA", "EC",
			"ED", "SL", " Z", "Z" };
	private static final String[] NF059_TYPE_FAMILY_DENTAL = new String[] { "AB", "AE", "AF", "AG", "AH", "EB", "EE", "EF", "EG", "EH", "EI", "EJ" };
	private static final String[] NF059_TYPE_CONTRACT_FAMILY = new String[] { " 2", "2", " J", "J", "AB", "AE", "AF", "AG", " 5", "5", " L", "L",
			"AH", "A7", "BA", "BB", " 7", "7", " M", "M", "BC", "B7", "EB", "EE", " 8", "8", " S", "S", "EF", "EG", "EH", "EI", " 9", "9", " U", "U",
			"EJ", "SA", "SB", "SC", " F", "F", " V", "V", "SD", "SE", "SM", "SN", " G", "G", " W", "W", "SP", "SQ", "SR", "SS", " H", "H", " X", "X",
			"ST", "SV", "SW", "SY" };
	public static final char NF05_EXPL9_DENIED = ' ';
	private static final char[] NF05_EXPL9_CONTRACTING = new char[] { 'S', 'U', 'W' };
	private static final char[] NF05_EXPL9_NON_CONTRACT = new char[] { 'V', 'T', 'X' };
	public static final char NF05_EXPL9_OUT_OF_AREA = 'D';
	public static final char NF05_EXPL9_PLAN65_SHIELD = 'E';
	public static final char NF05_EXPL9_INDEMNIFIED = 'F';
	public static final char NF05_EXPL9_SUBSCRIB_HOSP_CHC = 'H';
	public static final char NF05_EXPL965_PLUS = 'J';
	public static final char NF05_EXPL9_PLAN150 = 'L';
	public static final char NF05_EXPL9_KC_NON_PREFER_PVDR = 'N';
	public static final char NF05_EXPL9_KC_PREFERED_PVDR = 'P';
	public static final char NF05_EXPL9_CONTRACT_CPR = 'S';
	public static final char NF05_EXPL9_CONTRACT_CHC = 'S';
	public static final char NF05_EXPL9_NON_CONTRACT_CPR = 'T';
	public static final char NF05_EXPL9_NON_CONTRACT_CHC = 'T';
	public static final char NF05_EXPL9_CONTRACT_PR_CPR = 'U';
	public static final char NF05_EXPL9_CONTRACT_PR_CHC = 'U';
	public static final char NF05_EXPL9_NON_CONTRACT_P_CPR = 'V';
	public static final char NF05_EXPL9_NON_CONTRACT_P_CHC = 'V';
	public static final char NF05_EXPL9_CONTRACT_CCR = 'W';
	public static final char NF05_EXPL9_PLAN65_CROSS = 'Y';
	public static final char NF05_EXPL9_NON_CONTRACT_P_CCR = 'X';
	private static final char[] NF05_CARRYOVER_USED = new char[] { 'F', 'I' };
	private static final char[] NF05_LINE_ITEM_EXPL1 = new char[] { 'W', 'X', 'D', 'F', 'T', 'V', 'N', 'L', 'J' };
	private static final char[] NF05_LINE_ITEM_EXPL2 = new char[] { 'A', 'S', 'U', 'P', 'H' };
	private static final char[] NF05_LINE_ITEM_EXPL3 = new char[] { 'A', 'F', 'S', 'U', 'P', 'H' };
	private static final char[] NF05_LINE_ITEM_EXPL4 = new char[] { 'W', 'X', 'D', 'T', 'V', 'E', 'N', 'L', 'J' };
	private static final char[] NF05_LINE_ITEM_EXPL5 = new char[] { 'T', 'W', 'X', 'D', 'F', 'Y', 'N' };
	private static final char[] NF05_LINE_ITEM_EXPL6 = new char[] { 'A', 'S', 'P', 'H' };
	public static final char NF05_LINE_ITEM_EXPL7 = 'F';
	public static final char NF05_LINE_ITEM_EXPL8 = 'P';
	private static final char[] NF05_LINE_ITEM_EXPL9 = new char[] { 'S', 'A' };
	public static final char NF05_LINE_ITEM_EXPL10 = 'H';
	private static final char[] NF05_SELECT_CARE_CLAIM = new char[] { 'N', 'R', 'S', 'W', 'P', 'C' };
	public static final char NF05_PCP_PERFORMED_SERVICE = 'N';
	public static final char NF05_PCP_COVER_REFERRED = 'R';
	public static final char NF05_SELF_REFERRED = 'S';
	public static final char NF05_NOT_APPLICABLE = 'W';
	public static final char NF05_PROCESSOR_INTERVENTION = 'P';
	public static final char NF05_COVERING_PERF_SERVICE = 'C';
	public static final char NF05_HMS = 'H';
	public static final char NF05_FEP_PPO_CAP_HOSP_TC_FEC = 'A';
	public static final char NF05_FEP_CN_SPEC_PROV_TC_FEN = 'B';
	public static final char NF05_FEP_INNTWRK_NCN = 'D';
	public static final char NF05_FEP_INNTWRK_PPO_CC_HOSP = 'I';
	public static final char NF05_FEP_OUTNTWRK_NCN = 'O';
	public static final char NF05_EXTERNAL_REFERRAL = 'E';
	private static final String[] EOB_PAT_RESP_NOTE = new String[] { "N", "T", "V", "X", "D", "W" };
	private static final String[] EOB_PROV_WRITE_NOTE = new String[] { "P", "S", "U" };
	public static final String NF05_EXPL_CODE1 = "EXPLAN CODE";
	public static final String NF05_DENIAL_REMARK_CODE1 = "DENIAL CODE";
	public static final String NF05_PENALTY_CODE1 = "EOB NOTE CODE B8";
	public static final String NF05_EOB_NOTE_CODE1 = "EOB NOTE CODE";
	public static final String NF05_GMIS_REASON_CODE1 = "GMIS REASON CODE";
	public static final String NF05_FEP_REASON_CODE1 = "FEP REASON CODE";
	public static final String NF05_EXPL_CODE2 = "EXPLAN CODE";
	public static final String NF05_DENIAL_REMARK_CODE2 = "DENIAL CODE";
	public static final String NF05_PENALTY_CODE2 = "EOB NOTE CODE B8";
	public static final String NF05_EOB_NOTE_CODE2 = "EOB NOTE CODE";
	public static final String NF05_GMIS_REASON_CODE2 = "GMIS REASON CODE";
	public static final String NF05_FEP_REASON_CODE2 = "FEP REASON CODE";
	public static final String NF05_EXPL_CODE3 = "EXPLAN CODE";
	public static final String NF05_DENIAL_REMARK_CODE3 = "DENIAL CODE";
	public static final String NF05_PENALTY_CODE3 = "EOB NOTE CODE B8";
	public static final String NF05_EOB_NOTE_CODE3 = "EOB NOTE CODE";
	public static final String NF05_GMIS_REASON_CODE3 = "GMIS REASON CODE";
	public static final String NF05_FEP_REASON_CODE3 = "FEP REASON CODE";
	public static final String NF05_EXPL_CODE4 = "EXPLAN CODE";
	public static final String NF05_DENIAL_REMARK_CODE4 = "DENIAL CODE";
	public static final String NF05_PENALTY_CODE4 = "EOB NOTE CODE B8";
	public static final String NF05_EOB_NOTE_CODE4 = "EOB NOTE CODE";
	public static final String NF05_GMIS_REASON_CODE4 = "GMIS REASON CODE";
	public static final String NF05_FEP_REASON_CODE4 = "FEP REASON CODE";
	public static final String NF05_EXPL_CODE5 = "EXPLAN CODE";
	public static final String NF05_DENIAL_REMARK_CODE5 = "DENIAL CODE";
	public static final String NF05_PENALTY_CODE5 = "EOB NOTE CODE B8";
	public static final String NF05_EOB_NOTE_CODE5 = "EOB NOTE CODE";
	public static final String NF05_GMIS_REASON_CODE5 = "GMIS REASON CODE";
	public static final String NF05_FEP_REASON_CODE5 = "FEP REASON CODE";
	private static final String[] NF05_BILL_PVDR_MAJOR_MEDICALS = new String[] { "0000005000", "0000005009" };
	public static final String NF05_BILL_PVDR_TEST_1_MIN = "0000016900";
	public static final String NF05_BILL_PVDR_TEST_1_MAX = "0000016949";
	public static final String NF05_BILL_PVDR_TEST_2_MIN = "0000016952";
	public static final String NF05_BILL_PVDR_TEST_2_MAX = "0000016999";
	public static final String NF05_BILL_PVDR_TEST_3_MIN = "0000900000";
	public static final String NF05_BILL_PVDR_TEST_3_MAX = "0000999999";
	public static final String NF05_BILL_PVDR_CROSS = "1";
	public static final String NF05_BILL_PVDR_SHIELD = "3";
	public static final String NF05_BILL_PVDR_MEDA_INPAT = "4";
	private static final char[] NF05_BILL_PVDR_STATUS_VALUES = new char[] { 'C', 'N', 'O' };
	public static final char NF05_BILL_PVDR_CONTRACTING = 'C';
	public static final char NF05_BILL_PVDR_NON_CONTRCT = 'N';
	public static final char NF05_BILL_PVDR_OTHER = 'P';

	//==== CONSTRUCTORS ====
	public Nf05LineLevelRecord() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.NF05_TOTAL_RECORD;
	}

	public void setFromServiceDateFormatted(String data) {
		writeString(Pos.FROM_SERVICE_DATE, data, Len.FROM_SERVICE_DATE);
	}

	public String getFromServiceDateFormatted() {
		return readFixedString(Pos.FROM_SERVICE_DATE, Len.FROM_SERVICE_DATE);
	}

	public void setFromServiceDateCc(String fromServiceDateCc) {
		writeString(Pos.FROM_SERVICE_DATE_CC, fromServiceDateCc, Len.FROM_SERVICE_DATE_CC);
	}

	/**Original name: NF05-FROM-SERVICE-DATE-CC<br>*/
	public String getFromServiceDateCc() {
		return readString(Pos.FROM_SERVICE_DATE_CC, Len.FROM_SERVICE_DATE_CC);
	}

	public void setFromServiceDateYy(String fromServiceDateYy) {
		writeString(Pos.FROM_SERVICE_DATE_YY, fromServiceDateYy, Len.FROM_SERVICE_DATE_YY);
	}

	/**Original name: NF05-FROM-SERVICE-DATE-YY<br>*/
	public String getFromServiceDateYy() {
		return readString(Pos.FROM_SERVICE_DATE_YY, Len.FROM_SERVICE_DATE_YY);
	}

	public void setFromServiceDateMm(String fromServiceDateMm) {
		writeString(Pos.FROM_SERVICE_DATE_MM, fromServiceDateMm, Len.FROM_SERVICE_DATE_MM);
	}

	/**Original name: NF05-FROM-SERVICE-DATE-MM<br>*/
	public String getFromServiceDateMm() {
		return readString(Pos.FROM_SERVICE_DATE_MM, Len.FROM_SERVICE_DATE_MM);
	}

	public void setFromServiceDateDd(String fromServiceDateDd) {
		writeString(Pos.FROM_SERVICE_DATE_DD, fromServiceDateDd, Len.FROM_SERVICE_DATE_DD);
	}

	/**Original name: NF05-FROM-SERVICE-DATE-DD<br>*/
	public String getFromServiceDateDd() {
		return readString(Pos.FROM_SERVICE_DATE_DD, Len.FROM_SERVICE_DATE_DD);
	}

	public void setThruServiceDateFormatted(String data) {
		writeString(Pos.THRU_SERVICE_DATE, data, Len.THRU_SERVICE_DATE);
	}

	public String getThruServiceDateFormatted() {
		return readFixedString(Pos.THRU_SERVICE_DATE, Len.THRU_SERVICE_DATE);
	}

	public void setThruServiceDateCc(String thruServiceDateCc) {
		writeString(Pos.THRU_SERVICE_DATE_CC, thruServiceDateCc, Len.THRU_SERVICE_DATE_CC);
	}

	/**Original name: NF05-THRU-SERVICE-DATE-CC<br>*/
	public String getThruServiceDateCc() {
		return readString(Pos.THRU_SERVICE_DATE_CC, Len.THRU_SERVICE_DATE_CC);
	}

	public void setThruServiceDateYy(String thruServiceDateYy) {
		writeString(Pos.THRU_SERVICE_DATE_YY, thruServiceDateYy, Len.THRU_SERVICE_DATE_YY);
	}

	/**Original name: NF05-THRU-SERVICE-DATE-YY<br>*/
	public String getThruServiceDateYy() {
		return readString(Pos.THRU_SERVICE_DATE_YY, Len.THRU_SERVICE_DATE_YY);
	}

	public void setThruServiceDateMm(String thruServiceDateMm) {
		writeString(Pos.THRU_SERVICE_DATE_MM, thruServiceDateMm, Len.THRU_SERVICE_DATE_MM);
	}

	/**Original name: NF05-THRU-SERVICE-DATE-MM<br>*/
	public String getThruServiceDateMm() {
		return readString(Pos.THRU_SERVICE_DATE_MM, Len.THRU_SERVICE_DATE_MM);
	}

	public void setThurServiceDateDd(String thurServiceDateDd) {
		writeString(Pos.THUR_SERVICE_DATE_DD, thurServiceDateDd, Len.THUR_SERVICE_DATE_DD);
	}

	/**Original name: NF05-THUR-SERVICE-DATE-DD<br>*/
	public String getThurServiceDateDd() {
		return readString(Pos.THUR_SERVICE_DATE_DD, Len.THUR_SERVICE_DATE_DD);
	}

	public void setExaminerAction(char examinerAction) {
		writeChar(Pos.EXAMINER_ACTION, examinerAction);
	}

	/**Original name: NF05-EXAMINER-ACTION<br>*/
	public char getExaminerAction() {
		return readChar(Pos.EXAMINER_ACTION);
	}

	public void setTypeService(String typeService) {
		writeString(Pos.TYPE_SERVICE, typeService, Len.TYPE_SERVICE);
	}

	/**Original name: NF05-TYPE-SERVICE<br>*/
	public String getTypeService() {
		return readString(Pos.TYPE_SERVICE, Len.TYPE_SERVICE);
	}

	public void setPlaceService(String placeService) {
		writeString(Pos.PLACE_SERVICE, placeService, Len.PLACE_SERVICE);
	}

	/**Original name: NF05-PLACE-SERVICE<br>*/
	public String getPlaceService() {
		return readString(Pos.PLACE_SERVICE, Len.PLACE_SERVICE);
	}

	public void setBenefitType(String benefitType) {
		writeString(Pos.BENEFIT_TYPE, benefitType, Len.BENEFIT_TYPE);
	}

	/**Original name: NF05-BENEFIT-TYPE<br>*/
	public String getBenefitType() {
		return readString(Pos.BENEFIT_TYPE, Len.BENEFIT_TYPE);
	}

	public void setDnlRemark(String dnlRemark) {
		writeString(Pos.DNL_REMARK, dnlRemark, Len.DNL_REMARK);
	}

	/**Original name: NF05-DNL-REMARK<br>*/
	public String getDnlRemark() {
		return readString(Pos.DNL_REMARK, Len.DNL_REMARK);
	}

	public void setRvwByCd(String rvwByCd) {
		writeString(Pos.RVW_BY_CD, rvwByCd, Len.RVW_BY_CD);
	}

	/**Original name: NF05-RVW-BY-CD<br>*/
	public String getRvwByCd() {
		return readString(Pos.RVW_BY_CD, Len.RVW_BY_CD);
	}

	public void setAdjdProvStatCd(String adjdProvStatCd) {
		writeString(Pos.ADJD_PROV_STAT_CD, adjdProvStatCd, Len.ADJD_PROV_STAT_CD);
	}

	/**Original name: NF05-ADJD-PROV-STAT-CD<br>*/
	public String getAdjdProvStatCd() {
		return readString(Pos.ADJD_PROV_STAT_CD, Len.ADJD_PROV_STAT_CD);
	}

	public void setLiAlwChgAm(AfDecimal liAlwChgAm) {
		writeDecimal(Pos.LI_ALW_CHG_AM, liAlwChgAm.copy(), SignType.NO_SIGN);
	}

	/**Original name: NF05-LI-ALW-CHG-AM<br>*/
	public AfDecimal getLiAlwChgAm() {
		return readDecimal(Pos.LI_ALW_CHG_AM, Len.Int.LI_ALW_CHG_AM, Len.Fract.LI_ALW_CHG_AM, SignType.NO_SIGN);
	}

	public void setIndusCd(String indusCd) {
		writeString(Pos.INDUS_CD, indusCd, Len.INDUS_CD);
	}

	/**Original name: NF05-INDUS-CD<br>*/
	public String getIndusCd() {
		return readString(Pos.INDUS_CD, Len.INDUS_CD);
	}

	public void setAmountCharged(AfDecimal amountCharged) {
		writeDecimal(Pos.AMOUNT_CHARGED, amountCharged.copy());
	}

	/**Original name: NF05-AMOUNT-CHARGED<br>*/
	public AfDecimal getAmountCharged() {
		return readDecimal(Pos.AMOUNT_CHARGED, Len.Int.AMOUNT_CHARGED, Len.Fract.AMOUNT_CHARGED);
	}

	public void setLineNotCovrdAmt(AfDecimal lineNotCovrdAmt) {
		writeDecimal(Pos.LINE_NOT_COVRD_AMT, lineNotCovrdAmt.copy());
	}

	/**Original name: NF05-LINE-NOT-COVRD-AMT<br>*/
	public AfDecimal getLineNotCovrdAmt() {
		return readDecimal(Pos.LINE_NOT_COVRD_AMT, Len.Int.LINE_NOT_COVRD_AMT, Len.Fract.LINE_NOT_COVRD_AMT);
	}

	public void setLineOthrInsur(AfDecimal lineOthrInsur) {
		writeDecimal(Pos.LINE_OTHR_INSUR, lineOthrInsur.copy(), SignType.NO_SIGN);
	}

	/**Original name: NF05-LINE-OTHR-INSUR<br>*/
	public AfDecimal getLineOthrInsur() {
		return readDecimal(Pos.LINE_OTHR_INSUR, Len.Int.LINE_OTHR_INSUR, Len.Fract.LINE_OTHR_INSUR, SignType.NO_SIGN);
	}

	public void setOtherInsureCovered(AfDecimal otherInsureCovered) {
		writeDecimal(Pos.OTHER_INSURE_COVERED, otherInsureCovered.copy());
	}

	/**Original name: NF05-OTHER-INSURE-COVERED<br>*/
	public AfDecimal getOtherInsureCovered() {
		return readDecimal(Pos.OTHER_INSURE_COVERED, Len.Int.OTHER_INSURE_COVERED, Len.Fract.OTHER_INSURE_COVERED);
	}

	public void setOtherInsurePaid(AfDecimal otherInsurePaid) {
		writeDecimal(Pos.OTHER_INSURE_PAID, otherInsurePaid.copy());
	}

	/**Original name: NF05-OTHER-INSURE-PAID<br>*/
	public AfDecimal getOtherInsurePaid() {
		return readDecimal(Pos.OTHER_INSURE_PAID, Len.Int.OTHER_INSURE_PAID, Len.Fract.OTHER_INSURE_PAID);
	}

	public void setOtherInsureSavings(AfDecimal otherInsureSavings) {
		writeDecimal(Pos.OTHER_INSURE_SAVINGS, otherInsureSavings.copy());
	}

	/**Original name: NF05-OTHER-INSURE-SAVINGS<br>*/
	public AfDecimal getOtherInsureSavings() {
		return readDecimal(Pos.OTHER_INSURE_SAVINGS, Len.Int.OTHER_INSURE_SAVINGS, Len.Fract.OTHER_INSURE_SAVINGS);
	}

	public void setCalc1AmountPaid(AfDecimal calc1AmountPaid) {
		writeDecimal(Pos.CALC1_AMOUNT_PAID, calc1AmountPaid.copy());
	}

	/**Original name: NF05-CALC1-AMOUNT-PAID<br>*/
	public AfDecimal getCalc1AmountPaid() {
		return readDecimal(Pos.CALC1_AMOUNT_PAID, Len.Int.CALC1_AMOUNT_PAID, Len.Fract.CALC1_AMOUNT_PAID);
	}

	public void setCalc1Deductible(AfDecimal calc1Deductible) {
		writeDecimal(Pos.CALC1_DEDUCTIBLE, calc1Deductible.copy());
	}

	/**Original name: NF05-CALC1-DEDUCTIBLE<br>*/
	public AfDecimal getCalc1Deductible() {
		return readDecimal(Pos.CALC1_DEDUCTIBLE, Len.Int.CALC1_DEDUCTIBLE, Len.Fract.CALC1_DEDUCTIBLE);
	}

	public void setCalc1Copay(AfDecimal calc1Copay) {
		writeDecimal(Pos.CALC1_COPAY, calc1Copay.copy());
	}

	/**Original name: NF05-CALC1-COPAY<br>*/
	public AfDecimal getCalc1Copay() {
		return readDecimal(Pos.CALC1_COPAY, Len.Int.CALC1_COPAY, Len.Fract.CALC1_COPAY);
	}

	public void setCalc1Coinsurance(AfDecimal calc1Coinsurance) {
		writeDecimal(Pos.CALC1_COINSURANCE, calc1Coinsurance.copy());
	}

	/**Original name: NF05-CALC1-COINSURANCE<br>*/
	public AfDecimal getCalc1Coinsurance() {
		return readDecimal(Pos.CALC1_COINSURANCE, Len.Int.CALC1_COINSURANCE, Len.Fract.CALC1_COINSURANCE);
	}

	public void setCalc1CorpCode(char calc1CorpCode) {
		writeChar(Pos.CALC1_CORP_CODE, calc1CorpCode);
	}

	/**Original name: NF05-CALC1-CORP-CODE<br>*/
	public char getCalc1CorpCode() {
		return readChar(Pos.CALC1_CORP_CODE);
	}

	public void setCalc1ProdCode(String calc1ProdCode) {
		writeString(Pos.CALC1_PROD_CODE, calc1ProdCode, Len.CALC1_PROD_CODE);
	}

	/**Original name: NF05-CALC1-PROD-CODE<br>*/
	public String getCalc1ProdCode() {
		return readString(Pos.CALC1_PROD_CODE, Len.CALC1_PROD_CODE);
	}

	public void setCalc1TypeBenefit(String calc1TypeBenefit) {
		writeString(Pos.CALC1_TYPE_BENEFIT, calc1TypeBenefit, Len.CALC1_TYPE_BENEFIT);
	}

	/**Original name: NF05-CALC1-TYPE-BENEFIT<br>*/
	public String getCalc1TypeBenefit() {
		return readString(Pos.CALC1_TYPE_BENEFIT, Len.CALC1_TYPE_BENEFIT);
	}

	public void setCalc1SpecBenefit(String calc1SpecBenefit) {
		writeString(Pos.CALC1_SPEC_BENEFIT, calc1SpecBenefit, Len.CALC1_SPEC_BENEFIT);
	}

	/**Original name: NF05-CALC1-SPEC-BENEFIT<br>*/
	public String getCalc1SpecBenefit() {
		return readString(Pos.CALC1_SPEC_BENEFIT, Len.CALC1_SPEC_BENEFIT);
	}

	public void setCalc1CovLob(char calc1CovLob) {
		writeChar(Pos.CALC1_COV_LOB, calc1CovLob);
	}

	/**Original name: NF05-CALC1-COV-LOB<br>*/
	public char getCalc1CovLob() {
		return readChar(Pos.CALC1_COV_LOB);
	}

	public void setCalc1PayCode(char calc1PayCode) {
		writeChar(Pos.CALC1_PAY_CODE, calc1PayCode);
	}

	/**Original name: NF05-CALC1-PAY-CODE<br>*/
	public char getCalc1PayCode() {
		return readChar(Pos.CALC1_PAY_CODE);
	}

	public void setCalc1TypeContract(String calc1TypeContract) {
		writeString(Pos.CALC1_TYPE_CONTRACT, calc1TypeContract, Len.CALC1_TYPE_CONTRACT);
	}

	/**Original name: NF05-CALC1-TYPE-CONTRACT<br>*/
	public String getCalc1TypeContract() {
		return readString(Pos.CALC1_TYPE_CONTRACT, Len.CALC1_TYPE_CONTRACT);
	}

	public void setCalc1ExplnCd(char calc1ExplnCd) {
		writeChar(Pos.CALC1_EXPLN_CD, calc1ExplnCd);
	}

	/**Original name: NF05-CALC1-EXPLN-CD<br>*/
	public char getCalc1ExplnCd() {
		return readChar(Pos.CALC1_EXPLN_CD);
	}

	public void setCalc2AmountPaid(AfDecimal calc2AmountPaid) {
		writeDecimal(Pos.CALC2_AMOUNT_PAID, calc2AmountPaid.copy());
	}

	/**Original name: NF05-CALC2-AMOUNT-PAID<br>*/
	public AfDecimal getCalc2AmountPaid() {
		return readDecimal(Pos.CALC2_AMOUNT_PAID, Len.Int.CALC2_AMOUNT_PAID, Len.Fract.CALC2_AMOUNT_PAID);
	}

	public void setCalc2Deductible(AfDecimal calc2Deductible) {
		writeDecimal(Pos.CALC2_DEDUCTIBLE, calc2Deductible.copy());
	}

	/**Original name: NF05-CALC2-DEDUCTIBLE<br>*/
	public AfDecimal getCalc2Deductible() {
		return readDecimal(Pos.CALC2_DEDUCTIBLE, Len.Int.CALC2_DEDUCTIBLE, Len.Fract.CALC2_DEDUCTIBLE);
	}

	public void setCalc2Copay(AfDecimal calc2Copay) {
		writeDecimal(Pos.CALC2_COPAY, calc2Copay.copy());
	}

	/**Original name: NF05-CALC2-COPAY<br>*/
	public AfDecimal getCalc2Copay() {
		return readDecimal(Pos.CALC2_COPAY, Len.Int.CALC2_COPAY, Len.Fract.CALC2_COPAY);
	}

	public void setCalc2Coinsurance(AfDecimal calc2Coinsurance) {
		writeDecimal(Pos.CALC2_COINSURANCE, calc2Coinsurance.copy());
	}

	/**Original name: NF05-CALC2-COINSURANCE<br>*/
	public AfDecimal getCalc2Coinsurance() {
		return readDecimal(Pos.CALC2_COINSURANCE, Len.Int.CALC2_COINSURANCE, Len.Fract.CALC2_COINSURANCE);
	}

	public void setCalc2CorpCode(char calc2CorpCode) {
		writeChar(Pos.CALC2_CORP_CODE, calc2CorpCode);
	}

	/**Original name: NF05-CALC2-CORP-CODE<br>*/
	public char getCalc2CorpCode() {
		return readChar(Pos.CALC2_CORP_CODE);
	}

	public void setCalc2ProdCode(String calc2ProdCode) {
		writeString(Pos.CALC2_PROD_CODE, calc2ProdCode, Len.CALC2_PROD_CODE);
	}

	/**Original name: NF05-CALC2-PROD-CODE<br>*/
	public String getCalc2ProdCode() {
		return readString(Pos.CALC2_PROD_CODE, Len.CALC2_PROD_CODE);
	}

	public void setCalc2TypeBenefit(String calc2TypeBenefit) {
		writeString(Pos.CALC2_TYPE_BENEFIT, calc2TypeBenefit, Len.CALC2_TYPE_BENEFIT);
	}

	/**Original name: NF05-CALC2-TYPE-BENEFIT<br>*/
	public String getCalc2TypeBenefit() {
		return readString(Pos.CALC2_TYPE_BENEFIT, Len.CALC2_TYPE_BENEFIT);
	}

	public void setCalc2SpecBenefit(String calc2SpecBenefit) {
		writeString(Pos.CALC2_SPEC_BENEFIT, calc2SpecBenefit, Len.CALC2_SPEC_BENEFIT);
	}

	/**Original name: NF05-CALC2-SPEC-BENEFIT<br>*/
	public String getCalc2SpecBenefit() {
		return readString(Pos.CALC2_SPEC_BENEFIT, Len.CALC2_SPEC_BENEFIT);
	}

	public void setCalc2CovLob(char calc2CovLob) {
		writeChar(Pos.CALC2_COV_LOB, calc2CovLob);
	}

	/**Original name: NF05-CALC2-COV-LOB<br>*/
	public char getCalc2CovLob() {
		return readChar(Pos.CALC2_COV_LOB);
	}

	public void setCalc2PayCode(char calc2PayCode) {
		writeChar(Pos.CALC2_PAY_CODE, calc2PayCode);
	}

	/**Original name: NF05-CALC2-PAY-CODE<br>*/
	public char getCalc2PayCode() {
		return readChar(Pos.CALC2_PAY_CODE);
	}

	public void setCalc2TypeContract(String calc2TypeContract) {
		writeString(Pos.CALC2_TYPE_CONTRACT, calc2TypeContract, Len.CALC2_TYPE_CONTRACT);
	}

	/**Original name: NF05-CALC2-TYPE-CONTRACT<br>*/
	public String getCalc2TypeContract() {
		return readString(Pos.CALC2_TYPE_CONTRACT, Len.CALC2_TYPE_CONTRACT);
	}

	public void setCalc2ExplnCd(char calc2ExplnCd) {
		writeChar(Pos.CALC2_EXPLN_CD, calc2ExplnCd);
	}

	/**Original name: NF05-CALC2-EXPLN-CD<br>*/
	public char getCalc2ExplnCd() {
		return readChar(Pos.CALC2_EXPLN_CD);
	}

	public void setCalc3AmountPaid(AfDecimal calc3AmountPaid) {
		writeDecimal(Pos.CALC3_AMOUNT_PAID, calc3AmountPaid.copy());
	}

	/**Original name: NF05-CALC3-AMOUNT-PAID<br>*/
	public AfDecimal getCalc3AmountPaid() {
		return readDecimal(Pos.CALC3_AMOUNT_PAID, Len.Int.CALC3_AMOUNT_PAID, Len.Fract.CALC3_AMOUNT_PAID);
	}

	public void setCalc3Deductible(AfDecimal calc3Deductible) {
		writeDecimal(Pos.CALC3_DEDUCTIBLE, calc3Deductible.copy());
	}

	/**Original name: NF05-CALC3-DEDUCTIBLE<br>*/
	public AfDecimal getCalc3Deductible() {
		return readDecimal(Pos.CALC3_DEDUCTIBLE, Len.Int.CALC3_DEDUCTIBLE, Len.Fract.CALC3_DEDUCTIBLE);
	}

	public void setCalc3Copay(AfDecimal calc3Copay) {
		writeDecimal(Pos.CALC3_COPAY, calc3Copay.copy());
	}

	/**Original name: NF05-CALC3-COPAY<br>*/
	public AfDecimal getCalc3Copay() {
		return readDecimal(Pos.CALC3_COPAY, Len.Int.CALC3_COPAY, Len.Fract.CALC3_COPAY);
	}

	public void setCalc3Coinsurance(AfDecimal calc3Coinsurance) {
		writeDecimal(Pos.CALC3_COINSURANCE, calc3Coinsurance.copy());
	}

	/**Original name: NF05-CALC3-COINSURANCE<br>*/
	public AfDecimal getCalc3Coinsurance() {
		return readDecimal(Pos.CALC3_COINSURANCE, Len.Int.CALC3_COINSURANCE, Len.Fract.CALC3_COINSURANCE);
	}

	public void setCalc3CorpCode(char calc3CorpCode) {
		writeChar(Pos.CALC3_CORP_CODE, calc3CorpCode);
	}

	/**Original name: NF05-CALC3-CORP-CODE<br>*/
	public char getCalc3CorpCode() {
		return readChar(Pos.CALC3_CORP_CODE);
	}

	public void setCalc3ProdCode(String calc3ProdCode) {
		writeString(Pos.CALC3_PROD_CODE, calc3ProdCode, Len.CALC3_PROD_CODE);
	}

	/**Original name: NF05-CALC3-PROD-CODE<br>*/
	public String getCalc3ProdCode() {
		return readString(Pos.CALC3_PROD_CODE, Len.CALC3_PROD_CODE);
	}

	public void setCalc3TypeBenefit(String calc3TypeBenefit) {
		writeString(Pos.CALC3_TYPE_BENEFIT, calc3TypeBenefit, Len.CALC3_TYPE_BENEFIT);
	}

	/**Original name: NF05-CALC3-TYPE-BENEFIT<br>*/
	public String getCalc3TypeBenefit() {
		return readString(Pos.CALC3_TYPE_BENEFIT, Len.CALC3_TYPE_BENEFIT);
	}

	public void setCalc3SpecBenefit(String calc3SpecBenefit) {
		writeString(Pos.CALC3_SPEC_BENEFIT, calc3SpecBenefit, Len.CALC3_SPEC_BENEFIT);
	}

	/**Original name: NF05-CALC3-SPEC-BENEFIT<br>*/
	public String getCalc3SpecBenefit() {
		return readString(Pos.CALC3_SPEC_BENEFIT, Len.CALC3_SPEC_BENEFIT);
	}

	public void setCalc3CovLob(char calc3CovLob) {
		writeChar(Pos.CALC3_COV_LOB, calc3CovLob);
	}

	/**Original name: NF05-CALC3-COV-LOB<br>*/
	public char getCalc3CovLob() {
		return readChar(Pos.CALC3_COV_LOB);
	}

	public void setCalc3PayCode(char calc3PayCode) {
		writeChar(Pos.CALC3_PAY_CODE, calc3PayCode);
	}

	/**Original name: NF05-CALC3-PAY-CODE<br>*/
	public char getCalc3PayCode() {
		return readChar(Pos.CALC3_PAY_CODE);
	}

	public void setCalc3TypeContract(String calc3TypeContract) {
		writeString(Pos.CALC3_TYPE_CONTRACT, calc3TypeContract, Len.CALC3_TYPE_CONTRACT);
	}

	/**Original name: NF05-CALC3-TYPE-CONTRACT<br>*/
	public String getCalc3TypeContract() {
		return readString(Pos.CALC3_TYPE_CONTRACT, Len.CALC3_TYPE_CONTRACT);
	}

	public void setCalc3ExplnCd(char calc3ExplnCd) {
		writeChar(Pos.CALC3_EXPLN_CD, calc3ExplnCd);
	}

	/**Original name: NF05-CALC3-EXPLN-CD<br>*/
	public char getCalc3ExplnCd() {
		return readChar(Pos.CALC3_EXPLN_CD);
	}

	public void setCalc4AmountPaid(AfDecimal calc4AmountPaid) {
		writeDecimal(Pos.CALC4_AMOUNT_PAID, calc4AmountPaid.copy());
	}

	/**Original name: NF05-CALC4-AMOUNT-PAID<br>*/
	public AfDecimal getCalc4AmountPaid() {
		return readDecimal(Pos.CALC4_AMOUNT_PAID, Len.Int.CALC4_AMOUNT_PAID, Len.Fract.CALC4_AMOUNT_PAID);
	}

	public void setCalc4Deductible(AfDecimal calc4Deductible) {
		writeDecimal(Pos.CALC4_DEDUCTIBLE, calc4Deductible.copy());
	}

	/**Original name: NF05-CALC4-DEDUCTIBLE<br>*/
	public AfDecimal getCalc4Deductible() {
		return readDecimal(Pos.CALC4_DEDUCTIBLE, Len.Int.CALC4_DEDUCTIBLE, Len.Fract.CALC4_DEDUCTIBLE);
	}

	public void setCalc4Copay(AfDecimal calc4Copay) {
		writeDecimal(Pos.CALC4_COPAY, calc4Copay.copy());
	}

	/**Original name: NF05-CALC4-COPAY<br>*/
	public AfDecimal getCalc4Copay() {
		return readDecimal(Pos.CALC4_COPAY, Len.Int.CALC4_COPAY, Len.Fract.CALC4_COPAY);
	}

	public void setCalc4Coinsurance(AfDecimal calc4Coinsurance) {
		writeDecimal(Pos.CALC4_COINSURANCE, calc4Coinsurance.copy());
	}

	/**Original name: NF05-CALC4-COINSURANCE<br>*/
	public AfDecimal getCalc4Coinsurance() {
		return readDecimal(Pos.CALC4_COINSURANCE, Len.Int.CALC4_COINSURANCE, Len.Fract.CALC4_COINSURANCE);
	}

	public void setCalc4CorpCode(char calc4CorpCode) {
		writeChar(Pos.CALC4_CORP_CODE, calc4CorpCode);
	}

	/**Original name: NF05-CALC4-CORP-CODE<br>*/
	public char getCalc4CorpCode() {
		return readChar(Pos.CALC4_CORP_CODE);
	}

	public void setCalc4ProdCode(String calc4ProdCode) {
		writeString(Pos.CALC4_PROD_CODE, calc4ProdCode, Len.CALC4_PROD_CODE);
	}

	/**Original name: NF05-CALC4-PROD-CODE<br>*/
	public String getCalc4ProdCode() {
		return readString(Pos.CALC4_PROD_CODE, Len.CALC4_PROD_CODE);
	}

	public void setCalc4TypeBenefit(String calc4TypeBenefit) {
		writeString(Pos.CALC4_TYPE_BENEFIT, calc4TypeBenefit, Len.CALC4_TYPE_BENEFIT);
	}

	/**Original name: NF05-CALC4-TYPE-BENEFIT<br>*/
	public String getCalc4TypeBenefit() {
		return readString(Pos.CALC4_TYPE_BENEFIT, Len.CALC4_TYPE_BENEFIT);
	}

	public void setCalc4SpecBenefit(String calc4SpecBenefit) {
		writeString(Pos.CALC4_SPEC_BENEFIT, calc4SpecBenefit, Len.CALC4_SPEC_BENEFIT);
	}

	/**Original name: NF05-CALC4-SPEC-BENEFIT<br>*/
	public String getCalc4SpecBenefit() {
		return readString(Pos.CALC4_SPEC_BENEFIT, Len.CALC4_SPEC_BENEFIT);
	}

	public void setCalc4CovLob(char calc4CovLob) {
		writeChar(Pos.CALC4_COV_LOB, calc4CovLob);
	}

	/**Original name: NF05-CALC4-COV-LOB<br>*/
	public char getCalc4CovLob() {
		return readChar(Pos.CALC4_COV_LOB);
	}

	public void setCalc4PayCode(char calc4PayCode) {
		writeChar(Pos.CALC4_PAY_CODE, calc4PayCode);
	}

	/**Original name: NF05-CALC4-PAY-CODE<br>*/
	public char getCalc4PayCode() {
		return readChar(Pos.CALC4_PAY_CODE);
	}

	public void setCalc4TypeContract(String calc4TypeContract) {
		writeString(Pos.CALC4_TYPE_CONTRACT, calc4TypeContract, Len.CALC4_TYPE_CONTRACT);
	}

	/**Original name: NF05-CALC4-TYPE-CONTRACT<br>*/
	public String getCalc4TypeContract() {
		return readString(Pos.CALC4_TYPE_CONTRACT, Len.CALC4_TYPE_CONTRACT);
	}

	public void setCalc4ExplnCd(char calc4ExplnCd) {
		writeChar(Pos.CALC4_EXPLN_CD, calc4ExplnCd);
	}

	/**Original name: NF05-CALC4-EXPLN-CD<br>*/
	public char getCalc4ExplnCd() {
		return readChar(Pos.CALC4_EXPLN_CD);
	}

	public void setCalc5AmountPaid(AfDecimal calc5AmountPaid) {
		writeDecimal(Pos.CALC5_AMOUNT_PAID, calc5AmountPaid.copy());
	}

	/**Original name: NF05-CALC5-AMOUNT-PAID<br>*/
	public AfDecimal getCalc5AmountPaid() {
		return readDecimal(Pos.CALC5_AMOUNT_PAID, Len.Int.CALC5_AMOUNT_PAID, Len.Fract.CALC5_AMOUNT_PAID);
	}

	public void setCalc5Deductible(AfDecimal calc5Deductible) {
		writeDecimal(Pos.CALC5_DEDUCTIBLE, calc5Deductible.copy());
	}

	/**Original name: NF05-CALC5-DEDUCTIBLE<br>*/
	public AfDecimal getCalc5Deductible() {
		return readDecimal(Pos.CALC5_DEDUCTIBLE, Len.Int.CALC5_DEDUCTIBLE, Len.Fract.CALC5_DEDUCTIBLE);
	}

	public void setCalc5Copay(AfDecimal calc5Copay) {
		writeDecimal(Pos.CALC5_COPAY, calc5Copay.copy());
	}

	/**Original name: NF05-CALC5-COPAY<br>*/
	public AfDecimal getCalc5Copay() {
		return readDecimal(Pos.CALC5_COPAY, Len.Int.CALC5_COPAY, Len.Fract.CALC5_COPAY);
	}

	public void setCalc5Coinsurance(AfDecimal calc5Coinsurance) {
		writeDecimal(Pos.CALC5_COINSURANCE, calc5Coinsurance.copy());
	}

	/**Original name: NF05-CALC5-COINSURANCE<br>*/
	public AfDecimal getCalc5Coinsurance() {
		return readDecimal(Pos.CALC5_COINSURANCE, Len.Int.CALC5_COINSURANCE, Len.Fract.CALC5_COINSURANCE);
	}

	public void setCalc5CorpCode(char calc5CorpCode) {
		writeChar(Pos.CALC5_CORP_CODE, calc5CorpCode);
	}

	/**Original name: NF05-CALC5-CORP-CODE<br>*/
	public char getCalc5CorpCode() {
		return readChar(Pos.CALC5_CORP_CODE);
	}

	public void setCalc5ProdCode(String calc5ProdCode) {
		writeString(Pos.CALC5_PROD_CODE, calc5ProdCode, Len.CALC5_PROD_CODE);
	}

	/**Original name: NF05-CALC5-PROD-CODE<br>*/
	public String getCalc5ProdCode() {
		return readString(Pos.CALC5_PROD_CODE, Len.CALC5_PROD_CODE);
	}

	public void setCalc5TypeBenefit(String calc5TypeBenefit) {
		writeString(Pos.CALC5_TYPE_BENEFIT, calc5TypeBenefit, Len.CALC5_TYPE_BENEFIT);
	}

	/**Original name: NF05-CALC5-TYPE-BENEFIT<br>*/
	public String getCalc5TypeBenefit() {
		return readString(Pos.CALC5_TYPE_BENEFIT, Len.CALC5_TYPE_BENEFIT);
	}

	public void setCalc5SpecBenefit(String calc5SpecBenefit) {
		writeString(Pos.CALC5_SPEC_BENEFIT, calc5SpecBenefit, Len.CALC5_SPEC_BENEFIT);
	}

	/**Original name: NF05-CALC5-SPEC-BENEFIT<br>*/
	public String getCalc5SpecBenefit() {
		return readString(Pos.CALC5_SPEC_BENEFIT, Len.CALC5_SPEC_BENEFIT);
	}

	public void setCalc5CovLob(char calc5CovLob) {
		writeChar(Pos.CALC5_COV_LOB, calc5CovLob);
	}

	/**Original name: NF05-CALC5-COV-LOB<br>*/
	public char getCalc5CovLob() {
		return readChar(Pos.CALC5_COV_LOB);
	}

	public void setCalc5PayCode(char calc5PayCode) {
		writeChar(Pos.CALC5_PAY_CODE, calc5PayCode);
	}

	/**Original name: NF05-CALC5-PAY-CODE<br>*/
	public char getCalc5PayCode() {
		return readChar(Pos.CALC5_PAY_CODE);
	}

	public void setCalc5TypeContract(String calc5TypeContract) {
		writeString(Pos.CALC5_TYPE_CONTRACT, calc5TypeContract, Len.CALC5_TYPE_CONTRACT);
	}

	/**Original name: NF05-CALC5-TYPE-CONTRACT<br>*/
	public String getCalc5TypeContract() {
		return readString(Pos.CALC5_TYPE_CONTRACT, Len.CALC5_TYPE_CONTRACT);
	}

	public void setCalc5ExplnCd(char calc5ExplnCd) {
		writeChar(Pos.CALC5_EXPLN_CD, calc5ExplnCd);
	}

	/**Original name: NF05-CALC5-EXPLN-CD<br>*/
	public char getCalc5ExplnCd() {
		return readChar(Pos.CALC5_EXPLN_CD);
	}

	public void setCalc6AmountPaid(AfDecimal calc6AmountPaid) {
		writeDecimal(Pos.CALC6_AMOUNT_PAID, calc6AmountPaid.copy());
	}

	/**Original name: NF05-CALC6-AMOUNT-PAID<br>*/
	public AfDecimal getCalc6AmountPaid() {
		return readDecimal(Pos.CALC6_AMOUNT_PAID, Len.Int.CALC6_AMOUNT_PAID, Len.Fract.CALC6_AMOUNT_PAID);
	}

	public void setCalc6Deductible(AfDecimal calc6Deductible) {
		writeDecimal(Pos.CALC6_DEDUCTIBLE, calc6Deductible.copy());
	}

	/**Original name: NF05-CALC6-DEDUCTIBLE<br>*/
	public AfDecimal getCalc6Deductible() {
		return readDecimal(Pos.CALC6_DEDUCTIBLE, Len.Int.CALC6_DEDUCTIBLE, Len.Fract.CALC6_DEDUCTIBLE);
	}

	public void setCalc6Copay(AfDecimal calc6Copay) {
		writeDecimal(Pos.CALC6_COPAY, calc6Copay.copy());
	}

	/**Original name: NF05-CALC6-COPAY<br>*/
	public AfDecimal getCalc6Copay() {
		return readDecimal(Pos.CALC6_COPAY, Len.Int.CALC6_COPAY, Len.Fract.CALC6_COPAY);
	}

	public void setCalc6Coinsurance(AfDecimal calc6Coinsurance) {
		writeDecimal(Pos.CALC6_COINSURANCE, calc6Coinsurance.copy());
	}

	/**Original name: NF05-CALC6-COINSURANCE<br>*/
	public AfDecimal getCalc6Coinsurance() {
		return readDecimal(Pos.CALC6_COINSURANCE, Len.Int.CALC6_COINSURANCE, Len.Fract.CALC6_COINSURANCE);
	}

	public void setCalc6CorpCode(char calc6CorpCode) {
		writeChar(Pos.CALC6_CORP_CODE, calc6CorpCode);
	}

	/**Original name: NF05-CALC6-CORP-CODE<br>*/
	public char getCalc6CorpCode() {
		return readChar(Pos.CALC6_CORP_CODE);
	}

	public void setCalc6ProdCode(String calc6ProdCode) {
		writeString(Pos.CALC6_PROD_CODE, calc6ProdCode, Len.CALC6_PROD_CODE);
	}

	/**Original name: NF05-CALC6-PROD-CODE<br>*/
	public String getCalc6ProdCode() {
		return readString(Pos.CALC6_PROD_CODE, Len.CALC6_PROD_CODE);
	}

	public void setCalc6TypeBenefit(String calc6TypeBenefit) {
		writeString(Pos.CALC6_TYPE_BENEFIT, calc6TypeBenefit, Len.CALC6_TYPE_BENEFIT);
	}

	/**Original name: NF05-CALC6-TYPE-BENEFIT<br>*/
	public String getCalc6TypeBenefit() {
		return readString(Pos.CALC6_TYPE_BENEFIT, Len.CALC6_TYPE_BENEFIT);
	}

	public void setCalc6SpecBenefit(String calc6SpecBenefit) {
		writeString(Pos.CALC6_SPEC_BENEFIT, calc6SpecBenefit, Len.CALC6_SPEC_BENEFIT);
	}

	/**Original name: NF05-CALC6-SPEC-BENEFIT<br>*/
	public String getCalc6SpecBenefit() {
		return readString(Pos.CALC6_SPEC_BENEFIT, Len.CALC6_SPEC_BENEFIT);
	}

	public void setCalc6CovLob(char calc6CovLob) {
		writeChar(Pos.CALC6_COV_LOB, calc6CovLob);
	}

	/**Original name: NF05-CALC6-COV-LOB<br>*/
	public char getCalc6CovLob() {
		return readChar(Pos.CALC6_COV_LOB);
	}

	public void setCalc6PayCode(char calc6PayCode) {
		writeChar(Pos.CALC6_PAY_CODE, calc6PayCode);
	}

	/**Original name: NF05-CALC6-PAY-CODE<br>*/
	public char getCalc6PayCode() {
		return readChar(Pos.CALC6_PAY_CODE);
	}

	public void setCalc6TypeContract(String calc6TypeContract) {
		writeString(Pos.CALC6_TYPE_CONTRACT, calc6TypeContract, Len.CALC6_TYPE_CONTRACT);
	}

	/**Original name: NF05-CALC6-TYPE-CONTRACT<br>*/
	public String getCalc6TypeContract() {
		return readString(Pos.CALC6_TYPE_CONTRACT, Len.CALC6_TYPE_CONTRACT);
	}

	public void setCalc6ExplnCd(char calc6ExplnCd) {
		writeChar(Pos.CALC6_EXPLN_CD, calc6ExplnCd);
	}

	/**Original name: NF05-CALC6-EXPLN-CD<br>*/
	public char getCalc6ExplnCd() {
		return readChar(Pos.CALC6_EXPLN_CD);
	}

	public void setCalc7AmountPaid(AfDecimal calc7AmountPaid) {
		writeDecimal(Pos.CALC7_AMOUNT_PAID, calc7AmountPaid.copy());
	}

	/**Original name: NF05-CALC7-AMOUNT-PAID<br>*/
	public AfDecimal getCalc7AmountPaid() {
		return readDecimal(Pos.CALC7_AMOUNT_PAID, Len.Int.CALC7_AMOUNT_PAID, Len.Fract.CALC7_AMOUNT_PAID);
	}

	public void setCalc7Deductible(AfDecimal calc7Deductible) {
		writeDecimal(Pos.CALC7_DEDUCTIBLE, calc7Deductible.copy());
	}

	/**Original name: NF05-CALC7-DEDUCTIBLE<br>*/
	public AfDecimal getCalc7Deductible() {
		return readDecimal(Pos.CALC7_DEDUCTIBLE, Len.Int.CALC7_DEDUCTIBLE, Len.Fract.CALC7_DEDUCTIBLE);
	}

	public void setCalc7Copay(AfDecimal calc7Copay) {
		writeDecimal(Pos.CALC7_COPAY, calc7Copay.copy());
	}

	/**Original name: NF05-CALC7-COPAY<br>*/
	public AfDecimal getCalc7Copay() {
		return readDecimal(Pos.CALC7_COPAY, Len.Int.CALC7_COPAY, Len.Fract.CALC7_COPAY);
	}

	public void setCalc7Coinsurance(AfDecimal calc7Coinsurance) {
		writeDecimal(Pos.CALC7_COINSURANCE, calc7Coinsurance.copy());
	}

	/**Original name: NF05-CALC7-COINSURANCE<br>*/
	public AfDecimal getCalc7Coinsurance() {
		return readDecimal(Pos.CALC7_COINSURANCE, Len.Int.CALC7_COINSURANCE, Len.Fract.CALC7_COINSURANCE);
	}

	public void setCalc7CorpCode(char calc7CorpCode) {
		writeChar(Pos.CALC7_CORP_CODE, calc7CorpCode);
	}

	/**Original name: NF05-CALC7-CORP-CODE<br>*/
	public char getCalc7CorpCode() {
		return readChar(Pos.CALC7_CORP_CODE);
	}

	public void setCalc7ProdCode(String calc7ProdCode) {
		writeString(Pos.CALC7_PROD_CODE, calc7ProdCode, Len.CALC7_PROD_CODE);
	}

	/**Original name: NF05-CALC7-PROD-CODE<br>*/
	public String getCalc7ProdCode() {
		return readString(Pos.CALC7_PROD_CODE, Len.CALC7_PROD_CODE);
	}

	public void setCalc7TypeBenefit(String calc7TypeBenefit) {
		writeString(Pos.CALC7_TYPE_BENEFIT, calc7TypeBenefit, Len.CALC7_TYPE_BENEFIT);
	}

	/**Original name: NF05-CALC7-TYPE-BENEFIT<br>*/
	public String getCalc7TypeBenefit() {
		return readString(Pos.CALC7_TYPE_BENEFIT, Len.CALC7_TYPE_BENEFIT);
	}

	public void setCalc7SpecBenefit(String calc7SpecBenefit) {
		writeString(Pos.CALC7_SPEC_BENEFIT, calc7SpecBenefit, Len.CALC7_SPEC_BENEFIT);
	}

	/**Original name: NF05-CALC7-SPEC-BENEFIT<br>*/
	public String getCalc7SpecBenefit() {
		return readString(Pos.CALC7_SPEC_BENEFIT, Len.CALC7_SPEC_BENEFIT);
	}

	public void setCalc7CovLob(char calc7CovLob) {
		writeChar(Pos.CALC7_COV_LOB, calc7CovLob);
	}

	/**Original name: NF05-CALC7-COV-LOB<br>*/
	public char getCalc7CovLob() {
		return readChar(Pos.CALC7_COV_LOB);
	}

	public void setCalc7PayCode(char calc7PayCode) {
		writeChar(Pos.CALC7_PAY_CODE, calc7PayCode);
	}

	/**Original name: NF05-CALC7-PAY-CODE<br>*/
	public char getCalc7PayCode() {
		return readChar(Pos.CALC7_PAY_CODE);
	}

	public void setCalc7TypeContract(String calc7TypeContract) {
		writeString(Pos.CALC7_TYPE_CONTRACT, calc7TypeContract, Len.CALC7_TYPE_CONTRACT);
	}

	/**Original name: NF05-CALC7-TYPE-CONTRACT<br>*/
	public String getCalc7TypeContract() {
		return readString(Pos.CALC7_TYPE_CONTRACT, Len.CALC7_TYPE_CONTRACT);
	}

	public void setCalc7ExplnCd(char calc7ExplnCd) {
		writeChar(Pos.CALC7_EXPLN_CD, calc7ExplnCd);
	}

	/**Original name: NF05-CALC7-EXPLN-CD<br>*/
	public char getCalc7ExplnCd() {
		return readChar(Pos.CALC7_EXPLN_CD);
	}

	public void setCalc8AmountPaid(AfDecimal calc8AmountPaid) {
		writeDecimal(Pos.CALC8_AMOUNT_PAID, calc8AmountPaid.copy());
	}

	/**Original name: NF05-CALC8-AMOUNT-PAID<br>*/
	public AfDecimal getCalc8AmountPaid() {
		return readDecimal(Pos.CALC8_AMOUNT_PAID, Len.Int.CALC8_AMOUNT_PAID, Len.Fract.CALC8_AMOUNT_PAID);
	}

	public void setCalc8Deductible(AfDecimal calc8Deductible) {
		writeDecimal(Pos.CALC8_DEDUCTIBLE, calc8Deductible.copy());
	}

	/**Original name: NF05-CALC8-DEDUCTIBLE<br>*/
	public AfDecimal getCalc8Deductible() {
		return readDecimal(Pos.CALC8_DEDUCTIBLE, Len.Int.CALC8_DEDUCTIBLE, Len.Fract.CALC8_DEDUCTIBLE);
	}

	public void setCalc8Copay(AfDecimal calc8Copay) {
		writeDecimal(Pos.CALC8_COPAY, calc8Copay.copy());
	}

	/**Original name: NF05-CALC8-COPAY<br>*/
	public AfDecimal getCalc8Copay() {
		return readDecimal(Pos.CALC8_COPAY, Len.Int.CALC8_COPAY, Len.Fract.CALC8_COPAY);
	}

	public void setCalc8Coinsurance(AfDecimal calc8Coinsurance) {
		writeDecimal(Pos.CALC8_COINSURANCE, calc8Coinsurance.copy());
	}

	/**Original name: NF05-CALC8-COINSURANCE<br>*/
	public AfDecimal getCalc8Coinsurance() {
		return readDecimal(Pos.CALC8_COINSURANCE, Len.Int.CALC8_COINSURANCE, Len.Fract.CALC8_COINSURANCE);
	}

	public void setCalc8CorpCode(char calc8CorpCode) {
		writeChar(Pos.CALC8_CORP_CODE, calc8CorpCode);
	}

	/**Original name: NF05-CALC8-CORP-CODE<br>*/
	public char getCalc8CorpCode() {
		return readChar(Pos.CALC8_CORP_CODE);
	}

	public void setCalc8ProdCode(String calc8ProdCode) {
		writeString(Pos.CALC8_PROD_CODE, calc8ProdCode, Len.CALC8_PROD_CODE);
	}

	/**Original name: NF05-CALC8-PROD-CODE<br>*/
	public String getCalc8ProdCode() {
		return readString(Pos.CALC8_PROD_CODE, Len.CALC8_PROD_CODE);
	}

	public void setCalc8TypeBenefit(String calc8TypeBenefit) {
		writeString(Pos.CALC8_TYPE_BENEFIT, calc8TypeBenefit, Len.CALC8_TYPE_BENEFIT);
	}

	/**Original name: NF05-CALC8-TYPE-BENEFIT<br>*/
	public String getCalc8TypeBenefit() {
		return readString(Pos.CALC8_TYPE_BENEFIT, Len.CALC8_TYPE_BENEFIT);
	}

	public void setCalc8SpecBenefit(String calc8SpecBenefit) {
		writeString(Pos.CALC8_SPEC_BENEFIT, calc8SpecBenefit, Len.CALC8_SPEC_BENEFIT);
	}

	/**Original name: NF05-CALC8-SPEC-BENEFIT<br>*/
	public String getCalc8SpecBenefit() {
		return readString(Pos.CALC8_SPEC_BENEFIT, Len.CALC8_SPEC_BENEFIT);
	}

	public void setCalc8CovLob(char calc8CovLob) {
		writeChar(Pos.CALC8_COV_LOB, calc8CovLob);
	}

	/**Original name: NF05-CALC8-COV-LOB<br>*/
	public char getCalc8CovLob() {
		return readChar(Pos.CALC8_COV_LOB);
	}

	public void setCalc8PayCode(char calc8PayCode) {
		writeChar(Pos.CALC8_PAY_CODE, calc8PayCode);
	}

	/**Original name: NF05-CALC8-PAY-CODE<br>*/
	public char getCalc8PayCode() {
		return readChar(Pos.CALC8_PAY_CODE);
	}

	public void setCalc8TypeContract(String calc8TypeContract) {
		writeString(Pos.CALC8_TYPE_CONTRACT, calc8TypeContract, Len.CALC8_TYPE_CONTRACT);
	}

	/**Original name: NF05-CALC8-TYPE-CONTRACT<br>*/
	public String getCalc8TypeContract() {
		return readString(Pos.CALC8_TYPE_CONTRACT, Len.CALC8_TYPE_CONTRACT);
	}

	public void setCalc8ExplnCd(char calc8ExplnCd) {
		writeChar(Pos.CALC8_EXPLN_CD, calc8ExplnCd);
	}

	/**Original name: NF05-CALC8-EXPLN-CD<br>*/
	public char getCalc8ExplnCd() {
		return readChar(Pos.CALC8_EXPLN_CD);
	}

	public void setCalc9AmountPaid(AfDecimal calc9AmountPaid) {
		writeDecimal(Pos.CALC9_AMOUNT_PAID, calc9AmountPaid.copy());
	}

	/**Original name: NF05-CALC9-AMOUNT-PAID<br>*/
	public AfDecimal getCalc9AmountPaid() {
		return readDecimal(Pos.CALC9_AMOUNT_PAID, Len.Int.CALC9_AMOUNT_PAID, Len.Fract.CALC9_AMOUNT_PAID);
	}

	public void setCalc9Deductible(AfDecimal calc9Deductible) {
		writeDecimal(Pos.CALC9_DEDUCTIBLE, calc9Deductible.copy());
	}

	/**Original name: NF05-CALC9-DEDUCTIBLE<br>*/
	public AfDecimal getCalc9Deductible() {
		return readDecimal(Pos.CALC9_DEDUCTIBLE, Len.Int.CALC9_DEDUCTIBLE, Len.Fract.CALC9_DEDUCTIBLE);
	}

	public void setCalc9Copay(AfDecimal calc9Copay) {
		writeDecimal(Pos.CALC9_COPAY, calc9Copay.copy());
	}

	/**Original name: NF05-CALC9-COPAY<br>*/
	public AfDecimal getCalc9Copay() {
		return readDecimal(Pos.CALC9_COPAY, Len.Int.CALC9_COPAY, Len.Fract.CALC9_COPAY);
	}

	public void setCalc9Coinsurance(AfDecimal calc9Coinsurance) {
		writeDecimal(Pos.CALC9_COINSURANCE, calc9Coinsurance.copy());
	}

	/**Original name: NF05-CALC9-COINSURANCE<br>*/
	public AfDecimal getCalc9Coinsurance() {
		return readDecimal(Pos.CALC9_COINSURANCE, Len.Int.CALC9_COINSURANCE, Len.Fract.CALC9_COINSURANCE);
	}

	public void setCalc9CorpCode(char calc9CorpCode) {
		writeChar(Pos.CALC9_CORP_CODE, calc9CorpCode);
	}

	/**Original name: NF05-CALC9-CORP-CODE<br>*/
	public char getCalc9CorpCode() {
		return readChar(Pos.CALC9_CORP_CODE);
	}

	public void setCalc9ProdCode(String calc9ProdCode) {
		writeString(Pos.CALC9_PROD_CODE, calc9ProdCode, Len.CALC9_PROD_CODE);
	}

	/**Original name: NF05-CALC9-PROD-CODE<br>*/
	public String getCalc9ProdCode() {
		return readString(Pos.CALC9_PROD_CODE, Len.CALC9_PROD_CODE);
	}

	public void setCalc9TypeBenefit(String calc9TypeBenefit) {
		writeString(Pos.CALC9_TYPE_BENEFIT, calc9TypeBenefit, Len.CALC9_TYPE_BENEFIT);
	}

	/**Original name: NF05-CALC9-TYPE-BENEFIT<br>*/
	public String getCalc9TypeBenefit() {
		return readString(Pos.CALC9_TYPE_BENEFIT, Len.CALC9_TYPE_BENEFIT);
	}

	public void setCalc9SpecBenefit(String calc9SpecBenefit) {
		writeString(Pos.CALC9_SPEC_BENEFIT, calc9SpecBenefit, Len.CALC9_SPEC_BENEFIT);
	}

	/**Original name: NF05-CALC9-SPEC-BENEFIT<br>*/
	public String getCalc9SpecBenefit() {
		return readString(Pos.CALC9_SPEC_BENEFIT, Len.CALC9_SPEC_BENEFIT);
	}

	public void setCalc9CovLob(char calc9CovLob) {
		writeChar(Pos.CALC9_COV_LOB, calc9CovLob);
	}

	/**Original name: NF05-CALC9-COV-LOB<br>*/
	public char getCalc9CovLob() {
		return readChar(Pos.CALC9_COV_LOB);
	}

	public void setCalc9PayCode(char calc9PayCode) {
		writeChar(Pos.CALC9_PAY_CODE, calc9PayCode);
	}

	/**Original name: NF05-CALC9-PAY-CODE<br>*/
	public char getCalc9PayCode() {
		return readChar(Pos.CALC9_PAY_CODE);
	}

	public void setCalc9TypeContract(String calc9TypeContract) {
		writeString(Pos.CALC9_TYPE_CONTRACT, calc9TypeContract, Len.CALC9_TYPE_CONTRACT);
	}

	/**Original name: NF05-CALC9-TYPE-CONTRACT<br>*/
	public String getCalc9TypeContract() {
		return readString(Pos.CALC9_TYPE_CONTRACT, Len.CALC9_TYPE_CONTRACT);
	}

	public void setCalc9ExplnCd(char calc9ExplnCd) {
		writeChar(Pos.CALC9_EXPLN_CD, calc9ExplnCd);
	}

	/**Original name: NF05-CALC9-EXPLN-CD<br>*/
	public char getCalc9ExplnCd() {
		return readChar(Pos.CALC9_EXPLN_CD);
	}

	public void setDeductCarryover(char deductCarryover) {
		writeChar(Pos.DEDUCT_CARRYOVER, deductCarryover);
	}

	/**Original name: NF05-DEDUCT-CARRYOVER<br>*/
	public char getDeductCarryover() {
		return readChar(Pos.DEDUCT_CARRYOVER);
	}

	public void setFepDentPpoPrvWrtoff(AfDecimal fepDentPpoPrvWrtoff) {
		writeDecimal(Pos.FEP_DENT_PPO_PRV_WRTOFF, fepDentPpoPrvWrtoff.copy(), SignType.NO_SIGN);
	}

	/**Original name: NF05-FEP-DENT-PPO-PRV-WRTOFF<br>*/
	public AfDecimal getFepDentPpoPrvWrtoff() {
		return readDecimal(Pos.FEP_DENT_PPO_PRV_WRTOFF, Len.Int.FEP_DENT_PPO_PRV_WRTOFF, Len.Fract.FEP_DENT_PPO_PRV_WRTOFF, SignType.NO_SIGN);
	}

	public void setFepDentPpoInsLiab(AfDecimal fepDentPpoInsLiab) {
		writeDecimal(Pos.FEP_DENT_PPO_INS_LIAB, fepDentPpoInsLiab.copy(), SignType.NO_SIGN);
	}

	/**Original name: NF05-FEP-DENT-PPO-INS-LIAB<br>*/
	public AfDecimal getFepDentPpoInsLiab() {
		return readDecimal(Pos.FEP_DENT_PPO_INS_LIAB, Len.Int.FEP_DENT_PPO_INS_LIAB, Len.Fract.FEP_DENT_PPO_INS_LIAB, SignType.NO_SIGN);
	}

	public void setLineExplanationCode(char lineExplanationCode) {
		writeChar(Pos.LINE_EXPLANATION_CODE, lineExplanationCode);
	}

	/**Original name: NF05-LINE-EXPLANATION-CODE<br>*/
	public char getLineExplanationCode() {
		return readChar(Pos.LINE_EXPLANATION_CODE);
	}

	public void setSelfReferralIndicator(char selfReferralIndicator) {
		writeChar(Pos.SELF_REFERRAL_INDICATOR, selfReferralIndicator);
	}

	/**Original name: NF05-SELF-REFERRAL-INDICATOR<br>*/
	public char getSelfReferralIndicator() {
		return readChar(Pos.SELF_REFERRAL_INDICATOR);
	}

	public void setNotes1(String notes1) {
		writeString(Pos.NOTES1, notes1, Len.NOTES1);
	}

	/**Original name: NF05-NOTES-1<br>*/
	public String getNotes1() {
		return readString(Pos.NOTES1, Len.NOTES1);
	}

	public void setWhichNotesTableNote1(String whichNotesTableNote1) {
		writeString(Pos.WHICH_NOTES_TABLE_NOTE1, whichNotesTableNote1, Len.WHICH_NOTES_TABLE_NOTE1);
	}

	/**Original name: NF05-WHICH-NOTES-TABLE-NOTE-1<br>*/
	public String getWhichNotesTableNote1() {
		return readString(Pos.WHICH_NOTES_TABLE_NOTE1, Len.WHICH_NOTES_TABLE_NOTE1);
	}

	public void setNotes2(String notes2) {
		writeString(Pos.NOTES2, notes2, Len.NOTES2);
	}

	/**Original name: NF05-NOTES-2<br>*/
	public String getNotes2() {
		return readString(Pos.NOTES2, Len.NOTES2);
	}

	public void setWhichNotesTableNote2(String whichNotesTableNote2) {
		writeString(Pos.WHICH_NOTES_TABLE_NOTE2, whichNotesTableNote2, Len.WHICH_NOTES_TABLE_NOTE2);
	}

	/**Original name: NF05-WHICH-NOTES-TABLE-NOTE-2<br>*/
	public String getWhichNotesTableNote2() {
		return readString(Pos.WHICH_NOTES_TABLE_NOTE2, Len.WHICH_NOTES_TABLE_NOTE2);
	}

	public void setNotes3(String notes3) {
		writeString(Pos.NOTES3, notes3, Len.NOTES3);
	}

	/**Original name: NF05-NOTES-3<br>*/
	public String getNotes3() {
		return readString(Pos.NOTES3, Len.NOTES3);
	}

	public void setWhichNotesTableNote3(String whichNotesTableNote3) {
		writeString(Pos.WHICH_NOTES_TABLE_NOTE3, whichNotesTableNote3, Len.WHICH_NOTES_TABLE_NOTE3);
	}

	/**Original name: NF05-WHICH-NOTES-TABLE-NOTE-3<br>*/
	public String getWhichNotesTableNote3() {
		return readString(Pos.WHICH_NOTES_TABLE_NOTE3, Len.WHICH_NOTES_TABLE_NOTE3);
	}

	public void setNotes4(String notes4) {
		writeString(Pos.NOTES4, notes4, Len.NOTES4);
	}

	/**Original name: NF05-NOTES-4<br>*/
	public String getNotes4() {
		return readString(Pos.NOTES4, Len.NOTES4);
	}

	public void setWhichNotesTableNote4(String whichNotesTableNote4) {
		writeString(Pos.WHICH_NOTES_TABLE_NOTE4, whichNotesTableNote4, Len.WHICH_NOTES_TABLE_NOTE4);
	}

	/**Original name: NF05-WHICH-NOTES-TABLE-NOTE-4<br>*/
	public String getWhichNotesTableNote4() {
		return readString(Pos.WHICH_NOTES_TABLE_NOTE4, Len.WHICH_NOTES_TABLE_NOTE4);
	}

	public void setNotes5(String notes5) {
		writeString(Pos.NOTES5, notes5, Len.NOTES5);
	}

	/**Original name: NF05-NOTES-5<br>*/
	public String getNotes5() {
		return readString(Pos.NOTES5, Len.NOTES5);
	}

	public void setWhichNotesTableNote5(String whichNotesTableNote5) {
		writeString(Pos.WHICH_NOTES_TABLE_NOTE5, whichNotesTableNote5, Len.WHICH_NOTES_TABLE_NOTE5);
	}

	/**Original name: NF05-WHICH-NOTES-TABLE-NOTE-5<br>*/
	public String getWhichNotesTableNote5() {
		return readString(Pos.WHICH_NOTES_TABLE_NOTE5, Len.WHICH_NOTES_TABLE_NOTE5);
	}

	public void setVoidLiPdAm(AfDecimal voidLiPdAm) {
		writeDecimal(Pos.VOID_LI_PD_AM, voidLiPdAm.copy(), SignType.NO_SIGN);
	}

	/**Original name: NF05-VOID-LI-PD-AM<br>*/
	public AfDecimal getVoidLiPdAm() {
		return readDecimal(Pos.VOID_LI_PD_AM, Len.Int.VOID_LI_PD_AM, Len.Fract.VOID_LI_PD_AM, SignType.NO_SIGN);
	}

	public void setVoidDnlRemark(String voidDnlRemark) {
		writeString(Pos.VOID_DNL_REMARK, voidDnlRemark, Len.VOID_DNL_REMARK);
	}

	/**Original name: NF05-VOID-DNL-REMARK<br>*/
	public String getVoidDnlRemark() {
		return readString(Pos.VOID_DNL_REMARK, Len.VOID_DNL_REMARK);
	}

	public void setShareDateFlag(char shareDateFlag) {
		writeChar(Pos.SHARE_DATE_FLAG, shareDateFlag);
	}

	/**Original name: NF05-SHARE-DATE-FLAG<br>*/
	public char getShareDateFlag() {
		return readChar(Pos.SHARE_DATE_FLAG);
	}

	public void setOverrideCode1(String overrideCode1) {
		writeString(Pos.OVERRIDE_CODE1, overrideCode1, Len.OVERRIDE_CODE1);
	}

	/**Original name: NF05-OVERRIDE-CODE1<br>*/
	public String getOverrideCode1() {
		return readString(Pos.OVERRIDE_CODE1, Len.OVERRIDE_CODE1);
	}

	public void setOverrideCode2(String overrideCode2) {
		writeString(Pos.OVERRIDE_CODE2, overrideCode2, Len.OVERRIDE_CODE2);
	}

	/**Original name: NF05-OVERRIDE-CODE2<br>*/
	public String getOverrideCode2() {
		return readString(Pos.OVERRIDE_CODE2, Len.OVERRIDE_CODE2);
	}

	public void setOverrideCode3(String overrideCode3) {
		writeString(Pos.OVERRIDE_CODE3, overrideCode3, Len.OVERRIDE_CODE3);
	}

	/**Original name: NF05-OVERRIDE-CODE3<br>*/
	public String getOverrideCode3() {
		return readString(Pos.OVERRIDE_CODE3, Len.OVERRIDE_CODE3);
	}

	public void setOverrideCode4(String overrideCode4) {
		writeString(Pos.OVERRIDE_CODE4, overrideCode4, Len.OVERRIDE_CODE4);
	}

	/**Original name: NF05-OVERRIDE-CODE4<br>*/
	public String getOverrideCode4() {
		return readString(Pos.OVERRIDE_CODE4, Len.OVERRIDE_CODE4);
	}

	public void setOverrideCode5(String overrideCode5) {
		writeString(Pos.OVERRIDE_CODE5, overrideCode5, Len.OVERRIDE_CODE5);
	}

	/**Original name: NF05-OVERRIDE-CODE5<br>*/
	public String getOverrideCode5() {
		return readString(Pos.OVERRIDE_CODE5, Len.OVERRIDE_CODE5);
	}

	public void setOverrideCode6(String overrideCode6) {
		writeString(Pos.OVERRIDE_CODE6, overrideCode6, Len.OVERRIDE_CODE6);
	}

	/**Original name: NF05-OVERRIDE-CODE6<br>*/
	public String getOverrideCode6() {
		return readString(Pos.OVERRIDE_CODE6, Len.OVERRIDE_CODE6);
	}

	public void setOverrideCode7(String overrideCode7) {
		writeString(Pos.OVERRIDE_CODE7, overrideCode7, Len.OVERRIDE_CODE7);
	}

	/**Original name: NF05-OVERRIDE-CODE7<br>*/
	public String getOverrideCode7() {
		return readString(Pos.OVERRIDE_CODE7, Len.OVERRIDE_CODE7);
	}

	public void setOverrideCode8(String overrideCode8) {
		writeString(Pos.OVERRIDE_CODE8, overrideCode8, Len.OVERRIDE_CODE8);
	}

	/**Original name: NF05-OVERRIDE-CODE8<br>*/
	public String getOverrideCode8() {
		return readString(Pos.OVERRIDE_CODE8, Len.OVERRIDE_CODE8);
	}

	public void setOverrideCode9(String overrideCode9) {
		writeString(Pos.OVERRIDE_CODE9, overrideCode9, Len.OVERRIDE_CODE9);
	}

	/**Original name: NF05-OVERRIDE-CODE9<br>*/
	public String getOverrideCode9() {
		return readString(Pos.OVERRIDE_CODE9, Len.OVERRIDE_CODE9);
	}

	public void setOverrideCode10(String overrideCode10) {
		writeString(Pos.OVERRIDE_CODE10, overrideCode10, Len.OVERRIDE_CODE10);
	}

	/**Original name: NF05-OVERRIDE-CODE10<br>*/
	public String getOverrideCode10() {
		return readString(Pos.OVERRIDE_CODE10, Len.OVERRIDE_CODE10);
	}

	public void setAltInsId(String altInsId) {
		writeString(Pos.ALT_INS_ID, altInsId, Len.ALT_INS_ID);
	}

	/**Original name: NF05-ALT-INS-ID<br>*/
	public String getAltInsId() {
		return readString(Pos.ALT_INS_ID, Len.ALT_INS_ID);
	}

	public void setPaIdToAcumId(String paIdToAcumId) {
		writeString(Pos.PA_ID_TO_ACUM_ID, paIdToAcumId, Len.PA_ID_TO_ACUM_ID);
	}

	/**Original name: NF05-PA-ID-TO-ACUM-ID<br>*/
	public String getPaIdToAcumId() {
		return readString(Pos.PA_ID_TO_ACUM_ID, Len.PA_ID_TO_ACUM_ID);
	}

	public void setMemIdToAcumId(String memIdToAcumId) {
		writeString(Pos.MEM_ID_TO_ACUM_ID, memIdToAcumId, Len.MEM_ID_TO_ACUM_ID);
	}

	/**Original name: NF05-MEM-ID-TO-ACUM-ID<br>*/
	public String getMemIdToAcumId() {
		return readString(Pos.MEM_ID_TO_ACUM_ID, Len.MEM_ID_TO_ACUM_ID);
	}

	public byte[] getNf05TotalRecordBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.NF05_TOTAL_RECORD, Pos.NF05_TOTAL_RECORD);
		return buffer;
	}

	public void setBillPvdrNum(String billPvdrNum) {
		writeString(Pos.BILL_PVDR_NUM, billPvdrNum, Len.BILL_PVDR_NUM);
	}

	/**Original name: NF05-BILL-PVDR-NUM<br>*/
	public String getBillPvdrNum() {
		return readString(Pos.BILL_PVDR_NUM, Len.BILL_PVDR_NUM);
	}

	public void setBillPvdrLobFormatted(String billPvdrLob) {
		writeString(Pos.BILL_PVDR_LOB, Trunc.toUnsignedNumeric(billPvdrLob, Len.BILL_PVDR_LOB), Len.BILL_PVDR_LOB);
	}

	/**Original name: NF05-BILL-PVDR-LOB<br>*/
	public short getBillPvdrLob() {
		return readNumDispUnsignedShort(Pos.BILL_PVDR_LOB, Len.BILL_PVDR_LOB);
	}

	public String getBillPvdrLobFormatted() {
		return readFixedString(Pos.BILL_PVDR_LOB, Len.BILL_PVDR_LOB);
	}

	public void setBillPvdrStatus(char billPvdrStatus) {
		writeChar(Pos.BILL_PVDR_STATUS, billPvdrStatus);
	}

	/**Original name: NF05-BILL-PVDR-STATUS<br>*/
	public char getBillPvdrStatus() {
		return readChar(Pos.BILL_PVDR_STATUS);
	}

	public void setBillPvdrEin(String billPvdrEin) {
		writeString(Pos.BILL_PVDR_EIN, billPvdrEin, Len.BILL_PVDR_EIN);
	}

	/**Original name: NF05-BILL-PVDR-EIN<br>*/
	public String getBillPvdrEin() {
		return readString(Pos.BILL_PVDR_EIN, Len.BILL_PVDR_EIN);
	}

	public void setBillPvdrSsn(String billPvdrSsn) {
		writeString(Pos.BILL_PVDR_SSN, billPvdrSsn, Len.BILL_PVDR_SSN);
	}

	/**Original name: NF05-BILL-PVDR-SSN<br>*/
	public String getBillPvdrSsn() {
		return readString(Pos.BILL_PVDR_SSN, Len.BILL_PVDR_SSN);
	}

	public void setBillPvdrZip(String billPvdrZip) {
		writeString(Pos.BILL_PVDR_ZIP, billPvdrZip, Len.BILL_PVDR_ZIP);
	}

	/**Original name: NF05-BILL-PVDR-ZIP<br>*/
	public String getBillPvdrZip() {
		return readString(Pos.BILL_PVDR_ZIP, Len.BILL_PVDR_ZIP);
	}

	public void setBillPvdrSpec(String billPvdrSpec) {
		writeString(Pos.BILL_PVDR_SPEC, billPvdrSpec, Len.BILL_PVDR_SPEC);
	}

	/**Original name: NF05-BILL-PVDR-SPEC<br>*/
	public String getBillPvdrSpec() {
		return readString(Pos.BILL_PVDR_SPEC, Len.BILL_PVDR_SPEC);
	}

	public void setBillPvdrNameLst(String billPvdrNameLst) {
		writeString(Pos.BILL_PVDR_NAME_LST, billPvdrNameLst, Len.BILL_PVDR_NAME_LST);
	}

	/**Original name: NF05-BILL-PVDR-NAME-LST<br>*/
	public String getBillPvdrNameLst() {
		return readString(Pos.BILL_PVDR_NAME_LST, Len.BILL_PVDR_NAME_LST);
	}

	public void setTotalCharge(AfDecimal totalCharge) {
		writeDecimal(Pos.TOTAL_CHARGE, totalCharge.copy());
	}

	/**Original name: NF05-TOTAL-CHARGE<br>*/
	public AfDecimal getTotalCharge() {
		return readDecimal(Pos.TOTAL_CHARGE, Len.Int.TOTAL_CHARGE, Len.Fract.TOTAL_CHARGE);
	}

	public void setTotalNonCovrd(AfDecimal totalNonCovrd) {
		writeDecimal(Pos.TOTAL_NON_COVRD, totalNonCovrd.copy());
	}

	/**Original name: NF05-TOTAL-NON-COVRD<br>*/
	public AfDecimal getTotalNonCovrd() {
		return readDecimal(Pos.TOTAL_NON_COVRD, Len.Int.TOTAL_NON_COVRD, Len.Fract.TOTAL_NON_COVRD);
	}

	public void setTotalOthrInsur(AfDecimal totalOthrInsur) {
		writeDecimal(Pos.TOTAL_OTHR_INSUR, totalOthrInsur.copy());
	}

	/**Original name: NF05-TOTAL-OTHR-INSUR<br>*/
	public AfDecimal getTotalOthrInsur() {
		return readDecimal(Pos.TOTAL_OTHR_INSUR, Len.Int.TOTAL_OTHR_INSUR, Len.Fract.TOTAL_OTHR_INSUR);
	}

	public void setTotalDeduct(AfDecimal totalDeduct) {
		writeDecimal(Pos.TOTAL_DEDUCT, totalDeduct.copy());
	}

	/**Original name: NF05-TOTAL-DEDUCT<br>*/
	public AfDecimal getTotalDeduct() {
		return readDecimal(Pos.TOTAL_DEDUCT, Len.Int.TOTAL_DEDUCT, Len.Fract.TOTAL_DEDUCT);
	}

	public void setTotalCopay(AfDecimal totalCopay) {
		writeDecimal(Pos.TOTAL_COPAY, totalCopay.copy());
	}

	/**Original name: NF05-TOTAL-COPAY<br>*/
	public AfDecimal getTotalCopay() {
		return readDecimal(Pos.TOTAL_COPAY, Len.Int.TOTAL_COPAY, Len.Fract.TOTAL_COPAY);
	}

	public void setTotalCoinsur(AfDecimal totalCoinsur) {
		writeDecimal(Pos.TOTAL_COINSUR, totalCoinsur.copy());
	}

	/**Original name: NF05-TOTAL-COINSUR<br>*/
	public AfDecimal getTotalCoinsur() {
		return readDecimal(Pos.TOTAL_COINSUR, Len.Int.TOTAL_COINSUR, Len.Fract.TOTAL_COINSUR);
	}

	public void setTotalPatRespons(AfDecimal totalPatRespons) {
		writeDecimal(Pos.TOTAL_PAT_RESPONS, totalPatRespons.copy());
	}

	/**Original name: NF05-TOTAL-PAT-RESPONS<br>*/
	public AfDecimal getTotalPatRespons() {
		return readDecimal(Pos.TOTAL_PAT_RESPONS, Len.Int.TOTAL_PAT_RESPONS, Len.Fract.TOTAL_PAT_RESPONS);
	}

	public void setTotalProvWriteoff(AfDecimal totalProvWriteoff) {
		writeDecimal(Pos.TOTAL_PROV_WRITEOFF, totalProvWriteoff.copy());
	}

	/**Original name: NF05-TOTAL-PROV-WRITEOFF<br>*/
	public AfDecimal getTotalProvWriteoff() {
		return readDecimal(Pos.TOTAL_PROV_WRITEOFF, Len.Int.TOTAL_PROV_WRITEOFF, Len.Fract.TOTAL_PROV_WRITEOFF);
	}

	public void setTotalAmountPaid(AfDecimal totalAmountPaid) {
		writeDecimal(Pos.TOTAL_AMOUNT_PAID, totalAmountPaid.copy());
	}

	/**Original name: NF05-TOTAL-AMOUNT-PAID<br>*/
	public AfDecimal getTotalAmountPaid() {
		return readDecimal(Pos.TOTAL_AMOUNT_PAID, Len.Int.TOTAL_AMOUNT_PAID, Len.Fract.TOTAL_AMOUNT_PAID);
	}

	public void setPaidTo(String paidTo) {
		writeString(Pos.PAID_TO, paidTo, Len.PAID_TO);
	}

	/**Original name: NF05-PAID-TO<br>*/
	public String getPaidTo() {
		return readString(Pos.PAID_TO, Len.PAID_TO);
	}

	public void setN1N2CheckTotal(AfDecimal n1N2CheckTotal) {
		writeDecimal(Pos.N1_N2_CHECK_TOTAL, n1N2CheckTotal.copy());
	}

	/**Original name: NF05-N1-N2-CHECK-TOTAL<br>*/
	public AfDecimal getN1N2CheckTotal() {
		return readDecimal(Pos.N1_N2_CHECK_TOTAL, Len.Int.N1_N2_CHECK_TOTAL, Len.Fract.N1_N2_CHECK_TOTAL);
	}

	public void setTotFiller1(String totFiller1) {
		writeString(Pos.TOT_FILLER1, totFiller1, Len.TOT_FILLER1);
	}

	/**Original name: NF05-TOT-FILLER-1<br>*/
	public String getTotFiller1() {
		return readString(Pos.TOT_FILLER1, Len.TOT_FILLER1);
	}

	public void setGenNumber(String genNumber) {
		writeString(Pos.GEN_NUMBER, genNumber, Len.GEN_NUMBER);
	}

	/**Original name: NF05-GEN-NUMBER<br>*/
	public String getGenNumber() {
		return readString(Pos.GEN_NUMBER, Len.GEN_NUMBER);
	}

	public String getGenNumberFormatted() {
		return Functions.padBlanks(getGenNumber(), Len.GEN_NUMBER);
	}

	public void setGenDateFormatted(String genDate) {
		writeString(Pos.GEN_DATE, Trunc.toUnsignedNumeric(genDate, Len.GEN_DATE), Len.GEN_DATE);
	}

	/**Original name: NF05-GEN-DATE<br>*/
	public int getGenDate() {
		return readNumDispUnsignedInt(Pos.GEN_DATE, Len.GEN_DATE);
	}

	public void setInpatTypeService(String inpatTypeService) {
		writeString(Pos.INPAT_TYPE_SERVICE, inpatTypeService, Len.INPAT_TYPE_SERVICE);
	}

	/**Original name: NF05-INPAT-TYPE-SERVICE<br>*/
	public String getInpatTypeService() {
		return readString(Pos.INPAT_TYPE_SERVICE, Len.INPAT_TYPE_SERVICE);
	}

	public void setPerfProviderName(String perfProviderName) {
		writeString(Pos.PERF_PROVIDER_NAME, perfProviderName, Len.PERF_PROVIDER_NAME);
	}

	/**Original name: NF05-PERF-PROVIDER-NAME<br>*/
	public String getPerfProviderName() {
		return readString(Pos.PERF_PROVIDER_NAME, Len.PERF_PROVIDER_NAME);
	}

	public void setClmInsLnCd(String clmInsLnCd) {
		writeString(Pos.CLM_INS_LN_CD, clmInsLnCd, Len.CLM_INS_LN_CD);
	}

	/**Original name: NF05-CLM-INS-LN-CD<br>*/
	public String getClmInsLnCd() {
		return readString(Pos.CLM_INS_LN_CD, Len.CLM_INS_LN_CD);
	}

	public void setPmtMdgpPlanCd(char pmtMdgpPlanCd) {
		writeChar(Pos.PMT_MDGP_PLAN_CD, pmtMdgpPlanCd);
	}

	/**Original name: NF05-PMT-MDGP-PLAN-CD<br>*/
	public char getPmtMdgpPlanCd() {
		return readChar(Pos.PMT_MDGP_PLAN_CD);
	}

	public void setBenPlnId(String benPlnId) {
		writeString(Pos.BEN_PLN_ID, benPlnId, Len.BEN_PLN_ID);
	}

	/**Original name: NF05-BEN-PLN-ID<br>*/
	public String getBenPlnId() {
		return readString(Pos.BEN_PLN_ID, Len.BEN_PLN_ID);
	}

	public void setClmFromServiceDate(String clmFromServiceDate) {
		writeString(Pos.CLM_FROM_SERVICE_DATE, clmFromServiceDate, Len.CLM_FROM_SERVICE_DATE);
	}

	/**Original name: NF05-CLM-FROM-SERVICE-DATE<br>*/
	public String getClmFromServiceDate() {
		return readString(Pos.CLM_FROM_SERVICE_DATE, Len.CLM_FROM_SERVICE_DATE);
	}

	public void setClmThruServiceDate(String clmThruServiceDate) {
		writeString(Pos.CLM_THRU_SERVICE_DATE, clmThruServiceDate, Len.CLM_THRU_SERVICE_DATE);
	}

	/**Original name: NF05-CLM-THRU-SERVICE-DATE<br>*/
	public String getClmThruServiceDate() {
		return readString(Pos.CLM_THRU_SERVICE_DATE, Len.CLM_THRU_SERVICE_DATE);
	}

	public void setTotFiller2(String totFiller2) {
		writeString(Pos.TOT_FILLER2, totFiller2, Len.TOT_FILLER2);
	}

	/**Original name: NF05-TOT-FILLER-2<br>*/
	public String getTotFiller2() {
		return readString(Pos.TOT_FILLER2, Len.TOT_FILLER2);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int NF05_LINE_LEVEL_RECORD = 1;
		public static final int FROM_SERVICE_DATE = NF05_LINE_LEVEL_RECORD;
		public static final int FROM_SERVICE_DATE_CC = FROM_SERVICE_DATE;
		public static final int FROM_SERVICE_DATE_YY = FROM_SERVICE_DATE_CC + Len.FROM_SERVICE_DATE_CC;
		public static final int FLR1 = FROM_SERVICE_DATE_YY + Len.FROM_SERVICE_DATE_YY;
		public static final int FROM_SERVICE_DATE_MM = FLR1 + Len.FLR1;
		public static final int FLR2 = FROM_SERVICE_DATE_MM + Len.FROM_SERVICE_DATE_MM;
		public static final int FROM_SERVICE_DATE_DD = FLR2 + Len.FLR1;
		public static final int THRU_SERVICE_DATE = FROM_SERVICE_DATE_DD + Len.FROM_SERVICE_DATE_DD;
		public static final int THRU_SERVICE_DATE_CC = THRU_SERVICE_DATE;
		public static final int THRU_SERVICE_DATE_YY = THRU_SERVICE_DATE_CC + Len.THRU_SERVICE_DATE_CC;
		public static final int FLR3 = THRU_SERVICE_DATE_YY + Len.THRU_SERVICE_DATE_YY;
		public static final int THRU_SERVICE_DATE_MM = FLR3 + Len.FLR1;
		public static final int FLR4 = THRU_SERVICE_DATE_MM + Len.THRU_SERVICE_DATE_MM;
		public static final int THUR_SERVICE_DATE_DD = FLR4 + Len.FLR1;
		public static final int EXAMINER_ACTION = THUR_SERVICE_DATE_DD + Len.THUR_SERVICE_DATE_DD;
		public static final int TYPE_SERVICE = EXAMINER_ACTION + Len.EXAMINER_ACTION;
		public static final int PLACE_SERVICE = TYPE_SERVICE + Len.TYPE_SERVICE;
		public static final int BENEFIT_TYPE = PLACE_SERVICE + Len.PLACE_SERVICE;
		public static final int DNL_REMARK = BENEFIT_TYPE + Len.BENEFIT_TYPE;
		public static final int RVW_BY_CD = DNL_REMARK + Len.DNL_REMARK;
		public static final int ADJD_PROV_STAT_CD = RVW_BY_CD + Len.RVW_BY_CD;
		public static final int LI_ALW_CHG_AM = ADJD_PROV_STAT_CD + Len.ADJD_PROV_STAT_CD;
		public static final int INDUS_CD = LI_ALW_CHG_AM + Len.LI_ALW_CHG_AM;
		public static final int AMOUNT_CHARGED = INDUS_CD + Len.INDUS_CD;
		public static final int LINE_NOT_COVRD_AMT = AMOUNT_CHARGED + Len.AMOUNT_CHARGED;
		public static final int LINE_OTHR_INSUR = LINE_NOT_COVRD_AMT + Len.LINE_NOT_COVRD_AMT;
		public static final int OTHER_INSURANCE_DATA = LINE_OTHR_INSUR + Len.LINE_OTHR_INSUR;
		public static final int OTHER_INSURE_COVERED = OTHER_INSURANCE_DATA;
		public static final int OTHER_INSURE_PAID = OTHER_INSURE_COVERED + Len.OTHER_INSURE_COVERED;
		public static final int OTHER_INSURE_SAVINGS = OTHER_INSURE_PAID + Len.OTHER_INSURE_PAID;
		public static final int FLR5 = OTHER_INSURE_SAVINGS + Len.OTHER_INSURE_SAVINGS;
		public static final int CALC1_DATA = FLR5 + Len.FLR1;
		public static final int CALC1_AMOUNT_PAID = CALC1_DATA;
		public static final int CALC1_DEDUCTIBLE = CALC1_AMOUNT_PAID + Len.CALC1_AMOUNT_PAID;
		public static final int CALC1_COPAY = CALC1_DEDUCTIBLE + Len.CALC1_DEDUCTIBLE;
		public static final int CALC1_COINSURANCE = CALC1_COPAY + Len.CALC1_COPAY;
		public static final int CALC1_PAID_UNDER = CALC1_COINSURANCE + Len.CALC1_COINSURANCE;
		public static final int CALC1_CORP_CODE = CALC1_PAID_UNDER;
		public static final int CALC1_PROD_CODE = CALC1_CORP_CODE + Len.CALC1_CORP_CODE;
		public static final int CALC1_TYPE_BENEFIT = CALC1_PROD_CODE + Len.CALC1_PROD_CODE;
		public static final int CALC1_SPEC_BENEFIT = CALC1_TYPE_BENEFIT + Len.CALC1_TYPE_BENEFIT;
		public static final int CALC1_COV_LOB = CALC1_SPEC_BENEFIT + Len.CALC1_SPEC_BENEFIT;
		public static final int CALC1_PAY_CODE = CALC1_COV_LOB + Len.CALC1_COV_LOB;
		public static final int CALC1_TYPE_CONTRACT = CALC1_PAY_CODE + Len.CALC1_PAY_CODE;
		public static final int CALC1_EXPLN_CD = CALC1_TYPE_CONTRACT + Len.CALC1_TYPE_CONTRACT;
		public static final int CALC2_DATA = CALC1_EXPLN_CD + Len.CALC1_EXPLN_CD;
		public static final int CALC2_AMOUNT_PAID = CALC2_DATA;
		public static final int CALC2_DEDUCTIBLE = CALC2_AMOUNT_PAID + Len.CALC2_AMOUNT_PAID;
		public static final int CALC2_COPAY = CALC2_DEDUCTIBLE + Len.CALC2_DEDUCTIBLE;
		public static final int CALC2_COINSURANCE = CALC2_COPAY + Len.CALC2_COPAY;
		public static final int CALC2_PAID_UNDER = CALC2_COINSURANCE + Len.CALC2_COINSURANCE;
		public static final int CALC2_CORP_CODE = CALC2_PAID_UNDER;
		public static final int CALC2_PROD_CODE = CALC2_CORP_CODE + Len.CALC2_CORP_CODE;
		public static final int CALC2_TYPE_BENEFIT = CALC2_PROD_CODE + Len.CALC2_PROD_CODE;
		public static final int CALC2_SPEC_BENEFIT = CALC2_TYPE_BENEFIT + Len.CALC2_TYPE_BENEFIT;
		public static final int CALC2_COV_LOB = CALC2_SPEC_BENEFIT + Len.CALC2_SPEC_BENEFIT;
		public static final int CALC2_PAY_CODE = CALC2_COV_LOB + Len.CALC2_COV_LOB;
		public static final int CALC2_TYPE_CONTRACT = CALC2_PAY_CODE + Len.CALC2_PAY_CODE;
		public static final int CALC2_EXPLN_CD = CALC2_TYPE_CONTRACT + Len.CALC2_TYPE_CONTRACT;
		public static final int CALC3_DATA = CALC2_EXPLN_CD + Len.CALC2_EXPLN_CD;
		public static final int CALC3_AMOUNT_PAID = CALC3_DATA;
		public static final int CALC3_DEDUCTIBLE = CALC3_AMOUNT_PAID + Len.CALC3_AMOUNT_PAID;
		public static final int CALC3_COPAY = CALC3_DEDUCTIBLE + Len.CALC3_DEDUCTIBLE;
		public static final int CALC3_COINSURANCE = CALC3_COPAY + Len.CALC3_COPAY;
		public static final int CALC3_PAID_UNDER = CALC3_COINSURANCE + Len.CALC3_COINSURANCE;
		public static final int CALC3_CORP_CODE = CALC3_PAID_UNDER;
		public static final int CALC3_PROD_CODE = CALC3_CORP_CODE + Len.CALC3_CORP_CODE;
		public static final int CALC3_TYPE_BENEFIT = CALC3_PROD_CODE + Len.CALC3_PROD_CODE;
		public static final int CALC3_SPEC_BENEFIT = CALC3_TYPE_BENEFIT + Len.CALC3_TYPE_BENEFIT;
		public static final int CALC3_COV_LOB = CALC3_SPEC_BENEFIT + Len.CALC3_SPEC_BENEFIT;
		public static final int CALC3_PAY_CODE = CALC3_COV_LOB + Len.CALC3_COV_LOB;
		public static final int CALC3_TYPE_CONTRACT = CALC3_PAY_CODE + Len.CALC3_PAY_CODE;
		public static final int CALC3_EXPLN_CD = CALC3_TYPE_CONTRACT + Len.CALC3_TYPE_CONTRACT;
		public static final int CALC4_DATA = CALC3_EXPLN_CD + Len.CALC3_EXPLN_CD;
		public static final int CALC4_AMOUNT_PAID = CALC4_DATA;
		public static final int CALC4_DEDUCTIBLE = CALC4_AMOUNT_PAID + Len.CALC4_AMOUNT_PAID;
		public static final int CALC4_COPAY = CALC4_DEDUCTIBLE + Len.CALC4_DEDUCTIBLE;
		public static final int CALC4_COINSURANCE = CALC4_COPAY + Len.CALC4_COPAY;
		public static final int CALC4_PAID_UNDER = CALC4_COINSURANCE + Len.CALC4_COINSURANCE;
		public static final int CALC4_CORP_CODE = CALC4_PAID_UNDER;
		public static final int CALC4_PROD_CODE = CALC4_CORP_CODE + Len.CALC4_CORP_CODE;
		public static final int CALC4_TYPE_BENEFIT = CALC4_PROD_CODE + Len.CALC4_PROD_CODE;
		public static final int CALC4_SPEC_BENEFIT = CALC4_TYPE_BENEFIT + Len.CALC4_TYPE_BENEFIT;
		public static final int CALC4_COV_LOB = CALC4_SPEC_BENEFIT + Len.CALC4_SPEC_BENEFIT;
		public static final int CALC4_PAY_CODE = CALC4_COV_LOB + Len.CALC4_COV_LOB;
		public static final int CALC4_TYPE_CONTRACT = CALC4_PAY_CODE + Len.CALC4_PAY_CODE;
		public static final int CALC4_EXPLN_CD = CALC4_TYPE_CONTRACT + Len.CALC4_TYPE_CONTRACT;
		public static final int CALC5_DATA = CALC4_EXPLN_CD + Len.CALC4_EXPLN_CD;
		public static final int CALC5_AMOUNT_PAID = CALC5_DATA;
		public static final int CALC5_DEDUCTIBLE = CALC5_AMOUNT_PAID + Len.CALC5_AMOUNT_PAID;
		public static final int CALC5_COPAY = CALC5_DEDUCTIBLE + Len.CALC5_DEDUCTIBLE;
		public static final int CALC5_COINSURANCE = CALC5_COPAY + Len.CALC5_COPAY;
		public static final int CALC5_PAID_UNDER = CALC5_COINSURANCE + Len.CALC5_COINSURANCE;
		public static final int CALC5_CORP_CODE = CALC5_PAID_UNDER;
		public static final int CALC5_PROD_CODE = CALC5_CORP_CODE + Len.CALC5_CORP_CODE;
		public static final int CALC5_TYPE_BENEFIT = CALC5_PROD_CODE + Len.CALC5_PROD_CODE;
		public static final int CALC5_SPEC_BENEFIT = CALC5_TYPE_BENEFIT + Len.CALC5_TYPE_BENEFIT;
		public static final int CALC5_COV_LOB = CALC5_SPEC_BENEFIT + Len.CALC5_SPEC_BENEFIT;
		public static final int CALC5_PAY_CODE = CALC5_COV_LOB + Len.CALC5_COV_LOB;
		public static final int CALC5_TYPE_CONTRACT = CALC5_PAY_CODE + Len.CALC5_PAY_CODE;
		public static final int CALC5_EXPLN_CD = CALC5_TYPE_CONTRACT + Len.CALC5_TYPE_CONTRACT;
		public static final int CALC6_DATA = CALC5_EXPLN_CD + Len.CALC5_EXPLN_CD;
		public static final int CALC6_AMOUNT_PAID = CALC6_DATA;
		public static final int CALC6_DEDUCTIBLE = CALC6_AMOUNT_PAID + Len.CALC6_AMOUNT_PAID;
		public static final int CALC6_COPAY = CALC6_DEDUCTIBLE + Len.CALC6_DEDUCTIBLE;
		public static final int CALC6_COINSURANCE = CALC6_COPAY + Len.CALC6_COPAY;
		public static final int CALC6_PAID_UNDER = CALC6_COINSURANCE + Len.CALC6_COINSURANCE;
		public static final int CALC6_CORP_CODE = CALC6_PAID_UNDER;
		public static final int CALC6_PROD_CODE = CALC6_CORP_CODE + Len.CALC6_CORP_CODE;
		public static final int CALC6_TYPE_BENEFIT = CALC6_PROD_CODE + Len.CALC6_PROD_CODE;
		public static final int CALC6_SPEC_BENEFIT = CALC6_TYPE_BENEFIT + Len.CALC6_TYPE_BENEFIT;
		public static final int CALC6_COV_LOB = CALC6_SPEC_BENEFIT + Len.CALC6_SPEC_BENEFIT;
		public static final int CALC6_PAY_CODE = CALC6_COV_LOB + Len.CALC6_COV_LOB;
		public static final int CALC6_TYPE_CONTRACT = CALC6_PAY_CODE + Len.CALC6_PAY_CODE;
		public static final int CALC6_EXPLN_CD = CALC6_TYPE_CONTRACT + Len.CALC6_TYPE_CONTRACT;
		public static final int CALC7_DATA = CALC6_EXPLN_CD + Len.CALC6_EXPLN_CD;
		public static final int CALC7_AMOUNT_PAID = CALC7_DATA;
		public static final int CALC7_DEDUCTIBLE = CALC7_AMOUNT_PAID + Len.CALC7_AMOUNT_PAID;
		public static final int CALC7_COPAY = CALC7_DEDUCTIBLE + Len.CALC7_DEDUCTIBLE;
		public static final int CALC7_COINSURANCE = CALC7_COPAY + Len.CALC7_COPAY;
		public static final int CALC7_PAID_UNDER = CALC7_COINSURANCE + Len.CALC7_COINSURANCE;
		public static final int CALC7_CORP_CODE = CALC7_PAID_UNDER;
		public static final int CALC7_PROD_CODE = CALC7_CORP_CODE + Len.CALC7_CORP_CODE;
		public static final int CALC7_TYPE_BENEFIT = CALC7_PROD_CODE + Len.CALC7_PROD_CODE;
		public static final int CALC7_SPEC_BENEFIT = CALC7_TYPE_BENEFIT + Len.CALC7_TYPE_BENEFIT;
		public static final int CALC7_COV_LOB = CALC7_SPEC_BENEFIT + Len.CALC7_SPEC_BENEFIT;
		public static final int CALC7_PAY_CODE = CALC7_COV_LOB + Len.CALC7_COV_LOB;
		public static final int CALC7_TYPE_CONTRACT = CALC7_PAY_CODE + Len.CALC7_PAY_CODE;
		public static final int CALC7_EXPLN_CD = CALC7_TYPE_CONTRACT + Len.CALC7_TYPE_CONTRACT;
		public static final int CALC8_DATA = CALC7_EXPLN_CD + Len.CALC7_EXPLN_CD;
		public static final int CALC8_AMOUNT_PAID = CALC8_DATA;
		public static final int CALC8_DEDUCTIBLE = CALC8_AMOUNT_PAID + Len.CALC8_AMOUNT_PAID;
		public static final int CALC8_COPAY = CALC8_DEDUCTIBLE + Len.CALC8_DEDUCTIBLE;
		public static final int CALC8_COINSURANCE = CALC8_COPAY + Len.CALC8_COPAY;
		public static final int CALC8_PAID_UNDER = CALC8_COINSURANCE + Len.CALC8_COINSURANCE;
		public static final int CALC8_CORP_CODE = CALC8_PAID_UNDER;
		public static final int CALC8_PROD_CODE = CALC8_CORP_CODE + Len.CALC8_CORP_CODE;
		public static final int CALC8_TYPE_BENEFIT = CALC8_PROD_CODE + Len.CALC8_PROD_CODE;
		public static final int CALC8_SPEC_BENEFIT = CALC8_TYPE_BENEFIT + Len.CALC8_TYPE_BENEFIT;
		public static final int CALC8_COV_LOB = CALC8_SPEC_BENEFIT + Len.CALC8_SPEC_BENEFIT;
		public static final int CALC8_PAY_CODE = CALC8_COV_LOB + Len.CALC8_COV_LOB;
		public static final int CALC8_TYPE_CONTRACT = CALC8_PAY_CODE + Len.CALC8_PAY_CODE;
		public static final int CALC8_EXPLN_CD = CALC8_TYPE_CONTRACT + Len.CALC8_TYPE_CONTRACT;
		public static final int CALC9_DATA = CALC8_EXPLN_CD + Len.CALC8_EXPLN_CD;
		public static final int CALC9_AMOUNT_PAID = CALC9_DATA;
		public static final int CALC9_DEDUCTIBLE = CALC9_AMOUNT_PAID + Len.CALC9_AMOUNT_PAID;
		public static final int CALC9_COPAY = CALC9_DEDUCTIBLE + Len.CALC9_DEDUCTIBLE;
		public static final int CALC9_COINSURANCE = CALC9_COPAY + Len.CALC9_COPAY;
		public static final int CALC9_PAID_UNDER = CALC9_COINSURANCE + Len.CALC9_COINSURANCE;
		public static final int CALC9_CORP_CODE = CALC9_PAID_UNDER;
		public static final int CALC9_PROD_CODE = CALC9_CORP_CODE + Len.CALC9_CORP_CODE;
		public static final int CALC9_TYPE_BENEFIT = CALC9_PROD_CODE + Len.CALC9_PROD_CODE;
		public static final int CALC9_SPEC_BENEFIT = CALC9_TYPE_BENEFIT + Len.CALC9_TYPE_BENEFIT;
		public static final int CALC9_COV_LOB = CALC9_SPEC_BENEFIT + Len.CALC9_SPEC_BENEFIT;
		public static final int CALC9_PAY_CODE = CALC9_COV_LOB + Len.CALC9_COV_LOB;
		public static final int CALC9_TYPE_CONTRACT = CALC9_PAY_CODE + Len.CALC9_PAY_CODE;
		public static final int CALC9_EXPLN_CD = CALC9_TYPE_CONTRACT + Len.CALC9_TYPE_CONTRACT;
		public static final int DEDUCT_CARRYOVER = CALC9_EXPLN_CD + Len.CALC9_EXPLN_CD;
		public static final int FEP_DENT_PPO_PRV_WRTOFF = DEDUCT_CARRYOVER + Len.DEDUCT_CARRYOVER;
		public static final int FEP_DENT_PPO_INS_LIAB = FEP_DENT_PPO_PRV_WRTOFF + Len.FEP_DENT_PPO_PRV_WRTOFF;
		public static final int LINE_EXPLANATION_CODE = FEP_DENT_PPO_INS_LIAB + Len.FEP_DENT_PPO_INS_LIAB;
		public static final int SELF_REFERRAL_INDICATOR = LINE_EXPLANATION_CODE + Len.LINE_EXPLANATION_CODE;
		public static final int NOTES1 = SELF_REFERRAL_INDICATOR + Len.SELF_REFERRAL_INDICATOR;
		public static final int WHICH_NOTES_TABLE_NOTE1 = NOTES1 + Len.NOTES1;
		public static final int NOTES2 = WHICH_NOTES_TABLE_NOTE1 + Len.WHICH_NOTES_TABLE_NOTE1;
		public static final int WHICH_NOTES_TABLE_NOTE2 = NOTES2 + Len.NOTES2;
		public static final int NOTES3 = WHICH_NOTES_TABLE_NOTE2 + Len.WHICH_NOTES_TABLE_NOTE2;
		public static final int WHICH_NOTES_TABLE_NOTE3 = NOTES3 + Len.NOTES3;
		public static final int NOTES4 = WHICH_NOTES_TABLE_NOTE3 + Len.WHICH_NOTES_TABLE_NOTE3;
		public static final int WHICH_NOTES_TABLE_NOTE4 = NOTES4 + Len.NOTES4;
		public static final int NOTES5 = WHICH_NOTES_TABLE_NOTE4 + Len.WHICH_NOTES_TABLE_NOTE4;
		public static final int WHICH_NOTES_TABLE_NOTE5 = NOTES5 + Len.NOTES5;
		public static final int VOID_LI_PD_AM = WHICH_NOTES_TABLE_NOTE5 + Len.WHICH_NOTES_TABLE_NOTE5;
		public static final int VOID_DNL_REMARK = VOID_LI_PD_AM + Len.VOID_LI_PD_AM;
		public static final int SHARE_DATE_FLAG = VOID_DNL_REMARK + Len.VOID_DNL_REMARK;
		public static final int OVERRIDE_CODES_DATA = SHARE_DATE_FLAG + Len.SHARE_DATE_FLAG;
		public static final int OVERRIDE_CODE1 = OVERRIDE_CODES_DATA;
		public static final int OVERRIDE_CODE2 = OVERRIDE_CODE1 + Len.OVERRIDE_CODE1;
		public static final int OVERRIDE_CODE3 = OVERRIDE_CODE2 + Len.OVERRIDE_CODE2;
		public static final int OVERRIDE_CODE4 = OVERRIDE_CODE3 + Len.OVERRIDE_CODE3;
		public static final int OVERRIDE_CODE5 = OVERRIDE_CODE4 + Len.OVERRIDE_CODE4;
		public static final int OVERRIDE_CODE6 = OVERRIDE_CODE5 + Len.OVERRIDE_CODE5;
		public static final int OVERRIDE_CODE7 = OVERRIDE_CODE6 + Len.OVERRIDE_CODE6;
		public static final int OVERRIDE_CODE8 = OVERRIDE_CODE7 + Len.OVERRIDE_CODE7;
		public static final int OVERRIDE_CODE9 = OVERRIDE_CODE8 + Len.OVERRIDE_CODE8;
		public static final int OVERRIDE_CODE10 = OVERRIDE_CODE9 + Len.OVERRIDE_CODE9;
		public static final int ALT_INS_ID = OVERRIDE_CODE10 + Len.OVERRIDE_CODE10;
		public static final int PA_ID_TO_ACUM_ID = ALT_INS_ID + Len.ALT_INS_ID;
		public static final int MEM_ID_TO_ACUM_ID = PA_ID_TO_ACUM_ID + Len.PA_ID_TO_ACUM_ID;
		public static final int FLR6 = MEM_ID_TO_ACUM_ID + Len.MEM_ID_TO_ACUM_ID;
		public static final int NF05_TOTAL_RECORD = 1;
		public static final int BILLING_PROVIDER_DATA = NF05_TOTAL_RECORD;
		public static final int BILL_PVDR_NUM = BILLING_PROVIDER_DATA;
		public static final int BILL_PVDR_LOB = BILL_PVDR_NUM + Len.BILL_PVDR_NUM;
		public static final int BILL_PVDR_STATUS = BILL_PVDR_LOB + Len.BILL_PVDR_LOB;
		public static final int BILL_PVDR_EIN = BILL_PVDR_STATUS + Len.BILL_PVDR_STATUS;
		public static final int BILL_PVDR_SSN = BILL_PVDR_EIN + Len.BILL_PVDR_EIN;
		public static final int BILL_PVDR_ZIP = BILL_PVDR_SSN + Len.BILL_PVDR_SSN;
		public static final int BILL_PVDR_SPEC = BILL_PVDR_ZIP + Len.BILL_PVDR_ZIP;
		public static final int BILL_PVDR_NAME_LST = BILL_PVDR_SPEC + Len.BILL_PVDR_SPEC;
		public static final int TOTAL_CHARGE = BILL_PVDR_NAME_LST + Len.BILL_PVDR_NAME_LST;
		public static final int TOTAL_NON_COVRD = TOTAL_CHARGE + Len.TOTAL_CHARGE;
		public static final int TOTAL_OTHR_INSUR = TOTAL_NON_COVRD + Len.TOTAL_NON_COVRD;
		public static final int TOTAL_DEDUCT = TOTAL_OTHR_INSUR + Len.TOTAL_OTHR_INSUR;
		public static final int TOTAL_COPAY = TOTAL_DEDUCT + Len.TOTAL_DEDUCT;
		public static final int TOTAL_COINSUR = TOTAL_COPAY + Len.TOTAL_COPAY;
		public static final int TOTAL_PAT_RESPONS = TOTAL_COINSUR + Len.TOTAL_COINSUR;
		public static final int TOTAL_PROV_WRITEOFF = TOTAL_PAT_RESPONS + Len.TOTAL_PAT_RESPONS;
		public static final int TOTAL_AMOUNT_PAID = TOTAL_PROV_WRITEOFF + Len.TOTAL_PROV_WRITEOFF;
		public static final int PAID_TO = TOTAL_AMOUNT_PAID + Len.TOTAL_AMOUNT_PAID;
		public static final int N1_N2_CHECK_TOTAL = PAID_TO + Len.PAID_TO;
		public static final int TOT_FILLER1 = N1_N2_CHECK_TOTAL + Len.N1_N2_CHECK_TOTAL;
		public static final int GEN_NUMBER = TOT_FILLER1 + Len.TOT_FILLER1;
		public static final int GEN_DATE = GEN_NUMBER + Len.GEN_NUMBER;
		public static final int INPAT_TYPE_SERVICE = GEN_DATE + Len.GEN_DATE;
		public static final int PERF_PROVIDER_NAME = INPAT_TYPE_SERVICE + Len.INPAT_TYPE_SERVICE;
		public static final int CLM_INS_LN_CD = PERF_PROVIDER_NAME + Len.PERF_PROVIDER_NAME;
		public static final int PMT_MDGP_PLAN_CD = CLM_INS_LN_CD + Len.CLM_INS_LN_CD;
		public static final int BEN_PLN_ID = PMT_MDGP_PLAN_CD + Len.PMT_MDGP_PLAN_CD;
		public static final int CLM_FROM_SERVICE_DATE = BEN_PLN_ID + Len.BEN_PLN_ID;
		public static final int CLM_THRU_SERVICE_DATE = CLM_FROM_SERVICE_DATE + Len.CLM_FROM_SERVICE_DATE;
		public static final int TOT_FILLER2 = CLM_THRU_SERVICE_DATE + Len.CLM_THRU_SERVICE_DATE;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int FROM_SERVICE_DATE_CC = 2;
		public static final int FROM_SERVICE_DATE_YY = 2;
		public static final int FLR1 = 1;
		public static final int FROM_SERVICE_DATE_MM = 2;
		public static final int FROM_SERVICE_DATE_DD = 2;
		public static final int THRU_SERVICE_DATE_CC = 2;
		public static final int THRU_SERVICE_DATE_YY = 2;
		public static final int THRU_SERVICE_DATE_MM = 2;
		public static final int THUR_SERVICE_DATE_DD = 2;
		public static final int EXAMINER_ACTION = 1;
		public static final int TYPE_SERVICE = 2;
		public static final int PLACE_SERVICE = 2;
		public static final int BENEFIT_TYPE = 3;
		public static final int DNL_REMARK = 3;
		public static final int RVW_BY_CD = 3;
		public static final int ADJD_PROV_STAT_CD = 3;
		public static final int LI_ALW_CHG_AM = 11;
		public static final int INDUS_CD = 6;
		public static final int AMOUNT_CHARGED = 9;
		public static final int LINE_NOT_COVRD_AMT = 9;
		public static final int LINE_OTHR_INSUR = 10;
		public static final int OTHER_INSURE_COVERED = 9;
		public static final int OTHER_INSURE_PAID = 9;
		public static final int OTHER_INSURE_SAVINGS = 9;
		public static final int CALC1_AMOUNT_PAID = 9;
		public static final int CALC1_DEDUCTIBLE = 9;
		public static final int CALC1_COPAY = 5;
		public static final int CALC1_COINSURANCE = 9;
		public static final int CALC1_CORP_CODE = 1;
		public static final int CALC1_PROD_CODE = 2;
		public static final int CALC1_TYPE_BENEFIT = 3;
		public static final int CALC1_SPEC_BENEFIT = 4;
		public static final int CALC1_COV_LOB = 1;
		public static final int CALC1_PAY_CODE = 1;
		public static final int CALC1_TYPE_CONTRACT = 2;
		public static final int CALC1_EXPLN_CD = 1;
		public static final int CALC2_AMOUNT_PAID = 9;
		public static final int CALC2_DEDUCTIBLE = 9;
		public static final int CALC2_COPAY = 5;
		public static final int CALC2_COINSURANCE = 9;
		public static final int CALC2_CORP_CODE = 1;
		public static final int CALC2_PROD_CODE = 2;
		public static final int CALC2_TYPE_BENEFIT = 3;
		public static final int CALC2_SPEC_BENEFIT = 4;
		public static final int CALC2_COV_LOB = 1;
		public static final int CALC2_PAY_CODE = 1;
		public static final int CALC2_TYPE_CONTRACT = 2;
		public static final int CALC2_EXPLN_CD = 1;
		public static final int CALC3_AMOUNT_PAID = 9;
		public static final int CALC3_DEDUCTIBLE = 9;
		public static final int CALC3_COPAY = 5;
		public static final int CALC3_COINSURANCE = 9;
		public static final int CALC3_CORP_CODE = 1;
		public static final int CALC3_PROD_CODE = 2;
		public static final int CALC3_TYPE_BENEFIT = 3;
		public static final int CALC3_SPEC_BENEFIT = 4;
		public static final int CALC3_COV_LOB = 1;
		public static final int CALC3_PAY_CODE = 1;
		public static final int CALC3_TYPE_CONTRACT = 2;
		public static final int CALC3_EXPLN_CD = 1;
		public static final int CALC4_AMOUNT_PAID = 9;
		public static final int CALC4_DEDUCTIBLE = 9;
		public static final int CALC4_COPAY = 5;
		public static final int CALC4_COINSURANCE = 9;
		public static final int CALC4_CORP_CODE = 1;
		public static final int CALC4_PROD_CODE = 2;
		public static final int CALC4_TYPE_BENEFIT = 3;
		public static final int CALC4_SPEC_BENEFIT = 4;
		public static final int CALC4_COV_LOB = 1;
		public static final int CALC4_PAY_CODE = 1;
		public static final int CALC4_TYPE_CONTRACT = 2;
		public static final int CALC4_EXPLN_CD = 1;
		public static final int CALC5_AMOUNT_PAID = 9;
		public static final int CALC5_DEDUCTIBLE = 9;
		public static final int CALC5_COPAY = 5;
		public static final int CALC5_COINSURANCE = 9;
		public static final int CALC5_CORP_CODE = 1;
		public static final int CALC5_PROD_CODE = 2;
		public static final int CALC5_TYPE_BENEFIT = 3;
		public static final int CALC5_SPEC_BENEFIT = 4;
		public static final int CALC5_COV_LOB = 1;
		public static final int CALC5_PAY_CODE = 1;
		public static final int CALC5_TYPE_CONTRACT = 2;
		public static final int CALC5_EXPLN_CD = 1;
		public static final int CALC6_AMOUNT_PAID = 9;
		public static final int CALC6_DEDUCTIBLE = 9;
		public static final int CALC6_COPAY = 5;
		public static final int CALC6_COINSURANCE = 9;
		public static final int CALC6_CORP_CODE = 1;
		public static final int CALC6_PROD_CODE = 2;
		public static final int CALC6_TYPE_BENEFIT = 3;
		public static final int CALC6_SPEC_BENEFIT = 4;
		public static final int CALC6_COV_LOB = 1;
		public static final int CALC6_PAY_CODE = 1;
		public static final int CALC6_TYPE_CONTRACT = 2;
		public static final int CALC6_EXPLN_CD = 1;
		public static final int CALC7_AMOUNT_PAID = 9;
		public static final int CALC7_DEDUCTIBLE = 9;
		public static final int CALC7_COPAY = 5;
		public static final int CALC7_COINSURANCE = 9;
		public static final int CALC7_CORP_CODE = 1;
		public static final int CALC7_PROD_CODE = 2;
		public static final int CALC7_TYPE_BENEFIT = 3;
		public static final int CALC7_SPEC_BENEFIT = 4;
		public static final int CALC7_COV_LOB = 1;
		public static final int CALC7_PAY_CODE = 1;
		public static final int CALC7_TYPE_CONTRACT = 2;
		public static final int CALC7_EXPLN_CD = 1;
		public static final int CALC8_AMOUNT_PAID = 9;
		public static final int CALC8_DEDUCTIBLE = 9;
		public static final int CALC8_COPAY = 5;
		public static final int CALC8_COINSURANCE = 9;
		public static final int CALC8_CORP_CODE = 1;
		public static final int CALC8_PROD_CODE = 2;
		public static final int CALC8_TYPE_BENEFIT = 3;
		public static final int CALC8_SPEC_BENEFIT = 4;
		public static final int CALC8_COV_LOB = 1;
		public static final int CALC8_PAY_CODE = 1;
		public static final int CALC8_TYPE_CONTRACT = 2;
		public static final int CALC8_EXPLN_CD = 1;
		public static final int CALC9_AMOUNT_PAID = 9;
		public static final int CALC9_DEDUCTIBLE = 9;
		public static final int CALC9_COPAY = 5;
		public static final int CALC9_COINSURANCE = 9;
		public static final int CALC9_CORP_CODE = 1;
		public static final int CALC9_PROD_CODE = 2;
		public static final int CALC9_TYPE_BENEFIT = 3;
		public static final int CALC9_SPEC_BENEFIT = 4;
		public static final int CALC9_COV_LOB = 1;
		public static final int CALC9_PAY_CODE = 1;
		public static final int CALC9_TYPE_CONTRACT = 2;
		public static final int CALC9_EXPLN_CD = 1;
		public static final int DEDUCT_CARRYOVER = 1;
		public static final int FEP_DENT_PPO_PRV_WRTOFF = 8;
		public static final int FEP_DENT_PPO_INS_LIAB = 8;
		public static final int LINE_EXPLANATION_CODE = 1;
		public static final int SELF_REFERRAL_INDICATOR = 1;
		public static final int NOTES1 = 2;
		public static final int WHICH_NOTES_TABLE_NOTE1 = 16;
		public static final int NOTES2 = 2;
		public static final int WHICH_NOTES_TABLE_NOTE2 = 16;
		public static final int NOTES3 = 2;
		public static final int WHICH_NOTES_TABLE_NOTE3 = 16;
		public static final int NOTES4 = 2;
		public static final int WHICH_NOTES_TABLE_NOTE4 = 16;
		public static final int NOTES5 = 2;
		public static final int WHICH_NOTES_TABLE_NOTE5 = 16;
		public static final int VOID_LI_PD_AM = 11;
		public static final int VOID_DNL_REMARK = 3;
		public static final int SHARE_DATE_FLAG = 1;
		public static final int OVERRIDE_CODE1 = 3;
		public static final int OVERRIDE_CODE2 = 3;
		public static final int OVERRIDE_CODE3 = 3;
		public static final int OVERRIDE_CODE4 = 3;
		public static final int OVERRIDE_CODE5 = 3;
		public static final int OVERRIDE_CODE6 = 3;
		public static final int OVERRIDE_CODE7 = 3;
		public static final int OVERRIDE_CODE8 = 3;
		public static final int OVERRIDE_CODE9 = 3;
		public static final int OVERRIDE_CODE10 = 3;
		public static final int ALT_INS_ID = 12;
		public static final int PA_ID_TO_ACUM_ID = 12;
		public static final int MEM_ID_TO_ACUM_ID = 14;
		public static final int BILL_PVDR_NUM = 10;
		public static final int BILL_PVDR_LOB = 1;
		public static final int BILL_PVDR_STATUS = 1;
		public static final int BILL_PVDR_EIN = 9;
		public static final int BILL_PVDR_SSN = 9;
		public static final int BILL_PVDR_ZIP = 9;
		public static final int BILL_PVDR_SPEC = 4;
		public static final int BILL_PVDR_NAME_LST = 15;
		public static final int TOTAL_CHARGE = 10;
		public static final int TOTAL_NON_COVRD = 10;
		public static final int TOTAL_OTHR_INSUR = 10;
		public static final int TOTAL_DEDUCT = 7;
		public static final int TOTAL_COPAY = 5;
		public static final int TOTAL_COINSUR = 9;
		public static final int TOTAL_PAT_RESPONS = 10;
		public static final int TOTAL_PROV_WRITEOFF = 10;
		public static final int TOTAL_AMOUNT_PAID = 11;
		public static final int PAID_TO = 6;
		public static final int N1_N2_CHECK_TOTAL = 11;
		public static final int TOT_FILLER1 = 5;
		public static final int GEN_NUMBER = 15;
		public static final int GEN_DATE = 7;
		public static final int INPAT_TYPE_SERVICE = 2;
		public static final int PERF_PROVIDER_NAME = 25;
		public static final int CLM_INS_LN_CD = 3;
		public static final int PMT_MDGP_PLAN_CD = 1;
		public static final int BEN_PLN_ID = 4;
		public static final int CLM_FROM_SERVICE_DATE = 10;
		public static final int CLM_THRU_SERVICE_DATE = 10;
		public static final int BILLING_PROVIDER_DATA = BILL_PVDR_NUM + BILL_PVDR_LOB + BILL_PVDR_STATUS + BILL_PVDR_EIN + BILL_PVDR_SSN
				+ BILL_PVDR_ZIP + BILL_PVDR_SPEC + BILL_PVDR_NAME_LST;
		public static final int TOT_FILLER2 = 509;
		public static final int NF05_TOTAL_RECORD = BILLING_PROVIDER_DATA + TOTAL_CHARGE + TOTAL_NON_COVRD + TOTAL_OTHR_INSUR + TOTAL_DEDUCT
				+ TOTAL_COPAY + TOTAL_COINSUR + TOTAL_PAT_RESPONS + TOTAL_PROV_WRITEOFF + TOTAL_AMOUNT_PAID + PAID_TO + N1_N2_CHECK_TOTAL
				+ TOT_FILLER1 + GEN_NUMBER + GEN_DATE + INPAT_TYPE_SERVICE + PERF_PROVIDER_NAME + CLM_INS_LN_CD + PMT_MDGP_PLAN_CD + BEN_PLN_ID
				+ CLM_FROM_SERVICE_DATE + CLM_THRU_SERVICE_DATE + TOT_FILLER2;
		public static final int FROM_SERVICE_DATE = FROM_SERVICE_DATE_CC + FROM_SERVICE_DATE_YY + FROM_SERVICE_DATE_MM + FROM_SERVICE_DATE_DD
				+ 2 * FLR1;
		public static final int THRU_SERVICE_DATE = THRU_SERVICE_DATE_CC + THRU_SERVICE_DATE_YY + THRU_SERVICE_DATE_MM + THUR_SERVICE_DATE_DD
				+ 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int LI_ALW_CHG_AM = 9;
			public static final int AMOUNT_CHARGED = 7;
			public static final int LINE_NOT_COVRD_AMT = 7;
			public static final int LINE_OTHR_INSUR = 8;
			public static final int OTHER_INSURE_COVERED = 7;
			public static final int OTHER_INSURE_PAID = 7;
			public static final int OTHER_INSURE_SAVINGS = 7;
			public static final int CALC1_AMOUNT_PAID = 7;
			public static final int CALC1_DEDUCTIBLE = 7;
			public static final int CALC1_COPAY = 3;
			public static final int CALC1_COINSURANCE = 7;
			public static final int CALC2_AMOUNT_PAID = 7;
			public static final int CALC2_DEDUCTIBLE = 7;
			public static final int CALC2_COPAY = 3;
			public static final int CALC2_COINSURANCE = 7;
			public static final int CALC3_AMOUNT_PAID = 7;
			public static final int CALC3_DEDUCTIBLE = 7;
			public static final int CALC3_COPAY = 3;
			public static final int CALC3_COINSURANCE = 7;
			public static final int CALC4_AMOUNT_PAID = 7;
			public static final int CALC4_DEDUCTIBLE = 7;
			public static final int CALC4_COPAY = 3;
			public static final int CALC4_COINSURANCE = 7;
			public static final int CALC5_AMOUNT_PAID = 7;
			public static final int CALC5_DEDUCTIBLE = 7;
			public static final int CALC5_COPAY = 3;
			public static final int CALC5_COINSURANCE = 7;
			public static final int CALC6_AMOUNT_PAID = 7;
			public static final int CALC6_DEDUCTIBLE = 7;
			public static final int CALC6_COPAY = 3;
			public static final int CALC6_COINSURANCE = 7;
			public static final int CALC7_AMOUNT_PAID = 7;
			public static final int CALC7_DEDUCTIBLE = 7;
			public static final int CALC7_COPAY = 3;
			public static final int CALC7_COINSURANCE = 7;
			public static final int CALC8_AMOUNT_PAID = 7;
			public static final int CALC8_DEDUCTIBLE = 7;
			public static final int CALC8_COPAY = 3;
			public static final int CALC8_COINSURANCE = 7;
			public static final int CALC9_AMOUNT_PAID = 7;
			public static final int CALC9_DEDUCTIBLE = 7;
			public static final int CALC9_COPAY = 3;
			public static final int CALC9_COINSURANCE = 7;
			public static final int FEP_DENT_PPO_PRV_WRTOFF = 6;
			public static final int FEP_DENT_PPO_INS_LIAB = 6;
			public static final int VOID_LI_PD_AM = 9;
			public static final int TOTAL_CHARGE = 8;
			public static final int TOTAL_NON_COVRD = 8;
			public static final int TOTAL_OTHR_INSUR = 8;
			public static final int TOTAL_DEDUCT = 5;
			public static final int TOTAL_COPAY = 3;
			public static final int TOTAL_COINSUR = 7;
			public static final int TOTAL_PAT_RESPONS = 8;
			public static final int TOTAL_PROV_WRITEOFF = 8;
			public static final int TOTAL_AMOUNT_PAID = 9;
			public static final int N1_N2_CHECK_TOTAL = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int LI_ALW_CHG_AM = 2;
			public static final int AMOUNT_CHARGED = 2;
			public static final int LINE_NOT_COVRD_AMT = 2;
			public static final int LINE_OTHR_INSUR = 2;
			public static final int OTHER_INSURE_COVERED = 2;
			public static final int OTHER_INSURE_PAID = 2;
			public static final int OTHER_INSURE_SAVINGS = 2;
			public static final int CALC1_AMOUNT_PAID = 2;
			public static final int CALC1_DEDUCTIBLE = 2;
			public static final int CALC1_COPAY = 2;
			public static final int CALC1_COINSURANCE = 2;
			public static final int CALC2_AMOUNT_PAID = 2;
			public static final int CALC2_DEDUCTIBLE = 2;
			public static final int CALC2_COPAY = 2;
			public static final int CALC2_COINSURANCE = 2;
			public static final int CALC3_AMOUNT_PAID = 2;
			public static final int CALC3_DEDUCTIBLE = 2;
			public static final int CALC3_COPAY = 2;
			public static final int CALC3_COINSURANCE = 2;
			public static final int CALC4_AMOUNT_PAID = 2;
			public static final int CALC4_DEDUCTIBLE = 2;
			public static final int CALC4_COPAY = 2;
			public static final int CALC4_COINSURANCE = 2;
			public static final int CALC5_AMOUNT_PAID = 2;
			public static final int CALC5_DEDUCTIBLE = 2;
			public static final int CALC5_COPAY = 2;
			public static final int CALC5_COINSURANCE = 2;
			public static final int CALC6_AMOUNT_PAID = 2;
			public static final int CALC6_DEDUCTIBLE = 2;
			public static final int CALC6_COPAY = 2;
			public static final int CALC6_COINSURANCE = 2;
			public static final int CALC7_AMOUNT_PAID = 2;
			public static final int CALC7_DEDUCTIBLE = 2;
			public static final int CALC7_COPAY = 2;
			public static final int CALC7_COINSURANCE = 2;
			public static final int CALC8_AMOUNT_PAID = 2;
			public static final int CALC8_DEDUCTIBLE = 2;
			public static final int CALC8_COPAY = 2;
			public static final int CALC8_COINSURANCE = 2;
			public static final int CALC9_AMOUNT_PAID = 2;
			public static final int CALC9_DEDUCTIBLE = 2;
			public static final int CALC9_COPAY = 2;
			public static final int CALC9_COINSURANCE = 2;
			public static final int FEP_DENT_PPO_PRV_WRTOFF = 2;
			public static final int FEP_DENT_PPO_INS_LIAB = 2;
			public static final int VOID_LI_PD_AM = 2;
			public static final int TOTAL_CHARGE = 2;
			public static final int TOTAL_NON_COVRD = 2;
			public static final int TOTAL_OTHR_INSUR = 2;
			public static final int TOTAL_DEDUCT = 2;
			public static final int TOTAL_COPAY = 2;
			public static final int TOTAL_COINSUR = 2;
			public static final int TOTAL_PAT_RESPONS = 2;
			public static final int TOTAL_PROV_WRITEOFF = 2;
			public static final int TOTAL_AMOUNT_PAID = 2;
			public static final int N1_N2_CHECK_TOTAL = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
