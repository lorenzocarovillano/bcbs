/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [TRIGGER2]
 * 
 */
public interface ITrigger2 extends BaseSqlTo {

	/**
	 * Host Variable TRIG-PROD-IND
	 * 
	 */
	String getTrigProdInd();

	void setTrigProdInd(String trigProdInd);

	/**
	 * Host Variable TRIG-CLM-CNTL-ID
	 * 
	 */
	String getTrigClmCntlId();

	void setTrigClmCntlId(String trigClmCntlId);

	/**
	 * Host Variable TRIG-CLM-CNTL-SFX
	 * 
	 */
	char getTrigClmCntlSfx();

	void setTrigClmCntlSfx(char trigClmCntlSfx);

	/**
	 * Host Variable WS-TRIG-PMT-ID
	 * 
	 */
	short getWsTrigPmtId();

	void setWsTrigPmtId(short wsTrigPmtId);

	/**
	 * Host Variable TRIG-HIST-BILL-PROV-NPI-ID
	 * 
	 */
	String getTrigHistBillProvNpiId();

	void setTrigHistBillProvNpiId(String trigHistBillProvNpiId);

	/**
	 * Host Variable TRIG-LOCAL-BILL-PROV-LOB-CD
	 * 
	 */
	char getTrigLocalBillProvLobCd();

	void setTrigLocalBillProvLobCd(char trigLocalBillProvLobCd);

	/**
	 * Host Variable TRIG-LOCAL-BILL-PROV-ID
	 * 
	 */
	String getTrigLocalBillProvId();

	void setTrigLocalBillProvId(String trigLocalBillProvId);

	/**
	 * Host Variable TRIG-BILL-PROV-ID-QLF-CD
	 * 
	 */
	String getTrigBillProvIdQlfCd();

	void setTrigBillProvIdQlfCd(String trigBillProvIdQlfCd);

	/**
	 * Host Variable TRIG-BILL-PROV-SPEC-CD
	 * 
	 */
	String getTrigBillProvSpecCd();

	void setTrigBillProvSpecCd(String trigBillProvSpecCd);

	/**
	 * Host Variable TRIG-HIST-PERF-PROV-NPI-ID
	 * 
	 */
	String getTrigHistPerfProvNpiId();

	void setTrigHistPerfProvNpiId(String trigHistPerfProvNpiId);

	/**
	 * Host Variable TRIG-LOCAL-PERF-PROV-LOB-CD
	 * 
	 */
	char getTrigLocalPerfProvLobCd();

	void setTrigLocalPerfProvLobCd(char trigLocalPerfProvLobCd);

	/**
	 * Host Variable TRIG-LOCAL-PERF-PROV-ID
	 * 
	 */
	String getTrigLocalPerfProvId();

	void setTrigLocalPerfProvId(String trigLocalPerfProvId);

	/**
	 * Host Variable TRIG-INS-ID
	 * 
	 */
	String getTrigInsId();

	void setTrigInsId(String trigInsId);

	/**
	 * Host Variable TRIG-MEMBER-ID
	 * 
	 */
	String getTrigMemberId();

	void setTrigMemberId(String trigMemberId);

	/**
	 * Host Variable TRIG-CORP-RCV-DT
	 * 
	 */
	String getTrigCorpRcvDt();

	void setTrigCorpRcvDt(String trigCorpRcvDt);

	/**
	 * Host Variable TRIG-PMT-ADJD-DT
	 * 
	 */
	String getTrigPmtAdjdDt();

	void setTrigPmtAdjdDt(String trigPmtAdjdDt);

	/**
	 * Host Variable TRIG-PMT-FRM-SERV-DT
	 * 
	 */
	String getTrigPmtFrmServDt();

	void setTrigPmtFrmServDt(String trigPmtFrmServDt);

	/**
	 * Host Variable TRIG-PMT-THR-SERV-DT
	 * 
	 */
	String getTrigPmtThrServDt();

	void setTrigPmtThrServDt(String trigPmtThrServDt);

	/**
	 * Host Variable TRIG-BILL-FRM-DT
	 * 
	 */
	String getTrigBillFrmDt();

	void setTrigBillFrmDt(String trigBillFrmDt);

	/**
	 * Host Variable TRIG-BILL-THR-DT
	 * 
	 */
	String getTrigBillThrDt();

	void setTrigBillThrDt(String trigBillThrDt);

	/**
	 * Host Variable TRIG-ASG-CD
	 * 
	 */
	String getTrigAsgCd();

	void setTrigAsgCd(String trigAsgCd);

	/**
	 * Host Variable WS-TRIG-CLM-PD-AM
	 * 
	 */
	AfDecimal getWsTrigClmPdAm();

	void setWsTrigClmPdAm(AfDecimal wsTrigClmPdAm);

	/**
	 * Host Variable TRIG-ADJ-TYP-CD
	 * 
	 */
	char getTrigAdjTypCd();

	void setTrigAdjTypCd(char trigAdjTypCd);

	/**
	 * Host Variable TRIG-KCAPS-TEAM-NM
	 * 
	 */
	String getTrigKcapsTeamNm();

	void setTrigKcapsTeamNm(String trigKcapsTeamNm);

	/**
	 * Host Variable TRIG-KCAPS-USE-ID
	 * 
	 */
	String getTrigKcapsUseId();

	void setTrigKcapsUseId(String trigKcapsUseId);

	/**
	 * Host Variable TRIG-HIST-LOAD-CD
	 * 
	 */
	char getTrigHistLoadCd();

	void setTrigHistLoadCd(char trigHistLoadCd);

	/**
	 * Host Variable TRIG-PMT-BK-PROD-CD
	 * 
	 */
	char getTrigPmtBkProdCd();

	void setTrigPmtBkProdCd(char trigPmtBkProdCd);

	/**
	 * Host Variable TRIG-DRG-CD
	 * 
	 */
	String getTrigDrgCd();

	void setTrigDrgCd(String trigDrgCd);

	/**
	 * Host Variable WS-TRIG-SCHDL-DRG-ALW-AM
	 * 
	 */
	AfDecimal getWsTrigSchdlDrgAlwAm();

	void setWsTrigSchdlDrgAlwAm(AfDecimal wsTrigSchdlDrgAlwAm);

	/**
	 * Host Variable WS-TRIG-ALT-DRG-ALW-AM
	 * 
	 */
	AfDecimal getWsTrigAltDrgAlwAm();

	void setWsTrigAltDrgAlwAm(AfDecimal wsTrigAltDrgAlwAm);

	/**
	 * Host Variable WS-TRIG-OVER-DRG-ALW-AM
	 * 
	 */
	AfDecimal getWsTrigOverDrgAlwAm();

	void setWsTrigOverDrgAlwAm(AfDecimal wsTrigOverDrgAlwAm);

	/**
	 * Host Variable TRIG-GMIS-INDICATOR
	 * 
	 */
	String getTrigGmisIndicator();

	void setTrigGmisIndicator(String trigGmisIndicator);

	/**
	 * Host Variable TRIG-PMT-OI-IN
	 * 
	 */
	char getTrigPmtOiIn();

	void setTrigPmtOiIn(char trigPmtOiIn);

	/**
	 * Host Variable TRIG-PAT-ACT-MED-REC-ID
	 * 
	 */
	String getTrigPatActMedRecId();

	void setTrigPatActMedRecId(String trigPatActMedRecId);

	/**
	 * Host Variable TRIG-PGM-AREA-CD
	 * 
	 */
	String getTrigPgmAreaCd();

	void setTrigPgmAreaCd(String trigPgmAreaCd);

	/**
	 * Host Variable TRIG-FIN-CD
	 * 
	 */
	String getTrigFinCd();

	void setTrigFinCd(String trigFinCd);

	/**
	 * Host Variable TRIG-ADJD-PROV-STAT-CD
	 * 
	 */
	char getTrigAdjdProvStatCd();

	void setTrigAdjdProvStatCd(char trigAdjdProvStatCd);

	/**
	 * Host Variable TRIG-ITS-CLM-TYP-CD
	 * 
	 */
	char getTrigItsClmTypCd();

	void setTrigItsClmTypCd(char trigItsClmTypCd);

	/**
	 * Host Variable TRIG-PRMPT-PAY-DAY-CD
	 * 
	 */
	String getTrigPrmptPayDayCd();

	void setTrigPrmptPayDayCd(String trigPrmptPayDayCd);

	/**
	 * Host Variable TRIG-PRMPT-PAY-OVRD-CD
	 * 
	 */
	char getTrigPrmptPayOvrdCd();

	void setTrigPrmptPayOvrdCd(char trigPrmptPayOvrdCd);

	/**
	 * Host Variable WS-ACCRUED-PRMPT-PAY-INT-AM
	 * 
	 */
	AfDecimal getWsAccruedPrmptPayIntAm();

	void setWsAccruedPrmptPayIntAm(AfDecimal wsAccruedPrmptPayIntAm);

	/**
	 * Host Variable TRIG-PROV-UNWRP-DT
	 * 
	 */
	String getTrigProvUnwrpDt();

	void setTrigProvUnwrpDt(String trigProvUnwrpDt);

	/**
	 * Host Variable TRIG-GRP-ID
	 * 
	 */
	String getTrigGrpId();

	void setTrigGrpId(String trigGrpId);

	/**
	 * Host Variable TRIG-TYP-GRP-CD
	 * 
	 */
	String getTrigTypGrpCd();

	void setTrigTypGrpCd(String trigTypGrpCd);

	/**
	 * Host Variable TRIG-NPI-CD
	 * 
	 */
	char getTrigNpiCd();

	void setTrigNpiCd(char trigNpiCd);

	/**
	 * Host Variable TRIG-CLAIM-LOB-CD
	 * 
	 */
	char getTrigClaimLobCd();

	void setTrigClaimLobCd(char trigClaimLobCd);

	/**
	 * Host Variable TRIG-RATE-CD
	 * 
	 */
	String getTrigRateCd();

	void setTrigRateCd(String trigRateCd);

	/**
	 * Host Variable TRIG-NTWRK-CD
	 * 
	 */
	String getTrigNtwrkCd();

	void setTrigNtwrkCd(String trigNtwrkCd);

	/**
	 * Host Variable TRIG-BASE-CN-ARNG-CD
	 * 
	 */
	String getTrigBaseCnArngCd();

	void setTrigBaseCnArngCd(String trigBaseCnArngCd);

	/**
	 * Host Variable TRIG-PRIM-CN-ARNG-CD
	 * 
	 */
	String getTrigPrimCnArngCd();

	void setTrigPrimCnArngCd(String trigPrimCnArngCd);

	/**
	 * Host Variable TRIG-ENR-CL-CD
	 * 
	 */
	String getTrigEnrClCd();

	void setTrigEnrClCd(String trigEnrClCd);

	/**
	 * Host Variable WS-TRIG-LST-FNL-PMT-PT-ID
	 * 
	 */
	short getWsTrigLstFnlPmtPtId();

	void setWsTrigLstFnlPmtPtId(short wsTrigLstFnlPmtPtId);

	/**
	 * Host Variable TRIG-STAT-ADJ-PREV-PMT
	 * 
	 */
	char getTrigStatAdjPrevPmt();

	void setTrigStatAdjPrevPmt(char trigStatAdjPrevPmt);

	/**
	 * Host Variable TRIG-EFT-IND
	 * 
	 */
	char getTrigEftInd();

	void setTrigEftInd(char trigEftInd);

	/**
	 * Host Variable TRIG-EFT-ACCOUNT-TYPE
	 * 
	 */
	char getTrigEftAccountType();

	void setTrigEftAccountType(char trigEftAccountType);

	/**
	 * Host Variable TRIG-EFT-ACCT
	 * 
	 */
	String getTrigEftAcct();

	void setTrigEftAcct(String trigEftAcct);

	/**
	 * Host Variable TRIG-EFT-TRANS
	 * 
	 */
	String getTrigEftTrans();

	void setTrigEftTrans(String trigEftTrans);

	/**
	 * Host Variable TRIG-ALPH-PRFX-CD
	 * 
	 */
	String getTrigAlphPrfxCd();

	void setTrigAlphPrfxCd(String trigAlphPrfxCd);

	/**
	 * Host Variable TRIG-GL-OFST-ORIG-CD
	 * 
	 */
	String getTrigGlOfstOrigCd();

	void setTrigGlOfstOrigCd(String trigGlOfstOrigCd);

	/**
	 * Host Variable WS-TRIG-GL-SOTE-ORIG-CD
	 * 
	 */
	short getWsTrigGlSoteOrigCd();

	void setWsTrigGlSoteOrigCd(short wsTrigGlSoteOrigCd);

	/**
	 * Host Variable TRIG-VOID-CD
	 * 
	 */
	char getTrigVoidCd();

	void setTrigVoidCd(char trigVoidCd);

	/**
	 * Host Variable TRIG-ITS-INS-ID
	 * 
	 */
	String getTrigItsInsId();

	void setTrigItsInsId(String trigItsInsId);

	/**
	 * Host Variable TRIG-ADJ-RESP-CD
	 * 
	 */
	char getTrigAdjRespCd();

	void setTrigAdjRespCd(char trigAdjRespCd);

	/**
	 * Host Variable TRIG-ADJ-TRCK-CD
	 * 
	 */
	String getTrigAdjTrckCd();

	void setTrigAdjTrckCd(String trigAdjTrckCd);

	/**
	 * Host Variable TRIG-CLMCK-ADJ-STAT-CD
	 * 
	 */
	char getTrigClmckAdjStatCd();

	void setTrigClmckAdjStatCd(char trigClmckAdjStatCd);

	/**
	 * Host Variable TRIG-837-BILL-PROV-NPI-ID
	 * 
	 */
	String getTrig837BillProvNpiId();

	void setTrig837BillProvNpiId(String trig837BillProvNpiId);

	/**
	 * Host Variable TRIG-837-PERF-PROV-NPI-ID
	 * 
	 */
	String getTrig837PerfProvNpiId();

	void setTrig837PerfProvNpiId(String trig837PerfProvNpiId);

	/**
	 * Host Variable TRIG-ITS-CK
	 * 
	 */
	char getTrigItsCk();

	void setTrigItsCk(char trigItsCk);

	/**
	 * Host Variable TRIG-DATE-COVERAGE-LAPSED
	 * 
	 */
	String getTrigDateCoverageLapsed();

	void setTrigDateCoverageLapsed(String trigDateCoverageLapsed);

	/**
	 * Host Variable TRIG-OI-PAY-NM
	 * 
	 */
	String getTrigOiPayNm();

	void setTrigOiPayNm(String trigOiPayNm);

	/**
	 * Host Variable TRIG-CORR-PRIORITY-SUB-ID
	 * 
	 */
	String getTrigCorrPrioritySubId();

	void setTrigCorrPrioritySubId(String trigCorrPrioritySubId);

	/**
	 * Host Variable TRIG-HIPAA-VERSION-FORMAT-ID
	 * 
	 */
	String getTrigHipaaVersionFormatId();

	void setTrigHipaaVersionFormatId(String trigHipaaVersionFormatId);

	/**
	 * Host Variable TRIG-VBR-IN
	 * 
	 */
	char getTrigVbrIn();

	void setTrigVbrIn(char trigVbrIn);

	/**
	 * Host Variable TRIG-MRKT-PKG-CD
	 * 
	 */
	String getTrigMrktPkgCd();

	void setTrigMrktPkgCd(String trigMrktPkgCd);
};
