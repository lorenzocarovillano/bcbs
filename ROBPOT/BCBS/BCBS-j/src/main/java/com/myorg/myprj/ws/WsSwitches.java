/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-SWITCHES<br>
 * Variable: WS-SWITCHES from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSwitches {

	//==== PROPERTIES ====
	//Original name: EOF-TRIGGER-SW
	private String eofTriggerSw = "NO";
	public static final String TRIGGER_EOF = "YES";
	//Original name: COVID19-PROCCD-SW
	private boolean covid19ProccdSw = false;
	//Original name: EOF-BUSDATE
	private String eofBusdate = "NO";
	//Original name: HOLD-COVID-END-DATE
	private String holdCovidEndDate = "";
	//Original name: GET-VOID-FOR-EXP
	private char getVoidForExp = 'N';
	//Original name: FIRST-TIME-CHECK
	private String firstTimeCheck = "NO";
	//Original name: LAST-TIME-CHECK
	private String lastTimeCheck = "NO";
	//Original name: EXPENSE-SW
	private char expenseSw = 'Y';
	//Original name: BYPASS-SW
	private char bypassSw = 'N';
	//Original name: ATTN-SYS-SW
	private char attnSysSw = 'N';
	//Original name: HOLD-LINES-PRC-SW
	private char holdLinesPrcSw = 'N';
	//Original name: HOLD-EXP-PRC-SW
	private char holdExpPrcSw = 'N';
	//Original name: ALL-DENIED-SW
	private String allDeniedSw = "NO";
	public static final String ALL_DENIED = "YES";
	//Original name: WS-LOB-PROV-FOUND-SW
	private boolean wsLobProvFoundSw = false;
	//Original name: WS-END-OF-CURSOR-SW
	private char wsEndOfCursorSw = 'N';
	public static final char END_OF_CURSOR = 'Y';
	//Original name: NF0738-WRAP-SW
	private String nf0738WrapSw = "NO";
	public static final String WRAP_NF0738 = "YES";
	//Original name: VOID-REPAY-SW
	private String voidRepaySw = "NO";
	public static final String VOID_REPAY = "YES";
	//Original name: PROMPT-PAYMENT-CALC
	private String promptPaymentCalc = "NO";
	//Original name: PP-EXP-DIVISION-CODE
	private char ppExpDivisionCode = Types.SPACE_CHAR;
	//Original name: USE-RPT-INDC-1
	private String useRptIndc1 = "NO";
	//Original name: USE-RPT-INDC-2
	private String useRptIndc2 = "NO";
	//Original name: SRS-PROVIDER-SW
	private String srsProviderSw = DefaultValues.stringVal(Len.SRS_PROVIDER_SW);
	public static final String SRS_PROVIDER = "YES";

	//==== METHODS ====
	public void setEofTriggerSw(String eofTriggerSw) {
		this.eofTriggerSw = Functions.subString(eofTriggerSw, Len.EOF_TRIGGER_SW);
	}

	public String getEofTriggerSw() {
		return this.eofTriggerSw;
	}

	public boolean isTriggerEof() {
		return eofTriggerSw.equals(TRIGGER_EOF);
	}

	public void setCovid19ProccdSw(boolean covid19ProccdSw) {
		this.covid19ProccdSw = covid19ProccdSw;
	}

	public boolean isCovid19ProccdSw() {
		return this.covid19ProccdSw;
	}

	public void setEofBusdate(String eofBusdate) {
		this.eofBusdate = Functions.subString(eofBusdate, Len.EOF_BUSDATE);
	}

	public String getEofBusdate() {
		return this.eofBusdate;
	}

	public void setHoldCovidEndDate(String holdCovidEndDate) {
		this.holdCovidEndDate = Functions.subString(holdCovidEndDate, Len.HOLD_COVID_END_DATE);
	}

	public void setHoldCovidEndDateFromBuffer(byte[] buffer) {
		holdCovidEndDate = MarshalByte.readString(buffer, 1, Len.HOLD_COVID_END_DATE);
	}

	public String getHoldCovidEndDate() {
		return this.holdCovidEndDate;
	}

	public void setGetVoidForExp(char getVoidForExp) {
		this.getVoidForExp = getVoidForExp;
	}

	public void setGetVoidForExpFormatted(String getVoidForExp) {
		setGetVoidForExp(Functions.charAt(getVoidForExp, Types.CHAR_SIZE));
	}

	public char getGetVoidForExp() {
		return this.getVoidForExp;
	}

	public void setFirstTimeCheck(String firstTimeCheck) {
		this.firstTimeCheck = Functions.subString(firstTimeCheck, Len.FIRST_TIME_CHECK);
	}

	public String getFirstTimeCheck() {
		return this.firstTimeCheck;
	}

	public void setLastTimeCheck(String lastTimeCheck) {
		this.lastTimeCheck = Functions.subString(lastTimeCheck, Len.LAST_TIME_CHECK);
	}

	public String getLastTimeCheck() {
		return this.lastTimeCheck;
	}

	public void setExpenseSw(char expenseSw) {
		this.expenseSw = expenseSw;
	}

	public void setExpenseSwFormatted(String expenseSw) {
		setExpenseSw(Functions.charAt(expenseSw, Types.CHAR_SIZE));
	}

	public char getExpenseSw() {
		return this.expenseSw;
	}

	public void setBypassSw(char bypassSw) {
		this.bypassSw = bypassSw;
	}

	public void setBypassSwFormatted(String bypassSw) {
		setBypassSw(Functions.charAt(bypassSw, Types.CHAR_SIZE));
	}

	public char getBypassSw() {
		return this.bypassSw;
	}

	public void setAttnSysSw(char attnSysSw) {
		this.attnSysSw = attnSysSw;
	}

	public void setAttnSysSwFormatted(String attnSysSw) {
		setAttnSysSw(Functions.charAt(attnSysSw, Types.CHAR_SIZE));
	}

	public char getAttnSysSw() {
		return this.attnSysSw;
	}

	public void setHoldLinesPrcSw(char holdLinesPrcSw) {
		this.holdLinesPrcSw = holdLinesPrcSw;
	}

	public void setHoldLinesPrcSwFormatted(String holdLinesPrcSw) {
		setHoldLinesPrcSw(Functions.charAt(holdLinesPrcSw, Types.CHAR_SIZE));
	}

	public char getHoldLinesPrcSw() {
		return this.holdLinesPrcSw;
	}

	public void setHoldExpPrcSw(char holdExpPrcSw) {
		this.holdExpPrcSw = holdExpPrcSw;
	}

	public void setHoldExpPrcSwFormatted(String holdExpPrcSw) {
		setHoldExpPrcSw(Functions.charAt(holdExpPrcSw, Types.CHAR_SIZE));
	}

	public char getHoldExpPrcSw() {
		return this.holdExpPrcSw;
	}

	public void setAllDeniedSw(String allDeniedSw) {
		this.allDeniedSw = Functions.subString(allDeniedSw, Len.ALL_DENIED_SW);
	}

	public String getAllDeniedSw() {
		return this.allDeniedSw;
	}

	public boolean isAllDenied() {
		return allDeniedSw.equals(ALL_DENIED);
	}

	public void setWsLobProvFoundSw(boolean wsLobProvFoundSw) {
		this.wsLobProvFoundSw = wsLobProvFoundSw;
	}

	public boolean isWsLobProvFoundSw() {
		return this.wsLobProvFoundSw;
	}

	public void setWsEndOfCursorSw(char wsEndOfCursorSw) {
		this.wsEndOfCursorSw = wsEndOfCursorSw;
	}

	public void setWsEndOfCursorSwFormatted(String wsEndOfCursorSw) {
		setWsEndOfCursorSw(Functions.charAt(wsEndOfCursorSw, Types.CHAR_SIZE));
	}

	public char getWsEndOfCursorSw() {
		return this.wsEndOfCursorSw;
	}

	public boolean isEndOfCursor() {
		return wsEndOfCursorSw == END_OF_CURSOR;
	}

	public void setNf0738WrapSw(String nf0738WrapSw) {
		this.nf0738WrapSw = Functions.subString(nf0738WrapSw, Len.NF0738_WRAP_SW);
	}

	public String getNf0738WrapSw() {
		return this.nf0738WrapSw;
	}

	public boolean isWrapNf0738() {
		return nf0738WrapSw.equals(WRAP_NF0738);
	}

	public void setVoidRepaySw(String voidRepaySw) {
		this.voidRepaySw = Functions.subString(voidRepaySw, Len.VOID_REPAY_SW);
	}

	public String getVoidRepaySw() {
		return this.voidRepaySw;
	}

	public boolean isVoidRepay() {
		return voidRepaySw.equals(VOID_REPAY);
	}

	public void setPromptPaymentCalc(String promptPaymentCalc) {
		this.promptPaymentCalc = Functions.subString(promptPaymentCalc, Len.PROMPT_PAYMENT_CALC);
	}

	public String getPromptPaymentCalc() {
		return this.promptPaymentCalc;
	}

	public void setPpExpDivisionCode(char ppExpDivisionCode) {
		this.ppExpDivisionCode = ppExpDivisionCode;
	}

	public char getPpExpDivisionCode() {
		return this.ppExpDivisionCode;
	}

	public void setUseRptIndc1(String useRptIndc1) {
		this.useRptIndc1 = Functions.subString(useRptIndc1, Len.USE_RPT_INDC1);
	}

	public String getUseRptIndc1() {
		return this.useRptIndc1;
	}

	public void setUseRptIndc2(String useRptIndc2) {
		this.useRptIndc2 = Functions.subString(useRptIndc2, Len.USE_RPT_INDC2);
	}

	public String getUseRptIndc2() {
		return this.useRptIndc2;
	}

	public void setSrsProviderSw(String srsProviderSw) {
		this.srsProviderSw = Functions.subString(srsProviderSw, Len.SRS_PROVIDER_SW);
	}

	public String getSrsProviderSw() {
		return this.srsProviderSw;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SRS_PROVIDER_SW = 3;
		public static final int HOLD_COVID_END_DATE = 10;
		public static final int EOF_TRIGGER_SW = 3;
		public static final int FIRST_TIME_CHECK = 3;
		public static final int NF0738_WRAP_SW = 3;
		public static final int LAST_TIME_CHECK = 3;
		public static final int ALL_DENIED_SW = 3;
		public static final int USE_RPT_INDC1 = 3;
		public static final int USE_RPT_INDC2 = 3;
		public static final int PROMPT_PAYMENT_CALC = 3;
		public static final int VOID_REPAY_SW = 3;
		public static final int EOF_BUSDATE = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
