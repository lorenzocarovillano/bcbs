/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: HOLD-PROVIDER-ADRESS-STUFF<br>
 * Variable: HOLD-PROVIDER-ADRESS-STUFF from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class HoldProviderAdressStuff {

	//==== PROPERTIES ====
	//Original name: HOLD-BI-ADDRESS
	private String biAddress = DefaultValues.stringVal(Len.BI_ADDRESS);
	//Original name: HOLD-BI-ZIP
	private String biZip = DefaultValues.stringVal(Len.BI_ZIP);

	//==== METHODS ====
	public void setBiAddress(String biAddress) {
		this.biAddress = Functions.subString(biAddress, Len.BI_ADDRESS);
	}

	public String getBiAddress() {
		return this.biAddress;
	}

	public void setBiZip(String biZip) {
		this.biZip = Functions.subString(biZip, Len.BI_ZIP);
	}

	public String getBiZip() {
		return this.biZip;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BI_ADDRESS = 100;
		public static final int PE_ADDRESS = 100;
		public static final int BI_ZIP = 10;
		public static final int PE_ZIP = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
