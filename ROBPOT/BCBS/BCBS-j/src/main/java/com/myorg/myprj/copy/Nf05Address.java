/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: NF05-ADDRESS<br>
 * Variable: NF05-ADDRESS from copybook NF05EOB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Nf05Address {

	//==== PROPERTIES ====
	//Original name: NF05-ADDRESS-1
	private String address1 = DefaultValues.stringVal(Len.ADDRESS1);
	//Original name: NF05-ADDRESS-2
	private String address2 = DefaultValues.stringVal(Len.ADDRESS2);
	//Original name: NF05-ADDRESS-CITY
	private String addressCity = DefaultValues.stringVal(Len.ADDRESS_CITY);
	//Original name: NF05-FILLER
	private String filler = DefaultValues.stringVal(Len.FILLER);
	//Original name: NF05-ADDRESS-STATE
	private String addressState = DefaultValues.stringVal(Len.ADDRESS_STATE);
	//Original name: NF05-ADDRESS-ZIP
	private String addressZip = DefaultValues.stringVal(Len.ADDRESS_ZIP);

	//==== METHODS ====
	public byte[] getNf05AddressBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, address1, Len.ADDRESS1);
		position += Len.ADDRESS1;
		MarshalByte.writeString(buffer, position, address2, Len.ADDRESS2);
		position += Len.ADDRESS2;
		MarshalByte.writeString(buffer, position, addressCity, Len.ADDRESS_CITY);
		position += Len.ADDRESS_CITY;
		MarshalByte.writeString(buffer, position, filler, Len.FILLER);
		position += Len.FILLER;
		MarshalByte.writeString(buffer, position, addressState, Len.ADDRESS_STATE);
		position += Len.ADDRESS_STATE;
		MarshalByte.writeString(buffer, position, addressZip, Len.ADDRESS_ZIP);
		return buffer;
	}

	public void setAddress1(String address1) {
		this.address1 = Functions.subString(address1, Len.ADDRESS1);
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress2(String address2) {
		this.address2 = Functions.subString(address2, Len.ADDRESS2);
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddressCity(String addressCity) {
		this.addressCity = Functions.subString(addressCity, Len.ADDRESS_CITY);
	}

	public String getAddressCity() {
		return this.addressCity;
	}

	public void setFiller(String filler) {
		this.filler = Functions.subString(filler, Len.FILLER);
	}

	public String getFiller() {
		return this.filler;
	}

	public void setAddressState(String addressState) {
		this.addressState = Functions.subString(addressState, Len.ADDRESS_STATE);
	}

	public String getAddressState() {
		return this.addressState;
	}

	public void setAddressZip(String addressZip) {
		this.addressZip = Functions.subString(addressZip, Len.ADDRESS_ZIP);
	}

	public String getAddressZip() {
		return this.addressZip;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ADDRESS1 = 25;
		public static final int ADDRESS2 = 25;
		public static final int ADDRESS_CITY = 12;
		public static final int FILLER = 3;
		public static final int ADDRESS_STATE = 2;
		public static final int ADDRESS_ZIP = 9;
		public static final int NF05_ADDRESS = ADDRESS1 + ADDRESS2 + ADDRESS_CITY + FILLER + ADDRESS_STATE + ADDRESS_ZIP;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
