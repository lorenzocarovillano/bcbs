/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS02717sa;

/**
 * Data Access Object(DAO) for table [S02717SA]
 * 
 */
public class S02717saDao extends BaseSqlDao<IS02717sa> {

	public S02717saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02717sa> getToClass() {
		return IS02717sa.class;
	}

	public IS02717sa selectRec(String clmCntlId, char clmCntlSfxId, short provSbmtLnNoId, IS02717sa iS02717sa) {
		return buildQuery("selectRec").bind("clmCntlId", clmCntlId).bind("clmCntlSfxId", String.valueOf(clmCntlSfxId))
				.bind("provSbmtLnNoId", provSbmtLnNoId).singleResult(iS02717sa);
	}
}
