/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: NF05-CLAIM-NUM<br>
 * Variable: NF05-CLAIM-NUM from copybook NF05EOB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Nf05ClaimNum {

	//==== PROPERTIES ====
	//Original name: NF05-LOT-NUM
	private String lotNum = DefaultValues.stringVal(Len.LOT_NUM);
	//Original name: NF05-CONTROL-DATE-YY
	private String controlDateYy = DefaultValues.stringVal(Len.CONTROL_DATE_YY);
	//Original name: NF05-CONTROL-DATE-JJJ
	private String controlDateJjj = DefaultValues.stringVal(Len.CONTROL_DATE_JJJ);
	//Original name: FILLER-NF05-CLAIM-NUM
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setNf05ClaimNumFormatted(String data) {
		byte[] buffer = new byte[Len.NF05_CLAIM_NUM];
		MarshalByte.writeString(buffer, 1, data, Len.NF05_CLAIM_NUM);
		setNf05ClaimNumBytes(buffer, 1);
	}

	public String getNf05ClaimNumFormatted() {
		return MarshalByteExt.bufferToStr(getNf05ClaimNumBytes());
	}

	public byte[] getNf05ClaimNumBytes() {
		byte[] buffer = new byte[Len.NF05_CLAIM_NUM];
		return getNf05ClaimNumBytes(buffer, 1);
	}

	public void setNf05ClaimNumBytes(byte[] buffer, int offset) {
		int position = offset;
		lotNum = MarshalByte.readString(buffer, position, Len.LOT_NUM);
		position += Len.LOT_NUM;
		setControlDateBytes(buffer, position);
		position += Len.CONTROL_DATE;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getNf05ClaimNumBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, lotNum, Len.LOT_NUM);
		position += Len.LOT_NUM;
		getControlDateBytes(buffer, position);
		position += Len.CONTROL_DATE;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setLotNum(String lotNum) {
		this.lotNum = Functions.subString(lotNum, Len.LOT_NUM);
	}

	public String getLotNum() {
		return this.lotNum;
	}

	public void setControlDateBytes(byte[] buffer, int offset) {
		int position = offset;
		controlDateYy = MarshalByte.readFixedString(buffer, position, Len.CONTROL_DATE_YY);
		position += Len.CONTROL_DATE_YY;
		controlDateJjj = MarshalByte.readFixedString(buffer, position, Len.CONTROL_DATE_JJJ);
	}

	public byte[] getControlDateBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, controlDateYy, Len.CONTROL_DATE_YY);
		position += Len.CONTROL_DATE_YY;
		MarshalByte.writeString(buffer, position, controlDateJjj, Len.CONTROL_DATE_JJJ);
		return buffer;
	}

	public void setControlDateYyFormatted(String controlDateYy) {
		this.controlDateYy = Trunc.toUnsignedNumeric(controlDateYy, Len.CONTROL_DATE_YY);
	}

	public short getControlDateYy() {
		return NumericDisplay.asShort(this.controlDateYy);
	}

	public void setControlDateJjjFormatted(String controlDateJjj) {
		this.controlDateJjj = Trunc.toUnsignedNumeric(controlDateJjj, Len.CONTROL_DATE_JJJ);
	}

	public short getControlDateJjj() {
		return NumericDisplay.asShort(this.controlDateJjj);
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LOT_NUM = 2;
		public static final int CONTROL_DATE_YY = 2;
		public static final int CONTROL_DATE_JJJ = 3;
		public static final int FLR1 = 5;
		public static final int CONTROL_DATE = CONTROL_DATE_YY + CONTROL_DATE_JJJ;
		public static final int NF05_CLAIM_NUM = LOT_NUM + CONTROL_DATE + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
