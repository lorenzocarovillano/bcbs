/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.myorg.myprj.commons.data.to.IS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043;
import com.myorg.myprj.ws.Nf0735mlData;

/**Original name: S02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Nf0735ml<br>*/
public class S02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Nf0735ml
		implements IS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043 {

	//==== PROPERTIES ====
	private Nf0735mlData ws;

	//==== CONSTRUCTORS ====
	public S02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Nf0735ml(Nf0735mlData ws) {
		this.ws = ws;
	}

	//==== METHODS ====
	@Override
	public char getActInactCd() {
		return ws.getDcls04315sa().getActInactCd();
	}

	@Override
	public void setActInactCd(char actInactCd) {
		ws.getDcls04315sa().setActInactCd(actInactCd);
	}

	@Override
	public short getAddnlRmtLiId() {
		return ws.getDcls02809sa().getAddnlRmtLiId();
	}

	@Override
	public void setAddnlRmtLiId(short addnlRmtLiId) {
		ws.getDcls02809sa().setAddnlRmtLiId(addnlRmtLiId);
	}

	@Override
	public String getAdjdOvrd10Cd() {
		return ws.getDcls02809sa().getAdjdOvrd10Cd();
	}

	@Override
	public void setAdjdOvrd10Cd(String adjdOvrd10Cd) {
		ws.getDcls02809sa().setAdjdOvrd10Cd(adjdOvrd10Cd);
	}

	@Override
	public String getAdjdOvrd1Cd() {
		return ws.getDcls02809sa().getAdjdOvrd1Cd();
	}

	@Override
	public void setAdjdOvrd1Cd(String adjdOvrd1Cd) {
		ws.getDcls02809sa().setAdjdOvrd1Cd(adjdOvrd1Cd);
	}

	@Override
	public String getAdjdOvrd2Cd() {
		return ws.getDcls02809sa().getAdjdOvrd2Cd();
	}

	@Override
	public void setAdjdOvrd2Cd(String adjdOvrd2Cd) {
		ws.getDcls02809sa().setAdjdOvrd2Cd(adjdOvrd2Cd);
	}

	@Override
	public String getAdjdOvrd3Cd() {
		return ws.getDcls02809sa().getAdjdOvrd3Cd();
	}

	@Override
	public void setAdjdOvrd3Cd(String adjdOvrd3Cd) {
		ws.getDcls02809sa().setAdjdOvrd3Cd(adjdOvrd3Cd);
	}

	@Override
	public String getAdjdOvrd4Cd() {
		return ws.getDcls02809sa().getAdjdOvrd4Cd();
	}

	@Override
	public void setAdjdOvrd4Cd(String adjdOvrd4Cd) {
		ws.getDcls02809sa().setAdjdOvrd4Cd(adjdOvrd4Cd);
	}

	@Override
	public String getAdjdOvrd5Cd() {
		return ws.getDcls02809sa().getAdjdOvrd5Cd();
	}

	@Override
	public void setAdjdOvrd5Cd(String adjdOvrd5Cd) {
		ws.getDcls02809sa().setAdjdOvrd5Cd(adjdOvrd5Cd);
	}

	@Override
	public String getAdjdOvrd6Cd() {
		return ws.getDcls02809sa().getAdjdOvrd6Cd();
	}

	@Override
	public void setAdjdOvrd6Cd(String adjdOvrd6Cd) {
		ws.getDcls02809sa().setAdjdOvrd6Cd(adjdOvrd6Cd);
	}

	@Override
	public String getAdjdOvrd7Cd() {
		return ws.getDcls02809sa().getAdjdOvrd7Cd();
	}

	@Override
	public void setAdjdOvrd7Cd(String adjdOvrd7Cd) {
		ws.getDcls02809sa().setAdjdOvrd7Cd(adjdOvrd7Cd);
	}

	@Override
	public String getAdjdOvrd8Cd() {
		return ws.getDcls02809sa().getAdjdOvrd8Cd();
	}

	@Override
	public void setAdjdOvrd8Cd(String adjdOvrd8Cd) {
		ws.getDcls02809sa().setAdjdOvrd8Cd(adjdOvrd8Cd);
	}

	@Override
	public String getAdjdOvrd9Cd() {
		return ws.getDcls02809sa().getAdjdOvrd9Cd();
	}

	@Override
	public void setAdjdOvrd9Cd(String adjdOvrd9Cd) {
		ws.getDcls02809sa().setAdjdOvrd9Cd(adjdOvrd9Cd);
	}

	@Override
	public String getAdjdProcCd() {
		return ws.getDcls04315sa().getAdjdProcCd();
	}

	@Override
	public void setAdjdProcCd(String adjdProcCd) {
		ws.getDcls04315sa().setAdjdProcCd(adjdProcCd);
	}

	@Override
	public String getAdjdProcMod1Cd() {
		return ws.getDcls04315sa().getAdjdProcMod1Cd();
	}

	@Override
	public void setAdjdProcMod1Cd(String adjdProcMod1Cd) {
		ws.getDcls04315sa().setAdjdProcMod1Cd(adjdProcMod1Cd);
	}

	@Override
	public String getAdjdProcMod2Cd() {
		return ws.getDcls04315sa().getAdjdProcMod2Cd();
	}

	@Override
	public void setAdjdProcMod2Cd(String adjdProcMod2Cd) {
		ws.getDcls04315sa().setAdjdProcMod2Cd(adjdProcMod2Cd);
	}

	@Override
	public String getAdjdProcMod3Cd() {
		return ws.getDcls04315sa().getAdjdProcMod3Cd();
	}

	@Override
	public void setAdjdProcMod3Cd(String adjdProcMod3Cd) {
		ws.getDcls04315sa().setAdjdProcMod3Cd(adjdProcMod3Cd);
	}

	@Override
	public String getAdjdProcMod4Cd() {
		return ws.getDcls04315sa().getAdjdProcMod4Cd();
	}

	@Override
	public void setAdjdProcMod4Cd(String adjdProcMod4Cd) {
		ws.getDcls04315sa().setAdjdProcMod4Cd(adjdProcMod4Cd);
	}

	@Override
	public String getBenTypCd() {
		return ws.getDcls02809sa().getBenTypCd();
	}

	@Override
	public void setBenTypCd(String benTypCd) {
		ws.getDcls02809sa().setBenTypCd(benTypCd);
	}

	@Override
	public AfDecimal getChgAm() {
		return ws.getDcls02809sa().getChgAm();
	}

	@Override
	public void setChgAm(AfDecimal chgAm) {
		ws.getDcls02809sa().setChgAm(chgAm.copy());
	}

	@Override
	public short getCliId() {
		return ws.getDcls02809sa().getCliId();
	}

	@Override
	public void setCliId(short cliId) {
		ws.getDcls02809sa().setCliId(cliId);
	}

	@Override
	public String getClmAdjGrpCd() {
		return ws.getDcls02800sa().getClmAdjGrpCd();
	}

	@Override
	public void setClmAdjGrpCd(String clmAdjGrpCd) {
		ws.getDcls02800sa().setClmAdjGrpCd(clmAdjGrpCd);
	}

	@Override
	public String getClmAdjRsnCd() {
		return ws.getDcls02800sa().getClmAdjRsnCd();
	}

	@Override
	public void setClmAdjRsnCd(String clmAdjRsnCd) {
		ws.getDcls02800sa().setClmAdjRsnCd(clmAdjRsnCd);
	}

	@Override
	public String getClmCntlId() {
		return ws.getDcls02809sa().getClmCntlId();
	}

	@Override
	public void setClmCntlId(String clmCntlId) {
		ws.getDcls02809sa().setClmCntlId(clmCntlId);
	}

	@Override
	public char getClmCntlSfxId() {
		return ws.getDcls02809sa().getClmCntlSfxId();
	}

	@Override
	public void setClmCntlSfxId(char clmCntlSfxId) {
		ws.getDcls02809sa().setClmCntlSfxId(clmCntlSfxId);
	}

	@Override
	public char getClmFreqTypCd() {
		return ws.getDcls02652sa().getClmFreqTypCd();
	}

	@Override
	public void setClmFreqTypCd(char clmFreqTypCd) {
		ws.getDcls02652sa().setClmFreqTypCd(clmFreqTypCd);
	}

	@Override
	public short getClmPmtId() {
		return ws.getDcls02809sa().getClmPmtId();
	}

	@Override
	public void setClmPmtId(short clmPmtId) {
		ws.getDcls02809sa().setClmPmtId(clmPmtId);
	}

	@Override
	public String getClmSbmtId() {
		return ws.getDcls02652sa().getClmSbmtId();
	}

	@Override
	public void setClmSbmtId(String clmSbmtId) {
		ws.getDcls02652sa().setClmSbmtId(clmSbmtId);
	}

	@Override
	public String getCovPmtLvlSttCd() {
		return ws.getDcls02809sa().getCovPmtLvlSttCd();
	}

	@Override
	public void setCovPmtLvlSttCd(String covPmtLvlSttCd) {
		ws.getDcls02809sa().setCovPmtLvlSttCd(covPmtLvlSttCd);
	}

	@Override
	public short getCrsRefCliId() {
		return ws.getDcls04315sa().getCrsRefCliId();
	}

	@Override
	public void setCrsRefCliId(short crsRefCliId) {
		ws.getDcls04315sa().setCrsRefCliId(crsRefCliId);
	}

	@Override
	public short getCrsRefClmPmtId() {
		return ws.getDcls04315sa().getCrsRefClmPmtId();
	}

	@Override
	public void setCrsRefClmPmtId(short crsRefClmPmtId) {
		ws.getDcls04315sa().setCrsRefClmPmtId(crsRefClmPmtId);
	}

	@Override
	public char getCrsRefRsnCd() {
		return ws.getDcls04315sa().getCrsRefRsnCd();
	}

	@Override
	public void setCrsRefRsnCd(char crsRefRsnCd) {
		ws.getDcls04315sa().setCrsRefRsnCd(crsRefRsnCd);
	}

	@Override
	public String getCrsRfClmCntlId() {
		return ws.getDcls04315sa().getCrsRfClmCntlId();
	}

	@Override
	public void setCrsRfClmCntlId(String crsRfClmCntlId) {
		ws.getDcls04315sa().setCrsRfClmCntlId(crsRfClmCntlId);
	}

	@Override
	public char getCsRfClmClSxId() {
		return ws.getDcls04315sa().getCsRfClmClSxId();
	}

	@Override
	public void setCsRfClmClSxId(char csRfClmClSxId) {
		ws.getDcls04315sa().setCsRfClmClSxId(csRfClmClSxId);
	}

	@Override
	public String getDnlRmrkCd() {
		return ws.getDcls02809sa().getDnlRmrkCd();
	}

	@Override
	public void setDnlRmrkCd(String dnlRmrkCd) {
		ws.getDcls02809sa().setDnlRmrkCd(dnlRmrkCd);
	}

	@Override
	public char getDnlRmrkUsgCd() {
		return ws.getDcls02993sa().getDnlRmrkUsgCd();
	}

	@Override
	public void setDnlRmrkUsgCd(char dnlRmrkUsgCd) {
		ws.getDcls02993sa().setDnlRmrkUsgCd(dnlRmrkUsgCd);
	}

	@Override
	public char getExmnActnCd() {
		return ws.getDcls02809sa().getExmnActnCd();
	}

	@Override
	public void setExmnActnCd(char exmnActnCd) {
		ws.getDcls02809sa().setExmnActnCd(exmnActnCd);
	}

	@Override
	public String getFacQlfCd() {
		return ws.getDcls02652sa().getFacQlfCd();
	}

	@Override
	public void setFacQlfCd(String facQlfCd) {
		ws.getDcls02652sa().setFacQlfCd(facQlfCd);
	}

	@Override
	public String getFacVlCd() {
		return ws.getDcls02652sa().getFacVlCd();
	}

	@Override
	public void setFacVlCd(String facVlCd) {
		ws.getDcls02652sa().setFacVlCd(facVlCd);
	}

	@Override
	public String getFepEdtOvrd1Cd() {
		return ws.getDcls02809sa().getFepEdtOvrd1Cd();
	}

	@Override
	public void setFepEdtOvrd1Cd(String fepEdtOvrd1Cd) {
		ws.getDcls02809sa().setFepEdtOvrd1Cd(fepEdtOvrd1Cd);
	}

	@Override
	public String getFepEdtOvrd2Cd() {
		return ws.getDcls02809sa().getFepEdtOvrd2Cd();
	}

	@Override
	public void setFepEdtOvrd2Cd(String fepEdtOvrd2Cd) {
		ws.getDcls02809sa().setFepEdtOvrd2Cd(fepEdtOvrd2Cd);
	}

	@Override
	public String getFepEdtOvrd3Cd() {
		return ws.getDcls02809sa().getFepEdtOvrd3Cd();
	}

	@Override
	public void setFepEdtOvrd3Cd(String fepEdtOvrd3Cd) {
		ws.getDcls02809sa().setFepEdtOvrd3Cd(fepEdtOvrd3Cd);
	}

	@Override
	public String getFrstNm() {
		return ws.getDcls02682sa().getFrstNm();
	}

	@Override
	public void setFrstNm(String frstNm) {
		ws.getDcls02682sa().setFrstNm(frstNm);
	}

	@Override
	public String getIdCd() {
		return ws.getDcls02682sa().getIdCd();
	}

	@Override
	public void setIdCd(String idCd) {
		ws.getDcls02682sa().setIdCd(idCd);
	}

	@Override
	public String getInitClmCntlId() {
		return ws.getDcls04315sa().getInitClmCntlId();
	}

	@Override
	public void setInitClmCntlId(String initClmCntlId) {
		ws.getDcls04315sa().setInitClmCntlId(initClmCntlId);
	}

	@Override
	public short getInitClmPmtId() {
		return ws.getDcls04315sa().getInitClmPmtId();
	}

	@Override
	public void setInitClmPmtId(short initClmPmtId) {
		ws.getDcls04315sa().setInitClmPmtId(initClmPmtId);
	}

	@Override
	public String getInitClmTs() {
		return ws.getDcls04315sa().getInitClmTs();
	}

	@Override
	public void setInitClmTs(String initClmTs) {
		ws.getDcls04315sa().setInitClmTs(initClmTs);
	}

	@Override
	public char getIntClmCnlSfxId() {
		return ws.getDcls04315sa().getIntClmCnlSfxId();
	}

	@Override
	public void setIntClmCnlSfxId(char intClmCnlSfxId) {
		ws.getDcls04315sa().setIntClmCnlSfxId(intClmCnlSfxId);
	}

	@Override
	public AfDecimal getLiAlctOiCovAm() {
		return ws.getDcls02809sa().getLiAlctOiCovAm();
	}

	@Override
	public void setLiAlctOiCovAm(AfDecimal liAlctOiCovAm) {
		ws.getDcls02809sa().setLiAlctOiCovAm(liAlctOiCovAm.copy());
	}

	@Override
	public AfDecimal getLiAlctOiPdAm() {
		return ws.getDcls02809sa().getLiAlctOiPdAm();
	}

	@Override
	public void setLiAlctOiPdAm(AfDecimal liAlctOiPdAm) {
		ws.getDcls02809sa().setLiAlctOiPdAm(liAlctOiPdAm.copy());
	}

	@Override
	public AfDecimal getLiAlctOiSaveAm() {
		return ws.getDcls02809sa().getLiAlctOiSaveAm();
	}

	@Override
	public void setLiAlctOiSaveAm(AfDecimal liAlctOiSaveAm) {
		ws.getDcls02809sa().setLiAlctOiSaveAm(liAlctOiSaveAm.copy());
	}

	@Override
	public AfDecimal getLiAlwChgAm() {
		return ws.getDcls02809sa().getLiAlwChgAm();
	}

	@Override
	public void setLiAlwChgAm(AfDecimal liAlwChgAm) {
		ws.getDcls02809sa().setLiAlwChgAm(liAlwChgAm.copy());
	}

	@Override
	public char getLiExplnCd() {
		return ws.getDcls02809sa().getLiExplnCd();
	}

	@Override
	public void setLiExplnCd(char liExplnCd) {
		ws.getDcls02809sa().setLiExplnCd(liExplnCd);
	}

	@Override
	public String getLiFrmServDt() {
		return ws.getDcls02809sa().getLiFrmServDt();
	}

	@Override
	public void setLiFrmServDt(String liFrmServDt) {
		ws.getDcls02809sa().setLiFrmServDt(liFrmServDt);
	}

	@Override
	public String getLiThrServDt() {
		return ws.getDcls02809sa().getLiThrServDt();
	}

	@Override
	public void setLiThrServDt(String liThrServDt) {
		ws.getDcls02809sa().setLiThrServDt(liThrServDt);
	}

	@Override
	public String getLstOrgNm() {
		return ws.getDcls02682sa().getLstOrgNm();
	}

	@Override
	public void setLstOrgNm(String lstOrgNm) {
		ws.getDcls02682sa().setLstOrgNm(lstOrgNm);
	}

	@Override
	public String getMidNm() {
		return ws.getDcls02682sa().getMidNm();
	}

	@Override
	public void setMidNm(String midNm) {
		ws.getDcls02682sa().setMidNm(midNm);
	}

	@Override
	public AfDecimal getMntryAm() {
		return ws.getDcls02652sa().getMntryAm();
	}

	@Override
	public void setMntryAm(AfDecimal mntryAm) {
		ws.getDcls02652sa().setMntryAm(mntryAm.copy());
	}

	@Override
	public AfDecimal getOrigSbmtChgAm() {
		return ws.getDcls02809sa().getOrigSbmtChgAm();
	}

	@Override
	public void setOrigSbmtChgAm(AfDecimal origSbmtChgAm) {
		ws.getDcls02809sa().setOrigSbmtChgAm(origSbmtChgAm.copy());
	}

	@Override
	public AfDecimal getOrigSbmtUnitCt() {
		return ws.getDcls02809sa().getOrigSbmtUnitCt();
	}

	@Override
	public void setOrigSbmtUnitCt(AfDecimal origSbmtUnitCt) {
		ws.getDcls02809sa().setOrigSbmtUnitCt(origSbmtUnitCt.copy());
	}

	@Override
	public String getPosCd() {
		return ws.getDcls02809sa().getPosCd();
	}

	@Override
	public void setPosCd(String posCd) {
		ws.getDcls02809sa().setPosCd(posCd);
	}

	@Override
	public char getPrmptPayDnlIn() {
		return ws.getDcls02993sa().getPrmptPayDnlIn();
	}

	@Override
	public void setPrmptPayDnlIn(char prmptPayDnlIn) {
		ws.getDcls02993sa().setPrmptPayDnlIn(prmptPayDnlIn);
	}

	@Override
	public String getProcCd() {
		return ws.getDcls02809sa().getProcCd();
	}

	@Override
	public void setProcCd(String procCd) {
		ws.getDcls02809sa().setProcCd(procCd);
	}

	@Override
	public String getProcMod1Cd() {
		return ws.getDcls02809sa().getProcMod1Cd();
	}

	@Override
	public void setProcMod1Cd(String procMod1Cd) {
		ws.getDcls02809sa().setProcMod1Cd(procMod1Cd);
	}

	@Override
	public String getProcMod2Cd() {
		return ws.getDcls02809sa().getProcMod2Cd();
	}

	@Override
	public void setProcMod2Cd(String procMod2Cd) {
		ws.getDcls02809sa().setProcMod2Cd(procMod2Cd);
	}

	@Override
	public String getProcMod3Cd() {
		return ws.getDcls02809sa().getProcMod3Cd();
	}

	@Override
	public void setProcMod3Cd(String procMod3Cd) {
		ws.getDcls02809sa().setProcMod3Cd(procMod3Cd);
	}

	@Override
	public String getProcMod4Cd() {
		return ws.getDcls02809sa().getProcMod4Cd();
	}

	@Override
	public void setProcMod4Cd(String procMod4Cd) {
		ws.getDcls02809sa().setProcMod4Cd(procMod4Cd);
	}

	@Override
	public short getProvSbmtLnNoId() {
		return ws.getDcls02809sa().getProvSbmtLnNoId();
	}

	@Override
	public void setProvSbmtLnNoId(short provSbmtLnNoId) {
		ws.getDcls02809sa().setProvSbmtLnNoId(provSbmtLnNoId);
	}

	@Override
	public AfDecimal getQntyCt() {
		return ws.getDcls02800sa().getQntyCt();
	}

	@Override
	public void setQntyCt(AfDecimal qntyCt) {
		ws.getDcls02800sa().setQntyCt(qntyCt.copy());
	}

	@Override
	public String getRefId() {
		return ws.getDcls02713sa().getRefId();
	}

	@Override
	public void setRefId(String refId) {
		ws.getDcls02713sa().setRefId(refId);
	}

	@Override
	public String getRevCd() {
		return ws.getDcls02809sa().getRevCd();
	}

	@Override
	public void setRevCd(String revCd) {
		ws.getDcls02809sa().setRevCd(revCd);
	}

	@Override
	public String getRvwByCd() {
		return ws.getDcls02809sa().getRvwByCd();
	}

	@Override
	public void setRvwByCd(String rvwByCd) {
		ws.getDcls02809sa().setRvwByCd(rvwByCd);
	}

	@Override
	public String getSfxNm() {
		return ws.getDcls02682sa().getSfxNm();
	}

	@Override
	public void setSfxNm(String sfxNm) {
		ws.getDcls02682sa().setSfxNm(sfxNm);
	}

	@Override
	public String getTrig837BillProvNpiId() {
		return ws.getTrigRecordLayout().getTrig837BillProvNpiId();
	}

	@Override
	public void setTrig837BillProvNpiId(String trig837BillProvNpiId) {
		ws.getTrigRecordLayout().setTrig837BillProvNpiId(trig837BillProvNpiId);
	}

	@Override
	public String getTrig837PerfProvNpiId() {
		return ws.getTrigRecordLayout().getTrig837PerfProvNpiId();
	}

	@Override
	public void setTrig837PerfProvNpiId(String trig837PerfProvNpiId) {
		ws.getTrigRecordLayout().setTrig837PerfProvNpiId(trig837PerfProvNpiId);
	}

	@Override
	public char getTrigAdjRespCd() {
		return ws.getTrigRecordLayout().getTrigAdjRespCd();
	}

	@Override
	public void setTrigAdjRespCd(char trigAdjRespCd) {
		ws.getTrigRecordLayout().setTrigAdjRespCd(trigAdjRespCd);
	}

	@Override
	public String getTrigAdjTrckCd() {
		return ws.getTrigRecordLayout().getTrigAdjTrckCd();
	}

	@Override
	public void setTrigAdjTrckCd(String trigAdjTrckCd) {
		ws.getTrigRecordLayout().setTrigAdjTrckCd(trigAdjTrckCd);
	}

	@Override
	public char getTrigAdjTypCd() {
		return ws.getTrigRecordLayout().getTrigAdjTypCd();
	}

	@Override
	public void setTrigAdjTypCd(char trigAdjTypCd) {
		ws.getTrigRecordLayout().setTrigAdjTypCd(trigAdjTypCd);
	}

	@Override
	public char getTrigAdjdProvStatCd() {
		return ws.getTrigRecordLayout().getTrigAdjdProvStatCd();
	}

	@Override
	public void setTrigAdjdProvStatCd(char trigAdjdProvStatCd) {
		ws.getTrigRecordLayout().setTrigAdjdProvStatCd(trigAdjdProvStatCd);
	}

	@Override
	public String getTrigAlphPrfxCd() {
		return ws.getTrigRecordLayout().getTrigAlphPrfxCd();
	}

	@Override
	public void setTrigAlphPrfxCd(String trigAlphPrfxCd) {
		ws.getTrigRecordLayout().setTrigAlphPrfxCd(trigAlphPrfxCd);
	}

	@Override
	public String getTrigAsgCd() {
		return ws.getTrigRecordLayout().getTrigAsgCd();
	}

	@Override
	public void setTrigAsgCd(String trigAsgCd) {
		ws.getTrigRecordLayout().setTrigAsgCd(trigAsgCd);
	}

	@Override
	public String getTrigBaseCnArngCd() {
		return ws.getTrigRecordLayout().getTrigBaseCnArngCd();
	}

	@Override
	public void setTrigBaseCnArngCd(String trigBaseCnArngCd) {
		ws.getTrigRecordLayout().setTrigBaseCnArngCd(trigBaseCnArngCd);
	}

	@Override
	public String getTrigBillFrmDt() {
		return ws.getTrigRecordLayout().getTrigBillFrmDt();
	}

	@Override
	public void setTrigBillFrmDt(String trigBillFrmDt) {
		ws.getTrigRecordLayout().setTrigBillFrmDt(trigBillFrmDt);
	}

	@Override
	public String getTrigBillProvIdQlfCd() {
		return ws.getTrigRecordLayout().getTrigBillProvIdQlfCd();
	}

	@Override
	public void setTrigBillProvIdQlfCd(String trigBillProvIdQlfCd) {
		ws.getTrigRecordLayout().setTrigBillProvIdQlfCd(trigBillProvIdQlfCd);
	}

	@Override
	public String getTrigBillProvSpecCd() {
		return ws.getTrigRecordLayout().getTrigBillProvSpecCd();
	}

	@Override
	public void setTrigBillProvSpecCd(String trigBillProvSpecCd) {
		ws.getTrigRecordLayout().setTrigBillProvSpecCd(trigBillProvSpecCd);
	}

	@Override
	public String getTrigBillThrDt() {
		return ws.getTrigRecordLayout().getTrigBillThrDt();
	}

	@Override
	public void setTrigBillThrDt(String trigBillThrDt) {
		ws.getTrigRecordLayout().setTrigBillThrDt(trigBillThrDt);
	}

	@Override
	public char getTrigClaimLobCd() {
		return ws.getTrigRecordLayout().getTrigClaimLobCd();
	}

	@Override
	public void setTrigClaimLobCd(char trigClaimLobCd) {
		ws.getTrigRecordLayout().setTrigClaimLobCd(trigClaimLobCd);
	}

	@Override
	public char getTrigClmckAdjStatCd() {
		return ws.getTrigRecordLayout().getTrigClmckAdjStatCd();
	}

	@Override
	public void setTrigClmckAdjStatCd(char trigClmckAdjStatCd) {
		ws.getTrigRecordLayout().setTrigClmckAdjStatCd(trigClmckAdjStatCd);
	}

	@Override
	public String getTrigCorpRcvDt() {
		return ws.getTrigRecordLayout().getTrigCorpRcvDt();
	}

	@Override
	public void setTrigCorpRcvDt(String trigCorpRcvDt) {
		ws.getTrigRecordLayout().setTrigCorpRcvDt(trigCorpRcvDt);
	}

	@Override
	public String getTrigCorrPrioritySubId() {
		return ws.getTrigRecordLayout().getTrigCorrPrioritySubId();
	}

	@Override
	public void setTrigCorrPrioritySubId(String trigCorrPrioritySubId) {
		ws.getTrigRecordLayout().setTrigCorrPrioritySubId(trigCorrPrioritySubId);
	}

	@Override
	public String getTrigDateCoverageLapsed() {
		return ws.getTrigRecordLayout().getTrigDateCoverageLapsed();
	}

	@Override
	public void setTrigDateCoverageLapsed(String trigDateCoverageLapsed) {
		ws.getTrigRecordLayout().setTrigDateCoverageLapsed(trigDateCoverageLapsed);
	}

	@Override
	public String getTrigDrgCd() {
		return ws.getTrigRecordLayout().getTrigDrgCd();
	}

	@Override
	public void setTrigDrgCd(String trigDrgCd) {
		ws.getTrigRecordLayout().setTrigDrgCd(trigDrgCd);
	}

	@Override
	public char getTrigEftAccountType() {
		return ws.getTrigRecordLayout().getTrigEftAccountType();
	}

	@Override
	public void setTrigEftAccountType(char trigEftAccountType) {
		ws.getTrigRecordLayout().setTrigEftAccountType(trigEftAccountType);
	}

	@Override
	public String getTrigEftAcct() {
		return ws.getTrigRecordLayout().getTrigEftAcct();
	}

	@Override
	public void setTrigEftAcct(String trigEftAcct) {
		ws.getTrigRecordLayout().setTrigEftAcct(trigEftAcct);
	}

	@Override
	public char getTrigEftInd() {
		return ws.getTrigRecordLayout().getTrigEftInd();
	}

	@Override
	public void setTrigEftInd(char trigEftInd) {
		ws.getTrigRecordLayout().setTrigEftInd(trigEftInd);
	}

	@Override
	public String getTrigEftTrans() {
		return ws.getTrigRecordLayout().getTrigEftTrans();
	}

	@Override
	public void setTrigEftTrans(String trigEftTrans) {
		ws.getTrigRecordLayout().setTrigEftTrans(trigEftTrans);
	}

	@Override
	public String getTrigEnrClCd() {
		return ws.getTrigRecordLayout().getTrigEnrClCd();
	}

	@Override
	public void setTrigEnrClCd(String trigEnrClCd) {
		ws.getTrigRecordLayout().setTrigEnrClCd(trigEnrClCd);
	}

	@Override
	public String getTrigFinCd() {
		return ws.getTrigRecordLayout().getTrigFinCd();
	}

	@Override
	public void setTrigFinCd(String trigFinCd) {
		ws.getTrigRecordLayout().setTrigFinCd(trigFinCd);
	}

	@Override
	public String getTrigGlOfstOrigCd() {
		return ws.getTrigRecordLayout().getTrigGlOfstOrigCd();
	}

	@Override
	public void setTrigGlOfstOrigCd(String trigGlOfstOrigCd) {
		ws.getTrigRecordLayout().setTrigGlOfstOrigCd(trigGlOfstOrigCd);
	}

	@Override
	public String getTrigGmisIndicator() {
		return ws.getTrigRecordLayout().getTrigGmisIndicator();
	}

	@Override
	public void setTrigGmisIndicator(String trigGmisIndicator) {
		ws.getTrigRecordLayout().setTrigGmisIndicator(trigGmisIndicator);
	}

	@Override
	public String getTrigGrpId() {
		return ws.getTrigRecordLayout().getTrigGrpId();
	}

	@Override
	public void setTrigGrpId(String trigGrpId) {
		ws.getTrigRecordLayout().setTrigGrpId(trigGrpId);
	}

	@Override
	public String getTrigHipaaVersionFormatId() {
		return ws.getTrigRecordLayout().getTrigHipaaVersionFormatId();
	}

	@Override
	public void setTrigHipaaVersionFormatId(String trigHipaaVersionFormatId) {
		ws.getTrigRecordLayout().setTrigHipaaVersionFormatId(trigHipaaVersionFormatId);
	}

	@Override
	public String getTrigHistBillProvNpiId() {
		return ws.getTrigRecordLayout().getTrigHistBillProvNpiId();
	}

	@Override
	public void setTrigHistBillProvNpiId(String trigHistBillProvNpiId) {
		ws.getTrigRecordLayout().setTrigHistBillProvNpiId(trigHistBillProvNpiId);
	}

	@Override
	public char getTrigHistLoadCd() {
		return ws.getTrigRecordLayout().getTrigHistLoadCd();
	}

	@Override
	public void setTrigHistLoadCd(char trigHistLoadCd) {
		ws.getTrigRecordLayout().setTrigHistLoadCd(trigHistLoadCd);
	}

	@Override
	public String getTrigHistPerfProvNpiId() {
		return ws.getTrigRecordLayout().getTrigHistPerfProvNpiId();
	}

	@Override
	public void setTrigHistPerfProvNpiId(String trigHistPerfProvNpiId) {
		ws.getTrigRecordLayout().setTrigHistPerfProvNpiId(trigHistPerfProvNpiId);
	}

	@Override
	public String getTrigInsId() {
		return ws.getTrigRecordLayout().getTrigInsId();
	}

	@Override
	public void setTrigInsId(String trigInsId) {
		ws.getTrigRecordLayout().setTrigInsId(trigInsId);
	}

	@Override
	public char getTrigItsCk() {
		return ws.getTrigRecordLayout().getTrigItsCk();
	}

	@Override
	public void setTrigItsCk(char trigItsCk) {
		ws.getTrigRecordLayout().setTrigItsCk(trigItsCk);
	}

	@Override
	public char getTrigItsClmTypCd() {
		return ws.getTrigRecordLayout().getTrigItsClmTypCd();
	}

	@Override
	public void setTrigItsClmTypCd(char trigItsClmTypCd) {
		ws.getTrigRecordLayout().setTrigItsClmTypCd(trigItsClmTypCd);
	}

	@Override
	public String getTrigItsInsId() {
		return ws.getTrigRecordLayout().getTrigItsInsId();
	}

	@Override
	public void setTrigItsInsId(String trigItsInsId) {
		ws.getTrigRecordLayout().setTrigItsInsId(trigItsInsId);
	}

	@Override
	public String getTrigKcapsTeamNm() {
		return ws.getTrigRecordLayout().getTrigKcapsTeamNm();
	}

	@Override
	public void setTrigKcapsTeamNm(String trigKcapsTeamNm) {
		ws.getTrigRecordLayout().setTrigKcapsTeamNm(trigKcapsTeamNm);
	}

	@Override
	public String getTrigKcapsUseId() {
		return ws.getTrigRecordLayout().getTrigKcapsUseId();
	}

	@Override
	public void setTrigKcapsUseId(String trigKcapsUseId) {
		ws.getTrigRecordLayout().setTrigKcapsUseId(trigKcapsUseId);
	}

	@Override
	public String getTrigLocalBillProvId() {
		return ws.getTrigRecordLayout().getTrigLocalBillProvId();
	}

	@Override
	public void setTrigLocalBillProvId(String trigLocalBillProvId) {
		ws.getTrigRecordLayout().setTrigLocalBillProvId(trigLocalBillProvId);
	}

	@Override
	public char getTrigLocalBillProvLobCd() {
		return ws.getTrigRecordLayout().getTrigLocalBillProvLobCd();
	}

	@Override
	public void setTrigLocalBillProvLobCd(char trigLocalBillProvLobCd) {
		ws.getTrigRecordLayout().setTrigLocalBillProvLobCd(trigLocalBillProvLobCd);
	}

	@Override
	public String getTrigLocalPerfProvId() {
		return ws.getTrigRecordLayout().getTrigLocalPerfProvId();
	}

	@Override
	public void setTrigLocalPerfProvId(String trigLocalPerfProvId) {
		ws.getTrigRecordLayout().setTrigLocalPerfProvId(trigLocalPerfProvId);
	}

	@Override
	public char getTrigLocalPerfProvLobCd() {
		return ws.getTrigRecordLayout().getTrigLocalPerfProvLobCd();
	}

	@Override
	public void setTrigLocalPerfProvLobCd(char trigLocalPerfProvLobCd) {
		ws.getTrigRecordLayout().setTrigLocalPerfProvLobCd(trigLocalPerfProvLobCd);
	}

	@Override
	public String getTrigMemberId() {
		return ws.getTrigRecordLayout().getTrigMemberId();
	}

	@Override
	public void setTrigMemberId(String trigMemberId) {
		ws.getTrigRecordLayout().setTrigMemberId(trigMemberId);
	}

	@Override
	public String getTrigMrktPkgCd() {
		return ws.getTrigRecordLayout().getTrigMrktPkgCd();
	}

	@Override
	public void setTrigMrktPkgCd(String trigMrktPkgCd) {
		ws.getTrigRecordLayout().setTrigMrktPkgCd(trigMrktPkgCd);
	}

	@Override
	public char getTrigNpiCd() {
		return ws.getProgramHoldAreas().getTrigNpiCd();
	}

	@Override
	public void setTrigNpiCd(char trigNpiCd) {
		ws.getProgramHoldAreas().setTrigNpiCd(trigNpiCd);
	}

	@Override
	public String getTrigNtwrkCd() {
		return ws.getTrigRecordLayout().getTrigNtwrkCd();
	}

	@Override
	public void setTrigNtwrkCd(String trigNtwrkCd) {
		ws.getTrigRecordLayout().setTrigNtwrkCd(trigNtwrkCd);
	}

	@Override
	public String getTrigOiPayNm() {
		return ws.getTrigRecordLayout().getTrigOiPayNm();
	}

	@Override
	public void setTrigOiPayNm(String trigOiPayNm) {
		ws.getTrigRecordLayout().setTrigOiPayNm(trigOiPayNm);
	}

	@Override
	public String getTrigPatActMedRecId() {
		return ws.getTrigRecordLayout().getTrigPatActMedRecId();
	}

	@Override
	public void setTrigPatActMedRecId(String trigPatActMedRecId) {
		ws.getTrigRecordLayout().setTrigPatActMedRecId(trigPatActMedRecId);
	}

	@Override
	public String getTrigPgmAreaCd() {
		return ws.getTrigRecordLayout().getTrigPgmAreaCd();
	}

	@Override
	public void setTrigPgmAreaCd(String trigPgmAreaCd) {
		ws.getTrigRecordLayout().setTrigPgmAreaCd(trigPgmAreaCd);
	}

	@Override
	public String getTrigPmtAdjdDt() {
		return ws.getTrigRecordLayout().getTrigPmtAdjdDt();
	}

	@Override
	public void setTrigPmtAdjdDt(String trigPmtAdjdDt) {
		ws.getTrigRecordLayout().setTrigPmtAdjdDt(trigPmtAdjdDt);
	}

	@Override
	public char getTrigPmtBkProdCd() {
		return ws.getTrigRecordLayout().getTrigPmtBkProdCd();
	}

	@Override
	public void setTrigPmtBkProdCd(char trigPmtBkProdCd) {
		ws.getTrigRecordLayout().setTrigPmtBkProdCd(trigPmtBkProdCd);
	}

	@Override
	public String getTrigPmtFrmServDt() {
		return ws.getTrigRecordLayout().getTrigPmtFrmServDt();
	}

	@Override
	public void setTrigPmtFrmServDt(String trigPmtFrmServDt) {
		ws.getTrigRecordLayout().setTrigPmtFrmServDt(trigPmtFrmServDt);
	}

	@Override
	public char getTrigPmtOiIn() {
		return ws.getTrigRecordLayout().getTrigPmtOiIn();
	}

	@Override
	public void setTrigPmtOiIn(char trigPmtOiIn) {
		ws.getTrigRecordLayout().setTrigPmtOiIn(trigPmtOiIn);
	}

	@Override
	public String getTrigPmtThrServDt() {
		return ws.getTrigRecordLayout().getTrigPmtThrServDt();
	}

	@Override
	public void setTrigPmtThrServDt(String trigPmtThrServDt) {
		ws.getTrigRecordLayout().setTrigPmtThrServDt(trigPmtThrServDt);
	}

	@Override
	public String getTrigPrimCnArngCd() {
		return ws.getTrigRecordLayout().getTrigPrimCnArngCd();
	}

	@Override
	public void setTrigPrimCnArngCd(String trigPrimCnArngCd) {
		ws.getTrigRecordLayout().setTrigPrimCnArngCd(trigPrimCnArngCd);
	}

	@Override
	public String getTrigPrmptPayDayCd() {
		return ws.getTrigRecordLayout().getTrigPrmptPayDayCd();
	}

	@Override
	public void setTrigPrmptPayDayCd(String trigPrmptPayDayCd) {
		ws.getTrigRecordLayout().setTrigPrmptPayDayCd(trigPrmptPayDayCd);
	}

	@Override
	public char getTrigPrmptPayOvrdCd() {
		return ws.getTrigRecordLayout().getTrigPrmptPayOvrdCd();
	}

	@Override
	public void setTrigPrmptPayOvrdCd(char trigPrmptPayOvrdCd) {
		ws.getTrigRecordLayout().setTrigPrmptPayOvrdCd(trigPrmptPayOvrdCd);
	}

	@Override
	public String getTrigProdInd() {
		return ws.getTrigRecordLayout().getTrigProdInd();
	}

	@Override
	public void setTrigProdInd(String trigProdInd) {
		ws.getTrigRecordLayout().setTrigProdInd(trigProdInd);
	}

	@Override
	public String getTrigProvUnwrpDt() {
		return ws.getTrigRecordLayout().getTrigProvUnwrpDt();
	}

	@Override
	public void setTrigProvUnwrpDt(String trigProvUnwrpDt) {
		ws.getTrigRecordLayout().setTrigProvUnwrpDt(trigProvUnwrpDt);
	}

	@Override
	public String getTrigRateCd() {
		return ws.getTrigRecordLayout().getTrigRateCd();
	}

	@Override
	public void setTrigRateCd(String trigRateCd) {
		ws.getTrigRecordLayout().setTrigRateCd(trigRateCd);
	}

	@Override
	public char getTrigStatAdjPrevPmt() {
		return ws.getTrigRecordLayout().getTrigStatAdjPrevPmt();
	}

	@Override
	public void setTrigStatAdjPrevPmt(char trigStatAdjPrevPmt) {
		ws.getTrigRecordLayout().setTrigStatAdjPrevPmt(trigStatAdjPrevPmt);
	}

	@Override
	public String getTrigTypGrpCd() {
		return ws.getTrigRecordLayout().getTrigTypGrpCd();
	}

	@Override
	public void setTrigTypGrpCd(String trigTypGrpCd) {
		ws.getTrigRecordLayout().setTrigTypGrpCd(trigTypGrpCd);
	}

	@Override
	public char getTrigVbrIn() {
		return ws.getTrigRecordLayout().getTrigVbrIn();
	}

	@Override
	public void setTrigVbrIn(char trigVbrIn) {
		ws.getTrigRecordLayout().setTrigVbrIn(trigVbrIn);
	}

	@Override
	public char getTrigVoidCd() {
		return ws.getTrigRecordLayout().getTrigVoidCd();
	}

	@Override
	public void setTrigVoidCd(char trigVoidCd) {
		ws.getTrigRecordLayout().setTrigVoidCd(trigVoidCd);
	}

	@Override
	public String getTsCd() {
		return ws.getDcls02809sa().getTsCd();
	}

	@Override
	public void setTsCd(String tsCd) {
		ws.getDcls02809sa().setTsCd(tsCd);
	}

	@Override
	public AfDecimal getUnitCt() {
		return ws.getDcls02809sa().getUnitCt();
	}

	@Override
	public void setUnitCt(AfDecimal unitCt) {
		ws.getDcls02809sa().setUnitCt(unitCt.copy());
	}

	@Override
	public AfDecimal getWsAccruedPrmptPayIntAm() {
		return ws.getProgramHoldAreas().getWsAccruedPrmptPayIntAm();
	}

	@Override
	public void setWsAccruedPrmptPayIntAm(AfDecimal wsAccruedPrmptPayIntAm) {
		ws.getProgramHoldAreas().setWsAccruedPrmptPayIntAm(wsAccruedPrmptPayIntAm.copy());
	}

	@Override
	public AfDecimal getWsEobAm() {
		return ws.getProgramSwitches().getWsEobAm();
	}

	@Override
	public void setWsEobAm(AfDecimal wsEobAm) {
		ws.getProgramSwitches().setWsEobAm(wsEobAm.copy());
	}

	@Override
	public char getWsHistBundleInd() {
		return ws.getProgramSwitches().getWsHistBundleInd();
	}

	@Override
	public void setWsHistBundleInd(char wsHistBundleInd) {
		ws.getProgramSwitches().setWsHistBundleInd(wsHistBundleInd);
	}

	@Override
	public AfDecimal getWsTrigAltDrgAlwAm() {
		return ws.getProgramHoldAreas().getWsTrigAltDrgAlwAm();
	}

	@Override
	public void setWsTrigAltDrgAlwAm(AfDecimal wsTrigAltDrgAlwAm) {
		ws.getProgramHoldAreas().setWsTrigAltDrgAlwAm(wsTrigAltDrgAlwAm.copy());
	}

	@Override
	public AfDecimal getWsTrigClmPdAm() {
		return ws.getProgramHoldAreas().getWsTrigClmPdAm();
	}

	@Override
	public void setWsTrigClmPdAm(AfDecimal wsTrigClmPdAm) {
		ws.getProgramHoldAreas().setWsTrigClmPdAm(wsTrigClmPdAm.copy());
	}

	@Override
	public short getWsTrigGlSoteOrigCd() {
		return ws.getProgramHoldAreas().getWsTrigGlSoteOrigCd();
	}

	@Override
	public void setWsTrigGlSoteOrigCd(short wsTrigGlSoteOrigCd) {
		ws.getProgramHoldAreas().setWsTrigGlSoteOrigCd(wsTrigGlSoteOrigCd);
	}

	@Override
	public short getWsTrigLstFnlPmtPtId() {
		return ws.getProgramHoldAreas().getWsTrigLstFnlPmtPtId();
	}

	@Override
	public void setWsTrigLstFnlPmtPtId(short wsTrigLstFnlPmtPtId) {
		ws.getProgramHoldAreas().setWsTrigLstFnlPmtPtId(wsTrigLstFnlPmtPtId);
	}

	@Override
	public AfDecimal getWsTrigOverDrgAlwAm() {
		return ws.getProgramHoldAreas().getWsTrigOverDrgAlwAm();
	}

	@Override
	public void setWsTrigOverDrgAlwAm(AfDecimal wsTrigOverDrgAlwAm) {
		ws.getProgramHoldAreas().setWsTrigOverDrgAlwAm(wsTrigOverDrgAlwAm.copy());
	}

	@Override
	public AfDecimal getWsTrigSchdlDrgAlwAm() {
		return ws.getProgramHoldAreas().getWsTrigSchdlDrgAlwAm();
	}

	@Override
	public void setWsTrigSchdlDrgAlwAm(AfDecimal wsTrigSchdlDrgAlwAm) {
		ws.getProgramHoldAreas().setWsTrigSchdlDrgAlwAm(wsTrigSchdlDrgAlwAm.copy());
	}

	@Override
	public char getWsXr4ActInactCd() {
		return ws.getProgramHoldAreas().getWsXr4ActInactCd();
	}

	@Override
	public void setWsXr4ActInactCd(char wsXr4ActInactCd) {
		ws.getProgramHoldAreas().setWsXr4ActInactCd(wsXr4ActInactCd);
	}

	@Override
	public String getWsXr4AdjdProcCd() {
		return ws.getProgramHoldAreas().getWsXr4AdjdProcCd();
	}

	@Override
	public void setWsXr4AdjdProcCd(String wsXr4AdjdProcCd) {
		ws.getProgramHoldAreas().setWsXr4AdjdProcCd(wsXr4AdjdProcCd);
	}

	@Override
	public String getWsXr4AdjdProcMod1Cd() {
		return ws.getProgramHoldAreas().getWsXr4AdjdProcMod1Cd();
	}

	@Override
	public void setWsXr4AdjdProcMod1Cd(String wsXr4AdjdProcMod1Cd) {
		ws.getProgramHoldAreas().setWsXr4AdjdProcMod1Cd(wsXr4AdjdProcMod1Cd);
	}

	@Override
	public String getWsXr4AdjdProcMod2Cd() {
		return ws.getProgramHoldAreas().getWsXr4AdjdProcMod2Cd();
	}

	@Override
	public void setWsXr4AdjdProcMod2Cd(String wsXr4AdjdProcMod2Cd) {
		ws.getProgramHoldAreas().setWsXr4AdjdProcMod2Cd(wsXr4AdjdProcMod2Cd);
	}

	@Override
	public String getWsXr4AdjdProcMod3Cd() {
		return ws.getProgramHoldAreas().getWsXr4AdjdProcMod3Cd();
	}

	@Override
	public void setWsXr4AdjdProcMod3Cd(String wsXr4AdjdProcMod3Cd) {
		ws.getProgramHoldAreas().setWsXr4AdjdProcMod3Cd(wsXr4AdjdProcMod3Cd);
	}

	@Override
	public String getWsXr4AdjdProcMod4Cd() {
		return ws.getProgramHoldAreas().getWsXr4AdjdProcMod4Cd();
	}

	@Override
	public void setWsXr4AdjdProcMod4Cd(String wsXr4AdjdProcMod4Cd) {
		ws.getProgramHoldAreas().setWsXr4AdjdProcMod4Cd(wsXr4AdjdProcMod4Cd);
	}

	@Override
	public String getWsXr4CrsRefCliId() {
		return ws.getProgramHoldAreas().getWsXr4CrsRefCliN().getWsXr4CrsRefCliId();
	}

	@Override
	public void setWsXr4CrsRefCliId(String wsXr4CrsRefCliId) {
		ws.getProgramHoldAreas().getWsXr4CrsRefCliN().setWsXr4CrsRefCliId(wsXr4CrsRefCliId);
	}

	@Override
	public String getWsXr4CrsRefClmPmtId() {
		return ws.getProgramHoldAreas().getWsXr4CrsRefClmPmtN().getWsXr4CrsRefClmPmtId();
	}

	@Override
	public void setWsXr4CrsRefClmPmtId(String wsXr4CrsRefClmPmtId) {
		ws.getProgramHoldAreas().getWsXr4CrsRefClmPmtN().setWsXr4CrsRefClmPmtId(wsXr4CrsRefClmPmtId);
	}

	@Override
	public char getWsXr4CrsRefRsnCd() {
		return ws.getProgramHoldAreas().getWsXr4CrsRefRsnCd();
	}

	@Override
	public void setWsXr4CrsRefRsnCd(char wsXr4CrsRefRsnCd) {
		ws.getProgramHoldAreas().setWsXr4CrsRefRsnCd(wsXr4CrsRefRsnCd);
	}

	@Override
	public String getWsXr4CrsRfClmCntlId() {
		return ws.getProgramHoldAreas().getWsXr4CrsRfClmCntlId();
	}

	@Override
	public void setWsXr4CrsRfClmCntlId(String wsXr4CrsRfClmCntlId) {
		ws.getProgramHoldAreas().setWsXr4CrsRfClmCntlId(wsXr4CrsRfClmCntlId);
	}

	@Override
	public char getWsXr4CsRfClmClSxId() {
		return ws.getProgramHoldAreas().getWsXr4CsRfClmClSxId();
	}

	@Override
	public void setWsXr4CsRfClmClSxId(char wsXr4CsRfClmClSxId) {
		ws.getProgramHoldAreas().setWsXr4CsRfClmClSxId(wsXr4CsRfClmClSxId);
	}

	@Override
	public String getWsXr4DnlRmrkCd() {
		return ws.getProgramHoldAreas().getWsXr4DnlRmrkCd();
	}

	@Override
	public void setWsXr4DnlRmrkCd(String wsXr4DnlRmrkCd) {
		ws.getProgramHoldAreas().setWsXr4DnlRmrkCd(wsXr4DnlRmrkCd);
	}

	@Override
	public String getWsXr4InitClmCntlId() {
		return ws.getProgramHoldAreas().getWsXr4InitClmCntlId();
	}

	@Override
	public void setWsXr4InitClmCntlId(String wsXr4InitClmCntlId) {
		ws.getProgramHoldAreas().setWsXr4InitClmCntlId(wsXr4InitClmCntlId);
	}

	@Override
	public String getWsXr4InitClmPmtId() {
		return ws.getProgramHoldAreas().getWsXr4InitClmPmtId();
	}

	@Override
	public void setWsXr4InitClmPmtId(String wsXr4InitClmPmtId) {
		ws.getProgramHoldAreas().setWsXr4InitClmPmtId(wsXr4InitClmPmtId);
	}

	@Override
	public char getWsXr4IntClmCnlSfxId() {
		return ws.getProgramHoldAreas().getWsXr4IntClmCnlSfxId();
	}

	@Override
	public void setWsXr4IntClmCnlSfxId(char wsXr4IntClmCnlSfxId) {
		ws.getProgramHoldAreas().setWsXr4IntClmCnlSfxId(wsXr4IntClmCnlSfxId);
	}

	@Override
	public String getWsXr4Xr3InitClmTs() {
		return ws.getProgramHoldAreas().getWsXr4Xr3InitClmTs();
	}

	@Override
	public void setWsXr4Xr3InitClmTs(String wsXr4Xr3InitClmTs) {
		ws.getProgramHoldAreas().setWsXr4Xr3InitClmTs(wsXr4Xr3InitClmTs);
	}
}
