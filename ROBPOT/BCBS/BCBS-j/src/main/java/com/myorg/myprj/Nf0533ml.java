/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj;

import static com.bphx.ctu.af.lang.types.AfDecimal.abs;
import static java.lang.Math.abs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.DbService;
import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.io.file.FileAccessStatus;
import com.bphx.ctu.af.io.file.FileBufferedDAO;
import com.bphx.ctu.af.io.file.FileRecordBuffer;
import com.bphx.ctu.af.io.file.OpenMode;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.lang.types.RoundingMode;
import com.bphx.ctu.af.lang.util.MathUtil;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.bphx.ctu.af.util.date.DateFunctions;
import com.bphx.ctu.af.util.display.DisplayUtil;
import com.bphx.ctu.af.util.inspect.InspectUtil;
import com.modernsystems.batch.DdCard;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import com.myorg.myprj.commons.data.dao.S02800caDao;
import com.myorg.myprj.commons.data.dao.S02800caS02809saS02811saS02813saDao;
import com.myorg.myprj.commons.data.dao.S02800saDao;
import com.myorg.myprj.commons.data.dao.S02801saDao;
import com.myorg.myprj.commons.data.dao.S02805saS02809saS02811saS02813saS02952saTrigger1Dao;
import com.myorg.myprj.commons.data.dao.S02809saDao;
import com.myorg.myprj.commons.data.dao.S02813saDao;
import com.myorg.myprj.commons.data.dao.S02813saS04315saDao;
import com.myorg.myprj.commons.data.dao.S02813saS04315saS04316saDao;
import com.myorg.myprj.commons.data.dao.S02814saDao;
import com.myorg.myprj.commons.data.dao.S02815saDao;
import com.myorg.myprj.commons.data.dao.S02993saDao;
import com.myorg.myprj.commons.data.dao.S03085saDao;
import com.myorg.myprj.commons.data.dao.S03086saDao;
import com.myorg.myprj.commons.data.dao.Sr01451Dao;
import com.myorg.myprj.commons.data.dao.Trigger1Dao;
import com.myorg.myprj.commons.data.to.ITrigger1;
import com.myorg.myprj.copy.Sqlca;
import com.myorg.myprj.data.fao.BusinessDateDAO;
import com.myorg.myprj.data.fao.ErrFileDAO;
import com.myorg.myprj.data.fao.TriggerInDAO;
import com.myorg.myprj.data.fao.WriteoffRegDAO;
import com.myorg.myprj.data.fto.BusinessDateTO;
import com.myorg.myprj.data.fto.ErrFileTO;
import com.myorg.myprj.data.fto.TriggerInTO;
import com.myorg.myprj.data.fto.WriteoffRegTO;
import com.myorg.myprj.ws.Nf0533mlData;
import com.myorg.myprj.ws.WsSubroutine;

/**Original name: NF0533ML<br>
 * <pre>AUTHOR.        CLAIMS TEAM.
 * DATE-WRITTEN.  MARCH 2002.
 * DATE-COMPILED.
 * LCAROVILLANO SOURCE-COMPUTER. IBM-3081.
 * LCAROVILLANO SOURCE-COMPUTER. IBM-3033 WITH DEBUGGING MODE.
 * LCAROVILLANO OBJECT-COMPUTER. IBM-3081.
 * LCAROVILLANO SPECIAL-NAMES.
 * LCAROVILLANO     C01 IS TOP-OF-PAGE.</pre>*/
public class Nf0533ml extends BatchProgram implements ITrigger1 {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private S02993saDao s02993saDao = new S02993saDao(dbAccessStatus);
	private Sr01451Dao sr01451Dao = new Sr01451Dao(dbAccessStatus);
	private S02809saDao s02809saDao = new S02809saDao(dbAccessStatus);
	private S02813saS04315saDao s02813saS04315saDao = new S02813saS04315saDao(dbAccessStatus);
	private S02813saS04315saS04316saDao s02813saS04315saS04316saDao = new S02813saS04315saS04316saDao(dbAccessStatus);
	private S02800caDao s02800caDao = new S02800caDao(dbAccessStatus);
	private S02800saDao s02800saDao = new S02800saDao(dbAccessStatus);
	private S02800caS02809saS02811saS02813saDao s02800caS02809saS02811saS02813saDao = new S02800caS02809saS02811saS02813saDao(dbAccessStatus);
	private S02815saDao s02815saDao = new S02815saDao(dbAccessStatus);
	private S02813saDao s02813saDao = new S02813saDao(dbAccessStatus);
	private S03085saDao s03085saDao = new S03085saDao(dbAccessStatus);
	private S03086saDao s03086saDao = new S03086saDao(dbAccessStatus);
	private Trigger1Dao trigger1Dao = new Trigger1Dao(dbAccessStatus);
	private S02805saS02809saS02811saS02813saS02952saTrigger1Dao s02805saS02809saS02811saS02813saS02952saTrigger1Dao = new S02805saS02809saS02811saS02813saS02952saTrigger1Dao(
			dbAccessStatus);
	private S02814saDao s02814saDao = new S02814saDao(dbAccessStatus);
	private S02801saDao s02801saDao = new S02801saDao(dbAccessStatus);
	public ErrFileTO errFileTO = new ErrFileTO();
	public ErrFileDAO errFileDAO = new ErrFileDAO(new FileAccessStatus());
	public FileRecordBuffer restartCardTo = new FileRecordBuffer(Len.FLR1);
	public FileBufferedDAO restartCardDAO = new FileBufferedDAO(new FileAccessStatus(), "RESTART", Len.FLR1);
	public BusinessDateTO businessDateTO = new BusinessDateTO();
	public BusinessDateDAO businessDateDAO = new BusinessDateDAO(new FileAccessStatus());
	public TriggerInTO triggerInTO = new TriggerInTO();
	public TriggerInDAO triggerInDAO = new TriggerInDAO(new FileAccessStatus());
	public FileRecordBuffer covidEndDateTo = new FileRecordBuffer(Len.COVID_END_DTE);
	public FileBufferedDAO covidEndDateDAO = new FileBufferedDAO(new FileAccessStatus(), "ENDDTE", Len.COVID_END_DTE);
	public FileRecordBuffer dailyRegOutputTo = new FileRecordBuffer(Len.EOB_REG_OUTPUT_RECORD);
	public FileBufferedDAO dailyRegOutputDAO = new FileBufferedDAO(new FileAccessStatus(), "REGEOB", Len.EOB_REG_OUTPUT_RECORD);
	public FileRecordBuffer dailyKssOutputTo = new FileRecordBuffer(Len.EOB_KSS_OUTPUT_RECORD);
	public FileBufferedDAO dailyKssOutputDAO = new FileBufferedDAO(new FileAccessStatus(), "KSSEOB", Len.EOB_KSS_OUTPUT_RECORD);
	public FileRecordBuffer expenseRegTo = new FileRecordBuffer(Len.EXPENSE_REG_REC);
	public FileBufferedDAO expenseRegDAO = new FileBufferedDAO(new FileAccessStatus(), "REXPENS", Len.EXPENSE_REG_REC);
	public FileRecordBuffer expenseKssTo = new FileRecordBuffer(Len.EXPENSE_KSS_REC);
	public FileBufferedDAO expenseKssDAO = new FileBufferedDAO(new FileAccessStatus(), "KSSEXP", Len.EXPENSE_KSS_REC);
	public WriteoffRegTO writeoffRegTO = new WriteoffRegTO();
	public WriteoffRegDAO writeoffRegDAO = new WriteoffRegDAO(new FileAccessStatus());
	//Original name: WORKING-STORAGE
	private Nf0533mlData ws = new Nf0533mlData();

	//==== METHODS ====
	/**Original name: 0000-MAINLINE-PROCESS<br>
	 * <pre>    DISPLAY '** 0000-MAINLINE-PROCESS **'.
	 * **TEST MODE
	 *     MOVE 'Y' TO WS-TEST-FLAG.</pre>*/
	public long execute() {
		// COB_CODE: PERFORM 0100-HOUSEKEEPING THRU 0100-EXIT.
		housekeeping();
		// COB_CODE: IF WS-TRIGGER-IN > 0
		//               PERFORM 1000-PROCESS-TRIGGER THRU 1000-EXIT
		//           END-IF.
		if (ws.getWsAllCounters().getWsCounters().getWsTriggerIn() > 0) {
			// COB_CODE: MOVE 'N' TO WS-END-OF-CURSOR-SW
			ws.getWsSwitches().setWsEndOfCursorSwFormatted("N");
			// COB_CODE: PERFORM 0900-LOAD-TRIGGER THRU 0900-EXIT
			//               UNTIL TRIGGER-EOF
			while (!ws.getWsSwitches().isTriggerEof()) {
				loadTrigger();
			}
			// COB_CODE: PERFORM 9200-OPEN-CURSOR THRU 9200-EXIT
			openCursor();
			// COB_CODE: PERFORM 1000-PROCESS-TRIGGER THRU 1000-EXIT
			processTrigger();
		}
		// COB_CODE: PERFORM 9305-CLEAN-UP-RESTART THRU 9305-EXIT.
		cleanUpRestart();
		// COB_CODE: DISPLAY 'TRIGGER FILE INPUT COUNT: ' WS-TRIGGER-IN.
		DisplayUtil.sysout.write("TRIGGER FILE INPUT COUNT: ", ws.getWsAllCounters().getWsCounters().getWsTriggerInAsString());
		// COB_CODE: DISPLAY 'TRIGGER FILE LOADED     : ' WS-TRIGGER-LOADED.
		DisplayUtil.sysout.write("TRIGGER FILE LOADED     : ", ws.getWsAllCounters().getWsCounters().getWsTriggerLoadedAsString());
		// COB_CODE: DISPLAY 'EOB CURSOR RECORDS      : ' WS-FETCH-CNTR.
		DisplayUtil.sysout.write("EOB CURSOR RECORDS      : ", ws.getWsAllCounters().getWsCounters().getWsFetchCntrAsString());
		// COB_CODE: DISPLAY 'EOB BYPASS TRIGGER RECS : ' WS-BYPASS-PMTS.
		DisplayUtil.sysout.write("EOB BYPASS TRIGGER RECS : ", ws.getWsAllCounters().getWsCounters().getWsBypassPmtsAsString());
		// COB_CODE: DISPLAY 'EOB BYPASS NF0736 EOBS  : ' WS-BYPASS-PROV.
		DisplayUtil.sysout.write("EOB BYPASS NF0736 EOBS  : ", ws.getWsAllCounters().getWsCounters().getWsBypassProvAsString());
		// COB_CODE: DISPLAY 'PAYMENT RECORDS UPDATED : ' WS-S02813-UPDATE.
		DisplayUtil.sysout.write("PAYMENT RECORDS UPDATED : ", ws.getWsAllCounters().getWsCounters().getWsS02813UpdateAsString());
		// COB_CODE: DISPLAY 'XREF RECORDS UPDATED    : ' WS-S04315-UPDATES.
		DisplayUtil.sysout.write("XREF RECORDS UPDATED    : ", ws.getWsAllCounters().getWsCounters().getWsS04315UpdatesAsString());
		// COB_CODE: DISPLAY 'VOID PAYMENTS UPDATED   : ' WS-S02813-VOID-UPDATE.
		DisplayUtil.sysout.write("VOID PAYMENTS UPDATED   : ", ws.getWsAllCounters().getWsCounters().getWsS02813VoidUpdateAsString());
		// COB_CODE: DISPLAY 'ITS HOME Y1 EOBS        : ' WS-ITS-Y1-CNT.
		DisplayUtil.sysout.write("ITS HOME Y1 EOBS        : ", ws.getWsAllCounters().getWsCounters().getWsItsY1CntAsString());
		// COB_CODE: DISPLAY '2814 PMNT EVENT INSERTED: ' WS-S02814-INSERTED.
		DisplayUtil.sysout.write("2814 PMNT EVENT INSERTED: ", ws.getWsAllCounters().getWsCounters().getWsS02814InsertedAsString());
		// COB_CODE: DISPLAY '2814 MTH/QTRLY INSERTED : ' WS-S02814-MTH-QTR.
		DisplayUtil.sysout.write("2814 MTH/QTRLY INSERTED : ", ws.getWsAllCounters().getWsCounters().getWsS02814MthQtrAsString());
		// COB_CODE: DISPLAY '2801 HCC CATG INSERTED  : ' WS-S02801-INSERTED.
		DisplayUtil.sysout.write("2801 HCC CATG INSERTED  : ", ws.getWsAllCounters().getWsCounters().getWsS02801InsertedAsString());
		// COB_CODE: DISPLAY 'EOB MONTHLY/QUARTELY    : ' WS-MTH-QTR.
		DisplayUtil.sysout.write("EOB MONTHLY/QUARTELY    : ", ws.getWsAllCounters().getWsCounters().getWsMthQtrAsString());
		// COB_CODE: DISPLAY 'REG LINES OUT COUNTER   : '  WS-LINES-REG-OUT.
		DisplayUtil.sysout.write("REG LINES OUT COUNTER   : ", ws.getWsAllCounters().getWsCounters().getWsLinesRegOutAsString());
		// COB_CODE: DISPLAY 'REG EOB COUNTER         : '   WS-EOB-REG-OUT.
		DisplayUtil.sysout.write("REG EOB COUNTER         : ", ws.getWsAllCounters().getWsCounters().getWsEobRegOutAsString());
		// COB_CODE: DISPLAY 'REG EXPENSE OUT  COUNT  : ' REG-EXPENSE-COUNT.
		DisplayUtil.sysout.write("REG EXPENSE OUT  COUNT  : ", ws.getWsAllCounters().getRegExpenseCountAsString());
		// COB_CODE: DISPLAY 'KSS EXPENSE OUT  COUNT  : ' KSS-EXPENSE-COUNT.
		DisplayUtil.sysout.write("KSS EXPENSE OUT  COUNT  : ", ws.getWsAllCounters().getKssExpenseCountAsString());
		// COB_CODE: DISPLAY 'REG WRITE-OFF OUT COUNT : ' REG-WRITEOFF-CNTR.
		DisplayUtil.sysout.write("REG WRITE-OFF OUT COUNT : ", ws.getWsAllCounters().getRegWriteoffCntrAsString());
		// COB_CODE: DISPLAY 'INTEREST FILE OUT COUNT : ' OUT-INTEREST-CNTR.
		DisplayUtil.sysout.write("INTEREST FILE OUT COUNT : ", ws.getWsAllCounters().getOutInterestCntrAsString());
		// COB_CODE: DISPLAY 'KSS INST FILE OUT COUNT : ' OUT-KSS-INT-CNTR.
		DisplayUtil.sysout.write("KSS INST FILE OUT COUNT : ", ws.getWsAllCounters().getOutKssIntCntrAsString());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: MOVE TOT-LATE-PMT-INTEREST TO DISP-INTEREST-PAID.
		ws.getWsDisplayWorkArea().setDispInterestPaid(ws.getWsAllCounters().getTotLatePmtInterest());
		// COB_CODE: DISPLAY 'LATE PAYMENT INTEREST   : ' DISP-INTEREST-PAID.
		DisplayUtil.sysout.write("LATE PAYMENT INTEREST   : ", ws.getWsDisplayWorkArea().getDispInterestPaidAsString());
		// COB_CODE: MOVE TOT-LATE-DENIAL-INTEREST TO DISP-INTEREST-PAID.
		ws.getWsDisplayWorkArea().setDispInterestPaid(ws.getWsAllCounters().getTotLateDenialInterest());
		// COB_CODE: DISPLAY 'LATE DENIAL INTEREST    : ' DISP-INTEREST-PAID.
		DisplayUtil.sysout.write("LATE DENIAL INTEREST    : ", ws.getWsDisplayWorkArea().getDispInterestPaidAsString());
		// COB_CODE: DISPLAY '                           ------------'.
		DisplayUtil.sysout.write("                           ------------");
		// COB_CODE: MOVE TOT-TOTAL-INTEREST TO DISP-INTEREST-PAID.
		ws.getWsDisplayWorkArea().setDispInterestPaid(ws.getWsAllCounters().getTotTotalInterest());
		// COB_CODE: DISPLAY 'TOTAL PROMPT PAY INTREST: ' DISP-INTEREST-PAID.
		DisplayUtil.sysout.write("TOTAL PROMPT PAY INTREST: ", ws.getWsDisplayWorkArea().getDispInterestPaidAsString());
		// COB_CODE: DISPLAY '                           ------------'.
		DisplayUtil.sysout.write("                           ------------");
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN.
		ws.getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
		// COB_CODE: MOVE GREGORN-HH   TO FORMAT-HH.
		ws.getFormattedTime().setFormatHhFormatted(ws.getGregorn().getrGregornTime().getGregornHhFormatted());
		// COB_CODE: MOVE GREGORN-MI   TO FORMAT-MM.
		ws.getFormattedTime().setFormatMmFormatted(ws.getGregorn().getrGregornTime().getGregornMiFormatted());
		// COB_CODE: MOVE GREGORN-SS   TO FORMAT-SS.
		ws.getFormattedTime().setFormatSsFormatted(ws.getGregorn().getrGregornTime().getGregornSsFormatted());
		// COB_CODE: DISPLAY '**********************************'.
		DisplayUtil.sysout.write("**********************************");
		// COB_CODE: DISPLAY '  NF0533ML ENDING AT '
		//                 FORMATTED-TIME.
		DisplayUtil.sysout.write("  NF0533ML ENDING AT ", ws.getFormattedTime().getFormattedTimeFormatted());
		// COB_CODE: DISPLAY '**********************************'.
		DisplayUtil.sysout.write("**********************************");
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: CLOSE EXPENSE-REG
		//                 WRITEOFF-REG
		//                 RESTART-CARD
		//                 BUSINESS-DATE
		//                 EXPENSE-KSS
		//                 DAILY-REG-OUTPUT
		//                 DAILY-KSS-OUTPUT
		//                 ERR-FILE.
		expenseRegDAO.close();
		writeoffRegDAO.close();
		restartCardDAO.close();
		businessDateDAO.close();
		expenseKssDAO.close();
		dailyRegOutputDAO.close();
		dailyKssOutputDAO.close();
		errFileDAO.close();
		// COB_CODE: IF APPL-TRM-CD  = 'T'
		//               MOVE 0004   TO RETURN-CODE
		//           END-IF.
		if (ws.getDcls03086sa().getApplTrmCd() == 'T') {
			// COB_CODE: MOVE 0004   TO RETURN-CODE
			Session.setReturnCode(4);
		}
		// COB_CODE: GOBACK.
		//last return statement was skipped
		return 0;
	}

	public static Nf0533ml getInstance() {
		return (Programs.getInstance(Nf0533ml.class));
	}

	/**Original name: 0100-HOUSEKEEPING<br>
	 * <pre>                                                                *
	 *     DISPLAY '** 0100-HOUSEKEEPING     **'.</pre>*/
	private void housekeeping() {
		// COB_CODE: OPEN INPUT TRIGGER-IN
		//                      RESTART-CARD
		//                      BUSINESS-DATE
		//                      COVID-END-DATE
		//                OUTPUT EXPENSE-REG
		//                       WRITEOFF-REG
		//                       DAILY-REG-OUTPUT
		//                       DAILY-KSS-OUTPUT
		//                       EXPENSE-KSS
		//                       ERR-FILE.
		triggerInDAO.open(OpenMode.READ, "Nf0533ml");
		restartCardDAO.open(OpenMode.READ, "Nf0533ml");
		businessDateDAO.open(OpenMode.READ, "Nf0533ml");
		covidEndDateDAO.open(OpenMode.READ, "Nf0533ml");
		expenseRegDAO.open(OpenMode.WRITE, "Nf0533ml");
		writeoffRegDAO.open(OpenMode.WRITE, "Nf0533ml");
		dailyRegOutputDAO.open(OpenMode.WRITE, "Nf0533ml");
		dailyKssOutputDAO.open(OpenMode.WRITE, "Nf0533ml");
		expenseKssDAO.open(OpenMode.WRITE, "Nf0533ml");
		errFileDAO.open(OpenMode.WRITE, "Nf0533ml");
		// COB_CODE: READ COVID-END-DATE INTO HOLD-COVID-END-DATE.
		covidEndDateTo = covidEndDateDAO.read(covidEndDateTo);
		if (covidEndDateDAO.getFileStatus().isSuccess()) {
			ws.getWsSwitches().setHoldCovidEndDateFromBuffer(covidEndDateTo.getData());
		}
		// COB_CODE: PERFORM 8010-GET-BUSINESS-DATE THRU 8010-EXIT.
		getBusinessDate();
		// COB_CODE: COMPUTE DAILY-INTEREST-RATE = ((1 / 365) * .12).
		ws.getWorkFields().setDailyInterestRate(Trunc.toDecimal((new AfDecimal(((((double) 1)) / 365), 8, 7)).multiply("0.12"), 8, 7));
		// COB_CODE: INITIALIZE WS-ALL-COUNTERS.
		initWsAllCounters();
		// COB_CODE: INITIALIZE WS-LINE-ACCUMULATORS.
		initLineAccumulators();
		// COB_CODE: INITIALIZE WS-CLAIM-ACCUMULATORS.
		initClaimAccumulators();
		// COB_CODE: INITIALIZE WS-HOLD-NOTE-AREA.
		initWsHoldNoteArea();
		// COB_CODE: INITIALIZE WS-PROMPT-PAY-WORK-FIELDS.
		initWsPromptPayWorkFields();
		// COB_CODE: MOVE SPACES TO ERROR-FILE-LAYOUT.
		errFileTO.initErrorFileLayoutSpaces();
		//* LOAD PRINTABLE REMARKS FROM INTO INTERNAL TABLE
		// COB_CODE: PERFORM 0150-PROCESS-NON-PRT-RMKS THRU 0150-EXIT.
		processNonPrtRmks();
		//* LOAD PROVIDERS WITH A STOP PAY CODE OF G TO AN INTERNAL TABLE
		//* THESE ARE SRS PROVIDERS
		// COB_CODE: PERFORM 0175-PROCESS-SRS-PROVIDER THRU 0175-EXIT.
		processSrsProvider();
		// COB_CODE: PERFORM 9100-DECLARE-TEMP-TABLE THRU 9100-EXIT.
		declareTempTable();
		// COB_CODE: MOVE 'NO ' TO EOF-TRIGGER-SW.
		ws.getWsSwitches().setEofTriggerSw("NO ");
		// COB_CODE: PERFORM 0200-CHECK-FOR-RESTART THRU 0200-EXIT
		checkForRestart();
		// COB_CODE: EVALUATE APPL-TRM-CD
		//             WHEN 'T'
		//             WHEN 'R'
		//               PERFORM 9997-FORCE-EOJ THRU 9997-EXIT
		//             WHEN ' '
		//                 CONTINUE
		//             WHEN OTHER
		//               PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (ws.getDcls03086sa().getApplTrmCd()) {

		case 'T':
		case 'R':// COB_CODE: PERFORM 9997-FORCE-EOJ THRU 9997-EXIT
			forceEoj();
			break;

		case ' ':// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY '***********************************'
			DisplayUtil.sysout.write("***********************************");
			// COB_CODE: DISPLAY '      NF0533ML ABENDING     '
			DisplayUtil.sysout.write("      NF0533ML ABENDING     ");
			// COB_CODE: DISPLAY 'INVALID APPLICATION TERMINATION CODE:'
			DisplayUtil.sysout.write("INVALID APPLICATION TERMINATION CODE:");
			// COB_CODE: DISPLAY '  APPL-TRM-CD OF DCLS03086SA = '
			//                                APPL-TRM-CD
			DisplayUtil.sysout.write("  APPL-TRM-CD OF DCLS03086SA = ", String.valueOf(ws.getDcls03086sa().getApplTrmCd()));
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY 'UPDATE THIS COLUMN ON TABLE 3086.  '
			DisplayUtil.sysout.write("UPDATE THIS COLUMN ON TABLE 3086.  ");
			// COB_CODE: DISPLAY 'VALID VALUES ARE:                  '
			DisplayUtil.sysout.write("VALID VALUES ARE:                  ");
			// COB_CODE: DISPLAY '  BLANK -- NO EARLY TERMINATION, '
			//                      'RUN TILL GOOD EOJ'
			DisplayUtil.sysout.write("  BLANK -- NO EARLY TERMINATION, ", "RUN TILL GOOD EOJ");
			// COB_CODE: DISPLAY '  R     -- TERMINATE AT NEXT COMMIT AND '
			//                      'RUN SUBSEQUENT STEPS AND JOBS'
			DisplayUtil.sysout.write("  R     -- TERMINATE AT NEXT COMMIT AND ", "RUN SUBSEQUENT STEPS AND JOBS");
			// COB_CODE: DISPLAY '         (CONSIDER JOB COMPLETE)'
			DisplayUtil.sysout.write("         (CONSIDER JOB COMPLETE)");
			// COB_CODE: DISPLAY '  T     -- TERMINATE AT NEXT COMMIT AND '
			//                      'DO NOT RUN SUBSEQUENT STEPS AND JOBS'
			DisplayUtil.sysout.write("  T     -- TERMINATE AT NEXT COMMIT AND ", "DO NOT RUN SUBSEQUENT STEPS AND JOBS");
			// COB_CODE: DISPLAY '         (A RESTART WILL BE DONE LATER TO '
			//                      'PROCESS THE REMAINING DATA)'
			DisplayUtil.sysout.write("         (A RESTART WILL BE DONE LATER TO ", "PROCESS THE REMAINING DATA)");
			// COB_CODE: DISPLAY '***********************************'
			DisplayUtil.sysout.write("***********************************");
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 0150-PROCESS-NON-PRT-RMKS<br>
	 * <pre>                                                                *
	 *                                                                 *
	 *     DISPLAY '** 0150-PROCESS-NON-PRT-RMKS **'</pre>*/
	private void processNonPrtRmks() {
		// COB_CODE: EXEC SQL
		//            OPEN NON_PRT_RMKS
		//           END-EXEC
		s02993saDao.openNonPrtRmks();
		// COB_CODE: IF SQLCODE = 0
		//                     OR WS-NON-PRT-RMKS-FINISHED = 'Y'
		//           ELSE
		//              CALL 'ABEND'
		//           END-IF.
		if (sqlca.getSqlcode() == 0) {
			// COB_CODE: MOVE 'N' TO WS-NON-PRT-RMKS-FINISHED
			ws.getWsNonPrtRmksVariables().setNonPrtRmksFinishedFormatted("N");
			// COB_CODE: PERFORM 0151-LOAD-NON-PRT-RMKS THRU 0151-EXIT
			//               VARYING WS-SUB-NON-PRT-RMKS
			//               FROM 1 BY 1
			//               UNTIL WS-SUB-NON-PRT-RMKS >
			//                     WS-NON-PRT-RMKS-MAX
			//                  OR WS-NON-PRT-RMKS-FINISHED = 'Y'
			ws.getWsNonPrtRmksVariables().setSubNonPrtRmks(((short) 1));
			while (!(ws.getWsNonPrtRmksVariables().getSubNonPrtRmks() > ws.getWsNonPrtRmksVariables().getNonPrtRmksMax()
					|| ws.getWsNonPrtRmksVariables().getNonPrtRmksFinished() == 'Y')) {
				loadNonPrtRmks();
				ws.getWsNonPrtRmksVariables().setSubNonPrtRmks(Trunc.toShort(ws.getWsNonPrtRmksVariables().getSubNonPrtRmks() + 1, 4));
			}
		} else {
			// COB_CODE: MOVE 'TA02993'           TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("TA02993");
			// COB_CODE: MOVE 'OPEN'            TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("OPEN");
			// COB_CODE: MOVE '0150-PROCESS-NON-PRT-RMKS' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("0150-PROCESS-NON-PRT-RMKS");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
		}
		// COB_CODE: EXEC SQL
		//            CLOSE NON_PRT_RMKS
		//           END-EXEC.
		s02993saDao.closeNonPrtRmks();
	}

	/**Original name: 0151-LOAD-NON-PRT-RMKS<br>
	 * <pre>                                                                *
	 *     DISPLAY '** 0151-LOAD-NON-PRT-RMKS **'</pre>*/
	private void loadNonPrtRmks() {
		// COB_CODE: EXEC SQL
		//              FETCH NON_PRT_RMKS
		//              INTO   :DCLS02993SA.DNL-RMRK-CD
		//           END-EXEC
		s02993saDao.fetchNonPrtRmks(ws.getDcls02993sa());
		// COB_CODE: IF SQLCODE = 0
		//              END-IF
		//           ELSE
		//              END-IF
		//           END-IF.
		if (sqlca.getSqlcode() == 0) {
			// COB_CODE: IF WS-SUB-NON-PRT-RMKS > WS-NON-PRT-RMKS-MAX
			//              CALL 'ABEND'
			//           ELSE
			//                  WS-NON-PRT-RMKS (WS-SUB-NON-PRT-RMKS)
			//           END-IF
			if (ws.getWsNonPrtRmksVariables().getSubNonPrtRmks() > ws.getWsNonPrtRmksVariables().getNonPrtRmksMax()) {
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: DISPLAY '******************************'
				DisplayUtil.sysout.write("******************************");
				// COB_CODE: DISPLAY '  WS-NON-PRT-RMKS-TABLE NOT   '
				DisplayUtil.sysout.write("  WS-NON-PRT-RMKS-TABLE NOT   ");
				// COB_CODE: DISPLAY '  BIG ENOUGH                  '
				DisplayUtil.sysout.write("  BIG ENOUGH                  ");
				// COB_CODE: DISPLAY '******************************'
				DisplayUtil.sysout.write("******************************");
				// COB_CODE: CALL 'ABEND'
				DynamicCall.invoke("ABEND");
			} else {
				// COB_CODE: MOVE DNL-RMRK-CD OF DCLS02993SA TO
				//               WS-NON-PRT-RMKS (WS-SUB-NON-PRT-RMKS)
				ws.getWsNonPrtRmksVariables().getNonPrtRmksTable(ws.getWsNonPrtRmksVariables().getSubNonPrtRmks())
						.setWsNonPrtRmks(ws.getDcls02993sa().getDnlRmrkCd());
			}
		} else if (sqlca.getSqlcode() == 100) {
			// COB_CODE: IF SQLCODE = +100
			//              SUBTRACT 1 FROM WS-NON-PRT-RMKS-TBL-ENTRIES
			//           ELSE
			//              CALL 'ABEND'
			//           END-IF
			// COB_CODE: MOVE 'Y' TO WS-NON-PRT-RMKS-FINISHED
			ws.getWsNonPrtRmksVariables().setNonPrtRmksFinishedFormatted("Y");
			// COB_CODE: MOVE WS-SUB-NON-PRT-RMKS TO
			//                 WS-NON-PRT-RMKS-TBL-ENTRIES
			ws.getWsNonPrtRmksVariables().setNonPrtRmksTblEntries(ws.getWsNonPrtRmksVariables().getSubNonPrtRmks());
			// COB_CODE: SUBTRACT 1 FROM WS-NON-PRT-RMKS-TBL-ENTRIES
			ws.getWsNonPrtRmksVariables().setNonPrtRmksTblEntries(Trunc.toShort(abs(ws.getWsNonPrtRmksVariables().getNonPrtRmksTblEntries() - 1), 4));
		} else {
			// COB_CODE: MOVE 'TA02993' TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("TA02993");
			// COB_CODE: MOVE 'FETCH' TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("FETCH");
			// COB_CODE: MOVE '0151-LOAD-NON-PRT-RMKS' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("0151-LOAD-NON-PRT-RMKS");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
		}
	}

	/**Original name: 0175-PROCESS-SRS-PROVIDER<br>
	 * <pre>    DISPLAY '** 0175-PROCESS-SRS-PROVIDER ** '.</pre>*/
	private void processSrsProvider() {
		// COB_CODE: EXEC SQL
		//            OPEN SRS_STOP_PAY_G
		//           END-EXEC.
		sr01451Dao.openSrsStopPayG();
		// COB_CODE: DISPLAY ' ==================================='.
		DisplayUtil.sysout.write(" ===================================");
		// COB_CODE: DISPLAY ' TABLE STOP PAY PROVIDERS BEGIN LOAD'.
		DisplayUtil.sysout.write(" TABLE STOP PAY PROVIDERS BEGIN LOAD");
		// COB_CODE: DISPLAY ' ==================================='.
		DisplayUtil.sysout.write(" ===================================");
		// COB_CODE: IF SQLCODE = 0
		//                     OR WS-PRV-STOP-PAY-FINISHED = 'Y'
		//           ELSE
		//              CALL 'ABEND'
		//           END-IF.
		if (sqlca.getSqlcode() == 0) {
			// COB_CODE: MOVE 'N' TO WS-PRV-STOP-PAY-FINISHED
			ws.getWsPrvStopPayVariables().setWsPrvStopPayFinishedFormatted("N");
			// COB_CODE: PERFORM 0176-LOAD-PROV-STOP-PAY-CD THRU 0176-EXIT
			//               VARYING WS-SUB-PRV-STOP-PAY
			//               FROM 1 BY 1
			//               UNTIL WS-SUB-PRV-STOP-PAY >
			//                     WS-PRV-STOP-PAY-MAX
			//                  OR WS-PRV-STOP-PAY-FINISHED = 'Y'
			ws.getWsPrvStopPayVariables().setWsSubPrvStopPay(((short) 1));
			while (!(ws.getWsPrvStopPayVariables().getWsSubPrvStopPay() > ws.getWsPrvStopPayVariables().getWsPrvStopPayMax()
					|| ws.getWsPrvStopPayVariables().getWsPrvStopPayFinished() == 'Y')) {
				loadProvStopPayCd();
				ws.getWsPrvStopPayVariables().setWsSubPrvStopPay(Trunc.toShort(ws.getWsPrvStopPayVariables().getWsSubPrvStopPay() + 1, 4));
			}
		} else {
			// COB_CODE: MOVE 'TA01451'           TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("TA01451");
			// COB_CODE: MOVE 'OPEN TA01451'    TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("OPEN TA01451");
			// COB_CODE: MOVE '0175-PROCESS-SRS-PROVIDER' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("0175-PROCESS-SRS-PROVIDER");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
		}
		// COB_CODE: DISPLAY ' ==============================='.
		DisplayUtil.sysout.write(" ===============================");
		// COB_CODE: DISPLAY ' TABLE STOP PAY PROVIDERS LOADED'.
		DisplayUtil.sysout.write(" TABLE STOP PAY PROVIDERS LOADED");
		// COB_CODE: DISPLAY ' ==============================='.
		DisplayUtil.sysout.write(" ===============================");
		// COB_CODE: EXEC SQL
		//            CLOSE SRS_STOP_PAY_G
		//           END-EXEC.
		sr01451Dao.closeSrsStopPayG();
	}

	/**Original name: 0176-LOAD-PROV-STOP-PAY-CD<br>
	 * <pre>    DISPLAY '** 0176-LOAD-PROV-STOP-PAY-CD **'.</pre>*/
	private void loadProvStopPayCd() {
		// COB_CODE: EXEC SQL
		//              FETCH SRS_STOP_PAY_G
		//                 INTO  :DCLSR01451.PROV-ID
		//                     , :DCLSR01451.PROV-LOB-CD
		//                     , :DCLSR01451.PROV-CN-ARNG-EF-DT
		//                     , :DCLSR01451.PRV-CN-ARNG-TRM-DT
		//           END-EXEC.
		sr01451Dao.fetchSrsStopPayG(ws.getDclsr01451());
		// COB_CODE: IF SQLCODE = 0
		//              END-IF
		//           ELSE
		//              END-IF
		//           END-IF.
		if (sqlca.getSqlcode() == 0) {
			// COB_CODE: IF WS-SUB-PRV-STOP-PAY > WS-PRV-STOP-PAY-MAX
			//              CALL 'ABEND'
			//           ELSE
			//                  SRS-PROV-CN-ARNG-TRM-DT (WS-SUB-PRV-STOP-PAY)
			//           END-IF
			if (ws.getWsPrvStopPayVariables().getWsSubPrvStopPay() > ws.getWsPrvStopPayVariables().getWsPrvStopPayMax()) {
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: DISPLAY '******************************'
				DisplayUtil.sysout.write("******************************");
				// COB_CODE: DISPLAY '  WS-PRV-STOP-PAY-CD TABLE    '
				DisplayUtil.sysout.write("  WS-PRV-STOP-PAY-CD TABLE    ");
				// COB_CODE: DISPLAY '        NOT BIG ENOUGH        '
				DisplayUtil.sysout.write("        NOT BIG ENOUGH        ");
				// COB_CODE: DISPLAY '******************************'
				DisplayUtil.sysout.write("******************************");
				// COB_CODE: CALL 'ABEND'
				DynamicCall.invoke("ABEND");
			} else {
				// COB_CODE: MOVE PROV-ID OF DCLSR01451 TO
				//               SRS-PROV-ID (WS-SUB-PRV-STOP-PAY)
				ws.getWsPrvStopPayVariables().getWsPrvStopPayTable(ws.getWsPrvStopPayVariables().getWsSubPrvStopPay())
						.setId(ws.getDclsr01451().getProvId());
				// COB_CODE: MOVE PROV-LOB-CD OF DCLSR01451 TO
				//               SRS-PROV-LOB-CD (WS-SUB-PRV-STOP-PAY)
				ws.getWsPrvStopPayVariables().getWsPrvStopPayTable(ws.getWsPrvStopPayVariables().getWsSubPrvStopPay())
						.setLobCd(ws.getDclsr01451().getProvLobCd());
				// COB_CODE: MOVE PROV-CN-ARNG-EF-DT OF DCLSR01451 TO
				//               SRS-PROV-CN-ARNG-EF-DT (WS-SUB-PRV-STOP-PAY)
				ws.getWsPrvStopPayVariables().getWsPrvStopPayTable(ws.getWsPrvStopPayVariables().getWsSubPrvStopPay())
						.setCnArngEfDt(ws.getDclsr01451().getProvCnArngEfDt());
				// COB_CODE: MOVE PRV-CN-ARNG-TRM-DT OF DCLSR01451 TO
				//               SRS-PROV-CN-ARNG-TRM-DT (WS-SUB-PRV-STOP-PAY)
				ws.getWsPrvStopPayVariables().getWsPrvStopPayTable(ws.getWsPrvStopPayVariables().getWsSubPrvStopPay())
						.setCnArngTrmDt(ws.getDclsr01451().getPrvCnArngTrmDt());
			}
		} else if (sqlca.getSqlcode() == 100) {
			// COB_CODE: IF SQLCODE = +100
			//              SUBTRACT 1 FROM WS-PRV-STOP-PAY-TBL-ENTRIES
			//           ELSE
			//              CALL 'ABEND'
			//           END-IF
			// COB_CODE: MOVE 'Y' TO WS-PRV-STOP-PAY-FINISHED
			ws.getWsPrvStopPayVariables().setWsPrvStopPayFinishedFormatted("Y");
			// COB_CODE: MOVE WS-SUB-PRV-STOP-PAY TO
			//                 WS-PRV-STOP-PAY-TBL-ENTRIES
			ws.getWsPrvStopPayVariables().setWsPrvStopPayTblEntries(ws.getWsPrvStopPayVariables().getWsSubPrvStopPay());
			// COB_CODE: SUBTRACT 1 FROM WS-PRV-STOP-PAY-TBL-ENTRIES
			ws.getWsPrvStopPayVariables()
					.setWsPrvStopPayTblEntries(Trunc.toShort(abs(ws.getWsPrvStopPayVariables().getWsPrvStopPayTblEntries() - 1), 4));
		} else {
			// COB_CODE: MOVE 'TA01451' TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("TA01451");
			// COB_CODE: MOVE 'FETCH TA01451' TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("FETCH TA01451");
			// COB_CODE: MOVE '0176-LOAD-PROV-STOP-PAY-CD' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("0176-LOAD-PROV-STOP-PAY-CD");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
		}
	}

	/**Original name: 0200-CHECK-FOR-RESTART<br>
	 * <pre>                                                                *
	 *     DISPLAY '** 0200-CHECK-FOR-RESTART **'.
	 *                                                                 *</pre>*/
	private void checkForRestart() {
		// COB_CODE: PERFORM 8005-READ-RESTART-CARD THRU 8005-EXIT.
		readRestartCard();
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY 'RESTART PARMS ARE: '.
		DisplayUtil.sysout.write("RESTART PARMS ARE: ");
		// COB_CODE: DISPLAY '        JOB NAME : ' WS-RESTART-JOB-NAME.
		DisplayUtil.sysout.write("        JOB NAME : ", ws.getWsRestartStuff().getWsRestartJobParms().getRestartJobNameFormatted());
		// COB_CODE: DISPLAY '        JOB STEP : ' WS-RESTART-STEP-NAME.
		DisplayUtil.sysout.write("        JOB STEP : ", ws.getWsRestartStuff().getWsRestartJobParms().getRestartStepNameFormatted());
		// COB_CODE: DISPLAY '        JOB SEQ  : ' WS-RESTART-JOB-SEQ.
		DisplayUtil.sysout.write("        JOB SEQ  : ", ws.getWsRestartStuff().getWsRestartJobParms().getRestartJobSeqAsString());
		// COB_CODE: MOVE WS-RESTART-JOB-SEQ TO WS-RESTART-JOB-SEQ-PACKED.
		ws.getWsRestartStuff().setWsRestartJobSeqPacked(Trunc.toShort(ws.getWsRestartStuff().getWsRestartJobParms().getRestartJobSeq(), 3));
		// COB_CODE: PERFORM 9000-SELECT-S03085SA THRU 9000-EXIT.
		selectS03085sa();
		// COB_CODE: DISPLAY '        RSTRT-IN : ' RSTRT-IN.
		DisplayUtil.sysout.write("        RSTRT-IN : ", String.valueOf(ws.getDcls03085sa().getRstrtIn()));
		// COB_CODE: DISPLAY '    JBSTP-RUN-CT : ' JBSTP-RUN-CT.
		DisplayUtil.sysout.write("    JBSTP-RUN-CT : ", ws.getDcls03085sa().getJbstpRunCtAsString());
		// COB_CODE: PERFORM 9020-SELECT-S03086SA THRU 9020-EXIT.
		selectS03086sa();
		// COB_CODE: DISPLAY '     APPL-TRM-CD : ' APPL-TRM-CD.
		DisplayUtil.sysout.write("     APPL-TRM-CD : ", String.valueOf(ws.getDcls03086sa().getApplTrmCd()));
		// COB_CODE: DISPLAY '   KEEP HISTORY? : ' RSTRT-HIST-IN.
		DisplayUtil.sysout.write("   KEEP HISTORY? : ", String.valueOf(ws.getDcls03086sa().getRstrtHistIn()));
		// COB_CODE: MOVE ROW-FREQ-CMT-CT TO WS-DISP-CMT-CT.
		ws.getWsDisplayWorkArea().setWsDispCmtCt(TruncAbs.toInt(ws.getDcls03086sa().getRowFreqCmtCt(), 9));
		// COB_CODE: DISPLAY 'COMMIT FREQUENCY : ' WS-DISP-CMT-CT.
		DisplayUtil.sysout.write("COMMIT FREQUENCY : ", ws.getWsDisplayWorkArea().getWsDispCmtCtAsString());
		// COB_CODE: DISPLAY '   TIME INTERVAL : ' CMT-TM-INT-TM.
		DisplayUtil.sysout.write("   TIME INTERVAL : ", ws.getDcls03086sa().getCmtTmIntTmFormatted());
		// COB_CODE: MOVE CMT-CT TO WS-DISP-CMT-CT.
		ws.getWsDisplayWorkArea().setWsDispCmtCt(TruncAbs.toInt(ws.getDcls03086sa().getCmtCt(), 9));
		// COB_CODE: DISPLAY '           COUNT : ' WS-DISP-CMT-CT.
		DisplayUtil.sysout.write("           COUNT : ", ws.getWsDisplayWorkArea().getWsDispCmtCtAsString());
		// COB_CODE: DISPLAY '  LAST TIMESTAMP : ' CMT-TS.
		DisplayUtil.sysout.write("  LAST TIMESTAMP : ", ws.getDcls03086sa().getCmtTsFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: MOVE WS-RESTART-JOB-NAME       TO JOB-NM  OF DCLS03085SA.
		ws.getDcls03085sa().setJobNm(ws.getWsRestartStuff().getWsRestartJobParms().getRestartJobName());
		// COB_CODE: MOVE WS-RESTART-STEP-NAME      TO JBSTP-NM OF DCLS03085SA.
		ws.getDcls03085sa().setJbstpNm(ws.getWsRestartStuff().getWsRestartJobParms().getRestartStepName());
		// COB_CODE: MOVE WS-RESTART-JOB-SEQ-PACKED
		//                                        TO JBSTP-SEQ-ID OF DCLS03085SA.
		ws.getDcls03085sa().setJbstpSeqId(ws.getWsRestartStuff().getWsRestartJobSeqPacked());
		// COB_CODE: MOVE WS-RESTART-JOB-NAME       TO JOB-NM  OF DCLS03086SA.
		ws.getDcls03086sa().setJobNm(ws.getWsRestartStuff().getWsRestartJobParms().getRestartJobName());
		// COB_CODE: MOVE WS-RESTART-STEP-NAME      TO JBSTP-NM OF DCLS03086SA.
		ws.getDcls03086sa().setJbstpNm(ws.getWsRestartStuff().getWsRestartJobParms().getRestartStepName());
		// COB_CODE: MOVE WS-RESTART-JOB-SEQ-PACKED
		//                                        TO JBSTP-SEQ-ID OF DCLS03086SA.
		ws.getDcls03086sa().setJbstpSeqId(ws.getWsRestartStuff().getWsRestartJobSeqPacked());
		//*                                                            *
		//  IF THIS IS A RESTART, THE TRIGGER FILE WILL NEED TO BE     *
		//  REPOSITIONED TO START IMMEDIATELY FOLLOWING THAT LAST      *
		//  COMMIT POINT.  IF IT IS NOT A RESTART, THEN THE PRIMEING   *
		//  READ ON THE TRIGGER FILE WILL BE PERFORMED.  THIS MEANS    *
		//  ONLY TRIGGER RECORDS THAT HAVE NOT BEEN PROCESSED WILL     *
		//  BE LOADED TO THE TEMP TABLE.  SINCE THE TEMP TABLE IS      *
		//  THEN JOINED IN THE MAIN CURSOR, ONLY THE TRIGGER RECORDS   *
		//  LEFT TO BE PROCESSED WILL BE JOINED IN THAT CURSOR         *
		//                                                             *
		// COB_CODE: IF RSTRT-IN = 'Y'
		//               END-IF
		//           ELSE
		//               PERFORM 8100-READ-TRIGGER-IN    THRU 8100-EXIT
		//           END-IF.
		if (ws.getDcls03085sa().getRstrtIn() == 'Y') {
			// COB_CODE: IF RSTRT1-TX = SPACES
			//               CALL 'ABEND'
			//           ELSE
			//               PERFORM 0700-RESTART-NF0533ML THRU 0700-EXIT
			//           END-IF
			if (Characters.EQ_SPACE.test(ws.getDcls03086sa().getRstrt1Tx())) {
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: DISPLAY '********* NF0533ML ABENDING ************'
				DisplayUtil.sysout.write("********* NF0533ML ABENDING ************");
				// COB_CODE: DISPLAY ' RESTART FLAG IS SET TO YES'
				DisplayUtil.sysout.write(" RESTART FLAG IS SET TO YES");
				// COB_CODE: DISPLAY ' PROGRAM CAN NOT RESTART BECAUSE'
				DisplayUtil.sysout.write(" PROGRAM CAN NOT RESTART BECAUSE");
				// COB_CODE: DISPLAY ' RSTRT1-TS AREA IS BLANK'
				DisplayUtil.sysout.write(" RSTRT1-TS AREA IS BLANK");
				// COB_CODE: DISPLAY '****************************************'
				DisplayUtil.sysout.write("****************************************");
				// COB_CODE: CALL 'ABEND'
				DynamicCall.invoke("ABEND");
			} else {
				// COB_CODE: PERFORM 0700-RESTART-NF0533ML THRU 0700-EXIT
				restartNf0533ml();
			}
		} else {
			// COB_CODE: MOVE 1   TO JBSTP-RUN-CT
			ws.getDcls03085sa().setJbstpRunCt(((short) 1));
			// COB_CODE: MOVE 0   TO CMT-CT
			ws.getDcls03086sa().setCmtCt(0);
			// COB_CODE: PERFORM 9010-UPDATE-S03085SA    THRU 9010-EXIT
			updateS03085sa();
			// COB_CODE: PERFORM 8100-READ-TRIGGER-IN    THRU 8100-EXIT
			readTriggerIn();
		}
	}

	/**Original name: 0700-RESTART-NF0533ML<br>
	 * <pre>                                                                *
	 *     DISPLAY '** 0700-RESTART-NF0533ML **'.
	 *                                                                 *</pre>*/
	private void restartNf0533ml() {
		// COB_CODE: MOVE RSTRT1-TX TO WS-RESTART-TXT1.
		ws.getWsRestartStuff().setWsRestartTxt1Formatted(ws.getDcls03086sa().getRstrt1TxFormatted());
		// COB_CODE: MOVE RSTRT2-TX TO WS-ALL-COUNTERS.
		ws.getWsAllCounters().setWsAllCountersFormatted(ws.getDcls03086sa().getRstrt2TxFormatted());
		// COB_CODE: DISPLAY '************************************'.
		DisplayUtil.sysout.write("************************************");
		// COB_CODE: DISPLAY '   NF0533ML RESTARTING              '.
		DisplayUtil.sysout.write("   NF0533ML RESTARTING              ");
		// COB_CODE: DISPLAY '  '.
		DisplayUtil.sysout.write("  ");
		// COB_CODE: DISPLAY ' RESTART PROCESSING INDICATOR: ' RESTART-INDICATOR.
		DisplayUtil.sysout.write(" RESTART PROCESSING INDICATOR: ", ws.getWsRestartStuff().getRestartIndicator().getRestartIndicatorFormatted());
		// COB_CODE: DISPLAY '  '.
		DisplayUtil.sysout.write("  ");
		// COB_CODE: DISPLAY 'ON RESTART ----- PROCESS CLAIMS > : '
		//              RESTART-CLAIM-ID ' ' RESTART-CLAIM-SFX ' ' RESTART-PMT-ID.
		DisplayUtil.sysout.write(new String[] { "ON RESTART ----- PROCESS CLAIMS > : ", ws.getWsRestartStuff().getRestartClaimIdFormatted(), " ",
				String.valueOf(ws.getWsRestartStuff().getRestartClaimSfx()), " ", ws.getWsRestartStuff().getRestartPmtIdAsString() });
		// COB_CODE: IF RESTART-IN-NF05
		//               END-IF
		//           END-IF.
		if (ws.getWsRestartStuff().getRestartIndicator().isNf05()) {
			// COB_CODE: PERFORM 8100-READ-TRIGGER-IN    THRU 8100-EXIT
			//               UNTIL TRG-KEY > RESTART-KEY
			//               OR    TRIGGER-EOF
			while (!(Conditions.gt(triggerInTO.getTriggerRecordLayout().getKey().getKeyBytes(), ws.getWsRestartStuff().getRestartKeyBytes())
					|| ws.getWsSwitches().isTriggerEof())) {
				readTriggerIn();
			}
			// COB_CODE: IF TRIGGER-EOF
			//               MOVE 'NF07' TO RESTART-INDICATOR
			//           ELSE
			//                 ' ' TRG-CLM-PMT-ID
			//           END-IF
			if (ws.getWsSwitches().isTriggerEof()) {
				// COB_CODE: DISPLAY '----------> COMMIT WAS ON LAST NF05 RECORD'
				DisplayUtil.sysout.write("----------> COMMIT WAS ON LAST NF05 RECORD");
				// COB_CODE: MOVE 'NF07' TO RESTART-INDICATOR
				ws.getWsRestartStuff().getRestartIndicator().setRestartIndicator("NF07");
			} else {
				// COB_CODE: DISPLAY '------------> RESTARTING ON CLAIM : '
				//             TRG-CLM-CNTL-ID ' ' TRG-CLM-CNTL-SFX-ID
				//             ' ' TRG-CLM-PMT-ID
				DisplayUtil.sysout.write(
						new String[] { "------------> RESTARTING ON CLAIM : ", triggerInTO.getTriggerRecordLayout().getKey().getCntlIdFormatted(),
								" ", String.valueOf(triggerInTO.getTriggerRecordLayout().getKey().getCntlSfxId()), " ",
								triggerInTO.getTriggerRecordLayout().getKey().getPmtIdAsString() });
			}
		}
		// COB_CODE: DISPLAY '*** COUNTERS AT TIME OF LAST COMMIT: '
		DisplayUtil.sysout.write("*** COUNTERS AT TIME OF LAST COMMIT: ");
		// COB_CODE: DISPLAY 'TRIGGER FILE INPUT COUNT: ' WS-TRIGGER-IN.
		DisplayUtil.sysout.write("TRIGGER FILE INPUT COUNT: ", ws.getWsAllCounters().getWsCounters().getWsTriggerInAsString());
		// COB_CODE: DISPLAY 'TRIGGER FILE LOADED     : ' WS-TRIGGER-LOADED.
		DisplayUtil.sysout.write("TRIGGER FILE LOADED     : ", ws.getWsAllCounters().getWsCounters().getWsTriggerLoadedAsString());
		// COB_CODE: DISPLAY 'EOB CURSOR RECORDS      : ' WS-FETCH-CNTR.
		DisplayUtil.sysout.write("EOB CURSOR RECORDS      : ", ws.getWsAllCounters().getWsCounters().getWsFetchCntrAsString());
		// COB_CODE: DISPLAY 'EOB BYPASS TRIGGER RECS : ' WS-BYPASS-PMTS.
		DisplayUtil.sysout.write("EOB BYPASS TRIGGER RECS : ", ws.getWsAllCounters().getWsCounters().getWsBypassPmtsAsString());
		// COB_CODE: DISPLAY 'EOB BYPASS NF0736 EOBS  : ' WS-BYPASS-PROV.
		DisplayUtil.sysout.write("EOB BYPASS NF0736 EOBS  : ", ws.getWsAllCounters().getWsCounters().getWsBypassProvAsString());
		// COB_CODE: DISPLAY 'SRS-DATE-BYPASS-CNTR    : ' SRS-DATE-BYPASS-CNTR.
		DisplayUtil.sysout.write("SRS-DATE-BYPASS-CNTR    : ", ws.getWsAllCounters().getWsCounters().getSrsDateBypassCntrAsString());
		// COB_CODE: DISPLAY 'PAYMENT RECORDS UPDATED : ' WS-S02813-UPDATE.
		DisplayUtil.sysout.write("PAYMENT RECORDS UPDATED : ", ws.getWsAllCounters().getWsCounters().getWsS02813UpdateAsString());
		// COB_CODE: DISPLAY 'VOID PAYMENTS UPDATED   : ' WS-S02813-VOID-UPDATE.
		DisplayUtil.sysout.write("VOID PAYMENTS UPDATED   : ", ws.getWsAllCounters().getWsCounters().getWsS02813VoidUpdateAsString());
		// COB_CODE: DISPLAY 'ITS HOME Y1 EOBS        : ' WS-ITS-Y1-CNT.
		DisplayUtil.sysout.write("ITS HOME Y1 EOBS        : ", ws.getWsAllCounters().getWsCounters().getWsItsY1CntAsString());
		// COB_CODE: DISPLAY '2814 PMNT EVENT INSERTED: ' WS-S02814-INSERTED.
		DisplayUtil.sysout.write("2814 PMNT EVENT INSERTED: ", ws.getWsAllCounters().getWsCounters().getWsS02814InsertedAsString());
		// COB_CODE: DISPLAY '2814 MTH/QTRLY INSERTED : ' WS-S02814-MTH-QTR.
		DisplayUtil.sysout.write("2814 MTH/QTRLY INSERTED : ", ws.getWsAllCounters().getWsCounters().getWsS02814MthQtrAsString());
		// COB_CODE: DISPLAY '2801 HCC CATG INSERTED  : ' WS-S02801-INSERTED.
		DisplayUtil.sysout.write("2801 HCC CATG INSERTED  : ", ws.getWsAllCounters().getWsCounters().getWsS02801InsertedAsString());
		// COB_CODE: DISPLAY 'EOB MONTHLY/QUARTELY    : ' WS-MTH-QTR.
		DisplayUtil.sysout.write("EOB MONTHLY/QUARTELY    : ", ws.getWsAllCounters().getWsCounters().getWsMthQtrAsString());
		// COB_CODE: DISPLAY 'REG LINES OUT COUNTER   : '  WS-LINES-REG-OUT.
		DisplayUtil.sysout.write("REG LINES OUT COUNTER   : ", ws.getWsAllCounters().getWsCounters().getWsLinesRegOutAsString());
		// COB_CODE: DISPLAY 'REG EOB COUNTER         : '   WS-EOB-REG-OUT.
		DisplayUtil.sysout.write("REG EOB COUNTER         : ", ws.getWsAllCounters().getWsCounters().getWsEobRegOutAsString());
		// COB_CODE: DISPLAY 'REG EXPENSE OUT  COUNT  : ' REG-EXPENSE-COUNT.
		DisplayUtil.sysout.write("REG EXPENSE OUT  COUNT  : ", ws.getWsAllCounters().getRegExpenseCountAsString());
		// COB_CODE: DISPLAY 'KSS EXPENSE OUT  COUNT  : ' KSS-EXPENSE-COUNT.
		DisplayUtil.sysout.write("KSS EXPENSE OUT  COUNT  : ", ws.getWsAllCounters().getKssExpenseCountAsString());
		// COB_CODE: DISPLAY 'REG WRITE-OFF OUT COUNT : ' REG-WRITEOFF-CNTR.
		DisplayUtil.sysout.write("REG WRITE-OFF OUT COUNT : ", ws.getWsAllCounters().getRegWriteoffCntrAsString());
		// COB_CODE: DISPLAY 'INTEREST FILE OUT COUNT : ' OUT-INTEREST-CNTR.
		DisplayUtil.sysout.write("INTEREST FILE OUT COUNT : ", ws.getWsAllCounters().getOutInterestCntrAsString());
		// COB_CODE: DISPLAY 'KSS INT  FILE OUT COUNT : ' OUT-KSS-INT-CNTR.
		DisplayUtil.sysout.write("KSS INT  FILE OUT COUNT : ", ws.getWsAllCounters().getOutKssIntCntrAsString());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '********PROGRAM CAN NOW BEGIN RESTART***'.
		DisplayUtil.sysout.write("********PROGRAM CAN NOW BEGIN RESTART***");
	}

	/**Original name: 0900-LOAD-TRIGGER<br>
	 * <pre>                                                                *
	 *                                                                 *
	 *     DISPLAY '** 0900-LOAD-TRIGGER ** '.
	 *                                                                 *</pre>*/
	private void loadTrigger() {
		// COB_CODE: MOVE TRG-CLM-PMT-ID          TO WS-TRG-PMT-ID.
		ws.getWsNf0533WorkArea().setWsTrgPmtId(triggerInTO.getTriggerRecordLayout().getKey().getPmtId());
		//    DISPLAY 'TRG-CLM-PMT-ID:  ' TRG-CLM-PMT-ID.
		// COB_CODE: PERFORM 9110-INSERT-TEMP     THRU 9110-EXIT.
		insertTemp();
		// COB_CODE: PERFORM 8100-READ-TRIGGER-IN THRU 8100-EXIT.
		readTriggerIn();
	}

	/**Original name: 1000-PROCESS-TRIGGER<br>
	 * <pre>                                                                *
	 *     DISPLAY '** 1000-PROCESS-TRIGGER ** '.
	 *                                                                 *</pre>*/
	private void processTrigger() {
		// COB_CODE: MOVE 'YES' TO FIRST-TIME-CHECK.
		ws.getWsSwitches().setFirstTimeCheck("YES");
		// COB_CODE: PERFORM 9300-FETCH-EOB   THRU 9300-EXIT.
		fetchEob();
		// COB_CODE: MOVE DB2-CLM-CNTL-ID     TO NEW-CLAIM-ID.
		ws.getWsNf0533WorkArea().getNewKey().setClaimId(ws.getEobDb2CursorArea().getS02801Info().getClmCntlId());
		// COB_CODE: MOVE DB2-CLM-CNTL-SFX-ID TO NEW-CLAIM-SFX.
		ws.getWsNf0533WorkArea().getNewKey().setClaimSfx(ws.getEobDb2CursorArea().getS02801Info().getClmCntlSfxId());
		// COB_CODE: MOVE DB2-CLM-PMT-ID      TO NEW-PMT-ID.
		ws.getWsNf0533WorkArea().getNewKey().setPmtId(TruncAbs.toShort(ws.getEobDb2CursorArea().getS02801Info().getClmPmtId(), 2));
		// COB_CODE: MOVE NEW-KEY                 TO HOLD-KEY.
		ws.getWsNf0533WorkArea().getHoldKey().setHoldKeyBytes(ws.getWsNf0533WorkArea().getNewKey().getNewKeyBytes());
		// COB_CODE: MOVE DB2-PMT-KCAPS-TEAM-NM   TO HOLD-KCAPS-TEAM-NM.
		ws.getWsNf0533WorkArea().setHoldKcapsTeamNm(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsTeamNm());
		// COB_CODE: MOVE DB2-PMT-KCAPS-USE-ID    TO HOLD-KCAPS-USE-ID.
		ws.getWsNf0533WorkArea().setHoldKcapsUseId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsUseId());
		// COB_CODE: MOVE DB2-PMT-ASG-CD          TO HOLD-ASG-CD.
		ws.getWsNf0533WorkArea().setHoldAsgCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd());
		// COB_CODE: MOVE DB2-PMT-HIST-LOAD-CD    TO HOLD-HIST-LOAD-CD.
		ws.getWsNf0533WorkArea().setHoldHistLoadCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd());
		// COB_CODE: MOVE DB2-PMT-ITS-CLM-TYP-CD  TO HOLD-ITS-CLM-TYP-CD.
		ws.getWsNf0533WorkArea().setHoldItsClmTypCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd());
		// COB_CODE: MOVE DB2-PMT-PGM-AREA-CD     TO HOLD-PGM-AREA-CD.
		ws.getWsNf0533WorkArea().setHoldPgmAreaCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPgmAreaCd());
		// COB_CODE: MOVE DB2-PMT-ENR-CL-CD       TO HOLD-ENR-CL-CD.
		ws.getWsNf0533WorkArea().setHoldEnrClCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtEnrClCd());
		// COB_CODE: MOVE DB2-PMT-FIN-CD          TO HOLD-FIN-CD.
		ws.getWsNf0533WorkArea().setHoldFinCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtFinCd());
		// COB_CODE: MOVE DB2-PMT-INS-ID          TO HOLD-INS-ID.
		ws.getWsNf0533WorkArea().setHoldInsId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtInsId());
		// COB_CODE: MOVE DB2-PMT-ALT-INS-ID      TO HOLD-ALT-INS-ID.
		ws.getWsNf0533WorkArea().setHoldAltInsId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAltInsId());
		// COB_CODE: MOVE PATIENT-NAME            TO HOLD-PATIENT-NAME.
		ws.getWsNf0533WorkArea().setHoldPatientName(ws.getWsNf0533WorkArea().getPatientName());
		// COB_CODE: MOVE PERFORMING-NAME         TO HOLD-PERFORMING-NAME.
		ws.getWsNf0533WorkArea().setHoldPerformingName(ws.getWsNf0533WorkArea().getPerformingName());
		// COB_CODE: MOVE BILLING-NAME            TO HOLD-BILLING-NAME.
		ws.getWsNf0533WorkArea().setHoldBillingName(ws.getWsNf0533WorkArea().getBillingName());
		// COB_CODE: MOVE DB2-BI-PROV-ID          TO HOLD-BI-PROV-ID.
		ws.getWsNf0533WorkArea().setHoldBiProvId(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvId());
		// COB_CODE: MOVE DB2-BI-PROV-LOB         TO HOLD-BI-PROV-LOB.
		ws.getWsNf0533WorkArea().setHoldBiProvLob(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvLob());
		// COB_CODE: MOVE DB2-BI-PROV-ID          TO HOLD-PROVIDER-NUM.
		ws.getWsNf0533WorkArea().setHoldProviderNum(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvId());
		// COB_CODE: MOVE DB2-BI-PROV-LOB         TO HOLD-PROVIDER-LOB.
		ws.getWsNf0533WorkArea().setHoldProviderLobFormatted(String.valueOf(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvLob()));
		// COB_CODE: PERFORM 3700-GET-PROV-NPI THRU 3700-EXIT.
		getProvNpi();
		// COB_CODE: MOVE WS-DB2-BI-NPI-PROV-ID   TO HOLD-BI-PROV-NPI-NUM.
		ws.getWsNf0533WorkArea().setHoldBiProvNpiNum(ws.getWsNf0533WorkArea().getWsDb2BiNpiProvId());
		// COB_CODE: MOVE DB2-PMT-PMT-BK-PROD-CD  TO HOLD-BK-PROD-CD.
		ws.getWsNf0533WorkArea().setHoldBkProdCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPmtBkProdCd());
		// COB_CODE: MOVE DB2-PMT-TYP-GRP-CD      TO HOLD-TYP-GRP-CD.
		ws.getWsNf0533WorkArea().setHoldTypGrpCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd());
		// COB_CODE: MOVE DB2-PMT-NPI-CD          TO HOLD-NPI-CD.
		ws.getWsNf0533WorkArea().setHoldNpiCd(String.valueOf(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtNpiCd()));
		// COB_CODE: MOVE DB2-PMT-MEM-ID          TO HOLD-MEMBER-ID.
		ws.getWsNf0533WorkArea().setHoldMemberId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtMemId());
		// COB_CODE: MOVE DB2-CLM-SYST-VER-ID     TO HOLD-CLM-SYST-VER-ID.
		ws.getWsNf0533WorkArea().setHoldClmSystVerId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getClmSystVerId());
		// COB_CODE: MOVE DB2-PA-ID-TO-ACUM-ID    TO HOLD-PA-ID-TO-ACUM-ID.
		ws.getWsNf0533WorkArea().setHoldPaIdToAcumId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPaIdToAcumId());
		// COB_CODE: MOVE DB2-MEM-ID-TO-ACUM-ID   TO HOLD-MEM-ID-TO-ACUM-ID.
		ws.getWsNf0533WorkArea().setHoldMemIdToAcumId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getMemIdToAcumId());
		// COB_CODE: MOVE DB2-PROD-IND            TO HOLD-PROD-IND.
		ws.getWsNf0533WorkArea().setHoldProdInd(ws.getEobDb2CursorArea().getS02801Info().getProdInd());
		// COB_CODE: MOVE DB2-ACR-PRMT-PA-INT-AM  TO HOLD-ACR-PRMT-PA-INT-AM.
		ws.getWsNf0533WorkArea()
				.setHoldAcrPrmtPaIntAm(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getAcrPrmtPaIntAm(), 11, 2));
		// COB_CODE: MOVE DB2-CLM-INS-LN-CD       TO HOLD-CLM-INS-LN-CD.
		ws.getWsNf0533WorkArea().setHoldClmInsLnCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getClmInsLnCd());
		// COB_CODE: IF DB2-PMT-CLM-CORP-CODE = ' ' OR LOW-VALUES
		//              MOVE DB2-C1-CORP-CD TO HOLD-CLM-CORP-CODE
		//           ELSE
		//              MOVE DB2-PMT-CLM-CORP-CODE TO HOLD-CLM-CORP-CODE
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmCorpCode() == ' '
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmCorpCode(), Types.LOW_CHAR_VAL)) {
			// COB_CODE: MOVE DB2-C1-CORP-CD TO HOLD-CLM-CORP-CODE
			ws.getWsNf0533WorkArea().setHoldClmCorpCode(ws.getEobDb2CursorArea().getS02811Calc1Info().getCorpCd());
		} else {
			// COB_CODE: MOVE DB2-PMT-CLM-CORP-CODE TO HOLD-CLM-CORP-CODE
			ws.getWsNf0533WorkArea().setHoldClmCorpCode(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmCorpCode());
		}
		// COB_CODE: MOVE EOB-DB2-CURSOR-AREA TO EOB-HLD-CURSOR-AREA.
		ws.setEobHldCursorArea(ws.getEobDb2CursorArea().getEobDb2CursorAreaFormatted());
		// COB_CODE: MOVE 'NO ' TO NF0738-WRAP-SW.
		ws.getWsSwitches().setNf0738WrapSw("NO ");
		// COB_CODE: IF DB2-PMT-ADJD-PROV-STAT-CD = 'N'
		//           AND DB2-LI-THR-SERV-DT <= HOLD-COVID-END-DATE
		//                  THRU 2950-EXIT
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjdProvStatCd() == 'N'
				&& Conditions.lte(ws.getEobDb2CursorArea().getS02809LineInfo().getThrServDt(), ws.getWsSwitches().getHoldCovidEndDate())) {
			// COB_CODE: PERFORM 2950-COVID19-PROCCD-EXISTENCE
			//              THRU 2950-EXIT
			covid19ProccdExistence();
		}
		// COB_CODE: PERFORM 2200-PROCESS-LINES THRU 2200-EXIT
		//               UNTIL END-OF-CURSOR.
		while (!ws.getWsSwitches().isEndOfCursor()) {
			processLines();
		}
		// COB_CODE: MOVE 'YES' TO LAST-TIME-CHECK.
		ws.getWsSwitches().setLastTimeCheck("YES");
		// COB_CODE: PERFORM 2900-END-OF-CLAIM THRU 2900-EXIT.
		endOfClaim();
		// COB_CODE: PERFORM 9990-CLOSE-EOB-CURSOR THRU 9990-EXIT.
		closeEobCursor();
	}

	/**Original name: 1750-CHECK-FOR-SRS-PROV<br>
	 * <pre>    DISPLAY '** 1750-CHECK-FOR-SRS-PROV **'.</pre>*/
	private void checkForSrsProv() {
		// COB_CODE: MOVE 'N' TO WS-PRV-STOP-PAY-FOUND.
		ws.getWsPrvStopPayVariables().setWsPrvStopPayFoundFormatted("N");
		// COB_CODE: MOVE 'N' TO WS-PRV-STOP-PAY-FINISHED.
		ws.getWsPrvStopPayVariables().setWsPrvStopPayFinishedFormatted("N");
		// COB_CODE: MOVE 1 TO WS-SUB-PRV-STOP-PAY.
		ws.getWsPrvStopPayVariables().setWsSubPrvStopPay(((short) 1));
		// COB_CODE: MOVE 'NO ' TO SRS-PROVIDER-SW.
		ws.getWsSwitches().setSrsProviderSw("NO ");
		// COB_CODE: PERFORM 3800-CK-FOR-SRS-PROVIDER THRU 3800-EXIT
		//               VARYING WS-SUB-PRV-STOP-PAY
		//               FROM 1 BY 1
		//               UNTIL WS-SUB-PRV-STOP-PAY >
		//                     WS-PRV-STOP-PAY-MAX
		//                  OR WS-PRV-STOP-PAY-FINISHED = 'Y'
		//                  OR WS-PRV-STOP-PAY-FOUND = 'Y'.
		ws.getWsPrvStopPayVariables().setWsSubPrvStopPay(((short) 1));
		while (!(ws.getWsPrvStopPayVariables().getWsSubPrvStopPay() > ws.getWsPrvStopPayVariables().getWsPrvStopPayMax()
				|| ws.getWsPrvStopPayVariables().getWsPrvStopPayFinished() == 'Y' || ws.getWsPrvStopPayVariables().getWsPrvStopPayFound() == 'Y')) {
			ckForSrsProvider();
			ws.getWsPrvStopPayVariables().setWsSubPrvStopPay(Trunc.toShort(ws.getWsPrvStopPayVariables().getWsSubPrvStopPay() + 1, 4));
		}
		// COB_CODE: IF WS-PRV-STOP-PAY-FOUND = 'Y'
		//               ADD 1 TO SRS-PRV-MATCH-CNTR
		//           END-IF.
		if (ws.getWsPrvStopPayVariables().getWsPrvStopPayFound() == 'Y') {
			// COB_CODE: ADD 1 TO SRS-PRV-MATCH-CNTR
			ws.getWsAllCounters().getWsCounters().setSrsPrvMatchCntr(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getSrsPrvMatchCntr(), 7));
		}
	}

	/**Original name: 2000-DAILY-RUN-PROCESS<br>
	 * <pre>                                                                *
	 *     DISPLAY '** 2000-DAILY-RUN-PROCESS **'.
	 *                                                                 *</pre>*/
	private void dailyRunProcess() {
		// COB_CODE: MOVE EOB-DB2-CURSOR-AREA TO EOB-HLD-CURSOR-AREA.
		ws.setEobHldCursorArea(ws.getEobDb2CursorArea().getEobDb2CursorAreaFormatted());
		// COB_CODE: IF DB2-LI-EXMN-ACTN-CD = 'D'
		//               CONTINUE
		//           ELSE
		//               MOVE 'NO ' TO ALL-DENIED-SW
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() == 'D') {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: MOVE 'NO ' TO ALL-DENIED-SW
			ws.getWsSwitches().setAllDeniedSw("NO ");
		}
		// COB_CODE: IF DB2-LI-RVW-BY-CD = ' ZS'
		//               PERFORM 2010-SRS-CHECK THRU 2010-EXIT
		//           ELSE
		//               PERFORM 4000-ADD-LINE-CALC-AMTS THRU 4000-EXIT
		//           END-IF.
		if (Conditions.eq(String.valueOf(ws.getEobDb2CursorArea().getS02809LineInfo().getRvwByCd()), " ZS")) {
			// COB_CODE: PERFORM 2010-SRS-CHECK THRU 2010-EXIT
			srsCheck();
		} else {
			// COB_CODE: PERFORM 4000-ADD-LINE-CALC-AMTS THRU 4000-EXIT
			addLineCalcAmts();
		}
		// COB_CODE: IF DB2-PMT-ASG-CD = 'Y1'
		//              END-IF
		//           ELSE
		//              PERFORM 3201-PROMPT-PAY-LINE-AMT THRU 3201-EXIT
		//           END-IF.
		if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd(), "Y1")) {
			// COB_CODE: IF DB2-VBR-IN = 'Y'
			//              MOVE 'Y' TO HOLD-VBR-IN
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getVbrIn() == 'Y') {
				// COB_CODE: MOVE 'Y' TO HOLD-VBR-IN
				ws.getWsNf0533WorkArea().setHoldVbrInFormatted("Y");
			}
		} else {
			// COB_CODE: PERFORM 3201-PROMPT-PAY-LINE-AMT THRU 3201-EXIT
			promptPayLineAmt();
		}
		// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
		//               END-IF
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
			// COB_CODE: IF WS-LINE-ACCUM-WRITEOFF < 0
			//           AND DB2-LI-ALW-CHG-AM     > DB2-LI-CHG-AM
			//               MOVE 0 TO WS-LINE-ACCUM-WRITEOFF
			//           END-IF
			if (ws.getWsRollLogicWork().getLineAccumulators().getWriteoff().compareTo(0) < 0 && ws.getEobDb2CursorArea().getS02809LineInfo()
					.getAlwChgAm().compareTo(ws.getEobDb2CursorArea().getS02809LineInfo().getChgAm()) > 0) {
				// COB_CODE: MOVE 0 TO WS-LINE-ACCUM-WRITEOFF
				ws.getWsRollLogicWork().getLineAccumulators().setWriteoff(Trunc.toDecimal(0, 11, 2));
			}
		}
		// COB_CODE: IF DB2-PMT-LOB-CD = '1' OR '7'
		//               END-IF
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtLobCd() == '1'
				|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtLobCd() == '7') {
			// COB_CODE: IF (DB2-LI-TS-CD = '1C' OR '2C' OR '3C' OR '7C' OR '8C')
			//           AND DB2-PMT-OI-CD = 'L'
			//           AND (DB2-LI-EXMN-ACTN-CD = 'D')
			//           AND (DB2-LI-RMRK-CD  =  '  ' OR '40')
			//               MOVE SPACES     TO DB2-LI-RMRK-CD
			//           END-IF
			if ((Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getTsCd(), "1C")
					|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getTsCd(), "2C")
					|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getTsCd(), "3C")
					|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getTsCd(), "7C")
					|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getTsCd(), "8C"))
					&& ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtOiCd() == 'L'
					&& ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() == 'D'
					&& (Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getRmrkCd(), "  ")
							|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getRmrkCd(), "40"))) {
				// COB_CODE: MOVE MANUAL-PAY TO DB2-LI-EXMN-ACTN-CD
				ws.getEobDb2CursorArea().getS02809LineInfo().setExmnActnCd(ws.getWorkFields().getManualPay());
				// COB_CODE: MOVE SPACES     TO DB2-LI-RMRK-CD
				ws.getEobDb2CursorArea().getS02809LineInfo().setRmrkCd("");
			}
		}
		// COB_CODE: IF  PERFORMING-NAME  >  SPACES
		//               END-IF
		//           ELSE
		//               PERFORM 2020-PROVIDER-STUFF THRU 2020-EXIT
		//           END-IF.
		if (Characters.GT_SPACE.test(ws.getWsNf0533WorkArea().getPerformingName())) {
			// COB_CODE: IF  BILLING-NAME  >  SPACES
			//               CONTINUE
			//           ELSE
			//               PERFORM 2020-PROVIDER-STUFF THRU 2020-EXIT
			//           END-IF
			if (Characters.GT_SPACE.test(ws.getWsNf0533WorkArea().getBillingName())) {
				// COB_CODE: CONTINUE
				//continue
			} else {
				// COB_CODE: PERFORM 2020-PROVIDER-STUFF THRU 2020-EXIT
				providerStuff();
			}
		} else {
			// COB_CODE: PERFORM 2020-PROVIDER-STUFF THRU 2020-EXIT
			providerStuff();
		}
		//****PERFORM 2020-PROVIDER-STUFF THRU 2020-EXIT.
		// COB_CODE: IF DB2-PMT-ASG-CD = 'Y1'
		//           OR WS-LINE-ACCUM-WRITEOFF = 0
		//           OR DB2-PMT-HIST-LOAD-CD    = 'D'
		//           OR DB2-PMT-ITS-CLM-TYP-CD  = 'M'
		//               CONTINUE
		//           ELSE
		//               PERFORM 8700-WRITE-PROV-WO-FILE THRU 8700-EXIT
		//           END-IF.
		if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd(), "Y1")
				|| ws.getWsRollLogicWork().getLineAccumulators().getWriteoff().compareTo(0) == 0
				|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd() == 'D'
				|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: PERFORM 3700-GET-PROV-NPI THRU 3700-EXIT
			getProvNpi();
			// COB_CODE: PERFORM 8700-WRITE-PROV-WO-FILE THRU 8700-EXIT
			writeProvWoFile();
		}
		// COB_CODE: PERFORM 6000-BUILD-HEADER-SECTION THRU 6000-EXIT.
		buildHeaderSection();
		// COB_CODE: PERFORM 6100-BUILD-LINE-SECTION  THRU 6100-EXIT.
		buildLineSection();
		// COB_CODE: IF WRAP-NF0738
		//              PERFORM 8950-WRITE-MTH-QTR THRU 8950-EXIT
		//           ELSE
		//              PERFORM 8800-WRITE-EOB-RECORD    THRU 8800-EXIT
		//           END-IF.
		if (ws.getWsSwitches().isWrapNf0738()) {
			// COB_CODE: MOVE WS-MT-QT-INDICATOR TO NF05-MT-QT-INDICATOR
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05MtQtIndicator(ws.getWsNf0533WorkArea().getWsMtQtIndicator());
			// COB_CODE: PERFORM 8950-WRITE-MTH-QTR THRU 8950-EXIT
			writeMthQtr();
		} else {
			// COB_CODE: PERFORM 8800-WRITE-EOB-RECORD    THRU 8800-EXIT
			writeEobRecord();
		}
		// COB_CODE: INITIALIZE NF05-LINE-LEVEL-RECORD.
		initNf05LineLevelRecord();
		// COB_CODE: INITIALIZE NF05-TOTAL-RECORD.
		initNf05TotalRecord();
	}

	/**Original name: 2010-SRS-CHECK<br>
	 * <pre>                                                             *
	 *     DISPLAY '** 2010-SRS-CHECK **'.
	 *                                                              *
	 *                                                              *
	 *  10/01/00   PROJ 25191 SRS EXCESS PAYMENTS. ADDED SRS CHECK
	 *             TO CHECK THE REVIEW CODE FOR ' ZS'.
	 *            *IF YES THEN IT MOVES 0 TO ALLOWED, NOT-COVERED
	 *             PVDR W/O, PAT RESP., AND THE EXPL CODE. C298
	 *                                                              *</pre>*/
	private void srsCheck() {
		// COB_CODE: IF DB2-LI-RVW-BY-CD = ' ZS'
		//                MOVE SPACES TO DB2-LI-EXPLN-CD
		//           END-IF.
		if (Conditions.eq(String.valueOf(ws.getEobDb2CursorArea().getS02809LineInfo().getRvwByCd()), " ZS")) {
			// COB_CODE: MOVE 0 TO DB2-LI-ALW-CHG-AM
			//                     WS-LINE-ACCUM-NOTCOVD
			//                     WS-LINE-ACCUM-WRITEOFF
			//                     WS-LINE-ACCUM-PAT-OWES
			//                     WS-LINE-ACCUM-DEDUCTIBLE
			//                     WS-LINE-ACCUM-COINSURANCE
			//                     WS-LINE-ACCUM-COPAY
			//                     DB2-CAS-EOB-DED-AM
			//                     DB2-CAS-EOB-COINS-AM
			//                     DB2-CAS-EOB-COPAY-AM
			ws.getEobDb2CursorArea().getS02809LineInfo().setAlwChgAm(Trunc.toDecimal(0, 11, 2));
			ws.getWsRollLogicWork().getLineAccumulators().setNotcovd(Trunc.toDecimal(0, 11, 2));
			ws.getWsRollLogicWork().getLineAccumulators().setWriteoff(Trunc.toDecimal(0, 11, 2));
			ws.getWsRollLogicWork().getLineAccumulators().setPatOwes(Trunc.toDecimal(0, 11, 2));
			ws.getWsRollLogicWork().getLineAccumulators().setDeductible(Trunc.toDecimal(0, 11, 2));
			ws.getWsRollLogicWork().getLineAccumulators().setCoinsurance(Trunc.toDecimal(0, 11, 2));
			ws.getWsRollLogicWork().getLineAccumulators().setCopay(Trunc.toDecimal(0, 7, 2));
			ws.getEobDb2CursorArea().getCasCalc1Info().setEobDedAm(Trunc.toDecimal(0, 15, 2));
			ws.getEobDb2CursorArea().getCasCalc1Info().setEobCoinsAm(Trunc.toDecimal(0, 15, 2));
			ws.getEobDb2CursorArea().getCasCalc1Info().setEobCopayAm(Trunc.toDecimal(0, 15, 2));
			// COB_CODE: MOVE SPACES TO DB2-LI-EXPLN-CD
			ws.getEobDb2CursorArea().getS02809LineInfo().setExplnCd(Types.SPACE_CHAR);
		}
	}

	/**Original name: 2020-PROVIDER-STUFF<br>
	 * <pre>                                                             *
	 *     DISPLAY '** 2020-PROVIDER-STUFF **'.
	 *                                                              *</pre>*/
	private void providerStuff() {
		// COB_CODE: IF DB2-BI-PROV-ID  = HOLD-BI-PROVIDER-NUM
		//               CONTINUE
		//           ELSE
		//               MOVE HOLD-RA-ADDR3-ZIP     TO HOLD-BI-ZIP
		//           END-IF.
		if (Conditions.eq(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvId(), ws.getWsNf0533WorkArea().getHoldBiProviderNum())) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: MOVE DB2-BI-PROV-ID        TO HOLD-PROVIDER-NUM
			ws.getWsNf0533WorkArea().setHoldProviderNum(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvId());
			// COB_CODE: MOVE DB2-BI-PROV-LOB       TO HOLD-PROVIDER-LOB
			ws.getWsNf0533WorkArea().setHoldProviderLobFormatted(String.valueOf(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvLob()));
			// COB_CODE: PERFORM 3700-GET-PROV-NPI THRU 3700-EXIT
			getProvNpi();
			// COB_CODE: MOVE WS-DB2-BI-NPI-PROV-ID TO HOLD-BI-PROV-NPI-NUM
			ws.getWsNf0533WorkArea().setHoldBiProvNpiNum(ws.getWsNf0533WorkArea().getWsDb2BiNpiProvId());
			// COB_CODE: PERFORM 7200-CALL-NU0201ML THRU 7200-EXIT
			callNu0201ml();
			// COB_CODE: MOVE HOLD-PROVIDER-NUM     TO HOLD-BI-PROVIDER-NUM
			ws.getWsNf0533WorkArea().setHoldBiProviderNum(ws.getWsNf0533WorkArea().getHoldProviderNum());
			// COB_CODE: MOVE HOLD-PROVIDER-LOB     TO HOLD-BI-PROVIDER-LOB
			ws.getWsNf0533WorkArea().setHoldBiProviderLobFormatted(ws.getWsNf0533WorkArea().getHoldProviderLobFormatted());
			// COB_CODE: MOVE HOLD-PROVIDER-NAME    TO HOLD-BI-PROVIDER-NAME
			ws.getWsNf0533WorkArea().setHoldBiProviderName(ws.getWsNf0533WorkArea().getHoldProviderName());
			// COB_CODE: MOVE HOLD-RA-ADDRESS       TO HOLD-BI-ADDRESS
			ws.getWsNf0533WorkArea().getHoldProviderAdressStuff()
					.setBiAddress(ws.getWsNf0533WorkArea().getHoldRaAddress().getHoldRaAddressFormatted());
			// COB_CODE: MOVE HOLD-RA-ADDR3-ZIP     TO HOLD-BI-ZIP
			ws.getWsNf0533WorkArea().getHoldProviderAdressStuff().setBiZip(ws.getWsNf0533WorkArea().getHoldRaAddress().getAddr3Zip());
		}
		//*
		//* THE FOLLOWING CALL TO THE PROVIDER DATABASE IS TO GET
		//* THE PERFORMING PROVIDER NAME TO BE PRINTED ON THE EOB.
		//* ONLY IF THE BILLING AND PROVIDER NUMBERS ARE DIFFERENT WE
		//* WILL NEED TO GET THE PERFORMING PROVIDER NAME.
		//*
		//* WITH PROJECT 30554 - STARTED PRINTING BILLING AND PERFORMING
		//* PROVIDER EVEN IF THE NAMES ARE THE SAME.
		//*
		// COB_CODE: MOVE SPACES TO HOLD-PROVIDER-NAME.
		ws.getWsNf0533WorkArea().setHoldProviderName("");
		// COB_CODE: IF HOLD-BI-PROVIDER-NUM  = DB2-PE-PROV-ID
		//               MOVE HOLD-BI-PROVIDER-NAME TO HOLD-PE-PROVIDER-NAME
		//           ELSE
		//               PERFORM 7250-CALL-NU0201ML   THRU 7250-EXIT
		//           END-IF.
		if (Conditions.eq(ws.getWsNf0533WorkArea().getHoldBiProviderNum(), ws.getEobDb2CursorArea().getS02815ProviderInfo().getPeProvId())) {
			// COB_CODE: MOVE HOLD-BI-PROVIDER-NAME TO HOLD-PE-PROVIDER-NAME
			ws.getWsNf0533WorkArea().setHoldPeProviderName(ws.getWsNf0533WorkArea().getHoldBiProviderName());
		} else {
			// COB_CODE: MOVE DB2-PE-PROV-ID          TO HOLD-PROVIDER-NUM
			ws.getWsNf0533WorkArea().setHoldProviderNum(ws.getEobDb2CursorArea().getS02815ProviderInfo().getPeProvId());
			// COB_CODE: MOVE DB2-BI-PROV-LOB         TO HOLD-PROVIDER-LOB
			ws.getWsNf0533WorkArea().setHoldProviderLobFormatted(String.valueOf(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvLob()));
			// COB_CODE: PERFORM 3700-GET-PROV-NPI THRU 3700-EXIT
			getProvNpi();
			// COB_CODE: MOVE WS-DB2-BI-NPI-PROV-ID TO HOLD-BI-PROV-NPI-NUM
			ws.getWsNf0533WorkArea().setHoldBiProvNpiNum(ws.getWsNf0533WorkArea().getWsDb2BiNpiProvId());
			// COB_CODE: PERFORM 7250-CALL-NU0201ML   THRU 7250-EXIT
			callNu0201ml1();
		}
	}

	/**Original name: 2200-PROCESS-LINES<br>
	 * <pre>                                                             *
	 *     DISPLAY '** 2200-PROCESS-LINES **'.
	 *                                                              *</pre>*/
	private void processLines() {
		// COB_CODE: IF FIRST-TIME-CHECK = 'YES'
		//              MOVE 'NO' TO FIRST-TIME-CHECK
		//           END-IF.
		if (Conditions.eq(ws.getWsSwitches().getFirstTimeCheck(), "YES")) {
			// COB_CODE: IF DB2-PMT-ASG-CD = 'Y1'
			//              END-IF
			//           ELSE
			//              PERFORM 3200-PROMPT-PAY-INTEREST THRU 3200-EXIT
			//           END-IF
			if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd(), "Y1")) {
				// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
				//              CONTINUE
				//           ELSE
				//              PERFORM 2300-NEW-NF0738-CLAIM THRU 2300-EXIT
				//           END-IF
				if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
					// COB_CODE: CONTINUE
					//continue
				} else {
					// COB_CODE: PERFORM 2300-NEW-NF0738-CLAIM THRU 2300-EXIT
					newNf0738Claim();
				}
				// COB_CODE: IF DB2-VBR-IN = 'Y'
				//              MOVE 'Y' TO HOLD-VBR-IN
				//           END-IF
				if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getVbrIn() == 'Y') {
					// COB_CODE: MOVE 'Y' TO HOLD-VBR-IN
					ws.getWsNf0533WorkArea().setHoldVbrInFormatted("Y");
				}
			} else {
				// COB_CODE: PERFORM 7000-CALL-NF0531SR THRU 7000-EXIT
				callNf0531sr();
				// COB_CODE: PERFORM 3200-PROMPT-PAY-INTEREST THRU 3200-EXIT
				promptPayInterest();
			}
			// COB_CODE: MOVE 'NO' TO FIRST-TIME-CHECK
			ws.getWsSwitches().setFirstTimeCheck("NO");
		}
		// COB_CODE: IF HOLD-KEY = NEW-KEY
		//               CONTINUE
		//           ELSE
		//               PERFORM 2900-END-OF-CLAIM THRU 2900-EXIT
		//           END-IF.
		if (Conditions.eq(ws.getWsNf0533WorkArea().getHoldKey().getHoldKeyBytes(), ws.getWsNf0533WorkArea().getNewKey().getNewKeyBytes())) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: PERFORM 2900-END-OF-CLAIM THRU 2900-EXIT
			endOfClaim();
		}
		// COB_CODE: MOVE DB2-CLM-CNTL-ID     TO WS-CLAIM-ID.
		ws.getWsClaimId().setWsClaimIdFormatted(ws.getEobDb2CursorArea().getS02801Info().getClmCntlIdFormatted());
		//****         BYPASS EOBS FOR NO ACCUMULATOR CLAIMS PROCESSED
		//****                         BY THE CASHT TEAM
		// COB_CODE: IF DB2-PMT-HIST-LOAD-CD = 'A' AND
		//              DB2-PMT-KCAPS-TEAM-NM = 'CASHT'
		//                  MOVE 'Y' TO BYPASS-SW
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd() == 'A'
				&& Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsTeamNm(), "CASHT")) {
			// COB_CODE: MOVE 'Y' TO BYPASS-SW
			ws.getWsSwitches().setBypassSwFormatted("Y");
		}
		//****         BYPASS EOBS FOR MEDICARE LOAD
		// COB_CODE: IF DB2-PMT-HIST-LOAD-CD = 'M' AND
		//              DB2-CLM-PMT-ID = 1
		//                  MOVE 'Y' TO BYPASS-SW
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd() == 'M'
				&& ws.getEobDb2CursorArea().getS02801Info().getClmPmtId() == 1) {
			// COB_CODE: MOVE 'Y' TO BYPASS-SW
			ws.getWsSwitches().setBypassSwFormatted("Y");
		}
		//****         BYPASS EOBS FOR FEP
		// COB_CODE: IF DB2-PMT-TYP-GRP-CD = '9  ' OR ' 9 ' OR '  9'
		//              OR DB2-PMT-NPI-CD = 'F'
		//                  MOVE 'Y' TO BYPASS-SW
		//           END-IF.
		if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), "9  ")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), " 9 ")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), "  9")
				|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtNpiCd() == 'F') {
			// COB_CODE: MOVE 'Y' TO BYPASS-SW
			ws.getWsSwitches().setBypassSwFormatted("Y");
		}
		//****         BYPASS EOBS FOR STATE OF KANSAS NEAR SITE
		// COB_CODE: IF DB2-PMT-HIST-LOAD-CD = 'K'
		//              MOVE 'Y' TO BYPASS-SW
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd() == 'K') {
			// COB_CODE: MOVE 'Y' TO BYPASS-SW
			ws.getWsSwitches().setBypassSwFormatted("Y");
		}
		//****         BYPASS EOBS WITH SELECTED OVERRIDESS
		// COB_CODE: IF (DB2-LI-ADJD-OVRD1-CD  = 'Z8' OR
		//               DB2-LI-ADJD-OVRD2-CD  = 'Z8' OR
		//               DB2-LI-ADJD-OVRD3-CD  = 'Z8' OR
		//               DB2-LI-ADJD-OVRD4-CD  = 'Z8' OR
		//               DB2-LI-ADJD-OVRD5-CD  = 'Z8' OR
		//               DB2-LI-ADJD-OVRD6-CD  = 'Z8' OR
		//               DB2-LI-ADJD-OVRD7-CD  = 'Z8' OR
		//               DB2-LI-ADJD-OVRD8-CD  = 'Z8' OR
		//               DB2-LI-ADJD-OVRD9-CD  = 'Z8' OR
		//               DB2-LI-ADJD-OVRD10-CD = 'Z8') OR
		//              (DB2-PMT-HIST-LOAD-CD = 'A' AND
		//               DB2-CLM-PMT-ID = 1) OR
		//              (DB2-PMT-ADJ-TYP-CD > ' ' AND
		//               DB2-CLM-PMT-ID = 1)
		//               END-IF
		//           END-IF.
		if (Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd1Cd(), "Z8")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd2Cd(), "Z8")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd3Cd(), "Z8")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd4Cd(), "Z8")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd5Cd(), "Z8")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd6Cd(), "Z8")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd7Cd(), "Z8")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd8Cd(), "Z8")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd9Cd(), "Z8")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd10Cd(), "Z8")
				|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd() == 'A'
						&& ws.getEobDb2CursorArea().getS02801Info().getClmPmtId() == 1
				|| Conditions.gt(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd(), ' ')
						&& ws.getEobDb2CursorArea().getS02801Info().getClmPmtId() == 1) {
			// COB_CODE: MOVE 'N' TO EXPENSE-SW
			ws.getWsSwitches().setExpenseSwFormatted("N");
			// COB_CODE: MOVE 'Y' TO BYPASS-SW
			ws.getWsSwitches().setBypassSwFormatted("Y");
			// COB_CODE: IF (DB2-PMT-ADJ-TYP-CD > ' ' AND
			//               DB2-CLM-PMT-ID = 1)
			//              END-IF
			//           END-IF
			if (Conditions.gt(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd(), ' ')
					&& ws.getEobDb2CursorArea().getS02801Info().getClmPmtId() == 1) {
				// COB_CODE: IF ATTN-SYS-SW = 'Y'
				//              CONTINUE
				//           ELSE
				//              PERFORM 9995-PROCESS-ERROR THRU 9995-EXIT
				//           END-IF
				if (ws.getWsSwitches().getAttnSysSw() == 'Y') {
					// COB_CODE: CONTINUE
					//continue
				} else {
					// COB_CODE: MOVE 'Y' TO ATTN-SYS-SW
					ws.getWsSwitches().setAttnSysSwFormatted("Y");
					// COB_CODE: MOVE ORIG-IS-ADJ-MSG TO ERR-MSG-TEXT
					errFileTO.setErrMsgText(ws.getNf07ErrorMessages().getOrigIsAdjMsg());
					// COB_CODE: PERFORM 9995-PROCESS-ERROR THRU 9995-EXIT
					processError();
				}
			}
		}
		//****         BYPASS SUBROGATED CLAIMS
		// COB_CODE: IF DB2-PMT-OI-CD = 'S'
		//              MOVE 'Y' TO BYPASS-SW
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtOiCd() == 'S') {
			// COB_CODE: MOVE 'Y' TO BYPASS-SW
			ws.getWsSwitches().setBypassSwFormatted("Y");
		}
		//****         BYPASS CLAIMS WITH SELECTED EXAMINER ACTIONS
		// COB_CODE: IF BYPASS-SW = 'Y'
		//              CONTINUE
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getWsSwitches().getBypassSw() == 'Y') {
			// COB_CODE: CONTINUE
			//continue
		} else if (ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() == 'C') {
			// COB_CODE: IF DB2-LI-EXMN-ACTN-CD = 'C'
			//              MOVE 'Y' TO BYPASS-SW
			//           ELSE
			//           END-IF
			//           END-IF
			// COB_CODE: MOVE 'Y' TO BYPASS-SW
			ws.getWsSwitches().setBypassSwFormatted("Y");
		} else if (ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() == 'D'
				|| ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() == 'M'
				|| ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() == ' ') {
			// COB_CODE: IF DB2-LI-EXMN-ACTN-CD = 'D' OR 'M' OR ' '
			//              END-EVALUATE
			//           END-IF
			// COB_CODE: EVALUATE DB2-PMT-ADJ-TYP-CD
			//              WHEN ' '
			//                 END-IF
			//              WHEN 'A'
			//                 MOVE VOID-LI-DNL-RMRK-CD TO NF05-VOID-DNL-REMARK
			//           END-EVALUATE
			switch (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd()) {

			case ' ':// COB_CODE: IF (DB2-PMT-HIST-LOAD-CD = 'A' OR 'M') AND
				//              DB2-CLM-PMT-ID = 1
				//                 MOVE 'Y' TO BYPASS-SW
				//           END-IF
				if ((ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd() == 'A'
						|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd() == 'M')
						&& ws.getEobDb2CursorArea().getS02801Info().getClmPmtId() == 1) {
					// COB_CODE: MOVE 'Y' TO BYPASS-SW
					ws.getWsSwitches().setBypassSwFormatted("Y");
				}
				break;

			case 'A':// COB_CODE: MOVE DB2-CLI-ID TO WS-LINE-NUM
				ws.getWsExpWorkItems().setWsLineNum(ws.getEobDb2CursorArea().getS02801Info().getCliId());
				// COB_CODE: MOVE 'N' TO GET-VOID-FOR-EXP
				ws.getWsSwitches().setGetVoidForExpFormatted("N");
				// COB_CODE: PERFORM 3500-SELECT-VOID-DATA  THRU 3500-EXIT
				selectVoidData();
				// COB_CODE: COMPUTE NF05-VOID-LI-PD-AM =
				//                   VOID-C1-PD-AM + VOID-C2-PD-AM +
				//                   VOID-C3-PD-AM + VOID-C4-PD-AM +
				//                   VOID-C5-PD-AM + VOID-C6-PD-AM +
				//                   VOID-C7-PD-AM + VOID-C8-PD-AM +
				//                   VOID-C9-PD-AM
				//           END-COMPUTE
				ws.getNf05EobRecord().getNf05LineLevelRecord()
						.setVoidLiPdAm(Trunc.toDecimal(abs(ws.getVoidInformation().getVoidS02811Calc1Info().getC1PdAm()
								.add(ws.getVoidInformation().getVoidS02811Calc2Info().getC1PdAm())
								.add(ws.getVoidInformation().getVoidS02811Calc3Info().getC1PdAm())
								.add(ws.getVoidInformation().getVoidS02811Calc4Info().getC1PdAm())
								.add(ws.getVoidInformation().getVoidS02811Calc5Info().getC1PdAm())
								.add(ws.getVoidInformation().getVoidS02811Calc6Info().getC1PdAm())
								.add(ws.getVoidInformation().getVoidS02811Calc7Info().getC1PdAm())
								.add(ws.getVoidInformation().getVoidS02811Calc8Info().getC1PdAm())
								.add(ws.getVoidInformation().getVoidS02811Calc9Info().getC1PdAm())), 11, 2));
				// COB_CODE: MOVE VOID-LI-DNL-RMRK-CD TO NF05-VOID-DNL-REMARK
				ws.getNf05EobRecord().getNf05LineLevelRecord().setVoidDnlRemark(ws.getVoidInformation().getVoidS02809LineInfo().getLiDnlRmrkCd());
				break;

			default:
				break;
			}
		}
		//****         BYPASS CLAIMS WITH D EXAMINER AND NON-PRT RMKS
		// COB_CODE: IF BYPASS-SW = 'Y'
		//             CONTINUE
		//           ELSE
		//             END-IF
		//           END-IF.
		if (ws.getWsSwitches().getBypassSw() == 'Y') {
			// COB_CODE: CONTINUE
			//continue
		} else if (ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() == 'D') {
			// COB_CODE: IF DB2-LI-EXMN-ACTN-CD = 'D'
			//             END-IF
			//           END-IF
			// COB_CODE: MOVE 'N' TO WS-NON-PRT-RMKS-FOUND
			ws.getWsNonPrtRmksVariables().setNonPrtRmksFoundFormatted("N");
			// COB_CODE: MOVE 'N' TO WS-NON-PRT-RMKS-FINISHED
			ws.getWsNonPrtRmksVariables().setNonPrtRmksFinishedFormatted("N");
			// COB_CODE: MOVE DB2-LI-RMRK-CD TO WS-HOLD-REMARK-CODE
			ws.getWsNonPrtRmksVariables().setHoldRemarkCode(ws.getEobDb2CursorArea().getS02809LineInfo().getRmrkCd());
			// COB_CODE: MOVE DB2-PMT-ASG-CD TO WS-HOLD-ASSIGN-CODE
			ws.getWsNonPrtRmksVariables().setHoldAssignCode(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd());
			// COB_CODE: PERFORM 3600-CK-FOR-NON-PRT-RMKS THRU 3600-EXIT
			//           VARYING WS-SUB-NON-PRT-RMKS
			//           FROM 1 BY 1
			//           UNTIL WS-SUB-NON-PRT-RMKS >
			//                 WS-NON-PRT-RMKS-MAX
			//              OR WS-NON-PRT-RMKS-FINISHED = 'Y'
			ws.getWsNonPrtRmksVariables().setSubNonPrtRmks(((short) 1));
			while (!(ws.getWsNonPrtRmksVariables().getSubNonPrtRmks() > ws.getWsNonPrtRmksVariables().getNonPrtRmksMax()
					|| ws.getWsNonPrtRmksVariables().getNonPrtRmksFinished() == 'Y')) {
				ckForNonPrtRmks();
				ws.getWsNonPrtRmksVariables().setSubNonPrtRmks(Trunc.toShort(ws.getWsNonPrtRmksVariables().getSubNonPrtRmks() + 1, 4));
			}
			// COB_CODE: IF WS-NON-PRT-RMKS-FOUND = 'Y'
			//              MOVE 'Y' TO BYPASS-SW
			//           END-IF
			if (ws.getWsNonPrtRmksVariables().getNonPrtRmksFound() == 'Y') {
				// COB_CODE: MOVE 'Y' TO BYPASS-SW
				ws.getWsSwitches().setBypassSwFormatted("Y");
			}
		}
		//****         BYPASS CLAIMS WITH NO CHARGES
		// COB_CODE: IF BYPASS-SW = 'Y'
		//             CONTINUE
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getWsSwitches().getBypassSw() == 'Y') {
			// COB_CODE: CONTINUE
			//continue
		} else if (ws.getEobDb2CursorArea().getS02809LineInfo().getChgAm().compareTo(0) > 0
				|| ws.getEobDb2CursorArea().getS02809LineInfo().getChgAm().compareTo(0) == 0
						&& Conditions.eq(ws.getWsNf0533WorkArea().getHoldClmInsLnCd(), "HIP")) {
			// COB_CODE: IF ((DB2-LI-CHG-AM > 0) OR
			//                (DB2-LI-CHG-AM = 0 AND
			//                           HOLD-CLM-INS-LN-CD = 'HIP'))
			//              CONTINUE
			//           ELSE
			//              MOVE 'Y' TO BYPASS-SW
			//           END-IF
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: MOVE 'Y' TO BYPASS-SW
			ws.getWsSwitches().setBypassSwFormatted("Y");
		}
		// COB_CODE: IF DB2-C1-PD-AM > 0 OR
		//              DB2-C2-PD-AM > 0 OR
		//              DB2-C3-PD-AM > 0 OR
		//              DB2-C4-PD-AM > 0 OR
		//              DB2-C5-PD-AM > 0 OR
		//              DB2-C6-PD-AM > 0 OR
		//              DB2-C7-PD-AM > 0 OR
		//              DB2-C8-PD-AM > 0 OR
		//              DB2-C9-PD-AM > 0
		//           END-IF
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02811Calc1Info().getPdAm().compareTo(0) > 0
				|| ws.getEobDb2CursorArea().getS02811Calc2Info().getPdAm().compareTo(0) > 0
				|| ws.getEobDb2CursorArea().getS02811Calc3Info().getPdAm().compareTo(0) > 0
				|| ws.getEobDb2CursorArea().getS02811Calc4Info().getPdAm().compareTo(0) > 0
				|| ws.getEobDb2CursorArea().getS02811Calc5Info().getPdAm().compareTo(0) > 0
				|| ws.getEobDb2CursorArea().getS02811Calc6Info().getPdAm().compareTo(0) > 0
				|| ws.getEobDb2CursorArea().getS02811Calc7Info().getPdAm().compareTo(0) > 0
				|| ws.getEobDb2CursorArea().getS02811Calc8Info().getPdAm().compareTo(0) > 0
				|| ws.getEobDb2CursorArea().getS02811Calc9Info().getPdAm().compareTo(0) > 0) {
			// COB_CODE: IF EXPENSE-SW = 'Y' AND DB2-PMT-ASG-CD NOT = 'Y1'
			//              END-IF
			//           END-IF
			if (ws.getWsSwitches().getExpenseSw() == 'Y' && !Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd(), "Y1")) {
				// COB_CODE: MOVE DB2-BI-PROV-ID          TO HOLD-PROVIDER-NUM
				ws.getWsNf0533WorkArea().setHoldProviderNum(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvId());
				// COB_CODE: MOVE DB2-BI-PROV-LOB         TO HOLD-PROVIDER-LOB
				ws.getWsNf0533WorkArea().setHoldProviderLobFormatted(String.valueOf(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvLob()));
				// COB_CODE: PERFORM 3700-GET-PROV-NPI THRU 3700-EXIT
				getProvNpi();
				// COB_CODE: PERFORM 3100-CREATE-EXPENSE-RECS THRU 3100-EXIT
				createExpenseRecs();
				// COB_CODE: MOVE 'Y' TO HOLD-EXP-PRC-SW
				ws.getWsSwitches().setHoldExpPrcSwFormatted("Y");
				// COB_CODE: IF HOLD-CLAIM-ID-1 = 4
				//               CONTINUE
				//           ELSE
				//               END-IF
				//           END-IF
				if (ws.getWsNf0533WorkArea().getHoldKey().getClaimId1() == 4) {
					// COB_CODE: CONTINUE
					//continue
				} else if (ws.getEobDb2CursorArea().getS02811Calc1Info().getCalcExplnCd() == 'A'
						|| ws.getEobDb2CursorArea().getS02811Calc2Info().getCalcExplnCd() == 'A'
						|| ws.getEobDb2CursorArea().getS02811Calc3Info().getCalcExplnCd() == 'A'
						|| ws.getEobDb2CursorArea().getS02811Calc4Info().getCalcExplnCd() == 'A'
						|| ws.getEobDb2CursorArea().getS02811Calc5Info().getCalcExplnCd() == 'A'
						|| ws.getEobDb2CursorArea().getS02811Calc6Info().getCalcExplnCd() == 'A'
						|| ws.getEobDb2CursorArea().getS02811Calc7Info().getCalcExplnCd() == 'A'
						|| ws.getEobDb2CursorArea().getS02811Calc8Info().getCalcExplnCd() == 'A'
						|| ws.getEobDb2CursorArea().getS02811Calc9Info().getCalcExplnCd() == 'A'
						|| ws.getEobDb2CursorArea().getS02809LineInfo().getExplnCd() == 'A') {
					// COB_CODE: IF DB2-C1-CALC-EXPLN-CD = 'A'
					//           OR DB2-C2-CALC-EXPLN-CD = 'A'
					//           OR DB2-C3-CALC-EXPLN-CD = 'A'
					//           OR DB2-C4-CALC-EXPLN-CD = 'A'
					//           OR DB2-C5-CALC-EXPLN-CD = 'A'
					//           OR DB2-C6-CALC-EXPLN-CD = 'A'
					//           OR DB2-C7-CALC-EXPLN-CD = 'A'
					//           OR DB2-C8-CALC-EXPLN-CD = 'A'
					//           OR DB2-C9-CALC-EXPLN-CD = 'A'
					//           OR DB2-LI-EXPLN-CD      = 'A'
					//               CONTINUE
					//           ELSE
					//             END-IF
					//           END-IF
					// COB_CODE: CONTINUE
					//continue
				} else if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd() == 'L'
						&& ws.getEobDb2CursorArea().getS02801Info().getClmPmtId() == 1
						|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd() == 'B'
						|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd() == 'S'
						|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd() == 'R') {
					// COB_CODE: IF (DB2-PMT-HIST-LOAD-CD = 'L' AND
					//               DB2-CLM-PMT-ID = 1) OR
					//              (DB2-PMT-ADJ-TYP-CD = 'B' OR 'S' OR 'R')
					//                CONTINUE
					//           ELSE
					//             PERFORM 3300-CALCULATE-CHECKS THRU 3300-EXIT
					//           END-IF
					// COB_CODE: CONTINUE
					//continue
				} else {
					// COB_CODE: PERFORM 3300-CALCULATE-CHECKS THRU 3300-EXIT
					calculateChecks();
				}
			}
		}
		// COB_CODE: IF BYPASS-SW = 'N'
		//              MOVE 'Y' TO HOLD-LINES-PRC-SW
		//           END-IF.
		if (ws.getWsSwitches().getBypassSw() == 'N') {
			// COB_CODE: PERFORM 2000-DAILY-RUN-PROCESS THRU 2000-EXIT
			dailyRunProcess();
			// COB_CODE: MOVE 'Y' TO HOLD-LINES-PRC-SW
			ws.getWsSwitches().setHoldLinesPrcSwFormatted("Y");
		}
		// COB_CODE: IF ATTN-SYS-SW = 'N'
		//              MOVE 'N' TO BYPASS-SW
		//           END-IF.
		if (ws.getWsSwitches().getAttnSysSw() == 'N') {
			// COB_CODE: MOVE 'Y' TO EXPENSE-SW
			ws.getWsSwitches().setExpenseSwFormatted("Y");
			// COB_CODE: MOVE 'N' TO BYPASS-SW
			ws.getWsSwitches().setBypassSwFormatted("N");
		}
		// COB_CODE: PERFORM 9300-FETCH-EOB THRU 9300-EXIT.
		fetchEob();
		// COB_CODE: MOVE DB2-CLM-CNTL-ID     TO NEW-CLAIM-ID.
		ws.getWsNf0533WorkArea().getNewKey().setClaimId(ws.getEobDb2CursorArea().getS02801Info().getClmCntlId());
		// COB_CODE: MOVE DB2-CLM-CNTL-SFX-ID TO NEW-CLAIM-SFX.
		ws.getWsNf0533WorkArea().getNewKey().setClaimSfx(ws.getEobDb2CursorArea().getS02801Info().getClmCntlSfxId());
		// COB_CODE: MOVE DB2-CLM-PMT-ID      TO NEW-PMT-ID.
		ws.getWsNf0533WorkArea().getNewKey().setPmtId(TruncAbs.toShort(ws.getEobDb2CursorArea().getS02801Info().getClmPmtId(), 2));
	}

	/**Original name: 2300-NEW-NF0738-CLAIM<br>
	 * <pre>    DISPLAY '** 2300-NEW-NF0738-CLAIM **'.
	 *   THIS PARAGRAPH WILL DETERMINE IF THE CLAIM WILL BE:
	 *    1) MONTHLY EOB    (DRUG CLAIMS )
	 *    2) QUARTERLY EOB
	 *    3) DAILY EOB</pre>*/
	private void newNf0738Claim() {
		// COB_CODE: IF DB2-VBR-IN = 'Y'
		//              MOVE 'Y' TO HOLD-VBR-IN
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getVbrIn() == 'Y') {
			// COB_CODE: MOVE 'Y' TO HOLD-VBR-IN
			ws.getWsNf0533WorkArea().setHoldVbrInFormatted("Y");
		}
		// COB_CODE: IF DB2-PMT-HIST-LOAD-CD = 'D'
		//               MOVE 'YES' TO NF0738-WRAP-SW
		//           ELSE
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd() == 'D') {
			// COB_CODE: MOVE 'MONTHLY' TO EVNT-LOG-STAT-CD
			ws.getDcls02814sa().setEvntLogStatCd("MONTHLY");
			// COB_CODE: MOVE 'M' TO WS-MT-QT-INDICATOR
			ws.getWsNf0533WorkArea().setWsMtQtIndicatorFormatted("M");
			// COB_CODE: MOVE 'YES' TO NF0738-WRAP-SW
			ws.getWsSwitches().setNf0738WrapSw("YES");
		} else if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtLobCd() == 'D'
				&& ws.getWsNf0533WorkArea().getWsDb2NcovAmt().compareTo(0) == 0) {
			// COB_CODE: IF DB2-PMT-LOB-CD = 'D'
			//           AND WS-DB2-NCOV-AMT = 0
			//               MOVE 'YES' TO NF0738-WRAP-SW
			//           ELSE
			//           END-IF
			//           END-IF.
			// COB_CODE: MOVE 'QUARTERLY' TO EVNT-LOG-STAT-CD
			ws.getDcls02814sa().setEvntLogStatCd("QUARTERLY");
			// COB_CODE: MOVE 'Q' TO WS-MT-QT-INDICATOR
			ws.getWsNf0533WorkArea().setWsMtQtIndicatorFormatted("Q");
			// COB_CODE: MOVE 'YES' TO NF0738-WRAP-SW
			ws.getWsSwitches().setNf0738WrapSw("YES");
		} else if (ws.getWsNf0533WorkArea().getWsDb2PatResp().compareTo(0) > 0 || ws.getWsNf0533WorkArea().getWsDb2PatResp().compareTo(0) < 0
				|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtOiCd() == 'P'
				|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtOiCd() == 'Q') {
			// COB_CODE: IF WS-DB2-PAT-RESP > 0
			//           OR WS-DB2-PAT-RESP < 0
			//           OR DB2-PMT-OI-CD = 'P'
			//           OR DB2-PMT-OI-CD = 'Q'
			//               CONTINUE
			//           ELSE
			//               END-IF
			//           END-IF
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: MOVE DB2-PMT-GRP-ID TO HOLD-GROUP-NUMBER
			ws.getWsConstants().setHoldGroupNumber(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtGrpId());
			// COB_CODE: IF SPECIAL-DAILY-GROUP
			//              OR  (HOLD-TYP-GRP-CD = 'A  ' OR ' A ' OR '  A' )
			//                CONTINUE
			//           ELSE
			//           END-IF
			//           END-IF
			if (ws.getWsConstants().isSpecialDailyGroup() || Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), "A  ")
					|| Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), " A ")
					|| Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), "  A")) {
				// COB_CODE: CONTINUE
				//continue
			} else if (ws.getWsNf0533WorkArea().getWsDb2PwoAmt().compareTo(0) == 0
					&& (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtOiCd() != 'P'
							|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtOiCd() != 'Q')) {
				// COB_CODE: IF (WS-DB2-PWO-AMT = 0 AND
				//              (DB2-PMT-OI-CD NOT = 'P' OR 'Q'))
				//               MOVE 'YES' TO NF0738-WRAP-SW
				//           END-IF
				// COB_CODE: MOVE 'QUARTERLY' TO EVNT-LOG-STAT-CD
				ws.getDcls02814sa().setEvntLogStatCd("QUARTERLY");
				// COB_CODE: MOVE 'Q' TO WS-MT-QT-INDICATOR
				ws.getWsNf0533WorkArea().setWsMtQtIndicatorFormatted("Q");
				// COB_CODE: MOVE 'YES' TO NF0738-WRAP-SW
				ws.getWsSwitches().setNf0738WrapSw("YES");
			}
		}
	}

	/**Original name: 2400-UPDATE-NF0738-CLAIM<br>
	 * <pre>    DISPLAY '** 2400-UPDATE-NF0738-CLAIM **'.</pre>*/
	private void updateNf0738Claim() {
		// COB_CODE: MOVE HOLD-CLAIM-ID      TO CLM-CNTL-ID     OF DCLS02814SA.
		ws.getDcls02814sa().setClmCntlId(ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted());
		// COB_CODE: MOVE HOLD-CLAIM-SFX     TO CLM-CNTL-SFX-ID OF DCLS02814SA.
		ws.getDcls02814sa().setClmCntlSfxId(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx());
		// COB_CODE: MOVE HOLD-PMT-ID        TO CLM-PMT-ID      OF DCLS02814SA.
		ws.getDcls02814sa().setClmPmtId(ws.getWsNf0533WorkArea().getHoldKey().getPmtId());
		// COB_CODE: MOVE WS-BUSINESS-DATE   TO EVNT-LOG-BUS-DT.
		ws.getDcls02814sa().setEvntLogBusDt(ws.getWsDateFields().getWsBusinessDate().getWsBusinessDateFormatted());
		// COB_CODE: MOVE WS-BUSINESS-DATE   TO EVNT-LOG-SYST-DT.
		ws.getDcls02814sa().setEvntLogSystDt(ws.getWsDateFields().getWsBusinessDate().getWsBusinessDateFormatted());
		// COB_CODE: MOVE HOLD-KCAPS-TEAM-NM TO DB2-PMT-KCAPS-TEAM-NM.
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtKcapsTeamNm(ws.getWsNf0533WorkArea().getHoldKcapsTeamNm());
		// COB_CODE: MOVE HOLD-KCAPS-USE-ID  TO DB2-PMT-KCAPS-USE-ID.
		ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtKcapsUseId(ws.getWsNf0533WorkArea().getHoldKcapsUseId());
		// COB_CODE: IF WS-TEST-FLAG = 'Y'
		//                                 HOLD-PMT-ID
		//           ELSE
		//           PERFORM 9700-INSERT-S02814SA THRU 9700-EXIT
		//           END-IF.
		if (ws.getWsRestartStuff().getWsRestartJobParms().getTestFlag() == 'Y') {
			// COB_CODE: DISPLAY '2814 STATUS: ' EVNT-LOG-STAT-CD
			//                   ' CLAIM: ' HOLD-CLAIM-ID
			//                              HOLD-CLAIM-SFX
			//                              HOLD-PMT-ID
			DisplayUtil.sysout.write(new String[] { "2814 STATUS: ", ws.getDcls02814sa().getEvntLogStatCdFormatted(), " CLAIM: ",
					ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted(), String.valueOf(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx()),
					ws.getWsNf0533WorkArea().getHoldKey().getPmtIdAsString() });
		} else {
			// COB_CODE: IF HOLD-VBR-IN = 'Y'
			//              MOVE 'Y' TO HOLD-VBR-IN
			//           END-IF
			if (ws.getWsNf0533WorkArea().getHoldVbrIn() == 'Y') {
				// COB_CODE: MOVE 'EOB BYPASS' TO EVNT-LOG-STAT-CD
				ws.getDcls02814sa().setEvntLogStatCd("EOB BYPASS");
				// COB_CODE: MOVE 'Y' TO HOLD-VBR-IN
				ws.getWsNf0533WorkArea().setHoldVbrInFormatted("Y");
			}
			// COB_CODE: PERFORM 9700-INSERT-S02814SA THRU 9700-EXIT
			insertS02814sa();
		}
	}

	/**Original name: 2900-END-OF-CLAIM<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 2900-END-OF-CLAIM **'.
	 *                                                             *</pre>*/
	private void endOfClaim() {
		// COB_CODE: IF COMMIT-COUNTER >= ROW-FREQ-CMT-CT
		//               PERFORM 9090-SQL-COMMIT THRU 9090-EXIT
		//           END-IF.
		if (ws.getWsAllCounters().getWsCounters().getCommitCounter() >= ws.getDcls03086sa().getRowFreqCmtCt()) {
			// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN
			ws.getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
			// COB_CODE: MOVE GREGORN-HH   TO FORMAT-HH
			ws.getFormattedTime().setFormatHhFormatted(ws.getGregorn().getrGregornTime().getGregornHhFormatted());
			// COB_CODE: MOVE GREGORN-MI   TO FORMAT-MM
			ws.getFormattedTime().setFormatMmFormatted(ws.getGregorn().getrGregornTime().getGregornMiFormatted());
			// COB_CODE: MOVE GREGORN-SS   TO FORMAT-SS
			ws.getFormattedTime().setFormatSsFormatted(ws.getGregorn().getrGregornTime().getGregornSsFormatted());
			// COB_CODE: DISPLAY 'NF05 COMMIT TIME: ' FORMATTED-TIME
			//                   ' RECORD COUNT: ' COMMIT-COUNTER
			DisplayUtil.sysout.write("NF05 COMMIT TIME: ", ws.getFormattedTime().getFormattedTimeFormatted(), " RECORD COUNT: ",
					ws.getWsAllCounters().getWsCounters().getCommitCounterAsString());
			// COB_CODE: PERFORM 9020-SELECT-S03086SA THRU 9020-EXIT
			selectS03086sa();
			// COB_CODE: MOVE 'NF05'           TO RESTART-INDICATOR
			ws.getWsRestartStuff().getRestartIndicator().setRestartIndicator("NF05");
			// COB_CODE: MOVE HOLD-KEY         TO RESTART-KEY
			ws.getWsRestartStuff().setRestartKeyBytes(ws.getWsNf0533WorkArea().getHoldKey().getHoldKeyBytes());
			// COB_CODE: MOVE WS-RESTART-TXT1  TO RSTRT1-TX
			ws.getDcls03086sa().setRstrt1Tx(ws.getWsRestartStuff().getWsRestartTxt1Formatted());
			// COB_CODE: MOVE WS-ALL-COUNTERS  TO RSTRT2-TX
			ws.getDcls03086sa().setRstrt2Tx(ws.getWsAllCounters().getWsAllCountersFormatted());
			// COB_CODE: PERFORM 9030-UPDATE-S03086SA THRU 9030-EXIT
			updateS03086sa();
			// COB_CODE: PERFORM 9090-SQL-COMMIT THRU 9090-EXIT
			sqlCommit();
		}
		// COB_CODE: IF ATTN-SYS-SW = 'Y'
		//              CONTINUE
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getWsSwitches().getAttnSysSw() == 'Y') {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: PERFORM 3202-PROMPT-PAY-INT-CALC THRU 3202-EXIT
			promptPayIntCalc();
			// COB_CODE: PERFORM 2910-SET-CLAIM-CATG-STAT-CD THRU 2910-EXIT
			setClaimCatgStatCd();
			// COB_CODE: PERFORM 2920-UPDATE-XREF-DATA THRU 2920-EXIT
			updateXrefData();
			// COB_CODE: IF HOLD-LINES-PRC-SW = 'Y'
			//              END-IF
			//           ELSE
			//              END-IF
			//           END-IF
			if (ws.getWsSwitches().getHoldLinesPrcSw() == 'Y') {
				// COB_CODE: PERFORM 6200-BUILD-A-TOTAL-LINE  THRU 6200-EXIT
				buildATotalLine();
				// COB_CODE: IF WRAP-NF0738
				//              PERFORM 8950-WRITE-MTH-QTR THRU 8950-EXIT
				//           ELSE
				//              PERFORM 8800-WRITE-EOB-RECORD    THRU 8800-EXIT
				//           END-IF
				if (ws.getWsSwitches().isWrapNf0738()) {
					// COB_CODE: MOVE WS-MT-QT-INDICATOR TO NF05-MT-QT-INDICATOR
					ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05MtQtIndicator(ws.getWsNf0533WorkArea().getWsMtQtIndicator());
					// COB_CODE: PERFORM 2400-UPDATE-NF0738-CLAIM THRU 2400-EXIT
					updateNf0738Claim();
					// COB_CODE: PERFORM 8950-WRITE-MTH-QTR THRU 8950-EXIT
					writeMthQtr();
				} else {
					// COB_CODE: PERFORM 8800-WRITE-EOB-RECORD    THRU 8800-EXIT
					writeEobRecord();
				}
			} else {
				// COB_CODE: ADD 1 TO WS-BYPASS-PMTS
				ws.getWsAllCounters().getWsCounters().setWsBypassPmts(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsBypassPmts(), 7));
				// COB_CODE: IF HOLD-HIST-LOAD-CD = 'L'
				//              MOVE 'MANUAL CK' TO NF05-GEN-NUMBER
				//           ELSE
				//              END-IF
				//           END-IF
				if (ws.getWsNf0533WorkArea().getHoldHistLoadCd() == 'L') {
					// COB_CODE: MOVE 'MANUAL CK' TO NF05-GEN-NUMBER
					ws.getNf05EobRecord().getNf05LineLevelRecord().setGenNumber("MANUAL CK");
				} else if (ws.getWsNf0533WorkArea().getHoldHistLoadCd() == 'M') {
					// COB_CODE: IF HOLD-HIST-LOAD-CD = 'M'
					//              MOVE 'NOCHECK#' TO NF05-GEN-NUMBER
					//           ELSE
					//              END-IF
					//           END-IF
					// COB_CODE: MOVE 'NOCHECK#' TO NF05-GEN-NUMBER
					ws.getNf05EobRecord().getNf05LineLevelRecord().setGenNumber("NOCHECK#");
				} else if (ws.getWsNf0533WorkArea().getHoldHistLoadCd() == 'D' && ws.getWsNf0533WorkArea().getHoldKey().getPmtId() == 1) {
					// COB_CODE: IF HOLD-HIST-LOAD-CD = 'D' AND
					//              HOLD-PMT-ID = 1
					//              MOVE SPACES TO NF05-GEN-NUMBER
					//           ELSE
					//              MOVE 'NOCHECK#' TO NF05-GEN-NUMBER
					//           END-IF
					// COB_CODE: MOVE SPACES TO NF05-GEN-NUMBER
					ws.getNf05EobRecord().getNf05LineLevelRecord().setGenNumber("");
				} else {
					// COB_CODE: MOVE 'NOCHECK#' TO NF05-GEN-NUMBER
					ws.getNf05EobRecord().getNf05LineLevelRecord().setGenNumber("NOCHECK#");
				}
			}
			// COB_CODE: MOVE HOLD-CLAIM-ID TO CLM-CNTL-ID OF DCLS02813SA
			ws.getDcls02813sa().setClmCntlId(ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted());
			// COB_CODE: MOVE HOLD-CLAIM-SFX TO CLM-CNTL-SFX-ID OF DCLS02813SA
			ws.getDcls02813sa().setClmCntlSfxId(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx());
			// COB_CODE: MOVE HOLD-PMT-ID TO CLM-PMT-ID OF DCLS02813SA
			ws.getDcls02813sa().setClmPmtId(ws.getWsNf0533WorkArea().getHoldKey().getPmtId());
			// COB_CODE: MOVE SUBSCRIBER-INTEREST TO PRMPT-PAY-INT-AM
			ws.getDcls02813sa().setPrmptPayIntAm(Trunc.toDecimal(ws.getWsPromptPayWorkFields().getSubscriberInterest(), 11, 2));
			// COB_CODE: MOVE ACCRUED-SUB-INTEREST TO ACR-PRMT-PA-INT-AM
			//                                     OF DCLS02813SA
			ws.getDcls02813sa().setAcrPrmtPaIntAm(Trunc.toDecimal(ws.getWsPromptPayWorkFields().getAccruedSubInterest(), 11, 2));
			// COB_CODE: MOVE NF05-GEN-NUMBER TO CK-ID OF DCLS02813SA
			ws.getDcls02813sa().setCkId(ws.getNf05EobRecord().getNf05LineLevelRecord().getGenNumber());
			// COB_CODE: MOVE WS-BUSINESS-DATE TO CK-DT OF DCLS02813SA
			ws.getDcls02813sa().setCkDt(ws.getWsDateFields().getWsBusinessDate().getWsBusinessDateFormatted());
			// COB_CODE: IF HOLD-ASG-CD = 'Y1'
			//              END-IF
			//           ELSE
			//              END-IF
			//           END-IF
			if (Conditions.eq(ws.getWsNf0533WorkArea().getHoldAsgCd(), "Y1")) {
				// COB_CODE: IF HOLD-ITS-CLM-TYP-CD = 'M'
				//              ADD 1 TO WS-ITS-Y1-CNT
				//           END-IF
				if (ws.getWsNf0533WorkArea().getHoldItsClmTypCd() == 'M') {
					// COB_CODE: ADD 1 TO WS-ITS-Y1-CNT
					ws.getWsAllCounters().getWsCounters().setWsItsY1Cnt(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsItsY1Cnt(), 7));
				}
			} else if (ws.getWsRestartStuff().getWsRestartJobParms().getTestFlag() == 'Y') {
				// COB_CODE: IF WS-TEST-FLAG = 'Y'
				//              END-IF
				//           ELSE
				//              END-IF
				//           END-IF
				// COB_CODE: IF HOLD-HIST-LOAD-CD = 'M'
				//                        HOLD-PMT-ID
				//           ELSE
				//              END-IF
				//           END-IF
				if (ws.getWsNf0533WorkArea().getHoldHistLoadCd() == 'M') {
					// COB_CODE:  DISPLAY '2813 CK# - HIST LOAD M'
					//           ' CLAIM: ' HOLD-CLAIM-ID
					//                      HOLD-CLAIM-SFX
					//                      HOLD-PMT-ID
					DisplayUtil.sysout.write("2813 CK# - HIST LOAD M", " CLAIM: ", ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted(),
							String.valueOf(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx()),
							ws.getWsNf0533WorkArea().getHoldKey().getPmtIdAsString());
				} else if (ws.getWsNf0533WorkArea().getHoldHistLoadCd() == 'D' && ws.getWsNf0533WorkArea().getHoldKey().getPmtId() == 1) {
					// COB_CODE: IF HOLD-HIST-LOAD-CD = 'D' AND
					//              HOLD-PMT-ID = 1
					//                     HOLD-PMT-ID
					//           ELSE
					//              END-IF
					//           END-IF
					// COB_CODE:     DISPLAY '2813 CK# - HIST LOAD D'
					//           ' CLAIM: ' HOLD-CLAIM-ID
					//                      HOLD-CLAIM-SFX
					//                      HOLD-PMT-ID
					DisplayUtil.sysout.write("2813 CK# - HIST LOAD D", " CLAIM: ", ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted(),
							String.valueOf(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx()),
							ws.getWsNf0533WorkArea().getHoldKey().getPmtIdAsString());
				} else if (Conditions.eq(ws.getWsNf0533WorkArea().getHoldAsgCd(), " ") && ws.getWsNf0533WorkArea().getHoldItsClmTypCd() == 'M') {
					// COB_CODE: IF HOLD-ASG-CD = ' ' AND
					//              HOLD-ITS-CLM-TYP-CD = 'M'
					//                  HOLD-PMT-ID
					//           ELSE
					//                  HOLD-PMT-ID
					//           END-IF
					// COB_CODE:        DISPLAY '2813 CK# - RF-GEN'
					//           ' CLAIM: ' HOLD-CLAIM-ID
					//                      HOLD-CLAIM-SFX
					//                      HOLD-PMT-ID
					DisplayUtil.sysout.write("2813 CK# - RF-GEN", " CLAIM: ", ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted(),
							String.valueOf(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx()),
							ws.getWsNf0533WorkArea().getHoldKey().getPmtIdAsString());
				} else {
					// COB_CODE:        DISPLAY '2813 CHK #: ' NF05-GEN-NUMBER
					//           ' CLAIM: ' HOLD-CLAIM-ID
					//                      HOLD-CLAIM-SFX
					//                      HOLD-PMT-ID
					DisplayUtil.sysout.write(new String[] { "2813 CHK #: ", ws.getNf05EobRecord().getNf05LineLevelRecord().getGenNumberFormatted(),
							" CLAIM: ", ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted(),
							String.valueOf(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx()),
							ws.getWsNf0533WorkArea().getHoldKey().getPmtIdAsString() });
				}
			} else if (ws.getWsNf0533WorkArea().getHoldHistLoadCd() == 'M') {
				// COB_CODE: IF HOLD-HIST-LOAD-CD = 'M'
				//              PERFORM 9600-UPDATE-S02813SA THRU 9600-EXIT
				//           ELSE
				//              END-IF
				//           END-IF
				// COB_CODE: PERFORM 9600-UPDATE-S02813SA THRU 9600-EXIT
				updateS02813sa();
			} else if (ws.getWsNf0533WorkArea().getHoldHistLoadCd() == 'D' && ws.getWsNf0533WorkArea().getHoldKey().getPmtId() == 1) {
				// COB_CODE: IF HOLD-HIST-LOAD-CD = 'D' AND
				//              HOLD-PMT-ID = 1
				//                                  THRU 9600-EXIT
				//           ELSE
				//              END-IF
				//           END-IF
				// COB_CODE: PERFORM 9600-UPDATE-S02813SA
				//                               THRU 9600-EXIT
				updateS02813sa();
			} else if (Conditions.eq(ws.getWsNf0533WorkArea().getHoldAsgCd(), " ") && ws.getWsNf0533WorkArea().getHoldItsClmTypCd() == 'M') {
				// COB_CODE: IF HOLD-ASG-CD = ' ' AND
				//              HOLD-ITS-CLM-TYP-CD = 'M'
				//                                  THRU 9600-EXIT
				//           ELSE
				//                                  THRU 9600-EXIT
				//           END-IF
				// COB_CODE: PERFORM 9600-UPDATE-S02813SA
				//                               THRU 9600-EXIT
				updateS02813sa();
			} else {
				// COB_CODE: PERFORM 9600-UPDATE-S02813SA
				//                               THRU 9600-EXIT
				updateS02813sa();
			}
		}
		// COB_CODE: MOVE NEW-KEY                  TO HOLD-KEY.
		ws.getWsNf0533WorkArea().getHoldKey().setHoldKeyBytes(ws.getWsNf0533WorkArea().getNewKey().getNewKeyBytes());
		// COB_CODE: MOVE DB2-PMT-KCAPS-TEAM-NM    TO HOLD-KCAPS-TEAM-NM.
		ws.getWsNf0533WorkArea().setHoldKcapsTeamNm(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsTeamNm());
		// COB_CODE: MOVE DB2-PMT-KCAPS-USE-ID     TO HOLD-KCAPS-USE-ID.
		ws.getWsNf0533WorkArea().setHoldKcapsUseId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsUseId());
		// COB_CODE: MOVE DB2-PMT-ASG-CD           TO HOLD-ASG-CD.
		ws.getWsNf0533WorkArea().setHoldAsgCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd());
		// COB_CODE: MOVE DB2-PMT-HIST-LOAD-CD     TO HOLD-HIST-LOAD-CD.
		ws.getWsNf0533WorkArea().setHoldHistLoadCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd());
		// COB_CODE: MOVE DB2-PMT-ITS-CLM-TYP-CD   TO HOLD-ITS-CLM-TYP-CD.
		ws.getWsNf0533WorkArea().setHoldItsClmTypCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd());
		// COB_CODE: MOVE DB2-PMT-PGM-AREA-CD      TO HOLD-PGM-AREA-CD.
		ws.getWsNf0533WorkArea().setHoldPgmAreaCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPgmAreaCd());
		// COB_CODE: MOVE DB2-PMT-ENR-CL-CD        TO HOLD-ENR-CL-CD.
		ws.getWsNf0533WorkArea().setHoldEnrClCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtEnrClCd());
		// COB_CODE: MOVE DB2-PMT-FIN-CD           TO HOLD-FIN-CD.
		ws.getWsNf0533WorkArea().setHoldFinCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtFinCd());
		// COB_CODE: MOVE DB2-PMT-INS-ID           TO HOLD-INS-ID.
		ws.getWsNf0533WorkArea().setHoldInsId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtInsId());
		// COB_CODE: MOVE DB2-PMT-ALT-INS-ID       TO HOLD-ALT-INS-ID.
		ws.getWsNf0533WorkArea().setHoldAltInsId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAltInsId());
		// COB_CODE: MOVE PATIENT-NAME             TO HOLD-PATIENT-NAME.
		ws.getWsNf0533WorkArea().setHoldPatientName(ws.getWsNf0533WorkArea().getPatientName());
		// COB_CODE: MOVE PERFORMING-NAME          TO HOLD-PERFORMING-NAME.
		ws.getWsNf0533WorkArea().setHoldPerformingName(ws.getWsNf0533WorkArea().getPerformingName());
		// COB_CODE: MOVE BILLING-NAME             TO HOLD-BILLING-NAME.
		ws.getWsNf0533WorkArea().setHoldBillingName(ws.getWsNf0533WorkArea().getBillingName());
		// COB_CODE: MOVE DB2-BI-PROV-ID           TO HOLD-BI-PROV-ID.
		ws.getWsNf0533WorkArea().setHoldBiProvId(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvId());
		// COB_CODE: MOVE DB2-BI-PROV-LOB          TO HOLD-BI-PROV-LOB.
		ws.getWsNf0533WorkArea().setHoldBiProvLob(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvLob());
		// COB_CODE: MOVE DB2-PMT-PMT-BK-PROD-CD   TO HOLD-BK-PROD-CD.
		ws.getWsNf0533WorkArea().setHoldBkProdCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPmtBkProdCd());
		// COB_CODE: MOVE DB2-PMT-TYP-GRP-CD       TO HOLD-TYP-GRP-CD.
		ws.getWsNf0533WorkArea().setHoldTypGrpCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd());
		// COB_CODE: MOVE DB2-PMT-NPI-CD           TO HOLD-NPI-CD.
		ws.getWsNf0533WorkArea().setHoldNpiCd(String.valueOf(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtNpiCd()));
		// COB_CODE: MOVE DB2-PMT-MEM-ID           TO HOLD-MEMBER-ID.
		ws.getWsNf0533WorkArea().setHoldMemberId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtMemId());
		// COB_CODE: MOVE DB2-CLM-SYST-VER-ID      TO HOLD-CLM-SYST-VER-ID.
		ws.getWsNf0533WorkArea().setHoldClmSystVerId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getClmSystVerId());
		// COB_CODE: MOVE DB2-PA-ID-TO-ACUM-ID     TO HOLD-PA-ID-TO-ACUM-ID.
		ws.getWsNf0533WorkArea().setHoldPaIdToAcumId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPaIdToAcumId());
		// COB_CODE: MOVE DB2-MEM-ID-TO-ACUM-ID    TO HOLD-MEM-ID-TO-ACUM-ID.
		ws.getWsNf0533WorkArea().setHoldMemIdToAcumId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getMemIdToAcumId());
		// COB_CODE: MOVE DB2-PROD-IND             TO HOLD-PROD-IND.
		ws.getWsNf0533WorkArea().setHoldProdInd(ws.getEobDb2CursorArea().getS02801Info().getProdInd());
		// COB_CODE: MOVE DB2-ACR-PRMT-PA-INT-AM   TO HOLD-ACR-PRMT-PA-INT-AM.
		ws.getWsNf0533WorkArea()
				.setHoldAcrPrmtPaIntAm(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getAcrPrmtPaIntAm(), 11, 2));
		// COB_CODE: MOVE DB2-CLM-INS-LN-CD        TO HOLD-CLM-INS-LN-CD.
		ws.getWsNf0533WorkArea().setHoldClmInsLnCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getClmInsLnCd());
		// COB_CODE: MOVE 'N' TO HOLD-LINES-PRC-SW.
		ws.getWsSwitches().setHoldLinesPrcSwFormatted("N");
		// COB_CODE: MOVE 'N' TO HOLD-EXP-PRC-SW.
		ws.getWsSwitches().setHoldExpPrcSwFormatted("N");
		// COB_CODE: MOVE 'N' TO ATTN-SYS-SW.
		ws.getWsSwitches().setAttnSysSwFormatted("N");
		// COB_CODE: MOVE 'Y' TO EXPENSE-SW.
		ws.getWsSwitches().setExpenseSwFormatted("Y");
		// COB_CODE: MOVE 'N' TO BYPASS-SW.
		ws.getWsSwitches().setBypassSwFormatted("N");
		// COB_CODE: MOVE 'NO ' TO SRS-PROVIDER-SW.
		ws.getWsSwitches().setSrsProviderSw("NO ");
		// COB_CODE: MOVE 'N' TO COVID19-PROCCD-SW.
		ws.getWsSwitches().setCovid19ProccdSw(false);
		// COB_CODE: INITIALIZE WS-LINE-ACCUMULATORS.
		initLineAccumulators();
		// COB_CODE: INITIALIZE WS-CLAIM-ACCUMULATORS.
		initClaimAccumulators();
		// COB_CODE: INITIALIZE WS-PROMPT-PAY-WORK-FIELDS.
		initWsPromptPayWorkFields();
		// COB_CODE: INITIALIZE NF05-LINE-LEVEL-RECORD.
		initNf05LineLevelRecord();
		// COB_CODE: INITIALIZE NF05-TOTAL-RECORD.
		initNf05TotalRecord();
		// COB_CODE: MOVE 'N' TO HOLD-VBR-IN.
		ws.getWsNf0533WorkArea().setHoldVbrInFormatted("N");
		// COB_CODE: MOVE 'YES' TO ALL-DENIED-SW.
		ws.getWsSwitches().setAllDeniedSw("YES");
		// COB_CODE: MOVE 'NO ' TO NF0738-WRAP-SW.
		ws.getWsSwitches().setNf0738WrapSw("NO ");
		// COB_CODE: IF DB2-PMT-ADJD-PROV-STAT-CD = 'N'
		//           AND DB2-LI-THR-SERV-DT <= HOLD-COVID-END-DATE
		//                  THRU 2950-EXIT
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjdProvStatCd() == 'N'
				&& Conditions.lte(ws.getEobDb2CursorArea().getS02809LineInfo().getThrServDt(), ws.getWsSwitches().getHoldCovidEndDate())) {
			// COB_CODE: PERFORM 2950-COVID19-PROCCD-EXISTENCE
			//              THRU 2950-EXIT
			covid19ProccdExistence();
		}
		// COB_CODE: IF LAST-TIME-CHECK = 'YES'
		//              CONTINUE
		//           ELSE
		//              END-IF
		//           END-IF.
		if (Conditions.eq(ws.getWsSwitches().getLastTimeCheck(), "YES")) {
			// COB_CODE: CONTINUE
			//continue
		} else if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd(), "Y1")) {
			// COB_CODE: IF DB2-PMT-ASG-CD = 'Y1'
			//              END-IF
			//           ELSE
			//              PERFORM 3200-PROMPT-PAY-INTEREST THRU 3200-EXIT
			//           END-IF
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//              CONTINUE
			//           ELSE
			//              PERFORM 2300-NEW-NF0738-CLAIM THRU 2300-EXIT
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: CONTINUE
				//continue
			} else {
				// COB_CODE: PERFORM 2300-NEW-NF0738-CLAIM THRU 2300-EXIT
				newNf0738Claim();
			}
		} else {
			// COB_CODE: PERFORM 7000-CALL-NF0531SR      THRU 7000-EXIT
			callNf0531sr();
			// COB_CODE: PERFORM 3200-PROMPT-PAY-INTEREST THRU 3200-EXIT
			promptPayInterest();
		}
	}

	/**Original name: 2950-COVID19-PROCCD-EXISTENCE<br>
	 * <pre>                                                                *</pre>*/
	private void covid19ProccdExistence() {
		// COB_CODE: SET COVID19-PROCCD-NO TO TRUE.
		ws.getWsSwitches().setCovid19ProccdSw(false);
		// COB_CODE: EXEC SQL
		//              SELECT PROC_CD
		//                INTO :DCLS02809SA.PROC-CD
		//              FROM S02809SA
		//           WHERE PROC_CD IN ('91300','91301','0001A','0002A','0011A',
		//                             '0012A','0021A','0022A','0031A','91302',
		//                             '91303')
		//             AND CLM_CNTL_ID     = :DB2-CLM-CNTL-ID
		//             AND CLM_CNTL_SFX_ID = :DB2-CLM-CNTL-SFX-ID
		//             AND CLM_PMT_ID      = :DB2-CLM-PMT-ID
		//             FETCH FIRST ROW ONLY
		//             WITH UR
		//            END-EXEC.
		ws.getDcls02809sa()
				.setProcCd(s02809saDao.selectRec(ws.getEobDb2CursorArea().getS02801Info().getClmCntlId(),
						ws.getEobDb2CursorArea().getS02801Info().getClmCntlSfxId(), ws.getEobDb2CursorArea().getS02801Info().getClmPmtId(),
						ws.getDcls02809sa().getProcCd()));
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN +000
		//             WHEN -811
		//             WHEN +811
		//                  MOVE 'Y' TO COVID19-PROCCD-SW
		//             WHEN +100
		//                  CONTINUE
		//             WHEN OTHER
		//                  PERFORM 9999-DB2-ABEND
		//             END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:
		case -811:
		case 811:// COB_CODE: MOVE 'Y' TO COVID19-PROCCD-SW
			ws.getWsSwitches().setCovid19ProccdSw(true);
			break;

		case 100:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: MOVE 'TA02809'           TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("TA02809");
			// COB_CODE: MOVE 'SELECT'          TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("SELECT");
			// COB_CODE: MOVE '2950-COVID19-PROCCD-EXISTENCE' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("2950-COVID19-PROCCD-EXISTENCE");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 2910-SET-CLAIM-CATG-STAT-CD<br>
	 * <pre>    DISPLAY '** 2910-SET-CLAIM-CATG-STAT-CD **'.
	 *                                                                 *
	 * ****************************************************************
	 *     SETTING CATG-ST 'FINALIZES' THE PAY-SUB CLAIM.  THIS       *
	 *     PARAGRAPH IS PERFORMED AT THE END OF CLAIM PROCESSING.     *
	 *                                                                *
	 *                                                                *
	 *       'F1' = PAID                                              *
	 *       'F2' = FULLY DENIED                                      *
	 *       'F3' = FINALIZED/REVISED - ADJUD INFO HAS BEEN CHGD      *
	 *      'F3F' = FINALIZED/FORWARDED                               *
	 *      'F3N' = FINALIZED/NOT FORWARDED                           *
	 *       'F4' = FINALIZED NO PAYMENT FORTHCOMING (STAT ADJ?)      *
	 *       'F5' = FINALIZED/CANNOT PROCESS                          *
	 * ****************************************************************</pre>*/
	private void setClaimCatgStatCd() {
		// COB_CODE: IF HOLD-LINES-PRC-SW = 'N'
		//              END-IF
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getWsSwitches().getHoldLinesPrcSw() == 'N') {
			// COB_CODE: MOVE 'F4' TO HCC-STAT-CATG-CD
			ws.getDcls02801sa().setHccStatCatgCd("F4");
			// COB_CODE: IF HOLD-ASG-CD = 'Y1' AND
			//              HOLD-ITS-CLM-TYP-CD NOT = 'M'
			//              CONTINUE
			//           ELSE
			//           END-IF
			//           END-IF
			if (Conditions.eq(ws.getWsNf0533WorkArea().getHoldAsgCd(), "Y1") && ws.getWsNf0533WorkArea().getHoldItsClmTypCd() != 'M') {
				// COB_CODE: CONTINUE
				//continue
			} else {
				// COB_CODE: IF HOLD-EXP-PRC-SW = 'N' OR DB2-VBR-IN = 'Y'
				//              MOVE 'EOB BYPASS' TO EVNT-LOG-STAT-CD
				//           ELSE
				//              MOVE 'EXPENSE' TO EVNT-LOG-STAT-CD
				//           END-IF
				if (ws.getWsSwitches().getHoldExpPrcSw() == 'N' || ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getVbrIn() == 'Y') {
					// COB_CODE: MOVE 'EOB BYPASS' TO EVNT-LOG-STAT-CD
					ws.getDcls02814sa().setEvntLogStatCd("EOB BYPASS");
				} else {
					// COB_CODE: MOVE 'EXPENSE' TO EVNT-LOG-STAT-CD
					ws.getDcls02814sa().setEvntLogStatCd("EXPENSE");
				}
				// COB_CODE: IF WS-TEST-FLAG = 'Y'
				//                              HOLD-PMT-ID
				//           ELSE
				//              PERFORM 9750-INSERT-S02814SA-SUB THRU 9750-EXIT
				//           END-IF
				if (ws.getWsRestartStuff().getWsRestartJobParms().getTestFlag() == 'Y') {
					// COB_CODE: DISPLAY '2814 STATUS: ' EVNT-LOG-STAT-CD
					//                ' CLAIM: ' HOLD-CLAIM-ID
					//                           HOLD-CLAIM-SFX
					//                           HOLD-PMT-ID
					DisplayUtil.sysout.write(new String[] { "2814 STATUS: ", ws.getDcls02814sa().getEvntLogStatCdFormatted(), " CLAIM: ",
							ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted(),
							String.valueOf(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx()),
							ws.getWsNf0533WorkArea().getHoldKey().getPmtIdAsString() });
				} else {
					// COB_CODE: MOVE HOLD-CLAIM-ID TO CLM-CNTL-ID OF DCLS02814SA
					ws.getDcls02814sa().setClmCntlId(ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted());
					// COB_CODE: MOVE HOLD-CLAIM-SFX TO CLM-CNTL-SFX-ID OF DCLS02814SA
					ws.getDcls02814sa().setClmCntlSfxId(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx());
					// COB_CODE: MOVE HOLD-PMT-ID TO CLM-PMT-ID OF DCLS02814SA
					ws.getDcls02814sa().setClmPmtId(ws.getWsNf0533WorkArea().getHoldKey().getPmtId());
					// COB_CODE: MOVE HOLD-KCAPS-TEAM-NM TO KCAPS-TEAM-NM OF DCLS02814SA
					ws.getDcls02814sa().setKcapsTeamNm(ws.getWsNf0533WorkArea().getHoldKcapsTeamNm());
					// COB_CODE: MOVE HOLD-KCAPS-USE-ID TO KCAPS-USE-ID OF DCLS02814SA
					ws.getDcls02814sa().setKcapsUseId(ws.getWsNf0533WorkArea().getHoldKcapsUseId());
					// COB_CODE: MOVE WS-BUSINESS-DATE TO EVNT-LOG-SYST-DT
					//                                    OF DCLS02814SA
					ws.getDcls02814sa().setEvntLogSystDt(ws.getWsDateFields().getWsBusinessDate().getWsBusinessDateFormatted());
					// COB_CODE: PERFORM 9750-INSERT-S02814SA-SUB THRU 9750-EXIT
					insertS02814saSub();
				}
			}
		} else if (ws.getWsSwitches().isAllDenied()) {
			// COB_CODE: IF ALL-DENIED
			//              MOVE 'F2 ' TO HCC-STAT-CATG-CD
			//           ELSE
			//              MOVE 'F1 ' TO HCC-STAT-CATG-CD
			//           END-IF
			// COB_CODE: MOVE 'F2 ' TO HCC-STAT-CATG-CD
			ws.getDcls02801sa().setHccStatCatgCd("F2 ");
		} else {
			// COB_CODE: MOVE 'F1 ' TO HCC-STAT-CATG-CD
			ws.getDcls02801sa().setHccStatCatgCd("F1 ");
		}
		// COB_CODE: MOVE HOLD-CLAIM-ID       TO CLM-CNTL-ID     OF DCLS02801SA.
		ws.getDcls02801sa().setClmCntlId(ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted());
		// COB_CODE: MOVE HOLD-CLAIM-SFX      TO CLM-CNTL-SFX-ID OF DCLS02801SA.
		ws.getDcls02801sa().setClmCntlSfxId(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx());
		// COB_CODE: MOVE HOLD-PMT-ID         TO CLM-PMT-ID      OF DCLS02801SA.
		ws.getDcls02801sa().setClmPmtId(ws.getWsNf0533WorkArea().getHoldKey().getPmtId());
		// COB_CODE: MOVE WS-BUSINESS-DATE    TO FNL-BUS-DT      OF DCLS02801SA.
		ws.getDcls02801sa().setFnlBusDt(ws.getWsDateFields().getWsBusinessDate().getWsBusinessDateFormatted());
		// COB_CODE: IF HOLD-ASG-CD = 'Y1'
		//              CONTINUE
		//           ELSE
		//              END-IF
		//           END-IF.
		if (Conditions.eq(ws.getWsNf0533WorkArea().getHoldAsgCd(), "Y1")) {
			// COB_CODE: CONTINUE
			//continue
		} else if (ws.getWsRestartStuff().getWsRestartJobParms().getTestFlag() == 'Y') {
			// COB_CODE: IF WS-TEST-FLAG = 'Y'
			//                              HOLD-PMT-ID
			//           ELSE
			//              PERFORM 9800-INSERT-S02801SA THRU 9800-EXIT
			//           END-IF
			// COB_CODE: DISPLAY '2801 STATUS: ' HCC-STAT-CATG-CD
			//                ' CLAIM: ' HOLD-CLAIM-ID
			//                           HOLD-CLAIM-SFX
			//                           HOLD-PMT-ID
			DisplayUtil.sysout.write(new String[] { "2801 STATUS: ", ws.getDcls02801sa().getHccStatCatgCdFormatted(), " CLAIM: ",
					ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted(), String.valueOf(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx()),
					ws.getWsNf0533WorkArea().getHoldKey().getPmtIdAsString() });
		} else {
			// COB_CODE: PERFORM 9800-INSERT-S02801SA THRU 9800-EXIT
			insertS02801sa();
		}
	}

	/**Original name: 2920-UPDATE-XREF-DATA<br>
	 * <pre>    DISPLAY '** 2920-UPDATE-XREF-DATA  **'.
	 *                                                                 *
	 * ****************************************************************
	 *     UPDATING THE XREF TABLE (TA04315) RPT_IN TO A Y            *
	 *     WHEN THE ACT_INT_CD IS Y AND THERE IS A CLAIMCHECK STATUS  *
	 *     CODE OF X OR O ON THE PAYMENT TABLE (TA02813) AND A        *
	 *     THERE IS NOT A RESULT SET ON THE TA04316 TABLE FOR THIS    *
	 *     CLAIM,                                                     *
	 * ****************************************************************</pre>*/
	private void updateXrefData() {
		// COB_CODE: MOVE HOLD-CLAIM-ID TO CLM-CNTL-ID OF DCLS04315SA
		ws.getDcls04315sa().setClmCntlId(ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted());
		// COB_CODE: MOVE HOLD-CLAIM-SFX TO CLM-CNTL-SFX-ID OF DCLS04315SA
		ws.getDcls04315sa().setClmCntlSfxId(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx());
		// COB_CODE: MOVE HOLD-PMT-ID TO CLM-PMT-ID OF DCLS04315SA
		ws.getDcls04315sa().setClmPmtId(ws.getWsNf0533WorkArea().getHoldKey().getPmtId());
		//  UPDATE FOR NON OUTSTANDING ADJUSTMENTS
		// COB_CODE: EXEC SQL
		//             UPDATE S04315SA XREF
		//                 SET RPT_IN     = 'Y'
		//               WHERE XREF.CLM_CNTL_ID = :DCLS04315SA.CLM-CNTL-ID
		//                 AND XREF.CLM_CNTL_SFX_ID =
		//                       :DCLS04315SA.CLM-CNTL-SFX-ID
		//                 AND XREF.CLM_PMT_ID  =  :DCLS04315SA.CLM-PMT-ID
		//                 AND XREF.RPT_IN NOT IN ('Y')
		//                 AND XREF.ACT_INACT_CD = 'A'
		//                 AND XREF.CLM_CNTL_ID = XREF.INIT_CLM_CNTL_ID
		//                 AND XREF.CLM_CNTL_SFX_ID = XREF.INT_CLM_CNL_SFX_ID
		//                 AND XREF.CLM_PMT_ID  =  XREF.INIT_CLM_PMT_ID
		//                 AND EXISTS (
		//                     SELECT 1
		//                     FROM S02813SA  PAY
		//                     WHERE XREF.INIT_CLM_CNTL_ID = PAY.CLM_CNTL_ID
		//                       AND XREF.INT_CLM_CNL_SFX_ID  =
		//                          PAY.CLM_CNTL_SFX_ID
		//                       AND XREF.INIT_CLM_PMT_ID  = PAY.CLM_PMT_ID
		//                       AND PAY.CLMCK_ADJ_STAT_CD IN (' ','C')
		//                     )
		//           END-EXEC.
		s02813saS04315saDao.updateRec(ws.getDcls04315sa().getClmCntlId(), ws.getDcls04315sa().getClmCntlSfxId(), ws.getDcls04315sa().getClmPmtId());
		// COB_CODE:      EVALUATE SQLCODE
		//                  WHEN 0
		//                      ADD 1 TO WS-S04315-UPDATES
		//           *          DISPLAY 'UPDATE 1'
		//           *          DISPLAY 'HOLD-CLAIM-ID: ', HOLD-CLAIM-ID
		//           *          DISPLAY 'HOLD-CLAIM-SFX: ', HOLD-CLAIM-SFX
		//           *          DISPLAY 'HOLD-PMT-ID: ', HOLD-PMT-ID
		//                  WHEN 100
		//                      CONTINUE
		//                  WHEN OTHER
		//                      PERFORM 9999-DB2-ABEND
		//                END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: ADD 1 TO WS-S04315-UPDATES
			ws.getWsAllCounters().getWsCounters().setWsS04315Updates(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsS04315Updates(), 7));
			//          DISPLAY 'UPDATE 1'
			//          DISPLAY 'HOLD-CLAIM-ID: ', HOLD-CLAIM-ID
			//          DISPLAY 'HOLD-CLAIM-SFX: ', HOLD-CLAIM-SFX
			//          DISPLAY 'HOLD-PMT-ID: ', HOLD-PMT-ID
			break;

		case 100:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: MOVE 'S04315SA'              TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S04315SA");
			// COB_CODE: MOVE 'UPDATE S04315SA'       TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("UPDATE S04315SA");
			// COB_CODE: MOVE '2920-UPDATE-XREF-DATA-1' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("2920-UPDATE-XREF-DATA-1");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
		// COB_CODE: MOVE HOLD-CLAIM-ID TO CLM-CNTL-ID OF DCLS04315SA
		ws.getDcls04315sa().setClmCntlId(ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted());
		// COB_CODE: MOVE HOLD-CLAIM-SFX TO CLM-CNTL-SFX-ID OF DCLS04315SA
		ws.getDcls04315sa().setClmCntlSfxId(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx());
		// COB_CODE: MOVE HOLD-PMT-ID TO CLM-PMT-ID OF DCLS04315SA
		ws.getDcls04315sa().setClmPmtId(ws.getWsNf0533WorkArea().getHoldKey().getPmtId());
		//  UPDATE FOR OUTSTANDING ADJUSTMENTS
		// COB_CODE: EXEC SQL
		//             UPDATE S04315SA XREF
		//                 SET RPT_IN     = 'Y'
		//               WHERE XREF.CLM_CNTL_ID = :DCLS04315SA.CLM-CNTL-ID
		//                 AND XREF.CLM_CNTL_SFX_ID =
		//                       :DCLS04315SA.CLM-CNTL-SFX-ID
		//                 AND XREF.CLM_PMT_ID  =  :DCLS04315SA.CLM-PMT-ID
		//                 AND XREF.RPT_IN NOT IN ('Y')
		//                 AND XREF.ACT_INACT_CD = 'A'
		//                 AND XREF.CLM_CNTL_ID = XREF.INIT_CLM_CNTL_ID
		//                 AND XREF.CLM_CNTL_SFX_ID = XREF.INT_CLM_CNL_SFX_ID
		//                 AND XREF.CLM_PMT_ID  =  XREF.INIT_CLM_PMT_ID
		//                 AND EXISTS (
		//                     SELECT 1
		//                     FROM S02813SA  PAY
		//                     WHERE XREF.INIT_CLM_CNTL_ID = PAY.CLM_CNTL_ID
		//                       AND XREF.INT_CLM_CNL_SFX_ID  =
		//                          PAY.CLM_CNTL_SFX_ID
		//                       AND XREF.INIT_CLM_PMT_ID  = PAY.CLM_PMT_ID
		//                       AND PAY.CLMCK_ADJ_STAT_CD IN ('X','O')
		//                     )
		//                 AND NOT EXISTS (
		//                     SELECT 1
		//                     FROM S04316SA RES
		//                     WHERE XREF.INIT_CLM_CNTL_ID = RES.INIT_CLM_CNTL_ID
		//                       AND XREF.INT_CLM_CNL_SFX_ID  =
		//                          RES.INT_CLM_CNL_SFX_ID
		//                       AND XREF.INIT_CLM_PMT_ID  = RES.INIT_CLM_PMT_ID
		//                       AND XREF.INIT_CLM_TS      = RES.INIT_CLM_TS
		//                     )
		//           END-EXEC.
		s02813saS04315saS04316saDao.updateRec(ws.getDcls04315sa().getClmCntlId(), ws.getDcls04315sa().getClmCntlSfxId(),
				ws.getDcls04315sa().getClmPmtId());
		// COB_CODE:      EVALUATE SQLCODE
		//                  WHEN 0
		//                      ADD 1 TO WS-S04315-UPDATES
		//           *          DISPLAY 'UPDATE 2'
		//           *          DISPLAY 'HOLD-CLAIM-ID: ', HOLD-CLAIM-ID
		//           *          DISPLAY 'HOLD-CLAIM-SFX: ', HOLD-CLAIM-SFX
		//           *          DISPLAY 'HOLD-PMT-ID: ', HOLD-PMT-ID
		//                  WHEN 100
		//                      CONTINUE
		//                  WHEN OTHER
		//                      PERFORM 9999-DB2-ABEND
		//                END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: ADD 1 TO WS-S04315-UPDATES
			ws.getWsAllCounters().getWsCounters().setWsS04315Updates(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsS04315Updates(), 7));
			//          DISPLAY 'UPDATE 2'
			//          DISPLAY 'HOLD-CLAIM-ID: ', HOLD-CLAIM-ID
			//          DISPLAY 'HOLD-CLAIM-SFX: ', HOLD-CLAIM-SFX
			//          DISPLAY 'HOLD-PMT-ID: ', HOLD-PMT-ID
			break;

		case 100:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: MOVE 'S04315SA'              TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S04315SA");
			// COB_CODE: MOVE 'UPDATE S04315SA'       TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("UPDATE S04315SA");
			// COB_CODE: MOVE '2920-UPDATE-XREF-DATA-2' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("2920-UPDATE-XREF-DATA-2");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 3000-SELECT-CAS-DATA<br>
	 * <pre>    DISPLAY '** 3000-SELECT-CAS-DATA **'.</pre>*/
	private void selectCasData() {
		// COB_CODE: EXEC SQL
		//             SELECT CASV.LI_NCOV_AM
		//                  , CASV.LI_SOCP_DED_AM
		//                  , CASV.LI_SOCP_COINS_AM
		//                  , CASV.LI_SOCP_COPAY_AM
		//                  , CASV.LI_SOCP_PENALTY_AM
		//              INTO :DB2-CAS-NCOV-AM
		//                 , :DB2-CAS-EOB-DED-AM
		//                 , :DB2-CAS-EOB-COINS-AM
		//                 , :DB2-CAS-EOB-COPAY-AM
		//                 , :DB2-CAS-EOB-PENALTY-AM
		//              FROM S02800CA AS CASV
		//              WHERE CASV.CLM_CNTL_ID     = :DB2-CLM-CNTL-ID
		//                AND CASV.CLM_CNTL_SFX_ID = :DB2-CLM-CNTL-SFX-ID
		//                AND CASV.CLM_PMT_ID      = :DB2-CLM-PMT-ID
		//                AND CASV.CLI_ID          = :DB2-CLI-ID
		//            END-EXEC.
		s02800caDao.selectRec(ws.getEobDb2CursorArea().getS02801Info().getClmCntlId(), ws.getEobDb2CursorArea().getS02801Info().getClmCntlSfxId(),
				ws.getEobDb2CursorArea().getS02801Info().getClmPmtId(), ws.getEobDb2CursorArea().getS02801Info().getCliId(),
				ws.getEobDb2CursorArea().getCasCalc1Info());
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 PERFORM 3002-PROCESS-CAS THRU 3002-EXIT
		//             WHEN +100
		//                 CONTINUE
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: PERFORM 3002-PROCESS-CAS THRU 3002-EXIT
			processCas();
			break;

		case 100:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: MOVE 'S02800CA CAS VIEW '     TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S02800CA CAS VIEW ");
			// COB_CODE: MOVE 'SELECT CAS DATA   '     TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("SELECT CAS DATA   ");
			// COB_CODE: MOVE '3000-SELECT-CAS-DATA '  TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("3000-SELECT-CAS-DATA ");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 3001-SELECT-CAS-TABLE<br>
	 * <pre>    DISPLAY '** 3001-SELECT-CAS-TABLE **'.</pre>*/
	private void selectCasTable() {
		// COB_CODE: EXEC SQL
		//             SELECT CASV.CLM_ADJ_GRP_CD
		//                  , CASV.CLM_ADJ_RSN_CD
		//                  , SUM(CASV.MNTRY_AM)
		//              INTO :DB2-CLM-ADJ-GRP-CD
		//                 , :DB2-CLM-ADJ-RSN-CD
		//                 , :DB2-MNTRY-AM
		//              FROM S02800SA AS CASV
		//              WHERE CASV.CLM_CNTL_ID     = :DB2-CLM-CNTL-ID
		//                AND CASV.CLM_CNTL_SFX_ID = :DB2-CLM-CNTL-SFX-ID
		//                AND CASV.CLM_PMT_ID      = :DB2-CLM-PMT-ID
		//                AND CASV.CLI_ID          = :DB2-CLI-ID
		//                AND CASV.CLM_ADJ_GRP_CD  = 'PR'
		//                AND CASV.CLM_ADJ_RSN_CD  = '45'
		//              GROUP BY
		//                CASV.CLM_ADJ_GRP_CD,
		//                CASV.CLM_ADJ_RSN_CD
		//            END-EXEC.
		s02800saDao.selectRec(ws.getEobDb2CursorArea().getS02801Info().getClmCntlId(), ws.getEobDb2CursorArea().getS02801Info().getClmCntlSfxId(),
				ws.getEobDb2CursorArea().getS02801Info().getClmPmtId(), ws.getEobDb2CursorArea().getS02801Info().getCliId(),
				ws.getEobDb2CursorArea().getCasTableInfo());
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                         DB2-MNTRY-AM
		//             WHEN +100
		//                 CONTINUE
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: COMPUTE DB2-CAS-NCOV-AM =
				//                   DB2-CAS-NCOV-AM -
				//                   DB2-MNTRY-AM
			ws.getEobDb2CursorArea().getCasCalc1Info().setNcovAm(Trunc.toDecimal(
					ws.getEobDb2CursorArea().getCasCalc1Info().getNcovAm().subtract(ws.getEobDb2CursorArea().getCasTableInfo().getMntryAm()), 15, 2));
			break;

		case 100:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: MOVE 'S02800CA CAS VIEW '     TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S02800CA CAS VIEW ");
			// COB_CODE: MOVE 'SELECT CAS DATA   '     TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("SELECT CAS DATA   ");
			// COB_CODE: MOVE '3001-SELECT-CAS-TABLE'  TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("3001-SELECT-CAS-TABLE");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 3002-PROCESS-CAS<br>
	 * <pre>    DISPLAY '** 3002-PROCESS-CAS **'.</pre>*/
	private void processCas() {
		// COB_CODE: INITIALIZE WS-HOLD-NOTE-AREA.
		initWsHoldNoteArea();
		// COB_CODE: IF DB2-CAS-EOB-PENALTY-AM NOT EQUAL 0
		//                MOVE 'EOB NOTE CODE B8'     TO WS-WHICH-NOTES-TABLE-1.
		if (ws.getEobDb2CursorArea().getCasCalc1Info().getEobPenaltyAm().compareTo(0) != 0) {
			// COB_CODE:    IF WS-NOTE-1 = SPACES
			//                MOVE 'EOB NOTE CODE B8' TO WS-WHICH-NOTES-TABLE-1
			//           ELSE
			//                MOVE 'EOB NOTE CODE B8'     TO WS-WHICH-NOTES-TABLE-1.
			if (Characters.EQ_SPACE.test(ws.getWsHoldNoteArea().getNote1())) {
				// COB_CODE: MOVE 'B8'               TO WS-NOTE-1
				ws.getWsHoldNoteArea().setNote1("B8");
				// COB_CODE: MOVE 'EOB NOTE CODE B8' TO WS-WHICH-NOTES-TABLE-1
				ws.getWsHoldNoteArea().setWhichNotesTable1("EOB NOTE CODE B8");
			} else {
				// COB_CODE: MOVE WS-NOTE-1              TO WS-NOTE-2
				ws.getWsHoldNoteArea().setNote2(ws.getWsHoldNoteArea().getNote1());
				// COB_CODE: MOVE WS-WHICH-NOTES-TABLE-1 TO WS-WHICH-NOTES-TABLE-1
				ws.getWsHoldNoteArea().setWhichNotesTable1(ws.getWsHoldNoteArea().getWhichNotesTable1());
				// COB_CODE: MOVE 'B8'                   TO WS-NOTE-1
				ws.getWsHoldNoteArea().setNote1("B8");
				// COB_CODE: MOVE 'EOB NOTE CODE B8'     TO WS-WHICH-NOTES-TABLE-1.
				ws.getWsHoldNoteArea().setWhichNotesTable1("EOB NOTE CODE B8");
			}
		}
		// COB_CODE: IF COVID19-PROCCD-YES AND
		//              DB2-LI-PROC-CD = ('91300' OR '91301' OR '0001A' OR '0002A'
		//                                 OR '0011A' OR '0012A' OR '0012A'
		//                                 OR '0021A' OR '0022A' OR '0031A'
		//                                 OR '91302' OR '91303' OR '99211'
		//                                 OR '99202')
		//                  THRU 3003-EXIT
		//           END-IF.
		if (ws.getWsSwitches().isCovid19ProccdSw() && (Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "91300")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "91301")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "0001A")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "0002A")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "0011A")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "0012A")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "0012A")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "0021A")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "0022A")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "0031A")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "91302")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "91303")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "99211")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCd(), "99202"))) {
			// COB_CODE: PERFORM 3003-COVID-NOTES
			//              THRU 3003-EXIT
			covidNotes();
		}
		// COB_CODE: IF DB2-PMT-GRP-ID = '96263' OR '9626C'
		//                     PERFORM 3001-SELECT-CAS-TABLE THRU 3001-EXIT.
		if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtGrpId(), "96263")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtGrpId(), "9626C")) {
			// COB_CODE: IF DB2-PMT-LOB-CD = 'D' OR '5'
			//                  PERFORM 3001-SELECT-CAS-TABLE THRU 3001-EXIT.
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtLobCd() == 'D'
					|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtLobCd() == '5') {
				// COB_CODE: IF DB2-PMT-OI-CD = 'B' OR 'C' OR 'F'
				//               PERFORM 3001-SELECT-CAS-TABLE THRU 3001-EXIT.
				if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtOiCd() == 'B'
						|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtOiCd() == 'C'
						|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtOiCd() == 'F') {
					// COB_CODE: PERFORM 3001-SELECT-CAS-TABLE THRU 3001-EXIT.
					selectCasTable();
				}
			}
		}
	}

	/**Original name: 3003-COVID-NOTES<br>*/
	private void covidNotes() {
		// COB_CODE: IF WS-NOTE-1 = SPACES
		//             MOVE 'EOB NOTE CODE' TO WS-WHICH-NOTES-TABLE-1
		//           ELSE
		//              END-IF
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsHoldNoteArea().getNote1())) {
			// COB_CODE: MOVE 'CV'            TO WS-NOTE-1
			ws.getWsHoldNoteArea().setNote1("CV");
			// COB_CODE: MOVE 'EOB NOTE CODE' TO WS-WHICH-NOTES-TABLE-1
			ws.getWsHoldNoteArea().setWhichNotesTable1("EOB NOTE CODE");
		} else if (Characters.EQ_SPACE.test(ws.getWsHoldNoteArea().getNote2())) {
			// COB_CODE: IF WS-NOTE-2 = SPACES
			//              MOVE 'EOB NOTE CODE' TO WS-WHICH-NOTES-TABLE-2
			//           ELSE
			//              END-IF
			//            END-IF
			// COB_CODE: MOVE 'CV'            TO WS-NOTE-2
			ws.getWsHoldNoteArea().setNote2("CV");
			// COB_CODE: MOVE 'EOB NOTE CODE' TO WS-WHICH-NOTES-TABLE-2
			ws.getWsHoldNoteArea().setWhichNotesTable2("EOB NOTE CODE");
		} else if (Characters.EQ_SPACE.test(ws.getWsHoldNoteArea().getNote3())) {
			// COB_CODE: IF WS-NOTE-3 = SPACES
			//              MOVE 'EOB NOTE CODE' TO WS-WHICH-NOTES-TABLE-3
			//           ELSE
			//             END-IF
			//           END-IF
			// COB_CODE: MOVE 'CV'            TO WS-NOTE-3
			ws.getWsHoldNoteArea().setNote3("CV");
			// COB_CODE: MOVE 'EOB NOTE CODE' TO WS-WHICH-NOTES-TABLE-3
			ws.getWsHoldNoteArea().setWhichNotesTable3("EOB NOTE CODE");
		} else if (Characters.EQ_SPACE.test(ws.getWsHoldNoteArea().getNote4())) {
			// COB_CODE: IF WS-NOTE-4 = SPACES
			//              MOVE 'EOB NOTE CODE' TO WS-WHICH-NOTES-TABLE-4
			//           ELSE
			//              END-IF
			//           END-IF
			// COB_CODE: MOVE 'CV'            TO WS-NOTE-4
			ws.getWsHoldNoteArea().setNote4("CV");
			// COB_CODE: MOVE 'EOB NOTE CODE' TO WS-WHICH-NOTES-TABLE-4
			ws.getWsHoldNoteArea().setWhichNotesTable4("EOB NOTE CODE");
		} else if (Characters.EQ_SPACE.test(ws.getWsHoldNoteArea().getNote5())) {
			// COB_CODE: IF WS-NOTE-5 = SPACES
			//              MOVE 'EOB NOTE CODE' TO WS-WHICH-NOTES-TABLE-5
			//           END-IF
			// COB_CODE: MOVE 'CV'            TO WS-NOTE-5
			ws.getWsHoldNoteArea().setNote5("CV");
			// COB_CODE: MOVE 'EOB NOTE CODE' TO WS-WHICH-NOTES-TABLE-5
			ws.getWsHoldNoteArea().setWhichNotesTable5("EOB NOTE CODE");
		}
	}

	/**Original name: 3100-CREATE-EXPENSE-RECS<br>
	 * <pre>    DISPLAY '** 3100-PREPARE-EXPENSE **'.</pre>*/
	private void createExpenseRecs() {
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: MOVE 'NO ' TO USE-RPT-INDC-1.
		ws.getWsSwitches().setUseRptIndc1("NO ");
		// COB_CODE: MOVE 'NO ' TO USE-RPT-INDC-2.
		ws.getWsSwitches().setUseRptIndc2("NO ");
		// COB_CODE: IF DB2-LI-EXMN-ACTN-CD = ' ' OR
		//              DB2-LI-EXMN-ACTN-CD = 'M'
		//              END-IF
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() == ' '
				|| ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() == 'M') {
			// COB_CODE: IF DB2-CLM-PMT-ID > 1
			//              MOVE 'YES' TO USE-RPT-INDC-2
			//           ELSE
			//              MOVE 'YES' TO USE-RPT-INDC-1
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02801Info().getClmPmtId() > 1) {
				// COB_CODE: MOVE 'YES' TO USE-RPT-INDC-2
				ws.getWsSwitches().setUseRptIndc2("YES");
			} else {
				// COB_CODE: MOVE 'YES' TO USE-RPT-INDC-1
				ws.getWsSwitches().setUseRptIndc1("YES");
			}
		}
		// COB_CODE: IF DB2-C1-PD-AM > 0
		//               PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02811Calc1Info().getPdAm().compareTo(0) > 0) {
			// COB_CODE: IF USE-RPT-INDC-1 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc1(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF USE-RPT-INDC-2 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc2(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (2) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(2));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF DB2-C1-CALC-EXPLN-CD = 'A'
			//              MOVE ZERO TO EXP-AMOUNT-PAID
			//           ELSE
			//              MOVE DB2-C1-PD-AM TO EXP-AMOUNT-PAID
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02811Calc1Info().getCalcExplnCd() == 'A') {
				// COB_CODE: MOVE ZERO TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(new AfDecimal(0, 11, 2));
			} else {
				// COB_CODE: MOVE DB2-C1-PD-AM TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc1Info().getPdAm(), 11, 2));
			}
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//               MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//               MOVE DB2-C1-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE DB2-C1-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getEobDb2CursorArea().getS02811Calc1Info().getGlAcctId());
			}
			// COB_CODE: MOVE DB2-C1-CORP-CD         TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getEobDb2CursorArea().getS02811Calc1Info().getCorpCd());
			// COB_CODE: MOVE DB2-C1-PROD-CD         TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getEobDb2CursorArea().getS02811Calc1Info().getProdCd());
			// COB_CODE: MOVE DB2-C1-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getEobDb2CursorArea().getS02811Calc1Info().getCalcExplnCd());
			// COB_CODE: MOVE '1'                    TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("1");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF DB2-C2-PD-AM > 0
		//               PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02811Calc2Info().getPdAm().compareTo(0) > 0) {
			// COB_CODE: IF USE-RPT-INDC-1 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc1(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF USE-RPT-INDC-2 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc2(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (2) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(2));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF DB2-C2-CALC-EXPLN-CD = 'A'
			//              MOVE ZERO TO EXP-AMOUNT-PAID
			//           ELSE
			//              MOVE DB2-C2-PD-AM TO EXP-AMOUNT-PAID
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02811Calc2Info().getCalcExplnCd() == 'A') {
				// COB_CODE: MOVE ZERO TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(new AfDecimal(0, 11, 2));
			} else {
				// COB_CODE: MOVE DB2-C2-PD-AM TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc2Info().getPdAm(), 11, 2));
			}
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//               MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//               MOVE DB2-C2-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE DB2-C2-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getEobDb2CursorArea().getS02811Calc2Info().getGlAcctId());
			}
			// COB_CODE: MOVE DB2-C2-CORP-CD         TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getEobDb2CursorArea().getS02811Calc2Info().getCorpCd());
			// COB_CODE: MOVE DB2-C2-PROD-CD         TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getEobDb2CursorArea().getS02811Calc2Info().getProdCd());
			// COB_CODE: MOVE DB2-C2-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getEobDb2CursorArea().getS02811Calc2Info().getCalcExplnCd());
			// COB_CODE: MOVE '2'                    TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("2");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF DB2-C3-PD-AM > 0
		//               PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02811Calc3Info().getPdAm().compareTo(0) > 0) {
			// COB_CODE: IF USE-RPT-INDC-1 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc1(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF USE-RPT-INDC-2 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc2(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (2) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(2));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF DB2-C3-CALC-EXPLN-CD = 'A'
			//              MOVE ZERO TO EXP-AMOUNT-PAID
			//           ELSE
			//              MOVE DB2-C3-PD-AM TO EXP-AMOUNT-PAID
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02811Calc3Info().getCalcExplnCd() == 'A') {
				// COB_CODE: MOVE ZERO TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(new AfDecimal(0, 11, 2));
			} else {
				// COB_CODE: MOVE DB2-C3-PD-AM TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc3Info().getPdAm(), 11, 2));
			}
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//               MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//               MOVE DB2-C3-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE DB2-C3-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getEobDb2CursorArea().getS02811Calc3Info().getGlAcctId());
			}
			// COB_CODE: MOVE DB2-C3-CORP-CD         TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getEobDb2CursorArea().getS02811Calc3Info().getCorpCd());
			// COB_CODE: MOVE DB2-C3-PROD-CD         TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getEobDb2CursorArea().getS02811Calc3Info().getProdCd());
			// COB_CODE: MOVE DB2-C3-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getEobDb2CursorArea().getS02811Calc3Info().getCalcExplnCd());
			// COB_CODE: MOVE '3'                    TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("3");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF DB2-C4-PD-AM > 0
		//               PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02811Calc4Info().getPdAm().compareTo(0) > 0) {
			// COB_CODE: IF USE-RPT-INDC-1 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc1(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF USE-RPT-INDC-2 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc2(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (2) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(2));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF DB2-C4-CALC-EXPLN-CD = 'A'
			//              MOVE ZERO TO EXP-AMOUNT-PAID
			//           ELSE
			//              MOVE DB2-C4-PD-AM TO EXP-AMOUNT-PAID
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02811Calc4Info().getCalcExplnCd() == 'A') {
				// COB_CODE: MOVE ZERO TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(new AfDecimal(0, 11, 2));
			} else {
				// COB_CODE: MOVE DB2-C4-PD-AM TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc4Info().getPdAm(), 11, 2));
			}
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//               MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//               MOVE DB2-C4-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE DB2-C4-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getEobDb2CursorArea().getS02811Calc4Info().getGlAcctId());
			}
			// COB_CODE: MOVE DB2-C4-CORP-CD         TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getEobDb2CursorArea().getS02811Calc4Info().getCorpCd());
			// COB_CODE: MOVE DB2-C4-PROD-CD         TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getEobDb2CursorArea().getS02811Calc4Info().getProdCd());
			// COB_CODE: MOVE DB2-C4-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getEobDb2CursorArea().getS02811Calc4Info().getCalcExplnCd());
			// COB_CODE: MOVE '4'                    TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("4");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF DB2-C5-PD-AM > 0
		//               PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02811Calc5Info().getPdAm().compareTo(0) > 0) {
			// COB_CODE: IF USE-RPT-INDC-1 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc1(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF USE-RPT-INDC-2 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc2(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (2) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(2));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF DB2-C5-CALC-EXPLN-CD = 'A'
			//              MOVE ZERO TO EXP-AMOUNT-PAID
			//           ELSE
			//              MOVE DB2-C5-PD-AM TO EXP-AMOUNT-PAID
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02811Calc5Info().getCalcExplnCd() == 'A') {
				// COB_CODE: MOVE ZERO TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(new AfDecimal(0, 11, 2));
			} else {
				// COB_CODE: MOVE DB2-C5-PD-AM TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc5Info().getPdAm(), 11, 2));
			}
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//               MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//               MOVE DB2-C5-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE DB2-C5-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getEobDb2CursorArea().getS02811Calc5Info().getGlAcctId());
			}
			// COB_CODE: MOVE DB2-C5-CORP-CD         TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getEobDb2CursorArea().getS02811Calc5Info().getCorpCd());
			// COB_CODE: MOVE DB2-C5-PROD-CD         TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getEobDb2CursorArea().getS02811Calc5Info().getProdCd());
			// COB_CODE: MOVE DB2-C5-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getEobDb2CursorArea().getS02811Calc5Info().getCalcExplnCd());
			// COB_CODE: MOVE '5'                    TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("5");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF DB2-C6-PD-AM > 0
		//               PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02811Calc6Info().getPdAm().compareTo(0) > 0) {
			// COB_CODE: IF USE-RPT-INDC-1 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc1(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF USE-RPT-INDC-2 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc2(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (2) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(2));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF DB2-C6-CALC-EXPLN-CD = 'A'
			//              MOVE ZERO TO EXP-AMOUNT-PAID
			//           ELSE
			//              MOVE DB2-C6-PD-AM TO EXP-AMOUNT-PAID
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02811Calc6Info().getCalcExplnCd() == 'A') {
				// COB_CODE: MOVE ZERO TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(new AfDecimal(0, 11, 2));
			} else {
				// COB_CODE: MOVE DB2-C6-PD-AM TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc6Info().getPdAm(), 11, 2));
			}
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//               MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//               MOVE DB2-C6-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE DB2-C6-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getEobDb2CursorArea().getS02811Calc6Info().getGlAcctId());
			}
			// COB_CODE: MOVE DB2-C6-CORP-CD         TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getEobDb2CursorArea().getS02811Calc6Info().getCorpCd());
			// COB_CODE: MOVE DB2-C6-PROD-CD         TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getEobDb2CursorArea().getS02811Calc6Info().getProdCd());
			// COB_CODE: MOVE DB2-C6-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getEobDb2CursorArea().getS02811Calc6Info().getCalcExplnCd());
			// COB_CODE: MOVE '6'                    TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("6");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF DB2-C7-PD-AM > 0
		//               PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02811Calc7Info().getPdAm().compareTo(0) > 0) {
			// COB_CODE: IF USE-RPT-INDC-1 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc1(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF USE-RPT-INDC-2 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc2(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (2) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(2));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF DB2-C7-CALC-EXPLN-CD = 'A'
			//              MOVE ZERO TO EXP-AMOUNT-PAID
			//           ELSE
			//              MOVE DB2-C7-PD-AM TO EXP-AMOUNT-PAID
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02811Calc7Info().getCalcExplnCd() == 'A') {
				// COB_CODE: MOVE ZERO TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(new AfDecimal(0, 11, 2));
			} else {
				// COB_CODE: MOVE DB2-C7-PD-AM TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc7Info().getPdAm(), 11, 2));
			}
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//               MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//               MOVE DB2-C7-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE DB2-C7-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getEobDb2CursorArea().getS02811Calc7Info().getGlAcctId());
			}
			// COB_CODE: MOVE DB2-C7-CORP-CD         TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getEobDb2CursorArea().getS02811Calc7Info().getCorpCd());
			// COB_CODE: MOVE DB2-C7-PROD-CD         TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getEobDb2CursorArea().getS02811Calc7Info().getProdCd());
			// COB_CODE: MOVE DB2-C7-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getEobDb2CursorArea().getS02811Calc7Info().getCalcExplnCd());
			// COB_CODE: MOVE '7'                    TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("7");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF DB2-C8-PD-AM > 0
		//               PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02811Calc8Info().getPdAm().compareTo(0) > 0) {
			// COB_CODE: IF USE-RPT-INDC-1 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc1(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF USE-RPT-INDC-2 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc2(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (2) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(2));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF DB2-C8-CALC-EXPLN-CD = 'A'
			//              MOVE ZERO TO EXP-AMOUNT-PAID
			//           ELSE
			//              MOVE DB2-C8-PD-AM TO EXP-AMOUNT-PAID
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02811Calc8Info().getCalcExplnCd() == 'A') {
				// COB_CODE: MOVE ZERO TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(new AfDecimal(0, 11, 2));
			} else {
				// COB_CODE: MOVE DB2-C8-PD-AM TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc8Info().getPdAm(), 11, 2));
			}
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//               MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//               MOVE DB2-C8-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE DB2-C8-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getEobDb2CursorArea().getS02811Calc8Info().getGlAcctId());
			}
			// COB_CODE: MOVE DB2-C8-CORP-CD         TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getEobDb2CursorArea().getS02811Calc8Info().getCorpCd());
			// COB_CODE: MOVE DB2-C8-PROD-CD         TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getEobDb2CursorArea().getS02811Calc8Info().getProdCd());
			// COB_CODE: MOVE DB2-C8-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getEobDb2CursorArea().getS02811Calc8Info().getCalcExplnCd());
			// COB_CODE: MOVE '8'                    TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("8");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF DB2-C9-PD-AM > 0
		//               PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02811Calc9Info().getPdAm().compareTo(0) > 0) {
			// COB_CODE: IF USE-RPT-INDC-1 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc1(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF USE-RPT-INDC-2 = 'YES'
			//               MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			//           END-IF
			if (Conditions.eq(ws.getWsSwitches().getUseRptIndc2(), "YES")) {
				// COB_CODE: MOVE SBR-REPORT-INDC (2) TO EXP-REPORT-INDC
				ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(2));
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
				ws.getExpenseRecord().setExpAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			}
			// COB_CODE: IF DB2-C9-CALC-EXPLN-CD = 'A'
			//              MOVE ZERO TO EXP-AMOUNT-PAID
			//           ELSE
			//              MOVE DB2-C9-PD-AM TO EXP-AMOUNT-PAID
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02811Calc9Info().getCalcExplnCd() == 'A') {
				// COB_CODE: MOVE ZERO TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(new AfDecimal(0, 11, 2));
			} else {
				// COB_CODE: MOVE DB2-C9-PD-AM TO EXP-AMOUNT-PAID
				ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc9Info().getPdAm(), 11, 2));
			}
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//               MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//               MOVE DB2-C9-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE DB2-C9-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getEobDb2CursorArea().getS02811Calc9Info().getGlAcctId());
			}
			// COB_CODE: MOVE DB2-C9-CORP-CD         TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getEobDb2CursorArea().getS02811Calc9Info().getCorpCd());
			// COB_CODE: MOVE DB2-C9-PROD-CD         TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getEobDb2CursorArea().getS02811Calc9Info().getProdCd());
			// COB_CODE: MOVE DB2-C9-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getEobDb2CursorArea().getS02811Calc9Info().getCalcExplnCd());
			// COB_CODE: MOVE '9'                    TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("9");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
	}

	/**Original name: 3200-PROMPT-PAY-INTEREST<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 3200-PROMPT-PAY-INTEREST **'.
	 *                                                             *</pre>*/
	private void promptPayInterest() {
		// COB_CODE: MOVE 'NO ' TO PROMPT-PAYMENT-CALC.
		ws.getWsSwitches().setPromptPaymentCalc("NO ");
		// COB_CODE: MOVE SPACES TO PP-EXP-DIVISION-CODE.
		ws.getWsSwitches().setPpExpDivisionCode(Types.SPACE_CHAR);
		// COB_CODE: MOVE 0 TO CLAIM-AGE.
		ws.getWsPromptPayWorkFields().setClaimAge(0);
		// COB_CODE: MOVE DB2-PMT-PRMPT-PAY-DAY    TO HOLD-PRMPT-PAY-DAY-CD.
		ws.getWsPromptPayWorkFields().getHoldPrmptPayDayCd()
				.setHoldPrmptPayDayCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPrmptPayDay());
		// COB_CODE: IF HOLD-PRMPT-PAY-DAY-CD IS NUMERIC
		//               END-IF
		//           END-IF.
		if (Functions.isNumber(ws.getWsPromptPayWorkFields().getHoldPrmptPayDayCd().getHoldPrmptPayDayCdFormatted())) {
			// COB_CODE: MOVE HOLD-PRMPT-PAY-DAY-CD-9 TO PROMPT-PAY-DAYS
			ws.getWsPromptPayWorkFields().setPromptPayDays(ws.getWsPromptPayWorkFields().getHoldPrmptPayDayCd().getHoldPrmptPayDayCd9());
			// COB_CODE: MOVE DB2-PMT-CORP-RCV-DT TO WS-CORP-DATE-DB2
			ws.getWsDateFields().getWsCorpDateDb2()
					.setWsCorpDateDb2Formatted(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtCorpRcvDtFormatted());
			// COB_CODE: MOVE WS-CORP-CCYY TO WS-CORP-CCYY-N
			ws.getWsDateFields().getWsCorpDateN().setCcyyNFormatted(ws.getWsDateFields().getWsCorpDateDb2().getCcyyFormatted());
			// COB_CODE: MOVE WS-CORP-MM   TO WS-CORP-MM-N
			ws.getWsDateFields().getWsCorpDateN().setMmNFormatted(ws.getWsDateFields().getWsCorpDateDb2().getMmFormatted());
			// COB_CODE: MOVE WS-CORP-DD   TO WS-CORP-DD-N
			ws.getWsDateFields().getWsCorpDateN().setDdNFormatted(ws.getWsDateFields().getWsCorpDateDb2().getDdFormatted());
			// COB_CODE: COMPUTE CLAIM-AGE =
			//              (FUNCTION INTEGER-OF-DATE (WS-BUSINESS-DATE-9))
			//             -(FUNCTION INTEGER-OF-DATE (WS-CORP-DATE-9))
			//           END-COMPUTE
			ws.getWsPromptPayWorkFields()
					.setClaimAge(Trunc.toInt(DateFunctions.daysFromYYYYMMDD(ws.getWsDateFields().getFillerWsDateFields().getWsBusinessDate9())
							- DateFunctions.daysFromYYYYMMDD(ws.getWsDateFields().getWsCorpDateN().getWsCorpDate9()), 5));
			// COB_CODE: IF CLAIM-AGE > PROMPT-PAY-DAYS
			//             END-IF
			//           END-IF
			if (ws.getWsPromptPayWorkFields().getClaimAge() > ws.getWsPromptPayWorkFields().getPromptPayDays()) {
				// COB_CODE: MOVE 'YES' TO PROMPT-PAYMENT-CALC
				ws.getWsSwitches().setPromptPaymentCalc("YES");
				// COB_CODE: IF DB2-PMT-CLM-CORP-CODE = ' ' OR LOW-VALUES
				//              MOVE DB2-C1-CORP-CD TO PP-EXP-DIVISION-CODE
				//           ELSE
				//              MOVE DB2-PMT-CLM-CORP-CODE TO PP-EXP-DIVISION-CODE
				//           END-IF
				if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmCorpCode() == ' '
						|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmCorpCode(), Types.LOW_CHAR_VAL)) {
					// COB_CODE: MOVE DB2-C1-CORP-CD TO PP-EXP-DIVISION-CODE
					ws.getWsSwitches().setPpExpDivisionCode(ws.getEobDb2CursorArea().getS02811Calc1Info().getCorpCd());
				} else {
					// COB_CODE: MOVE DB2-PMT-CLM-CORP-CODE TO PP-EXP-DIVISION-CODE
					ws.getWsSwitches().setPpExpDivisionCode(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmCorpCode());
				}
			}
		}
		// COB_CODE: MOVE 0 TO CLAIM-LINE-PAYMENTS.
		ws.getWsPromptPayWorkFields().setClaimLinePayments(Trunc.toDecimal(0, 11, 2));
		// COB_CODE: MOVE 0 TO CLAIM-LINE-DENIALS.
		ws.getWsPromptPayWorkFields().setClaimLineDenials(Trunc.toDecimal(0, 11, 2));
		// COB_CODE: MOVE 0 TO CLAIM-LINE-INTEREST.
		ws.getWsPromptPayWorkFields().setClaimLineInterest(Trunc.toDecimal(0, 11, 2));
	}

	/**Original name: 3201-PROMPT-PAY-LINE-AMT<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 3201-PROMPT-PAY-LINE-AMT **'.
	 *                                                             *</pre>*/
	private void promptPayLineAmt() {
		// COB_CODE: IF PROMPT-PAYMENT-CALC = 'YES'
		//               END-EVALUATE
		//           END-IF.
		if (Conditions.eq(ws.getWsSwitches().getPromptPaymentCalc(), "YES")) {
			// COB_CODE: EVALUATE DB2-LI-EXMN-ACTN-CD
			//              WHEN ' '
			//                 END-COMPUTE
			//              WHEN 'M'
			//                 END-COMPUTE
			//              WHEN 'D'
			//                 END-IF
			//            END-EVALUATE
			switch (ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd()) {

			case ' ':// COB_CODE: COMPUTE CLAIM-LINE-PAYMENTS =
				//                   CLAIM-LINE-PAYMENTS + WS-LINE-ACCUM-PAID
				//           END-COMPUTE
				ws.getWsPromptPayWorkFields().setClaimLinePayments(Trunc.toDecimal(
						ws.getWsPromptPayWorkFields().getClaimLinePayments().add(ws.getWsRollLogicWork().getLineAccumulators().getPaid()), 11, 2));
				break;

			case 'M':// COB_CODE: COMPUTE CLAIM-LINE-PAYMENTS =
				//                   CLAIM-LINE-PAYMENTS + WS-LINE-ACCUM-PAID
				//           END-COMPUTE
				ws.getWsPromptPayWorkFields().setClaimLinePayments(Trunc.toDecimal(
						ws.getWsPromptPayWorkFields().getClaimLinePayments().add(ws.getWsRollLogicWork().getLineAccumulators().getPaid()), 11, 2));
				break;

			case 'D':// COB_CODE: PERFORM 3210-READ-REMARK-TABLE THRU 3210-EXIT
				readRemarkTable();
				// COB_CODE: IF PRMPT-PAY-DNL-IN = 'Y'
				//              END-COMPUTE
				//           END-IF
				if (ws.getDcls02993sa().getPrmptPayDnlIn() == 'Y') {
					// COB_CODE: COMPUTE CLAIM-LINE-DENIALS =
					//                   CLAIM-LINE-DENIALS + DB2-LI-CHG-AM
					//           END-COMPUTE
					ws.getWsPromptPayWorkFields().setClaimLineDenials(Trunc.toDecimal(
							ws.getWsPromptPayWorkFields().getClaimLineDenials().add(ws.getEobDb2CursorArea().getS02809LineInfo().getChgAm()), 11, 2));
				}
				break;

			default:
				break;
			}
		}
	}

	/**Original name: 3202-PROMPT-PAY-INT-CALC<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 3202-PROMPT-PAY-INT-CALC **'.
	 *                                                             *</pre>*/
	private void promptPayIntCalc() {
		// COB_CODE: MOVE HOLD-ACR-PRMT-PA-INT-AM TO
		//                ACCRUED-SUB-INTEREST.
		ws.getWsPromptPayWorkFields().setAccruedSubInterest(Trunc.toDecimal(ws.getWsNf0533WorkArea().getHoldAcrPrmtPaIntAm(), 11, 2));
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE:       IF PROMPT-PAYMENT-CALC = 'YES'
		//           ******    DISPLAY 'COMPUTING INTEREST DAYS/INTEREST/DENIAL'
		//                     END-COMPUTE
		//                 END-IF.
		if (Conditions.eq(ws.getWsSwitches().getPromptPaymentCalc(), "YES")) {
			//*****    DISPLAY 'COMPUTING INTEREST DAYS/INTEREST/DENIAL'
			// COB_CODE: COMPUTE INTEREST-DAYS-DUE =
			//                     (CLAIM-AGE - PROMPT-PAY-DAYS)
			//           END-COMPUTE
			ws.getWorkFields()
					.setInterestDaysDue(abs(ws.getWsPromptPayWorkFields().getClaimAge() - ws.getWsPromptPayWorkFields().getPromptPayDays()));
			// COB_CODE: COMPUTE LATE-PAYMENT-INTEREST ROUNDED =
			//               (CLAIM-LINE-PAYMENTS * DAILY-INTEREST-RATE)
			//                  * INTEREST-DAYS-DUE
			//           END-COMPUTE
			ws.getWsPromptPayWorkFields()
					.setLatePaymentInterest(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getWsPromptPayWorkFields().getClaimLinePayments()
							.multiply(ws.getWorkFields().getDailyInterestRate()).multiply(ws.getWorkFields().getInterestDaysDue())), 2,
							RoundingMode.ROUND_UP, 31, 2), 9, 2));
			// COB_CODE: COMPUTE LATE-DENIAL-INTEREST ROUNDED =
			//                (CLAIM-LINE-DENIALS * DAILY-INTEREST-RATE)
			//                  * INTEREST-DAYS-DUE
			//           END-COMPUTE
			ws.getWsPromptPayWorkFields()
					.setLateDenialInterest(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getWsPromptPayWorkFields().getClaimLineDenials()
							.multiply(ws.getWorkFields().getDailyInterestRate()).multiply(ws.getWorkFields().getInterestDaysDue())), 2,
							RoundingMode.ROUND_UP, 31, 2), 9, 2));
			// COB_CODE: COMPUTE CLAIM-LINE-INTEREST =
			//               LATE-PAYMENT-INTEREST + LATE-DENIAL-INTEREST
			//           END-COMPUTE
			ws.getWsPromptPayWorkFields().setClaimLineInterest(Trunc.toDecimal(
					ws.getWsPromptPayWorkFields().getLatePaymentInterest().add(ws.getWsPromptPayWorkFields().getLateDenialInterest()), 11, 2));
			// COB_CODE: COMPUTE SUBSCRIBER-INTEREST = SUBSCRIBER-INTEREST
			//                                       + CLAIM-LINE-INTEREST
			//           END-COMPUTE
			ws.getWsPromptPayWorkFields().setSubscriberInterest(Trunc.toDecimal(
					ws.getWsPromptPayWorkFields().getSubscriberInterest().add(ws.getWsPromptPayWorkFields().getClaimLineInterest()), 11, 2));
			// COB_CODE: COMPUTE ACCRUED-SUB-INTEREST =
			//                   ACCRUED-SUB-INTEREST
			//                 + SUBSCRIBER-INTEREST
			//           END-COMPUTE
			ws.getWsPromptPayWorkFields().setAccruedSubInterest(Trunc.toDecimal(
					ws.getWsPromptPayWorkFields().getAccruedSubInterest().add(ws.getWsPromptPayWorkFields().getSubscriberInterest()), 11, 2));
		}
		// COB_CODE: ADD CLAIM-LINE-INTEREST         TO TOT-TOTAL-INTEREST.
		ws.getWsAllCounters().setTotTotalInterest(
				Trunc.toDecimal(ws.getWsPromptPayWorkFields().getClaimLineInterest().add(ws.getWsAllCounters().getTotTotalInterest()), 11, 2));
		// COB_CODE: ADD LATE-PAYMENT-INTEREST       TO TOT-LATE-PMT-INTEREST.
		ws.getWsAllCounters().setTotLatePmtInterest(
				Trunc.toDecimal(ws.getWsPromptPayWorkFields().getLatePaymentInterest().add(ws.getWsAllCounters().getTotLatePmtInterest()), 11, 2));
		// COB_CODE: ADD LATE-DENIAL-INTEREST        TO TOT-LATE-DENIAL-INTEREST.
		ws.getWsAllCounters().setTotLateDenialInterest(
				Trunc.toDecimal(ws.getWsPromptPayWorkFields().getLateDenialInterest().add(ws.getWsAllCounters().getTotLateDenialInterest()), 11, 2));
		// COB_CODE: MOVE 'B '                        TO EXP-GEN-LDGR-OFFSET
		ws.getExpenseRecord().setExpGenLdgrOffset("B ");
		// COB_CODE: MOVE CLAIM-LINE-INTEREST         TO EXP-AMOUNT-PAID.
		ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getWsPromptPayWorkFields().getClaimLineInterest(), 11, 2));
		// COB_CODE: MOVE 84750100                    TO EXP-ACCOUNT-NUM.
		ws.getExpenseRecord().setExpAccountNum("84750100");
		// COB_CODE: MOVE SPACE                       TO EXP-ADJUSTMENT-TYPE.
		ws.getExpenseRecord().setExpAdjustmentType(Types.SPACE_CHAR);
		// COB_CODE:       IF EXP-AMOUNT-PAID > 0
		//           *        EVALUATE PP-EXP-DIVISION-CODE
		//           *           WHEN 'G'
		//           *               ADD 1 TO OUT-KSS-INT-CNTR
		//           *               MOVE 'I'  TO EXP-CALC-TYPE
		//           *               PERFORM 8605-WRITE-INT-EXP-FILE THRU 8605-EXIT
		//           *           WHEN OTHER
		//                     PERFORM 8605-WRITE-INT-EXP-FILE THRU 8605-EXIT
		//           *        END-EVALUATE
		//                 END-IF.
		if (ws.getExpenseRecord().getExpAmountPaid().compareTo(0) > 0) {
			//        EVALUATE PP-EXP-DIVISION-CODE
			//           WHEN 'G'
			//               ADD 1 TO OUT-KSS-INT-CNTR
			//               MOVE 'I'  TO EXP-CALC-TYPE
			//               PERFORM 8605-WRITE-INT-EXP-FILE THRU 8605-EXIT
			//           WHEN OTHER
			// COB_CODE: ADD 1 TO OUT-INTEREST-CNTR
			ws.getWsAllCounters().setOutInterestCntr(Trunc.toInt(1 + ws.getWsAllCounters().getOutInterestCntr(), 7));
			// COB_CODE: MOVE 'I'  TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("I");
			// COB_CODE: PERFORM 8605-WRITE-INT-EXP-FILE THRU 8605-EXIT
			writeIntExpFile();
			//        END-EVALUATE
		}
	}

	/**Original name: 3210-READ-REMARK-TABLE<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 3210-READ-REMARK-TABLE **'.
	 *                                                             *</pre>*/
	private void readRemarkTable() {
		// COB_CODE: EXEC SQL
		//             SELECT DNL.PRMPT_PAY_DNL_IN
		//              INTO :DCLS02993SA.PRMPT-PAY-DNL-IN
		//              FROM S02993SA AS DNL
		//              WHERE DNL.DNL_RMRK_CD      = :DB2-LI-RMRK-CD
		//            END-EXEC.
		ws.getDcls02993sa().setPrmptPayDnlIn(
				s02993saDao.selectByDb2LiRmrkCd(ws.getEobDb2CursorArea().getS02809LineInfo().getRmrkCd(), ws.getDcls02993sa().getPrmptPayDnlIn()));
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 CONTINUE
		//             WHEN +100
		//                 MOVE 'N' TO PRMPT-PAY-DNL-IN
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		case 100:// COB_CODE: MOVE 'N' TO PRMPT-PAY-DNL-IN
			ws.getDcls02993sa().setPrmptPayDnlInFormatted("N");
			break;

		default:// COB_CODE: MOVE 'S02993SA DNL RMRK '     TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S02993SA DNL RMRK ");
			// COB_CODE: MOVE 'SELECT DNL RMRK   '     TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("SELECT DNL RMRK   ");
			// COB_CODE: MOVE '3210-READ-REMARK-TABLE' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("3210-READ-REMARK-TABLE");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 3300-CALCULATE-CHECKS<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 3300-CALCULATE-CHECKS **'.
	 *                                                             *</pre>*/
	private void calculateChecks() {
		// COB_CODE: IF VOID-REPAY
		//              SUBTRACT VOID-C9-PD-AM FROM TOTAL-CHECK-AMOUNT
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getWsSwitches().isVoidRepay()) {
			// COB_CODE: SUBTRACT VOID-C1-PD-AM FROM TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getWsPromptPayWorkFields().getTotalCheckAmount().subtract(ws.getVoidInformation().getVoidS02811Calc1Info().getC1PdAm()), 11,
					2));
			// COB_CODE: SUBTRACT VOID-C2-PD-AM FROM TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getWsPromptPayWorkFields().getTotalCheckAmount().subtract(ws.getVoidInformation().getVoidS02811Calc2Info().getC1PdAm()), 11,
					2));
			// COB_CODE: SUBTRACT VOID-C3-PD-AM FROM TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getWsPromptPayWorkFields().getTotalCheckAmount().subtract(ws.getVoidInformation().getVoidS02811Calc3Info().getC1PdAm()), 11,
					2));
			// COB_CODE: SUBTRACT VOID-C4-PD-AM FROM TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getWsPromptPayWorkFields().getTotalCheckAmount().subtract(ws.getVoidInformation().getVoidS02811Calc4Info().getC1PdAm()), 11,
					2));
			// COB_CODE: SUBTRACT VOID-C5-PD-AM FROM TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getWsPromptPayWorkFields().getTotalCheckAmount().subtract(ws.getVoidInformation().getVoidS02811Calc5Info().getC1PdAm()), 11,
					2));
			// COB_CODE: SUBTRACT VOID-C6-PD-AM FROM TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getWsPromptPayWorkFields().getTotalCheckAmount().subtract(ws.getVoidInformation().getVoidS02811Calc6Info().getC1PdAm()), 11,
					2));
			// COB_CODE: SUBTRACT VOID-C7-PD-AM FROM TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getWsPromptPayWorkFields().getTotalCheckAmount().subtract(ws.getVoidInformation().getVoidS02811Calc7Info().getC1PdAm()), 11,
					2));
			// COB_CODE: SUBTRACT VOID-C8-PD-AM FROM TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getWsPromptPayWorkFields().getTotalCheckAmount().subtract(ws.getVoidInformation().getVoidS02811Calc8Info().getC1PdAm()), 11,
					2));
			// COB_CODE: SUBTRACT VOID-C9-PD-AM FROM TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getWsPromptPayWorkFields().getTotalCheckAmount().subtract(ws.getVoidInformation().getVoidS02811Calc9Info().getC1PdAm()), 11,
					2));
		} else if (ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() == 'M'
				|| ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() == ' ') {
			// COB_CODE: IF DB2-LI-EXMN-ACTN-CD = 'M' OR
			//              DB2-LI-EXMN-ACTN-CD = ' '
			//                 ADD DB2-C9-PD-AM TO TOTAL-CHECK-AMOUNT
			//           END-IF
			// COB_CODE: ADD DB2-C1-PD-AM TO TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getEobDb2CursorArea().getS02811Calc1Info().getPdAm().add(ws.getWsPromptPayWorkFields().getTotalCheckAmount()), 11, 2));
			// COB_CODE: ADD DB2-C2-PD-AM TO TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getEobDb2CursorArea().getS02811Calc2Info().getPdAm().add(ws.getWsPromptPayWorkFields().getTotalCheckAmount()), 11, 2));
			// COB_CODE: ADD DB2-C3-PD-AM TO TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getEobDb2CursorArea().getS02811Calc3Info().getPdAm().add(ws.getWsPromptPayWorkFields().getTotalCheckAmount()), 11, 2));
			// COB_CODE: ADD DB2-C4-PD-AM TO TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getEobDb2CursorArea().getS02811Calc4Info().getPdAm().add(ws.getWsPromptPayWorkFields().getTotalCheckAmount()), 11, 2));
			// COB_CODE: ADD DB2-C5-PD-AM TO TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getEobDb2CursorArea().getS02811Calc5Info().getPdAm().add(ws.getWsPromptPayWorkFields().getTotalCheckAmount()), 11, 2));
			// COB_CODE: ADD DB2-C6-PD-AM TO TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getEobDb2CursorArea().getS02811Calc6Info().getPdAm().add(ws.getWsPromptPayWorkFields().getTotalCheckAmount()), 11, 2));
			// COB_CODE: ADD DB2-C7-PD-AM TO TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getEobDb2CursorArea().getS02811Calc7Info().getPdAm().add(ws.getWsPromptPayWorkFields().getTotalCheckAmount()), 11, 2));
			// COB_CODE: ADD DB2-C8-PD-AM TO TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getEobDb2CursorArea().getS02811Calc8Info().getPdAm().add(ws.getWsPromptPayWorkFields().getTotalCheckAmount()), 11, 2));
			// COB_CODE: ADD DB2-C9-PD-AM TO TOTAL-CHECK-AMOUNT
			ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc.toDecimal(
					ws.getEobDb2CursorArea().getS02811Calc9Info().getPdAm().add(ws.getWsPromptPayWorkFields().getTotalCheckAmount()), 11, 2));
		}
	}

	/**Original name: 3400-CREATE-VOID-EXP-RECS<br>
	 * <pre>    DISPLAY '** 3400-CREATE-VOID-EXP-RECS **'.</pre>*/
	private void createVoidExpRecs() {
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: MOVE 'YES' TO USE-RPT-INDC-1.
		ws.getWsSwitches().setUseRptIndc1("YES");
		// COB_CODE: IF VOID-C1-PD-AM > 0
		//              PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getVoidInformation().getVoidS02811Calc1Info().getC1PdAm().compareTo(0) > 0) {
			// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
			ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
			// COB_CODE: MOVE VOID-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			ws.getExpenseRecord().setExpAdjustmentType(ws.getVoidInformation().getVoidS02813Info().getPmtAdjTypCd());
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//              MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//              MOVE VOID-C1-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE VOID-C1-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getVoidInformation().getVoidS02811Calc1Info().getC1GlAcctId());
			}
			// COB_CODE: MOVE VOID-C1-PD-AM           TO EXP-AMOUNT-PAID
			ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getVoidInformation().getVoidS02811Calc1Info().getC1PdAm(), 11, 2));
			// COB_CODE: MOVE VOID-C1-CORP-CD          TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getVoidInformation().getVoidS02811Calc1Info().getC1CorpCd());
			// COB_CODE: MOVE VOID-C1-PROD-CD          TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getVoidInformation().getVoidS02811Calc1Info().getC1ProdCd());
			// COB_CODE: MOVE VOID-C1-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getVoidInformation().getVoidS02811Calc1Info().getC1CalcExplnCd());
			// COB_CODE: MOVE '1'                     TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("1");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF VOID-C2-PD-AM > 0
		//              PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getVoidInformation().getVoidS02811Calc2Info().getC1PdAm().compareTo(0) > 0) {
			// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
			ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
			// COB_CODE: MOVE VOID-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			ws.getExpenseRecord().setExpAdjustmentType(ws.getVoidInformation().getVoidS02813Info().getPmtAdjTypCd());
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//              MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//              MOVE VOID-C2-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE VOID-C2-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getVoidInformation().getVoidS02811Calc2Info().getC1GlAcctId());
			}
			// COB_CODE: MOVE VOID-C2-PD-AM           TO EXP-AMOUNT-PAID
			ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getVoidInformation().getVoidS02811Calc2Info().getC1PdAm(), 11, 2));
			// COB_CODE: MOVE VOID-C2-CORP-CD          TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getVoidInformation().getVoidS02811Calc2Info().getC1CorpCd());
			// COB_CODE: MOVE VOID-C2-PROD-CD          TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getVoidInformation().getVoidS02811Calc2Info().getC1ProdCd());
			// COB_CODE: MOVE VOID-C2-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getVoidInformation().getVoidS02811Calc2Info().getC1CalcExplnCd());
			// COB_CODE: MOVE '2'                     TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("2");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF VOID-C3-PD-AM > 0
		//              PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getVoidInformation().getVoidS02811Calc3Info().getC1PdAm().compareTo(0) > 0) {
			// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
			ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
			// COB_CODE: MOVE VOID-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			ws.getExpenseRecord().setExpAdjustmentType(ws.getVoidInformation().getVoidS02813Info().getPmtAdjTypCd());
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//              MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//              MOVE VOID-C3-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE VOID-C3-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getVoidInformation().getVoidS02811Calc3Info().getC1GlAcctId());
			}
			// COB_CODE: MOVE VOID-C3-PD-AM           TO EXP-AMOUNT-PAID
			ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getVoidInformation().getVoidS02811Calc3Info().getC1PdAm(), 11, 2));
			// COB_CODE: MOVE VOID-C3-CORP-CD          TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getVoidInformation().getVoidS02811Calc3Info().getC1CorpCd());
			// COB_CODE: MOVE VOID-C3-PROD-CD          TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getVoidInformation().getVoidS02811Calc3Info().getC1ProdCd());
			// COB_CODE: MOVE VOID-C3-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getVoidInformation().getVoidS02811Calc3Info().getC1CalcExplnCd());
			// COB_CODE: MOVE '3'                     TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("3");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF VOID-C4-PD-AM > 0
		//              PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getVoidInformation().getVoidS02811Calc4Info().getC1PdAm().compareTo(0) > 0) {
			// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
			ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
			// COB_CODE: MOVE VOID-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			ws.getExpenseRecord().setExpAdjustmentType(ws.getVoidInformation().getVoidS02813Info().getPmtAdjTypCd());
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//              MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//              MOVE VOID-C4-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE VOID-C4-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getVoidInformation().getVoidS02811Calc4Info().getC1GlAcctId());
			}
			// COB_CODE: MOVE VOID-C4-PD-AM           TO EXP-AMOUNT-PAID
			ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getVoidInformation().getVoidS02811Calc4Info().getC1PdAm(), 11, 2));
			// COB_CODE: MOVE VOID-C4-CORP-CD          TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getVoidInformation().getVoidS02811Calc4Info().getC1CorpCd());
			// COB_CODE: MOVE VOID-C4-PROD-CD          TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getVoidInformation().getVoidS02811Calc4Info().getC1ProdCd());
			// COB_CODE: MOVE VOID-C4-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getVoidInformation().getVoidS02811Calc4Info().getC1CalcExplnCd());
			// COB_CODE: MOVE '4'                     TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("4");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF VOID-C5-PD-AM > 0
		//              PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getVoidInformation().getVoidS02811Calc5Info().getC1PdAm().compareTo(0) > 0) {
			// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
			ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
			// COB_CODE: MOVE VOID-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			ws.getExpenseRecord().setExpAdjustmentType(ws.getVoidInformation().getVoidS02813Info().getPmtAdjTypCd());
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//              MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//              MOVE VOID-C5-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE VOID-C5-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getVoidInformation().getVoidS02811Calc5Info().getC1GlAcctId());
			}
			// COB_CODE: MOVE VOID-C5-PD-AM           TO EXP-AMOUNT-PAID
			ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getVoidInformation().getVoidS02811Calc5Info().getC1PdAm(), 11, 2));
			// COB_CODE: MOVE VOID-C5-CORP-CD          TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getVoidInformation().getVoidS02811Calc5Info().getC1CorpCd());
			// COB_CODE: MOVE VOID-C5-PROD-CD          TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getVoidInformation().getVoidS02811Calc5Info().getC1ProdCd());
			// COB_CODE: MOVE VOID-C5-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getVoidInformation().getVoidS02811Calc5Info().getC1CalcExplnCd());
			// COB_CODE: MOVE '5'                     TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("5");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF VOID-C6-PD-AM > 0
		//              PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getVoidInformation().getVoidS02811Calc6Info().getC1PdAm().compareTo(0) > 0) {
			// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
			ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
			// COB_CODE: MOVE VOID-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			ws.getExpenseRecord().setExpAdjustmentType(ws.getVoidInformation().getVoidS02813Info().getPmtAdjTypCd());
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//              MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//              MOVE VOID-C6-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE VOID-C6-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getVoidInformation().getVoidS02811Calc6Info().getC1GlAcctId());
			}
			// COB_CODE: MOVE VOID-C6-PD-AM           TO EXP-AMOUNT-PAID
			ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getVoidInformation().getVoidS02811Calc6Info().getC1PdAm(), 11, 2));
			// COB_CODE: MOVE VOID-C6-CORP-CD          TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getVoidInformation().getVoidS02811Calc6Info().getC1CorpCd());
			// COB_CODE: MOVE VOID-C6-PROD-CD          TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getVoidInformation().getVoidS02811Calc6Info().getC1ProdCd());
			// COB_CODE: MOVE VOID-C6-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getVoidInformation().getVoidS02811Calc6Info().getC1CalcExplnCd());
			// COB_CODE: MOVE '6'                     TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("6");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF VOID-C7-PD-AM > 0
		//              PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getVoidInformation().getVoidS02811Calc7Info().getC1PdAm().compareTo(0) > 0) {
			// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
			ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
			// COB_CODE: MOVE VOID-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			ws.getExpenseRecord().setExpAdjustmentType(ws.getVoidInformation().getVoidS02813Info().getPmtAdjTypCd());
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//              MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//              MOVE VOID-C7-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE VOID-C7-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getVoidInformation().getVoidS02811Calc7Info().getC1GlAcctId());
			}
			// COB_CODE: MOVE VOID-C7-PD-AM           TO EXP-AMOUNT-PAID
			ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getVoidInformation().getVoidS02811Calc7Info().getC1PdAm(), 11, 2));
			// COB_CODE: MOVE VOID-C7-CORP-CD          TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getVoidInformation().getVoidS02811Calc7Info().getC1CorpCd());
			// COB_CODE: MOVE VOID-C7-PROD-CD          TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getVoidInformation().getVoidS02811Calc7Info().getC1ProdCd());
			// COB_CODE: MOVE VOID-C7-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getVoidInformation().getVoidS02811Calc7Info().getC1CalcExplnCd());
			// COB_CODE: MOVE '7'                     TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("7");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF VOID-C8-PD-AM > 0
		//              PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getVoidInformation().getVoidS02811Calc8Info().getC1PdAm().compareTo(0) > 0) {
			// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
			ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
			// COB_CODE: MOVE VOID-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			ws.getExpenseRecord().setExpAdjustmentType(ws.getVoidInformation().getVoidS02813Info().getPmtAdjTypCd());
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//              MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//              MOVE VOID-C8-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE VOID-C8-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getVoidInformation().getVoidS02811Calc8Info().getC1GlAcctId());
			}
			// COB_CODE: MOVE VOID-C8-PD-AM           TO EXP-AMOUNT-PAID
			ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getVoidInformation().getVoidS02811Calc8Info().getC1PdAm(), 11, 2));
			// COB_CODE: MOVE VOID-C8-CORP-CD          TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getVoidInformation().getVoidS02811Calc8Info().getC1CorpCd());
			// COB_CODE: MOVE VOID-C8-PROD-CD          TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getVoidInformation().getVoidS02811Calc8Info().getC1ProdCd());
			// COB_CODE: MOVE VOID-C8-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getVoidInformation().getVoidS02811Calc8Info().getC1CalcExplnCd());
			// COB_CODE: MOVE '8'                     TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("8");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
		// COB_CODE: INITIALIZE WS-EXP-FILE-REC.
		initWsExpFileRec();
		// COB_CODE: IF VOID-C9-PD-AM > 0
		//              PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
		//           END-IF.
		if (ws.getVoidInformation().getVoidS02811Calc9Info().getC1PdAm().compareTo(0) > 0) {
			// COB_CODE: MOVE SBR-REPORT-INDC (1) TO EXP-REPORT-INDC
			ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted(ws.getWsSubroutine().getSbrReportIndcFormatted(1));
			// COB_CODE: MOVE VOID-PMT-ADJ-TYP-CD  TO EXP-ADJUSTMENT-TYPE
			ws.getExpenseRecord().setExpAdjustmentType(ws.getVoidInformation().getVoidS02813Info().getPmtAdjTypCd());
			// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
			//              MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
			//           ELSE
			//              MOVE VOID-C9-GL-ACCT-ID TO EXP-ACCOUNT-NUM
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
				// COB_CODE: MOVE GL-SOTE-NUM TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getWsConstants().getGlSoteNum());
			} else {
				// COB_CODE: MOVE VOID-C9-GL-ACCT-ID TO EXP-ACCOUNT-NUM
				ws.getExpenseRecord().setExpAccountNum(ws.getVoidInformation().getVoidS02811Calc9Info().getC1GlAcctId());
			}
			// COB_CODE: MOVE VOID-C9-PD-AM           TO EXP-AMOUNT-PAID
			ws.getExpenseRecord().setExpAmountPaid(Trunc.toDecimal(ws.getVoidInformation().getVoidS02811Calc9Info().getC1PdAm(), 11, 2));
			// COB_CODE: MOVE VOID-C9-CORP-CD          TO EXP-DIVISION-CODE
			ws.getExpenseRecord().setExpDivisionCode(ws.getVoidInformation().getVoidS02811Calc9Info().getC1CorpCd());
			// COB_CODE: MOVE VOID-C9-PROD-CD          TO EXP-PRODUCT-CODE
			ws.getExpenseRecord().setExpProductCode(ws.getVoidInformation().getVoidS02811Calc9Info().getC1ProdCd());
			// COB_CODE: MOVE VOID-C9-CALC-EXPLN-CD   TO EXP-EXPLANATION-CODE
			ws.getExpenseRecord().setExpExplanationCode(ws.getVoidInformation().getVoidS02811Calc9Info().getC1CalcExplnCd());
			// COB_CODE: MOVE '9'                     TO EXP-CALC-TYPE
			ws.getExpenseRecord().setExpCalcTypeFormatted("9");
			// COB_CODE: PERFORM 8600-WRITE-EXPENSE-FILE     THRU 8600-EXIT
			writeExpenseFile();
		}
	}

	/**Original name: 3500-SELECT-VOID-DATA<br>
	 * <pre>    DISPLAY '** 5400-SELECT-VOID-DATA **'.</pre>*/
	private void selectVoidData() {
		// COB_CODE: INITIALIZE VOID-INFORMATION.
		initVoidInformation();
		// COB_CODE: COMPUTE PREV-CLM-PMT-ID =
		//                   DB2-CLM-PMT-ID - 1
		//           END-COMPUTE.
		ws.getWsExpWorkItems().setPrevClmPmtId(Trunc.toShort(ws.getEobDb2CursorArea().getS02801Info().getClmPmtId() - 1, 2));
		// COB_CODE: EXEC SQL
		//             SELECT VPMT.CLM_CNTL_ID
		//                  , VPMT.CLM_CNTL_SFX_ID
		//                  , VPMT.CLM_PMT_ID
		//                  , VPMT.ADJ_TYP_CD
		//                  , VPMT.CLM_PD_AM
		//                  , VPMT.HIST_LOAD_CD
		//                  , VPMT.KCAPS_TEAM_NM
		//                  , VPMT.KCAPS_USE_ID
		//                  , VPMT.PGM_AREA_CD
		//                  , VPMT.ENR_CL_CD
		//                  , VPMT.FIN_CD
		//                  , VPMT.NTWRK_CD
		//                  , VLI.CLI_ID
		//                  , VLI.CHG_AM
		//                  , VLI.LI_PAT_RESP_AM
		//                  , VLI.LI_PWO_AM
		//                  , VLI.LI_ALCT_OI_PD_AM
		//                  , VLI.DNL_RMRK_CD
		//                  , VLI.LI_EXPLN_CD
		//                  , COALESCE(VC1.CALC_EXPLN_CD, ' ')
		//                  , COALESCE(VC1.PD_AM, 0)
		//                  , CASE
		//                       WHEN VPMT.CLM_GL_ACCT_ID > ' '
		//                            THEN VPMT.CLM_GL_ACCT_ID
		//                             ELSE
		//                                 VC1.GL_ACCT_ID
		//                    END
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_CORP_CD > ' '
		//                           THEN VPMT.CLM_CORP_CD
		//                           ELSE
		//                               VC1.CORP_CD
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_PROD_CD > ' '
		//                           THEN VPMT.CLM_PROD_CD
		//                           ELSE
		//                               VC1.PROD_CD
		//                    END,'')
		//                  , COALESCE(VC2.CALC_EXPLN_CD, ' ')
		//                  , COALESCE(VC2.PD_AM, 0)
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_GL_ACCT_ID > ' '
		//                            THEN VPMT.CLM_GL_ACCT_ID
		//                             ELSE
		//                                 VC2.GL_ACCT_ID
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_CORP_CD > ' '
		//                           THEN VPMT.CLM_CORP_CD
		//                           ELSE
		//                               VC2.CORP_CD
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_PROD_CD > ' '
		//                           THEN VPMT.CLM_PROD_CD
		//                           ELSE
		//                               VC2.PROD_CD
		//                    END,'')
		//                  , COALESCE(VC3.CALC_EXPLN_CD, ' ')
		//                  , COALESCE(VC3.PD_AM, 0)
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_GL_ACCT_ID > ' '
		//                            THEN VPMT.CLM_GL_ACCT_ID
		//                             ELSE
		//                                 VC3.GL_ACCT_ID
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_CORP_CD > ' '
		//                           THEN VPMT.CLM_CORP_CD
		//                           ELSE
		//                               VC3.CORP_CD
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_PROD_CD > ' '
		//                           THEN VPMT.CLM_PROD_CD
		//                           ELSE
		//                               VC3.PROD_CD
		//                    END,'')
		//                  , COALESCE(VC4.CALC_EXPLN_CD, ' ')
		//                  , COALESCE(VC4.PD_AM, 0)
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_GL_ACCT_ID > ' '
		//                            THEN VPMT.CLM_GL_ACCT_ID
		//                             ELSE
		//                                 VC4.GL_ACCT_ID
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_CORP_CD > ' '
		//                           THEN VPMT.CLM_CORP_CD
		//                           ELSE
		//                               VC4.CORP_CD
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_PROD_CD > ' '
		//                           THEN VPMT.CLM_PROD_CD
		//                           ELSE
		//                               VC4.PROD_CD
		//                    END,'')
		//                  , COALESCE(VC5.CALC_EXPLN_CD, ' ')
		//                  , COALESCE(VC5.PD_AM, 0)
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_GL_ACCT_ID > ' '
		//                            THEN VPMT.CLM_GL_ACCT_ID
		//                             ELSE
		//                                 VC5.GL_ACCT_ID
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_CORP_CD > ' '
		//                           THEN VPMT.CLM_CORP_CD
		//                           ELSE
		//                               VC5.CORP_CD
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_PROD_CD > ' '
		//                           THEN VPMT.CLM_PROD_CD
		//                           ELSE
		//                               VC5.PROD_CD
		//                    END,'')
		//                  , COALESCE(VC6.CALC_EXPLN_CD, ' ')
		//                  , COALESCE(VC6.PD_AM, 0)
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_GL_ACCT_ID > ' '
		//                            THEN VPMT.CLM_GL_ACCT_ID
		//                             ELSE
		//                                 VC6.GL_ACCT_ID
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_CORP_CD > ' '
		//                           THEN VPMT.CLM_CORP_CD
		//                           ELSE
		//                               VC6.CORP_CD
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_PROD_CD > ' '
		//                           THEN VPMT.CLM_PROD_CD
		//                           ELSE
		//                               VC6.PROD_CD
		//                    END,'')
		//                  , COALESCE(VC7.CALC_EXPLN_CD, ' ')
		//                  , COALESCE(VC7.PD_AM, 0)
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_GL_ACCT_ID > ' '
		//                            THEN VPMT.CLM_GL_ACCT_ID
		//                             ELSE
		//                                 VC7.GL_ACCT_ID
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_CORP_CD > ' '
		//                           THEN VPMT.CLM_CORP_CD
		//                           ELSE
		//                               VC7.CORP_CD
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_PROD_CD > ' '
		//                           THEN VPMT.CLM_PROD_CD
		//                           ELSE
		//                               VC7.PROD_CD
		//                    END,'')
		//                  , COALESCE(VC8.CALC_EXPLN_CD, ' ')
		//                  , COALESCE(VC8.PD_AM, 0)
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_GL_ACCT_ID > ' '
		//                            THEN VPMT.CLM_GL_ACCT_ID
		//                             ELSE
		//                                 VC8.GL_ACCT_ID
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_CORP_CD > ' '
		//                           THEN VPMT.CLM_CORP_CD
		//                           ELSE
		//                               VC8.CORP_CD
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_PROD_CD > ' '
		//                           THEN VPMT.CLM_PROD_CD
		//                           ELSE
		//                               VC8.PROD_CD
		//                    END,'')
		//                  , COALESCE(VC9.CALC_EXPLN_CD, ' ')
		//                  , COALESCE(VC9.PD_AM, 0)
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_GL_ACCT_ID > ' '
		//                            THEN VPMT.CLM_GL_ACCT_ID
		//                             ELSE
		//                                 VC9.GL_ACCT_ID
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_CORP_CD > ' '
		//                           THEN VPMT.CLM_CORP_CD
		//                           ELSE
		//                               VC9.CORP_CD
		//                    END,'')
		//                  , COALESCE(CASE
		//                       WHEN VPMT.CLM_PROD_CD > ' '
		//                           THEN VPMT.CLM_PROD_CD
		//                           ELSE
		//                               VC9.PROD_CD
		//                    END,'')
		//                  , COALESCE(VCAS.LI_NCOV_AM, 0)
		//                  , COALESCE(VCAS.LI_SOCP_DED_AM, 0)
		//                  , COALESCE(VCAS.LI_SOCP_COINS_AM, 0)
		//                  , COALESCE(VCAS.LI_SOCP_COPAY_AM, 0)
		//              INTO :VOID-CLM-CNTL-ID
		//                 , :VOID-CLM-CNTL-SFX-ID
		//                 , :VOID-CLM-PMT-ID
		//                 , :VOID-PMT-ADJ-TYP-CD
		//                 , :VOID-CLM-PD-AM
		//                 , :VOID-PMT-HIST-LOAD-CD
		//                 , :VOID-PMT-KCAPS-TEAM-NM
		//                 , :VOID-PMT-KCAPS-USE-ID
		//                 , :VOID-PMT-PGM-AREA-CD
		//                 , :VOID-PMT-ENR-CL-CD
		//                 , :VOID-PMT-FIN-CD
		//                 , :VOID-PMT-NTWRK-CD
		//                 , :VOID-CLI-ID
		//                 , :VOID-LI-CHG-AM
		//                 , :VOID-LI-PAT-RESP-AM
		//                 , :VOID-LI-PWO-AM
		//                 , :VOID-LI-ALCT-OI-PD-AM
		//                 , :VOID-LI-DNL-RMRK-CD
		//                 , :VOID-LI-EXPLN-CD
		//                 , :VOID-C1-CALC-EXPLN-CD
		//                 , :VOID-C1-PD-AM
		//                 , :VOID-C1-GL-ACCT-ID
		//                 , :VOID-C1-CORP-CD
		//                 , :VOID-C1-PROD-CD
		//                 , :VOID-C2-CALC-EXPLN-CD
		//                 , :VOID-C2-PD-AM
		//                 , :VOID-C2-GL-ACCT-ID
		//                 , :VOID-C2-CORP-CD
		//                 , :VOID-C2-PROD-CD
		//                 , :VOID-C3-CALC-EXPLN-CD
		//                 , :VOID-C3-PD-AM
		//                 , :VOID-C3-GL-ACCT-ID
		//                 , :VOID-C3-CORP-CD
		//                 , :VOID-C3-PROD-CD
		//                 , :VOID-C4-CALC-EXPLN-CD
		//                 , :VOID-C4-PD-AM
		//                 , :VOID-C4-GL-ACCT-ID
		//                 , :VOID-C4-CORP-CD
		//                 , :VOID-C4-PROD-CD
		//                 , :VOID-C5-CALC-EXPLN-CD
		//                 , :VOID-C5-PD-AM
		//                 , :VOID-C5-GL-ACCT-ID
		//                 , :VOID-C5-CORP-CD
		//                 , :VOID-C5-PROD-CD
		//                 , :VOID-C6-CALC-EXPLN-CD
		//                 , :VOID-C6-PD-AM
		//                 , :VOID-C6-GL-ACCT-ID
		//                 , :VOID-C6-CORP-CD
		//                 , :VOID-C6-PROD-CD
		//                 , :VOID-C7-CALC-EXPLN-CD
		//                 , :VOID-C7-PD-AM
		//                 , :VOID-C7-GL-ACCT-ID
		//                 , :VOID-C7-CORP-CD
		//                 , :VOID-C7-PROD-CD
		//                 , :VOID-C8-CALC-EXPLN-CD
		//                 , :VOID-C8-PD-AM
		//                 , :VOID-C8-GL-ACCT-ID
		//                 , :VOID-C8-CORP-CD
		//                 , :VOID-C8-PROD-CD
		//                 , :VOID-C9-CALC-EXPLN-CD
		//                 , :VOID-C9-PD-AM
		//                 , :VOID-C9-GL-ACCT-ID
		//                 , :VOID-C9-CORP-CD
		//                 , :VOID-C9-PROD-CD
		//                 , :VOID-LI-NCOV-AM
		//                 , :VOID-CAS-EOB-DED-AM
		//                 , :VOID-CAS-EOB-COINS-AM
		//                 , :VOID-CAS-EOB-COPAY-AM
		//              FROM S02813SA AS VPMT
		//             INNER JOIN S02809SA AS VLI
		//                ON VLI.CLM_CNTL_ID     = VPMT.CLM_CNTL_ID
		//               AND VLI.CLM_CNTL_SFX_ID = VPMT.CLM_CNTL_SFX_ID
		//               AND VLI.CLM_PMT_ID      = VPMT.CLM_PMT_ID
		//               AND VLI.CLI_ID          = :WS-LINE-NUM
		//              LEFT OUTER JOIN S02811SA AS   VC1
		//                ON VC1.CLM_CNTL_ID     = VLI.CLM_CNTL_ID
		//               AND VC1.CLM_CNTL_SFX_ID = VLI.CLM_CNTL_SFX_ID
		//               AND VC1.CLM_PMT_ID      = VLI.CLM_PMT_ID
		//               AND VC1.CLI_ID          = VLI.CLI_ID
		//               AND VC1.CALC_TYP_CD     = '1'
		//              LEFT OUTER JOIN S02811SA AS    VC2
		//                ON VC2.CLM_CNTL_ID     = VLI.CLM_CNTL_ID
		//               AND VC2.CLM_CNTL_SFX_ID = VLI.CLM_CNTL_SFX_ID
		//               AND VC2.CLM_PMT_ID      = VLI.CLM_PMT_ID
		//               AND VC2.CLI_ID          = VLI.CLI_ID
		//               AND VC2.CALC_TYP_CD     = '2'
		//              LEFT OUTER JOIN S02811SA AS    VC3
		//                ON VC3.CLM_CNTL_ID     = VLI.CLM_CNTL_ID
		//               AND VC3.CLM_CNTL_SFX_ID = VLI.CLM_CNTL_SFX_ID
		//               AND VC3.CLM_PMT_ID      = VLI.CLM_PMT_ID
		//               AND VC3.CLI_ID          = VLI.CLI_ID
		//               AND VC3.CALC_TYP_CD     = '3'
		//              LEFT OUTER JOIN S02811SA AS    VC4
		//                ON VC4.CLM_CNTL_ID     = VLI.CLM_CNTL_ID
		//               AND VC4.CLM_CNTL_SFX_ID = VLI.CLM_CNTL_SFX_ID
		//               AND VC4.CLM_PMT_ID      = VLI.CLM_PMT_ID
		//               AND VC4.CLI_ID          = VLI.CLI_ID
		//               AND VC4.CALC_TYP_CD     = '4'
		//              LEFT OUTER JOIN S02811SA AS    VC5
		//                ON VC5.CLM_CNTL_ID     = VLI.CLM_CNTL_ID
		//               AND VC5.CLM_CNTL_SFX_ID = VLI.CLM_CNTL_SFX_ID
		//               AND VC5.CLM_PMT_ID      = VLI.CLM_PMT_ID
		//               AND VC5.CLI_ID          = VLI.CLI_ID
		//               AND VC5.CALC_TYP_CD     = '5'
		//              LEFT OUTER JOIN S02811SA AS    VC6
		//                ON VC6.CLM_CNTL_ID     = VLI.CLM_CNTL_ID
		//               AND VC6.CLM_CNTL_SFX_ID = VLI.CLM_CNTL_SFX_ID
		//               AND VC6.CLM_PMT_ID      = VLI.CLM_PMT_ID
		//               AND VC6.CLI_ID          = VLI.CLI_ID
		//               AND VC6.CALC_TYP_CD     = '6'
		//              LEFT OUTER JOIN S02811SA AS    VC7
		//                ON VC7.CLM_CNTL_ID     = VLI.CLM_CNTL_ID
		//               AND VC7.CLM_CNTL_SFX_ID = VLI.CLM_CNTL_SFX_ID
		//               AND VC7.CLM_PMT_ID      = VLI.CLM_PMT_ID
		//               AND VC7.CLI_ID          = VLI.CLI_ID
		//               AND VC7.CALC_TYP_CD     = '7'
		//              LEFT OUTER JOIN S02811SA AS    VC8
		//                ON VC8.CLM_CNTL_ID     = VLI.CLM_CNTL_ID
		//               AND VC8.CLM_CNTL_SFX_ID = VLI.CLM_CNTL_SFX_ID
		//               AND VC8.CLM_PMT_ID      = VLI.CLM_PMT_ID
		//               AND VC8.CLI_ID          = VLI.CLI_ID
		//               AND VC8.CALC_TYP_CD     = '8'
		//              LEFT OUTER JOIN S02811SA AS    VC9
		//                ON VC9.CLM_CNTL_ID     = VLI.CLM_CNTL_ID
		//               AND VC9.CLM_CNTL_SFX_ID = VLI.CLM_CNTL_SFX_ID
		//               AND VC9.CLM_PMT_ID      = VLI.CLM_PMT_ID
		//               AND VC9.CLI_ID          = VLI.CLI_ID
		//               AND VC9.CALC_TYP_CD     = '9'
		//              LEFT OUTER JOIN
		//                (SELECT  A.CLM_CNTL_ID
		//                       , A.CLM_CNTL_SFX_ID
		//                       , A.CLM_PMT_ID
		//                       , A.CLI_ID
		//                       , A.LI_NCOV_AM
		//                       , A.LI_SOCP_DED_AM
		//                       , A.LI_SOCP_COINS_AM
		//                       , A.LI_SOCP_COPAY_AM
		//                   FROM S02800CA AS A
		//                   WHERE A.CLM_CNTL_ID     = :DB2-CLM-CNTL-ID
		//                     AND A.CLM_CNTL_SFX_ID = :DB2-CLM-CNTL-SFX-ID
		//                     AND A.CLM_PMT_ID      = :PREV-CLM-PMT-ID
		//                     AND A.CLI_ID          = :WS-LINE-NUM) VCAS
		//                ON VCAS.CLM_CNTL_ID     = VLI.CLM_CNTL_ID
		//               AND VCAS.CLM_CNTL_SFX_ID = VLI.CLM_CNTL_SFX_ID
		//               AND VCAS.CLM_PMT_ID      = VLI.CLM_PMT_ID
		//               AND VCAS.CLI_ID          = VLI.CLI_ID
		//            WHERE VPMT.CLM_CNTL_ID      = :DB2-CLM-CNTL-ID
		//              AND VPMT.CLM_CNTL_SFX_ID  = :DB2-CLM-CNTL-SFX-ID
		//              AND VPMT.CLM_PMT_ID       = :PREV-CLM-PMT-ID
		//           END-EXEC.
		s02800caS02809saS02811saS02813saDao.selectRec(ws.getWsExpWorkItems().getWsLineNum(), ws.getEobDb2CursorArea().getS02801Info().getClmCntlId(),
				ws.getEobDb2CursorArea().getS02801Info().getClmCntlSfxId(), ws.getWsExpWorkItems().getPrevClmPmtId(), ws.getVoidInformation());
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 END-IF
		//             WHEN +100
		//                 MOVE 'Y' TO WS-END-OF-LINES
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: IF GET-VOID-FOR-EXP = 'N'
				//              CONTINUE
				//           ELSE
				//              MOVE 'NO' TO VOID-REPAY-SW
				//           END-IF
			if (ws.getWsSwitches().getGetVoidForExp() == 'N') {
				// COB_CODE: CONTINUE
				//continue
			} else {
				// COB_CODE: MOVE 'YES' TO VOID-REPAY-SW
				ws.getWsSwitches().setVoidRepaySw("YES");
				// COB_CODE: MOVE VOID-CLM-CNTL-ID TO HOLD-VOID-CLAIM-ID
				ws.getWsNf0533WorkArea().getHoldVoidKey().setClaimIdFormatted(ws.getVoidInformation().getVoidS02813Info().getClmCntlIdFormatted());
				// COB_CODE: PERFORM 3400-CREATE-VOID-EXP-RECS THRU 3400-EXIT
				createVoidExpRecs();
				// COB_CODE: IF HOLD-VOID-CLAIM-ID-1 = 4
				//              CONTINUE
				//           ELSE
				//              END-IF
				//           END-IF
				if (ws.getWsNf0533WorkArea().getHoldVoidKey().getClaimId1() == 4) {
					// COB_CODE: CONTINUE
					//continue
				} else if (ws.getVoidInformation().getVoidS02811Calc1Info().getC1CalcExplnCd() == 'A'
						|| ws.getVoidInformation().getVoidS02811Calc2Info().getC1CalcExplnCd() == 'A'
						|| ws.getVoidInformation().getVoidS02811Calc3Info().getC1CalcExplnCd() == 'A'
						|| ws.getVoidInformation().getVoidS02811Calc4Info().getC1CalcExplnCd() == 'A'
						|| ws.getVoidInformation().getVoidS02811Calc5Info().getC1CalcExplnCd() == 'A'
						|| ws.getVoidInformation().getVoidS02811Calc6Info().getC1CalcExplnCd() == 'A'
						|| ws.getVoidInformation().getVoidS02811Calc7Info().getC1CalcExplnCd() == 'A'
						|| ws.getVoidInformation().getVoidS02811Calc8Info().getC1CalcExplnCd() == 'A'
						|| ws.getVoidInformation().getVoidS02811Calc9Info().getC1CalcExplnCd() == 'A'
						|| ws.getVoidInformation().getVoidS02809LineInfo().getLiExplnCd() == 'A') {
					// COB_CODE: IF VOID-C1-CALC-EXPLN-CD = 'A'
					//           OR VOID-C2-CALC-EXPLN-CD = 'A'
					//           OR VOID-C3-CALC-EXPLN-CD = 'A'
					//           OR VOID-C4-CALC-EXPLN-CD = 'A'
					//           OR VOID-C5-CALC-EXPLN-CD = 'A'
					//           OR VOID-C6-CALC-EXPLN-CD = 'A'
					//           OR VOID-C7-CALC-EXPLN-CD = 'A'
					//           OR VOID-C8-CALC-EXPLN-CD = 'A'
					//           OR VOID-C9-CALC-EXPLN-CD = 'A'
					//           OR VOID-LI-EXPLN-CD = 'A'
					//              CONTINUE
					//           ELSE
					//                                       THRU 3300-EXIT
					//           END-IF
					// COB_CODE: CONTINUE
					//continue
				} else {
					// COB_CODE: PERFORM 3300-CALCULATE-CHECKS
					//                                  THRU 3300-EXIT
					calculateChecks();
				}
				// COB_CODE: ADD 1 TO WS-LINE-NUM
				ws.getWsExpWorkItems().setWsLineNum(Trunc.toShort(1 + ws.getWsExpWorkItems().getWsLineNum(), 3));
				// COB_CODE: MOVE 'NO' TO VOID-REPAY-SW
				ws.getWsSwitches().setVoidRepaySw("NO");
			}
			break;

		case 100:// COB_CODE: MOVE 'Y' TO WS-END-OF-LINES
			ws.getWsExpWorkItems().setWsEndOfLinesFormatted("Y");
			break;

		default:// COB_CODE: MOVE 'S02813SA/2809/2811'     TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S02813SA/2809/2811");
			// COB_CODE: MOVE 'JOIN TABLES FOR VOID'   TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("JOIN TABLES FOR VOID");
			// COB_CODE: MOVE '3500-SELECT-VOID-DATA ' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("3500-SELECT-VOID-DATA ");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 3600-CK-FOR-NON-PRT-RMKS<br>
	 * <pre>    DISPLAY '3600-CK-FOR-NON-PRT-RMKS'</pre>*/
	private void ckForNonPrtRmks() {
		// COB_CODE: IF WS-SUB-NON-PRT-RMKS > WS-NON-PRT-RMKS-TBL-ENTRIES
		//             MOVE 'Y' TO WS-NON-PRT-RMKS-FINISHED
		//           ELSE
		//             END-IF
		//           END-IF.
		if (ws.getWsNonPrtRmksVariables().getSubNonPrtRmks() > ws.getWsNonPrtRmksVariables().getNonPrtRmksTblEntries()) {
			// COB_CODE: MOVE 'Y' TO WS-NON-PRT-RMKS-FINISHED
			ws.getWsNonPrtRmksVariables().setNonPrtRmksFinishedFormatted("Y");
		} else if (Conditions.eq(ws.getWsNonPrtRmksVariables().getHoldRemarkCode(),
				ws.getWsNonPrtRmksVariables().getNonPrtRmksTable(ws.getWsNonPrtRmksVariables().getSubNonPrtRmks()).getWsNonPrtRmks())) {
			// COB_CODE: IF WS-HOLD-REMARK-CODE =
			//                 WS-NON-PRT-RMKS (WS-SUB-NON-PRT-RMKS)
			//              MOVE 'Y' TO WS-NON-PRT-RMKS-FINISHED
			//           END-IF
			// COB_CODE: MOVE 'Y' TO WS-NON-PRT-RMKS-FOUND
			ws.getWsNonPrtRmksVariables().setNonPrtRmksFoundFormatted("Y");
			// COB_CODE: MOVE 'Y' TO WS-NON-PRT-RMKS-FINISHED
			ws.getWsNonPrtRmksVariables().setNonPrtRmksFinishedFormatted("Y");
		}
	}

	/**Original name: 3700-GET-PROV-NPI<br>
	 * <pre>    DISPLAY '** 3700-GET-PROV-NPI **'.</pre>*/
	private void getProvNpi() {
		// COB_CODE: EXEC SQL
		//             SELECT BI_NPI.PROV_ID
		//              INTO :WS-DB2-BI-NPI-PROV-ID
		//              FROM S02815SA AS BI_NPI
		//              WHERE BI_NPI.CLM_CNTL_ID   = :DB2-CLM-CNTL-ID
		//                AND BI_NPI.CLM_CNTL_SFX_ID = :DB2-CLM-CNTL-SFX-ID
		//                AND BI_NPI.CLM_PMT_ID    = :DB2-CLM-PMT-ID
		//                AND BI_NPI.REF_ID_QLF_CD = 'HPI'
		//                AND BI_NPI.PROV_CD     = 'BI'
		//            END-EXEC.
		ws.getWsNf0533WorkArea()
				.setWsDb2BiNpiProvId(s02815saDao.selectRec(ws.getEobDb2CursorArea().getS02801Info().getClmCntlId(),
						ws.getEobDb2CursorArea().getS02801Info().getClmCntlSfxId(), ws.getEobDb2CursorArea().getS02801Info().getClmPmtId(),
						ws.getWsNf0533WorkArea().getWsDb2BiNpiProvId()));
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 CONTINUE
		//             WHEN +100
		//                 MOVE SPACES TO WS-DB2-BI-NPI-PROV-ID
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		case 100:// COB_CODE: MOVE SPACES TO WS-DB2-BI-NPI-PROV-ID
			ws.getWsNf0533WorkArea().setWsDb2BiNpiProvId("");
			break;

		default:// COB_CODE: MOVE 'S02815SA PROV NPI '     TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S02815SA PROV NPI ");
			// COB_CODE: MOVE 'SELECT NPI DATA   '     TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("SELECT NPI DATA   ");
			// COB_CODE: MOVE '3700-GET-PROV-NPI'      TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("3700-GET-PROV-NPI");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 3800-CK-FOR-SRS-PROVIDER<br>
	 * <pre>    DISPLAY '3800-CK-FOR-SRS-PROVIDER'.</pre>*/
	private void ckForSrsProvider() {
		// COB_CODE: IF WS-SUB-PRV-STOP-PAY > WS-PRV-STOP-PAY-TBL-ENTRIES
		//             MOVE SPACES TO WS-PROV-STOP-PAY-CD
		//           ELSE
		//             END-IF
		//           END-IF.
		if (ws.getWsPrvStopPayVariables().getWsSubPrvStopPay() > ws.getWsPrvStopPayVariables().getWsPrvStopPayTblEntries()) {
			// COB_CODE: MOVE 'Y' TO WS-PRV-STOP-PAY-FINISHED
			ws.getWsPrvStopPayVariables().setWsPrvStopPayFinishedFormatted("Y");
			// COB_CODE: MOVE SPACES TO WS-PROV-STOP-PAY-CD
			ws.getWsPrvStopPayVariables().setWsProvStopPayCd("");
		} else if (Conditions.gt(ws.getWsPrvStopPayVariables().getWsPrvStopPayTable(ws.getWsPrvStopPayVariables().getWsSubPrvStopPay()).getId(),
				ws.getWsPrvStopPayVariables().getHldSrsProvId())) {
			// COB_CODE: IF SRS-PROV-ID (WS-SUB-PRV-STOP-PAY) >
			//                HLD-SRS-PROV-ID
			//              MOVE SPACES TO WS-PROV-STOP-PAY-CD
			//           ELSE
			//             END-IF
			//           END-IF
			// COB_CODE: MOVE 'Y' TO WS-PRV-STOP-PAY-FINISHED
			ws.getWsPrvStopPayVariables().setWsPrvStopPayFinishedFormatted("Y");
			// COB_CODE: MOVE SPACES TO WS-PROV-STOP-PAY-CD
			ws.getWsPrvStopPayVariables().setWsProvStopPayCd("");
		} else if (Conditions.eq(ws.getWsPrvStopPayVariables().getHldSrsProvId(),
				ws.getWsPrvStopPayVariables().getWsPrvStopPayTable(ws.getWsPrvStopPayVariables().getWsSubPrvStopPay()).getId())
				&& ws.getWsPrvStopPayVariables().getHldSrsPrvLobCd() == ws.getWsPrvStopPayVariables()
						.getWsPrvStopPayTable(ws.getWsPrvStopPayVariables().getWsSubPrvStopPay()).getLobCd()) {
			// COB_CODE: IF  HLD-SRS-PROV-ID =
			//                SRS-PROV-ID (WS-SUB-PRV-STOP-PAY)
			//           AND HLD-SRS-PRV-LOB-CD =
			//                SRS-PROV-LOB-CD (WS-SUB-PRV-STOP-PAY)
			//              MOVE 'YES' TO SRS-PROVIDER-SW
			//           ELSE
			//              MOVE SPACES TO WS-PROV-STOP-PAY-CD
			//           END-IF
			// COB_CODE: MOVE 'Y' TO WS-PRV-STOP-PAY-FOUND
			ws.getWsPrvStopPayVariables().setWsPrvStopPayFoundFormatted("Y");
			// COB_CODE: MOVE 'G' TO WS-PROV-STOP-PAY-CD
			ws.getWsPrvStopPayVariables().setWsProvStopPayCd("G");
			// COB_CODE: MOVE 'Y' TO WS-PRV-STOP-PAY-FINISHED
			ws.getWsPrvStopPayVariables().setWsPrvStopPayFinishedFormatted("Y");
			// COB_CODE: MOVE 'YES' TO SRS-PROVIDER-SW
			ws.getWsSwitches().setSrsProviderSw("YES");
		} else {
			// COB_CODE: MOVE SPACES TO WS-PROV-STOP-PAY-CD
			ws.getWsPrvStopPayVariables().setWsProvStopPayCd("");
		}
	}

	/**Original name: 3900-SET-COPAY-MAX-NOTE<br>
	 * <pre>    DISPLAY '** 3900-SET-COPAY-MAX-NOTE **'.
	 *     DISPLAY 'DB2-CLM-CNTL-ID = ' DB2-CLM-CNTL-ID ' '
	 *          DB2-CLM-CNTL-SFX-ID '   '
	 *         'DB2-SPEC-DRUG-CPN-IN = *' DB2-SPEC-DRUG-CPN-IN '*'.</pre>*/
	private void setCopayMaxNote() {
		// COB_CODE: IF DB2-SPEC-DRUG-CPN-IN NOT EQUAL 'Y'
		//               GO TO 3900-EXIT.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getSpecDrugCpnIn() != 'Y') {
			// COB_CODE: GO TO 3900-EXIT.
			return;
		}
		// COB_CODE: IF WS-NOTE-1 = SPACES
		//              MOVE 'EOB NOTE CODE SZ' TO WS-WHICH-NOTES-TABLE-1
		//           ELSE
		//              MOVE 'EOB NOTE CODE SZ' TO WS-WHICH-NOTES-TABLE-2.
		if (Characters.EQ_SPACE.test(ws.getWsHoldNoteArea().getNote1())) {
			// COB_CODE: MOVE 'SZ'               TO WS-NOTE-1
			ws.getWsHoldNoteArea().setNote1("SZ");
			// COB_CODE: MOVE 'EOB NOTE CODE SZ' TO WS-WHICH-NOTES-TABLE-1
			ws.getWsHoldNoteArea().setWhichNotesTable1("EOB NOTE CODE SZ");
		} else if (Conditions.eq(ws.getWsHoldNoteArea().getNote1(), "SZ")) {
			// COB_CODE: IF WS-NOTE-1 = 'SZ'
			//              GO TO 3900-EXIT
			//           ELSE
			//              MOVE 'EOB NOTE CODE SZ' TO WS-WHICH-NOTES-TABLE-2.
			// COB_CODE: GO TO 3900-EXIT
			return;
		} else {
			// COB_CODE: MOVE 'SZ'               TO WS-NOTE-2
			ws.getWsHoldNoteArea().setNote2("SZ");
			// COB_CODE: MOVE 'EOB NOTE CODE SZ' TO WS-WHICH-NOTES-TABLE-2.
			ws.getWsHoldNoteArea().setWhichNotesTable2("EOB NOTE CODE SZ");
		}
	}

	/**Original name: 4000-ADD-LINE-CALC-AMTS<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 4000-ADD-LINE-CALC-AMTS **'.
	 *                                                             *</pre>*/
	private void addLineCalcAmts() {
		// COB_CODE: COMPUTE WS-LINE-ACCUM-PAID = DB2-C1-PD-AM
		//                                      + DB2-C2-PD-AM
		//                                      + DB2-C3-PD-AM
		//                                      + DB2-C4-PD-AM
		//                                      + DB2-C5-PD-AM
		//                                      + DB2-C6-PD-AM
		//                                      + DB2-C7-PD-AM
		//                                      + DB2-C8-PD-AM
		//                                      + DB2-C9-PD-AM
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getLineAccumulators()
				.setPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc1Info().getPdAm()
						.add(ws.getEobDb2CursorArea().getS02811Calc2Info().getPdAm()).add(ws.getEobDb2CursorArea().getS02811Calc3Info().getPdAm())
						.add(ws.getEobDb2CursorArea().getS02811Calc4Info().getPdAm()).add(ws.getEobDb2CursorArea().getS02811Calc5Info().getPdAm())
						.add(ws.getEobDb2CursorArea().getS02811Calc6Info().getPdAm()).add(ws.getEobDb2CursorArea().getS02811Calc7Info().getPdAm())
						.add(ws.getEobDb2CursorArea().getS02811Calc8Info().getPdAm()).add(ws.getEobDb2CursorArea().getS02811Calc9Info().getPdAm()),
						11, 2));
		// COB_CODE: COMPUTE WS-CLAIM-ACCUM-PAID = WS-CLAIM-ACCUM-PAID
		//                                       + WS-LINE-ACCUM-PAID
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getClaimAccumulators().setPaid(Trunc.toDecimal(
				ws.getWsRollLogicWork().getClaimAccumulators().getPaid().add(ws.getWsRollLogicWork().getLineAccumulators().getPaid()), 13, 2));
		// COB_CODE: COMPUTE WS-LINE-ACCUM-PAT-OWES = WS-LINE-ACCUM-PAT-OWES
		//                                          + DB2-LI-PAT-RESP-AM
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getLineAccumulators().setPatOwes(Trunc.toDecimal(
				ws.getWsRollLogicWork().getLineAccumulators().getPatOwes().add(ws.getEobDb2CursorArea().getS02809LineInfo().getPatRespAm()), 11, 2));
		// COB_CODE: COMPUTE WS-CLAIM-ACCUM-PAT-OWES = WS-CLAIM-ACCUM-PAT-OWES
		//                                           + WS-LINE-ACCUM-PAT-OWES
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getClaimAccumulators().setPatOwes(Trunc.toDecimal(
				ws.getWsRollLogicWork().getClaimAccumulators().getPatOwes().add(ws.getWsRollLogicWork().getLineAccumulators().getPatOwes()), 13, 2));
		// COB_CODE: COMPUTE WS-LINE-ACCUM-WRITEOFF = WS-LINE-ACCUM-WRITEOFF
		//                                          + DB2-LI-PWO-AM
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getLineAccumulators().setWriteoff(Trunc.toDecimal(
				ws.getWsRollLogicWork().getLineAccumulators().getWriteoff().add(ws.getEobDb2CursorArea().getS02809LineInfo().getPwoAm()), 11, 2));
		// COB_CODE: COMPUTE WS-CLAIM-ACCUM-WRITEOFF = WS-CLAIM-ACCUM-WRITEOFF
		//                                           + WS-LINE-ACCUM-WRITEOFF
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getClaimAccumulators()
				.setWriteoff(Trunc.toDecimal(
						ws.getWsRollLogicWork().getClaimAccumulators().getWriteoff().add(ws.getWsRollLogicWork().getLineAccumulators().getWriteoff()),
						13, 2));
		// COB_CODE: COMPUTE WS-CLAIM-ACCUM-OI-PAID = WS-CLAIM-ACCUM-OI-PAID
		//                                          + DB2-LI-ALCT-OI-PD-AM
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getClaimAccumulators().setOiPaid(Trunc.toDecimal(
				ws.getWsRollLogicWork().getClaimAccumulators().getOiPaid().add(ws.getEobDb2CursorArea().getS02809LineInfo().getAlctOiPdAm()), 13, 2));
		// COB_CODE: COMPUTE WS-CLAIM-ACCUM-CHARGE = WS-CLAIM-ACCUM-CHARGE
		//                                         + DB2-LI-CHG-AM
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getClaimAccumulators().setCharge(Trunc.toDecimal(
				ws.getWsRollLogicWork().getClaimAccumulators().getCharge().add(ws.getEobDb2CursorArea().getS02809LineInfo().getChgAm()), 13, 2));
		// COB_CODE: COMPUTE WS-CLAIM-ACCUM-ALLOWD = WS-CLAIM-ACCUM-ALLOWD
		//                                         + DB2-LI-ALW-CHG-AM
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getClaimAccumulators().setAllowd(Trunc.toDecimal(
				ws.getWsRollLogicWork().getClaimAccumulators().getAllowd().add(ws.getEobDb2CursorArea().getS02809LineInfo().getAlwChgAm()), 13, 2));
		// COB_CODE: PERFORM 4100-ADD-LI-NCOV-AMTS THRU 4100-EXIT.
		addLiNcovAmts();
		// COB_CODE: PERFORM 4200-ADD-DED-COINS-AMTS  THRU 4200-EXIT.
		addDedCoinsAmts();
	}

	/**Original name: 4100-ADD-LI-NCOV-AMTS<br>
	 * <pre>                                                                *
	 *     DISPLAY '** 4100-ADD-LI-NCOV-AMTS **'.
	 *                                                                 *</pre>*/
	private void addLiNcovAmts() {
		// COB_CODE: COMPUTE WS-LINE-ACCUM-NOTCOVD = WS-LINE-ACCUM-NOTCOVD
		//                                         + DB2-CAS-NCOV-AM
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getLineAccumulators().setNotcovd(Trunc.toDecimal(
				ws.getWsRollLogicWork().getLineAccumulators().getNotcovd().add(ws.getEobDb2CursorArea().getCasCalc1Info().getNcovAm()), 11, 2));
		// COB_CODE: COMPUTE WS-CLAIM-ACCUM-NOT-COVD = WS-CLAIM-ACCUM-NOT-COVD
		//                                           + WS-LINE-ACCUM-NOTCOVD
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getClaimAccumulators().setNotCovd(Trunc.toDecimal(
				ws.getWsRollLogicWork().getClaimAccumulators().getNotCovd().add(ws.getWsRollLogicWork().getLineAccumulators().getNotcovd()), 13, 2));
	}

	/**Original name: 4200-ADD-DED-COINS-AMTS<br>
	 * <pre>                                                               *
	 *     DISPLAY '** 4200-ADD-DED-COINS-AMTS **'.
	 *                                                                *
	 * ****************************************************************
	 *  THIS LOADS ALL CAS TRIOS FROM THE "PR" ADJUSTMENT GROUP. IT   *
	 *  INCLUDES  BOTH CALC1 AND CALC2 IN PREPARATION FOR COMBINING   *
	 *  LIKE CAS SEGMENTS.                                            *
	 * ****************************************************************</pre>*/
	private void addDedCoinsAmts() {
		// COB_CODE: COMPUTE WS-LINE-ACCUM-DEDUCTIBLE = WS-LINE-ACCUM-DEDUCTIBLE
		//                                            + DB2-CAS-EOB-DED-AM
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getLineAccumulators().setDeductible(Trunc.toDecimal(
				ws.getWsRollLogicWork().getLineAccumulators().getDeductible().add(ws.getEobDb2CursorArea().getCasCalc1Info().getEobDedAm()), 11, 2));
		// COB_CODE: COMPUTE WS-LINE-ACCUM-COINSURANCE = WS-LINE-ACCUM-COINSURANCE
		//                                             + DB2-CAS-EOB-COINS-AM
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getLineAccumulators().setCoinsurance(Trunc.toDecimal(
				ws.getWsRollLogicWork().getLineAccumulators().getCoinsurance().add(ws.getEobDb2CursorArea().getCasCalc1Info().getEobCoinsAm()), 11,
				2));
		// COB_CODE: COMPUTE WS-LINE-ACCUM-COPAY      = WS-LINE-ACCUM-COPAY
		//                                            + DB2-CAS-EOB-COPAY-AM
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getLineAccumulators().setCopay(Trunc.toDecimal(
				ws.getWsRollLogicWork().getLineAccumulators().getCopay().add(ws.getEobDb2CursorArea().getCasCalc1Info().getEobCopayAm()), 7, 2));
		// COB_CODE: COMPUTE WS-CLAIM-ACCUM-DEDUCT = WS-CLAIM-ACCUM-DEDUCT
		//                                         + WS-LINE-ACCUM-DEDUCTIBLE
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getClaimAccumulators()
				.setDeduct(Trunc.toDecimal(
						ws.getWsRollLogicWork().getClaimAccumulators().getDeduct().add(ws.getWsRollLogicWork().getLineAccumulators().getDeductible()),
						13, 2));
		// COB_CODE: COMPUTE WS-CLAIM-ACCUM-COINS = WS-CLAIM-ACCUM-COINS
		//                                        + WS-LINE-ACCUM-COINSURANCE
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getClaimAccumulators()
				.setCoins(Trunc.toDecimal(
						ws.getWsRollLogicWork().getClaimAccumulators().getCoins().add(ws.getWsRollLogicWork().getLineAccumulators().getCoinsurance()),
						13, 2));
		// COB_CODE: COMPUTE WS-CLAIM-ACCUM-COPAY  = WS-CLAIM-ACCUM-COPAY
		//                                         + WS-LINE-ACCUM-COPAY
		//           END-COMPUTE.
		ws.getWsRollLogicWork().getClaimAccumulators().setCopay(Trunc.toDecimal(
				ws.getWsRollLogicWork().getClaimAccumulators().getCopay().add(ws.getWsRollLogicWork().getLineAccumulators().getCopay()), 13, 2));
	}

	/**Original name: 6000-BUILD-HEADER-SECTION<br>
	 * <pre>                                                              *
	 *     DISPLAY '** 6000-BUILD-HEADER-SECTION **'.
	 *                                                               *</pre>*/
	private void buildHeaderSection() {
		// COB_CODE: INITIALIZE NF05-EOB-RECORD.
		initNf05EobRecord();
		// COB_CODE: MOVE DB2-PROD-IND            TO  NF05-PROD-IND.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProdInd(ws.getEobDb2CursorArea().getS02801Info().getProdInd());
		// COB_CODE: MOVE DB2-CLM-CNTL-ID         TO  NF05-CLAIM-NUM.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ClaimNum()
				.setNf05ClaimNumFormatted(ws.getEobDb2CursorArea().getS02801Info().getClmCntlIdFormatted());
		// COB_CODE: MOVE DB2-CLM-CNTL-SFX-ID     TO  NF05-CLAIM-SFX.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ClaimSfx(ws.getEobDb2CursorArea().getS02801Info().getClmCntlSfxId());
		// COB_CODE: MOVE DB2-PMT-LOB-CD          TO  NF05-CLAIM-LOB.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ClaimLob()
				.setNf05ClaimLob(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtLobCd());
		// COB_CODE: MOVE DB2-PMT-ITS-CLM-TYP-CD  TO  NF05-ITS-CLAIM-TYPE.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ItsClaimType()
				.setNf05ItsClaimType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd());
		// COB_CODE: MOVE DB2-PMT-PRIM-CN-ARNG-CD TO  NF05-PROV-TYP-CN-CD.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProvTypCnCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPrimCnArngCd());
		// COB_CODE: MOVE SPACES                  TO  NF05-INST-REIMB-MTHD-CD.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05InstReimbMthdCd("");
		// COB_CODE: MOVE SPACES                  TO  NF05-PROF-REIMB-MTHD-CD.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProfReimbMthdCd("");
		// COB_CODE: MOVE DB2-PMT-TYP-GRP-CD      TO  NF05-TYPE-GROUP.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05TypeGroup(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd());
		// COB_CODE: MOVE DB2-CLM-PMT-ID          TO  NF05-PMT-NUM.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05PmtNum(TruncAbs.toShort(ws.getEobDb2CursorArea().getS02801Info().getClmPmtId(), 2));
		// COB_CODE: MOVE DB2-LI-POS-CD           TO  NF05-PLACE-SERVICE-WHY.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05PlaceServiceWhy(ws.getEobDb2CursorArea().getS02809LineInfo().getPosCd());
		// COB_CODE: MOVE DB2-PMT-HIST-LOAD-CD    TO  NF05-HISTORY-LOAD-INDC.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05HistoryLoadIndc()
				.setNf05HistoryLoadIndc(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd());
		// COB_CODE: MOVE DB2-PMT-KCAPS-TEAM-NM   TO  NF05-TEAM.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05Team(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsTeamNm());
		// COB_CODE: MOVE DB2-PMT-KCAPS-USE-ID    TO  NF05-ID.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05Id(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsUseId());
		// COB_CODE: IF DB2-PMT-ASG-CD = 'N2'
		//              MOVE '2N' TO NF05-ASSIGNED-INDC
		//           ELSE
		//              MOVE DB2-PMT-ASG-CD TO NF05-ASSIGNED-INDC
		//           END-IF.
		if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd(), "N2")) {
			// COB_CODE: MOVE '2N' TO NF05-ASSIGNED-INDC
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05AssignedIndc().setNf05AssignedIndc("2N");
		} else {
			// COB_CODE: MOVE DB2-PMT-ASG-CD TO NF05-ASSIGNED-INDC
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05AssignedIndc()
					.setNf05AssignedIndc(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd());
		}
		// COB_CODE: MOVE DB2-PMT-GRP-ID          TO  NF05-GROUP-AND-ACCOUNT-12.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05GroupAndAccount12()
				.setNf05GroupAndAccount12Formatted(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtGrpIdFormatted());
		// COB_CODE: MOVE DB2-PMT-CORP-RCV-DT     TO  NF05-DATE-RECEIVED.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05DateReceived(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtCorpRcvDt());
		// COB_CODE: MOVE WS-BUSINESS-DATE        TO  NF05-DATE-PAID
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05DatePaid(ws.getWsDateFields().getWsBusinessDate().getWsBusinessDateFormatted());
		// COB_CODE: MOVE WS-GL-OFST-ORIG-CD      TO  NF05-GL-OFFST-CD.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05GlOffstCd(ws.getWsExpWorkItems().getWsGlOfstOrigCd());
		// COB_CODE: MOVE DB2-PMT-ALPH-PRFX-CD    TO  NF05-INSURED-ID-PREFIX.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord()
				.setNf05InsuredIdPrefix(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAlphPrfxCd());
		// COB_CODE: MOVE DB2-PMT-INS-ID          TO  NF05-INSURED-ID-DIGITS.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05InsuredIdDigits(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtInsId());
		// COB_CODE: MOVE DB2-PMT-ALT-INS-ID      TO  NF05-ALT-INS-ID.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setAltInsId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAltInsId());
		// COB_CODE: MOVE DB2-PMT-MEM-ID          TO  NF05-MEMBER-ID.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05MemberId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtMemId());
		// COB_CODE: MOVE DB2-CLM-SYST-VER-ID     TO  NF05-CLM-SYST-VER-ID.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ClmSystVerId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getClmSystVerId());
		// COB_CODE: MOVE DB2-PA-ID-TO-ACUM-ID    TO  NF05-PA-ID-TO-ACUM-ID.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setPaIdToAcumId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPaIdToAcumId());
		// COB_CODE: MOVE DB2-MEM-ID-TO-ACUM-ID   TO  NF05-MEM-ID-TO-ACUM-ID.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setMemIdToAcumId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getMemIdToAcumId());
		// COB_CODE: IF DB2-PMT-CLM-CORP-CODE = ' ' OR LOW-VALUES
		//              MOVE DB2-C1-CORP-CD TO NF05-CLM-CORP-CODE
		//           ELSE
		//              MOVE DB2-PMT-CLM-CORP-CODE   TO  NF05-CLM-CORP-CODE
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmCorpCode() == ' '
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmCorpCode(), Types.LOW_CHAR_VAL)) {
			// COB_CODE: MOVE DB2-C1-CORP-CD TO NF05-CLM-CORP-CODE
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ClmCorpCode(ws.getEobDb2CursorArea().getS02811Calc1Info().getCorpCd());
		} else {
			// COB_CODE: MOVE DB2-PMT-CLM-CORP-CODE   TO  NF05-CLM-CORP-CODE
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord()
					.setNf05ClmCorpCode(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmCorpCode());
		}
		// COB_CODE: IF DB2-LI-ADJD-OVRD1-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD2-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD3-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD4-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD5-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD6-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD7-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD8-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD9-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD10-CD = 'BIR'
		//              MOVE SPACES TO NF05-PROVIDER-NAME
		//           ELSE
		//              END-IF
		//           END-IF.
		if (Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd1Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd2Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd3Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd4Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd5Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd6Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd7Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd8Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd9Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd10Cd(), "BIR")) {
			// COB_CODE: MOVE SPACES TO NF05-PROVIDER-NAME
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProviderName("");
		} else {
			// COB_CODE: IF  BILLING-NAME     >  SPACES
			//               MOVE  BILLING-NAME       TO  NF05-PROVIDER-NAME
			//           ELSE
			//               MOVE  DB2-PROVIDER-NAME  TO  NF05-PROVIDER-NAME
			//           END-IF
			if (Characters.GT_SPACE.test(ws.getWsNf0533WorkArea().getBillingName())) {
				// COB_CODE: MOVE  BILLING-NAME       TO  NF05-PROVIDER-NAME
				ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProviderName(ws.getWsNf0533WorkArea().getBillingName());
			} else {
				// COB_CODE: MOVE  DB2-PROVIDER-NAME  TO  NF05-PROVIDER-NAME
				ws.getNf05EobRecord().getNf05HeaderPartOfRecord()
						.setNf05ProviderName(ws.getEobDb2CursorArea().getS02952NameDemoInfo().getProviderName());
			}
			// COB_CODE: IF NF05-TOTAL
			//              END-IF
			//           END-IF
			if (ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecordType().isTotal()) {
				// COB_CODE: IF NF05-PERF-PROVIDER-NAME = SPACES
				//              MOVE NF05-PROVIDER-NAME TO NF05-PERF-PROVIDER-NAME
				//           END-IF
				if (Characters.EQ_SPACE.test(ws.getNf05EobRecord().getNf05LineLevelRecord().getPerfProviderName())) {
					// COB_CODE: MOVE NF05-PROVIDER-NAME TO NF05-PERF-PROVIDER-NAME
					ws.getNf05EobRecord().getNf05LineLevelRecord()
							.setPerfProviderName(ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ProviderName());
				}
			}
		}
		// COB_CODE: MOVE HOLD-CLM-FORM           TO  NF05-CLAIM-FORM-INSERT.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ClaimFormInsertFormatted(ws.getClaimFormWkArea().getHoldClmFormFormatted());
		// COB_CODE: MOVE WS-LINE-ACCUM-PAID      TO  NF05-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord()
				.setNf05AmountPaid(Trunc.toDecimal(ws.getWsRollLogicWork().getLineAccumulators().getPaid(), 9, 2));
		// COB_CODE: MOVE CLAIM-LINE-INTEREST     TO  NF05-PRMPT-PAY-INT-AM.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord()
				.setNf05PrmptPayIntAm(Trunc.toDecimal(ws.getWsPromptPayWorkFields().getClaimLineInterest(), 9, 2));
		// COB_CODE: MOVE DB2-PMT-PAT-ACT-ID      TO  NF05-PATIENT-ACCOUNT-NUM.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord()
				.setNf05PatientAccountNum(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPatActId());
		// COB_CODE: MOVE DB2-PMT-MEM-ID          TO  NF05-MEMBER-ID.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05MemberId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtMemId());
		// COB_CODE: MOVE DB2-CLM-SYST-VER-ID     TO  NF05-CLM-SYST-VER-ID.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ClmSystVerId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getClmSystVerId());
		// COB_CODE: MOVE DB2-PA-ID-TO-ACUM-ID    TO  NF05-PA-ID-TO-ACUM-ID.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setPaIdToAcumId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPaIdToAcumId());
		// COB_CODE: MOVE DB2-MEM-ID-TO-ACUM-ID   TO  NF05-MEM-ID-TO-ACUM-ID.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setMemIdToAcumId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getMemIdToAcumId());
		// COB_CODE: MOVE SPACES                  TO  NF05-CROSS-REIMBURSE-INDC.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05CrossReimburseIndc().setNf05CrossReimburseIndc("");
		// COB_CODE: MOVE SPACES                  TO  NF05-SHIELD-REIMBURSE-INDC.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ShieldReimburseIndc().setNf05CrossReimburseIndc("");
		// COB_CODE: MOVE DB2-PMT-TYP-GRP-CD      TO  NF05-TYP-GRP-CD.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05TypGrpCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd());
		// COB_CODE: MOVE DB2-PE-PROV-ID          TO  NF05-PERF-PROVIDER-NUM.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05PerfProviderNum(ws.getEobDb2CursorArea().getS02815ProviderInfo().getPeProvId());
		// COB_CODE: MOVE DB2-PMT-BAL-BILL-AM     TO  NF05-BAL-BILL-AM.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord()
				.setNf05BalBillAm(TruncAbs.toDecimal(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtBalBillAm(), 9, 2));
		// COB_CODE: MOVE DB2-PMT-CORP-RCV-DT     TO  NF05-RECIEPT-DATE.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecieptDate()
				.setNf05RecieptDateFormatted(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtCorpRcvDtFormatted());
		// COB_CODE: MOVE DB2-PMT-ADJD-DT         TO  NF05-ADJUDICATION-DATE.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05AdjudicationDate()
				.setNf05RecieptDateFormatted(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjdDtFormatted());
		// COB_CODE: MOVE DB2-PMT-BILL-FRM-DT     TO  NF05-BILLING-FROM-DATE.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05BillingFromDate()
				.setNf05RecieptDateFormatted(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtBillFrmDtFormatted());
		// COB_CODE: MOVE DB2-PMT-BILL-THR-DT     TO  NF05-BILLING-THRU-DATE.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05BillingThruDate()
				.setNf05RecieptDateFormatted(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtBillThrDtFormatted());
		// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD      TO  NF05-ADJ-TYP-CD.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05AdjTypCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
		// COB_CODE: MOVE DB2-PMT-PRIM-CN-ARNG-CD TO  NF05-PRIM-CA-CD.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProvTypCnCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPrimCnArngCd());
		// COB_CODE: MOVE DB2-PMT-RATE-CD         TO  NF05-RATE-CD.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05RateCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtRateCd());
		// COB_CODE: MOVE DB2-PMT-MDGP-PLN-CD     TO  NF05-PMT-MDGP-PLAN-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setPmtMdgpPlanCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtMdgpPlnCd());
	}

	/**Original name: 6100-BUILD-LINE-SECTION<br>
	 * <pre>                                                              *
	 *     DISPLAY '** 6100-BUILD-LINE-SECTION **'.
	 *                                                               *</pre>*/
	private void buildLineSection() {
		// COB_CODE: MOVE 'L'                     TO  NF05-RECORD-TYPE.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecordType().setNf05RecordTypeFormatted("L");
		// COB_CODE: MOVE DB2-CLI-ID              TO  NF05-LINE-NUM.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05LineNum(TruncAbs.toInt(ws.getEobDb2CursorArea().getS02801Info().getCliId(), 5));
		// COB_CODE: MOVE DB2-LI-FRM-SERV-DT      TO  NF05-FROM-SERVICE-DATE.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setFromServiceDateFormatted(ws.getEobDb2CursorArea().getS02809LineInfo().getFrmServDtFormatted());
		// COB_CODE: MOVE DB2-LI-THR-SERV-DT      TO  NF05-THRU-SERVICE-DATE.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setThruServiceDateFormatted(ws.getEobDb2CursorArea().getS02809LineInfo().getThrServDtFormatted());
		// COB_CODE: MOVE DB2-LI-EXMN-ACTN-CD     TO  NF05-EXAMINER-ACTION.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setExaminerAction(ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd());
		// COB_CODE: MOVE DB2-LI-TS-CD            TO  NF05-TYPE-SERVICE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTypeService(ws.getEobDb2CursorArea().getS02809LineInfo().getTsCd());
		// COB_CODE: MOVE DB2-LI-POS-CD           TO  NF05-PLACE-SERVICE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setPlaceService(ws.getEobDb2CursorArea().getS02809LineInfo().getPosCd());
		// COB_CODE: MOVE DB2-C1-CALC-EXPLN-CD    TO  NF05-CALC1-EXPLN-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1ExplnCd(ws.getEobDb2CursorArea().getS02811Calc1Info().getCalcExplnCd());
		// COB_CODE: MOVE DB2-C2-CALC-EXPLN-CD    TO  NF05-CALC2-EXPLN-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2ExplnCd(ws.getEobDb2CursorArea().getS02811Calc2Info().getCalcExplnCd());
		// COB_CODE: MOVE DB2-C3-CALC-EXPLN-CD    TO  NF05-CALC3-EXPLN-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3ExplnCd(ws.getEobDb2CursorArea().getS02811Calc3Info().getCalcExplnCd());
		// COB_CODE: MOVE DB2-C4-CALC-EXPLN-CD    TO  NF05-CALC4-EXPLN-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4ExplnCd(ws.getEobDb2CursorArea().getS02811Calc4Info().getCalcExplnCd());
		// COB_CODE: MOVE DB2-C5-CALC-EXPLN-CD    TO  NF05-CALC5-EXPLN-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5ExplnCd(ws.getEobDb2CursorArea().getS02811Calc5Info().getCalcExplnCd());
		// COB_CODE: MOVE DB2-C6-CALC-EXPLN-CD    TO  NF05-CALC6-EXPLN-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6ExplnCd(ws.getEobDb2CursorArea().getS02811Calc6Info().getCalcExplnCd());
		// COB_CODE: MOVE DB2-C7-CALC-EXPLN-CD    TO  NF05-CALC7-EXPLN-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7ExplnCd(ws.getEobDb2CursorArea().getS02811Calc7Info().getCalcExplnCd());
		// COB_CODE: MOVE DB2-C8-CALC-EXPLN-CD    TO  NF05-CALC8-EXPLN-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8ExplnCd(ws.getEobDb2CursorArea().getS02811Calc8Info().getCalcExplnCd());
		// COB_CODE: MOVE DB2-C9-CALC-EXPLN-CD    TO  NF05-CALC9-EXPLN-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9ExplnCd(ws.getEobDb2CursorArea().getS02811Calc9Info().getCalcExplnCd());
		// COB_CODE: MOVE DB2-LI-BEN-TYP-CD       TO  NF05-BENEFIT-TYPE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBenefitType(ws.getEobDb2CursorArea().getS02809LineInfo().getBenTypCd());
		// COB_CODE: MOVE DB2-LI-CHG-AM           TO  NF05-AMOUNT-CHARGED.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setAmountCharged(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02809LineInfo().getChgAm(), 9, 2));
		// COB_CODE: MOVE WS-LINE-ACCUM-NOTCOVD   TO  NF05-LINE-NOT-COVRD-AMT.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setLineNotCovrdAmt(Trunc.toDecimal(ws.getWsRollLogicWork().getLineAccumulators().getNotcovd(), 9, 2));
		// COB_CODE: MOVE DB2-LI-ALCT-OI-COV-AM   TO  NF05-OTHER-INSURE-COVERED.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setOtherInsureCovered(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02809LineInfo().getAlctOiCovAm(), 9, 2));
		// COB_CODE: MOVE DB2-LI-ALCT-OI-PD-AM    TO  NF05-OTHER-INSURE-PAID.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setOtherInsurePaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02809LineInfo().getAlctOiPdAm(), 9, 2));
		// COB_CODE: MOVE DB2-LI-ALCT-OI-SAVE-AM  TO  NF05-OTHER-INSURE-SAVINGS.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setOtherInsureSavings(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02809LineInfo().getAlctOiSaveAm(), 9, 2));
		// COB_CODE: MOVE DB2-PMT-OI-CD           TO  NF05-OTHER-INSURE-CODE.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05OtherInsureCode()
				.setNf05OtherInsureCode(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtOiCd());
		//--
		// COB_CODE: MOVE DB2-C1-TYP-BEN-CD       TO  NF05-CALC1-TYPE-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1TypeBenefit(ws.getEobDb2CursorArea().getS02811Calc1Info().getTypBenCd());
		// COB_CODE: MOVE DB2-C1-SPECI-BEN-CD     TO  NF05-CALC1-SPEC-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1SpecBenefit(ws.getEobDb2CursorArea().getS02811Calc1Info().getSpeciBenCd());
		// COB_CODE: MOVE DB2-C1-INS-TC-CD        TO  NF05-CALC1-TYPE-CONTRACT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1TypeContract(ws.getEobDb2CursorArea().getS02811Calc1Info().getInsTcCd());
		// COB_CODE: MOVE DB2-C1-PD-AM            TO  NF05-CALC1-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setCalc1AmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc1Info().getPdAm(), 9, 2));
		// COB_CODE: MOVE DB2-CAS-EOB-DED-AM      TO  NF05-CALC1-DEDUCTIBLE.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setCalc1Deductible(Trunc.toDecimal(ws.getEobDb2CursorArea().getCasCalc1Info().getEobDedAm(), 9, 2));
		// COB_CODE: MOVE DB2-CAS-EOB-COINS-AM    TO  NF05-CALC1-COINSURANCE.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setCalc1Coinsurance(Trunc.toDecimal(ws.getEobDb2CursorArea().getCasCalc1Info().getEobCoinsAm(), 9, 2));
		// COB_CODE: MOVE DB2-CAS-EOB-COPAY-AM    TO  NF05-CALC1-COPAY.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setCalc1Copay(Trunc.toDecimal(ws.getEobDb2CursorArea().getCasCalc1Info().getEobCopayAm(), 5, 2));
		// COB_CODE: MOVE DB2-C1-CORP-CD          TO  NF05-CALC1-CORP-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1CorpCode(ws.getEobDb2CursorArea().getS02811Calc1Info().getCorpCd());
		// COB_CODE: MOVE DB2-C1-PROD-CD          TO  NF05-CALC1-PROD-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1ProdCode(ws.getEobDb2CursorArea().getS02811Calc1Info().getProdCd());
		// COB_CODE: MOVE DB2-C1-COV-LOB-CD       TO  NF05-CALC1-COV-LOB.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1CovLob(ws.getEobDb2CursorArea().getS02811Calc1Info().getCovLobCd());
		// COB_CODE: MOVE DB2-C1-PAY-CD           TO  NF05-CALC1-PAY-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1PayCode(ws.getEobDb2CursorArea().getS02811Calc1Info().getPayCd());
		//--
		// COB_CODE: MOVE DB2-C2-TYP-BEN-CD       TO  NF05-CALC2-TYPE-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2TypeBenefit(ws.getEobDb2CursorArea().getS02811Calc2Info().getTypBenCd());
		// COB_CODE: MOVE DB2-C2-SPECI-BEN-CD     TO  NF05-CALC2-SPEC-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2SpecBenefit(ws.getEobDb2CursorArea().getS02811Calc2Info().getSpeciBenCd());
		// COB_CODE: MOVE DB2-C2-INS-TC-CD        TO  NF05-CALC2-TYPE-CONTRACT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2TypeContract(ws.getEobDb2CursorArea().getS02811Calc2Info().getInsTcCd());
		// COB_CODE: MOVE DB2-C2-PD-AM            TO  NF05-CALC2-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setCalc2AmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc2Info().getPdAm(), 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC2-DEDUCTIBLE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2Deductible(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC2-COINSURANCE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2Coinsurance(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC2-COPAY.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2Copay(Trunc.toDecimal(0, 5, 2));
		// COB_CODE: MOVE DB2-C2-CORP-CD          TO  NF05-CALC2-CORP-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2CorpCode(ws.getEobDb2CursorArea().getS02811Calc2Info().getCorpCd());
		// COB_CODE: MOVE DB2-C2-PROD-CD          TO  NF05-CALC2-PROD-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2ProdCode(ws.getEobDb2CursorArea().getS02811Calc2Info().getProdCd());
		// COB_CODE: MOVE DB2-C2-COV-LOB-CD       TO  NF05-CALC2-COV-LOB.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2CovLob(ws.getEobDb2CursorArea().getS02811Calc2Info().getCovLobCd());
		// COB_CODE: MOVE DB2-C2-PAY-CD           TO  NF05-CALC2-PAY-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2PayCode(ws.getEobDb2CursorArea().getS02811Calc2Info().getPayCd());
		//--
		// COB_CODE: MOVE DB2-C3-TYP-BEN-CD       TO  NF05-CALC3-TYPE-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3TypeBenefit(ws.getEobDb2CursorArea().getS02811Calc3Info().getTypBenCd());
		// COB_CODE: MOVE DB2-C3-SPECI-BEN-CD     TO  NF05-CALC3-SPEC-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3SpecBenefit(ws.getEobDb2CursorArea().getS02811Calc3Info().getSpeciBenCd());
		// COB_CODE: MOVE DB2-C3-INS-TC-CD        TO  NF05-CALC3-TYPE-CONTRACT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3TypeContract(ws.getEobDb2CursorArea().getS02811Calc3Info().getInsTcCd());
		// COB_CODE: MOVE DB2-C3-PD-AM            TO  NF05-CALC3-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setCalc3AmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc3Info().getPdAm(), 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC3-DEDUCTIBLE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3Deductible(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC3-COINSURANCE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3Coinsurance(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC3-COPAY.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3Copay(Trunc.toDecimal(0, 5, 2));
		// COB_CODE: MOVE DB2-C3-CORP-CD          TO  NF05-CALC3-CORP-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3CorpCode(ws.getEobDb2CursorArea().getS02811Calc3Info().getCorpCd());
		// COB_CODE: MOVE DB2-C3-PROD-CD          TO  NF05-CALC3-PROD-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3ProdCode(ws.getEobDb2CursorArea().getS02811Calc3Info().getProdCd());
		// COB_CODE: MOVE DB2-C3-COV-LOB-CD       TO  NF05-CALC3-COV-LOB.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3CovLob(ws.getEobDb2CursorArea().getS02811Calc3Info().getCovLobCd());
		// COB_CODE: MOVE DB2-C3-PAY-CD           TO  NF05-CALC3-PAY-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3PayCode(ws.getEobDb2CursorArea().getS02811Calc3Info().getPayCd());
		//--
		// COB_CODE: MOVE DB2-C4-TYP-BEN-CD       TO  NF05-CALC4-TYPE-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4TypeBenefit(ws.getEobDb2CursorArea().getS02811Calc4Info().getTypBenCd());
		// COB_CODE: MOVE DB2-C4-SPECI-BEN-CD     TO  NF05-CALC4-SPEC-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4SpecBenefit(ws.getEobDb2CursorArea().getS02811Calc4Info().getSpeciBenCd());
		// COB_CODE: MOVE DB2-C4-INS-TC-CD        TO  NF05-CALC4-TYPE-CONTRACT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4TypeContract(ws.getEobDb2CursorArea().getS02811Calc4Info().getInsTcCd());
		// COB_CODE: MOVE DB2-C4-PD-AM            TO  NF05-CALC4-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setCalc4AmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc4Info().getPdAm(), 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC4-DEDUCTIBLE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4Deductible(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC4-COINSURANCE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4Coinsurance(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC4-COPAY.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4Copay(Trunc.toDecimal(0, 5, 2));
		// COB_CODE: MOVE DB2-C4-CORP-CD          TO  NF05-CALC4-CORP-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4CorpCode(ws.getEobDb2CursorArea().getS02811Calc4Info().getCorpCd());
		// COB_CODE: MOVE DB2-C4-PROD-CD          TO  NF05-CALC4-PROD-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4ProdCode(ws.getEobDb2CursorArea().getS02811Calc4Info().getProdCd());
		// COB_CODE: MOVE DB2-C4-COV-LOB-CD       TO  NF05-CALC4-COV-LOB.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4CovLob(ws.getEobDb2CursorArea().getS02811Calc4Info().getCovLobCd());
		// COB_CODE: MOVE DB2-C4-PAY-CD           TO  NF05-CALC4-PAY-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4PayCode(ws.getEobDb2CursorArea().getS02811Calc4Info().getPayCd());
		//--
		// COB_CODE: MOVE DB2-C5-TYP-BEN-CD       TO  NF05-CALC5-TYPE-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5TypeBenefit(ws.getEobDb2CursorArea().getS02811Calc5Info().getTypBenCd());
		// COB_CODE: MOVE DB2-C5-SPECI-BEN-CD     TO  NF05-CALC5-SPEC-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5SpecBenefit(ws.getEobDb2CursorArea().getS02811Calc5Info().getSpeciBenCd());
		// COB_CODE: MOVE DB2-C5-INS-TC-CD        TO  NF05-CALC5-TYPE-CONTRACT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5TypeContract(ws.getEobDb2CursorArea().getS02811Calc5Info().getInsTcCd());
		// COB_CODE: MOVE DB2-C5-PD-AM            TO  NF05-CALC5-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setCalc5AmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc5Info().getPdAm(), 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC5-DEDUCTIBLE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5Deductible(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC5-COINSURANCE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5Coinsurance(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC5-COPAY.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5Copay(Trunc.toDecimal(0, 5, 2));
		// COB_CODE: MOVE DB2-C5-CORP-CD          TO  NF05-CALC5-CORP-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5CorpCode(ws.getEobDb2CursorArea().getS02811Calc5Info().getCorpCd());
		// COB_CODE: MOVE DB2-C5-PROD-CD          TO  NF05-CALC5-PROD-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5ProdCode(ws.getEobDb2CursorArea().getS02811Calc5Info().getProdCd());
		// COB_CODE: MOVE DB2-C5-COV-LOB-CD       TO  NF05-CALC5-COV-LOB.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5CovLob(ws.getEobDb2CursorArea().getS02811Calc5Info().getCovLobCd());
		// COB_CODE: MOVE DB2-C5-PAY-CD           TO  NF05-CALC5-PAY-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5PayCode(ws.getEobDb2CursorArea().getS02811Calc5Info().getPayCd());
		//--
		// COB_CODE: MOVE DB2-C6-TYP-BEN-CD       TO  NF05-CALC6-TYPE-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6TypeBenefit(ws.getEobDb2CursorArea().getS02811Calc6Info().getTypBenCd());
		// COB_CODE: MOVE DB2-C6-SPECI-BEN-CD     TO  NF05-CALC6-SPEC-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6SpecBenefit(ws.getEobDb2CursorArea().getS02811Calc6Info().getSpeciBenCd());
		// COB_CODE: MOVE DB2-C6-INS-TC-CD        TO  NF05-CALC6-TYPE-CONTRACT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6TypeContract(ws.getEobDb2CursorArea().getS02811Calc6Info().getInsTcCd());
		// COB_CODE: MOVE DB2-C6-PD-AM            TO  NF05-CALC6-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setCalc6AmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc6Info().getPdAm(), 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC6-DEDUCTIBLE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6Deductible(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC6-COINSURANCE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6Coinsurance(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC6-COPAY.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6Copay(Trunc.toDecimal(0, 5, 2));
		// COB_CODE: MOVE DB2-C6-CORP-CD          TO  NF05-CALC6-CORP-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6CorpCode(ws.getEobDb2CursorArea().getS02811Calc6Info().getCorpCd());
		// COB_CODE: MOVE DB2-C6-PROD-CD          TO  NF05-CALC6-PROD-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6ProdCode(ws.getEobDb2CursorArea().getS02811Calc6Info().getProdCd());
		// COB_CODE: MOVE DB2-C6-COV-LOB-CD       TO  NF05-CALC6-COV-LOB.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6CovLob(ws.getEobDb2CursorArea().getS02811Calc6Info().getCovLobCd());
		// COB_CODE: MOVE DB2-C6-PAY-CD           TO  NF05-CALC6-PAY-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6PayCode(ws.getEobDb2CursorArea().getS02811Calc6Info().getPayCd());
		//--
		// COB_CODE: MOVE DB2-C7-TYP-BEN-CD       TO  NF05-CALC7-TYPE-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7TypeBenefit(ws.getEobDb2CursorArea().getS02811Calc7Info().getTypBenCd());
		// COB_CODE: MOVE DB2-C7-SPECI-BEN-CD     TO  NF05-CALC7-SPEC-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7SpecBenefit(ws.getEobDb2CursorArea().getS02811Calc7Info().getSpeciBenCd());
		// COB_CODE: MOVE DB2-C7-INS-TC-CD        TO  NF05-CALC7-TYPE-CONTRACT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7TypeContract(ws.getEobDb2CursorArea().getS02811Calc7Info().getInsTcCd());
		// COB_CODE: MOVE DB2-C7-PD-AM            TO  NF05-CALC7-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setCalc7AmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc7Info().getPdAm(), 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC7-DEDUCTIBLE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7Deductible(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC7-COINSURANCE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7Coinsurance(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC7-COPAY.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7Copay(Trunc.toDecimal(0, 5, 2));
		// COB_CODE: MOVE DB2-C7-CORP-CD          TO  NF05-CALC7-CORP-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7CorpCode(ws.getEobDb2CursorArea().getS02811Calc7Info().getCorpCd());
		// COB_CODE: MOVE DB2-C7-PROD-CD          TO  NF05-CALC7-PROD-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7ProdCode(ws.getEobDb2CursorArea().getS02811Calc7Info().getProdCd());
		// COB_CODE: MOVE DB2-C7-COV-LOB-CD       TO  NF05-CALC7-COV-LOB.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7CovLob(ws.getEobDb2CursorArea().getS02811Calc7Info().getCovLobCd());
		// COB_CODE: MOVE DB2-C7-PAY-CD           TO  NF05-CALC7-PAY-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7PayCode(ws.getEobDb2CursorArea().getS02811Calc7Info().getPayCd());
		//--
		// COB_CODE: MOVE DB2-C8-TYP-BEN-CD       TO  NF05-CALC8-TYPE-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8TypeBenefit(ws.getEobDb2CursorArea().getS02811Calc8Info().getTypBenCd());
		// COB_CODE: MOVE DB2-C8-SPECI-BEN-CD     TO  NF05-CALC8-SPEC-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8SpecBenefit(ws.getEobDb2CursorArea().getS02811Calc8Info().getSpeciBenCd());
		// COB_CODE: MOVE DB2-C8-INS-TC-CD        TO  NF05-CALC8-TYPE-CONTRACT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8TypeContract(ws.getEobDb2CursorArea().getS02811Calc8Info().getInsTcCd());
		// COB_CODE: MOVE DB2-C8-PD-AM            TO  NF05-CALC8-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setCalc8AmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc8Info().getPdAm(), 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC8-DEDUCTIBLE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8Deductible(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC8-COINSURANCE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8Coinsurance(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC8-COPAY.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8Copay(Trunc.toDecimal(0, 5, 2));
		// COB_CODE: MOVE DB2-C8-CORP-CD          TO  NF05-CALC8-CORP-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8CorpCode(ws.getEobDb2CursorArea().getS02811Calc8Info().getCorpCd());
		// COB_CODE: MOVE DB2-C8-PROD-CD          TO  NF05-CALC8-PROD-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8ProdCode(ws.getEobDb2CursorArea().getS02811Calc8Info().getProdCd());
		// COB_CODE: MOVE DB2-C8-COV-LOB-CD       TO  NF05-CALC8-COV-LOB.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8CovLob(ws.getEobDb2CursorArea().getS02811Calc8Info().getCovLobCd());
		// COB_CODE: MOVE DB2-C8-PAY-CD           TO  NF05-CALC8-PAY-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8PayCode(ws.getEobDb2CursorArea().getS02811Calc8Info().getPayCd());
		//--
		// COB_CODE: MOVE DB2-C9-TYP-BEN-CD       TO  NF05-CALC9-TYPE-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9TypeBenefit(ws.getEobDb2CursorArea().getS02811Calc9Info().getTypBenCd());
		// COB_CODE: MOVE DB2-C9-SPECI-BEN-CD     TO  NF05-CALC9-SPEC-BENEFIT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9SpecBenefit(ws.getEobDb2CursorArea().getS02811Calc9Info().getSpeciBenCd());
		// COB_CODE: MOVE DB2-C9-INS-TC-CD        TO  NF05-CALC9-TYPE-CONTRACT.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9TypeContract(ws.getEobDb2CursorArea().getS02811Calc9Info().getInsTcCd());
		// COB_CODE: MOVE DB2-C9-PD-AM            TO  NF05-CALC9-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setCalc9AmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02811Calc9Info().getPdAm(), 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC9-DEDUCTIBLE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9Deductible(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC9-COINSURANCE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9Coinsurance(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: MOVE 0                       TO  NF05-CALC9-COPAY.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9Copay(Trunc.toDecimal(0, 5, 2));
		// COB_CODE: MOVE DB2-C9-CORP-CD          TO  NF05-CALC9-CORP-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9CorpCode(ws.getEobDb2CursorArea().getS02811Calc9Info().getCorpCd());
		// COB_CODE: MOVE DB2-C9-PROD-CD          TO  NF05-CALC9-PROD-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9ProdCode(ws.getEobDb2CursorArea().getS02811Calc9Info().getProdCd());
		// COB_CODE: MOVE DB2-C9-COV-LOB-CD       TO  NF05-CALC9-COV-LOB.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9CovLob(ws.getEobDb2CursorArea().getS02811Calc9Info().getCovLobCd());
		// COB_CODE: MOVE DB2-C9-PAY-CD           TO  NF05-CALC9-PAY-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9PayCode(ws.getEobDb2CursorArea().getS02811Calc9Info().getPayCd());
		// COB_CODE: MOVE DB2-LI-EXPLN-CD         TO  NF05-LINE-EXPLANATION-CODE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setLineExplanationCode(ws.getEobDb2CursorArea().getS02809LineInfo().getExplnCd());
		// COB_CODE: MOVE DB2-LI-PWO-AM           TO  NF05-LINE-PROV-WRITEOFF.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord()
				.setNf05LineProvWriteoff(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02809LineInfo().getPwoAm(), 9, 2));
		// COB_CODE: MOVE WS-LINE-ACCUM-PAT-OWES  TO  NF05-LINE-PAT-RESPONS.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord()
				.setNf05LinePatRespons(Trunc.toDecimal(ws.getWsRollLogicWork().getLineAccumulators().getPatOwes(), 9, 2));
		// COB_CODE: MOVE DB2-LI-RMRK-CD          TO  NF05-DNL-REMARK.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setDnlRemark(ws.getEobDb2CursorArea().getS02809LineInfo().getRmrkCd());
		// COB_CODE: MOVE DB2-PMT-ADJD-PROV-STAT-CD TO NF05-ADJD-PROV-STAT-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setAdjdProvStatCd(String.valueOf(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjdProvStatCd()));
		// COB_CODE: MOVE DB2-LI-ALW-CHG-AM       TO  NF05-LI-ALW-CHG-AM.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setLiAlwChgAm(TruncAbs.toDecimal(ws.getEobDb2CursorArea().getS02809LineInfo().getAlwChgAm(), 11, 2));
		// COB_CODE: MOVE DB2-HC-INDUS-CD         TO  NF05-INDUS-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setIndusCd(ws.getEobDb2CursorArea().getHcIndusCd());
		//    DISPLAY 'BEFORE MOVE TO OUTPUT'.
		//    DISPLAY 'NOTE-1              = ' WS-NOTE-1
		//    DISPLAY 'WHICH-NOTES-TABLE-1 = ' WS-WHICH-NOTES-TABLE-1
		//    DISPLAY 'NOTE-2              = ' WS-NOTE-2
		//    DISPLAY 'WHICH-NOTES-TABLE-2 = ' WS-WHICH-NOTES-TABLE-2
		//    DISPLAY 'NOTE-3              = ' WS-NOTE-3
		//    DISPLAY 'WHICH-NOTES-TABLE-3 = ' WS-WHICH-NOTES-TABLE-3
		//    DISPLAY 'NOTE-4              = ' WS-NOTE-4
		//    DISPLAY 'WHICH-NOTES-TABLE-4 = ' WS-WHICH-NOTES-TABLE-4
		//    DISPLAY 'NOTE-5              = ' WS-NOTE-5
		//    DISPLAY 'WHICH-NOTES-TABLE-5 = ' WS-WHICH-NOTES-TABLE-5
		// COB_CODE: MOVE WS-NOTE-1           TO NF05-NOTES-1.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes1(ws.getWsHoldNoteArea().getNote1());
		// COB_CODE: MOVE WS-NOTE-2           TO NF05-NOTES-2.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes2(ws.getWsHoldNoteArea().getNote2());
		// COB_CODE: MOVE WS-NOTE-3           TO NF05-NOTES-3.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes3(ws.getWsHoldNoteArea().getNote3());
		// COB_CODE: MOVE WS-NOTE-4           TO NF05-NOTES-4.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes4(ws.getWsHoldNoteArea().getNote4());
		// COB_CODE: MOVE WS-NOTE-5           TO NF05-NOTES-5.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes5(ws.getWsHoldNoteArea().getNote5());
		// COB_CODE: MOVE WS-WHICH-NOTES-TABLE-1 TO NF05-WHICH-NOTES-TABLE-NOTE-1.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote1(ws.getWsHoldNoteArea().getWhichNotesTable1());
		// COB_CODE: MOVE WS-WHICH-NOTES-TABLE-2 TO NF05-WHICH-NOTES-TABLE-NOTE-2.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote2(ws.getWsHoldNoteArea().getWhichNotesTable2());
		// COB_CODE: MOVE WS-WHICH-NOTES-TABLE-3 TO NF05-WHICH-NOTES-TABLE-NOTE-3.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote3(ws.getWsHoldNoteArea().getWhichNotesTable3());
		// COB_CODE: MOVE WS-WHICH-NOTES-TABLE-4 TO NF05-WHICH-NOTES-TABLE-NOTE-4.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote4(ws.getWsHoldNoteArea().getWhichNotesTable4());
		// COB_CODE: MOVE WS-WHICH-NOTES-TABLE-5 TO NF05-WHICH-NOTES-TABLE-NOTE-5.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote5(ws.getWsHoldNoteArea().getWhichNotesTable5());
		// COB_CODE: MOVE SPACES TO NF05-PROV-STOP-PAY-CD.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProvStopPayCd("");
		// COB_CODE: MOVE DB2-LI-ADJD-OVRD1-CD  TO  NF05-OVERRIDE-CODE1.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode1(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd1Cd());
		// COB_CODE: MOVE DB2-LI-ADJD-OVRD2-CD  TO  NF05-OVERRIDE-CODE2.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode2(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd2Cd());
		// COB_CODE: MOVE DB2-LI-ADJD-OVRD3-CD  TO  NF05-OVERRIDE-CODE3.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode3(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd3Cd());
		// COB_CODE: MOVE DB2-LI-ADJD-OVRD4-CD  TO  NF05-OVERRIDE-CODE4.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode4(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd4Cd());
		// COB_CODE: MOVE DB2-LI-ADJD-OVRD5-CD  TO  NF05-OVERRIDE-CODE5.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode5(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd5Cd());
		// COB_CODE: MOVE DB2-LI-ADJD-OVRD6-CD  TO  NF05-OVERRIDE-CODE6.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode6(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd6Cd());
		// COB_CODE: MOVE DB2-LI-ADJD-OVRD7-CD  TO  NF05-OVERRIDE-CODE7.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode7(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd7Cd());
		// COB_CODE: MOVE DB2-LI-ADJD-OVRD8-CD  TO  NF05-OVERRIDE-CODE8.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode8(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd8Cd());
		// COB_CODE: MOVE DB2-LI-ADJD-OVRD9-CD  TO  NF05-OVERRIDE-CODE9.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode9(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd9Cd());
		// COB_CODE: MOVE DB2-LI-ADJD-OVRD10-CD TO  NF05-OVERRIDE-CODE10.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode10(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd10Cd());
		// COB_CODE: INITIALIZE WS-LINE-ACCUMULATORS.
		initLineAccumulators();
	}

	/**Original name: 6200-BUILD-A-TOTAL-LINE<br>
	 * <pre>                                                              *
	 *     DISPLAY '** 6200-BUILD-A-TOTAL-LINE **'.
	 *                                                               *</pre>*/
	private void buildATotalLine() {
		// COB_CODE: MOVE EOB-DB2-CURSOR-AREA TO EOB-CUR-CURSOR-AREA.
		ws.setEobCurCursorArea(ws.getEobDb2CursorArea().getEobDb2CursorAreaFormatted());
		// COB_CODE: MOVE EOB-HLD-CURSOR-AREA TO EOB-DB2-CURSOR-AREA.
		ws.getEobDb2CursorArea().setEobDb2CursorAreaFormatted(ws.getEobHldCursorAreaFormatted());
		// COB_CODE: MOVE 'T'                        TO  NF05-RECORD-TYPE.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecordType().setNf05RecordTypeFormatted("T");
		// COB_CODE: MOVE 0                          TO  NF05-LINE-PROV-WRITEOFF.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05LineProvWriteoff(Trunc.toDecimal(0, 9, 2));
		// COB_CODE: IF DB2-PMT-LOB-CD = '1' OR '7'
		//              MOVE DB2-LI-TS-CD            TO  NF05-INPAT-TYPE-SERVICE
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtLobCd() == '1'
				|| ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtLobCd() == '7') {
			// COB_CODE: MOVE DB2-LI-TS-CD            TO  NF05-INPAT-TYPE-SERVICE
			ws.getNf05EobRecord().getNf05LineLevelRecord().setInpatTypeService(ws.getEobDb2CursorArea().getS02809LineInfo().getTsCd());
		}
		// COB_CODE: MOVE DB2-BI-PROV-ID             TO  NF05-BILL-PVDR-NUM.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrNum(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvId());
		// COB_CODE: MOVE DB2-BI-PROV-LOB            TO  NF05-BILL-PVDR-LOB.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setBillPvdrLobFormatted(String.valueOf(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvLob()));
		// COB_CODE: MOVE DB2-PMT-ADJD-PROV-STAT-CD  TO  NF05-BILL-PVDR-STATUS.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrStatus(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjdProvStatCd());
		// COB_CODE: MOVE DB2-BI-PROV-SPEC           TO  NF05-BILL-PVDR-SPEC.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrSpec(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvSpec());
		// COB_CODE: MOVE HOLD-BI-ZIP                TO  NF05-BILL-PVDR-ZIP.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrZip(ws.getWsNf0533WorkArea().getHoldProviderAdressStuff().getBiZip());
		// COB_CODE: MOVE DB2-LI-FRM-SERV-DT TO  NF05-CLM-FROM-SERVICE-DATE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setClmFromServiceDate(ws.getEobDb2CursorArea().getS02809LineInfo().getFrmServDt());
		// COB_CODE: MOVE DB2-LI-THR-SERV-DT TO  NF05-CLM-THRU-SERVICE-DATE.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setClmThruServiceDate(ws.getEobDb2CursorArea().getS02809LineInfo().getThrServDt());
		// COB_CODE: IF  HOLD-BILLING-NAME  >  SPACES
		//               MOVE  HOLD-BILLING-NAME  TO  NF05-BILL-PVDR-NAME-LST
		//           ELSE
		//               MOVE DB2-PROVIDER-NAME      TO  NF05-BILL-PVDR-NAME-LST
		//           END-IF.
		if (Characters.GT_SPACE.test(ws.getWsNf0533WorkArea().getHoldBillingName())) {
			// COB_CODE: MOVE  HOLD-BILLING-NAME  TO  NF05-BILL-PVDR-NAME-LST
			ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrNameLst(ws.getWsNf0533WorkArea().getHoldBillingName());
		} else {
			// COB_CODE: MOVE DB2-PROVIDER-NAME      TO  NF05-BILL-PVDR-NAME-LST
			ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrNameLst(ws.getEobDb2CursorArea().getS02952NameDemoInfo().getProviderName());
		}
		// COB_CODE: MOVE SPACES                     TO  NF05-BILL-PVDR-EIN
		//                                               NF05-BILL-PVDR-SSN.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrEin("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrSsn("");
		// COB_CODE: IF  HOLD-PERFORMING-NAME  >  SPACES
		//               MOVE  HOLD-PERFORMING-NAME  TO  NF05-PERF-PROVIDER-NAME
		//           ELSE
		//               MOVE HOLD-PE-PROVIDER-NAME  TO  NF05-PERF-PROVIDER-NAME
		//           END-IF.
		if (Characters.GT_SPACE.test(ws.getWsNf0533WorkArea().getHoldPerformingName())) {
			// COB_CODE: MOVE  HOLD-PERFORMING-NAME  TO  NF05-PERF-PROVIDER-NAME
			ws.getNf05EobRecord().getNf05LineLevelRecord().setPerfProviderName(ws.getWsNf0533WorkArea().getHoldPerformingName());
		} else {
			// COB_CODE: INSPECT HOLD-PE-PROVIDER-NAME CONVERTING LOW-VALUES  TO
			//                   SPACES
			ws.getWsNf0533WorkArea().setHoldPeProviderName(InspectUtil.convert(ws.getWsNf0533WorkArea().getHoldPeProviderNameFormatted(),
					String.valueOf(Types.LOW_CHAR_VAL), Types.SPACE_STRING));
			// COB_CODE: MOVE HOLD-PE-PROVIDER-NAME  TO  NF05-PERF-PROVIDER-NAME
			ws.getNf05EobRecord().getNf05LineLevelRecord().setPerfProviderName(ws.getWsNf0533WorkArea().getHoldPeProviderName());
		}
		// COB_CODE: IF NF05-TOTAL
		//              END-IF
		//           END-IF.
		if (ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecordType().isTotal()) {
			// COB_CODE: IF NF05-PERF-PROVIDER-NAME = SPACES
			//              MOVE NF05-PROVIDER-NAME TO NF05-PERF-PROVIDER-NAME
			//           END-IF
			if (Characters.EQ_SPACE.test(ws.getNf05EobRecord().getNf05LineLevelRecord().getPerfProviderName())) {
				// COB_CODE: MOVE NF05-PROVIDER-NAME TO NF05-PERF-PROVIDER-NAME
				ws.getNf05EobRecord().getNf05LineLevelRecord()
						.setPerfProviderName(ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ProviderName());
			}
		}
		// COB_CODE: IF DB2-LI-ADJD-OVRD1-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD2-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD3-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD4-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD5-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD6-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD7-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD8-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD9-CD = 'BIR' OR
		//              DB2-LI-ADJD-OVRD10-CD = 'BIR'
		//              MOVE SPACES         TO NF05-PATIENT-NAME-LAST
		//           END-IF.
		if (Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd1Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd2Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd3Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd4Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd5Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd6Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd7Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd8Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd9Cd(), "BIR")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02809LineInfo().getAdjdOvrd10Cd(), "BIR")) {
			// COB_CODE: MOVE SPACES         TO NF05-PROVIDER-NAME
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProviderName("");
			// COB_CODE: MOVE SPACES         TO NF05-PERF-PROVIDER-NAME
			ws.getNf05EobRecord().getNf05LineLevelRecord().setPerfProviderName("");
			// COB_CODE: MOVE SPACES         TO NF05-PATIENT-NAME
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05PatientName().initNf05PatientNameSpaces();
			// COB_CODE: MOVE 'BIRTH MOTHER' TO NF05-PATIENT-NAME-FIRST
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05PatientName().setFirst("BIRTH MOTHER");
			// COB_CODE: MOVE SPACES         TO NF05-PATIENT-NAME-LAST
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05PatientName().setLast("");
		}
		// COB_CODE: MOVE WS-CLAIM-ACCUM-CHARGE      TO  NF05-TOTAL-CHARGE.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setTotalCharge(Trunc.toDecimal(ws.getWsRollLogicWork().getClaimAccumulators().getCharge(), 10, 2));
		// COB_CODE: MOVE WS-CLAIM-ACCUM-NOT-COVD    TO  NF05-TOTAL-NON-COVRD.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setTotalNonCovrd(Trunc.toDecimal(ws.getWsRollLogicWork().getClaimAccumulators().getNotCovd(), 10, 2));
		// COB_CODE: MOVE WS-CLAIM-ACCUM-OI-PAID     TO  NF05-TOTAL-OTHR-INSUR.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setTotalOthrInsur(Trunc.toDecimal(ws.getWsRollLogicWork().getClaimAccumulators().getOiPaid(), 10, 2));
		// COB_CODE: MOVE WS-CLAIM-ACCUM-PAT-OWES    TO  NF05-TOTAL-PAT-RESPONS.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setTotalPatRespons(Trunc.toDecimal(ws.getWsRollLogicWork().getClaimAccumulators().getPatOwes(), 10, 2));
		// COB_CODE: MOVE WS-CLAIM-ACCUM-WRITEOFF    TO  NF05-TOTAL-PROV-WRITEOFF.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setTotalProvWriteoff(Trunc.toDecimal(ws.getWsRollLogicWork().getClaimAccumulators().getWriteoff(), 10, 2));
		// COB_CODE: MOVE WS-CLAIM-ACCUM-DEDUCT      TO  NF05-TOTAL-DEDUCT.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setTotalDeduct(Trunc.toDecimal(ws.getWsRollLogicWork().getClaimAccumulators().getDeduct(), 7, 2));
		// COB_CODE: MOVE WS-CLAIM-ACCUM-COINS       TO  NF05-TOTAL-COINSUR.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setTotalCoinsur(Trunc.toDecimal(ws.getWsRollLogicWork().getClaimAccumulators().getCoins(), 9, 2));
		// COB_CODE: MOVE HOLD-CLM-INS-LN-CD         TO  NF05-CLM-INS-LN-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setClmInsLnCd(ws.getWsNf0533WorkArea().getHoldClmInsLnCd());
		// COB_CODE: MOVE DB2-CLM-INS-LN-CD          TO  NF05-CLM-INS-LN-CD.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setClmInsLnCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getClmInsLnCd());
		// COB_CODE: MOVE WS-CLAIM-ACCUM-COPAY       TO  NF05-TOTAL-COPAY.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setTotalCopay(Trunc.toDecimal(ws.getWsRollLogicWork().getClaimAccumulators().getCopay(), 5, 2));
		// COB_CODE: MOVE DB2-PMT-CLM-PD-AM          TO  NF05-TOTAL-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setTotalAmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmPdAm(), 11, 2));
		// COB_CODE: ADD SUBSCRIBER-INTEREST         TO  TOTAL-CHECK-AMOUNT.
		ws.getWsPromptPayWorkFields().setTotalCheckAmount(Trunc
				.toDecimal(ws.getWsPromptPayWorkFields().getSubscriberInterest().add(ws.getWsPromptPayWorkFields().getTotalCheckAmount()), 11, 2));
		// COB_CODE: ADD SUBSCRIBER-INTEREST         TO  NF05-TOTAL-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTotalAmountPaid(Trunc.toDecimal(
				ws.getWsPromptPayWorkFields().getSubscriberInterest().add(ws.getNf05EobRecord().getNf05LineLevelRecord().getTotalAmountPaid()), 11,
				2));
		// COB_CODE: MOVE SUBSCRIBER-INTEREST        TO  NF05-PRMPT-PAY-INT-AM.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord()
				.setNf05PrmptPayIntAm(Trunc.toDecimal(ws.getWsPromptPayWorkFields().getSubscriberInterest(), 9, 2));
		// COB_CODE: MOVE TOTAL-CHECK-AMOUNT         TO  NF05-AMOUNT-PAID.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord()
				.setNf05AmountPaid(Trunc.toDecimal(ws.getWsPromptPayWorkFields().getTotalCheckAmount(), 9, 2));
		// COB_CODE: MOVE TOTAL-CHECK-AMOUNT         TO  NF05-N1-N2-CHECK-TOTAL.
		ws.getNf05EobRecord().getNf05LineLevelRecord().setN1N2CheckTotal(Trunc.toDecimal(ws.getWsPromptPayWorkFields().getTotalCheckAmount(), 11, 2));
		// COB_CODE:      IF TOTAL-CHECK-AMOUNT > 0
		//                   MOVE 'YOU'                   TO  NF05-PAID-TO
		//                ELSE
		//           *                                                              *
		//           *  CHECK AMOUNT WILL BE ZERO OR NEGATIVE
		//           *                                                              *
		//                    END-IF
		//                END-IF.
		if (ws.getWsPromptPayWorkFields().getTotalCheckAmount().compareTo(0) > 0) {
			// COB_CODE: MOVE SPACES TO NF05-GEN-NUMBER
			ws.getNf05EobRecord().getNf05LineLevelRecord().setGenNumber("");
			// COB_CODE: MOVE 'YOU'                   TO  NF05-PAID-TO
			ws.getNf05EobRecord().getNf05LineLevelRecord().setPaidTo("YOU");
		} else {
			//                                                              *
			//  CHECK AMOUNT WILL BE ZERO OR NEGATIVE
			//                                                              *
			// COB_CODE: MOVE SPACES TO NF05-PAID-TO
			ws.getNf05EobRecord().getNf05LineLevelRecord().setPaidTo("");
			// COB_CODE: MOVE 'NOCHECK#' TO NF05-GEN-NUMBER
			ws.getNf05EobRecord().getNf05LineLevelRecord().setGenNumber("NOCHECK#");
			// COB_CODE: IF DB2-PMT-HIST-LOAD-CD = 'L'
			//              MOVE 'MANUAL CK' TO NF05-GEN-NUMBER
			//           ELSE
			//              MOVE 'NOCHECK#' TO NF05-GEN-NUMBER
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd() == 'L') {
				// COB_CODE: MOVE 'MANUAL CK' TO NF05-GEN-NUMBER
				ws.getNf05EobRecord().getNf05LineLevelRecord().setGenNumber("MANUAL CK");
			} else {
				// COB_CODE: MOVE 'NOCHECK#' TO NF05-GEN-NUMBER
				ws.getNf05EobRecord().getNf05LineLevelRecord().setGenNumber("NOCHECK#");
			}
		}
		// COB_CODE: IF DB2-PMT-CK-ID(1:5) = 'PRIME'
		//              MOVE DB2-PMT-CK-ID TO NF05-GEN-NUMBER
		//           END-IF.
		if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtCkIdFormatted().substring((1) - 1, 5), "PRIME")) {
			// COB_CODE: MOVE SPACES TO NF05-PAID-TO
			ws.getNf05EobRecord().getNf05LineLevelRecord().setPaidTo("");
			// COB_CODE: MOVE DB2-PMT-CK-ID TO NF05-GEN-NUMBER
			ws.getNf05EobRecord().getNf05LineLevelRecord().setGenNumber(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtCkId());
		}
		// COB_CODE: MOVE WS-BUSINESS-JULIAN-DATE    TO  NF05-GEN-DATE.
		ws.getNf05EobRecord().getNf05LineLevelRecord()
				.setGenDateFormatted(ws.getWsDateFields().getFillerWsDateFields1().getWsBusinessJulianDateFormatted());
		// COB_CODE: MOVE DB2-PMT-ALPH-PRFX-CD       TO  NF05-INSURED-ID-PREFIX.
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord()
				.setNf05InsuredIdPrefix(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAlphPrfxCd());
		// COB_CODE: IF  HOLD-PATIENT-NAME  >  SPACES
		//               MOVE HOLD-PATIENT-NAME      TO  NF05-PATIENT-NAME
		//           END-IF.
		if (Characters.GT_SPACE.test(ws.getWsNf0533WorkArea().getHoldPatientName())) {
			// COB_CODE: MOVE HOLD-PATIENT-NAME      TO  NF05-PATIENT-NAME
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05PatientName()
					.setNf05PatientNameFormatted(ws.getWsNf0533WorkArea().getHoldPatientNameFormatted());
		}
		// COB_CODE: MOVE EOB-CUR-CURSOR-AREA        TO  EOB-DB2-CURSOR-AREA.
		ws.getEobDb2CursorArea().setEobDb2CursorAreaFormatted(ws.getEobCurCursorAreaFormatted());
		// COB_CODE: MOVE SPACES                     TO  HOLD-PE-PROVIDER-NAME.
		ws.getWsNf0533WorkArea().setHoldPeProviderName("");
	}

	/**Original name: 7000-CALL-NF0531SR<br>
	 * <pre>    DISPLAY '** 7000-CALL-NF0531SR **'.</pre>*/
	private void callNf0531sr() {
		// COB_CODE: INITIALIZE WS-SUBROUTINE.
		initWsSubroutine();
		// COB_CODE: MOVE 'N' TO WS-END-OF-LINES.
		ws.getWsExpWorkItems().setWsEndOfLinesFormatted("N");
		// COB_CODE: MOVE DB2-PMT-ASG-CD             TO SBR-ASSIGNED-INDC.
		ws.getWsSubroutine().getSbrHeaderData().getAssignedIndc().setAssignedIndc(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd());
		// COB_CODE: MOVE DB2-PMT-HIST-LOAD-CD       TO SBR-HISTORY-LOAD-INDC.
		ws.getWsSubroutine().getSbrHeaderData().getHistoryLoadIndc()
				.setHistoryLoadIndc(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd());
		// COB_CODE: IF DB2-PMT-ITS-CLM-TYP-CD = 'M'
		//               MOVE SPACES                 TO SBR-ITS-CLAIM-TYPE
		//           ELSE
		//               MOVE DB2-PMT-ITS-CLM-TYP-CD TO SBR-ITS-CLAIM-TYPE
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd() == 'M') {
			// COB_CODE: MOVE SPACES                 TO SBR-ITS-CLAIM-TYPE
			ws.getWsSubroutine().getSbrHeaderData().getItsClaimType().setItsClaimType(Types.SPACE_CHAR);
		} else {
			// COB_CODE: MOVE DB2-PMT-ITS-CLM-TYP-CD TO SBR-ITS-CLAIM-TYPE
			ws.getWsSubroutine().getSbrHeaderData().getItsClaimType()
					.setItsClaimType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd());
		}
		// COB_CODE: MOVE DB2-PMT-TYP-GRP-CD TO SBR-TYP-GRP-CD.
		ws.getWsSubroutine().getSbrHeaderData().setTypGrpCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd());
		// COB_CODE: IF DB2-CLM-PMT-ID > 1
		//              END-IF
		//           ELSE
		//                     TO WS-GL-SOTE-ORIG-CD
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02801Info().getClmPmtId() > 1) {
			// COB_CODE: MOVE 1 TO WS-LINE-NUM
			ws.getWsExpWorkItems().setWsLineNum(((short) 1));
			// COB_CODE: MOVE 'N' TO GET-VOID-FOR-EXP
			ws.getWsSwitches().setGetVoidForExpFormatted("N");
			// COB_CODE: PERFORM 3500-SELECT-VOID-DATA
			selectVoidData();
			// COB_CODE: IF WS-END-OF-LINES = 'Y'
			//              PERFORM 9995-PROCESS-ERROR THRU 9995-EXIT
			//           ELSE
			//                   TO WS-GL-SOTE-ORIG-CD
			//           END-IF
			if (ws.getWsExpWorkItems().getWsEndOfLines() == 'Y') {
				// COB_CODE: MOVE 'N' TO EXPENSE-SW
				ws.getWsSwitches().setExpenseSwFormatted("N");
				// COB_CODE: MOVE 'Y' TO BYPASS-SW
				ws.getWsSwitches().setBypassSwFormatted("Y");
				// COB_CODE: MOVE 'Y' TO ATTN-SYS-SW
				ws.getWsSwitches().setAttnSysSwFormatted("Y");
				// COB_CODE: MOVE VOID-NOT-FOUND-MSG TO ERR-MSG-TEXT
				errFileTO.setErrMsgText(ws.getNf07ErrorMessages().getVoidNotFoundMsg());
				// COB_CODE: PERFORM 9995-PROCESS-ERROR THRU 9995-EXIT
				processError();
			} else {
				// COB_CODE: MOVE 'Y'                  TO SBR-VOID-CD(1)
				ws.getWsSubroutine().getSbrVoidRepayData(1).setVoidCdFormatted("Y");
				// COB_CODE: MOVE VOID-PMT-ADJ-TYP-CD  TO SBR-ADJUSTMENT-TYPE(1)
				ws.getWsSubroutine().getSbrVoidRepayData(1).getAdjustmentType()
						.setAdjustmentType(ws.getVoidInformation().getVoidS02813Info().getPmtAdjTypCd());
				// COB_CODE: MOVE VOID-CLM-PD-AM       TO SBR-TOTAL-AMOUNT-PAID(1)
				ws.getWsSubroutine().getSbrVoidRepayData(1)
						.setTotalAmountPaid(Trunc.toDecimal(ws.getVoidInformation().getVoidS02813Info().getClmPdAm(), 11, 2));
				// COB_CODE: MOVE ' '                 TO SBR-VOID-CD(2)
				ws.getWsSubroutine().getSbrVoidRepayData(2).setVoidCdFormatted(" ");
				// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO SBR-ADJUSTMENT-TYPE(2)
				ws.getWsSubroutine().getSbrVoidRepayData(2).getAdjustmentType()
						.setAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
				// COB_CODE: MOVE DB2-PMT-CLM-PD-AM   TO SBR-TOTAL-AMOUNT-PAID(2)
				ws.getWsSubroutine().getSbrVoidRepayData(2)
						.setTotalAmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmPdAm(), 11, 2));
				// COB_CODE: CALL NF0531SR-PGM USING SUBROUTINE-RECORD
				DynamicCall.invoke(ws.getWsConstants().getNf0531srPgm(), ws.getWsSubroutine());
				// COB_CODE: MOVE SBR-GEN-LDGR-OFFSET-INDC(1)
				//                TO WS-GL-OFST-VOID-CD
				ws.getWsExpWorkItems().setWsGlOfstVoidCd(ws.getWsSubroutine().getSbrGenLdgrOffsetIndc(1));
				// COB_CODE: MOVE SBR-REPORT-INDC(1)
				//                TO WS-GL-SOTE-VOID-CD
				ws.getWsExpWorkItems().setWsGlSoteVoidCd(ws.getWsSubroutine().getSbrReportIndc(1));
				// COB_CODE: IF WS-TEST-FLAG = 'Y'
				//                      ' CLAIM: ' DB2-CLM-CNTL-ID
				//           ELSE
				//              PERFORM 7020-UPDATE-2813-VOID THRU 7020-EXIT
				//           END-IF
				if (ws.getWsRestartStuff().getWsRestartJobParms().getTestFlag() == 'Y') {
					// COB_CODE: DISPLAY 'UPDATE VOID GL CODES'
					//                   ' CLAIM: ' DB2-CLM-CNTL-ID
					DisplayUtil.sysout.write("UPDATE VOID GL CODES", " CLAIM: ", ws.getEobDb2CursorArea().getS02801Info().getClmCntlIdFormatted());
				} else {
					// COB_CODE: PERFORM 7020-UPDATE-2813-VOID THRU 7020-EXIT
					update2813Void();
				}
				// COB_CODE: MOVE SBR-GEN-LDGR-OFFSET-INDC(2)
				//                TO WS-GL-OFST-ORIG-CD
				ws.getWsExpWorkItems().setWsGlOfstOrigCd(ws.getWsSubroutine().getSbrGenLdgrOffsetIndc(2));
				// COB_CODE: MOVE SBR-REPORT-INDC(2)
				//                TO WS-GL-SOTE-ORIG-CD
				ws.getWsExpWorkItems().setWsGlSoteOrigCd(ws.getWsSubroutine().getSbrReportIndc(2));
			}
		} else {
			// COB_CODE: MOVE ' '                 TO SBR-VOID-CD(1)
			ws.getWsSubroutine().getSbrVoidRepayData(1).setVoidCdFormatted(" ");
			// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD  TO SBR-ADJUSTMENT-TYPE(1)
			ws.getWsSubroutine().getSbrVoidRepayData(1).getAdjustmentType()
					.setAdjustmentType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
			// COB_CODE: MOVE DB2-PMT-CLM-PD-AM   TO SBR-TOTAL-AMOUNT-PAID(1)
			ws.getWsSubroutine().getSbrVoidRepayData(1)
					.setTotalAmountPaid(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmPdAm(), 11, 2));
			// COB_CODE: CALL NF0531SR-PGM USING SUBROUTINE-RECORD
			DynamicCall.invoke(ws.getWsConstants().getNf0531srPgm(), ws.getWsSubroutine());
			// COB_CODE: MOVE SBR-GEN-LDGR-OFFSET-INDC(1)
			//                  TO WS-GL-OFST-ORIG-CD
			ws.getWsExpWorkItems().setWsGlOfstOrigCd(ws.getWsSubroutine().getSbrGenLdgrOffsetIndc(1));
			// COB_CODE: MOVE SBR-REPORT-INDC(1)
			//                  TO WS-GL-SOTE-ORIG-CD
			ws.getWsExpWorkItems().setWsGlSoteOrigCd(ws.getWsSubroutine().getSbrReportIndc(1));
		}
		// COB_CODE: IF DB2-CLM-PMT-ID > 1 AND
		//                  WS-END-OF-LINES = 'N'
		//                     UNTIL WS-END-OF-LINES = 'Y'
		//           END-IF.
		if (ws.getEobDb2CursorArea().getS02801Info().getClmPmtId() > 1 && ws.getWsExpWorkItems().getWsEndOfLines() == 'N') {
			// COB_CODE: MOVE 1 TO WS-LINE-NUM
			ws.getWsExpWorkItems().setWsLineNum(((short) 1));
			// COB_CODE: MOVE 'Y' TO GET-VOID-FOR-EXP
			ws.getWsSwitches().setGetVoidForExpFormatted("Y");
			// COB_CODE: PERFORM 3500-SELECT-VOID-DATA
			//                  UNTIL WS-END-OF-LINES = 'Y'
			while (!(ws.getWsExpWorkItems().getWsEndOfLines() == 'Y')) {
				selectVoidData();
			}
		}
	}

	/**Original name: 7020-UPDATE-2813-VOID<br>
	 * <pre>    DISPLAY '** 7020-UPDATE-2813-VOID **'.</pre>*/
	private void update2813Void() {
		// COB_CODE: EXEC SQL
		//             UPDATE S02813SA
		//                 SET GL_OFST_VOID_CD  = :WS-GL-OFST-VOID-CD
		//                   , GL_SOTE_VOID_CD  = :WS-GL-SOTE-VOID-CD
		//                   , INFO_CHG_ID      = 'NF0533ML'
		//                   , INFO_CHG_TS      = CURRENT_TIMESTAMP
		//               WHERE CLM_CNTL_ID      = :DB2-CLM-CNTL-ID
		//                 AND CLM_CNTL_SFX_ID  = :DB2-CLM-CNTL-SFX-ID
		//                 AND CLM_PMT_ID       = :VOID-CLM-PMT-ID
		//           END-EXEC.
		s02813saDao.updateRec(ws);
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 ADD 1 TO WS-S02813-VOID-UPDATE
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: ADD 1 TO WS-S02813-VOID-UPDATE
			ws.getWsAllCounters().getWsCounters()
					.setWsS02813VoidUpdate(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsS02813VoidUpdate(), 7));
			break;

		default:// COB_CODE: MOVE 'S02813SA'              TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S02813SA");
			// COB_CODE: MOVE 'UPDATE S02813SA'       TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("UPDATE S02813SA");
			// COB_CODE: MOVE '7020-UPDATE-2813-VOID' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("7020-UPDATE-2813-VOID");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 7200-CALL-NU0201ML<br>
	 * <pre>                                                              *
	 *     DISPLAY '** 7200-CALL-NU0201ML **'.
	 *                                                               *</pre>*/
	private void callNu0201ml() {
		// COB_CODE: MOVE 'N' TO WS-LOB-PROV-FOUND-SW.
		ws.getWsSwitches().setWsLobProvFoundSw(false);
		// COB_CODE: MOVE SPACES TO PROV-PASSED-INDIC-REC.
		ws.getProvPassedIndicRec().initProvPassedIndicRecSpaces();
		// COB_CODE: MOVE HOLD-PROVIDER-NUM TO PROV-ID OF PROV-PASSED-INDIC-REC.
		ws.getProvPassedIndicRec().getProvPassedIndicInput().setId(ws.getWsNf0533WorkArea().getHoldProviderNum());
		// COB_CODE: MOVE HOLD-PROVIDER-LOB TO PROV-LOB-CD
		//                                             OF PROV-PASSED-INDIC-REC.
		ws.getProvPassedIndicRec().getProvPassedIndicInput().setLobCdFormatted(ws.getWsNf0533WorkArea().getHoldProviderLobFormatted());
		// COB_CODE: MOVE 'R1'              TO PROV-AD-CD.
		ws.getProvPassedIndicRec().getProvPassedIndicInput().setAdCd("R1");
		// COB_CODE: IF DB2-BI-PROV-ID = DB2-PE-PROV-ID
		//                                             OF PROV-PASSED-INDIC-REC
		//           ELSE
		//                                             OF PROV-PASSED-INDIC-REC
		//           END-IF.
		if (Conditions.eq(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvId(),
				ws.getEobDb2CursorArea().getS02815ProviderInfo().getPeProvId())) {
			// COB_CODE: MOVE DB2-PMT-PRIM-CN-ARNG-CD TO PROV-CN-ARNG-CD
			//                                         OF PROV-PASSED-INDIC-REC
			ws.getProvPassedIndicRec().getProvPassedIndicInput().setCnArngCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPrimCnArngCd());
		} else {
			// COB_CODE: MOVE SPACES TO PROV-CN-ARNG-CD
			//                                         OF PROV-PASSED-INDIC-REC
			ws.getProvPassedIndicRec().getProvPassedIndicInput().setCnArngCd("");
		}
		// COB_CODE: CALL NU0201ML USING PROV-PASSED-INDIC-REC.
		DynamicCall.invoke(ws.getNu0201ml(), ws.getProvPassedIndicRec());
		// COB_CODE: IF PROGRAM-STAT  OF INDIC-STATUS-CODES < 500
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 550
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 551
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 552
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 700
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 750
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 810
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 820
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 830
		//               MOVE 'Y' TO WS-LOB-PROV-FOUND-SW
		//           ELSE
		//               END-IF
		//           END-IF.
		if (Conditions.lt(ws.getProvPassedIndicRec().getProgramStat(), "500") || Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "550")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "551")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "552")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "700")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "750")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "810")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "820")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "830")) {
			// COB_CODE: MOVE 'Y' TO WS-LOB-PROV-FOUND-SW
			ws.getWsSwitches().setWsLobProvFoundSw(true);
		} else if (Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "600")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "900")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "999")) {
			// COB_CODE: IF PROGRAM-STAT OF INDIC-STATUS-CODES = 600
			//           OR PROGRAM-STAT OF INDIC-STATUS-CODES = 900
			//           OR PROGRAM-STAT OF INDIC-STATUS-CODES = 999
			//               END-IF
			//           ELSE
			//               PERFORM 9998-ABEND-FROM-NU0201ML THRU 9998-EXIT
			//           END-IF
			// COB_CODE: MOVE 'N' TO WS-LOB-PROV-FOUND-SW
			ws.getWsSwitches().setWsLobProvFoundSw(false);
			// COB_CODE: IF DB2-LI-EXMN-ACTN-CD NOT = 'C'
			//               DISPLAY '  '
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() != 'C') {
				// COB_CODE: DISPLAY ' 7200-CALL-NU0201ML, '
				//              'LOB/PVDR NOT FOUND: '  PROV-LOB-CD
				//                           OF PROV-PASSED-INDIC-REC
				//                           ' - '
				//                PROV-ID OF PROV-PASSED-INDIC-REC
				DisplayUtil.sysout.write(" 7200-CALL-NU0201ML, ", "LOB/PVDR NOT FOUND: ",
						String.valueOf(ws.getProvPassedIndicRec().getProvPassedIndicInput().getLobCd()), " - ",
						ws.getProvPassedIndicRec().getProvPassedIndicInput().getIdFormatted());
				// COB_CODE: DISPLAY ' FOR KEY:  '   NEW-KEY
				DisplayUtil.sysout.write(" FOR KEY:  ", ws.getWsNf0533WorkArea().getNewKey().getNewKeyFormatted());
				// COB_CODE: DISPLAY '  '
				DisplayUtil.sysout.write("  ");
			}
		} else {
			// COB_CODE: MOVE '***      7200-CALL-NU0201ML       ***'
			//               TO WS-DISP-ABENDING-PARAGRAPH
			ws.getWsDisplayWorkArea().setWsDispAbendingParagraph("***      7200-CALL-NU0201ML       ***");
			// COB_CODE: PERFORM 9998-ABEND-FROM-NU0201ML THRU 9998-EXIT
			abendFromNu0201ml();
		}
		// COB_CODE: IF LOB-PROV-WAS-FOUND
		//                   END-IF
		//           ELSE
		//                              HOLD-RA-ADDR3
		//           END-IF.
		if (ws.getWsSwitches().isWsLobProvFoundSw()) {
			// COB_CODE: MOVE PROV-ADDR-ATTN      TO HOLD-RA-ADDR1
			ws.getWsNf0533WorkArea().getHoldRaAddress().setAddr1(ws.getProvPassedIndicRec().getProvAddrArea().getAddrAttn());
			// COB_CODE: MOVE PROV-ADDR-STREET    TO HOLD-RA-ADDR2
			ws.getWsNf0533WorkArea().getHoldRaAddress().setAddr2(ws.getProvPassedIndicRec().getProvAddrArea().getAddrStreet());
			// COB_CODE: MOVE PROV-ADDR-CITY      TO HOLD-RA-ADDR3-CITY
			ws.getWsNf0533WorkArea().getHoldRaAddress().setAddr3City(ws.getProvPassedIndicRec().getProvAddrArea().getAddrCity());
			// COB_CODE: MOVE PROV-ADDR-ST        TO HOLD-RA-ADDR3-ST
			ws.getWsNf0533WorkArea().getHoldRaAddress().setAddr3St(ws.getProvPassedIndicRec().getProvAddrArea().getAddrSt());
			// COB_CODE: MOVE PROV-ADDR-ZIP       TO HOLD-RA-ADDR3-ZIP
			ws.getWsNf0533WorkArea().getHoldRaAddress().setAddr3Zip(ws.getProvPassedIndicRec().getProvAddrArea().getAddrZipFormatted());
			// COB_CODE: IF PROV-LOB-CD OF PROV-PASSED-INDIC-REC = '1'
			//               MOVE PROV-INST-NAME TO HOLD-PROVIDER-NAME
			//           ELSE
			//               END-IF
			if (ws.getProvPassedIndicRec().getProvPassedIndicInput().getLobCd() == '1') {
				// COB_CODE: MOVE PROV-INST-NAME TO HOLD-PROVIDER-NAME
				ws.getWsNf0533WorkArea().setHoldProviderName(ws.getProvPassedIndicRec().getProvName1().getInstName());
			} else if (ws.getProvPassedIndicRec().getProvPassedIndicInput().getLobCd() == '3'
					&& Characters.EQ_SPACE.test(ws.getProvPassedIndicRec().getProvName1().getlName())) {
				// COB_CODE: IF PROV-LOB-CD OF PROV-PASSED-INDIC-REC = '3' AND
				//              PROV-L-NAME = SPACES
				//              MOVE PROV-SUPL-NAME TO HOLD-PROVIDER-NAME
				//           ELSE
				//              MOVE WS-PRV-NAME TO HOLD-PROVIDER-NAME
				//           END-IF
				// COB_CODE: MOVE PROV-SUPL-NAME TO HOLD-PROVIDER-NAME
				ws.getWsNf0533WorkArea().setHoldProviderName(ws.getProvPassedIndicRec().getProvName1().getSuplName());
			} else {
				// COB_CODE: MOVE PROV-L-NAME TO WS-PRV-LAST
				ws.getWsNf0533WorkArea().getWsPrvName().setLast(ws.getProvPassedIndicRec().getProvName1().getlName());
				// COB_CODE: MOVE PROV-F-NAME TO WS-PRV-FIRST
				ws.getWsNf0533WorkArea().getWsPrvName().setFirst(ws.getProvPassedIndicRec().getProvName1().getfName());
				// COB_CODE: MOVE PROV-M-INIT TO WS-PRV-INIT
				ws.getWsNf0533WorkArea().getWsPrvName().setInit(ws.getProvPassedIndicRec().getProvName1().getmInit());
				// COB_CODE: MOVE WS-PRV-NAME TO HOLD-PROVIDER-NAME
				ws.getWsNf0533WorkArea().setHoldProviderName(ws.getWsNf0533WorkArea().getWsPrvName().getWsPrvNameFormatted());
			}
		} else {
			// COB_CODE: MOVE SPACES TO HOLD-PROVIDER-NAME
			//                          HOLD-PE-PROVIDER-NAME
			//                          HOLD-RA-ADDR1
			//                          HOLD-RA-ADDR2
			//                          HOLD-RA-ADDR3
			ws.getWsNf0533WorkArea().setHoldProviderName("");
			ws.getWsNf0533WorkArea().setHoldPeProviderName("");
			ws.getWsNf0533WorkArea().getHoldRaAddress().setAddr1("");
			ws.getWsNf0533WorkArea().getHoldRaAddress().setAddr2("");
			ws.getWsNf0533WorkArea().getHoldRaAddress().initAddr3Spaces();
		}
	}

	/**Original name: 7250-CALL-NU0201ML<br>
	 * <pre>                                                              *
	 *     DISPLAY '**7250-CALL-NU0201ML FOR PERFORMING PROV NAME**'.
	 *                                                               *</pre>*/
	private void callNu0201ml1() {
		// COB_CODE: MOVE 'N' TO WS-LOB-PROV-FOUND-SW.
		ws.getWsSwitches().setWsLobProvFoundSw(false);
		// COB_CODE: MOVE SPACES TO PROV-PASSED-INDIC-REC.
		ws.getProvPassedIndicRec().initProvPassedIndicRecSpaces();
		// COB_CODE: MOVE HOLD-PROVIDER-NUM TO PROV-ID OF PROV-PASSED-INDIC-REC.
		ws.getProvPassedIndicRec().getProvPassedIndicInput().setId(ws.getWsNf0533WorkArea().getHoldProviderNum());
		// COB_CODE: MOVE HOLD-PROVIDER-LOB TO PROV-LOB-CD
		//                                             OF PROV-PASSED-INDIC-REC.
		ws.getProvPassedIndicRec().getProvPassedIndicInput().setLobCdFormatted(ws.getWsNf0533WorkArea().getHoldProviderLobFormatted());
		// COB_CODE: CALL NU0201ML USING PROV-PASSED-INDIC-REC.
		DynamicCall.invoke(ws.getNu0201ml(), ws.getProvPassedIndicRec());
		// COB_CODE: IF PROGRAM-STAT  OF INDIC-STATUS-CODES < 500
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 550
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 551
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 552
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 600
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 700
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 750
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 810
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 820
		//           OR PROGRAM-STAT  OF INDIC-STATUS-CODES = 830
		//               MOVE 'Y' TO WS-LOB-PROV-FOUND-SW
		//           ELSE
		//               END-IF
		//           END-IF.
		if (Conditions.lt(ws.getProvPassedIndicRec().getProgramStat(), "500") || Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "550")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "551")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "552")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "600")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "700")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "750")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "810")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "820")
				|| Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "830")) {
			// COB_CODE: MOVE 'Y' TO WS-LOB-PROV-FOUND-SW
			ws.getWsSwitches().setWsLobProvFoundSw(true);
		} else if (Conditions.eq(ws.getProvPassedIndicRec().getProgramStat(), "900")) {
			// COB_CODE: IF PROGRAM-STAT OF INDIC-STATUS-CODES = 900
			//               END-IF
			//           ELSE
			//               PERFORM 9998-ABEND-FROM-NU0201ML THRU 9998-EXIT
			//           END-IF
			// COB_CODE: MOVE 'N' TO WS-LOB-PROV-FOUND-SW
			ws.getWsSwitches().setWsLobProvFoundSw(false);
			// COB_CODE: IF DB2-LI-EXMN-ACTN-CD NOT = 'C'
			//               DISPLAY '  '
			//           END-IF
			if (ws.getEobDb2CursorArea().getS02809LineInfo().getExmnActnCd() != 'C') {
				// COB_CODE: DISPLAY ' 7250-CALL-NU0201ML, '
				//              'LOB/PVDR NOT FOUND: '  PROV-LOB-CD
				//                                 OF PROV-PASSED-INDIC-REC
				//                                 ' - '
				//                PROV-ID OF PROV-PASSED-INDIC-REC
				DisplayUtil.sysout.write(" 7250-CALL-NU0201ML, ", "LOB/PVDR NOT FOUND: ",
						String.valueOf(ws.getProvPassedIndicRec().getProvPassedIndicInput().getLobCd()), " - ",
						ws.getProvPassedIndicRec().getProvPassedIndicInput().getIdFormatted());
				// COB_CODE: DISPLAY ' FOR KEY:  '   NEW-KEY
				DisplayUtil.sysout.write(" FOR KEY:  ", ws.getWsNf0533WorkArea().getNewKey().getNewKeyFormatted());
				// COB_CODE: DISPLAY '  '
				DisplayUtil.sysout.write("  ");
			}
		} else {
			// COB_CODE: MOVE '***      7250-CALL-NU0201ML       ***'
			//               TO WS-DISP-ABENDING-PARAGRAPH
			ws.getWsDisplayWorkArea().setWsDispAbendingParagraph("***      7250-CALL-NU0201ML       ***");
			// COB_CODE: PERFORM 9998-ABEND-FROM-NU0201ML THRU 9998-EXIT
			abendFromNu0201ml();
		}
		// COB_CODE: IF PROV-LOB-CD OF PROV-PASSED-INDIC-REC = '3' AND
		//              PROV-L-NAME = SPACES
		//              MOVE PROV-SUPL-NAME TO HOLD-PE-PROVIDER-NAME
		//           ELSE
		//              MOVE PROV-L-NAME TO HOLD-PE-PROVIDER-NAME
		//           END-IF.
		if (ws.getProvPassedIndicRec().getProvPassedIndicInput().getLobCd() == '3'
				&& Characters.EQ_SPACE.test(ws.getProvPassedIndicRec().getProvName1().getlName())) {
			// COB_CODE: MOVE PROV-SUPL-NAME TO HOLD-PE-PROVIDER-NAME
			ws.getWsNf0533WorkArea().setHoldPeProviderName(ws.getProvPassedIndicRec().getProvName1().getSuplName());
		} else {
			// COB_CODE: MOVE PROV-L-NAME TO HOLD-PE-PROVIDER-NAME
			ws.getWsNf0533WorkArea().setHoldPeProviderName(ws.getProvPassedIndicRec().getProvName1().getlName());
		}
	}

	/**Original name: 8005-READ-RESTART-CARD<br>
	 * <pre>                                                              *
	 *     DISPLAY '** 8005-READ-RESTART-CARD **'.
	 *                                                               *</pre>*/
	private void readRestartCard() {
		// COB_CODE: READ RESTART-CARD
		//             INTO WS-RESTART-JOB-PARMS
		//               AT END
		//                  CALL 'ABEND'.
		restartCardTo = restartCardDAO.read(restartCardTo);
		if (restartCardDAO.getFileStatus().isSuccess()) {
			ws.getWsRestartStuff().getWsRestartJobParms().setWsRestartJobParmsBytes(restartCardTo.getData());
		}
		if (restartCardDAO.getFileStatus().isEOF()) {
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
			// COB_CODE: DISPLAY '  JOB PARMS FILE IS EMPTY     '
			DisplayUtil.sysout.write("  JOB PARMS FILE IS EMPTY     ");
			// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
			// COB_CODE: CALL 'ABEND'.
			DynamicCall.invoke("ABEND");
		}
		// COB_CODE: IF WS-TEST-FLAG = 'Y'
		//              DISPLAY '******************************'
		//           END-IF.
		if (ws.getWsRestartStuff().getWsRestartJobParms().getTestFlag() == 'Y') {
			// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
			// COB_CODE: DISPLAY '           WARNING!           '
			DisplayUtil.sysout.write("           WARNING!           ");
			// COB_CODE: DISPLAY '    PROGRAM IS IN TEST MODE   '
			DisplayUtil.sysout.write("    PROGRAM IS IN TEST MODE   ");
			// COB_CODE: DISPLAY '   DB2 UPDATES ARE TURNED OFF '
			DisplayUtil.sysout.write("   DB2 UPDATES ARE TURNED OFF ");
			// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
		}
	}

	/**Original name: 8010-GET-BUSINESS-DATE<br>
	 * <pre>                                                                *
	 *     DISPLAY '** 8010-GET-BUSINESS-DATE **'.
	 *                                                                 *</pre>*/
	private void getBusinessDate() {
		// COB_CODE: READ BUSINESS-DATE
		//             AT END
		//               MOVE 'YES' TO EOF-BUSDATE.
		businessDateTO = businessDateDAO.read(businessDateTO);
		if (businessDateDAO.getFileStatus().isEOF()) {
			// COB_CODE: MOVE 'YES' TO EOF-BUSDATE.
			ws.getWsSwitches().setEofBusdate("YES");
		}
		// COB_CODE: MOVE CURRENT-DATE        TO WS-BUSINESS-DATE.
		ws.getWsDateFields().getWsBusinessDate()
				.setWsBusinessDateFormatted(businessDateTO.getDateRecord().getFillerDateRecord().getCurrentDateFormatted());
		// COB_CODE: MOVE CURRENT-CCYY        TO WS-BUSINESS-JULIAN-YEAR.
		ws.getWsDateFields().getFillerWsDateFields1().setYear(businessDateTO.getDateRecord().getFillerDateRecord().getCcyyFormatted());
		// COB_CODE: MOVE CURRENT-JULIAN-DAY  TO WS-BUSINESS-JULIAN-DAY.
		ws.getWsDateFields().getFillerWsDateFields1().setDayFormatted(businessDateTO.getDateRecord().getCurrentJulianDayFormatted());
		// COB_CODE: MOVE CURRENT-CCYY        TO WS-BUSINESS-CCYY-N.
		ws.getWsDateFields().getFillerWsDateFields().setCcyyNFromBuffer(businessDateTO.getDateRecord().getFillerDateRecord().getCcyyBytes());
		// COB_CODE: MOVE CURRENT-MM          TO WS-BUSINESS-MM-N.
		ws.getWsDateFields().getFillerWsDateFields().setMmNFormatted(businessDateTO.getDateRecord().getFillerDateRecord().getMmFormatted());
		// COB_CODE: MOVE CURRENT-DD          TO WS-BUSINESS-DD-N.
		ws.getWsDateFields().getFillerWsDateFields().setDdNFormatted(businessDateTO.getDateRecord().getFillerDateRecord().getDdFormatted());
		// COB_CODE: DISPLAY 'BUSINESS DATE = ' WS-BUSINESS-DATE
		//                                  ' ' WS-BUSINESS-JULIAN-DATE
		//                                  ' ' WS-BUSINESS-DATE-9.
		DisplayUtil.sysout.write(new String[] { "BUSINESS DATE = ", ws.getWsDateFields().getWsBusinessDate().getWsBusinessDateFormatted(), " ",
				ws.getWsDateFields().getFillerWsDateFields1().getWsBusinessJulianDateAsString(), " ",
				ws.getWsDateFields().getFillerWsDateFields().getWsBusinessDate9AsString() });
	}

	/**Original name: 8100-READ-TRIGGER-IN<br>
	 * <pre>                                                                *
	 *     DISPLAY '** 8100-READ-TRIGGER-IN **'.
	 *                                                                 *</pre>*/
	private void readTriggerIn() {
		// COB_CODE: READ TRIGGER-IN INTO TRIGGER-RECORD-LAYOUT
		//             AT END
		//               MOVE 'YES' TO EOF-TRIGGER-SW.
		triggerInTO = triggerInDAO.read(triggerInTO);
		if (triggerInDAO.getFileStatus().isEOF()) {
			// COB_CODE: MOVE 'YES' TO EOF-TRIGGER-SW.
			ws.getWsSwitches().setEofTriggerSw("YES");
		}
		// COB_CODE: IF EOF-TRIGGER-SW = 'YES'
		//                         WS-TRIGGER-LOADED
		//           ELSE
		//               ADD 1 TO WS-TRIGGER-IN
		//           END-IF.
		if (Conditions.eq(ws.getWsSwitches().getEofTriggerSw(), "YES")) {
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY ' TOTAL TRIGGER RECORDS READ    : '
			//                     WS-TRIGGER-IN
			DisplayUtil.sysout.write(" TOTAL TRIGGER RECORDS READ    : ", ws.getWsAllCounters().getWsCounters().getWsTriggerInAsString());
			// COB_CODE: DISPLAY ' TOTAL TEMP TABLE ROWS INSERTED: '
			//                     WS-TRIGGER-LOADED
			DisplayUtil.sysout.write(" TOTAL TEMP TABLE ROWS INSERTED: ", ws.getWsAllCounters().getWsCounters().getWsTriggerLoadedAsString());
		} else {
			// COB_CODE: ADD 1 TO WS-TRIGGER-IN
			ws.getWsAllCounters().getWsCounters().setWsTriggerIn(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsTriggerIn(), 7));
		}
	}

	/**Original name: 8600-WRITE-EXPENSE-FILE<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 8600-WRITE-EXPENSE-FILE **'.
	 *                                                             *</pre>*/
	private void writeExpenseFile() {
		// COB_CODE: IF VOID-REPAY
		//              MOVE VOID-PMT-NTWRK-CD        TO EXP-NETWORK-CODE
		//           ELSE
		//              MOVE DB2-PMT-NTWRK-CD         TO EXP-NETWORK-CODE
		//           END-IF.
		if (ws.getWsSwitches().isVoidRepay()) {
			// COB_CODE: MOVE VOID-CLM-CNTL-ID         TO EXP-CLAIM-ID
			ws.getExpenseRecord().setExpClaimId(ws.getVoidInformation().getVoidS02813Info().getClmCntlId());
			// COB_CODE: MOVE VOID-CLM-CNTL-SFX-ID     TO EXP-CLAIM-ID-SUFFIX
			ws.getExpenseRecord().setExpClaimIdSuffix(ws.getVoidInformation().getVoidS02813Info().getClmCntlSfxId());
			// COB_CODE: MOVE VOID-CLM-PMT-ID          TO EXP-PAYMENT-NUM
			ws.getExpenseRecord().setExpPaymentNum(TruncAbs.toShort(ws.getVoidInformation().getVoidS02813Info().getClmPmtId(), 2));
			// COB_CODE: MOVE VOID-CLI-ID              TO EXP-LINE-NUM
			ws.getExpenseRecord().setExpLineNum(TruncAbs.toShort(ws.getVoidInformation().getVoidS02809LineInfo().getCliId(), 3));
			// COB_CODE: MOVE SPACES                   TO EXP-REIMBURSE-IND
			ws.getExpenseRecord().setExpReimburseInd("");
			// COB_CODE: MOVE WS-GL-OFST-VOID-CD       TO EXP-GEN-LDGR-OFFSET
			ws.getExpenseRecord().setExpGenLdgrOffset(ws.getWsExpWorkItems().getWsGlOfstVoidCd());
			// COB_CODE: MOVE WS-GL-SOTE-VOID-CD       TO EXP-REPORT-INDC
			ws.getExpenseRecord().getExpReportIndc().setExpReportIndc(TruncAbs.toShort(ws.getWsExpWorkItems().getWsGlSoteVoidCd(), 2));
			// COB_CODE: MOVE 'V'                      TO EXP-EXAMINER-ACTION
			ws.getExpenseRecord().getExpExaminerAction().setExpExaminerActionFormatted("V");
			// COB_CODE: MOVE VOID-PMT-KCAPS-TEAM-NM   TO EXP-TEAM
			ws.getExpenseRecord().setExpTeam(ws.getVoidInformation().getVoidS02813Info().getPmtKcapsTeamNm());
			// COB_CODE: MOVE VOID-PMT-KCAPS-USE-ID    TO EXP-ID
			ws.getExpenseRecord().setExpId(ws.getVoidInformation().getVoidS02813Info().getPmtKcapsUseId());
			// COB_CODE: MOVE VOID-PMT-PGM-AREA-CD     TO EXP-PROGRAM-AREA
			ws.getExpenseRecord().setExpProgramArea(ws.getVoidInformation().getVoidS02813Info().getPmtPgmAreaCd());
			// COB_CODE: MOVE VOID-PMT-ENR-CL-CD       TO EXP-ENROLLMENT-CLASS
			ws.getExpenseRecord().setExpEnrollmentClass(ws.getVoidInformation().getVoidS02813Info().getPmtEnrClCd());
			// COB_CODE: MOVE VOID-PMT-FIN-CD          TO EXP-FINANCIAL-CODE
			ws.getExpenseRecord().setExpFinancialCode(ws.getVoidInformation().getVoidS02813Info().getPmtFinCd());
			// COB_CODE: MOVE VOID-PMT-NTWRK-CD        TO EXP-NETWORK-CODE
			ws.getExpenseRecord().setExpNetworkCode(ws.getVoidInformation().getVoidS02813Info().getPmtNtwrkCd());
		} else {
			// COB_CODE: MOVE DB2-CLM-CNTL-ID          TO EXP-CLAIM-ID
			ws.getExpenseRecord().setExpClaimId(ws.getEobDb2CursorArea().getS02801Info().getClmCntlId());
			// COB_CODE: MOVE DB2-CLM-CNTL-SFX-ID      TO EXP-CLAIM-ID-SUFFIX
			ws.getExpenseRecord().setExpClaimIdSuffix(ws.getEobDb2CursorArea().getS02801Info().getClmCntlSfxId());
			// COB_CODE: MOVE DB2-CLI-ID               TO EXP-LINE-NUM
			ws.getExpenseRecord().setExpLineNum(TruncAbs.toShort(ws.getEobDb2CursorArea().getS02801Info().getCliId(), 3));
			// COB_CODE: MOVE DB2-CLM-PMT-ID           TO EXP-PAYMENT-NUM
			ws.getExpenseRecord().setExpPaymentNum(TruncAbs.toShort(ws.getEobDb2CursorArea().getS02801Info().getClmPmtId(), 2));
			// COB_CODE: MOVE SPACES                   TO EXP-REIMBURSE-IND
			ws.getExpenseRecord().setExpReimburseInd("");
			// COB_CODE: MOVE WS-GL-OFST-ORIG-CD       TO EXP-GEN-LDGR-OFFSET
			ws.getExpenseRecord().setExpGenLdgrOffset(ws.getWsExpWorkItems().getWsGlOfstOrigCd());
			// COB_CODE: MOVE WS-GL-SOTE-ORIG-CD       TO EXP-REPORT-INDC
			ws.getExpenseRecord().getExpReportIndc().setExpReportIndc(TruncAbs.toShort(ws.getWsExpWorkItems().getWsGlSoteOrigCd(), 2));
			// COB_CODE: MOVE ' '                      TO EXP-EXAMINER-ACTION
			ws.getExpenseRecord().getExpExaminerAction().setExpExaminerActionFormatted(" ");
			// COB_CODE: MOVE DB2-PMT-KCAPS-TEAM-NM    TO EXP-TEAM
			ws.getExpenseRecord().setExpTeam(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsTeamNm());
			// COB_CODE: MOVE DB2-PMT-KCAPS-USE-ID     TO EXP-ID
			ws.getExpenseRecord().setExpId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsUseId());
			// COB_CODE: MOVE DB2-PMT-PGM-AREA-CD      TO EXP-PROGRAM-AREA
			ws.getExpenseRecord().setExpProgramArea(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPgmAreaCd());
			// COB_CODE: MOVE DB2-PMT-ENR-CL-CD        TO EXP-ENROLLMENT-CLASS
			ws.getExpenseRecord().setExpEnrollmentClass(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtEnrClCd());
			// COB_CODE: MOVE DB2-PMT-FIN-CD           TO EXP-FINANCIAL-CODE
			ws.getExpenseRecord().setExpFinancialCode(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtFinCd());
			// COB_CODE: MOVE DB2-PMT-NTWRK-CD         TO EXP-NETWORK-CODE
			ws.getExpenseRecord().setExpNetworkCode(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtNtwrkCd());
		}
		// COB_CODE: MOVE DB2-PMT-INS-ID              TO EXP-INS-ID.
		ws.getExpenseRecord().setExpInsId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtInsId());
		// COB_CODE: MOVE DB2-BI-PROV-ID              TO EXP-BILL-PROV-ID.
		ws.getExpenseRecord().setExpBillProvId(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvId());
		// COB_CODE: MOVE DB2-BI-PROV-LOB             TO EXP-BILL-PROV-LOB.
		ws.getExpenseRecord().getExpBillProvLob().setExpBillProvLob(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvLob());
		// COB_CODE: MOVE DB2-PMT-ASG-CD              TO EXP-ASSIGNED-INDC.
		ws.getExpenseRecord().getExpAssignedIndc().setExpAssignedIndc(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd());
		// COB_CODE: MOVE WS-DB2-BI-NPI-PROV-ID       TO EXP-NPI-PROV-NO.
		ws.getExpenseRecord().setExpNpiProvNo(ws.getWsNf0533WorkArea().getWsDb2BiNpiProvId());
		// COB_CODE: MOVE 'P'                         TO EXP-WRAP-INDC.
		ws.getExpenseRecord().getExpWrapIndc().setExpWrapIndcFormatted("P");
		// COB_CODE: MOVE DB2-PMT-PMT-BK-PROD-CD      TO EXP-BANK-INDICATOR.
		ws.getExpenseRecord().getExpBankIndicator().setExpBankIndicator(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPmtBkProdCd());
		// COB_CODE: IF DB2-PMT-TYP-GRP-CD = SPACES
		//              MOVE DB2-NPI-CD  TO EXP-CONVRTD-TYP-GRP-CD
		//           ELSE
		//              END-IF
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd())) {
			// COB_CODE: MOVE DB2-NPI-CD  TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCd(ws.getDb2NpiCd());
		} else if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), "A  ")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), " A ")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), "  A")) {
			// COB_CODE: IF DB2-PMT-TYP-GRP-CD = 'A  ' OR ' A ' OR '  A'
			//              MOVE 0 TO EXP-CONVRTD-TYP-GRP-CD
			//           ELSE
			//              END-IF
			//           END-IF
			// COB_CODE: MOVE 0 TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCdFormatted("0");
		} else if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), "2  ")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), " 2 ")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), "  2")) {
			// COB_CODE: IF DB2-PMT-TYP-GRP-CD = '2  ' OR ' 2 ' OR '  2'
			//              MOVE 'L' TO EXP-CONVRTD-TYP-GRP-CD
			//           ELSE
			//              END-IF
			//           END-IF
			// COB_CODE: MOVE 'L' TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCdFormatted("L");
		} else if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), "9  ")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), " 9 ")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), "  9")) {
			// COB_CODE: IF DB2-PMT-TYP-GRP-CD = '9  ' OR ' 9 ' OR '  9'
			//              MOVE 'F' TO EXP-CONVRTD-TYP-GRP-CD
			//           ELSE
			//              END-IF
			//           END-IF
			// COB_CODE: MOVE 'F' TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCdFormatted("F");
		} else if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), "13 ")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), " 13")) {
			// COB_CODE: IF DB2-PMT-TYP-GRP-CD = '13 ' OR ' 13'
			//              MOVE 'P' TO EXP-CONVRTD-TYP-GRP-CD
			//           ELSE
			//              END-IF
			//           END-IF
			// COB_CODE: MOVE 'P' TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCdFormatted("P");
		} else if (Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), "15 ")
				|| Conditions.eq(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtTypGrpCd(), " 15")) {
			// COB_CODE: IF DB2-PMT-TYP-GRP-CD = '15 ' OR ' 15'
			//              MOVE '0' TO EXP-CONVRTD-TYP-GRP-CD
			//           ELSE
			//              MOVE ' ' TO EXP-CONVRTD-TYP-GRP-CD
			//           END-IF
			// COB_CODE: MOVE '0' TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCdFormatted("0");
		} else {
			// COB_CODE: MOVE ' ' TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCdFormatted(" ");
		}
		// COB_CODE: MOVE DB2-PMT-HIST-LOAD-CD        TO EXP-HISTORY-LOAD-IND.
		ws.getExpenseRecord().setExpHistoryLoadInd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd());
		// COB_CODE: MOVE DB2-PMT-ITS-CLM-TYP-CD      TO EXP-ITS-IND.
		ws.getExpenseRecord().setExpItsInd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd());
		// COB_CODE: MOVE DB2-PMT-MEM-ID              TO EXP-MEMBER-ID.
		ws.getExpenseRecord().setExpMemberId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtMemId());
		// COB_CODE: MOVE DB2-CLM-SYST-VER-ID         TO EXP-CLM-SYST-VER-ID.
		ws.getExpenseRecord().setExpClmSystVerId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getClmSystVerId());
		// COB_CODE: MOVE DB2-CLM-INS-LN-CD           TO EXP-CLM-INS-LN-CD.
		ws.getExpenseRecord().setExpClmInsLnCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getClmInsLnCd());
		// COB_CODE: MOVE ALL '0'                     TO EXP-WRAP-DATE-CCYY-MM-DD.
		ws.getExpenseRecord().getExpWrapDateCcyyMmDd().setNf05RecieptDateFormatted("0000000000");
		//    EVALUATE EXP-DIVISION-CODE
		//       WHEN 'G'
		//          ADD 1 TO KSS-EXPENSE-COUNT
		//          WRITE EXPENSE-KSS-REC FROM WS-EXP-FILE-REC
		//       WHEN OTHER
		// COB_CODE: ADD 1 TO REG-EXPENSE-COUNT.
		ws.getWsAllCounters().setRegExpenseCount(Trunc.toInt(1 + ws.getWsAllCounters().getRegExpenseCount(), 7));
		// COB_CODE: code not available
		expenseRegTo.setVariable(ws.getWsExpFileRecFormatted());
		expenseRegDAO.write(expenseRegTo);
	}

	/**Original name: 8605-WRITE-INT-EXP-FILE<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 8605-WRITE-INT-EXP-FILE **'.
	 *                                                             *</pre>*/
	private void writeIntExpFile() {
		// COB_CODE: MOVE HOLD-CLAIM-ID            TO EXP-CLAIM-ID.
		ws.getExpenseRecord().setExpClaimId(ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted());
		// COB_CODE: MOVE HOLD-CLAIM-SFX           TO EXP-CLAIM-ID-SUFFIX.
		ws.getExpenseRecord().setExpClaimIdSuffix(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx());
		// COB_CODE: MOVE HOLD-PMT-ID              TO EXP-PAYMENT-NUM.
		ws.getExpenseRecord().setExpPaymentNumFormatted(ws.getWsNf0533WorkArea().getHoldKey().getPmtIdFormatted());
		// COB_CODE: MOVE SPACES                   TO EXP-REIMBURSE-IND.
		ws.getExpenseRecord().setExpReimburseInd("");
		// COB_CODE: MOVE 01                       TO EXP-REPORT-INDC.
		ws.getExpenseRecord().getExpReportIndc().setExpReportIndc(((short) 1));
		// COB_CODE: MOVE ' '                      TO EXP-EXAMINER-ACTION.
		ws.getExpenseRecord().getExpExaminerAction().setExpExaminerActionFormatted(" ");
		// COB_CODE: MOVE HOLD-KCAPS-TEAM-NM       TO EXP-TEAM.
		ws.getExpenseRecord().setExpTeam(ws.getWsNf0533WorkArea().getHoldKcapsTeamNm());
		// COB_CODE: MOVE HOLD-KCAPS-USE-ID        TO EXP-ID.
		ws.getExpenseRecord().setExpId(ws.getWsNf0533WorkArea().getHoldKcapsUseId());
		// COB_CODE: MOVE HOLD-PGM-AREA-CD         TO EXP-PROGRAM-AREA.
		ws.getExpenseRecord().setExpProgramArea(ws.getWsNf0533WorkArea().getHoldPgmAreaCd());
		// COB_CODE: MOVE HOLD-ENR-CL-CD           TO EXP-ENROLLMENT-CLASS.
		ws.getExpenseRecord().setExpEnrollmentClass(ws.getWsNf0533WorkArea().getHoldEnrClCd());
		// COB_CODE: MOVE HOLD-FIN-CD              TO EXP-FINANCIAL-CODE.
		ws.getExpenseRecord().setExpFinancialCode(ws.getWsNf0533WorkArea().getHoldFinCd());
		// COB_CODE: MOVE HOLD-NTWRK-CD            TO EXP-NETWORK-CODE.
		ws.getExpenseRecord().setExpNetworkCode(ws.getWsNf0533WorkArea().getHoldNtwrkCd());
		// COB_CODE: MOVE HOLD-INS-ID              TO EXP-INS-ID.
		ws.getExpenseRecord().setExpInsId(ws.getWsNf0533WorkArea().getHoldInsId());
		// COB_CODE: MOVE HOLD-BI-PROV-ID          TO EXP-BILL-PROV-ID.
		ws.getExpenseRecord().setExpBillProvId(ws.getWsNf0533WorkArea().getHoldBiProvId());
		// COB_CODE: MOVE HOLD-BI-PROV-LOB         TO EXP-BILL-PROV-LOB.
		ws.getExpenseRecord().getExpBillProvLob().setExpBillProvLob(ws.getWsNf0533WorkArea().getHoldBiProvLob());
		// COB_CODE: MOVE HOLD-ASG-CD              TO EXP-ASSIGNED-INDC.
		ws.getExpenseRecord().getExpAssignedIndc().setExpAssignedIndc(ws.getWsNf0533WorkArea().getHoldAsgCd());
		// COB_CODE: MOVE HOLD-BI-PROV-NPI-NUM     TO EXP-NPI-PROV-NO.
		ws.getExpenseRecord().setExpNpiProvNo(ws.getWsNf0533WorkArea().getHoldBiProvNpiNum());
		// COB_CODE: MOVE 'P'                      TO EXP-WRAP-INDC.
		ws.getExpenseRecord().getExpWrapIndc().setExpWrapIndcFormatted("P");
		// COB_CODE: MOVE HOLD-BK-PROD-CD          TO EXP-BANK-INDICATOR.
		ws.getExpenseRecord().getExpBankIndicator().setExpBankIndicator(ws.getWsNf0533WorkArea().getHoldBkProdCd());
		// COB_CODE: IF HOLD-TYP-GRP-CD = 'A  ' OR ' A ' OR '  A'
		//              MOVE 0 TO EXP-CONVRTD-TYP-GRP-CD
		//           ELSE
		//              END-IF
		//           END-IF.
		if (Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), "A  ") || Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), " A ")
				|| Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), "  A")) {
			// COB_CODE: MOVE 0 TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCdFormatted("0");
		} else if (Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), "2  ")
				|| Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), " 2 ")
				|| Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), "  2")) {
			// COB_CODE: IF HOLD-TYP-GRP-CD = '2  ' OR ' 2 ' OR '  2'
			//              MOVE 'L' TO EXP-CONVRTD-TYP-GRP-CD
			//           ELSE
			//              END-IF
			//           END-IF
			// COB_CODE: MOVE 'L' TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCdFormatted("L");
		} else if (Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), "9  ")
				|| Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), " 9 ")
				|| Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), "  9") || Conditions.eq(ws.getWsNf0533WorkArea().getHoldNpiCd(), "F")) {
			// COB_CODE: IF HOLD-TYP-GRP-CD = '9  ' OR ' 9 ' OR '  9'
			//              OR HOLD-NPI-CD = 'F'
			//              MOVE 'F' TO EXP-CONVRTD-TYP-GRP-CD
			//           ELSE
			//              END-IF
			//           END-IF
			// COB_CODE: MOVE 'F' TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCdFormatted("F");
		} else if (Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), "13 ")
				|| Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), " 13")) {
			// COB_CODE: IF HOLD-TYP-GRP-CD = '13 ' OR ' 13'
			//              MOVE 'P' TO EXP-CONVRTD-TYP-GRP-CD
			//           ELSE
			//              END-IF
			//           END-IF
			// COB_CODE: MOVE 'P' TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCdFormatted("P");
		} else if (Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), "15 ")
				|| Conditions.eq(ws.getWsNf0533WorkArea().getHoldTypGrpCd(), " 15")) {
			// COB_CODE: IF HOLD-TYP-GRP-CD = '15 ' OR ' 15'
			//              MOVE '0' TO EXP-CONVRTD-TYP-GRP-CD
			//           ELSE
			//              MOVE ' ' TO EXP-CONVRTD-TYP-GRP-CD
			//           END-IF
			// COB_CODE: MOVE '0' TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCdFormatted("0");
		} else {
			// COB_CODE: MOVE ' ' TO EXP-CONVRTD-TYP-GRP-CD
			ws.getExpenseRecord().setExpConvrtdTypGrpCdFormatted(" ");
		}
		// COB_CODE: MOVE HOLD-HIST-LOAD-CD        TO EXP-HISTORY-LOAD-IND.
		ws.getExpenseRecord().setExpHistoryLoadInd(ws.getWsNf0533WorkArea().getHoldHistLoadCd());
		// COB_CODE: MOVE HOLD-ITS-CLM-TYP-CD      TO EXP-ITS-IND.
		ws.getExpenseRecord().setExpItsInd(ws.getWsNf0533WorkArea().getHoldItsClmTypCd());
		// COB_CODE: MOVE HOLD-MEMBER-ID           TO EXP-MEMBER-ID.
		ws.getExpenseRecord().setExpMemberId(ws.getWsNf0533WorkArea().getHoldMemberId());
		// COB_CODE: MOVE HOLD-CLM-SYST-VER-ID     TO EXP-CLM-SYST-VER-ID.
		ws.getExpenseRecord().setExpClmSystVerId(ws.getWsNf0533WorkArea().getHoldClmSystVerId());
		// COB_CODE: MOVE HOLD-CLM-INS-LN-CD       TO EXP-CLM-INS-LN-CD.
		ws.getExpenseRecord().setExpClmInsLnCd(ws.getWsNf0533WorkArea().getHoldClmInsLnCd());
		// COB_CODE: MOVE ALL '0'                  TO EXP-WRAP-DATE-CCYY-MM-DD.
		ws.getExpenseRecord().getExpWrapDateCcyyMmDd().setNf05RecieptDateFormatted("0000000000");
		//    EVALUATE PP-EXP-DIVISION-CODE
		//        WHEN 'G'
		//           ADD 1 TO KSS-EXPENSE-COUNT
		//           WRITE EXPENSE-KSS-REC FROM WS-EXP-FILE-REC
		//        WHEN OTHER
		// COB_CODE: ADD 1 TO REG-EXPENSE-COUNT.
		ws.getWsAllCounters().setRegExpenseCount(Trunc.toInt(1 + ws.getWsAllCounters().getRegExpenseCount(), 7));
		// COB_CODE: code not available
		expenseRegTo.setVariable(ws.getWsExpFileRecFormatted());
		expenseRegDAO.write(expenseRegTo);
	}

	/**Original name: 8700-WRITE-PROV-WO-FILE<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 8700-WRITE-PROV-WO-FILE **'.
	 *                                                             *</pre>*/
	private void writeProvWoFile() {
		// COB_CODE: MOVE SPACES                  TO REMITT-TAPE-RECORD.
		writeoffRegTO.initRemittTapeRecordSpaces();
		// COB_CODE: INITIALIZE                      REMITT-TAPE-RECORD.
		initRemittTapeRecord();
		// COB_CODE: MOVE WS-DB2-BI-NPI-PROV-ID   TO REM-TAPE-NPI-BILL-PVDR-NUM.
		writeoffRegTO.getHeaderPartOfRecord().setTapeNpiBillPvdrNum(ws.getWsNf0533WorkArea().getWsDb2BiNpiProvId());
		// COB_CODE: MOVE DB2-BI-PROV-LOB         TO REM-TAPE-LOCAL-BILL-PVDR-LOB.
		writeoffRegTO.getHeaderPartOfRecord().setTapeLocalBillPvdrLob(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvLob());
		// COB_CODE: MOVE DB2-BI-PROV-ID          TO REM-TAPE-LOCAL-BILL-PVDR-NUM.
		writeoffRegTO.getHeaderPartOfRecord().setTapeLocalBillPvdrNum(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvId());
		// COB_CODE: MOVE DB2-PMT-ALPH-PRFX-CD    TO REM-TAPE-GRP-ALP-PRFX-CD.
		writeoffRegTO.getDetailPartOfRecord().setTapeGrpAlpPrfxCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAlphPrfxCd());
		// COB_CODE: MOVE DB2-PMT-INS-ID          TO REM-DETL-ID-NUM.
		writeoffRegTO.getDetailPartOfRecord().setDetlIdNum(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtInsId());
		// COB_CODE: MOVE DB2-PMT-ITS-CLM-TYP-CD  TO REM-ITS-CLAIM-TYPE.
		writeoffRegTO.getHeaderPartOfRecord().setItsClaimType(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd());
		// COB_CODE: MOVE DB2-CLM-CNTL-ID         TO REM-DETL-CLAIM-ID.
		writeoffRegTO.getDetailPartOfRecord().setDetlClaimId(ws.getEobDb2CursorArea().getS02801Info().getClmCntlId());
		// COB_CODE: MOVE DB2-CLM-CNTL-SFX-ID     TO REM-DETL-CLAIM-ID-SUFFIX.
		writeoffRegTO.getDetailPartOfRecord().setDetlClaimIdSuffix(ws.getEobDb2CursorArea().getS02801Info().getClmCntlSfxId());
		// COB_CODE: MOVE DB2-CLM-PMT-ID          TO REM-REL-PAYMENT.
		writeoffRegTO.getDetailPartOfRecord().setRelPayment(TruncAbs.toShort(ws.getEobDb2CursorArea().getS02801Info().getClmPmtId(), 4));
		// COB_CODE: MOVE DB2-PAT-LST-NM          TO REM-DETL-PATIENT-NAME-LAST.
		writeoffRegTO.getDetailPartOfRecord().getDetlPatientName().setLast(ws.getEobDb2CursorArea().getS02952NameDemoInfo().getPatLstNm());
		// COB_CODE: MOVE DB2-PAT-FRST-NM         TO REM-DETL-PATIENT-NAME-FIRST
		writeoffRegTO.getDetailPartOfRecord().getDetlPatientName().setFirst(ws.getEobDb2CursorArea().getS02952NameDemoInfo().getPatFrstNm());
		// COB_CODE: MOVE DB2-PAT-MID-NM          TO REM-DETL-PATIENT-NAME-MI.
		writeoffRegTO.getDetailPartOfRecord().getDetlPatientName()
				.setMiFormatted(ws.getEobDb2CursorArea().getS02952NameDemoInfo().getPatMidNmFormatted());
		// COB_CODE: MOVE DB2-PMT-PAT-ACT-ID      TO REM-DETL-PATIENT-ACCT.
		writeoffRegTO.getDetailPartOfRecord().setDetlPatientAcct(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPatActId());
		// COB_CODE: MOVE DB2-LI-FRM-SERV-DT      TO WORK-DATE.
		ws.getWorkFields().getWorkDate().setWorkDateFormatted(ws.getEobDb2CursorArea().getS02809LineInfo().getFrmServDtFormatted());
		// COB_CODE: MOVE WORK-CENTURY            TO REM-DETL-SERV-CC.
		writeoffRegTO.getDetailPartOfRecord().getDetlServDate().setCcFormatted(ws.getWorkFields().getWorkDate().getCenturyFormatted());
		// COB_CODE: MOVE WORK-YEAR               TO REM-DETL-SERV-YY.
		writeoffRegTO.getDetailPartOfRecord().getDetlServDate().setYyFormatted(ws.getWorkFields().getWorkDate().getYearFormatted());
		// COB_CODE: MOVE WORK-MONTH              TO REM-DETL-SERV-MM.
		writeoffRegTO.getDetailPartOfRecord().getDetlServDate().setMmFormatted(ws.getWorkFields().getWorkDate().getMonthFormatted());
		// COB_CODE: MOVE WORK-DAY                TO REM-DETL-SERV-DD.
		writeoffRegTO.getDetailPartOfRecord().getDetlServDate().setDdFormatted(ws.getWorkFields().getWorkDate().getDayFormatted());
		// COB_CODE: MOVE DB2-LI-POS-CD           TO REM-DETL-POS.
		writeoffRegTO.getDetailPartOfRecord().setDetlPos(ws.getEobDb2CursorArea().getS02809LineInfo().getPosCd());
		// COB_CODE: MOVE DB2-LI-PROC-CD          TO REM-ADJU-DRG-PROC.
		writeoffRegTO.getDetailPartOfRecord().getAdjuDrgProc()
				.setAdjuDrgProcFormatted(ws.getEobDb2CursorArea().getS02809LineInfo().getProcCdFormatted());
		// COB_CODE: MOVE DB2-PMT-ALPH-PRFX-CD    TO REM-TAPE-GRP-ALP-PRFX-CD.
		writeoffRegTO.getDetailPartOfRecord().setTapeGrpAlpPrfxCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAlphPrfxCd());
		// COB_CODE: MOVE DB2-LI-CHG-AM           TO REM-DETL-TOTAL-CHARGE.
		writeoffRegTO.getDetailPartOfRecord().setDetlTotalCharge(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02809LineInfo().getChgAm(), 9, 2));
		// COB_CODE: MOVE WS-LINE-ACCUM-WRITEOFF  TO REM-DETL-PVDR-WRITEOFF
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued()
				.setDetlPvdrWriteoff(Trunc.toDecimal(ws.getWsRollLogicWork().getLineAccumulators().getWriteoff(), 9, 2));
		// COB_CODE: MOVE DB2-LI-ALW-CHG-AM       TO REM-DETL-ALLOWED-AMT.
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued()
				.setDetlAllowedAmt(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02809LineInfo().getAlwChgAm(), 9, 2));
		// COB_CODE: PERFORM 2020-PROVIDER-STUFF     THRU 2020-EXIT.
		providerStuff();
		// COB_CODE: IF LOB-PROV-WAS-FOUND
		//               MOVE HOLD-BI-PROVIDER-NAME TO REM-TAPE-RA-ADDR-NAME
		//           ELSE
		//               MOVE BLUE-CROSS-ADDR3-ZIP  TO REM-TAPE-ADDR-ZIP
		//           END-IF.
		if (ws.getWsSwitches().isWsLobProvFoundSw()) {
			// COB_CODE: MOVE HOLD-RA-ADDR1         TO REM-TAPE-RA-ADDR-1
			writeoffRegTO.getHeaderPartOfRecord().setTapeRaAddr1(ws.getWsNf0533WorkArea().getHoldRaAddress().getAddr1());
			// COB_CODE: MOVE HOLD-RA-ADDR2         TO REM-TAPE-RA-ADDR-2
			writeoffRegTO.getHeaderPartOfRecord().setTapeRaAddr2(ws.getWsNf0533WorkArea().getHoldRaAddress().getAddr2());
			// COB_CODE: MOVE HOLD-RA-ADDR3         TO REM-TAPE-RA-ADDR-3
			writeoffRegTO.getHeaderPartOfRecord().setTapeRaAddr3Bytes(ws.getWsNf0533WorkArea().getHoldRaAddress().getAddr3Bytes());
			// COB_CODE: MOVE HOLD-BI-PROVIDER-NAME TO REM-TAPE-RA-ADDR-NAME
			writeoffRegTO.getHeaderPartOfRecord().getTapeRaAddrName()
					.setTapeRaAddrNameFormatted(ws.getWsNf0533WorkArea().getHoldBiProviderNameFormatted());
		} else {
			// COB_CODE: MOVE BLUE-CROSS-NAME       TO REM-TAPE-RA-ADDR-NAME
			writeoffRegTO.getHeaderPartOfRecord().getTapeRaAddrName().setTapeRaAddrNameFormatted(ws.getWsBlueCrossAddressStuff().getNameFormatted());
			// COB_CODE: MOVE BLUE-CROSS-ADDRESS-1  TO REM-TAPE-RA-ADDR-1
			writeoffRegTO.getHeaderPartOfRecord().setTapeRaAddr1(ws.getWsBlueCrossAddressStuff().getAddress1());
			// COB_CODE: MOVE BLUE-CROSS-ADDRESS-2  TO REM-TAPE-RA-ADDR-2
			writeoffRegTO.getHeaderPartOfRecord().setTapeRaAddr2(ws.getWsBlueCrossAddressStuff().getAddress2());
			// COB_CODE: MOVE BLUE-CROSS-ADDR3-CITY TO REM-TAPE-ADDR-CITY
			writeoffRegTO.getHeaderPartOfRecord().setTapeAddrCity(ws.getWsBlueCrossAddressStuff().getAddr3City());
			// COB_CODE: MOVE BLUE-CROSS-ADDR3-ST   TO REM-TAPE-ADDR-ST
			writeoffRegTO.getHeaderPartOfRecord().setTapeAddrSt(ws.getWsBlueCrossAddressStuff().getAddr3St());
			// COB_CODE: MOVE BLUE-CROSS-ADDR3-ZIP  TO REM-TAPE-ADDR-ZIP
			writeoffRegTO.getHeaderPartOfRecord().setTapeAddrZipFormatted(ws.getWsBlueCrossAddressStuff().getAddr3ZipFormatted());
		}
		// COB_CODE: MOVE HIGH-VALUES     TO  REM-TAPE-CLAIM-LOB.
		writeoffRegTO.getHeaderPartOfRecord().setTapeClaimLob(Types.HIGH_CHAR_VAL);
		// COB_CODE: MOVE 'Z'             TO  REM-TAPE-SUBTOT-INDC.
		writeoffRegTO.getHeaderPartOfRecord().getTapeSubtotIndc().setTapeSubtotIndcFormatted("Z");
		// COB_CODE: MOVE 999             TO  REM-LINE-NUM.
		writeoffRegTO.getDetailPartOfRecord().setLineNum(((short) 999));
		// COB_CODE: MOVE REM-DETL-ID-NUM TO REM-DETL-SUBSCRIBER-DIGITS.
		writeoffRegTO.getDetailPartOfRecord().setDetlSubscriberDigits(writeoffRegTO.getDetailPartOfRecord().getDetlIdNum());
		// COB_CODE: IF REM-TAPE-GRP-ALP-PRFX-CD = SPACES
		//              MOVE REM-DETL-ID-NUM TO REM-DETL-SUBSCRIBER-ID
		//           END-IF.
		if (Characters.EQ_SPACE.test(writeoffRegTO.getDetailPartOfRecord().getTapeGrpAlpPrfxCd())) {
			// COB_CODE: MOVE REM-DETL-ID-NUM TO REM-DETL-SUBSCRIBER-ID
			writeoffRegTO.getDetailPartOfRecord().setDetlSubscriberIdFormatted(writeoffRegTO.getDetailPartOfRecord().getDetlIdNumFormatted());
		}
		// COB_CODE: WRITE REG-WRITEOFF-REC FROM REMITT-TAPE-RECORD.
		writeoffRegTO.setData(writeoffRegTO.getRemittTapeRecordBytes());
		writeoffRegDAO.write(writeoffRegTO);
		// COB_CODE: ADD 1 TO REG-WRITEOFF-CNTR.
		ws.getWsAllCounters().setRegWriteoffCntr(Trunc.toInt(1 + ws.getWsAllCounters().getRegWriteoffCntr(), 7));
	}

	/**Original name: 8800-WRITE-EOB-RECORD<br>
	 * <pre>                                                              *
	 *     DISPLAY '** 8800-WRITE-EOB-RECORD **'.
	 *                                                               *</pre>*/
	private void writeEobRecord() {
		// COB_CODE:      IF NF05-RECORD-TYPE = 'T'
		//           ***  MUST CHECK FOR SRS, STOP PAY CODE OF 'G'
		//                   MOVE WS-PROV-STOP-PAY-CD TO NF05-PROV-STOP-PAY-CD
		//                END-IF.
		if (ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecordType().getNf05RecordType() == 'T') {
			//**  MUST CHECK FOR SRS, STOP PAY CODE OF 'G'
			// COB_CODE: MOVE NF05-BILL-PVDR-NUM  TO  HLD-SRS-PROV-ID
			ws.getWsPrvStopPayVariables().setHldSrsProvId(ws.getNf05EobRecord().getNf05LineLevelRecord().getBillPvdrNum());
			// COB_CODE: MOVE NF05-BILL-PVDR-LOB  TO  HLD-SRS-PRV-LOB-CD
			ws.getWsPrvStopPayVariables().setHldSrsPrvLobCdFormatted(ws.getNf05EobRecord().getNf05LineLevelRecord().getBillPvdrLobFormatted());
			// COB_CODE: MOVE NF05-FROM-SERVICE-DATE TO HLD-SRS-FRM-SERV-DT
			ws.getWsPrvStopPayVariables().setHldSrsFrmServDt(ws.getNf05EobRecord().getNf05LineLevelRecord().getFromServiceDateFormatted());
			// COB_CODE: MOVE NF05-THRU-SERVICE-DATE TO HLD-SRS-THR-SERV-DT
			ws.getWsPrvStopPayVariables().setHldSrsThrServDt(ws.getNf05EobRecord().getNf05LineLevelRecord().getThruServiceDateFormatted());
			// COB_CODE: MOVE HOLD-CLM-INS-LN-CD TO  NF05-CLM-INS-LN-CD
			ws.getNf05EobRecord().getNf05LineLevelRecord().setClmInsLnCd(ws.getWsNf0533WorkArea().getHoldClmInsLnCd());
			// COB_CODE: MOVE NF05-CLAIM-NUM      TO WS-CLAIM-ID
			ws.getWsClaimId().setWsClaimIdBytes(ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ClaimNum().getNf05ClaimNumBytes());
			// COB_CODE: PERFORM 1750-CHECK-FOR-SRS-PROV THRU 1750-EXIT
			checkForSrsProv();
			// COB_CODE: MOVE WS-PROV-STOP-PAY-CD TO NF05-PROV-STOP-PAY-CD
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProvStopPayCd(ws.getWsPrvStopPayVariables().getWsProvStopPayCd());
		}
		// COB_CODE: IF NF05-OTHER-INS-SUBROGATION
		//              NEXT SENTENCE
		//           ELSE
		//             END-IF
		//           END-IF.
		if (ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05OtherInsureCode().isOtherInsSubrogation()) {
			// COB_CODE: NEXT SENTENCE
			//next sentence
		} else {
			// COB_CODE: IF WS-TEST-FLAG = 'Y'
			//                               NF05-PMT-NUM
			//           END-IF
			if (ws.getWsRestartStuff().getWsRestartJobParms().getTestFlag() == 'Y') {
				// COB_CODE: DISPLAY '2814 STATUS: ' 'EOB           '
				//               ' CLAIM: ' NF05-CLAIM-NUM
				//                          NF05-CLAIM-SFX
				//                          NF05-PMT-NUM
				DisplayUtil.sysout.write(new String[] { "2814 STATUS: ", "EOB           ", " CLAIM: ",
						ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ClaimNum().getNf05ClaimNumFormatted(),
						String.valueOf(ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ClaimSfx()),
						ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05PmtNumAsString() });
			}
			// COB_CODE: IF HOLD-VBR-IN = 'Y'
			//              ADD 1 TO WS-BYPASS-PROV
			//           ELSE
			//              END-IF
			//           END-IF
			if (ws.getWsNf0533WorkArea().getHoldVbrIn() == 'Y') {
				// COB_CODE: ADD 1 TO WS-BYPASS-PROV
				ws.getWsAllCounters().getWsCounters().setWsBypassProv(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsBypassProv(), 7));
			} else {
				// COB_CODE:           IF  NF05-MT-QT-INDICATOR  =  'M'
				//                         WRITE EOB-REG-OUTPUT-RECORD FROM NF05-EOB-RECORD
				//                         WRITE EOB-REG-OUTPUT-RECORD FROM NF05-EOB-RECORD
				//           *             END-IF
				//                     END-IF
				if (ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05MtQtIndicator() == 'M'
						|| ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05MtQtIndicator() == 'Q') {
					// COB_CODE: code not available
					dailyRegOutputTo.setVariable(ws.getNf05EobRecord().getNf05EobRecordFormatted());
					dailyRegOutputDAO.write(dailyRegOutputTo);
				} else {
					//             IF  NF05-CLM-CORP-CODE  =  'G'
					//                 WRITE EOB-KSS-OUTPUT-RECORD FROM NF05-EOB-RECORD
					//             ELSE
					// COB_CODE: code not available
					dailyRegOutputTo.setVariable(ws.getNf05EobRecord().getNf05EobRecordFormatted());
					dailyRegOutputDAO.write(dailyRegOutputTo);
					//             END-IF
				}
				// COB_CODE: ADD 1 TO WS-LINES-REG-OUT
				ws.getWsAllCounters().getWsCounters().setWsLinesRegOut(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsLinesRegOut(), 7));
				// COB_CODE: IF NF05-RECORD-TYPE = 'T'
				//              ADD 1 TO WS-EOB-REG-OUT
				//           END-IF
				if (ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecordType().getNf05RecordType() == 'T') {
					// COB_CODE: ADD 1 TO WS-EOB-REG-OUT
					ws.getWsAllCounters().getWsCounters().setWsEobRegOut(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsEobRegOut(), 7));
				}
			}
		}
	}

	/**Original name: 8950-WRITE-MTH-QTR<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 8950-WRITE-MTH-QTR **'.
	 *                                                             *</pre>*/
	private void writeMthQtr() {
		// COB_CODE: IF NF05-RECORD-TYPE = 'T'
		//              MOVE WS-PROV-STOP-PAY-CD TO NF05-PROV-STOP-PAY-CD
		//           END-IF.
		if (ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecordType().getNf05RecordType() == 'T') {
			// COB_CODE: ADD 1 TO WS-MTH-QTR
			ws.getWsAllCounters().getWsCounters().setWsMthQtr(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsMthQtr(), 7));
			//**  MUST CHECK FOR SRS, STOP PAY CODE OF 'G'
			// COB_CODE: MOVE NF05-BILL-PVDR-NUM  TO  HLD-SRS-PROV-ID
			ws.getWsPrvStopPayVariables().setHldSrsProvId(ws.getNf05EobRecord().getNf05LineLevelRecord().getBillPvdrNum());
			// COB_CODE: MOVE NF05-BILL-PVDR-LOB  TO  HLD-SRS-PRV-LOB-CD
			ws.getWsPrvStopPayVariables().setHldSrsPrvLobCdFormatted(ws.getNf05EobRecord().getNf05LineLevelRecord().getBillPvdrLobFormatted());
			// COB_CODE: MOVE NF05-FROM-SERVICE-DATE TO HLD-SRS-FRM-SERV-DT
			ws.getWsPrvStopPayVariables().setHldSrsFrmServDt(ws.getNf05EobRecord().getNf05LineLevelRecord().getFromServiceDateFormatted());
			// COB_CODE: MOVE NF05-THRU-SERVICE-DATE TO HLD-SRS-THR-SERV-DT
			ws.getWsPrvStopPayVariables().setHldSrsThrServDt(ws.getNf05EobRecord().getNf05LineLevelRecord().getThruServiceDateFormatted());
			// COB_CODE: MOVE NF05-CLAIM-NUM      TO WS-CLAIM-ID
			ws.getWsClaimId().setWsClaimIdBytes(ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ClaimNum().getNf05ClaimNumBytes());
			// COB_CODE: PERFORM 1750-CHECK-FOR-SRS-PROV THRU 1750-EXIT
			checkForSrsProv();
			// COB_CODE: MOVE WS-PROV-STOP-PAY-CD TO NF05-PROV-STOP-PAY-CD
			ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProvStopPayCd(ws.getWsPrvStopPayVariables().getWsProvStopPayCd());
		}
		// COB_CODE: IF HOLD-VBR-IN = 'Y'
		//              ADD 1 TO WS-BYPASS-PROV
		//           ELSE
		//                END-IF
		//           END-IF.
		if (ws.getWsNf0533WorkArea().getHoldVbrIn() == 'Y') {
			// COB_CODE: ADD 1 TO WS-BYPASS-PROV
			ws.getWsAllCounters().getWsCounters().setWsBypassProv(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsBypassProv(), 7));
		} else if (ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05MtQtIndicator() == 'M'
				|| ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05MtQtIndicator() == 'Q') {
			// COB_CODE:           IF  NF05-MT-QT-INDICATOR  =  'M'
			//                         WRITE EOB-REG-OUTPUT-RECORD FROM NF05-EOB-RECORD
			//                     WRITE EOB-REG-OUTPUT-RECORD FROM NF05-EOB-RECORD
			//           *             END-IF
			//                     END-IF
			// COB_CODE: code not available
			dailyRegOutputTo.setVariable(ws.getNf05EobRecord().getNf05EobRecordFormatted());
			dailyRegOutputDAO.write(dailyRegOutputTo);
		} else {
			//             IF  NF05-CLM-CORP-CODE  =  'G'
			//                 WRITE EOB-KSS-OUTPUT-RECORD FROM NF05-EOB-RECORD
			//             ELSE
			// COB_CODE: code not available
			dailyRegOutputTo.setVariable(ws.getNf05EobRecord().getNf05EobRecordFormatted());
			dailyRegOutputDAO.write(dailyRegOutputTo);
			//             END-IF
		}
	}

	/**Original name: 9000-SELECT-S03085SA<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 9000-SELECT-S03085SA **'.
	 *                                                             *</pre>*/
	private void selectS03085sa() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//              SELECT RSTRT_IN
		//                  ,  JBSTP_RUN_CT
		//              INTO  :DCLS03085SA.RSTRT-IN
		//                  , :DCLS03085SA.JBSTP-RUN-CT
		//              FROM S03085SA
		//                  WHERE JOB_NM       = :WS-RESTART-JOB-NAME
		//                    AND JBSTP_NM     = :WS-RESTART-STEP-NAME
		//                    AND JBSTP_SEQ_ID = :WS-RESTART-JOB-SEQ-PACKED
		//           END-EXEC.
		s03085saDao.selectRec(ws.getWsRestartStuff().getWsRestartJobParms().getRestartJobName(),
				ws.getWsRestartStuff().getWsRestartJobParms().getRestartStepName(), ws.getWsRestartStuff().getWsRestartJobSeqPacked(),
				ws.getDcls03085sa());
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: DISPLAY '       NF0533ML   ABENDING  '
			DisplayUtil.sysout.write("       NF0533ML   ABENDING  ");
			// COB_CODE: DISPLAY 'ERROR SELECTING S03085SA TABLE '
			DisplayUtil.sysout.write("ERROR SELECTING S03085SA TABLE ");
			// COB_CODE: DISPLAY 'FOR RESTART CONTROL INFORMATION'
			DisplayUtil.sysout.write("FOR RESTART CONTROL INFORMATION");
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: STRING WS-RESTART-JOB-PARMS
			//                DELIMITED BY '&'    INTO DB2-ERR-KEY
			ws.getWsDisplayWorkArea()
					.setDb2ErrKey(Functions.substringBefore(ws.getWsRestartStuff().getWsRestartJobParms().getWsRestartJobParmsFormatted(), "&"));
			// COB_CODE: MOVE 'S03085SA'               TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S03085SA");
			// COB_CODE: MOVE 'SELECT S03085SA'        TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("SELECT S03085SA");
			// COB_CODE: MOVE '9000-SELECT-TA03085-CONTROL' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9000-SELECT-TA03085-CONTROL");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9010-UPDATE-S03085SA<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 9010-UPDATE-S03085SA **'.
	 *                                                             *</pre>*/
	private void updateS03085sa() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//              UPDATE S03085SA
		//                  SET RSTRT_IN       = 'Y'
		//                    , JBSTP_RUN_CT   = :DCLS03085SA.JBSTP-RUN-CT +
		//                      DECIMAL (1, 3)
		//                    , JBSTP_STRT_TS  = CURRENT_TIMESTAMP
		//                  WHERE JOB_NM       = :DCLS03085SA.JOB-NM
		//                    AND JBSTP_NM     = :DCLS03085SA.JBSTP-NM
		//                    AND JBSTP_SEQ_ID = :DCLS03085SA.JBSTP-SEQ-ID
		//           END-EXEC.
		s03085saDao.updateRec(ws.getDcls03085sa().getJbstpRunCt(), ws.getDcls03085sa().getJobNm(), ws.getDcls03085sa().getJbstpNm(),
				ws.getDcls03085sa().getJbstpSeqId());
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: STRING WS-RESTART-JOB-PARMS
			//                DELIMITED BY '&'    INTO DB2-ERR-KEY
			ws.getWsDisplayWorkArea()
					.setDb2ErrKey(Functions.substringBefore(ws.getWsRestartStuff().getWsRestartJobParms().getWsRestartJobParmsFormatted(), "&"));
			// COB_CODE: MOVE 'S03085SA'               TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S03085SA");
			// COB_CODE: MOVE 'UPDATE S03085SA'        TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("UPDATE S03085SA");
			// COB_CODE: MOVE '9010-UPDATE-S03085SA  ' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9010-UPDATE-S03085SA  ");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9020-SELECT-S03086SA<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 9020-SELECT-S03086SA **'.
	 *                                                             *</pre>*/
	private void selectS03086sa() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//              SELECT APPL_TRM_CD
		//                  ,  RSTRT_HIST_IN
		//                  ,  ROW_FREQ_CMT_CT
		//                  ,  CMT_TM_INT_TM
		//                  ,  CMT_CT
		//                  ,  CMT_TS
		//                  ,  RSTRT1_TX
		//                  ,  RSTRT2_TX
		//              INTO  :DCLS03086SA.APPL-TRM-CD
		//                  , :DCLS03086SA.RSTRT-HIST-IN
		//                  , :DCLS03086SA.ROW-FREQ-CMT-CT
		//                  , :DCLS03086SA.CMT-TM-INT-TM
		//                  , :DCLS03086SA.CMT-CT
		//                  , :DCLS03086SA.CMT-TS
		//                  , :DCLS03086SA.RSTRT1-TX
		//                  , :DCLS03086SA.RSTRT2-TX
		//              FROM S03086SA
		//                  WHERE JOB_NM       = :WS-RESTART-JOB-NAME
		//                    AND JBSTP_NM     = :WS-RESTART-STEP-NAME
		//                    AND JBSTP_SEQ_ID = :WS-RESTART-JOB-SEQ-PACKED
		//           END-EXEC.
		s03086saDao.selectRec(ws.getWsRestartStuff().getWsRestartJobParms().getRestartJobName(),
				ws.getWsRestartStuff().getWsRestartJobParms().getRestartStepName(), ws.getWsRestartStuff().getWsRestartJobSeqPacked(),
				ws.getDcls03086sa());
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: DISPLAY '       NF0533ML   ABENDING  '
			DisplayUtil.sysout.write("       NF0533ML   ABENDING  ");
			// COB_CODE: DISPLAY 'ERROR SELECTING S03086SA TABLE '
			DisplayUtil.sysout.write("ERROR SELECTING S03086SA TABLE ");
			// COB_CODE: DISPLAY 'FOR RESTART INFORMATION.       '
			DisplayUtil.sysout.write("FOR RESTART INFORMATION.       ");
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: STRING WS-RESTART-JOB-PARMS
			//                DELIMITED BY '&'    INTO DB2-ERR-KEY
			ws.getWsDisplayWorkArea()
					.setDb2ErrKey(Functions.substringBefore(ws.getWsRestartStuff().getWsRestartJobParms().getWsRestartJobParmsFormatted(), "&"));
			// COB_CODE: MOVE 'S03086SA'                TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S03086SA");
			// COB_CODE: MOVE 'SELECT S03086SA'         TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("SELECT S03086SA");
			// COB_CODE: MOVE '9020-SELECT-TA03086-RESTART' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9020-SELECT-TA03086-RESTART");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9030-UPDATE-S03086SA<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 9030-UPDATE-S03086SA **'.
	 *                                                             *</pre>*/
	private void updateS03086sa() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//              UPDATE S03086SA
		//                  SET CMT_CT         = :DCLS03086SA.CMT-CT +
		//                      DECIMAL (1, 9)
		//                    , RSTRT1_TX      = :DCLS03086SA.RSTRT1-TX
		//                    , RSTRT2_TX      = :DCLS03086SA.RSTRT2-TX
		//                    , CMT_TS         = CURRENT_TIMESTAMP
		//                    , APPL_TRM_CD    = :DCLS03086SA.APPL-TRM-CD
		//                  WHERE JOB_NM       = :DCLS03086SA.JOB-NM
		//                    AND JBSTP_NM     = :DCLS03086SA.JBSTP-NM
		//                    AND JBSTP_SEQ_ID = :DCLS03086SA.JBSTP-SEQ-ID
		//           END-EXEC.
		s03086saDao.updateRec(ws.getDcls03086sa());
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: STRING WS-RESTART-JOB-PARMS
			//                DELIMITED BY '&'    INTO DB2-ERR-KEY
			ws.getWsDisplayWorkArea()
					.setDb2ErrKey(Functions.substringBefore(ws.getWsRestartStuff().getWsRestartJobParms().getWsRestartJobParmsFormatted(), "&"));
			// COB_CODE: MOVE 'S03086SA'               TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S03086SA");
			// COB_CODE: MOVE 'UPDATE S03086SA'        TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("UPDATE S03086SA");
			// COB_CODE: MOVE '9030-UPDATE-S03086SA  ' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9030-UPDATE-S03086SA  ");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9090-SQL-COMMIT<br>
	 * <pre>                                                            *
	 *     DISPLAY '** 9090-SQL-COMMIT **'.</pre>*/
	private void sqlCommit() {
		GenericParam abendCode = null;
		// COB_CODE: EXEC SQL
		//              COMMIT
		//           END-EXEC.
		DbService.getCurrent().commit(dbAccessStatus);
		// COB_CODE: EVALUATE SQLCODE
		//           WHEN 0
		//               END-IF
		//           WHEN OTHER
		//               CALL 'ABEND' USING ABEND-CODE
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE 0 TO COMMIT-COUNTER
			ws.getWsAllCounters().getWsCounters().setCommitCounter(0);
			// COB_CODE: IF APPL-TRM-CD = 'T' OR 'R'
			//               PERFORM 9997-FORCE-EOJ THRU 9997-EXIT
			//           END-IF
			if (ws.getDcls03086sa().getApplTrmCd() == 'T' || ws.getDcls03086sa().getApplTrmCd() == 'R') {
				// COB_CODE: PERFORM 9997-FORCE-EOJ THRU 9997-EXIT
				forceEoj();
			}
			break;

		default:// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
			// COB_CODE: DISPLAY '       NF0533ML   ABENDING '
			DisplayUtil.sysout.write("       NF0533ML   ABENDING ");
			// COB_CODE: DISPLAY '    ERROR DOING A COMMIT      '
			DisplayUtil.sysout.write("    ERROR DOING A COMMIT      ");
			// COB_CODE: DISPLAY 'IN PARAGRAPH: 9090-SQL-COMMIT '
			DisplayUtil.sysout.write("IN PARAGRAPH: 9090-SQL-COMMIT ");
			// COB_CODE: DISPLAY ' RESTART KEY IS: ' RESTART-KEY
			DisplayUtil.sysout.write(" RESTART KEY IS: ", ws.getWsRestartStuff().getRestartKeyFormatted());
			// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
			// COB_CODE: MOVE +200   TO ABEND-CODE
			ws.setAbendCode(200);
			// COB_CODE: MOVE +200   TO RETURN-CODE
			Session.setReturnCode(200);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;
		}
	}

	/**Original name: 9100-DECLARE-TEMP-TABLE<br>
	 * <pre>    DISPLAY '** 9100-DECLARE-TEMP-TABLE **'.
	 * ***************************************************************
	 *  THIS STATEMENT 'CREATES' THE TEMPORARY DB2 TABLE THAT THE    *
	 *  TRIGGER FILE WILL BE LOADED INTO SO THAT THE TRIGGER FILE    *
	 *  CAN BE JOINED WITH THE REST OF THE DB2 TABLES NEEDED.        *
	 * ***************************************************************</pre>*/
	private void declareTempTable() {
		// COB_CODE: EXEC SQL
		//             DECLARE GLOBAL TEMPORARY TABLE TRIGGER1
		//              (T_PROD_IND           CHAR(3)  NOT NULL WITH DEFAULT ' '
		//             , T_CLM_CNTL_ID        CHAR(12) NOT NULL WITH DEFAULT ' '
		//             , T_CLM_CNTL_SFX_ID    CHAR(1)  NOT NULL WITH DEFAULT ' '
		//             , T_PMT_ID         DECIMAL(2,0) NOT NULL WITH DEFAULT 0
		//             , T_FNL_BUS_DT         CHAR(10) NOT NULL WITH DEFAULT ' '
		//             , T_STAT_CATG_CD       CHAR(3)  NOT NULL WITH DEFAULT ' '
		//             , T_BI_PROV_ID         CHAR(10) NOT NULL WITH DEFAULT ' '
		//             , T_BI_PROV_LOB        CHAR(1) NOT NULL WITH DEFAULT ' '
		//             , T_BI_SPEC_CD         CHAR(4) NOT NULL WITH DEFAULT ' '
		//             , T_PE_PROV_ID         CHAR(10) NOT NULL WITH DEFAULT ' '
		//             , T_PE_PROV_LOB        CHAR(1) NOT NULL WITH DEFAULT ' '
		//             , T_ALPH_PRFX_CD       CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_INS_ID             CHAR(12) NOT NULL WITH DEFAULT ' '
		//             , T_MEMBER_ID         CHAR(14)  NOT NULL WITH DEFAULT ' '
		//             , T_CLM_SYST_VER_ID   CHAR(06)  NOT NULL WITH DEFAULT ' '
		//             , T_CORP_RCV_DT       CHAR(10) NOT NULL WITH DEFAULT ' '
		//             , T_ADJD_DT           CHAR(10) NOT NULL WITH DEFAULT ' '
		//             , T_BILL_FRM_DT       CHAR(10) NOT NULL WITH DEFAULT ' '
		//             , T_BILL_THR_DT       CHAR(10) NOT NULL WITH DEFAULT ' '
		//             , T_ASG_CD             CHAR(2) NOT NULL WITH DEFAULT ' '
		//             , T_CLM_PD_AM     DECIMAL(11,2) NOT NULL WITH DEFAULT 0
		//             , T_KCAPS_TEAM_NM      CHAR(5) NOT NULL WITH DEFAULT ' '
		//             , T_KCAPS_USE_ID       CHAR(4) NOT NULL WITH DEFAULT ' '
		//             , T_HIST_LOAD_CD       CHAR(1) NOT NULL WITH DEFAULT ' '
		//             , T_PMT_BK_PROD_CD     CHAR(1) NOT NULL WITH DEFAULT ' '
		//             , T_PAT_RVW_CD         CHAR(1) NOT NULL WITH DEFAULT ' '
		//             , T_ADJD_OVRD1_CD      CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_ADJD_OVRD2_CD      CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_ADJD_OVRD3_CD      CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_ADJD_OVRD4_CD      CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_ADJD_OVRD5_CD      CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_ADJD_OVRD6_CD      CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_ADJD_OVRD7_CD      CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_ADJD_OVRD8_CD      CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_ADJD_OVRD9_CD      CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_ADJD_OVRD10_CD     CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_DRG                CHAR(4) NOT NULL WITH DEFAULT ' '
		//             , T_PAT_ACT_ID         CHAR(25) NOT NULL WITH DEFAULT ' '
		//             , T_PGM_AREA_CD        CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_FIN_CD             CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_ADJD_PROV_STAT     CHAR(1) NOT NULL WITH DEFAULT ' '
		//             , T_PRMPT_PAY_DAY      CHAR(2) NOT NULL WITH DEFAULT ' '
		//             , T_PRMPT_PAY_OVRD     CHAR(1) NOT NULL WITH DEFAULT ' '
		//             , T_ITS_CLM_TYP_CD     CHAR(1) NOT NULL WITH DEFAULT ' '
		//             , T_GRP_ID             CHAR(12) NOT NULL WITH DEFAULT ' '
		//             , T_LOB_CD             CHAR(1) NOT NULL WITH DEFAULT ' '
		//             , T_TYP_GRP_CD         CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_NPI_CD             CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_BEN_PLN_ID         CHAR(12) NOT NULL WITH DEFAULT ' '
		//             , T_IRC_CD             CHAR(2) NOT NULL WITH DEFAULT ' '
		//             , T_PMT_NTWRK_CD       CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_PRIM_CN_ARNG_CD    CHAR(5) NOT NULL WITH DEFAULT ' '
		//             , T_ENR_CL_CD          CHAR(3) NOT NULL WITH DEFAULT ' '
		//             , T_OI_CD              CHAR(1) NOT NULL WITH DEFAULT ' '
		//             , T_PAT_RESP      DECIMAL(11,2) NOT NULL WITH DEFAULT 0
		//             , T_PWO_AMT       DECIMAL(11,2) NOT NULL WITH DEFAULT 0
		//             , T_NCOV_AMT      DECIMAL(11,2) NOT NULL WITH DEFAULT 0
		//             , T_PATIENT_NAME  CHAR(39)      NOT NULL WITH DEFAULT ' '
		//             , T_PERFORMING_NAME CHAR(25)      NOT NULL WITH DEFAULT ' '
		//             , T_BILLING_NAME  CHAR(25)      NOT NULL WITH DEFAULT ' '
		//             , T_CLM_INS_LN_CD CHAR(3)      NOT NULL WITH DEFAULT ' '
		//             , T_ALT_INS_ID    CHAR(12)     NOT NULL WITH DEFAULT ' '
		//               )
		//               ON COMMIT PRESERVE ROWS
		//           END-EXEC.
		//SQL statement db2sql:DeclareGlobalTempTable doesn't need a translation
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 CONTINUE
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: MOVE 'GLOBAL TEMP TABLE'       TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("GLOBAL TEMP TABLE");
			// COB_CODE: MOVE 'DECLARE TEMP TABLE '     TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("DECLARE TEMP TABLE ");
			// COB_CODE: MOVE '9100-DECLARE-TEMP-TABLE' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9100-DECLARE-TEMP-TABLE");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9110-INSERT-TEMP<br>
	 * <pre>    DISPLAY '** 9110-INSERT-TEMP **'.
	 * ***************************************************************
	 *    THIS PARAGRAPH INSERTS A ROW INTO THE TEMP DB2 TABLE FROM  *
	 *    THE TRIGGER FILE CREATED IN NF0530ML.                      *
	 *                                                               *
	 *    PROCESSING OF CLAIMS BEGINS AFTER TEMP TABLE IS LOADED.    *
	 *                                                               *
	 * ***************************************************************</pre>*/
	private void insertTemp() {
		// COB_CODE: EXEC SQL
		//             INSERT INTO SESSION.TRIGGER1 (
		//                            T_PROD_IND
		//                          , T_CLM_CNTL_ID
		//                          , T_CLM_CNTL_SFX_ID
		//                          , T_PMT_ID
		//                          , T_FNL_BUS_DT
		//                          , T_STAT_CATG_CD
		//                          , T_BI_PROV_ID
		//                          , T_BI_PROV_LOB
		//                          , T_BI_SPEC_CD
		//                          , T_PE_PROV_ID
		//                          , T_PE_PROV_LOB
		//                          , T_ALPH_PRFX_CD
		//                          , T_INS_ID
		//                          , T_MEMBER_ID
		//                          , T_CLM_SYST_VER_ID
		//                          , T_CORP_RCV_DT
		//                          , T_ADJD_DT
		//                          , T_BILL_FRM_DT
		//                          , T_BILL_THR_DT
		//                          , T_ASG_CD
		//                          , T_CLM_PD_AM
		//                          , T_KCAPS_TEAM_NM
		//                          , T_KCAPS_USE_ID
		//                          , T_HIST_LOAD_CD
		//                          , T_PMT_BK_PROD_CD
		//                          , T_DRG
		//                          , T_PAT_ACT_ID
		//                          , T_PGM_AREA_CD
		//                          , T_FIN_CD
		//                          , T_ADJD_PROV_STAT
		//                          , T_PRMPT_PAY_DAY
		//                          , T_PRMPT_PAY_OVRD
		//                          , T_ITS_CLM_TYP_CD
		//                          , T_GRP_ID
		//                          , T_LOB_CD
		//                          , T_TYP_GRP_CD
		//                          , T_NPI_CD
		//                          , T_BEN_PLN_ID
		//                          , T_IRC_CD
		//                          , T_PMT_NTWRK_CD
		//                          , T_PRIM_CN_ARNG_CD
		//                          , T_ENR_CL_CD
		//                          , T_OI_CD
		//                          , T_PAT_RESP
		//                          , T_PWO_AMT
		//                          , T_NCOV_AMT
		//                          , T_PATIENT_NAME
		//                          , T_PERFORMING_NAME
		//                          , T_BILLING_NAME
		//                          , T_CLM_INS_LN_CD
		//                          , T_ALT_INS_ID)
		//                VALUES (:TRG-PROD-IND
		//                      , :TRG-CLM-CNTL-ID
		//                      , :TRG-CLM-CNTL-SFX-ID
		//                      , :WS-TRG-PMT-ID
		//                      , :TRG-FNL-BUS-DT
		//                      , :TRG-STAT-CATG-CD
		//                      , :TRG-BI-PROV-ID
		//                      , :TRG-BI-PROV-LOB
		//                      , :TRG-BI-SPEC-CD
		//                      , :TRG-PE-PROV-ID
		//                      , :TRG-PE-PROV-LOB
		//                      , :TRG-ALPH-PRFX-CD
		//                      , :TRG-INS-ID
		//                      , :TRG-MEMBER-ID
		//                      , :TRG-CLM-SYST-VER-ID
		//                      , :TRG-CORP-RCV-DT
		//                      , :TRG-ADJD-DT
		//                      , :TRG-BILL-FRM-DT
		//                      , :TRG-BILL-THR-DT
		//                      , :TRG-ASG-CD
		//                      , :TRG-CLM-PD-AM
		//                      , :TRG-KCAPS-TEAM-NM
		//                      , :TRG-KCAPS-USE-ID
		//                      , :TRG-HIST-LOAD-CD
		//                      , :TRG-PMT-BK-PROD-CD
		//                      , :TRG-DRG
		//                      , :TRG-PAT-ACT-ID
		//                      , :TRG-PGM-AREA-CD
		//                      , :TRG-FIN-CD
		//                      , :TRG-ADJD-PROV-STAT
		//                      , :TRG-PRMPT-PAY-DAY
		//                      , :TRG-PRMPT-PAY-OVRD
		//                      , :TRG-ITS-CLM-TYP-CD
		//                      , :TRG-GRP-ID
		//                      , :TRG-LOB-CD
		//                      , :TRG-TYP-GRP-CD
		//                      , :TRG-NPI-CD
		//                      , :TRG-BEN-PLN-ID
		//                      , :TRG-IRC-CD
		//                      , :TRG-NTWRK-CD
		//                      , :TRG-PRIM-CN-ARNG-CD
		//                      , :TRG-ENR-CL-CD
		//                      , :TRG-OI-CD
		//                      , :TRG-PAT-RESP
		//                      , :TRG-PWO-AMT
		//                      , :TRG-NCOV-AMT
		//                      , :TRG-PATIENT-NAME
		//                      , :TRG-PERFORMING-PROVIDER-NAME
		//                      , :TRG-BILLING-PROVIDER-NAME
		//                      , :TRG-CLM-INS-LN-CD
		//                      , :TRG-ALT-INS-ID)
		//           END-EXEC.
		trigger1Dao.insertRec(this);
		// COB_CODE:      EVALUATE SQLCODE
		//                  WHEN 0
		//                      ADD 1 TO WS-TRIGGER-LOADED
		//           D    DISPLAY 'TRG-CLM-CNTL-ID    *' TRG-CLM-CNTL-ID
		//           D    DISPLAY 'TRG-CLM-CNTL-SFX-ID*' TRG-CLM-CNTL-SFX-ID
		//           D    DISPLAY 'WS-TRG-PMT-ID      *' WS-TRG-PMT-ID
		//           D    DISPLAY 'TRG-CLM-INS-LN-CD  *' TRG-CLM-INS-LN-CD
		//                  WHEN OTHER
		//                      PERFORM 9999-DB2-ABEND
		//                END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: ADD 1 TO WS-TRIGGER-LOADED
			ws.getWsAllCounters().getWsCounters().setWsTriggerLoaded(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsTriggerLoaded(), 7));
			//    DISPLAY 'TRG-CLM-CNTL-ID    *' TRG-CLM-CNTL-ID
			//    DISPLAY 'TRG-CLM-CNTL-SFX-ID*' TRG-CLM-CNTL-SFX-ID
			//    DISPLAY 'WS-TRG-PMT-ID      *' WS-TRG-PMT-ID
			//    DISPLAY 'TRG-CLM-INS-LN-CD  *' TRG-CLM-INS-LN-CD
			break;

		default:// COB_CODE: MOVE 'GLOBAL TEMP TABLE'  TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("GLOBAL TEMP TABLE");
			// COB_CODE: MOVE 'INSERT TEMP TABLE ' TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("INSERT TEMP TABLE ");
			// COB_CODE: MOVE '9110-INSERT-TEMP '  TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9110-INSERT-TEMP ");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9200-OPEN-CURSOR<br>
	 * <pre>    DISPLAY '** 9200-OPEN-CURSOR **'.</pre>*/
	private void openCursor() {
		// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN.
		ws.getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
		// COB_CODE: MOVE GREGORN-HH   TO FORMAT-HH.
		ws.getFormattedTime().setFormatHhFormatted(ws.getGregorn().getrGregornTime().getGregornHhFormatted());
		// COB_CODE: MOVE GREGORN-MI   TO FORMAT-MM.
		ws.getFormattedTime().setFormatMmFormatted(ws.getGregorn().getrGregornTime().getGregornMiFormatted());
		// COB_CODE: MOVE GREGORN-SS   TO FORMAT-SS.
		ws.getFormattedTime().setFormatSsFormatted(ws.getGregorn().getrGregornTime().getGregornSsFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '   OPENING CURSOR AT ' FORMATTED-TIME.
		DisplayUtil.sysout.write("   OPENING CURSOR AT ", ws.getFormattedTime().getFormattedTimeFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: EXEC SQL
		//              OPEN EOB_CURSOR
		//           END-EXEC.
		s02805saS02809saS02811saS02813saS02952saTrigger1Dao.openEobCursor();
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 CONTINUE
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: MOVE 'EOB_CURSOR  '          TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("EOB_CURSOR  ");
			// COB_CODE: MOVE 'OPEN  EOB CURSOR  '    TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("OPEN  EOB CURSOR  ");
			// COB_CODE: MOVE '9200-OPEN-CURSOR'      TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9200-OPEN-CURSOR");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
		// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN.
		ws.getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
		// COB_CODE: MOVE GREGORN-HH   TO FORMAT-HH.
		ws.getFormattedTime().setFormatHhFormatted(ws.getGregorn().getrGregornTime().getGregornHhFormatted());
		// COB_CODE: MOVE GREGORN-MI   TO FORMAT-MM.
		ws.getFormattedTime().setFormatMmFormatted(ws.getGregorn().getrGregornTime().getGregornMiFormatted());
		// COB_CODE: MOVE GREGORN-SS   TO FORMAT-SS.
		ws.getFormattedTime().setFormatSsFormatted(ws.getGregorn().getrGregornTime().getGregornSsFormatted());
		// COB_CODE: DISPLAY ' THE CURSOR HAS BEEN OPENED  AT  '
		//                    FORMATTED-TIME.
		DisplayUtil.sysout.write(" THE CURSOR HAS BEEN OPENED  AT  ", ws.getFormattedTime().getFormattedTimeFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
	}

	/**Original name: 9300-FETCH-EOB<br>
	 * <pre>    DISPLAY '** 9300-FETCH-EOB **'.</pre>*/
	private void fetchEob() {
		// COB_CODE: EXEC SQL
		//               FETCH EOB_CURSOR
		//                   INTO  :DB2-PMT-ADJ-TYP-CD
		//                       , :DB2-VOID-CD
		//                       , :DB2-PMT-PRIM-CN-ARNG-CD
		//                       , :DB2-PMT-RATE-CD
		//                       , :DB2-PROVIDER-NAME
		//                       , :DB2-PAT-LST-NM
		//                       , :DB2-PAT-FRST-NM
		//                       , :DB2-PAT-MID-NM
		//                       , :DB2-PROD-IND
		//                       , :DB2-CLM-CNTL-ID
		//                       , :DB2-CLM-CNTL-SFX-ID
		//                       , :DB2-CLM-PMT-ID
		//                       , :DB2-FNL-BUS-DT
		//                       , :DB2-STAT-CATG-CD
		//                       , :DB2-BI-PROV-ID
		//                       , :DB2-BI-PROV-LOB
		//                       , :DB2-BI-PROV-SPEC
		//                       , :DB2-PE-PROV-ID
		//                       , :DB2-PE-PROV-LOB
		//                       , :DB2-PMT-ALPH-PRFX-CD
		//                       , :DB2-PMT-INS-ID
		//                       , :DB2-PMT-ALT-INS-ID
		//                       , :DB2-PMT-MEM-ID
		//                       , :DB2-PMT-CLM-SYST-VER-ID
		//                       , :DB2-PMT-CORP-RCV-DT
		//                       , :DB2-PMT-ADJD-DT
		//                       , :DB2-PMT-BILL-FRM-DT
		//                       , :DB2-PMT-BILL-THR-DT
		//                       , :DB2-PMT-ASG-CD
		//                       , :DB2-PMT-CLM-PD-AM
		//                       , :DB2-PMT-KCAPS-TEAM-NM
		//                       , :DB2-PMT-KCAPS-USE-ID
		//                       , :DB2-PMT-HIST-LOAD-CD
		//                       , :DB2-PMT-PMT-BK-PROD-CD
		//                       , :DB2-PMT-DRG
		//                       , :DB2-PMT-PAT-ACT-ID
		//                       , :DB2-PMT-PGM-AREA-CD
		//                       , :DB2-PMT-FIN-CD
		//                       , :DB2-PMT-ADJD-PROV-STAT-CD
		//                       , :DB2-PMT-PRMPT-PAY-DAY
		//                       , :DB2-PMT-PRMPT-PAY-OVRD
		//                       , :DB2-PMT-ITS-CLM-TYP-CD
		//                       , :DB2-PMT-GRP-ID
		//                       , :DB2-PMT-LOB-CD
		//                       , :DB2-PMT-TYP-GRP-CD
		//                       , :DB2-PMT-NPI-CD
		//                       , :DB2-PMT-BEN-PLN-ID
		//                       , :DB2-PMT-IRC-CD
		//                       , :DB2-PMT-NTWRK-CD
		//                       , :DB2-PMT-PRIM-CN-ARNG-CD
		//                       , :DB2-PMT-ENR-CL-CD
		//                       , :DB2-PMT-OI-CD
		//                       , :WS-DB2-PAT-RESP
		//                       , :WS-DB2-PWO-AMT
		//                       , :WS-DB2-NCOV-AMT
		//                       , :PATIENT-NAME
		//                       , :PERFORMING-NAME
		//                       , :BILLING-NAME
		//                       , :DB2-CLI-ID
		//                       , :DB2-LI-FRM-SERV-DT
		//                       , :DB2-LI-THR-SERV-DT
		//                       , :DB2-LI-BEN-TYP-CD
		//                       , :DB2-LI-EXMN-ACTN-CD
		//                       , :DB2-LI-RMRK-CD
		//                       , :DB2-LI-EXPLN-CD
		//                       , :DB2-LI-RVW-BY-CD
		//                       , :DB2-LI-ADJD-OVRD1-CD
		//                       , :DB2-LI-ADJD-OVRD2-CD
		//                       , :DB2-LI-ADJD-OVRD3-CD
		//                       , :DB2-LI-ADJD-OVRD4-CD
		//                       , :DB2-LI-ADJD-OVRD5-CD
		//                       , :DB2-LI-ADJD-OVRD6-CD
		//                       , :DB2-LI-ADJD-OVRD7-CD
		//                       , :DB2-LI-ADJD-OVRD8-CD
		//                       , :DB2-LI-ADJD-OVRD9-CD
		//                       , :DB2-LI-ADJD-OVRD10-CD
		//                       , :DB2-LI-POS-CD
		//                       , :DB2-LI-TS-CD
		//                       , :DB2-LI-CHG-AM
		//                       , :DB2-LI-PAT-RESP-AM
		//                       , :DB2-LI-PWO-AM
		//                       , :DB2-PMT-ADJD-PROV-STAT-CD
		//                       , :DB2-LI-ALW-CHG-AM
		//                       , :DB2-LI-ALCT-OI-COV-AM
		//                       , :DB2-LI-ALCT-OI-PD-AM
		//                       , :DB2-LI-ALCT-OI-SAVE-AM
		//                       , :DB2-LI-PROC-CD
		//                       , :DB2-C1-CALC-TYP-CD
		//                       , :DB2-C1-CALC-EXPLN-CD
		//                       , :DB2-C1-PAY-CD
		//                       , :DB2-C1-COV-LOB-CD
		//                       , :DB2-C1-ALW-CHG-AM
		//                       , :DB2-C1-PD-AM
		//                       , :DB2-C1-CORP-CD
		//                       , :DB2-C1-PROD-CD
		//                       , :DB2-C1-GL-ACCT-ID
		//                       , :DB2-C1-TYP-BEN-CD
		//                       , :DB2-C1-SPECI-BEN-CD
		//                       , :DB2-C1-INS-TC-CD
		//                       , :DB2-C2-CALC-TYP-CD
		//                       , :DB2-C2-CALC-EXPLN-CD
		//                       , :DB2-C2-PAY-CD
		//                       , :DB2-C2-COV-LOB-CD
		//                       , :DB2-C2-ALW-CHG-AM
		//                       , :DB2-C2-PD-AM
		//                       , :DB2-C2-CORP-CD
		//                       , :DB2-C2-PROD-CD
		//                       , :DB2-C2-GL-ACCT-ID
		//                       , :DB2-C2-TYP-BEN-CD
		//                       , :DB2-C2-SPECI-BEN-CD
		//                       , :DB2-C2-INS-TC-CD
		//                       , :DB2-C3-CALC-TYP-CD
		//                       , :DB2-C3-CALC-EXPLN-CD
		//                       , :DB2-C3-PAY-CD
		//                       , :DB2-C3-COV-LOB-CD
		//                       , :DB2-C3-ALW-CHG-AM
		//                       , :DB2-C3-PD-AM
		//                       , :DB2-C3-CORP-CD
		//                       , :DB2-C3-PROD-CD
		//                       , :DB2-C3-GL-ACCT-ID
		//                       , :DB2-C3-TYP-BEN-CD
		//                       , :DB2-C3-SPECI-BEN-CD
		//                       , :DB2-C3-INS-TC-CD
		//                       , :DB2-C4-CALC-TYP-CD
		//                       , :DB2-C4-CALC-EXPLN-CD
		//                       , :DB2-C4-PAY-CD
		//                       , :DB2-C4-COV-LOB-CD
		//                       , :DB2-C4-ALW-CHG-AM
		//                       , :DB2-C4-PD-AM
		//                       , :DB2-C4-CORP-CD
		//                       , :DB2-C4-PROD-CD
		//                       , :DB2-C4-GL-ACCT-ID
		//                       , :DB2-C4-TYP-BEN-CD
		//                       , :DB2-C4-SPECI-BEN-CD
		//                       , :DB2-C4-INS-TC-CD
		//                       , :DB2-C5-CALC-TYP-CD
		//                       , :DB2-C5-CALC-EXPLN-CD
		//                       , :DB2-C5-PAY-CD
		//                       , :DB2-C5-COV-LOB-CD
		//                       , :DB2-C5-ALW-CHG-AM
		//                       , :DB2-C5-PD-AM
		//                       , :DB2-C5-CORP-CD
		//                       , :DB2-C5-PROD-CD
		//                       , :DB2-C5-GL-ACCT-ID
		//                       , :DB2-C5-TYP-BEN-CD
		//                       , :DB2-C5-SPECI-BEN-CD
		//                       , :DB2-C5-INS-TC-CD
		//                       , :DB2-C6-CALC-TYP-CD
		//                       , :DB2-C6-CALC-EXPLN-CD
		//                       , :DB2-C6-PAY-CD
		//                       , :DB2-C6-COV-LOB-CD
		//                       , :DB2-C6-ALW-CHG-AM
		//                       , :DB2-C6-PD-AM
		//                       , :DB2-C6-CORP-CD
		//                       , :DB2-C6-PROD-CD
		//                       , :DB2-C6-GL-ACCT-ID
		//                       , :DB2-C6-TYP-BEN-CD
		//                       , :DB2-C6-SPECI-BEN-CD
		//                       , :DB2-C6-INS-TC-CD
		//                       , :DB2-C7-CALC-TYP-CD
		//                       , :DB2-C7-CALC-EXPLN-CD
		//                       , :DB2-C7-PAY-CD
		//                       , :DB2-C7-COV-LOB-CD
		//                       , :DB2-C7-ALW-CHG-AM
		//                       , :DB2-C7-PD-AM
		//                       , :DB2-C7-CORP-CD
		//                       , :DB2-C7-PROD-CD
		//                       , :DB2-C7-GL-ACCT-ID
		//                       , :DB2-C7-TYP-BEN-CD
		//                       , :DB2-C7-SPECI-BEN-CD
		//                       , :DB2-C7-INS-TC-CD
		//                       , :DB2-C8-CALC-TYP-CD
		//                       , :DB2-C8-CALC-EXPLN-CD
		//                       , :DB2-C8-PAY-CD
		//                       , :DB2-C8-COV-LOB-CD
		//                       , :DB2-C8-ALW-CHG-AM
		//                       , :DB2-C8-PD-AM
		//                       , :DB2-C8-CORP-CD
		//                       , :DB2-C8-PROD-CD
		//                       , :DB2-C8-GL-ACCT-ID
		//                       , :DB2-C8-TYP-BEN-CD
		//                       , :DB2-C8-SPECI-BEN-CD
		//                       , :DB2-C8-INS-TC-CD
		//                       , :DB2-C9-CALC-TYP-CD
		//                       , :DB2-C9-CALC-EXPLN-CD
		//                       , :DB2-C9-PAY-CD
		//                       , :DB2-C9-COV-LOB-CD
		//                       , :DB2-C9-ALW-CHG-AM
		//                       , :DB2-C9-PD-AM
		//                       , :DB2-C9-CORP-CD
		//                       , :DB2-C9-PROD-CD
		//                       , :DB2-C9-GL-ACCT-ID
		//                       , :DB2-C9-TYP-BEN-CD
		//                       , :DB2-C9-SPECI-BEN-CD
		//                       , :DB2-C9-INS-TC-CD
		//                       , :WS-INDUS-CD
		//                       , :DB2-PMT-CK-ID
		//                       , :DB2-PMT-MDGP-PLN-CD
		//                       , :DB2-ACR-PRMT-PA-INT-AM
		//                       , :DB2-CLM-INS-LN-CD
		//                       , :DB2-CLM-SYST-VER-ID
		//                       , :DB2-PA-ID-TO-ACUM-ID
		//                       , :DB2-MEM-ID-TO-ACUM-ID
		//                       , :DB2-VBR-IN
		//                       , :DB2-PMT-BAL-BILL-AM
		//                       , :DB2-SPEC-DRUG-CPN-IN
		//           END-EXEC.
		s02805saS02809saS02811saS02813saS02952saTrigger1Dao.fetchEobCursor(ws.getS02805saS02809saS02811saS02813saS02952saTrigger1Nf0533ml());
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 PERFORM 3900-SET-COPAY-MAX-NOTE THRU 3900-EXIT
		//             WHEN +100
		//                   MOVE 'Y' TO WS-END-OF-CURSOR-SW
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: ADD 1 TO WS-FETCH-CNTR
			ws.getWsAllCounters().getWsCounters().setWsFetchCntr(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsFetchCntr(), 7));
			// COB_CODE: ADD 1 TO COMMIT-COUNTER
			ws.getWsAllCounters().getWsCounters().setCommitCounter(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getCommitCounter(), 7));
			// COB_CODE: MOVE ZEROS TO DB2-CAS-NCOV-AM
			ws.getEobDb2CursorArea().getCasCalc1Info().setNcovAm(new AfDecimal(0, 15, 2));
			// COB_CODE: MOVE ZEROS TO DB2-CAS-EOB-DED-AM
			ws.getEobDb2CursorArea().getCasCalc1Info().setEobDedAm(new AfDecimal(0, 15, 2));
			// COB_CODE: MOVE ZEROS TO DB2-CAS-EOB-COINS-AM
			ws.getEobDb2CursorArea().getCasCalc1Info().setEobCoinsAm(new AfDecimal(0, 15, 2));
			// COB_CODE: MOVE ZEROS TO DB2-CAS-EOB-COPAY-AM
			ws.getEobDb2CursorArea().getCasCalc1Info().setEobCopayAm(new AfDecimal(0, 15, 2));
			// COB_CODE: MOVE WS-INDUS-CD TO DB2-HC-INDUS-CD
			ws.getEobDb2CursorArea().setHcIndusCd(ws.getWsNf0533WorkArea().getWsIndusCd());
			// COB_CODE: PERFORM 3000-SELECT-CAS-DATA THRU 3000-EXIT
			selectCasData();
			// COB_CODE: PERFORM 3900-SET-COPAY-MAX-NOTE THRU 3900-EXIT
			setCopayMaxNote();
			break;

		case 100:// COB_CODE: MOVE 'Y' TO WS-END-OF-CURSOR-SW
			ws.getWsSwitches().setWsEndOfCursorSwFormatted("Y");
			break;

		default:// COB_CODE: MOVE 'EOB_CURSOR  '              TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("EOB_CURSOR  ");
			// COB_CODE: MOVE 'FETCH EOB CURSOR  '        TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("FETCH EOB CURSOR  ");
			// COB_CODE: MOVE '9300-FETCH-EOB '           TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9300-FETCH-EOB ");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9305-CLEAN-UP-RESTART<br>
	 * <pre>    DISPLAY '** 9305-CLEAN-UP-RESTART'.</pre>*/
	private void cleanUpRestart() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE SPACES TO RSTRT1-TX.
		ws.getDcls03086sa().setRstrt1Tx("");
		// COB_CODE: MOVE SPACES TO RSTRT2-TX.
		ws.getDcls03086sa().setRstrt2Tx("");
		// COB_CODE: MOVE 0      TO CMT-CT.
		ws.getDcls03086sa().setCmtCt(0);
		// COB_CODE: PERFORM 9030-UPDATE-S03086SA THRU 9030-EXIT.
		updateS03086sa();
		// COB_CODE: EXEC SQL
		//              UPDATE S03085SA
		//                  SET RSTRT_IN       = 'N'
		//                    , JBSTP_END_TS   = CURRENT_TIMESTAMP
		//                  WHERE JOB_NM       = :DCLS03085SA.JOB-NM
		//                    AND JBSTP_NM     = :DCLS03085SA.JBSTP-NM
		//                    AND JBSTP_SEQ_ID = :DCLS03085SA.JBSTP-SEQ-ID
		//           END-EXEC.
		s03085saDao.updateRec1(ws.getDcls03085sa().getJobNm(), ws.getDcls03085sa().getJbstpNm(), ws.getDcls03085sa().getJbstpSeqId());
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: STRING WS-RESTART-JOB-PARMS
			//                DELIMITED BY '&'    INTO DB2-ERR-KEY
			ws.getWsDisplayWorkArea()
					.setDb2ErrKey(Functions.substringBefore(ws.getWsRestartStuff().getWsRestartJobParms().getWsRestartJobParmsFormatted(), "&"));
			// COB_CODE: MOVE 'S03085SA'                TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S03085SA");
			// COB_CODE: MOVE 'UPDATE S03085SA EOJ'     TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("UPDATE S03085SA EOJ");
			// COB_CODE: MOVE '9305-CLEAN-UP-RESTART '  TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9305-CLEAN-UP-RESTART ");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9600-UPDATE-S02813SA<br>
	 * <pre>    DISPLAY '** 9600-UPDATE-S02813SA **'.</pre>*/
	private void updateS02813sa() {
		// COB_CODE: EXEC SQL
		//             UPDATE S02813SA
		//                 SET CK_ID            = :DCLS02813SA.CK-ID
		//                   , CK_DT            = :DCLS02813SA.CK-DT
		//                   , PRMPT_PAY_INT_AM = :DCLS02813SA.PRMPT-PAY-INT-AM
		//                   , ACR_PRMT_PA_INT_AM =
		//                     :DCLS02813SA.ACR-PRMT-PA-INT-AM
		//                   , GL_OFST_ORIG_CD  = :WS-GL-OFST-ORIG-CD
		//                   , GL_SOTE_ORIG_CD  = :WS-GL-SOTE-ORIG-CD
		//                   , INFO_CHG_ID      = 'NF0533ML'
		//                   , INFO_CHG_TS      = CURRENT_TIMESTAMP
		//               WHERE CLM_CNTL_ID      = :DCLS02813SA.CLM-CNTL-ID
		//                 AND CLM_CNTL_SFX_ID  = :DCLS02813SA.CLM-CNTL-SFX-ID
		//                 AND CLM_PMT_ID       = :DCLS02813SA.CLM-PMT-ID
		//           END-EXEC.
		s02813saDao.updateRec1(ws);
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 ADD 1 TO WS-S02813-UPDATE
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: ADD 1 TO WS-S02813-UPDATE
			ws.getWsAllCounters().getWsCounters().setWsS02813Update(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsS02813Update(), 7));
			break;

		default:// COB_CODE: MOVE 'S02813SA'              TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S02813SA");
			// COB_CODE: MOVE 'UPDATE S02813SA'       TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("UPDATE S02813SA");
			// COB_CODE: MOVE '9600-UPDATE-S02813SA ' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9600-UPDATE-S02813SA ");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9700-INSERT-S02814SA<br>
	 * <pre>    DISPLAY '** 9700-INSERT-S02814SA **'.</pre>*/
	private void insertS02814sa() {
		// COB_CODE: EXEC SQL
		//               INSERT INTO S02814SA
		//                          (CLM_CNTL_ID
		//                        ,  CLM_CNTL_SFX_ID
		//                        ,  CLM_PMT_ID
		//                        ,  EVNT_LOG_STAT_CD
		//                        ,  EVNT_LOG_BUS_DT
		//                        ,  EVNT_LOG_SYST_DT
		//                        ,  INFO_SSYST_ID
		//                        ,  INFO_INSRT_PGM_NM
		//                        ,  KCAPS_TEAM_NM
		//                        ,  KCAPS_USE_ID
		//                        ,  INFO_CHG_ID)
		//                  VALUES (:DCLS02814SA.CLM-CNTL-ID
		//                        , :DCLS02814SA.CLM-CNTL-SFX-ID
		//                        , :DCLS02814SA.CLM-PMT-ID
		//                        , :DCLS02814SA.EVNT-LOG-STAT-CD
		//                        , :DCLS02814SA.EVNT-LOG-BUS-DT
		//                        , :DCLS02814SA.EVNT-LOG-SYST-DT
		//                        , 'NF05'
		//                        , 'NF0533ML'
		//                        , :DB2-PMT-KCAPS-TEAM-NM
		//                        , :DB2-PMT-KCAPS-USE-ID
		//                        , 'NF0533ML')
		//           END-EXEC.
		s02814saDao.insertRec(ws.getS02814saNf0533ml());
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 ADD 1 TO WS-S02814-MTH-QTR
		//             WHEN -530
		//                 DISPLAY '** NO PMT: '  DB2-ERR-KEY
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: ADD 1 TO WS-S02814-MTH-QTR
			ws.getWsAllCounters().getWsCounters().setWsS02814MthQtr(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsS02814MthQtr(), 7));
			break;

		case -530:// COB_CODE: MOVE HOLD-CLAIM-ID    TO WS-ERR-ID
			ws.getWsDisplayWorkArea().getWsDb2ErrKey().setId(ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted());
			// COB_CODE: MOVE HOLD-CLAIM-SFX   TO WS-ERR-SFX-ID
			ws.getWsDisplayWorkArea().getWsDb2ErrKey().setSfxId(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx());
			// COB_CODE: MOVE HOLD-PMT-ID      TO WS-ERR-PMT-ID
			ws.getWsDisplayWorkArea().getWsDb2ErrKey().setPmtIdFormatted(ws.getWsNf0533WorkArea().getHoldKey().getPmtIdFormatted());
			// COB_CODE: MOVE WS-DB2-ERR-KEY  TO DB2-ERR-KEY
			ws.getWsDisplayWorkArea().setDb2ErrKey(ws.getWsDisplayWorkArea().getWsDb2ErrKey().getWsDb2ErrKeyFormatted());
			// COB_CODE: DISPLAY '** NO PMT: '  DB2-ERR-KEY
			DisplayUtil.sysout.write("** NO PMT: ", ws.getWsDisplayWorkArea().getDb2ErrKeyFormatted());
			break;

		default:// COB_CODE: MOVE 'S02814SA'              TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S02814SA");
			// COB_CODE: MOVE 'INSERT INTO S02814SA'  TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("INSERT INTO S02814SA");
			// COB_CODE: MOVE '9700-INSERT-S02814SA ' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9700-INSERT-S02814SA ");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9750-INSERT-S02814SA-SUB<br>
	 * <pre>    DISPLAY '** 9750-INSERT-S02814SA-SUB **'.</pre>*/
	private void insertS02814saSub() {
		// COB_CODE: EXEC SQL
		//               INSERT INTO S02814SA
		//                          (CLM_CNTL_ID
		//                        ,  CLM_CNTL_SFX_ID
		//                        ,  CLM_PMT_ID
		//                        ,  EVNT_LOG_STAT_CD
		//                        ,  EVNT_LOG_BUS_DT
		//                        ,  EVNT_LOG_SYST_DT
		//                        ,  INFO_SSYST_ID
		//                        ,  INFO_INSRT_PGM_NM
		//                        ,  KCAPS_TEAM_NM
		//                        ,  KCAPS_USE_ID
		//                        ,  INFO_CHG_ID)
		//                  VALUES (:DCLS02814SA.CLM-CNTL-ID
		//                        , :DCLS02814SA.CLM-CNTL-SFX-ID
		//                        , :DCLS02814SA.CLM-PMT-ID
		//                        , :DCLS02814SA.EVNT-LOG-STAT-CD
		//                        , CURRENT DATE
		//                        , :DCLS02814SA.EVNT-LOG-SYST-DT
		//                        , 'NF05'
		//                        , 'NF0533ML'
		//                        , :DCLS02814SA.KCAPS-TEAM-NM
		//                        , :DCLS02814SA.KCAPS-USE-ID
		//                        , 'NF0533ML')
		//           END-EXEC.
		s02814saDao.insertRec1(ws.getDcls02814sa());
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 ADD 1 TO WS-S02814-INSERTED
		//             WHEN -530
		//                 DISPLAY '** NO PMT: '  DB2-ERR-KEY
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: ADD 1 TO WS-S02814-INSERTED
			ws.getWsAllCounters().getWsCounters()
					.setWsS02814Inserted(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsS02814Inserted(), 7));
			break;

		case -530:// COB_CODE: MOVE HOLD-CLAIM-ID    TO WS-ERR-ID
			ws.getWsDisplayWorkArea().getWsDb2ErrKey().setId(ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted());
			// COB_CODE: MOVE HOLD-CLAIM-SFX   TO WS-ERR-SFX-ID
			ws.getWsDisplayWorkArea().getWsDb2ErrKey().setSfxId(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx());
			// COB_CODE: MOVE HOLD-PMT-ID      TO WS-ERR-PMT-ID
			ws.getWsDisplayWorkArea().getWsDb2ErrKey().setPmtIdFormatted(ws.getWsNf0533WorkArea().getHoldKey().getPmtIdFormatted());
			// COB_CODE: MOVE WS-DB2-ERR-KEY  TO DB2-ERR-KEY
			ws.getWsDisplayWorkArea().setDb2ErrKey(ws.getWsDisplayWorkArea().getWsDb2ErrKey().getWsDb2ErrKeyFormatted());
			// COB_CODE: DISPLAY '** NO PMT: '  DB2-ERR-KEY
			DisplayUtil.sysout.write("** NO PMT: ", ws.getWsDisplayWorkArea().getDb2ErrKeyFormatted());
			break;

		default:// COB_CODE: MOVE 'S02814SA'              TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S02814SA");
			// COB_CODE: MOVE 'INSERT INTO S02814SA'  TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("INSERT INTO S02814SA");
			// COB_CODE: MOVE '9750-INSERT-S02814SA ' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9750-INSERT-S02814SA ");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9800-INSERT-S02801SA<br>
	 * <pre>    DISPLAY '**  9800-INSERT-S02801SA **'.</pre>*/
	private void insertS02801sa() {
		// COB_CODE: EXEC SQL
		//               INSERT INTO S02801SA
		//                          (CLM_CNTL_ID
		//                        ,  CLM_CNTL_SFX_ID
		//                        ,  CLM_PMT_ID
		//                        ,  HCC_STAT_CATG_CD
		//                        ,  FNL_BUS_DT
		//                        ,  INFO_SSYST_ID
		//                        ,  INFO_INSRT_PGM_NM
		//                        ,  INFO_CHG_ID)
		//                  VALUES (:DCLS02801SA.CLM-CNTL-ID
		//                        , :DCLS02801SA.CLM-CNTL-SFX-ID
		//                        , :DCLS02801SA.CLM-PMT-ID
		//                        , :DCLS02801SA.HCC-STAT-CATG-CD
		//                        , :DCLS02801SA.FNL-BUS-DT
		//                        ,  'NF05'
		//                        ,  'NF0533ML'
		//                        ,  'NF0533ML')
		//           END-EXEC.
		s02801saDao.insertRec(ws.getDcls02801sa());
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 ADD 1 TO WS-S02801-INSERTED
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: ADD 1 TO WS-S02801-INSERTED
			ws.getWsAllCounters().getWsCounters()
					.setWsS02801Inserted(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getWsS02801Inserted(), 7));
			break;

		default:// COB_CODE: MOVE 'S02801SA'              TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("S02801SA");
			// COB_CODE: MOVE 'INSERT INTO S02801SA'  TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("INSERT INTO S02801SA");
			// COB_CODE: MOVE '9800-INSERT-S02801SA ' TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9800-INSERT-S02801SA ");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9990-CLOSE-EOB-CURSOR<br>
	 * <pre>    DISPLAY '** 9990-CLOSE-EOB-CURSOR'.</pre>*/
	private void closeEobCursor() {
		// COB_CODE: EXEC SQL
		//              CLOSE EOB_CURSOR
		//           END-EXEC.
		s02805saS02809saS02811saS02813saS02952saTrigger1Dao.closeEobCursor();
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 CONTINUE
		//             WHEN OTHER
		//                 PERFORM 9999-DB2-ABEND
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: MOVE 'EOB_CURSOR  '              TO DB2-ERR-TABLE
			ws.getWsDisplayWorkArea().setDb2ErrTable("EOB_CURSOR  ");
			// COB_CODE: MOVE 'CLOSE EOB CURSOR  '        TO DB2-ERR-LAST-CALL
			ws.getWsDisplayWorkArea().setDb2ErrLastCall("CLOSE EOB CURSOR  ");
			// COB_CODE: MOVE '9990-CLOSE-EOB-CURSOR'     TO DB2-ERR-PARA
			ws.getWsDisplayWorkArea().setDb2ErrPara("9990-CLOSE-EOB-CURSOR");
			// COB_CODE: PERFORM 9999-DB2-ABEND
			db2Abend();
			break;
		}
	}

	/**Original name: 9995-PROCESS-ERROR<br>
	 * <pre>    DISPLAY '** 9995-PROCESS-ERROR **'.</pre>*/
	private void processError() {
		// COB_CODE: MOVE DB2-PROD-IND               TO ERR-PROD-IND.
		errFileTO.setErrProdInd(ws.getEobDb2CursorArea().getS02801Info().getProdInd());
		// COB_CODE: MOVE DB2-CLM-CNTL-ID            TO ERR-CLM-CNTL-ID.
		errFileTO.setErrClmCntlId(ws.getEobDb2CursorArea().getS02801Info().getClmCntlId());
		// COB_CODE: MOVE DB2-CLM-CNTL-SFX-ID        TO ERR-CLM-CNTL-SFX.
		errFileTO.setErrClmCntlSfx(ws.getEobDb2CursorArea().getS02801Info().getClmCntlSfxId());
		// COB_CODE: MOVE DB2-CLM-PMT-ID             TO ERR-CLM-PMT-ID.
		errFileTO.setErrClmPmtId(TruncAbs.toShort(ws.getEobDb2CursorArea().getS02801Info().getClmPmtId(), 2));
		// COB_CODE: MOVE DB2-BI-PROV-ID             TO ERR-LOCAL-BILL-PROV-ID.
		errFileTO.getErrLocalBillProvInfo().setLocalBillProvId(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvId());
		// COB_CODE: MOVE DB2-BI-PROV-LOB          TO ERR-LOCAL-BILL-PROV-LOB-CD.
		errFileTO.getErrLocalBillProvInfo().setLocalBillProvLobCd(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvLob());
		// COB_CODE: MOVE WS-DB2-BI-NPI-PROV-ID    TO ERR-HIST-NPI-BILL-PROV-ID.
		errFileTO.setErrHistNpiBillProvId(ws.getWsNf0533WorkArea().getWsDb2BiNpiProvId());
		// COB_CODE: MOVE DB2-PMT-ADJ-TYP-CD         TO ERR-ADJ-TYP-CD.
		errFileTO.setErrAdjTypCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjTypCd());
		// COB_CODE: MOVE DB2-PMT-ASG-CD             TO ERR-ASG-CD.
		errFileTO.setErrAsgCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAsgCd());
		// COB_CODE: MOVE SPACES                     TO ERR-BILL-PROV-ID-QLF-CD.
		errFileTO.getErrLocalBillProvInfo().setBillProvIdQlfCd("");
		// COB_CODE: MOVE DB2-BI-PROV-SPEC           TO ERR-BILL-PROV-SPEC-CD.
		errFileTO.getErrLocalBillProvInfo().setBillProvSpecCd(ws.getEobDb2CursorArea().getS02815ProviderInfo().getBiProvSpec());
		// COB_CODE: MOVE DB2-PMT-INS-ID             TO ERR-INS-ID.
		errFileTO.setErrInsId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtInsId());
		// COB_CODE: MOVE WS-THIS-PROGRAM            TO ERR-PGM-NAME.
		errFileTO.setErrPgmName(ws.getWsConstants().getWsThisProgram());
		// COB_CODE: MOVE DB2-PMT-ADJD-DT            TO ERR-PMT-ADJD-DT.
		errFileTO.setErrPmtAdjdDt(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtAdjdDt());
		// COB_CODE: MOVE SPACES                     TO ERR-INFO-SSYST-ID.
		errFileTO.setErrInfoSsystId("");
		// COB_CODE: MOVE DB2-PMT-KCAPS-TEAM-NM      TO ERR-KCAPS-TEAM-NM.
		errFileTO.setErrKcapsTeamNm(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsTeamNm());
		// COB_CODE: MOVE DB2-PMT-KCAPS-USE-ID       TO ERR-KCAPS-USE-ID.
		errFileTO.setErrKcapsUseId(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsUseId());
		// COB_CODE: MOVE DB2-PMT-HIST-LOAD-CD       TO ERR-HIST-LOAD-CD.
		errFileTO.setErrHistLoadCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtHistLoadCd());
		// COB_CODE: MOVE DB2-PMT-PMT-BK-PROD-CD     TO ERR-PMT-BK-PROD-CD.
		errFileTO.setErrPmtBkProdCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtPmtBkProdCd());
		// COB_CODE: MOVE DB2-PMT-ITS-CLM-TYP-CD     TO ERR-ITS-CLM-TYP-CD.
		errFileTO.setErrItsClmTypCd(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtItsClmTypCd());
		// COB_CODE: MOVE DB2-PMT-CLM-PD-AM          TO ERR-CLM-PD-AM.
		errFileTO.setErrClmPdAm(Trunc.toDecimal(ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtClmPdAm(), 11, 2));
		// COB_CODE: WRITE ERR-FILE-REC FROM ERROR-FILE-LAYOUT.
		errFileTO.setData(errFileTO.getErrorFileLayoutBytes());
		errFileDAO.write(errFileTO);
		// COB_CODE: ADD 1 TO ERR-FILE-CNT.
		ws.getWsAllCounters().getWsCounters().setErrFileCnt(Trunc.toInt(1 + ws.getWsAllCounters().getWsCounters().getErrFileCnt(), 7));
		// COB_CODE: MOVE SPACES TO ERROR-FILE-LAYOUT.
		errFileTO.initErrorFileLayoutSpaces();
	}

	/**Original name: 9997-FORCE-EOJ<br>
	 * <pre>    DISPLAY '** 9997-FORCE-EOJ **'.
	 * - VALID VALUES FOR APPL-TERM-CD ARE:
	 * -     BLANK - RUN TILL GOOD END OF JOB.  RETURN-CODE = 0000.
	 * -     R     - FORCE END OF JOB BUT ALLOW SUBSEQUENT JOB STEPS
	 * -             AND JOBS TO RUN.  NO RESTART IS ANTICIPATED.
	 * -             RETURN CODE = 0000.
	 * -     T     - FORCE END OF JOB BUT DO NOT ALLOW SUBSEQUENT JOB
	 * -             STEPS AND JOBS TO RUN.  RESTART IS REQUIRED TO
	 * -             PROCESS REMAINING DATA.
	 * -             RETURN CODE = 0004.</pre>*/
	private void forceEoj() {
		// COB_CODE: MOVE 'YES' TO EOF-TRIGGER-SW.
		ws.getWsSwitches().setEofTriggerSw("YES");
		// COB_CODE: MOVE 'Y'   TO WS-END-OF-CURSOR-SW.
		ws.getWsSwitches().setWsEndOfCursorSwFormatted("Y");
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '*************************************************'.
		DisplayUtil.sysout.write("*************************************************");
		// COB_CODE: DISPLAY '*************************************************'.
		DisplayUtil.sysout.write("*************************************************");
		// COB_CODE: DISPLAY '     A T T E N T I O N    '.
		DisplayUtil.sysout.write("     A T T E N T I O N    ");
		// COB_CODE: DISPLAY '     A T T E N T I O N    '.
		DisplayUtil.sysout.write("     A T T E N T I O N    ");
		// COB_CODE: DISPLAY '     A T T E N T I O N    '.
		DisplayUtil.sysout.write("     A T T E N T I O N    ");
		// COB_CODE: DISPLAY '      NF0533ML STOPPING'.
		DisplayUtil.sysout.write("      NF0533ML STOPPING");
		// COB_CODE: DISPLAY ' APPL-TRM-CD VALUE FORCED EARLY END OF JOB.'.
		DisplayUtil.sysout.write(" APPL-TRM-CD VALUE FORCED EARLY END OF JOB.");
		// COB_CODE: DISPLAY ' APPL-TRM-CD = ' APPL-TRM-CD.
		DisplayUtil.sysout.write(" APPL-TRM-CD = ", String.valueOf(ws.getDcls03086sa().getApplTrmCd()));
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: EVALUATE APPL-TRM-CD
		//             WHEN 'R'
		//                       'SUBSEQUENT STEPS AND JOBS CAN RUN '
		//             WHEN 'T'
		//                       'TO FINISH PROCESSING DATA '
		//           END-EVALUATE.
		switch (ws.getDcls03086sa().getApplTrmCd()) {

		case 'R':// COB_CODE: DISPLAY ' NO RESTART IS PLANNED, '
			//                   'SUBSEQUENT STEPS AND JOBS CAN RUN '
			DisplayUtil.sysout.write(" NO RESTART IS PLANNED, ", "SUBSEQUENT STEPS AND JOBS CAN RUN ");
			break;

		case 'T':// COB_CODE: DISPLAY ' SUBSEQUENT STEPS AND JOBS SHOULD NOT RUN '
			DisplayUtil.sysout.write(" SUBSEQUENT STEPS AND JOBS SHOULD NOT RUN ");
			// COB_CODE: DISPLAY ' THIS JOB MUST BE RESTARTED LATER '
			//                   'TO FINISH PROCESSING DATA '
			DisplayUtil.sysout.write(" THIS JOB MUST BE RESTARTED LATER ", "TO FINISH PROCESSING DATA ");
			break;

		default:
			break;
		}
		// COB_CODE: DISPLAY ' MUST MANUALLY UPDATE APPL-TRM-CD '
		//                       'IN TABLE 3086 BEFORE RESTARTING '.
		DisplayUtil.sysout.write(" MUST MANUALLY UPDATE APPL-TRM-CD ", "IN TABLE 3086 BEFORE RESTARTING ");
		// COB_CODE: DISPLAY '*************************************************'.
		DisplayUtil.sysout.write("*************************************************");
		// COB_CODE: DISPLAY '*************************************************'.
		DisplayUtil.sysout.write("*************************************************");
	}

	/**Original name: 9998-ABEND-FROM-NU0201ML<br>
	 * <pre>    DISPLAY '** 9998-ABEND-FROM-NU0201ML **'.</pre>*/
	private void abendFromNu0201ml() {
		// COB_CODE: MOVE PROGRAM-STAT OF INDIC-STATUS-CODES
		//                            TO WS-DISP-PROG-STATUS.
		ws.getWsDisplayWorkArea().setWsDispProgStatus(ws.getProvPassedIndicRec().getProgramStat());
		// COB_CODE: MOVE DATABASE-STAT OF INDIC-STATUS-CODES
		//                            TO WS-DISP-DATABASE-STATUS.
		ws.getWsDisplayWorkArea().setWsDispDatabaseStatus(TruncAbs.toInt(ws.getProvPassedIndicRec().getDatabaseStat(), 9));
		// COB_CODE: IF DATABASE-STAT OF INDIC-STATUS-CODES < 0
		//               MOVE '-' TO WS-DISP-DB-STATUS-SIGN
		//           ELSE
		//               MOVE '+' TO WS-DISP-DB-STATUS-SIGN
		//           END-IF.
		if (ws.getProvPassedIndicRec().getDatabaseStat() < 0) {
			// COB_CODE: MOVE '-' TO WS-DISP-DB-STATUS-SIGN
			ws.getWsDisplayWorkArea().setWsDispDbStatusSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+' TO WS-DISP-DB-STATUS-SIGN
			ws.getWsDisplayWorkArea().setWsDispDbStatusSignFormatted("+");
		}
		// COB_CODE: DISPLAY '***************************************'.
		DisplayUtil.sysout.write("***************************************");
		// COB_CODE: DISPLAY WS-DISP-ABENDING-PARAGRAPH.
		DisplayUtil.sysout.write(ws.getWsDisplayWorkArea().getWsDispAbendingParagraphFormatted());
		// COB_CODE: DISPLAY '*   PROGRAM STATUS RETURNED : '
		//                            WS-DISP-PROG-STATUS.
		DisplayUtil.sysout.write("*   PROGRAM STATUS RETURNED : ", ws.getWsDisplayWorkArea().getWsDispProgStatusFormatted());
		// COB_CODE: DISPLAY '*   DATABASE STATUS RETURNED: '
		//                            WS-DISP-DB-STATUS-SIGN
		//                            WS-DISP-DATABASE-STATUS.
		DisplayUtil.sysout.write("*   DATABASE STATUS RETURNED: ", String.valueOf(ws.getWsDisplayWorkArea().getWsDispDbStatusSign()),
				ws.getWsDisplayWorkArea().getWsDispDatabaseStatusAsString());
		// COB_CODE: DISPLAY '*'.
		DisplayUtil.sysout.write("*");
		// COB_CODE: IF PROV-ERR-MSG-LG < 0
		//               MOVE '-' TO WS-DISP-ERR-MSG-SIGN
		//           ELSE
		//               MOVE '+' TO WS-DISP-ERR-MSG-SIGN.
		if (ws.getProvPassedIndicRec().getProvErrMsgLg() < 0) {
			// COB_CODE: MOVE '-' TO WS-DISP-ERR-MSG-SIGN
			ws.getWsDisplayWorkArea().setWsDispErrMsgSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+' TO WS-DISP-ERR-MSG-SIGN.
			ws.getWsDisplayWorkArea().setWsDispErrMsgSignFormatted("+");
		}
		// COB_CODE: MOVE PROV-ERR-MSG-LG TO WS-DISP-ERR-MSG-LG.
		ws.getWsDisplayWorkArea().setWsDispErrMsgLg(TruncAbs.toShort(ws.getProvPassedIndicRec().getProvErrMsgLg(), 4));
		// COB_CODE: DISPLAY '*   PROV-ERR-MSG-LG: '
		//               WS-DISP-ERR-MSG-SIGN    WS-DISP-ERR-MSG-LG.
		DisplayUtil.sysout.write("*   PROV-ERR-MSG-LG: ", String.valueOf(ws.getWsDisplayWorkArea().getWsDispErrMsgSign()),
				ws.getWsDisplayWorkArea().getWsDispErrMsgLgAsString());
		// COB_CODE: DISPLAY '*'.
		DisplayUtil.sysout.write("*");
		// COB_CODE: DISPLAY '*   PROV-ERR-MSG:'
		DisplayUtil.sysout.write("*   PROV-ERR-MSG:");
		// COB_CODE: PERFORM VARYING MSG-SUB FROM 1 BY 1
		//             UNTIL MSG-SUB IS GREATER THAN 10
		//               DISPLAY PROV-ERR-MSG (MSG-SUB)
		//           END-PERFORM.
		ws.setMsgSub(1);
		while (!(ws.getMsgSub() > 10)) {
			// COB_CODE: DISPLAY PROV-ERR-MSG (MSG-SUB)
			DisplayUtil.sysout.write(ws.getProvPassedIndicRec().getProvErrMsgFormatted(ws.getMsgSub()));
			ws.setMsgSub(Trunc.toInt(ws.getMsgSub() + 1, 5));
		}
		// COB_CODE: DISPLAY '*'.
		DisplayUtil.sysout.write("*");
		// COB_CODE: IF PROV-MSG-LG < 0
		//               MOVE '-' TO WS-DISP-ERR-MSG-SIGN
		//           ELSE
		//               MOVE '+' TO WS-DISP-ERR-MSG-SIGN.
		if (ws.getProvPassedIndicRec().getProvMsgLg() < 0) {
			// COB_CODE: MOVE '-' TO WS-DISP-ERR-MSG-SIGN
			ws.getWsDisplayWorkArea().setWsDispErrMsgSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+' TO WS-DISP-ERR-MSG-SIGN.
			ws.getWsDisplayWorkArea().setWsDispErrMsgSignFormatted("+");
		}
		// COB_CODE: MOVE PROV-MSG-LG TO WS-DISP-MSG-LG.
		ws.getWsDisplayWorkArea().setWsDispMsgLg(TruncAbs.toInt(ws.getProvPassedIndicRec().getProvMsgLg(), 9));
		// COB_CODE: DISPLAY '*   PROV-MSG-LG: '
		//               WS-DISP-ERR-MSG-SIGN    WS-DISP-MSG-LG.
		DisplayUtil.sysout.write("*   PROV-MSG-LG: ", String.valueOf(ws.getWsDisplayWorkArea().getWsDispErrMsgSign()),
				ws.getWsDisplayWorkArea().getWsDispMsgLgAsString());
		// COB_CODE: DISPLAY '*'.
		DisplayUtil.sysout.write("*");
		// COB_CODE: DISPLAY '*     PROVIDER NUMBER : ' PROV-ID OF
		//                                                  PROV-PASSED-INDIC-REC.
		DisplayUtil.sysout.write("*     PROVIDER NUMBER : ", ws.getProvPassedIndicRec().getProvPassedIndicInput().getIdFormatted());
		// COB_CODE: DISPLAY '*     PROVIDER LOB    : ' PROV-LOB-CD OF
		//                                                  PROV-PASSED-INDIC-REC.
		DisplayUtil.sysout.write("*     PROVIDER LOB    : ", String.valueOf(ws.getProvPassedIndicRec().getProvPassedIndicInput().getLobCd()));
		// COB_CODE: DISPLAY '*     CONTRACT ARNG CD: ' PROV-CN-ARNG-CD
		//                                             OF PROV-PASSED-INDIC-REC.
		DisplayUtil.sysout.write("*     CONTRACT ARNG CD: ", ws.getProvPassedIndicRec().getProvPassedIndicInput().getCnArngCdFormatted());
		// COB_CODE: DISPLAY '*     ADDRESS CODE    : ' PROV-AD-CD.
		DisplayUtil.sysout.write("*     ADDRESS CODE    : ", ws.getProvPassedIndicRec().getProvPassedIndicInput().getAdCdFormatted());
		// COB_CODE: DISPLAY '*     XREF TYPE-1     : ' PROV-XREF-NUM-1-TYPE.
		DisplayUtil.sysout.write("*     XREF TYPE-1     : ", ws.getProvPassedIndicRec().getProvPassedIndicInput().getXrefNum1TypeFormatted());
		// COB_CODE: DISPLAY '*     XREF TYPE-2     : ' PROV-XREF-NUM-2-TYPE.
		DisplayUtil.sysout.write("*     XREF TYPE-2     : ", ws.getProvPassedIndicRec().getProvPassedIndicInput().getXrefNum2TypeFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY HOLD-KEY.
		DisplayUtil.sysout.write(ws.getWsNf0533WorkArea().getHoldKey().getHoldKeyFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '***************************************'.
		DisplayUtil.sysout.write("***************************************");
		// COB_CODE: CALL 'ABEND'.
		DynamicCall.invoke("ABEND");
	}

	/**Original name: 9999-DB2-ABEND<br>
	 * <pre>              DB2 ERROR ROUTINE                                 *
	 *     DISPLAY '** 9999-DB2-ABEND **'.</pre>*/
	private void db2Abend() {
		GenericParam errorTextLen = null;
		// COB_CODE: IF SQLCODE < 0
		//               MOVE '-' TO WS-SQLCODE-SIGN
		//           ELSE
		//               MOVE '+' TO WS-SQLCODE-SIGN
		//           END-IF.
		if (sqlca.getSqlcode() < 0) {
			// COB_CODE: MOVE '-' TO WS-SQLCODE-SIGN
			ws.getWsDisplayWorkArea().setWsSqlcodeSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+' TO WS-SQLCODE-SIGN
			ws.getWsDisplayWorkArea().setWsSqlcodeSignFormatted("+");
		}
		// COB_CODE: DISPLAY '*******************************'.
		DisplayUtil.sysout.write("*******************************");
		// COB_CODE: DISPLAY '      NF0533ML       ABENDING  '.
		DisplayUtil.sysout.write("      NF0533ML       ABENDING  ");
		// COB_CODE: MOVE SQLCODE TO WS-SQLCODE.
		ws.getWsDisplayWorkArea().setWsSqlcode(TruncAbs.toInt(sqlca.getSqlcode(), 9));
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '***>>>  SQLCODE = ' WS-SQLCODE-UNPACKED
		//                   '       SQLSTATE = ' SQLSTATE.
		DisplayUtil.sysout.write("***>>>  SQLCODE = ", ws.getWsDisplayWorkArea().getWsSqlcodeUnpackedFormatted(), "       SQLSTATE = ",
				sqlca.getSqlstateFormatted());
		// COB_CODE: DISPLAY '***>>> SQLERRMC = ' SQLERRMC.
		DisplayUtil.sysout.write("***>>> SQLERRMC = ", sqlca.getSqlerrmcFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		//
		// COB_CODE: MOVE HOLD-CLAIM-ID    TO WS-ERR-ID.
		ws.getWsDisplayWorkArea().getWsDb2ErrKey().setId(ws.getWsNf0533WorkArea().getHoldKey().getClaimIdFormatted());
		// COB_CODE: MOVE HOLD-CLAIM-SFX   TO WS-ERR-SFX-ID.
		ws.getWsDisplayWorkArea().getWsDb2ErrKey().setSfxId(ws.getWsNf0533WorkArea().getHoldKey().getClaimSfx());
		// COB_CODE: MOVE HOLD-PMT-ID      TO WS-ERR-PMT-ID.
		ws.getWsDisplayWorkArea().getWsDb2ErrKey().setPmtIdFormatted(ws.getWsNf0533WorkArea().getHoldKey().getPmtIdFormatted());
		// COB_CODE: MOVE WS-DB2-ERR-KEY  TO DB2-ERR-KEY.
		ws.getWsDisplayWorkArea().setDb2ErrKey(ws.getWsDisplayWorkArea().getWsDb2ErrKey().getWsDb2ErrKeyFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY ' DB2-ERR-PARA  IS: ' DB2-ERR-PARA.
		DisplayUtil.sysout.write(" DB2-ERR-PARA  IS: ", ws.getWsDisplayWorkArea().getDb2ErrParaFormatted());
		// COB_CODE: DISPLAY ' DB2-ERR-TABLE IS: ' DB2-ERR-TABLE.
		DisplayUtil.sysout.write(" DB2-ERR-TABLE IS: ", ws.getWsDisplayWorkArea().getDb2ErrTableFormatted());
		// COB_CODE: DISPLAY ' DB2-ERR-KEY IS  : ' DB2-ERR-KEY.
		DisplayUtil.sysout.write(" DB2-ERR-KEY IS  : ", ws.getWsDisplayWorkArea().getDb2ErrKeyFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: CALL 'DSNTIAR' USING SQLCA
		//                                ERROR-MESSAGE
		//                                ERROR-TEXT-LEN.
		errorTextLen = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getWsDisplayWorkArea().getErrorTextLen()));
		DynamicCall.invoke("DSNTIAR", sqlca, ws.getWsDisplayWorkArea().getErrorMessage(), errorTextLen);
		ws.getWsDisplayWorkArea().setErrorTextLenFromBuffer(errorTextLen.getByteData());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '****************************************'.
		DisplayUtil.sysout.write("****************************************");
		// COB_CODE: DISPLAY 'CLAIMS SYSTEM BATCH DB2 ERROR EXPLANATION'.
		DisplayUtil.sysout.write("CLAIMS SYSTEM BATCH DB2 ERROR EXPLANATION");
		// COB_CODE: DISPLAY ERROR-TEXT(1).
		DisplayUtil.sysout.write(ws.getWsDisplayWorkArea().getErrorMessage().getTextFormatted(1));
		// COB_CODE: DISPLAY ERROR-TEXT(2).
		DisplayUtil.sysout.write(ws.getWsDisplayWorkArea().getErrorMessage().getTextFormatted(2));
		// COB_CODE: DISPLAY ERROR-TEXT(3).
		DisplayUtil.sysout.write(ws.getWsDisplayWorkArea().getErrorMessage().getTextFormatted(3));
		// COB_CODE: DISPLAY ERROR-TEXT(4).
		DisplayUtil.sysout.write(ws.getWsDisplayWorkArea().getErrorMessage().getTextFormatted(4));
		// COB_CODE: DISPLAY ERROR-TEXT(5).
		DisplayUtil.sysout.write(ws.getWsDisplayWorkArea().getErrorMessage().getTextFormatted(5));
		// COB_CODE: DISPLAY ERROR-TEXT(6).
		DisplayUtil.sysout.write(ws.getWsDisplayWorkArea().getErrorMessage().getTextFormatted(6));
		// COB_CODE: DISPLAY ERROR-TEXT(7).
		DisplayUtil.sysout.write(ws.getWsDisplayWorkArea().getErrorMessage().getTextFormatted(7));
		// COB_CODE: DISPLAY ERROR-TEXT(8).
		DisplayUtil.sysout.write(ws.getWsDisplayWorkArea().getErrorMessage().getTextFormatted(8));
		// COB_CODE: DISPLAY '***********************************************'.
		DisplayUtil.sysout.write("***********************************************");
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: EXEC SQL
		//              ROLLBACK
		//           END-EXEC.
		DbService.getCurrent().rollback(dbAccessStatus);
		// COB_CODE: CALL 'ABEND'.
		DynamicCall.invoke("ABEND");
	}

	@Override
	public DdCard[] getDefaultDdCards() {
		return new DdCard[] { new DdCard("RWRITOFF", 650), new DdCard("CYCLDATE", 80), new DdCard("KSSEXP", 150), new DdCard("TRIGERIN", 400),
				new DdCard("ENDDTE", 80), new DdCard("ERRORS", 200), new DdCard("REXPENS", 150), new DdCard("RESTART", 80),
				new DdCard("KSSEOB", 1400), new DdCard("REGEOB", 1400) };
	}

	public void initWsAllCounters() {
		ws.getWsAllCounters().setHeadingCntrFormatted("0000000");
		ws.getWsAllCounters().setRecLineCntrFormatted("0000000");
		ws.getWsAllCounters().setDetailLineCntrFormatted("0000000");
		ws.getWsAllCounters().setRegExpenseCountFormatted("0000000");
		ws.getWsAllCounters().setKssExpenseCountFormatted("0000000");
		ws.getWsAllCounters().setAdjustInDollars(new AfDecimal(0, 13, 2));
		ws.getWsAllCounters().setRegWriteoffCntrFormatted("0000000");
		ws.getWsAllCounters().setOutInterestCntrFormatted("0000000");
		ws.getWsAllCounters().setOutKssIntCntrFormatted("0000000");
		ws.getWsAllCounters().setTotTotalInterest(new AfDecimal(0, 11, 2));
		ws.getWsAllCounters().setTotLatePmtInterest(new AfDecimal(0, 11, 2));
		ws.getWsAllCounters().setTotLateDenialInterest(new AfDecimal(0, 11, 2));
		ws.getWsAllCounters().getWsCounters().setWsFetchCntrFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsTriggerInFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsTriggerLoadedFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsBypassPmtsFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsBypassProvFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsEobRegOutFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsLinesRegOutFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsLinesKssOutFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsLinesKssExpOutFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsItsY1CntFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsS02813UpdateFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsS04315UpdatesFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsS02813VoidUpdateFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsS02801InsertedFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsS02814InsertedFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsS02814MthQtrFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setWsMthQtrFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setCommitCounterFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setErrFileCntFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setSrsPrvMatchCntrFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setSrsDateBypassCntrFormatted("0000000");
		ws.getWsAllCounters().getWsCounters().setSrsClaimCntrFormatted("0000000");
	}

	public void initLineAccumulators() {
		ws.getWsRollLogicWork().getLineAccumulators().setPaid(new AfDecimal(0, 11, 2));
		ws.getWsRollLogicWork().getLineAccumulators().setNotcovd(new AfDecimal(0, 11, 2));
		ws.getWsRollLogicWork().getLineAccumulators().setPatOwes(new AfDecimal(0, 11, 2));
		ws.getWsRollLogicWork().getLineAccumulators().setDeductible(new AfDecimal(0, 11, 2));
		ws.getWsRollLogicWork().getLineAccumulators().setCoinsurance(new AfDecimal(0, 11, 2));
		ws.getWsRollLogicWork().getLineAccumulators().setCopay(new AfDecimal(0, 7, 2));
		ws.getWsRollLogicWork().getLineAccumulators().setWriteoff(new AfDecimal(0, 11, 2));
	}

	public void initClaimAccumulators() {
		ws.getWsRollLogicWork().getClaimAccumulators().setOiPaid(new AfDecimal(0, 13, 2));
		ws.getWsRollLogicWork().getClaimAccumulators().setPaid(new AfDecimal(0, 13, 2));
		ws.getWsRollLogicWork().getClaimAccumulators().setAllowd(new AfDecimal(0, 13, 2));
		ws.getWsRollLogicWork().getClaimAccumulators().setCharge(new AfDecimal(0, 13, 2));
		ws.getWsRollLogicWork().getClaimAccumulators().setWriteoff(new AfDecimal(0, 13, 2));
		ws.getWsRollLogicWork().getClaimAccumulators().setNotCovd(new AfDecimal(0, 13, 2));
		ws.getWsRollLogicWork().getClaimAccumulators().setPatOwes(new AfDecimal(0, 13, 2));
		ws.getWsRollLogicWork().getClaimAccumulators().setDeduct(new AfDecimal(0, 13, 2));
		ws.getWsRollLogicWork().getClaimAccumulators().setCoins(new AfDecimal(0, 13, 2));
		ws.getWsRollLogicWork().getClaimAccumulators().setCopay(new AfDecimal(0, 13, 2));
	}

	public void initWsHoldNoteArea() {
		ws.getWsHoldNoteArea().setNote1("");
		ws.getWsHoldNoteArea().setWhichNotesTable1("");
		ws.getWsHoldNoteArea().setNote2("");
		ws.getWsHoldNoteArea().setWhichNotesTable2("");
		ws.getWsHoldNoteArea().setNote3("");
		ws.getWsHoldNoteArea().setWhichNotesTable3("");
		ws.getWsHoldNoteArea().setNote4("");
		ws.getWsHoldNoteArea().setWhichNotesTable4("");
		ws.getWsHoldNoteArea().setNote5("");
		ws.getWsHoldNoteArea().setWhichNotesTable5("");
	}

	public void initWsPromptPayWorkFields() {
		ws.getWsPromptPayWorkFields().setTotalCheckAmount(new AfDecimal(0, 11, 2));
		ws.getWsPromptPayWorkFields().setSubscriberInterest(new AfDecimal(0, 11, 2));
		ws.getWsPromptPayWorkFields().setAccruedSubInterest(new AfDecimal(0, 11, 2));
		ws.getWsPromptPayWorkFields().setPromptPayDays(0);
		ws.getWsPromptPayWorkFields().setClaimAge(0);
		ws.getWsPromptPayWorkFields().getHoldPrmptPayDayCd().setHoldPrmptPayDayCd("");
		ws.getWsPromptPayWorkFields().setLatePaymentInterest(new AfDecimal(0, 9, 2));
		ws.getWsPromptPayWorkFields().setLateDenialInterest(new AfDecimal(0, 9, 2));
		ws.getWsPromptPayWorkFields().setClaimLinePayments(new AfDecimal(0, 11, 2));
		ws.getWsPromptPayWorkFields().setClaimLineDenials(new AfDecimal(0, 11, 2));
		ws.getWsPromptPayWorkFields().setClaimLineInterest(new AfDecimal(0, 11, 2));
	}

	public void initNf05LineLevelRecord() {
		ws.getNf05EobRecord().getNf05LineLevelRecord().setFromServiceDateCc("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setFromServiceDateYy("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setFromServiceDateMm("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setFromServiceDateDd("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setThruServiceDateCc("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setThruServiceDateYy("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setThruServiceDateMm("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setThurServiceDateDd("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setExaminerAction(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTypeService("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setPlaceService("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBenefitType("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setDnlRemark("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setRvwByCd("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setAdjdProvStatCd("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setLiAlwChgAm(new AfDecimal(0, 11, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setIndusCd("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setAmountCharged(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setLineNotCovrdAmt(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setLineOthrInsur(new AfDecimal(0, 10, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOtherInsureCovered(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOtherInsurePaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOtherInsureSavings(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setDeductCarryover(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setFepDentPpoPrvWrtoff(new AfDecimal(0, 8, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setFepDentPpoInsLiab(new AfDecimal(0, 8, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setLineExplanationCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setSelfReferralIndicator(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes1("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote1("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes2("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote2("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes3("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote3("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes4("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote4("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes5("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote5("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setVoidLiPdAm(new AfDecimal(0, 11, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setVoidDnlRemark("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setShareDateFlag(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode1("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode2("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode3("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode4("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode5("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode6("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode7("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode8("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode9("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode10("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setAltInsId("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setPaIdToAcumId("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setMemIdToAcumId("");
	}

	public void initNf05TotalRecord() {
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrNum("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrLobFormatted("0");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrStatus(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrEin("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrSsn("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrZip("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrSpec("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBillPvdrNameLst("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTotalCharge(new AfDecimal(0, 10, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTotalNonCovrd(new AfDecimal(0, 10, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTotalOthrInsur(new AfDecimal(0, 10, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTotalDeduct(new AfDecimal(0, 7, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTotalCopay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTotalCoinsur(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTotalPatRespons(new AfDecimal(0, 10, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTotalProvWriteoff(new AfDecimal(0, 10, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTotalAmountPaid(new AfDecimal(0, 11, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setPaidTo("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setN1N2CheckTotal(new AfDecimal(0, 11, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTotFiller1("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setGenNumber("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setGenDateFormatted("0000000");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setInpatTypeService("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setPerfProviderName("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setClmInsLnCd("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setPmtMdgpPlanCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBenPlnId("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setClmFromServiceDate("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setClmThruServiceDate("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTotFiller2("");
	}

	public void initWsExpFileRec() {
		ws.getExpenseRecord().setExpAccountNum("");
		ws.getExpenseRecord().setExpClaimId("");
		ws.getExpenseRecord().setExpClaimIdSuffix(Types.SPACE_CHAR);
		ws.getExpenseRecord().setExpInsId("");
		ws.getExpenseRecord().setExpBillProvId("");
		ws.getExpenseRecord().getExpBillProvLob().setExpBillProvLob(Types.SPACE_CHAR);
		ws.getExpenseRecord().getExpBankIndicator().setExpBankIndicator(Types.SPACE_CHAR);
		ws.getExpenseRecord().getExpAssignedIndc().setExpAssignedIndc("");
		ws.getExpenseRecord().setExpAmountPaid(new AfDecimal(0, 11, 2));
		ws.getExpenseRecord().getExpReportIndc().setExpReportIndcFormatted("00");
		ws.getExpenseRecord().setExpTeam("");
		ws.getExpenseRecord().setExpId("");
		ws.getExpenseRecord().getExpWrapIndc().setExpWrapIndc(Types.SPACE_CHAR);
		ws.getExpenseRecord().getExpWrapDateCcyyMmDd().setCc("");
		ws.getExpenseRecord().getExpWrapDateCcyyMmDd().setYy("");
		ws.getExpenseRecord().getExpWrapDateCcyyMmDd().setSep11(Types.SPACE_CHAR);
		ws.getExpenseRecord().getExpWrapDateCcyyMmDd().setMm("");
		ws.getExpenseRecord().getExpWrapDateCcyyMmDd().setSep12(Types.SPACE_CHAR);
		ws.getExpenseRecord().getExpWrapDateCcyyMmDd().setDd("");
		ws.getExpenseRecord().getExpExaminerAction().setExpExaminerAction(Types.SPACE_CHAR);
		ws.getExpenseRecord().setExpNpiProvNo("");
		ws.getExpenseRecord().setExpGenLdgrOffset("");
		ws.getExpenseRecord().setExpDivisionCode(Types.SPACE_CHAR);
		ws.getExpenseRecord().setExpProductCode("");
		ws.getExpenseRecord().setExpProgramArea("");
		ws.getExpenseRecord().setExpEnrollmentClass("");
		ws.getExpenseRecord().setExpFinancialCode("");
		ws.getExpenseRecord().setExpConvrtdTypGrpCd(Types.SPACE_CHAR);
		ws.getExpenseRecord().setExpAdjustmentType(Types.SPACE_CHAR);
		ws.getExpenseRecord().setExpHistoryLoadInd(Types.SPACE_CHAR);
		ws.getExpenseRecord().setExpItsInd(Types.SPACE_CHAR);
		ws.getExpenseRecord().setExpReimburseInd("");
		ws.getExpenseRecord().setExpExplanationCode(Types.SPACE_CHAR);
		ws.getExpenseRecord().setExpNetworkCode("");
		ws.getExpenseRecord().setExpMemberId("");
		ws.getExpenseRecord().setExpPaymentNumFormatted("00");
		ws.getExpenseRecord().setExpLineNumFormatted("000");
		ws.getExpenseRecord().setExpCalcType(Types.SPACE_CHAR);
		ws.getExpenseRecord().setExpTypGrpCd("");
		ws.getExpenseRecord().setExpClmSystVerId("");
		ws.getExpenseRecord().setExpClmInsLnCd("");
		ws.getExpenseRecord().setFillerX("");
	}

	public void initVoidInformation() {
		ws.getVoidInformation().getVoidS02813Info().setClmCntlId("");
		ws.getVoidInformation().getVoidS02813Info().setClmCntlSfxId(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02813Info().setClmPmtId(((short) 0));
		ws.getVoidInformation().getVoidS02813Info().setPmtAsgCd("");
		ws.getVoidInformation().getVoidS02813Info().setPmtAdjTypCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02813Info().setClmPdAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02813Info().setPmtEligInstReimb("");
		ws.getVoidInformation().getVoidS02813Info().setPmtEligProfReimb("");
		ws.getVoidInformation().getVoidS02813Info().setPmtHistLoadCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02813Info().setPmtKcapsTeamNm("");
		ws.getVoidInformation().getVoidS02813Info().setPmtKcapsUseId("");
		ws.getVoidInformation().getVoidS02813Info().setPmtPgmAreaCd("");
		ws.getVoidInformation().getVoidS02813Info().setPmtFinCd("");
		ws.getVoidInformation().getVoidS02813Info().setPmtEnrClCd("");
		ws.getVoidInformation().getVoidS02813Info().setPmtNtwrkCd("");
		ws.getVoidInformation().getVoidS02809LineInfo().setCliId(((short) 0));
		ws.getVoidInformation().getVoidS02809LineInfo().setLiChgAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02809LineInfo().setLiPatRespAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02809LineInfo().setLiPwoAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02809LineInfo().setLiAlctOiPdAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02809LineInfo().setLiNcovAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02809LineInfo().setLiDnlRmrkCd("");
		ws.getVoidInformation().getVoidS02809LineInfo().setLiExplnCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc1Info().setC1CalcExplnCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc1Info().setC1PdAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc1Info().setAsEobDedAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc1Info().setAsEobCoinsAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc1Info().setAsEobCopayAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc1Info().setC1GlAcctId("");
		ws.getVoidInformation().getVoidS02811Calc1Info().setC1CorpCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc1Info().setC1ProdCd("");
		ws.getVoidInformation().getVoidS02811Calc2Info().setC1CalcExplnCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc2Info().setC1PdAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc2Info().setAsEobDedAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc2Info().setAsEobCoinsAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc2Info().setAsEobCopayAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc2Info().setC1GlAcctId("");
		ws.getVoidInformation().getVoidS02811Calc2Info().setC1CorpCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc2Info().setC1ProdCd("");
		ws.getVoidInformation().getVoidS02811Calc3Info().setC1CalcExplnCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc3Info().setC1PdAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc3Info().setAsEobDedAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc3Info().setAsEobCoinsAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc3Info().setAsEobCopayAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc3Info().setC1GlAcctId("");
		ws.getVoidInformation().getVoidS02811Calc3Info().setC1CorpCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc3Info().setC1ProdCd("");
		ws.getVoidInformation().getVoidS02811Calc4Info().setC1CalcExplnCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc4Info().setC1PdAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc4Info().setAsEobDedAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc4Info().setAsEobCoinsAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc4Info().setAsEobCopayAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc4Info().setC1GlAcctId("");
		ws.getVoidInformation().getVoidS02811Calc4Info().setC1CorpCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc4Info().setC1ProdCd("");
		ws.getVoidInformation().getVoidS02811Calc5Info().setC1CalcExplnCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc5Info().setC1PdAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc5Info().setAsEobDedAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc5Info().setAsEobCoinsAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc5Info().setAsEobCopayAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc5Info().setC1GlAcctId("");
		ws.getVoidInformation().getVoidS02811Calc5Info().setC1CorpCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc5Info().setC1ProdCd("");
		ws.getVoidInformation().getVoidS02811Calc6Info().setC1CalcExplnCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc6Info().setC1PdAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc6Info().setAsEobDedAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc6Info().setAsEobCoinsAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc6Info().setAsEobCopayAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc6Info().setC1GlAcctId("");
		ws.getVoidInformation().getVoidS02811Calc6Info().setC1CorpCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc6Info().setC1ProdCd("");
		ws.getVoidInformation().getVoidS02811Calc7Info().setC1CalcExplnCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc7Info().setC1PdAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc7Info().setAsEobDedAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc7Info().setAsEobCoinsAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc7Info().setAsEobCopayAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc7Info().setC1GlAcctId("");
		ws.getVoidInformation().getVoidS02811Calc7Info().setC1CorpCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc7Info().setC1ProdCd("");
		ws.getVoidInformation().getVoidS02811Calc8Info().setC1CalcExplnCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc8Info().setC1PdAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc8Info().setAsEobDedAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc8Info().setAsEobCoinsAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc8Info().setAsEobCopayAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc8Info().setC1GlAcctId("");
		ws.getVoidInformation().getVoidS02811Calc8Info().setC1CorpCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc8Info().setC1ProdCd("");
		ws.getVoidInformation().getVoidS02811Calc9Info().setC1CalcExplnCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc9Info().setC1PdAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc9Info().setAsEobDedAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc9Info().setAsEobCoinsAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc9Info().setAsEobCopayAm(new AfDecimal(0, 11, 2));
		ws.getVoidInformation().getVoidS02811Calc9Info().setC1GlAcctId("");
		ws.getVoidInformation().getVoidS02811Calc9Info().setC1CorpCd(Types.SPACE_CHAR);
		ws.getVoidInformation().getVoidS02811Calc9Info().setC1ProdCd("");
	}

	public void initNf05EobRecord() {
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProdInd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ClaimNum().setLotNum("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ClaimNum().setControlDateYyFormatted("00");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ClaimNum().setControlDateJjjFormatted("000");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ClaimSfx(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05PmtNumFormatted("00");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05LineNumFormatted("00000");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecordType().setNf05RecordType(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ClaimLob().setNf05ClaimLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ItsClaimType().setNf05ItsClaimType(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ReviewIndc().setNf05ReviewIndc(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProvTypCnCd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05InstReimbMthdCd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProfReimbMthdCd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05Filler("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05TypeGroup("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05PlaceServiceWhy("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05FepRefundWithheldAmt(new AfDecimal(0, 8, 2));
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05FepDbrPrvRntDt(0);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05FepManagedCareAmt(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05HistoryLoadIndc().setNf05HistoryLoadIndc(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05Team("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05Id("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05AssignedIndc().setNf05AssignedIndc("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05GroupAndAccount12().setNf05GroupAndAccountX("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05DateReceived("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05DatePaid("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05GlOffstCd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05InsuredIdPrefix("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05InsuredIdDigits("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05SubscriberName().setLast("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05SubscriberName().setTitle("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05SubscriberName().setFirst("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05SubscriberName().setMi(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05Address().setAddress1("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05Address().setAddress2("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05Address().setAddressCity("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05Address().setFiller("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05Address().setAddressState("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05Address().setAddressZip("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05MemberId("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf053rdPtyPayeeData().setIndc(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf053rdPtyPayeeData().setPayeeName("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf053rdPtyPayeeData().setPayeeName2("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf053rdPtyPayeeData().setPayeeStreet("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf053rdPtyPayeeData().setPayeeCity("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf053rdPtyPayeeData().setPayeeState("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf053rdPtyPayeeData().setPayeeZip("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf053rdPtyPayeeData().setPayeeZip4("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProviderName("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ClfmRun("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ClfmSt(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05PrmptPayIntAm(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05PatientAccountNum("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05PatientRelation("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05LineProvWriteoff(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05LinePatRespons(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05CrossReimburseIndc().setNf05CrossReimburseIndc("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05ShieldReimburseIndc().setNf05CrossReimburseIndc("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05NationProgramIndc().setNf05NationProgramIndc(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05PerfProviderNum("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05DiagnosisCodesData().setCode1("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05DiagnosisCodesData().setCode2("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05DiagnosisCodesData().setCode3("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05DiagnosisCodesData().setCode4("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05DiagnosisCodesData().setCode5("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05DiagnosisCodesData().setCode6("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05DiagnosisCodesData().setCode7("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05DiagnosisCodesData().setCode8("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05DiagnosisCodesData().setCode9("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05DiagnosisCodesData().setCode10("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05BalBillAm(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05PatientName().setLast("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05PatientName().setTitle("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05PatientName().setFirst("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05PatientName().setMi(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecieptDate().setCc("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecieptDate().setYy("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecieptDate().setMm("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05RecieptDate().setDd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05AdjudicationDate().setCc("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05AdjudicationDate().setYy("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05AdjudicationDate().setMm("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05AdjudicationDate().setDd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05BillingFromDate().setCc("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05BillingFromDate().setYy("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05BillingFromDate().setMm("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05BillingFromDate().setDd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05BillingThruDate().setCc("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05BillingThruDate().setYy("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05BillingThruDate().setMm("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05BillingThruDate().setDd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05MtQtIndicator(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05AdjTypCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05BaseCaCd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05RateCd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().getNf05OtherInsureCode().setNf05OtherInsureCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ProvStopPayCd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05WasAnN2Claim(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05TypGrpCd("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ClmSystVerId("");
		ws.getNf05EobRecord().getNf05HeaderPartOfRecord().setNf05ClmCorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setFromServiceDateCc("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setFromServiceDateYy("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setFromServiceDateMm("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setFromServiceDateDd("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setThruServiceDateCc("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setThruServiceDateYy("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setThruServiceDateMm("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setThurServiceDateDd("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setExaminerAction(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setTypeService("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setPlaceService("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setBenefitType("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setDnlRemark("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setRvwByCd("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setAdjdProvStatCd("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setLiAlwChgAm(new AfDecimal(0, 11, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setIndusCd("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setAmountCharged(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setLineNotCovrdAmt(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setLineOthrInsur(new AfDecimal(0, 10, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOtherInsureCovered(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOtherInsurePaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOtherInsureSavings(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc1ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc2ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc3ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc4ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc5ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc6ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc7ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc8ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9AmountPaid(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9Deductible(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9Copay(new AfDecimal(0, 5, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9Coinsurance(new AfDecimal(0, 9, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9CorpCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9ProdCode("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9TypeBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9SpecBenefit("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9CovLob(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9PayCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9TypeContract("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setCalc9ExplnCd(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setDeductCarryover(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setFepDentPpoPrvWrtoff(new AfDecimal(0, 8, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setFepDentPpoInsLiab(new AfDecimal(0, 8, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setLineExplanationCode(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setSelfReferralIndicator(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes1("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote1("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes2("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote2("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes3("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote3("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes4("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote4("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setNotes5("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setWhichNotesTableNote5("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setVoidLiPdAm(new AfDecimal(0, 11, 2));
		ws.getNf05EobRecord().getNf05LineLevelRecord().setVoidDnlRemark("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setShareDateFlag(Types.SPACE_CHAR);
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode1("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode2("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode3("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode4("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode5("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode6("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode7("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode8("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode9("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setOverrideCode10("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setAltInsId("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setPaIdToAcumId("");
		ws.getNf05EobRecord().getNf05LineLevelRecord().setMemIdToAcumId("");
	}

	public void initWsSubroutine() {
		ws.getWsSubroutine().getSbrHeaderData().getAssignedIndc().setAssignedIndc("");
		ws.getWsSubroutine().getSbrHeaderData().getHistoryLoadIndc().setHistoryLoadIndc(Types.SPACE_CHAR);
		ws.getWsSubroutine().getSbrHeaderData().setPipProviderIndc(Types.SPACE_CHAR);
		ws.getWsSubroutine().getSbrHeaderData().getItsClaimType().setItsClaimType(Types.SPACE_CHAR);
		ws.getWsSubroutine().getSbrHeaderData().setEftIndc(Types.SPACE_CHAR);
		ws.getWsSubroutine().getSbrHeaderData().setTypGrpCd("");
		ws.getWsSubroutine().getSbrHeaderData().setFepIndicator(Types.SPACE_CHAR);
		for (int idx0 = 1; idx0 <= WsSubroutine.SBR_VOID_REPAY_DATA_MAXOCCURS; idx0++) {
			ws.getWsSubroutine().getSbrVoidRepayData(idx0).setVoidCd(Types.SPACE_CHAR);
			ws.getWsSubroutine().getSbrVoidRepayData(idx0).getAdjustmentType().setAdjustmentType(Types.SPACE_CHAR);
			ws.getWsSubroutine().getSbrVoidRepayData(idx0).setTotalAmountPaid(new AfDecimal(0, 11, 2));
		}
		for (int idx0 = 1; idx0 <= WsSubroutine.SBR_GEN_LDGR_OFFSET_INDC_MAXOCCURS; idx0++) {
			ws.getWsSubroutine().setSbrGenLdgrOffsetIndc(idx0, "");
		}
		for (int idx0 = 1; idx0 <= WsSubroutine.SBR_REPORT_INDC_MAXOCCURS; idx0++) {
			ws.getWsSubroutine().setSbrReportIndcFormatted(idx0, "00");
		}
	}

	public void initRemittTapeRecord() {
		writeoffRegTO.getHeaderPartOfRecord().setTapeNpiBillPvdrNum("");
		writeoffRegTO.getHeaderPartOfRecord().setTapeLocalBillPvdrLob(Types.SPACE_CHAR);
		writeoffRegTO.getHeaderPartOfRecord().setTapeLocalBillPvdrNum("");
		writeoffRegTO.getHeaderPartOfRecord().setTapeRaSequenceNumber("");
		writeoffRegTO.getHeaderPartOfRecord().getTapeRaAddrName().setLast("");
		writeoffRegTO.getHeaderPartOfRecord().getTapeRaAddrName().setFirst("");
		writeoffRegTO.getHeaderPartOfRecord().getTapeRaAddrName().setMid("");
		writeoffRegTO.getHeaderPartOfRecord().setTapeRaAddr1("");
		writeoffRegTO.getHeaderPartOfRecord().setTapeRaAddr2("");
		writeoffRegTO.getHeaderPartOfRecord().setTapeAddrCity("");
		writeoffRegTO.getHeaderPartOfRecord().setTapeAddrSt("");
		writeoffRegTO.getHeaderPartOfRecord().setTapeAddrZip5("");
		writeoffRegTO.getHeaderPartOfRecord().setTapeAddrZip4("");
		writeoffRegTO.getHeaderPartOfRecord().setTapeClaimLob(Types.SPACE_CHAR);
		writeoffRegTO.getHeaderPartOfRecord().getTapeSubtotIndc().setTapeSubtotIndc(Types.SPACE_CHAR);
		writeoffRegTO.getHeaderPartOfRecord().setVoidInd(Types.SPACE_CHAR);
		writeoffRegTO.getHeaderPartOfRecord().setCompany("");
		writeoffRegTO.getHeaderPartOfRecord().setTapeBankIndicator(Types.SPACE_CHAR);
		writeoffRegTO.getHeaderPartOfRecord().setItsClaimType(Types.SPACE_CHAR);
		writeoffRegTO.getDetailPartOfRecord().setTapeGrpAlpPrfxCd("");
		writeoffRegTO.getDetailPartOfRecord().setDetlIdNum("");
		writeoffRegTO.getDetailPartOfRecord().setDetlSubscriberDigits("");
		writeoffRegTO.getDetailPartOfRecord().setDetlClaimId("");
		writeoffRegTO.getDetailPartOfRecord().setDetlClaimIdSuffix(Types.SPACE_CHAR);
		writeoffRegTO.getDetailPartOfRecord().setRelPaymentFormatted("0000");
		writeoffRegTO.getDetailPartOfRecord().setLineNumFormatted("000");
		writeoffRegTO.getDetailPartOfRecord().getDetlPatientName().setLast("");
		writeoffRegTO.getDetailPartOfRecord().getDetlPatientName().setTitle("");
		writeoffRegTO.getDetailPartOfRecord().getDetlPatientName().setFirst("");
		writeoffRegTO.getDetailPartOfRecord().getDetlPatientName().setMi(Types.SPACE_CHAR);
		writeoffRegTO.getDetailPartOfRecord().setDetlPatientAcct("");
		writeoffRegTO.getDetailPartOfRecord().setDetlPos("");
		writeoffRegTO.getDetailPartOfRecord().setDetlOtlr(Types.SPACE_CHAR);
		writeoffRegTO.getDetailPartOfRecord().getDetlServDate().setCcFormatted("00");
		writeoffRegTO.getDetailPartOfRecord().getDetlServDate().setYyFormatted("00");
		writeoffRegTO.getDetailPartOfRecord().getDetlServDate().setMmFormatted("00");
		writeoffRegTO.getDetailPartOfRecord().getDetlServDate().setDdFormatted("00");
		writeoffRegTO.getDetailPartOfRecord().getAdjuDrgProc().setCode("");
		writeoffRegTO.getDetailPartOfRecord().getAdjuDrgProc().setMod1("");
		writeoffRegTO.getDetailPartOfRecord().getAdjuDrgProc().setMod2("");
		writeoffRegTO.getDetailPartOfRecord().getSubmDrgProc().setCode("");
		writeoffRegTO.getDetailPartOfRecord().getSubmDrgProc().setMod1("");
		writeoffRegTO.getDetailPartOfRecord().getSubmDrgProc().setMod2("");
		writeoffRegTO.getDetailPartOfRecord().setDetlDaysFormatted("000");
		writeoffRegTO.getDetailPartOfRecord().setDetlTotalCharge(new AfDecimal(0, 9, 2));
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlAllowedAmt(new AfDecimal(0, 9, 2));
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlPvdrWriteoff(new AfDecimal(0, 9, 2));
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlAmtOtherAdj(new AfDecimal(0, 9, 2));
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlAmtNotCovered(new AfDecimal(0, 9, 2));
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlAmtCoinsurance(new AfDecimal(0, 9, 2));
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlAmtDeductible(new AfDecimal(0, 9, 2));
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlAmtCopay(new AfDecimal(0, 9, 2));
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlAmtPatOwes(new AfDecimal(0, 9, 2));
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlAmtPaidWithoutInt(new AfDecimal(0, 9, 2));
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlBegBalanceForMsg(new AfDecimal(0, 9, 2));
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlAmtInterestPd(new AfDecimal(0, 9, 2));
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlBegOrEnd("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlRxNum("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlTeam("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlId("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlTthSrfcCd("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlTthId("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlOpPrcProcCd("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().getDetlReceiptDate().setCcFormatted("00");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().getDetlReceiptDate().setYyFormatted("00");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().getDetlReceiptDate().setMmFormatted("00");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().getDetlReceiptDate().setDdFormatted("00");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlClassOfCntr1("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlClassOfCntr2("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().getDetlAdjustRsnCds().setCd1("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().getDetlAdjustRsnCds().setCd2("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().getDetlAdjustRsnCds().setCd3("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().getDetlAdjustRsnCds().setCd4("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlRemarks1("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlRemarks2("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlDenialRemarkCode("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlAdjustReasonCode("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlAdjustRespCode(Types.SPACE_CHAR);
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setDetlAdjustTypeCode(Types.SPACE_CHAR);
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setTapeNpiPerfPvdrNum("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setTapeLocalPerfPvdrNum("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setTapeNetwork("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setPendMessage117("");
		writeoffRegTO.getDetailPartOfRecord().getDetlContinued().setPendMessage18100("");
		writeoffRegTO.getRemLineOfBusiness().setRemLineOfBusiness("");
		writeoffRegTO.getRemPrintNoPrint().setRemPrintNoPrint("");
		writeoffRegTO.getRemTypeOfRemit().setRemTypeOfRemit("");
	}

	@Override
	public String getTrgAdjdDt() {
		return triggerInTO.getTriggerRecordLayout().getAdjdDt();
	}

	@Override
	public void setTrgAdjdDt(String trgAdjdDt) {
		this.triggerInTO.getTriggerRecordLayout().setAdjdDt(trgAdjdDt);
	}

	@Override
	public char getTrgAdjdProvStat() {
		return triggerInTO.getTriggerRecordLayout().getAdjdProvStat();
	}

	@Override
	public void setTrgAdjdProvStat(char trgAdjdProvStat) {
		this.triggerInTO.getTriggerRecordLayout().setAdjdProvStat(trgAdjdProvStat);
	}

	@Override
	public String getTrgAlphPrfxCd() {
		return triggerInTO.getTriggerRecordLayout().getAlphPrfxCd();
	}

	@Override
	public void setTrgAlphPrfxCd(String trgAlphPrfxCd) {
		this.triggerInTO.getTriggerRecordLayout().setAlphPrfxCd(trgAlphPrfxCd);
	}

	@Override
	public String getTrgAltInsId() {
		return triggerInTO.getTriggerRecordLayout().getAltInsId();
	}

	@Override
	public void setTrgAltInsId(String trgAltInsId) {
		this.triggerInTO.getTriggerRecordLayout().setAltInsId(trgAltInsId);
	}

	@Override
	public String getTrgAsgCd() {
		return triggerInTO.getTriggerRecordLayout().getAsgCd();
	}

	@Override
	public void setTrgAsgCd(String trgAsgCd) {
		this.triggerInTO.getTriggerRecordLayout().setAsgCd(trgAsgCd);
	}

	@Override
	public String getTrgBenPlnId() {
		return triggerInTO.getTriggerRecordLayout().getBenPlnId();
	}

	@Override
	public void setTrgBenPlnId(String trgBenPlnId) {
		this.triggerInTO.getTriggerRecordLayout().setBenPlnId(trgBenPlnId);
	}

	@Override
	public String getTrgBiProvId() {
		return triggerInTO.getTriggerRecordLayout().getBiProvId();
	}

	@Override
	public void setTrgBiProvId(String trgBiProvId) {
		this.triggerInTO.getTriggerRecordLayout().setBiProvId(trgBiProvId);
	}

	@Override
	public char getTrgBiProvLob() {
		return triggerInTO.getTriggerRecordLayout().getBiProvLob();
	}

	@Override
	public void setTrgBiProvLob(char trgBiProvLob) {
		this.triggerInTO.getTriggerRecordLayout().setBiProvLob(trgBiProvLob);
	}

	@Override
	public String getTrgBiSpecCd() {
		return triggerInTO.getTriggerRecordLayout().getBiSpecCd();
	}

	@Override
	public void setTrgBiSpecCd(String trgBiSpecCd) {
		this.triggerInTO.getTriggerRecordLayout().setBiSpecCd(trgBiSpecCd);
	}

	@Override
	public String getTrgBillFrmDt() {
		return triggerInTO.getTriggerRecordLayout().getBillFrmDt();
	}

	@Override
	public void setTrgBillFrmDt(String trgBillFrmDt) {
		this.triggerInTO.getTriggerRecordLayout().setBillFrmDt(trgBillFrmDt);
	}

	@Override
	public String getTrgBillThrDt() {
		return triggerInTO.getTriggerRecordLayout().getBillThrDt();
	}

	@Override
	public void setTrgBillThrDt(String trgBillThrDt) {
		this.triggerInTO.getTriggerRecordLayout().setBillThrDt(trgBillThrDt);
	}

	@Override
	public String getTrgBillingProviderName() {
		return triggerInTO.getTriggerRecordLayout().getBillingProviderName();
	}

	@Override
	public void setTrgBillingProviderName(String trgBillingProviderName) {
		this.triggerInTO.getTriggerRecordLayout().setBillingProviderName(trgBillingProviderName);
	}

	@Override
	public String getTrgClmCntlId() {
		return triggerInTO.getTriggerRecordLayout().getKey().getCntlId();
	}

	@Override
	public void setTrgClmCntlId(String trgClmCntlId) {
		this.triggerInTO.getTriggerRecordLayout().getKey().setCntlId(trgClmCntlId);
	}

	@Override
	public char getTrgClmCntlSfxId() {
		return triggerInTO.getTriggerRecordLayout().getKey().getCntlSfxId();
	}

	@Override
	public void setTrgClmCntlSfxId(char trgClmCntlSfxId) {
		this.triggerInTO.getTriggerRecordLayout().getKey().setCntlSfxId(trgClmCntlSfxId);
	}

	@Override
	public String getTrgClmInsLnCd() {
		return triggerInTO.getTriggerRecordLayout().getClmInsLnCd();
	}

	@Override
	public void setTrgClmInsLnCd(String trgClmInsLnCd) {
		this.triggerInTO.getTriggerRecordLayout().setClmInsLnCd(trgClmInsLnCd);
	}

	@Override
	public AfDecimal getTrgClmPdAm() {
		return triggerInTO.getTriggerRecordLayout().getClmPdAm();
	}

	@Override
	public void setTrgClmPdAm(AfDecimal trgClmPdAm) {
		this.triggerInTO.getTriggerRecordLayout().setClmPdAm(trgClmPdAm.copy());
	}

	@Override
	public String getTrgClmSystVerId() {
		return triggerInTO.getTriggerRecordLayout().getClmSystVerId();
	}

	@Override
	public void setTrgClmSystVerId(String trgClmSystVerId) {
		this.triggerInTO.getTriggerRecordLayout().setClmSystVerId(trgClmSystVerId);
	}

	@Override
	public String getTrgCorpRcvDt() {
		return triggerInTO.getTriggerRecordLayout().getCorpRcvDt();
	}

	@Override
	public void setTrgCorpRcvDt(String trgCorpRcvDt) {
		this.triggerInTO.getTriggerRecordLayout().setCorpRcvDt(trgCorpRcvDt);
	}

	@Override
	public String getTrgDrg() {
		return triggerInTO.getTriggerRecordLayout().getDrg();
	}

	@Override
	public void setTrgDrg(String trgDrg) {
		this.triggerInTO.getTriggerRecordLayout().setDrg(trgDrg);
	}

	@Override
	public String getTrgEnrClCd() {
		return triggerInTO.getTriggerRecordLayout().getEnrClCd();
	}

	@Override
	public void setTrgEnrClCd(String trgEnrClCd) {
		this.triggerInTO.getTriggerRecordLayout().setEnrClCd(trgEnrClCd);
	}

	@Override
	public String getTrgFinCd() {
		return triggerInTO.getTriggerRecordLayout().getFinCd();
	}

	@Override
	public void setTrgFinCd(String trgFinCd) {
		this.triggerInTO.getTriggerRecordLayout().setFinCd(trgFinCd);
	}

	@Override
	public String getTrgFnlBusDt() {
		return triggerInTO.getTriggerRecordLayout().getFnlBusDt();
	}

	@Override
	public void setTrgFnlBusDt(String trgFnlBusDt) {
		this.triggerInTO.getTriggerRecordLayout().setFnlBusDt(trgFnlBusDt);
	}

	@Override
	public String getTrgGrpId() {
		return triggerInTO.getTriggerRecordLayout().getGrpId();
	}

	@Override
	public void setTrgGrpId(String trgGrpId) {
		this.triggerInTO.getTriggerRecordLayout().setGrpId(trgGrpId);
	}

	@Override
	public char getTrgHistLoadCd() {
		return triggerInTO.getTriggerRecordLayout().getHistLoadCd();
	}

	@Override
	public void setTrgHistLoadCd(char trgHistLoadCd) {
		this.triggerInTO.getTriggerRecordLayout().setHistLoadCd(trgHistLoadCd);
	}

	@Override
	public String getTrgInsId() {
		return triggerInTO.getTriggerRecordLayout().getInsId();
	}

	@Override
	public void setTrgInsId(String trgInsId) {
		this.triggerInTO.getTriggerRecordLayout().setInsId(trgInsId);
	}

	@Override
	public String getTrgIrcCd() {
		return triggerInTO.getTriggerRecordLayout().getIrcCd();
	}

	@Override
	public void setTrgIrcCd(String trgIrcCd) {
		this.triggerInTO.getTriggerRecordLayout().setIrcCd(trgIrcCd);
	}

	@Override
	public char getTrgItsClmTypCd() {
		return triggerInTO.getTriggerRecordLayout().getItsClmTypCd();
	}

	@Override
	public void setTrgItsClmTypCd(char trgItsClmTypCd) {
		this.triggerInTO.getTriggerRecordLayout().setItsClmTypCd(trgItsClmTypCd);
	}

	@Override
	public String getTrgKcapsTeamNm() {
		return triggerInTO.getTriggerRecordLayout().getKcapsTeamNm();
	}

	@Override
	public void setTrgKcapsTeamNm(String trgKcapsTeamNm) {
		this.triggerInTO.getTriggerRecordLayout().setKcapsTeamNm(trgKcapsTeamNm);
	}

	@Override
	public String getTrgKcapsUseId() {
		return triggerInTO.getTriggerRecordLayout().getKcapsUseId();
	}

	@Override
	public void setTrgKcapsUseId(String trgKcapsUseId) {
		this.triggerInTO.getTriggerRecordLayout().setKcapsUseId(trgKcapsUseId);
	}

	@Override
	public char getTrgLobCd() {
		return triggerInTO.getTriggerRecordLayout().getLobCd();
	}

	@Override
	public void setTrgLobCd(char trgLobCd) {
		this.triggerInTO.getTriggerRecordLayout().setLobCd(trgLobCd);
	}

	@Override
	public String getTrgMemberId() {
		return triggerInTO.getTriggerRecordLayout().getMemberId();
	}

	@Override
	public void setTrgMemberId(String trgMemberId) {
		this.triggerInTO.getTriggerRecordLayout().setMemberId(trgMemberId);
	}

	@Override
	public AfDecimal getTrgNcovAmt() {
		return triggerInTO.getTriggerRecordLayout().getNcovAmt();
	}

	@Override
	public void setTrgNcovAmt(AfDecimal trgNcovAmt) {
		this.triggerInTO.getTriggerRecordLayout().setNcovAmt(trgNcovAmt.copy());
	}

	@Override
	public char getTrgNpiCd() {
		return triggerInTO.getTriggerRecordLayout().getNpiCd();
	}

	@Override
	public void setTrgNpiCd(char trgNpiCd) {
		this.triggerInTO.getTriggerRecordLayout().setNpiCd(trgNpiCd);
	}

	@Override
	public String getTrgNtwrkCd() {
		return triggerInTO.getTriggerRecordLayout().getNtwrkCd();
	}

	@Override
	public void setTrgNtwrkCd(String trgNtwrkCd) {
		this.triggerInTO.getTriggerRecordLayout().setNtwrkCd(trgNtwrkCd);
	}

	@Override
	public char getTrgOiCd() {
		return triggerInTO.getTriggerRecordLayout().getOiCd();
	}

	@Override
	public void setTrgOiCd(char trgOiCd) {
		this.triggerInTO.getTriggerRecordLayout().setOiCd(trgOiCd);
	}

	@Override
	public String getTrgPatActId() {
		return triggerInTO.getTriggerRecordLayout().getPatActId();
	}

	@Override
	public void setTrgPatActId(String trgPatActId) {
		this.triggerInTO.getTriggerRecordLayout().setPatActId(trgPatActId);
	}

	@Override
	public AfDecimal getTrgPatResp() {
		return triggerInTO.getTriggerRecordLayout().getPatResp();
	}

	@Override
	public void setTrgPatResp(AfDecimal trgPatResp) {
		this.triggerInTO.getTriggerRecordLayout().setPatResp(trgPatResp.copy());
	}

	@Override
	public String getTrgPatientName() {
		return triggerInTO.getTriggerRecordLayout().getPatientName();
	}

	@Override
	public void setTrgPatientName(String trgPatientName) {
		this.triggerInTO.getTriggerRecordLayout().setPatientName(trgPatientName);
	}

	@Override
	public String getTrgPeProvId() {
		return triggerInTO.getTriggerRecordLayout().getPeProvId();
	}

	@Override
	public void setTrgPeProvId(String trgPeProvId) {
		this.triggerInTO.getTriggerRecordLayout().setPeProvId(trgPeProvId);
	}

	@Override
	public char getTrgPeProvLob() {
		return triggerInTO.getTriggerRecordLayout().getPeProvLob();
	}

	@Override
	public void setTrgPeProvLob(char trgPeProvLob) {
		this.triggerInTO.getTriggerRecordLayout().setPeProvLob(trgPeProvLob);
	}

	@Override
	public String getTrgPerformingProviderName() {
		return triggerInTO.getTriggerRecordLayout().getPerformingProviderName();
	}

	@Override
	public void setTrgPerformingProviderName(String trgPerformingProviderName) {
		this.triggerInTO.getTriggerRecordLayout().setPerformingProviderName(trgPerformingProviderName);
	}

	@Override
	public String getTrgPgmAreaCd() {
		return triggerInTO.getTriggerRecordLayout().getPgmAreaCd();
	}

	@Override
	public void setTrgPgmAreaCd(String trgPgmAreaCd) {
		this.triggerInTO.getTriggerRecordLayout().setPgmAreaCd(trgPgmAreaCd);
	}

	@Override
	public char getTrgPmtBkProdCd() {
		return triggerInTO.getTriggerRecordLayout().getPmtBkProdCd();
	}

	@Override
	public void setTrgPmtBkProdCd(char trgPmtBkProdCd) {
		this.triggerInTO.getTriggerRecordLayout().setPmtBkProdCd(trgPmtBkProdCd);
	}

	@Override
	public String getTrgPrimCnArngCd() {
		return triggerInTO.getTriggerRecordLayout().getPrimCnArngCd();
	}

	@Override
	public void setTrgPrimCnArngCd(String trgPrimCnArngCd) {
		this.triggerInTO.getTriggerRecordLayout().setPrimCnArngCd(trgPrimCnArngCd);
	}

	@Override
	public String getTrgPrmptPayDay() {
		return triggerInTO.getTriggerRecordLayout().getPrmptPayDay();
	}

	@Override
	public void setTrgPrmptPayDay(String trgPrmptPayDay) {
		this.triggerInTO.getTriggerRecordLayout().setPrmptPayDay(trgPrmptPayDay);
	}

	@Override
	public char getTrgPrmptPayOvrd() {
		return triggerInTO.getTriggerRecordLayout().getPrmptPayOvrd();
	}

	@Override
	public void setTrgPrmptPayOvrd(char trgPrmptPayOvrd) {
		this.triggerInTO.getTriggerRecordLayout().setPrmptPayOvrd(trgPrmptPayOvrd);
	}

	@Override
	public String getTrgProdInd() {
		return triggerInTO.getTriggerRecordLayout().getProdInd();
	}

	@Override
	public void setTrgProdInd(String trgProdInd) {
		this.triggerInTO.getTriggerRecordLayout().setProdInd(trgProdInd);
	}

	@Override
	public AfDecimal getTrgPwoAmt() {
		return triggerInTO.getTriggerRecordLayout().getPwoAmt();
	}

	@Override
	public void setTrgPwoAmt(AfDecimal trgPwoAmt) {
		this.triggerInTO.getTriggerRecordLayout().setPwoAmt(trgPwoAmt.copy());
	}

	@Override
	public String getTrgStatCatgCd() {
		return triggerInTO.getTriggerRecordLayout().getStatCatgCd();
	}

	@Override
	public void setTrgStatCatgCd(String trgStatCatgCd) {
		this.triggerInTO.getTriggerRecordLayout().setStatCatgCd(trgStatCatgCd);
	}

	@Override
	public String getTrgTypGrpCd() {
		return triggerInTO.getTriggerRecordLayout().getTypGrpCd();
	}

	@Override
	public void setTrgTypGrpCd(String trgTypGrpCd) {
		this.triggerInTO.getTriggerRecordLayout().setTypGrpCd(trgTypGrpCd);
	}

	@Override
	public short getWsTrgPmtId() {
		return ws.getWsNf0533WorkArea().getWsTrgPmtId();
	}

	@Override
	public void setWsTrgPmtId(short wsTrgPmtId) {
		this.ws.getWsNf0533WorkArea().setWsTrgPmtId(wsTrgPmtId);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 80;
		public static final int COVID_END_DTE = 80;
		public static final int EOB_REG_OUTPUT_RECORD = 1400;
		public static final int EOB_KSS_OUTPUT_RECORD = 1400;
		public static final int EXPENSE_REG_REC = 150;
		public static final int EXPENSE_KSS_REC = 150;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
