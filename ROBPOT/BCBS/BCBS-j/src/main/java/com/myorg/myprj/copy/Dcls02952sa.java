/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.commons.data.to.IS02952sa;

/**Original name: DCLS02952SA<br>
 * Variable: DCLS02952SA from copybook S02952SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02952sa implements IS02952sa {

	//==== PROPERTIES ====
	//Original name: ENTY-ID-CD
	private String entyIdCd = DefaultValues.stringVal(Len.ENTY_ID_CD);
	//Original name: ENTY-TYP-QLF-CD
	private char entyTypQlfCd = DefaultValues.CHAR_VAL;
	//Original name: LST-ORG-NM
	private String lstOrgNm = DefaultValues.stringVal(Len.LST_ORG_NM);
	//Original name: FRST-NM
	private String frstNm = DefaultValues.stringVal(Len.FRST_NM);
	//Original name: MID-NM
	private String midNm = DefaultValues.stringVal(Len.MID_NM);
	//Original name: SFX-NM
	private String sfxNm = DefaultValues.stringVal(Len.SFX_NM);
	//Original name: SSN-ID
	private String ssnId = DefaultValues.stringVal(Len.SSN_ID);

	//==== METHODS ====
	@Override
	public void setEntyIdCd(String entyIdCd) {
		this.entyIdCd = Functions.subString(entyIdCd, Len.ENTY_ID_CD);
	}

	@Override
	public String getEntyIdCd() {
		return this.entyIdCd;
	}

	@Override
	public void setEntyTypQlfCd(char entyTypQlfCd) {
		this.entyTypQlfCd = entyTypQlfCd;
	}

	@Override
	public char getEntyTypQlfCd() {
		return this.entyTypQlfCd;
	}

	@Override
	public void setLstOrgNm(String lstOrgNm) {
		this.lstOrgNm = Functions.subString(lstOrgNm, Len.LST_ORG_NM);
	}

	@Override
	public String getLstOrgNm() {
		return this.lstOrgNm;
	}

	@Override
	public void setFrstNm(String frstNm) {
		this.frstNm = Functions.subString(frstNm, Len.FRST_NM);
	}

	@Override
	public String getFrstNm() {
		return this.frstNm;
	}

	@Override
	public void setMidNm(String midNm) {
		this.midNm = Functions.subString(midNm, Len.MID_NM);
	}

	@Override
	public String getMidNm() {
		return this.midNm;
	}

	@Override
	public void setSfxNm(String sfxNm) {
		this.sfxNm = Functions.subString(sfxNm, Len.SFX_NM);
	}

	@Override
	public String getSfxNm() {
		return this.sfxNm;
	}

	@Override
	public void setSsnId(String ssnId) {
		this.ssnId = Functions.subString(ssnId, Len.SSN_ID);
	}

	@Override
	public String getSsnId() {
		return this.ssnId;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_CNTL_ID = 12;
		public static final int ENTY_ID_CD = 3;
		public static final int LST_ORG_NM = 35;
		public static final int FRST_NM = 25;
		public static final int MID_NM = 25;
		public static final int SFX_NM = 10;
		public static final int LN1_AD = 55;
		public static final int LN2_AD = 55;
		public static final int CITY_NM = 30;
		public static final int ST_CD = 2;
		public static final int ZIP5_CD = 5;
		public static final int ZIP4_CD = 4;
		public static final int SSN_ID = 9;
		public static final int BRTH_DT = 10;
		public static final int PLN_ST_CD = 2;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_TS = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
