/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS02718sa;

/**
 * Data Access Object(DAO) for table [S02718SA]
 * 
 */
public class S02718saDao extends BaseSqlDao<IS02718sa> {

	public S02718saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02718sa> getToClass() {
		return IS02718sa.class;
	}

	public IS02718sa selectRec(String clmCntlId, char clmCntlSfxId, short provSbmtLnNoId, IS02718sa iS02718sa) {
		return buildQuery("selectRec").bind("clmCntlId", clmCntlId).bind("clmCntlSfxId", String.valueOf(clmCntlSfxId))
				.bind("provSbmtLnNoId", provSbmtLnNoId).singleResult(iS02718sa);
	}
}
