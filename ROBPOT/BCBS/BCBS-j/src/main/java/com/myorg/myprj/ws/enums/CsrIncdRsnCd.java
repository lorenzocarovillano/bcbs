/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: CSR-INCD-RSN-CD<br>
 * Variable: CSR-INCD-RSN-CD from copybook NF07AREA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CsrIncdRsnCd {

	//==== PROPERTIES ====
	private String value = "";
	private static final String[] B_U_R_INCD_RSN = new String[] { "B", "R", "U", "M", "L", "I" };
	public static final String BUNDLE_INCIDENTAL_REASON = "B";
	public static final String UNBUNDL_INCIDENTAL_REASN = "U";
	public static final String REPLACE_INCIDENTAL_REASN = "R";
	public static final String PAYPRCT_INCIDENTAL_REASN = "M";
	public static final String UNITS_INCIDENTAL_REASN = "L";
	public static final String INCDNTL_INCIDENTAL_REASN = "I";

	//==== METHODS ====
	public void setRsnCd(String rsnCd) {
		this.value = Functions.subString(rsnCd, Len.RSN_CD);
	}

	public String getRsnCd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RSN_CD = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
