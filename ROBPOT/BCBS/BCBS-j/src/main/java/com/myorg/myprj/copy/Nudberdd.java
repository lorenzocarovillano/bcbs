/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: NUDBERDD<br>
 * Variable: NUDBERDD from copybook NUDBERDD<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Nudberdd {

	//==== PROPERTIES ====
	public static final int XXX_ERR_MSG_MAXOCCURS = 8;
	//Original name: DB2-STAT-ERR-MSG
	private Db2StatErrMsg db2StatErrMsg = new Db2StatErrMsg();
	//Original name: XXX-ERR-MSG
	private String[] xxxErrMsg = new String[XXX_ERR_MSG_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public Nudberdd() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int xxxErrMsgIdx = 1; xxxErrMsgIdx <= XXX_ERR_MSG_MAXOCCURS; xxxErrMsgIdx++) {
			setXxxErrMsg(xxxErrMsgIdx, DefaultValues.stringVal(Len.XXX_ERR_MSG));
		}
	}

	public void setXxxErrMsg(int xxxErrMsgIdx, String xxxErrMsg) {
		this.xxxErrMsg[xxxErrMsgIdx - 1] = Functions.subString(xxxErrMsg, Len.XXX_ERR_MSG);
	}

	public String getXxxErrMsg(int xxxErrMsgIdx) {
		return this.xxxErrMsg[xxxErrMsgIdx - 1];
	}

	public Db2StatErrMsg getDb2StatErrMsg() {
		return db2StatErrMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XXX_ERR_MSG = 78;
		public static final int DB2_SQLCODE = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
