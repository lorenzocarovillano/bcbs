/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS02719sa;

/**
 * Data Access Object(DAO) for table [S02719SA]
 * 
 */
public class S02719saDao extends BaseSqlDao<IS02719sa> {

	public S02719saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02719sa> getToClass() {
		return IS02719sa.class;
	}

	public IS02719sa selectRec(String clmCntlId, char clmCntlSfxId, short provSbmtLnNoId, IS02719sa iS02719sa) {
		return buildQuery("selectRec").bind("clmCntlId", clmCntlId).bind("clmCntlSfxId", String.valueOf(clmCntlSfxId))
				.bind("provSbmtLnNoId", provSbmtLnNoId).singleResult(iS02719sa);
	}
}
