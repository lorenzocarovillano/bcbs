/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: RESTART-INDICATOR<br>
 * Variable: RESTART-INDICATOR from program NF0533ML<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class RestartIndicator {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.RESTART_INDICATOR);
	public static final String NF05 = "NF05";
	public static final String NF07 = "NF07";

	//==== METHODS ====
	public void setRestartIndicator(String restartIndicator) {
		this.value = Functions.subString(restartIndicator, Len.RESTART_INDICATOR);
	}

	public String getRestartIndicator() {
		return this.value;
	}

	public String getRestartIndicatorFormatted() {
		return Functions.padBlanks(getRestartIndicator(), Len.RESTART_INDICATOR);
	}

	public boolean isNf05() {
		return value.equals(NF05);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RESTART_INDICATOR = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
