/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.FillerDateRecord;
import com.myorg.myprj.ws.WsBusinessDate;
import com.myorg.myprj.ws.enums.CurrentDateFlag;
import com.myorg.myprj.ws.enums.NextBusDateFlag;
import com.myorg.myprj.ws.enums.PreviousDateFlag;

/**Original name: DATE-RECORD<br>
 * Variable: DATE-RECORD from copybook NN02DATE<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class DateRecord {

	//==== PROPERTIES ====
	//Original name: FILLER-DATE-RECORD
	private FillerDateRecord fillerDateRecord = new FillerDateRecord();
	//Original name: FILLER-DATE-RECORD-1
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: CURRENT-JULIAN-DAY
	private String currentJulianDay = DefaultValues.stringVal(Len.CURRENT_JULIAN_DAY);
	//Original name: FILLER-DATE-RECORD-2
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: PREVIOUS-DATE
	private String previousDate = DefaultValues.stringVal(Len.PREVIOUS_DATE);
	//Original name: FILLER-DATE-RECORD-4
	private char flr3 = DefaultValues.CHAR_VAL;
	//Original name: PREVIOUS-JULIAN-DAY
	private String previousJulianDay = DefaultValues.stringVal(Len.PREVIOUS_JULIAN_DAY);
	//Original name: FILLER-DATE-RECORD-5
	private char flr4 = DefaultValues.CHAR_VAL;
	//Original name: ENVIRONMENT-ID
	private String environmentId = DefaultValues.stringVal(Len.ENVIRONMENT_ID);
	//Original name: FILLER-DATE-RECORD-6
	private char flr5 = DefaultValues.CHAR_VAL;
	//Original name: NEXT-BUSINESS-DATE
	private WsBusinessDate nextBusinessDate = new WsBusinessDate();
	//Original name: FILLER-DATE-RECORD-7
	private char flr6 = DefaultValues.CHAR_VAL;
	//Original name: NEXT-BUS-JULIAN-DAY
	private String nextBusJulianDay = DefaultValues.stringVal(Len.NEXT_BUS_JULIAN_DAY);
	//Original name: CURRENT-SEPARATOR
	private char currentSeparator = DefaultValues.CHAR_VAL;
	//Original name: CURRENT-DATE-DAY
	private String currentDateDay = DefaultValues.stringVal(Len.CURRENT_DATE_DAY);
	//Original name: CURRENT-DATE-DOW
	private String currentDateDow = DefaultValues.stringVal(Len.CURRENT_DATE_DOW);
	//Original name: CURRENT-DATE-FLAG
	private CurrentDateFlag currentDateFlag = new CurrentDateFlag();
	//Original name: PREVIOUS-SEPARATOR
	private char previousSeparator = DefaultValues.CHAR_VAL;
	//Original name: PREVIOUS-DATE-DAY
	private String previousDateDay = DefaultValues.stringVal(Len.PREVIOUS_DATE_DAY);
	//Original name: PREVIOUS-DATE-DOW
	private String previousDateDow = DefaultValues.stringVal(Len.PREVIOUS_DATE_DOW);
	//Original name: PREVIOUS-DATE-FLAG
	private PreviousDateFlag previousDateFlag = new PreviousDateFlag();
	//Original name: NEXT-SEPARATOR
	private char nextSeparator = DefaultValues.CHAR_VAL;
	//Original name: NEXT-BUS-DATE-DAY
	private String nextBusDateDay = DefaultValues.stringVal(Len.NEXT_BUS_DATE_DAY);
	//Original name: NEXT-BUS-DATE-DOW
	private String nextBusDateDow = DefaultValues.stringVal(Len.NEXT_BUS_DATE_DOW);
	//Original name: NEXT-BUS-DATE-FLAG
	private NextBusDateFlag nextBusDateFlag = new NextBusDateFlag();
	//Original name: FILLER-DATE-RECORD-8
	private String flr7 = DefaultValues.stringVal(Len.FLR7);

	//==== METHODS ====
	public void setDateRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		fillerDateRecord.setFillerDateRecordBytes(buffer, position);
		position += FillerDateRecord.Len.FILLER_DATE_RECORD;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		currentJulianDay = MarshalByte.readFixedString(buffer, position, Len.CURRENT_JULIAN_DAY);
		position += Len.CURRENT_JULIAN_DAY;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		previousDate = MarshalByte.readString(buffer, position, Len.PREVIOUS_DATE);
		position += Len.PREVIOUS_DATE;
		flr3 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		previousJulianDay = MarshalByte.readFixedString(buffer, position, Len.PREVIOUS_JULIAN_DAY);
		position += Len.PREVIOUS_JULIAN_DAY;
		flr4 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		setCurrentEnvironmentIdBytes(buffer, position);
		position += Len.CURRENT_ENVIRONMENT_ID;
		flr5 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		nextBusinessDate.setNextBusinessDateBytes(buffer, position);
		position += WsBusinessDate.Len.NEXT_BUSINESS_DATE;
		flr6 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		nextBusJulianDay = MarshalByte.readFixedString(buffer, position, Len.NEXT_BUS_JULIAN_DAY);
		position += Len.NEXT_BUS_JULIAN_DAY;
		currentSeparator = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		currentDateDay = MarshalByte.readString(buffer, position, Len.CURRENT_DATE_DAY);
		position += Len.CURRENT_DATE_DAY;
		currentDateDow = MarshalByte.readFixedString(buffer, position, Len.CURRENT_DATE_DOW);
		position += Len.CURRENT_DATE_DOW;
		currentDateFlag.setCurrentDateFlag(MarshalByte.readString(buffer, position, CurrentDateFlag.Len.CURRENT_DATE_FLAG));
		position += CurrentDateFlag.Len.CURRENT_DATE_FLAG;
		previousSeparator = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		previousDateDay = MarshalByte.readString(buffer, position, Len.PREVIOUS_DATE_DAY);
		position += Len.PREVIOUS_DATE_DAY;
		previousDateDow = MarshalByte.readFixedString(buffer, position, Len.PREVIOUS_DATE_DOW);
		position += Len.PREVIOUS_DATE_DOW;
		previousDateFlag.setPreviousDateFlag(MarshalByte.readString(buffer, position, PreviousDateFlag.Len.PREVIOUS_DATE_FLAG));
		position += PreviousDateFlag.Len.PREVIOUS_DATE_FLAG;
		nextSeparator = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		nextBusDateDay = MarshalByte.readString(buffer, position, Len.NEXT_BUS_DATE_DAY);
		position += Len.NEXT_BUS_DATE_DAY;
		nextBusDateDow = MarshalByte.readFixedString(buffer, position, Len.NEXT_BUS_DATE_DOW);
		position += Len.NEXT_BUS_DATE_DOW;
		nextBusDateFlag.setNextBusDateFlag(MarshalByte.readString(buffer, position, NextBusDateFlag.Len.NEXT_BUS_DATE_FLAG));
		position += NextBusDateFlag.Len.NEXT_BUS_DATE_FLAG;
		flr7 = MarshalByte.readString(buffer, position, Len.FLR7);
	}

	public byte[] getDateRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		fillerDateRecord.getFillerDateRecordBytes(buffer, position);
		position += FillerDateRecord.Len.FILLER_DATE_RECORD;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, currentJulianDay, Len.CURRENT_JULIAN_DAY);
		position += Len.CURRENT_JULIAN_DAY;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, previousDate, Len.PREVIOUS_DATE);
		position += Len.PREVIOUS_DATE;
		MarshalByte.writeChar(buffer, position, flr3);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, previousJulianDay, Len.PREVIOUS_JULIAN_DAY);
		position += Len.PREVIOUS_JULIAN_DAY;
		MarshalByte.writeChar(buffer, position, flr4);
		position += Types.CHAR_SIZE;
		getCurrentEnvironmentIdBytes(buffer, position);
		position += Len.CURRENT_ENVIRONMENT_ID;
		MarshalByte.writeChar(buffer, position, flr5);
		position += Types.CHAR_SIZE;
		nextBusinessDate.getNextBusinessDateBytes(buffer, position);
		position += WsBusinessDate.Len.NEXT_BUSINESS_DATE;
		MarshalByte.writeChar(buffer, position, flr6);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nextBusJulianDay, Len.NEXT_BUS_JULIAN_DAY);
		position += Len.NEXT_BUS_JULIAN_DAY;
		MarshalByte.writeChar(buffer, position, currentSeparator);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, currentDateDay, Len.CURRENT_DATE_DAY);
		position += Len.CURRENT_DATE_DAY;
		MarshalByte.writeString(buffer, position, currentDateDow, Len.CURRENT_DATE_DOW);
		position += Len.CURRENT_DATE_DOW;
		MarshalByte.writeString(buffer, position, currentDateFlag.getCurrentDateFlag(), CurrentDateFlag.Len.CURRENT_DATE_FLAG);
		position += CurrentDateFlag.Len.CURRENT_DATE_FLAG;
		MarshalByte.writeChar(buffer, position, previousSeparator);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, previousDateDay, Len.PREVIOUS_DATE_DAY);
		position += Len.PREVIOUS_DATE_DAY;
		MarshalByte.writeString(buffer, position, previousDateDow, Len.PREVIOUS_DATE_DOW);
		position += Len.PREVIOUS_DATE_DOW;
		MarshalByte.writeString(buffer, position, previousDateFlag.getPreviousDateFlag(), PreviousDateFlag.Len.PREVIOUS_DATE_FLAG);
		position += PreviousDateFlag.Len.PREVIOUS_DATE_FLAG;
		MarshalByte.writeChar(buffer, position, nextSeparator);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nextBusDateDay, Len.NEXT_BUS_DATE_DAY);
		position += Len.NEXT_BUS_DATE_DAY;
		MarshalByte.writeString(buffer, position, nextBusDateDow, Len.NEXT_BUS_DATE_DOW);
		position += Len.NEXT_BUS_DATE_DOW;
		MarshalByte.writeString(buffer, position, nextBusDateFlag.getNextBusDateFlag(), NextBusDateFlag.Len.NEXT_BUS_DATE_FLAG);
		position += NextBusDateFlag.Len.NEXT_BUS_DATE_FLAG;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		return buffer;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getCurrentJulianDayFormatted() {
		return this.currentJulianDay;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setPreviousDate(String previousDate) {
		this.previousDate = Functions.subString(previousDate, Len.PREVIOUS_DATE);
	}

	public String getPreviousDate() {
		return this.previousDate;
	}

	public void setFlr3(char flr3) {
		this.flr3 = flr3;
	}

	public char getFlr3() {
		return this.flr3;
	}

	public void setFlr4(char flr4) {
		this.flr4 = flr4;
	}

	public char getFlr4() {
		return this.flr4;
	}

	public void setCurrentEnvironmentIdBytes(byte[] buffer, int offset) {
		int position = offset;
		environmentId = MarshalByte.readString(buffer, position, Len.ENVIRONMENT_ID);
	}

	public byte[] getCurrentEnvironmentIdBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, environmentId, Len.ENVIRONMENT_ID);
		return buffer;
	}

	public void setEnvironmentId(String environmentId) {
		this.environmentId = Functions.subString(environmentId, Len.ENVIRONMENT_ID);
	}

	public String getEnvironmentId() {
		return this.environmentId;
	}

	public void setFlr5(char flr5) {
		this.flr5 = flr5;
	}

	public char getFlr5() {
		return this.flr5;
	}

	public void setFlr6(char flr6) {
		this.flr6 = flr6;
	}

	public char getFlr6() {
		return this.flr6;
	}

	public void setCurrentSeparator(char currentSeparator) {
		this.currentSeparator = currentSeparator;
	}

	public char getCurrentSeparator() {
		return this.currentSeparator;
	}

	public void setCurrentDateDay(String currentDateDay) {
		this.currentDateDay = Functions.subString(currentDateDay, Len.CURRENT_DATE_DAY);
	}

	public String getCurrentDateDay() {
		return this.currentDateDay;
	}

	public void setPreviousSeparator(char previousSeparator) {
		this.previousSeparator = previousSeparator;
	}

	public char getPreviousSeparator() {
		return this.previousSeparator;
	}

	public void setPreviousDateDay(String previousDateDay) {
		this.previousDateDay = Functions.subString(previousDateDay, Len.PREVIOUS_DATE_DAY);
	}

	public String getPreviousDateDay() {
		return this.previousDateDay;
	}

	public void setNextSeparator(char nextSeparator) {
		this.nextSeparator = nextSeparator;
	}

	public char getNextSeparator() {
		return this.nextSeparator;
	}

	public void setNextBusDateDay(String nextBusDateDay) {
		this.nextBusDateDay = Functions.subString(nextBusDateDay, Len.NEXT_BUS_DATE_DAY);
	}

	public String getNextBusDateDay() {
		return this.nextBusDateDay;
	}

	public void setFlr7(String flr7) {
		this.flr7 = Functions.subString(flr7, Len.FLR7);
	}

	public String getFlr7() {
		return this.flr7;
	}

	public FillerDateRecord getFillerDateRecord() {
		return fillerDateRecord;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 1;
		public static final int CURRENT_JULIAN_DAY = 3;
		public static final int PREVIOUS_DATE = 10;
		public static final int PREVIOUS_JULIAN_DAY = 3;
		public static final int ENVIRONMENT_ID = 4;
		public static final int CURRENT_ENVIRONMENT_ID = ENVIRONMENT_ID;
		public static final int NEXT_BUS_JULIAN_DAY = 3;
		public static final int CURRENT_SEPARATOR = 1;
		public static final int CURRENT_DATE_DAY = 3;
		public static final int CURRENT_DATE_DOW = 2;
		public static final int PREVIOUS_SEPARATOR = 1;
		public static final int PREVIOUS_DATE_DAY = 3;
		public static final int PREVIOUS_DATE_DOW = 2;
		public static final int NEXT_SEPARATOR = 1;
		public static final int NEXT_BUS_DATE_DAY = 3;
		public static final int NEXT_BUS_DATE_DOW = 2;
		public static final int FLR7 = 4;
		public static final int DATE_RECORD = CURRENT_JULIAN_DAY + PREVIOUS_DATE + PREVIOUS_JULIAN_DAY + CURRENT_ENVIRONMENT_ID
				+ WsBusinessDate.Len.NEXT_BUSINESS_DATE + NEXT_BUS_JULIAN_DAY + CURRENT_SEPARATOR + CURRENT_DATE_DAY + CURRENT_DATE_DOW
				+ CurrentDateFlag.Len.CURRENT_DATE_FLAG + PREVIOUS_SEPARATOR + PREVIOUS_DATE_DAY + PREVIOUS_DATE_DOW
				+ PreviousDateFlag.Len.PREVIOUS_DATE_FLAG + NEXT_SEPARATOR + NEXT_BUS_DATE_DAY + NEXT_BUS_DATE_DOW
				+ NextBusDateFlag.Len.NEXT_BUS_DATE_FLAG + 6 * FLR1 + FillerDateRecord.Len.FILLER_DATE_RECORD + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
