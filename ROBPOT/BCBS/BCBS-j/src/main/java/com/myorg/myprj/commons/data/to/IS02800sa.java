/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [S02800SA]
 * 
 */
public interface IS02800sa extends BaseSqlTo {

	/**
	 * Host Variable DB2-CLM-ADJ-GRP-CD
	 * 
	 */
	String getClmAdjGrpCd();

	void setClmAdjGrpCd(String clmAdjGrpCd);

	/**
	 * Host Variable DB2-CLM-ADJ-RSN-CD
	 * 
	 */
	String getClmAdjRsnCd();

	void setClmAdjRsnCd(String clmAdjRsnCd);

	/**
	 * Host Variable DB2-MNTRY-AM
	 * 
	 */
	AfDecimal getMntryAm();

	void setMntryAm(AfDecimal mntryAm);
};
