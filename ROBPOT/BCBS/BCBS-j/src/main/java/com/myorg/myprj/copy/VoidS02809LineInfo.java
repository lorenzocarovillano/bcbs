/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: VOID-S02809-LINE-INFO<br>
 * Variable: VOID-S02809-LINE-INFO from copybook NF05VOID<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class VoidS02809LineInfo {

	//==== PROPERTIES ====
	//Original name: VOID-CLI-ID
	private short cliId = DefaultValues.SHORT_VAL;
	//Original name: VOID-LI-CHG-AM
	private AfDecimal liChgAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: VOID-LI-PAT-RESP-AM
	private AfDecimal liPatRespAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: VOID-LI-PWO-AM
	private AfDecimal liPwoAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: VOID-LI-ALCT-OI-PD-AM
	private AfDecimal liAlctOiPdAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: VOID-LI-NCOV-AM
	private AfDecimal liNcovAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: VOID-LI-DNL-RMRK-CD
	private String liDnlRmrkCd = DefaultValues.stringVal(Len.LI_DNL_RMRK_CD);
	//Original name: VOID-LI-EXPLN-CD
	private char liExplnCd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setCliId(short cliId) {
		this.cliId = cliId;
	}

	public short getCliId() {
		return this.cliId;
	}

	public void setLiChgAm(AfDecimal liChgAm) {
		this.liChgAm.assign(liChgAm);
	}

	public AfDecimal getLiChgAm() {
		return this.liChgAm.copy();
	}

	public void setLiPatRespAm(AfDecimal liPatRespAm) {
		this.liPatRespAm.assign(liPatRespAm);
	}

	public AfDecimal getLiPatRespAm() {
		return this.liPatRespAm.copy();
	}

	public void setLiPwoAm(AfDecimal liPwoAm) {
		this.liPwoAm.assign(liPwoAm);
	}

	public AfDecimal getLiPwoAm() {
		return this.liPwoAm.copy();
	}

	public void setLiAlctOiPdAm(AfDecimal liAlctOiPdAm) {
		this.liAlctOiPdAm.assign(liAlctOiPdAm);
	}

	public AfDecimal getLiAlctOiPdAm() {
		return this.liAlctOiPdAm.copy();
	}

	public void setLiNcovAm(AfDecimal liNcovAm) {
		this.liNcovAm.assign(liNcovAm);
	}

	public AfDecimal getLiNcovAm() {
		return this.liNcovAm.copy();
	}

	public void setLiDnlRmrkCd(String liDnlRmrkCd) {
		this.liDnlRmrkCd = Functions.subString(liDnlRmrkCd, Len.LI_DNL_RMRK_CD);
	}

	public String getLiDnlRmrkCd() {
		return this.liDnlRmrkCd;
	}

	public void setLiExplnCd(char liExplnCd) {
		this.liExplnCd = liExplnCd;
	}

	public char getLiExplnCd() {
		return this.liExplnCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LI_DNL_RMRK_CD = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
