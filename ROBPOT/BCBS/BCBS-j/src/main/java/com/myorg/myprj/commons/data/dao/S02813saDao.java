/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS02813sa;

/**
 * Data Access Object(DAO) for table [S02813SA]
 * 
 */
public class S02813saDao extends BaseSqlDao<IS02813sa> {

	public S02813saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02813sa> getToClass() {
		return IS02813sa.class;
	}

	public DbAccessStatus updateRec(IS02813sa iS02813sa) {
		return buildQuery("updateRec").bind(iS02813sa).executeUpdate();
	}

	public DbAccessStatus updateRec1(IS02813sa iS02813sa) {
		return buildQuery("updateRec1").bind(iS02813sa).executeUpdate();
	}
}
