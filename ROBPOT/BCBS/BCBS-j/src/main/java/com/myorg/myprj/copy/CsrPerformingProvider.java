/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.enums.CsrLocalPerfProvLobCd;

/**Original name: CSR-PERFORMING-PROVIDER<br>
 * Variable: CSR-PERFORMING-PROVIDER from copybook NF07AREA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CsrPerformingProvider {

	//==== PROPERTIES ====
	//Original name: CSR-LOCAL-PERF-PROV-LOB-CD
	private CsrLocalPerfProvLobCd localPerfProvLobCd = new CsrLocalPerfProvLobCd();
	//Original name: CSR-HIST-PERF-PROV-NPI-ID
	private String histPerfProvNpiId = "";
	//Original name: CSR-LOCAL-PERF-PROV-ID
	private String localPerfProvId = "";

	//==== METHODS ====
	public byte[] getPerformingProviderBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, localPerfProvLobCd.getLocalPerfProvLobCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, histPerfProvNpiId, Len.HIST_PERF_PROV_NPI_ID);
		position += Len.HIST_PERF_PROV_NPI_ID;
		MarshalByte.writeString(buffer, position, localPerfProvId, Len.LOCAL_PERF_PROV_ID);
		return buffer;
	}

	public void initPerformingProviderSpaces() {
		localPerfProvLobCd.setLocalPerfProvLobCd(Types.SPACE_CHAR);
		histPerfProvNpiId = "";
		localPerfProvId = "";
	}

	public void setHistPerfProvNpiId(String histPerfProvNpiId) {
		this.histPerfProvNpiId = Functions.subString(histPerfProvNpiId, Len.HIST_PERF_PROV_NPI_ID);
	}

	public String getHistPerfProvNpiId() {
		return this.histPerfProvNpiId;
	}

	public void setLocalPerfProvId(String localPerfProvId) {
		this.localPerfProvId = Functions.subString(localPerfProvId, Len.LOCAL_PERF_PROV_ID);
	}

	public String getLocalPerfProvId() {
		return this.localPerfProvId;
	}

	public CsrLocalPerfProvLobCd getLocalPerfProvLobCd() {
		return localPerfProvLobCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HIST_PERF_PROV_NPI_ID = 10;
		public static final int LOCAL_PERF_PROV_ID = 10;
		public static final int PERFORMING_PROVIDER = CsrLocalPerfProvLobCd.Len.LOCAL_PERF_PROV_LOB_CD + HIST_PERF_PROV_NPI_ID + LOCAL_PERF_PROV_ID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
