/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SBR-ASSIGNED-INDC<br>
 * Variable: SBR-ASSIGNED-INDC from copybook NF05SUBR<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SbrAssignedIndc {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.ASSIGNED_INDC);
	public static final String CONTRACTING_PVDR = "Y1";
	public static final String SUBSCRIBER = "N1";
	public static final String THIRD_PARTY = "N2";

	//==== METHODS ====
	public void setAssignedIndc(String assignedIndc) {
		this.value = Functions.subString(assignedIndc, Len.ASSIGNED_INDC);
	}

	public String getAssignedIndc() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ASSIGNED_INDC = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
