/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.enums.CsrIncdRsnCd;

/**Original name: CSR-XREF-INCD<br>
 * Variable: CSR-XREF-INCD from copybook NF07AREA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CsrXrefIncd {

	//==== PROPERTIES ====
	//Original name: CSR-INCD-RSN-CD
	private CsrIncdRsnCd rsnCd = new CsrIncdRsnCd();
	//Original name: CSR-INCD-ADJD-PROC-CD
	private String adjdProcCd = "";
	//Original name: CSR-INCD-ADJD-MOD1-CD
	private String adjdMod1Cd = "";
	//Original name: CSR-INCD-ADJD-MOD2-CD
	private String adjdMod2Cd = "";
	//Original name: CSR-INCD-ADJD-MOD3-CD
	private String adjdMod3Cd = "";
	//Original name: CSR-INCD-ADJD-MOD4-CD
	private String adjdMod4Cd = "";

	//==== METHODS ====
	public byte[] getXrefIncdBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, rsnCd.getRsnCd(), CsrIncdRsnCd.Len.RSN_CD);
		position += CsrIncdRsnCd.Len.RSN_CD;
		MarshalByte.writeString(buffer, position, adjdProcCd, Len.ADJD_PROC_CD);
		position += Len.ADJD_PROC_CD;
		MarshalByte.writeString(buffer, position, adjdMod1Cd, Len.ADJD_MOD1_CD);
		position += Len.ADJD_MOD1_CD;
		MarshalByte.writeString(buffer, position, adjdMod2Cd, Len.ADJD_MOD2_CD);
		position += Len.ADJD_MOD2_CD;
		MarshalByte.writeString(buffer, position, adjdMod3Cd, Len.ADJD_MOD3_CD);
		position += Len.ADJD_MOD3_CD;
		MarshalByte.writeString(buffer, position, adjdMod4Cd, Len.ADJD_MOD4_CD);
		return buffer;
	}

	public void setAdjdProcCd(String adjdProcCd) {
		this.adjdProcCd = Functions.subString(adjdProcCd, Len.ADJD_PROC_CD);
	}

	public String getAdjdProcCd() {
		return this.adjdProcCd;
	}

	public void setAdjdMod1Cd(String adjdMod1Cd) {
		this.adjdMod1Cd = Functions.subString(adjdMod1Cd, Len.ADJD_MOD1_CD);
	}

	public String getAdjdMod1Cd() {
		return this.adjdMod1Cd;
	}

	public void setAdjdMod2Cd(String adjdMod2Cd) {
		this.adjdMod2Cd = Functions.subString(adjdMod2Cd, Len.ADJD_MOD2_CD);
	}

	public String getAdjdMod2Cd() {
		return this.adjdMod2Cd;
	}

	public void setAdjdMod3Cd(String adjdMod3Cd) {
		this.adjdMod3Cd = Functions.subString(adjdMod3Cd, Len.ADJD_MOD3_CD);
	}

	public String getAdjdMod3Cd() {
		return this.adjdMod3Cd;
	}

	public void setAdjdMod4Cd(String adjdMod4Cd) {
		this.adjdMod4Cd = Functions.subString(adjdMod4Cd, Len.ADJD_MOD4_CD);
	}

	public String getAdjdMod4Cd() {
		return this.adjdMod4Cd;
	}

	public CsrIncdRsnCd getRsnCd() {
		return rsnCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ADJD_PROC_CD = 5;
		public static final int ADJD_MOD1_CD = 2;
		public static final int ADJD_MOD2_CD = 2;
		public static final int ADJD_MOD3_CD = 2;
		public static final int ADJD_MOD4_CD = 2;
		public static final int XREF_INCD = CsrIncdRsnCd.Len.RSN_CD + ADJD_PROC_CD + ADJD_MOD1_CD + ADJD_MOD2_CD + ADJD_MOD3_CD + ADJD_MOD4_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
