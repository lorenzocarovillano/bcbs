/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import com.myorg.myprj.commons.data.to.IS02805saS02809saS02811saS02813saS02952saTrigger1;

/**
 * Data Access Object(DAO) for tables [S02805SA, S02809SA, S02811SA, S02813SA, S02952SA, TRIGGER1]
 * 
 */
public class S02805saS02809saS02811saS02813saS02952saTrigger1Dao extends BaseSqlDao<IS02805saS02809saS02811saS02813saS02952saTrigger1> {

	private Cursor eobCursor;
	private final IRowMapper<IS02805saS02809saS02811saS02813saS02952saTrigger1> fetchEobCursorRm = buildNamedRowMapper(
			IS02805saS02809saS02811saS02813saS02952saTrigger1.class, "db2PmtAdjTypCd", "db2VoidCd", "db2PmtPrimCnArngCd", "db2PmtRateCd",
			"db2ProviderName", "db2PatLstNm", "db2PatFrstNm", "db2PatMidNm", "db2ProdInd", "db2ClmCntlId", "db2ClmCntlSfxId", "db2ClmPmtId",
			"db2FnlBusDt", "db2StatCatgCd", "db2BiProvId", "db2BiProvLob", "db2BiProvSpec", "db2PeProvId", "db2PeProvLob", "db2PmtAlphPrfxCd",
			"db2PmtInsId", "db2PmtAltInsId", "db2PmtMemId", "db2PmtClmSystVerId", "db2PmtCorpRcvDt", "db2PmtAdjdDt", "db2PmtBillFrmDt",
			"db2PmtBillThrDt", "db2PmtAsgCd", "db2PmtClmPdAm", "db2PmtKcapsTeamNm", "db2PmtKcapsUseId", "db2PmtHistLoadCd", "db2PmtPmtBkProdCd",
			"db2PmtDrg", "db2PmtPatActId", "db2PmtPgmAreaCd", "db2PmtFinCd", "db2PmtAdjdProvStatCd", "db2PmtPrmptPayDay", "db2PmtPrmptPayOvrd",
			"db2PmtItsClmTypCd", "db2PmtGrpId", "db2PmtLobCd", "db2PmtTypGrpCd", "db2PmtNpiCd", "db2PmtBenPlnId", "db2PmtIrcCd", "db2PmtNtwrkCd",
			"db2PmtPrimCnArngCd", "db2PmtEnrClCd", "db2PmtOiCd", "wsDb2PatResp", "wsDb2PwoAmt", "wsDb2NcovAmt", "patientName", "performingName",
			"billingName", "db2CliId", "db2LiFrmServDt", "db2LiThrServDt", "db2LiBenTypCd", "db2LiExmnActnCd", "db2LiRmrkCd", "db2LiExplnCd",
			"db2LiRvwByCd", "db2LiAdjdOvrd1Cd", "db2LiAdjdOvrd2Cd", "db2LiAdjdOvrd3Cd", "db2LiAdjdOvrd4Cd", "db2LiAdjdOvrd5Cd", "db2LiAdjdOvrd6Cd",
			"db2LiAdjdOvrd7Cd", "db2LiAdjdOvrd8Cd", "db2LiAdjdOvrd9Cd", "db2LiAdjdOvrd10Cd", "db2LiPosCd", "db2LiTsCd", "db2LiChgAm",
			"db2LiPatRespAm", "db2LiPwoAm", "db2PmtAdjdProvStatCd", "db2LiAlwChgAm", "db2LiAlctOiCovAm", "db2LiAlctOiPdAm", "db2LiAlctOiSaveAm",
			"db2LiProcCd", "db2C1CalcTypCd", "db2C1CalcExplnCd", "db2C1PayCd", "db2C1CovLobCd", "db2C1AlwChgAm", "db2C1PdAm", "db2C1CorpCd",
			"db2C1ProdCd", "db2C1GlAcctId", "db2C1TypBenCd", "db2C1SpeciBenCd", "db2C1InsTcCd", "db2C2CalcTypCd", "db2C2CalcExplnCd", "db2C2PayCd",
			"db2C2CovLobCd", "db2C2AlwChgAm", "db2C2PdAm", "db2C2CorpCd", "db2C2ProdCd", "db2C2GlAcctId", "db2C2TypBenCd", "db2C2SpeciBenCd",
			"db2C2InsTcCd", "db2C3CalcTypCd", "db2C3CalcExplnCd", "db2C3PayCd", "db2C3CovLobCd", "db2C3AlwChgAm", "db2C3PdAm", "db2C3CorpCd",
			"db2C3ProdCd", "db2C3GlAcctId", "db2C3TypBenCd", "db2C3SpeciBenCd", "db2C3InsTcCd", "db2C4CalcTypCd", "db2C4CalcExplnCd", "db2C4PayCd",
			"db2C4CovLobCd", "db2C4AlwChgAm", "db2C4PdAm", "db2C4CorpCd", "db2C4ProdCd", "db2C4GlAcctId", "db2C4TypBenCd", "db2C4SpeciBenCd",
			"db2C4InsTcCd", "db2C5CalcTypCd", "db2C5CalcExplnCd", "db2C5PayCd", "db2C5CovLobCd", "db2C5AlwChgAm", "db2C5PdAm", "db2C5CorpCd",
			"db2C5ProdCd", "db2C5GlAcctId", "db2C5TypBenCd", "db2C5SpeciBenCd", "db2C5InsTcCd", "db2C6CalcTypCd", "db2C6CalcExplnCd", "db2C6PayCd",
			"db2C6CovLobCd", "db2C6AlwChgAm", "db2C6PdAm", "db2C6CorpCd", "db2C6ProdCd", "db2C6GlAcctId", "db2C6TypBenCd", "db2C6SpeciBenCd",
			"db2C6InsTcCd", "db2C7CalcTypCd", "db2C7CalcExplnCd", "db2C7PayCd", "db2C7CovLobCd", "db2C7AlwChgAm", "db2C7PdAm", "db2C7CorpCd",
			"db2C7ProdCd", "db2C7GlAcctId", "db2C7TypBenCd", "db2C7SpeciBenCd", "db2C7InsTcCd", "db2C8CalcTypCd", "db2C8CalcExplnCd", "db2C8PayCd",
			"db2C8CovLobCd", "db2C8AlwChgAm", "db2C8PdAm", "db2C8CorpCd", "db2C8ProdCd", "db2C8GlAcctId", "db2C8TypBenCd", "db2C8SpeciBenCd",
			"db2C8InsTcCd", "db2C9CalcTypCd", "db2C9CalcExplnCd", "db2C9PayCd", "db2C9CovLobCd", "db2C9AlwChgAm", "db2C9PdAm", "db2C9CorpCd",
			"db2C9ProdCd", "db2C9GlAcctId", "db2C9TypBenCd", "db2C9SpeciBenCd", "db2C9InsTcCd", "wsIndusCd", "db2PmtCkId", "db2PmtMdgpPlnCd",
			"db2AcrPrmtPaIntAm", "db2ClmInsLnCd", "db2ClmSystVerId", "db2PaIdToAcumId", "db2MemIdToAcumId", "db2VbrIn", "db2PmtBalBillAm",
			"db2SpecDrugCpnIn");

	public S02805saS02809saS02811saS02813saS02952saTrigger1Dao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02805saS02809saS02811saS02813saS02952saTrigger1> getToClass() {
		return IS02805saS02809saS02811saS02813saS02952saTrigger1.class;
	}

	public DbAccessStatus openEobCursor() {
		eobCursor = buildQuery("openEobCursor").withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public IS02805saS02809saS02811saS02813saS02952saTrigger1 fetchEobCursor(
			IS02805saS02809saS02811saS02813saS02952saTrigger1 iS02805saS02809saS02811saS02813saS02952saTrigger1) {
		return fetch(eobCursor, iS02805saS02809saS02811saS02813saS02952saTrigger1, fetchEobCursorRm);
	}

	public DbAccessStatus closeEobCursor() {
		return closeCursor(eobCursor);
	}
}
