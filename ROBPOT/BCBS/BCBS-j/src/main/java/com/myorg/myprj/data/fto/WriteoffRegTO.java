/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.data.fto;

import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.myorg.myprj.copy.DetailPartOfRecord;
import com.myorg.myprj.copy.HeaderPartOfRecord;
import com.myorg.myprj.ws.enums.RemLineOfBusiness;
import com.myorg.myprj.ws.enums.RemPrintNoPrint;
import com.myorg.myprj.ws.enums.RemTypeOfRemit;

/**Original name: WRITEOFF-REG<br>
 * File: WRITEOFF-REG from program NF0533ML<br>
 * Generated as a class for rule FTO.<br>*/
public class WriteoffRegTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: HEADER-PART-OF-RECORD
	private HeaderPartOfRecord headerPartOfRecord = new HeaderPartOfRecord();
	//Original name: DETAIL-PART-OF-RECORD
	private DetailPartOfRecord detailPartOfRecord = new DetailPartOfRecord();
	//Original name: REM-LINE-OF-BUSINESS
	private RemLineOfBusiness remLineOfBusiness = new RemLineOfBusiness();
	//Original name: REM-PRINT-NO-PRINT
	private RemPrintNoPrint remPrintNoPrint = new RemPrintNoPrint();
	//Original name: REM-TYPE-OF-REMIT
	private RemTypeOfRemit remTypeOfRemit = new RemTypeOfRemit();

	//==== METHODS ====
	/**Original name: REMITT-TAPE-RECORD<br>
	 * <pre> *---------------------
	 *  *             NF07RMIT                RECORD LENGTH:650
	 *  *   REMITTANCE ADVICE FILE LAYOUT
	 *  *   COPYBOOK   :  NFREMIT
	 *  *   LAST UPDATE:  07/97 EXPAND PROVIDER NBR, ADDRESS, ETC
	 *  *                 07/97 ADD TOOTH SVC CODE & ID FIELDS
	 *  *                 06/99 ADD A-INVALID-SW
	 *  *                 05/00 GMIS: RENAME REM-DETL-DENTAL-EXPL TO
	 *  *                       REM-DETL-REASON-EXPL.
	 *  *                 12/00 ADD: PROMPT PAY INTEREST FIELD
	 *  *                       AND RECEIPT DATE.
	 *  *                 02/02 CHANGES FOR THE HIPAA PROJECT
	 *  *                 09/04 CHANGES FOR AUTO DEDUCT MANAGEMENT RPT
	 *  *                 11/06 ADD FIELDS FOR NPI NUMBERS.
	 *  *   LENGTH     :  650
	 *  *---------------------------------</pre>*/
	public byte[] getRemittTapeRecordBytes() {
		byte[] buffer = new byte[Len.REMITT_TAPE_RECORD];
		return getRemittTapeRecordBytes(buffer, 1);
	}

	public void setRemittTapeRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		headerPartOfRecord.setHeaderPartOfRecordBytes(buffer, position);
		position += HeaderPartOfRecord.Len.HEADER_PART_OF_RECORD;
		detailPartOfRecord.setDetailPartOfRecordBytes(buffer, position);
		position += DetailPartOfRecord.Len.DETAIL_PART_OF_RECORD;
		setTrailerAreaBytes(buffer, position);
	}

	public byte[] getRemittTapeRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		headerPartOfRecord.getHeaderPartOfRecordBytes(buffer, position);
		position += HeaderPartOfRecord.Len.HEADER_PART_OF_RECORD;
		detailPartOfRecord.getDetailPartOfRecordBytes(buffer, position);
		position += DetailPartOfRecord.Len.DETAIL_PART_OF_RECORD;
		getTrailerAreaBytes(buffer, position);
		return buffer;
	}

	public void initRemittTapeRecordSpaces() {
		headerPartOfRecord.initHeaderPartOfRecordSpaces();
		detailPartOfRecord.initDetailPartOfRecordSpaces();
		initTrailerAreaSpaces();
	}

	public void setTrailerAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		remLineOfBusiness.setRemLineOfBusiness(MarshalByte.readString(buffer, position, RemLineOfBusiness.Len.REM_LINE_OF_BUSINESS));
		position += RemLineOfBusiness.Len.REM_LINE_OF_BUSINESS;
		remPrintNoPrint.setRemPrintNoPrint(MarshalByte.readString(buffer, position, RemPrintNoPrint.Len.REM_PRINT_NO_PRINT));
		position += RemPrintNoPrint.Len.REM_PRINT_NO_PRINT;
		remTypeOfRemit.setRemTypeOfRemit(MarshalByte.readString(buffer, position, RemTypeOfRemit.Len.REM_TYPE_OF_REMIT));
	}

	public byte[] getTrailerAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, remLineOfBusiness.getRemLineOfBusiness(), RemLineOfBusiness.Len.REM_LINE_OF_BUSINESS);
		position += RemLineOfBusiness.Len.REM_LINE_OF_BUSINESS;
		MarshalByte.writeString(buffer, position, remPrintNoPrint.getRemPrintNoPrint(), RemPrintNoPrint.Len.REM_PRINT_NO_PRINT);
		position += RemPrintNoPrint.Len.REM_PRINT_NO_PRINT;
		MarshalByte.writeString(buffer, position, remTypeOfRemit.getRemTypeOfRemit(), RemTypeOfRemit.Len.REM_TYPE_OF_REMIT);
		return buffer;
	}

	public void initTrailerAreaSpaces() {
		remLineOfBusiness.setRemLineOfBusiness("");
		remPrintNoPrint.setRemPrintNoPrint("");
		remTypeOfRemit.setRemTypeOfRemit("");
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getRemittTapeRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setRemittTapeRecordBytes(data, offset);
	}

	public DetailPartOfRecord getDetailPartOfRecord() {
		return detailPartOfRecord;
	}

	public HeaderPartOfRecord getHeaderPartOfRecord() {
		return headerPartOfRecord;
	}

	@Override
	public int getLength() {
		return Len.REMITT_TAPE_RECORD;
	}

	public RemLineOfBusiness getRemLineOfBusiness() {
		return remLineOfBusiness;
	}

	public RemPrintNoPrint getRemPrintNoPrint() {
		return remPrintNoPrint;
	}

	public RemTypeOfRemit getRemTypeOfRemit() {
		return remTypeOfRemit;
	}

	@Override
	public IBuffer copy() {
		WriteoffRegTO copyTO = new WriteoffRegTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TRAILER_AREA = RemLineOfBusiness.Len.REM_LINE_OF_BUSINESS + RemPrintNoPrint.Len.REM_PRINT_NO_PRINT
				+ RemTypeOfRemit.Len.REM_TYPE_OF_REMIT;
		public static final int REMITT_TAPE_RECORD = HeaderPartOfRecord.Len.HEADER_PART_OF_RECORD + DetailPartOfRecord.Len.DETAIL_PART_OF_RECORD
				+ TRAILER_AREA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
