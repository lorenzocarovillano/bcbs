/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: NF05-ITS-CLAIM-TYPE<br>
 * Variable: NF05-ITS-CLAIM-TYPE from copybook NF05EOB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Nf05ItsClaimType {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char HOST = 'S';
	public static final char HOME = 'M';
	public static final char BCBSK = 'K';

	//==== METHODS ====
	public void setNf05ItsClaimType(char nf05ItsClaimType) {
		this.value = nf05ItsClaimType;
	}

	public char getNf05ItsClaimType() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NF05_ITS_CLAIM_TYPE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
