/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import com.myorg.myprj.commons.data.to.IS02811saS02813sa;

/**
 * Data Access Object(DAO) for tables [S02811SA, S02813SA]
 * 
 */
public class S02811saS02813saDao extends BaseSqlDao<IS02811saS02813sa> {

	private Cursor calcCursor;
	private final IRowMapper<IS02811saS02813sa> fetchCalcCursorRm = buildNamedRowMapper(IS02811saS02813sa.class, "calcTypCd", "covLobCd",
			"calcExplnCd", "payCd", "alwChgAm", "pdAm", "grpId", "corpCd", "prodCd", "typBenCd", "speciBenCd", "glAcctId", "insTcCd");

	public S02811saS02813saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02811saS02813sa> getToClass() {
		return IS02811saS02813sa.class;
	}

	public DbAccessStatus openCalcCursor(String cntlId, char sfxId, short pmtNum, short liIdPacked) {
		calcCursor = buildQuery("openCalcCursor").bind("cntlId", cntlId).bind("sfxId", String.valueOf(sfxId)).bind("pmtNum", pmtNum)
				.bind("liIdPacked", liIdPacked).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public IS02811saS02813sa fetchCalcCursor(IS02811saS02813sa iS02811saS02813sa) {
		return fetch(calcCursor, iS02811saS02813sa, fetchCalcCursorRm);
	}

	public DbAccessStatus closeCalcCursor() {
		return closeCursor(calcCursor);
	}
}
