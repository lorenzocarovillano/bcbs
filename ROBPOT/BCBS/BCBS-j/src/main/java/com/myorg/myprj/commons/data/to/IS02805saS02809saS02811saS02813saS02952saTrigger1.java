/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [S02805SA, S02809SA, S02811SA, S02813SA, S02952SA, TRIGGER1]
 * 
 */
public interface IS02805saS02809saS02811saS02813saS02952saTrigger1 extends BaseSqlTo {

	/**
	 * Host Variable DB2-PMT-ADJ-TYP-CD
	 * 
	 */
	char getDb2PmtAdjTypCd();

	void setDb2PmtAdjTypCd(char db2PmtAdjTypCd);

	/**
	 * Host Variable DB2-VOID-CD
	 * 
	 */
	char getDb2VoidCd();

	void setDb2VoidCd(char db2VoidCd);

	/**
	 * Host Variable DB2-PMT-PRIM-CN-ARNG-CD
	 * 
	 */
	String getDb2PmtPrimCnArngCd();

	void setDb2PmtPrimCnArngCd(String db2PmtPrimCnArngCd);

	/**
	 * Host Variable DB2-PMT-RATE-CD
	 * 
	 */
	String getDb2PmtRateCd();

	void setDb2PmtRateCd(String db2PmtRateCd);

	/**
	 * Host Variable DB2-PROVIDER-NAME
	 * 
	 */
	String getDb2ProviderName();

	void setDb2ProviderName(String db2ProviderName);

	/**
	 * Host Variable DB2-PAT-LST-NM
	 * 
	 */
	String getDb2PatLstNm();

	void setDb2PatLstNm(String db2PatLstNm);

	/**
	 * Host Variable DB2-PAT-FRST-NM
	 * 
	 */
	String getDb2PatFrstNm();

	void setDb2PatFrstNm(String db2PatFrstNm);

	/**
	 * Host Variable DB2-PAT-MID-NM
	 * 
	 */
	String getDb2PatMidNm();

	void setDb2PatMidNm(String db2PatMidNm);

	/**
	 * Host Variable DB2-PROD-IND
	 * 
	 */
	String getDb2ProdInd();

	void setDb2ProdInd(String db2ProdInd);

	/**
	 * Host Variable DB2-CLM-CNTL-ID
	 * 
	 */
	String getDb2ClmCntlId();

	void setDb2ClmCntlId(String db2ClmCntlId);

	/**
	 * Host Variable DB2-CLM-CNTL-SFX-ID
	 * 
	 */
	char getDb2ClmCntlSfxId();

	void setDb2ClmCntlSfxId(char db2ClmCntlSfxId);

	/**
	 * Host Variable DB2-CLM-PMT-ID
	 * 
	 */
	short getDb2ClmPmtId();

	void setDb2ClmPmtId(short db2ClmPmtId);

	/**
	 * Host Variable DB2-FNL-BUS-DT
	 * 
	 */
	String getDb2FnlBusDt();

	void setDb2FnlBusDt(String db2FnlBusDt);

	/**
	 * Host Variable DB2-STAT-CATG-CD
	 * 
	 */
	String getDb2StatCatgCd();

	void setDb2StatCatgCd(String db2StatCatgCd);

	/**
	 * Host Variable DB2-BI-PROV-ID
	 * 
	 */
	String getDb2BiProvId();

	void setDb2BiProvId(String db2BiProvId);

	/**
	 * Host Variable DB2-BI-PROV-LOB
	 * 
	 */
	char getDb2BiProvLob();

	void setDb2BiProvLob(char db2BiProvLob);

	/**
	 * Host Variable DB2-BI-PROV-SPEC
	 * 
	 */
	String getDb2BiProvSpec();

	void setDb2BiProvSpec(String db2BiProvSpec);

	/**
	 * Host Variable DB2-PE-PROV-ID
	 * 
	 */
	String getDb2PeProvId();

	void setDb2PeProvId(String db2PeProvId);

	/**
	 * Host Variable DB2-PE-PROV-LOB
	 * 
	 */
	char getDb2PeProvLob();

	void setDb2PeProvLob(char db2PeProvLob);

	/**
	 * Host Variable DB2-PMT-ALPH-PRFX-CD
	 * 
	 */
	String getDb2PmtAlphPrfxCd();

	void setDb2PmtAlphPrfxCd(String db2PmtAlphPrfxCd);

	/**
	 * Host Variable DB2-PMT-INS-ID
	 * 
	 */
	String getDb2PmtInsId();

	void setDb2PmtInsId(String db2PmtInsId);

	/**
	 * Host Variable DB2-PMT-ALT-INS-ID
	 * 
	 */
	String getDb2PmtAltInsId();

	void setDb2PmtAltInsId(String db2PmtAltInsId);

	/**
	 * Host Variable DB2-PMT-MEM-ID
	 * 
	 */
	String getDb2PmtMemId();

	void setDb2PmtMemId(String db2PmtMemId);

	/**
	 * Host Variable DB2-PMT-CLM-SYST-VER-ID
	 * 
	 */
	String getDb2PmtClmSystVerId();

	void setDb2PmtClmSystVerId(String db2PmtClmSystVerId);

	/**
	 * Host Variable DB2-PMT-CORP-RCV-DT
	 * 
	 */
	String getDb2PmtCorpRcvDt();

	void setDb2PmtCorpRcvDt(String db2PmtCorpRcvDt);

	/**
	 * Host Variable DB2-PMT-ADJD-DT
	 * 
	 */
	String getDb2PmtAdjdDt();

	void setDb2PmtAdjdDt(String db2PmtAdjdDt);

	/**
	 * Host Variable DB2-PMT-BILL-FRM-DT
	 * 
	 */
	String getDb2PmtBillFrmDt();

	void setDb2PmtBillFrmDt(String db2PmtBillFrmDt);

	/**
	 * Host Variable DB2-PMT-BILL-THR-DT
	 * 
	 */
	String getDb2PmtBillThrDt();

	void setDb2PmtBillThrDt(String db2PmtBillThrDt);

	/**
	 * Host Variable DB2-PMT-ASG-CD
	 * 
	 */
	String getDb2PmtAsgCd();

	void setDb2PmtAsgCd(String db2PmtAsgCd);

	/**
	 * Host Variable DB2-PMT-CLM-PD-AM
	 * 
	 */
	AfDecimal getDb2PmtClmPdAm();

	void setDb2PmtClmPdAm(AfDecimal db2PmtClmPdAm);

	/**
	 * Host Variable DB2-PMT-KCAPS-TEAM-NM
	 * 
	 */
	String getDb2PmtKcapsTeamNm();

	void setDb2PmtKcapsTeamNm(String db2PmtKcapsTeamNm);

	/**
	 * Host Variable DB2-PMT-KCAPS-USE-ID
	 * 
	 */
	String getDb2PmtKcapsUseId();

	void setDb2PmtKcapsUseId(String db2PmtKcapsUseId);

	/**
	 * Host Variable DB2-PMT-HIST-LOAD-CD
	 * 
	 */
	char getDb2PmtHistLoadCd();

	void setDb2PmtHistLoadCd(char db2PmtHistLoadCd);

	/**
	 * Host Variable DB2-PMT-PMT-BK-PROD-CD
	 * 
	 */
	char getDb2PmtPmtBkProdCd();

	void setDb2PmtPmtBkProdCd(char db2PmtPmtBkProdCd);

	/**
	 * Host Variable DB2-PMT-DRG
	 * 
	 */
	String getDb2PmtDrg();

	void setDb2PmtDrg(String db2PmtDrg);

	/**
	 * Host Variable DB2-PMT-PAT-ACT-ID
	 * 
	 */
	String getDb2PmtPatActId();

	void setDb2PmtPatActId(String db2PmtPatActId);

	/**
	 * Host Variable DB2-PMT-PGM-AREA-CD
	 * 
	 */
	String getDb2PmtPgmAreaCd();

	void setDb2PmtPgmAreaCd(String db2PmtPgmAreaCd);

	/**
	 * Host Variable DB2-PMT-FIN-CD
	 * 
	 */
	String getDb2PmtFinCd();

	void setDb2PmtFinCd(String db2PmtFinCd);

	/**
	 * Host Variable DB2-PMT-ADJD-PROV-STAT-CD
	 * 
	 */
	char getDb2PmtAdjdProvStatCd();

	void setDb2PmtAdjdProvStatCd(char db2PmtAdjdProvStatCd);

	/**
	 * Host Variable DB2-PMT-PRMPT-PAY-DAY
	 * 
	 */
	String getDb2PmtPrmptPayDay();

	void setDb2PmtPrmptPayDay(String db2PmtPrmptPayDay);

	/**
	 * Host Variable DB2-PMT-PRMPT-PAY-OVRD
	 * 
	 */
	char getDb2PmtPrmptPayOvrd();

	void setDb2PmtPrmptPayOvrd(char db2PmtPrmptPayOvrd);

	/**
	 * Host Variable DB2-PMT-ITS-CLM-TYP-CD
	 * 
	 */
	char getDb2PmtItsClmTypCd();

	void setDb2PmtItsClmTypCd(char db2PmtItsClmTypCd);

	/**
	 * Host Variable DB2-PMT-GRP-ID
	 * 
	 */
	String getDb2PmtGrpId();

	void setDb2PmtGrpId(String db2PmtGrpId);

	/**
	 * Host Variable DB2-PMT-LOB-CD
	 * 
	 */
	char getDb2PmtLobCd();

	void setDb2PmtLobCd(char db2PmtLobCd);

	/**
	 * Host Variable DB2-PMT-TYP-GRP-CD
	 * 
	 */
	String getDb2PmtTypGrpCd();

	void setDb2PmtTypGrpCd(String db2PmtTypGrpCd);

	/**
	 * Host Variable DB2-PMT-NPI-CD
	 * 
	 */
	char getDb2PmtNpiCd();

	void setDb2PmtNpiCd(char db2PmtNpiCd);

	/**
	 * Host Variable DB2-PMT-BEN-PLN-ID
	 * 
	 */
	String getDb2PmtBenPlnId();

	void setDb2PmtBenPlnId(String db2PmtBenPlnId);

	/**
	 * Host Variable DB2-PMT-IRC-CD
	 * 
	 */
	String getDb2PmtIrcCd();

	void setDb2PmtIrcCd(String db2PmtIrcCd);

	/**
	 * Host Variable DB2-PMT-NTWRK-CD
	 * 
	 */
	String getDb2PmtNtwrkCd();

	void setDb2PmtNtwrkCd(String db2PmtNtwrkCd);

	/**
	 * Host Variable DB2-PMT-ENR-CL-CD
	 * 
	 */
	String getDb2PmtEnrClCd();

	void setDb2PmtEnrClCd(String db2PmtEnrClCd);

	/**
	 * Host Variable DB2-PMT-OI-CD
	 * 
	 */
	char getDb2PmtOiCd();

	void setDb2PmtOiCd(char db2PmtOiCd);

	/**
	 * Host Variable WS-DB2-PAT-RESP
	 * 
	 */
	AfDecimal getWsDb2PatResp();

	void setWsDb2PatResp(AfDecimal wsDb2PatResp);

	/**
	 * Host Variable WS-DB2-PWO-AMT
	 * 
	 */
	AfDecimal getWsDb2PwoAmt();

	void setWsDb2PwoAmt(AfDecimal wsDb2PwoAmt);

	/**
	 * Host Variable WS-DB2-NCOV-AMT
	 * 
	 */
	AfDecimal getWsDb2NcovAmt();

	void setWsDb2NcovAmt(AfDecimal wsDb2NcovAmt);

	/**
	 * Host Variable PATIENT-NAME
	 * 
	 */
	String getPatientName();

	void setPatientName(String patientName);

	/**
	 * Host Variable PERFORMING-NAME
	 * 
	 */
	String getPerformingName();

	void setPerformingName(String performingName);

	/**
	 * Host Variable BILLING-NAME
	 * 
	 */
	String getBillingName();

	void setBillingName(String billingName);

	/**
	 * Host Variable DB2-CLI-ID
	 * 
	 */
	short getDb2CliId();

	void setDb2CliId(short db2CliId);

	/**
	 * Host Variable DB2-LI-FRM-SERV-DT
	 * 
	 */
	String getDb2LiFrmServDt();

	void setDb2LiFrmServDt(String db2LiFrmServDt);

	/**
	 * Host Variable DB2-LI-THR-SERV-DT
	 * 
	 */
	String getDb2LiThrServDt();

	void setDb2LiThrServDt(String db2LiThrServDt);

	/**
	 * Host Variable DB2-LI-BEN-TYP-CD
	 * 
	 */
	String getDb2LiBenTypCd();

	void setDb2LiBenTypCd(String db2LiBenTypCd);

	/**
	 * Host Variable DB2-LI-EXMN-ACTN-CD
	 * 
	 */
	char getDb2LiExmnActnCd();

	void setDb2LiExmnActnCd(char db2LiExmnActnCd);

	/**
	 * Host Variable DB2-LI-RMRK-CD
	 * 
	 */
	String getDb2LiRmrkCd();

	void setDb2LiRmrkCd(String db2LiRmrkCd);

	/**
	 * Host Variable DB2-LI-EXPLN-CD
	 * 
	 */
	char getDb2LiExplnCd();

	void setDb2LiExplnCd(char db2LiExplnCd);

	/**
	 * Host Variable DB2-LI-RVW-BY-CD
	 * 
	 */
	char getDb2LiRvwByCd();

	void setDb2LiRvwByCd(char db2LiRvwByCd);

	/**
	 * Host Variable DB2-LI-ADJD-OVRD1-CD
	 * 
	 */
	String getDb2LiAdjdOvrd1Cd();

	void setDb2LiAdjdOvrd1Cd(String db2LiAdjdOvrd1Cd);

	/**
	 * Host Variable DB2-LI-ADJD-OVRD2-CD
	 * 
	 */
	String getDb2LiAdjdOvrd2Cd();

	void setDb2LiAdjdOvrd2Cd(String db2LiAdjdOvrd2Cd);

	/**
	 * Host Variable DB2-LI-ADJD-OVRD3-CD
	 * 
	 */
	String getDb2LiAdjdOvrd3Cd();

	void setDb2LiAdjdOvrd3Cd(String db2LiAdjdOvrd3Cd);

	/**
	 * Host Variable DB2-LI-ADJD-OVRD4-CD
	 * 
	 */
	String getDb2LiAdjdOvrd4Cd();

	void setDb2LiAdjdOvrd4Cd(String db2LiAdjdOvrd4Cd);

	/**
	 * Host Variable DB2-LI-ADJD-OVRD5-CD
	 * 
	 */
	String getDb2LiAdjdOvrd5Cd();

	void setDb2LiAdjdOvrd5Cd(String db2LiAdjdOvrd5Cd);

	/**
	 * Host Variable DB2-LI-ADJD-OVRD6-CD
	 * 
	 */
	String getDb2LiAdjdOvrd6Cd();

	void setDb2LiAdjdOvrd6Cd(String db2LiAdjdOvrd6Cd);

	/**
	 * Host Variable DB2-LI-ADJD-OVRD7-CD
	 * 
	 */
	String getDb2LiAdjdOvrd7Cd();

	void setDb2LiAdjdOvrd7Cd(String db2LiAdjdOvrd7Cd);

	/**
	 * Host Variable DB2-LI-ADJD-OVRD8-CD
	 * 
	 */
	String getDb2LiAdjdOvrd8Cd();

	void setDb2LiAdjdOvrd8Cd(String db2LiAdjdOvrd8Cd);

	/**
	 * Host Variable DB2-LI-ADJD-OVRD9-CD
	 * 
	 */
	String getDb2LiAdjdOvrd9Cd();

	void setDb2LiAdjdOvrd9Cd(String db2LiAdjdOvrd9Cd);

	/**
	 * Host Variable DB2-LI-ADJD-OVRD10-CD
	 * 
	 */
	String getDb2LiAdjdOvrd10Cd();

	void setDb2LiAdjdOvrd10Cd(String db2LiAdjdOvrd10Cd);

	/**
	 * Host Variable DB2-LI-POS-CD
	 * 
	 */
	String getDb2LiPosCd();

	void setDb2LiPosCd(String db2LiPosCd);

	/**
	 * Host Variable DB2-LI-TS-CD
	 * 
	 */
	String getDb2LiTsCd();

	void setDb2LiTsCd(String db2LiTsCd);

	/**
	 * Host Variable DB2-LI-CHG-AM
	 * 
	 */
	AfDecimal getDb2LiChgAm();

	void setDb2LiChgAm(AfDecimal db2LiChgAm);

	/**
	 * Host Variable DB2-LI-PAT-RESP-AM
	 * 
	 */
	AfDecimal getDb2LiPatRespAm();

	void setDb2LiPatRespAm(AfDecimal db2LiPatRespAm);

	/**
	 * Host Variable DB2-LI-PWO-AM
	 * 
	 */
	AfDecimal getDb2LiPwoAm();

	void setDb2LiPwoAm(AfDecimal db2LiPwoAm);

	/**
	 * Host Variable DB2-LI-ALW-CHG-AM
	 * 
	 */
	AfDecimal getDb2LiAlwChgAm();

	void setDb2LiAlwChgAm(AfDecimal db2LiAlwChgAm);

	/**
	 * Host Variable DB2-LI-ALCT-OI-COV-AM
	 * 
	 */
	AfDecimal getDb2LiAlctOiCovAm();

	void setDb2LiAlctOiCovAm(AfDecimal db2LiAlctOiCovAm);

	/**
	 * Host Variable DB2-LI-ALCT-OI-PD-AM
	 * 
	 */
	AfDecimal getDb2LiAlctOiPdAm();

	void setDb2LiAlctOiPdAm(AfDecimal db2LiAlctOiPdAm);

	/**
	 * Host Variable DB2-LI-ALCT-OI-SAVE-AM
	 * 
	 */
	AfDecimal getDb2LiAlctOiSaveAm();

	void setDb2LiAlctOiSaveAm(AfDecimal db2LiAlctOiSaveAm);

	/**
	 * Host Variable DB2-LI-PROC-CD
	 * 
	 */
	String getDb2LiProcCd();

	void setDb2LiProcCd(String db2LiProcCd);

	/**
	 * Host Variable DB2-C1-CALC-TYP-CD
	 * 
	 */
	char getDb2C1CalcTypCd();

	void setDb2C1CalcTypCd(char db2C1CalcTypCd);

	/**
	 * Host Variable DB2-C1-CALC-EXPLN-CD
	 * 
	 */
	char getDb2C1CalcExplnCd();

	void setDb2C1CalcExplnCd(char db2C1CalcExplnCd);

	/**
	 * Host Variable DB2-C1-PAY-CD
	 * 
	 */
	char getDb2C1PayCd();

	void setDb2C1PayCd(char db2C1PayCd);

	/**
	 * Host Variable DB2-C1-COV-LOB-CD
	 * 
	 */
	char getDb2C1CovLobCd();

	void setDb2C1CovLobCd(char db2C1CovLobCd);

	/**
	 * Host Variable DB2-C1-ALW-CHG-AM
	 * 
	 */
	AfDecimal getDb2C1AlwChgAm();

	void setDb2C1AlwChgAm(AfDecimal db2C1AlwChgAm);

	/**
	 * Host Variable DB2-C1-PD-AM
	 * 
	 */
	AfDecimal getDb2C1PdAm();

	void setDb2C1PdAm(AfDecimal db2C1PdAm);

	/**
	 * Host Variable DB2-C1-CORP-CD
	 * 
	 */
	char getDb2C1CorpCd();

	void setDb2C1CorpCd(char db2C1CorpCd);

	/**
	 * Host Variable DB2-C1-PROD-CD
	 * 
	 */
	String getDb2C1ProdCd();

	void setDb2C1ProdCd(String db2C1ProdCd);

	/**
	 * Host Variable DB2-C1-GL-ACCT-ID
	 * 
	 */
	String getDb2C1GlAcctId();

	void setDb2C1GlAcctId(String db2C1GlAcctId);

	/**
	 * Host Variable DB2-C1-TYP-BEN-CD
	 * 
	 */
	String getDb2C1TypBenCd();

	void setDb2C1TypBenCd(String db2C1TypBenCd);

	/**
	 * Host Variable DB2-C1-SPECI-BEN-CD
	 * 
	 */
	String getDb2C1SpeciBenCd();

	void setDb2C1SpeciBenCd(String db2C1SpeciBenCd);

	/**
	 * Host Variable DB2-C1-INS-TC-CD
	 * 
	 */
	String getDb2C1InsTcCd();

	void setDb2C1InsTcCd(String db2C1InsTcCd);

	/**
	 * Host Variable DB2-C2-CALC-TYP-CD
	 * 
	 */
	char getDb2C2CalcTypCd();

	void setDb2C2CalcTypCd(char db2C2CalcTypCd);

	/**
	 * Host Variable DB2-C2-CALC-EXPLN-CD
	 * 
	 */
	char getDb2C2CalcExplnCd();

	void setDb2C2CalcExplnCd(char db2C2CalcExplnCd);

	/**
	 * Host Variable DB2-C2-PAY-CD
	 * 
	 */
	char getDb2C2PayCd();

	void setDb2C2PayCd(char db2C2PayCd);

	/**
	 * Host Variable DB2-C2-COV-LOB-CD
	 * 
	 */
	char getDb2C2CovLobCd();

	void setDb2C2CovLobCd(char db2C2CovLobCd);

	/**
	 * Host Variable DB2-C2-ALW-CHG-AM
	 * 
	 */
	AfDecimal getDb2C2AlwChgAm();

	void setDb2C2AlwChgAm(AfDecimal db2C2AlwChgAm);

	/**
	 * Host Variable DB2-C2-PD-AM
	 * 
	 */
	AfDecimal getDb2C2PdAm();

	void setDb2C2PdAm(AfDecimal db2C2PdAm);

	/**
	 * Host Variable DB2-C2-CORP-CD
	 * 
	 */
	char getDb2C2CorpCd();

	void setDb2C2CorpCd(char db2C2CorpCd);

	/**
	 * Host Variable DB2-C2-PROD-CD
	 * 
	 */
	String getDb2C2ProdCd();

	void setDb2C2ProdCd(String db2C2ProdCd);

	/**
	 * Host Variable DB2-C2-GL-ACCT-ID
	 * 
	 */
	String getDb2C2GlAcctId();

	void setDb2C2GlAcctId(String db2C2GlAcctId);

	/**
	 * Host Variable DB2-C2-TYP-BEN-CD
	 * 
	 */
	String getDb2C2TypBenCd();

	void setDb2C2TypBenCd(String db2C2TypBenCd);

	/**
	 * Host Variable DB2-C2-SPECI-BEN-CD
	 * 
	 */
	String getDb2C2SpeciBenCd();

	void setDb2C2SpeciBenCd(String db2C2SpeciBenCd);

	/**
	 * Host Variable DB2-C2-INS-TC-CD
	 * 
	 */
	String getDb2C2InsTcCd();

	void setDb2C2InsTcCd(String db2C2InsTcCd);

	/**
	 * Host Variable DB2-C3-CALC-TYP-CD
	 * 
	 */
	char getDb2C3CalcTypCd();

	void setDb2C3CalcTypCd(char db2C3CalcTypCd);

	/**
	 * Host Variable DB2-C3-CALC-EXPLN-CD
	 * 
	 */
	char getDb2C3CalcExplnCd();

	void setDb2C3CalcExplnCd(char db2C3CalcExplnCd);

	/**
	 * Host Variable DB2-C3-PAY-CD
	 * 
	 */
	char getDb2C3PayCd();

	void setDb2C3PayCd(char db2C3PayCd);

	/**
	 * Host Variable DB2-C3-COV-LOB-CD
	 * 
	 */
	char getDb2C3CovLobCd();

	void setDb2C3CovLobCd(char db2C3CovLobCd);

	/**
	 * Host Variable DB2-C3-ALW-CHG-AM
	 * 
	 */
	AfDecimal getDb2C3AlwChgAm();

	void setDb2C3AlwChgAm(AfDecimal db2C3AlwChgAm);

	/**
	 * Host Variable DB2-C3-PD-AM
	 * 
	 */
	AfDecimal getDb2C3PdAm();

	void setDb2C3PdAm(AfDecimal db2C3PdAm);

	/**
	 * Host Variable DB2-C3-CORP-CD
	 * 
	 */
	char getDb2C3CorpCd();

	void setDb2C3CorpCd(char db2C3CorpCd);

	/**
	 * Host Variable DB2-C3-PROD-CD
	 * 
	 */
	String getDb2C3ProdCd();

	void setDb2C3ProdCd(String db2C3ProdCd);

	/**
	 * Host Variable DB2-C3-GL-ACCT-ID
	 * 
	 */
	String getDb2C3GlAcctId();

	void setDb2C3GlAcctId(String db2C3GlAcctId);

	/**
	 * Host Variable DB2-C3-TYP-BEN-CD
	 * 
	 */
	String getDb2C3TypBenCd();

	void setDb2C3TypBenCd(String db2C3TypBenCd);

	/**
	 * Host Variable DB2-C3-SPECI-BEN-CD
	 * 
	 */
	String getDb2C3SpeciBenCd();

	void setDb2C3SpeciBenCd(String db2C3SpeciBenCd);

	/**
	 * Host Variable DB2-C3-INS-TC-CD
	 * 
	 */
	String getDb2C3InsTcCd();

	void setDb2C3InsTcCd(String db2C3InsTcCd);

	/**
	 * Host Variable DB2-C4-CALC-TYP-CD
	 * 
	 */
	char getDb2C4CalcTypCd();

	void setDb2C4CalcTypCd(char db2C4CalcTypCd);

	/**
	 * Host Variable DB2-C4-CALC-EXPLN-CD
	 * 
	 */
	char getDb2C4CalcExplnCd();

	void setDb2C4CalcExplnCd(char db2C4CalcExplnCd);

	/**
	 * Host Variable DB2-C4-PAY-CD
	 * 
	 */
	char getDb2C4PayCd();

	void setDb2C4PayCd(char db2C4PayCd);

	/**
	 * Host Variable DB2-C4-COV-LOB-CD
	 * 
	 */
	char getDb2C4CovLobCd();

	void setDb2C4CovLobCd(char db2C4CovLobCd);

	/**
	 * Host Variable DB2-C4-ALW-CHG-AM
	 * 
	 */
	AfDecimal getDb2C4AlwChgAm();

	void setDb2C4AlwChgAm(AfDecimal db2C4AlwChgAm);

	/**
	 * Host Variable DB2-C4-PD-AM
	 * 
	 */
	AfDecimal getDb2C4PdAm();

	void setDb2C4PdAm(AfDecimal db2C4PdAm);

	/**
	 * Host Variable DB2-C4-CORP-CD
	 * 
	 */
	char getDb2C4CorpCd();

	void setDb2C4CorpCd(char db2C4CorpCd);

	/**
	 * Host Variable DB2-C4-PROD-CD
	 * 
	 */
	String getDb2C4ProdCd();

	void setDb2C4ProdCd(String db2C4ProdCd);

	/**
	 * Host Variable DB2-C4-GL-ACCT-ID
	 * 
	 */
	String getDb2C4GlAcctId();

	void setDb2C4GlAcctId(String db2C4GlAcctId);

	/**
	 * Host Variable DB2-C4-TYP-BEN-CD
	 * 
	 */
	String getDb2C4TypBenCd();

	void setDb2C4TypBenCd(String db2C4TypBenCd);

	/**
	 * Host Variable DB2-C4-SPECI-BEN-CD
	 * 
	 */
	String getDb2C4SpeciBenCd();

	void setDb2C4SpeciBenCd(String db2C4SpeciBenCd);

	/**
	 * Host Variable DB2-C4-INS-TC-CD
	 * 
	 */
	String getDb2C4InsTcCd();

	void setDb2C4InsTcCd(String db2C4InsTcCd);

	/**
	 * Host Variable DB2-C5-CALC-TYP-CD
	 * 
	 */
	char getDb2C5CalcTypCd();

	void setDb2C5CalcTypCd(char db2C5CalcTypCd);

	/**
	 * Host Variable DB2-C5-CALC-EXPLN-CD
	 * 
	 */
	char getDb2C5CalcExplnCd();

	void setDb2C5CalcExplnCd(char db2C5CalcExplnCd);

	/**
	 * Host Variable DB2-C5-PAY-CD
	 * 
	 */
	char getDb2C5PayCd();

	void setDb2C5PayCd(char db2C5PayCd);

	/**
	 * Host Variable DB2-C5-COV-LOB-CD
	 * 
	 */
	char getDb2C5CovLobCd();

	void setDb2C5CovLobCd(char db2C5CovLobCd);

	/**
	 * Host Variable DB2-C5-ALW-CHG-AM
	 * 
	 */
	AfDecimal getDb2C5AlwChgAm();

	void setDb2C5AlwChgAm(AfDecimal db2C5AlwChgAm);

	/**
	 * Host Variable DB2-C5-PD-AM
	 * 
	 */
	AfDecimal getDb2C5PdAm();

	void setDb2C5PdAm(AfDecimal db2C5PdAm);

	/**
	 * Host Variable DB2-C5-CORP-CD
	 * 
	 */
	char getDb2C5CorpCd();

	void setDb2C5CorpCd(char db2C5CorpCd);

	/**
	 * Host Variable DB2-C5-PROD-CD
	 * 
	 */
	String getDb2C5ProdCd();

	void setDb2C5ProdCd(String db2C5ProdCd);

	/**
	 * Host Variable DB2-C5-GL-ACCT-ID
	 * 
	 */
	String getDb2C5GlAcctId();

	void setDb2C5GlAcctId(String db2C5GlAcctId);

	/**
	 * Host Variable DB2-C5-TYP-BEN-CD
	 * 
	 */
	String getDb2C5TypBenCd();

	void setDb2C5TypBenCd(String db2C5TypBenCd);

	/**
	 * Host Variable DB2-C5-SPECI-BEN-CD
	 * 
	 */
	String getDb2C5SpeciBenCd();

	void setDb2C5SpeciBenCd(String db2C5SpeciBenCd);

	/**
	 * Host Variable DB2-C5-INS-TC-CD
	 * 
	 */
	String getDb2C5InsTcCd();

	void setDb2C5InsTcCd(String db2C5InsTcCd);

	/**
	 * Host Variable DB2-C6-CALC-TYP-CD
	 * 
	 */
	char getDb2C6CalcTypCd();

	void setDb2C6CalcTypCd(char db2C6CalcTypCd);

	/**
	 * Host Variable DB2-C6-CALC-EXPLN-CD
	 * 
	 */
	char getDb2C6CalcExplnCd();

	void setDb2C6CalcExplnCd(char db2C6CalcExplnCd);

	/**
	 * Host Variable DB2-C6-PAY-CD
	 * 
	 */
	char getDb2C6PayCd();

	void setDb2C6PayCd(char db2C6PayCd);

	/**
	 * Host Variable DB2-C6-COV-LOB-CD
	 * 
	 */
	char getDb2C6CovLobCd();

	void setDb2C6CovLobCd(char db2C6CovLobCd);

	/**
	 * Host Variable DB2-C6-ALW-CHG-AM
	 * 
	 */
	AfDecimal getDb2C6AlwChgAm();

	void setDb2C6AlwChgAm(AfDecimal db2C6AlwChgAm);

	/**
	 * Host Variable DB2-C6-PD-AM
	 * 
	 */
	AfDecimal getDb2C6PdAm();

	void setDb2C6PdAm(AfDecimal db2C6PdAm);

	/**
	 * Host Variable DB2-C6-CORP-CD
	 * 
	 */
	char getDb2C6CorpCd();

	void setDb2C6CorpCd(char db2C6CorpCd);

	/**
	 * Host Variable DB2-C6-PROD-CD
	 * 
	 */
	String getDb2C6ProdCd();

	void setDb2C6ProdCd(String db2C6ProdCd);

	/**
	 * Host Variable DB2-C6-GL-ACCT-ID
	 * 
	 */
	String getDb2C6GlAcctId();

	void setDb2C6GlAcctId(String db2C6GlAcctId);

	/**
	 * Host Variable DB2-C6-TYP-BEN-CD
	 * 
	 */
	String getDb2C6TypBenCd();

	void setDb2C6TypBenCd(String db2C6TypBenCd);

	/**
	 * Host Variable DB2-C6-SPECI-BEN-CD
	 * 
	 */
	String getDb2C6SpeciBenCd();

	void setDb2C6SpeciBenCd(String db2C6SpeciBenCd);

	/**
	 * Host Variable DB2-C6-INS-TC-CD
	 * 
	 */
	String getDb2C6InsTcCd();

	void setDb2C6InsTcCd(String db2C6InsTcCd);

	/**
	 * Host Variable DB2-C7-CALC-TYP-CD
	 * 
	 */
	char getDb2C7CalcTypCd();

	void setDb2C7CalcTypCd(char db2C7CalcTypCd);

	/**
	 * Host Variable DB2-C7-CALC-EXPLN-CD
	 * 
	 */
	char getDb2C7CalcExplnCd();

	void setDb2C7CalcExplnCd(char db2C7CalcExplnCd);

	/**
	 * Host Variable DB2-C7-PAY-CD
	 * 
	 */
	char getDb2C7PayCd();

	void setDb2C7PayCd(char db2C7PayCd);

	/**
	 * Host Variable DB2-C7-COV-LOB-CD
	 * 
	 */
	char getDb2C7CovLobCd();

	void setDb2C7CovLobCd(char db2C7CovLobCd);

	/**
	 * Host Variable DB2-C7-ALW-CHG-AM
	 * 
	 */
	AfDecimal getDb2C7AlwChgAm();

	void setDb2C7AlwChgAm(AfDecimal db2C7AlwChgAm);

	/**
	 * Host Variable DB2-C7-PD-AM
	 * 
	 */
	AfDecimal getDb2C7PdAm();

	void setDb2C7PdAm(AfDecimal db2C7PdAm);

	/**
	 * Host Variable DB2-C7-CORP-CD
	 * 
	 */
	char getDb2C7CorpCd();

	void setDb2C7CorpCd(char db2C7CorpCd);

	/**
	 * Host Variable DB2-C7-PROD-CD
	 * 
	 */
	String getDb2C7ProdCd();

	void setDb2C7ProdCd(String db2C7ProdCd);

	/**
	 * Host Variable DB2-C7-GL-ACCT-ID
	 * 
	 */
	String getDb2C7GlAcctId();

	void setDb2C7GlAcctId(String db2C7GlAcctId);

	/**
	 * Host Variable DB2-C7-TYP-BEN-CD
	 * 
	 */
	String getDb2C7TypBenCd();

	void setDb2C7TypBenCd(String db2C7TypBenCd);

	/**
	 * Host Variable DB2-C7-SPECI-BEN-CD
	 * 
	 */
	String getDb2C7SpeciBenCd();

	void setDb2C7SpeciBenCd(String db2C7SpeciBenCd);

	/**
	 * Host Variable DB2-C7-INS-TC-CD
	 * 
	 */
	String getDb2C7InsTcCd();

	void setDb2C7InsTcCd(String db2C7InsTcCd);

	/**
	 * Host Variable DB2-C8-CALC-TYP-CD
	 * 
	 */
	char getDb2C8CalcTypCd();

	void setDb2C8CalcTypCd(char db2C8CalcTypCd);

	/**
	 * Host Variable DB2-C8-CALC-EXPLN-CD
	 * 
	 */
	char getDb2C8CalcExplnCd();

	void setDb2C8CalcExplnCd(char db2C8CalcExplnCd);

	/**
	 * Host Variable DB2-C8-PAY-CD
	 * 
	 */
	char getDb2C8PayCd();

	void setDb2C8PayCd(char db2C8PayCd);

	/**
	 * Host Variable DB2-C8-COV-LOB-CD
	 * 
	 */
	char getDb2C8CovLobCd();

	void setDb2C8CovLobCd(char db2C8CovLobCd);

	/**
	 * Host Variable DB2-C8-ALW-CHG-AM
	 * 
	 */
	AfDecimal getDb2C8AlwChgAm();

	void setDb2C8AlwChgAm(AfDecimal db2C8AlwChgAm);

	/**
	 * Host Variable DB2-C8-PD-AM
	 * 
	 */
	AfDecimal getDb2C8PdAm();

	void setDb2C8PdAm(AfDecimal db2C8PdAm);

	/**
	 * Host Variable DB2-C8-CORP-CD
	 * 
	 */
	char getDb2C8CorpCd();

	void setDb2C8CorpCd(char db2C8CorpCd);

	/**
	 * Host Variable DB2-C8-PROD-CD
	 * 
	 */
	String getDb2C8ProdCd();

	void setDb2C8ProdCd(String db2C8ProdCd);

	/**
	 * Host Variable DB2-C8-GL-ACCT-ID
	 * 
	 */
	String getDb2C8GlAcctId();

	void setDb2C8GlAcctId(String db2C8GlAcctId);

	/**
	 * Host Variable DB2-C8-TYP-BEN-CD
	 * 
	 */
	String getDb2C8TypBenCd();

	void setDb2C8TypBenCd(String db2C8TypBenCd);

	/**
	 * Host Variable DB2-C8-SPECI-BEN-CD
	 * 
	 */
	String getDb2C8SpeciBenCd();

	void setDb2C8SpeciBenCd(String db2C8SpeciBenCd);

	/**
	 * Host Variable DB2-C8-INS-TC-CD
	 * 
	 */
	String getDb2C8InsTcCd();

	void setDb2C8InsTcCd(String db2C8InsTcCd);

	/**
	 * Host Variable DB2-C9-CALC-TYP-CD
	 * 
	 */
	char getDb2C9CalcTypCd();

	void setDb2C9CalcTypCd(char db2C9CalcTypCd);

	/**
	 * Host Variable DB2-C9-CALC-EXPLN-CD
	 * 
	 */
	char getDb2C9CalcExplnCd();

	void setDb2C9CalcExplnCd(char db2C9CalcExplnCd);

	/**
	 * Host Variable DB2-C9-PAY-CD
	 * 
	 */
	char getDb2C9PayCd();

	void setDb2C9PayCd(char db2C9PayCd);

	/**
	 * Host Variable DB2-C9-COV-LOB-CD
	 * 
	 */
	char getDb2C9CovLobCd();

	void setDb2C9CovLobCd(char db2C9CovLobCd);

	/**
	 * Host Variable DB2-C9-ALW-CHG-AM
	 * 
	 */
	AfDecimal getDb2C9AlwChgAm();

	void setDb2C9AlwChgAm(AfDecimal db2C9AlwChgAm);

	/**
	 * Host Variable DB2-C9-PD-AM
	 * 
	 */
	AfDecimal getDb2C9PdAm();

	void setDb2C9PdAm(AfDecimal db2C9PdAm);

	/**
	 * Host Variable DB2-C9-CORP-CD
	 * 
	 */
	char getDb2C9CorpCd();

	void setDb2C9CorpCd(char db2C9CorpCd);

	/**
	 * Host Variable DB2-C9-PROD-CD
	 * 
	 */
	String getDb2C9ProdCd();

	void setDb2C9ProdCd(String db2C9ProdCd);

	/**
	 * Host Variable DB2-C9-GL-ACCT-ID
	 * 
	 */
	String getDb2C9GlAcctId();

	void setDb2C9GlAcctId(String db2C9GlAcctId);

	/**
	 * Host Variable DB2-C9-TYP-BEN-CD
	 * 
	 */
	String getDb2C9TypBenCd();

	void setDb2C9TypBenCd(String db2C9TypBenCd);

	/**
	 * Host Variable DB2-C9-SPECI-BEN-CD
	 * 
	 */
	String getDb2C9SpeciBenCd();

	void setDb2C9SpeciBenCd(String db2C9SpeciBenCd);

	/**
	 * Host Variable DB2-C9-INS-TC-CD
	 * 
	 */
	String getDb2C9InsTcCd();

	void setDb2C9InsTcCd(String db2C9InsTcCd);

	/**
	 * Host Variable WS-INDUS-CD
	 * 
	 */
	String getWsIndusCd();

	void setWsIndusCd(String wsIndusCd);

	/**
	 * Host Variable DB2-PMT-CK-ID
	 * 
	 */
	String getDb2PmtCkId();

	void setDb2PmtCkId(String db2PmtCkId);

	/**
	 * Host Variable DB2-PMT-MDGP-PLN-CD
	 * 
	 */
	char getDb2PmtMdgpPlnCd();

	void setDb2PmtMdgpPlnCd(char db2PmtMdgpPlnCd);

	/**
	 * Host Variable DB2-ACR-PRMT-PA-INT-AM
	 * 
	 */
	AfDecimal getDb2AcrPrmtPaIntAm();

	void setDb2AcrPrmtPaIntAm(AfDecimal db2AcrPrmtPaIntAm);

	/**
	 * Host Variable DB2-CLM-INS-LN-CD
	 * 
	 */
	String getDb2ClmInsLnCd();

	void setDb2ClmInsLnCd(String db2ClmInsLnCd);

	/**
	 * Host Variable DB2-CLM-SYST-VER-ID
	 * 
	 */
	String getDb2ClmSystVerId();

	void setDb2ClmSystVerId(String db2ClmSystVerId);

	/**
	 * Host Variable DB2-PA-ID-TO-ACUM-ID
	 * 
	 */
	String getDb2PaIdToAcumId();

	void setDb2PaIdToAcumId(String db2PaIdToAcumId);

	/**
	 * Host Variable DB2-MEM-ID-TO-ACUM-ID
	 * 
	 */
	String getDb2MemIdToAcumId();

	void setDb2MemIdToAcumId(String db2MemIdToAcumId);

	/**
	 * Host Variable DB2-VBR-IN
	 * 
	 */
	char getDb2VbrIn();

	void setDb2VbrIn(char db2VbrIn);

	/**
	 * Host Variable DB2-PMT-BAL-BILL-AM
	 * 
	 */
	AfDecimal getDb2PmtBalBillAm();

	void setDb2PmtBalBillAm(AfDecimal db2PmtBalBillAm);

	/**
	 * Host Variable DB2-SPEC-DRUG-CPN-IN
	 * 
	 */
	char getDb2SpecDrugCpnIn();

	void setDb2SpecDrugCpnIn(char db2SpecDrugCpnIn);
};
