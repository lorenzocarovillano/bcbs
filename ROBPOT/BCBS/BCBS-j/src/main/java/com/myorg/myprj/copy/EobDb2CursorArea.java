/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EOB-DB2-CURSOR-AREA<br>
 * Variable: EOB-DB2-CURSOR-AREA from copybook NF05CURS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class EobDb2CursorArea {

	//==== PROPERTIES ====
	//Original name: DB2-S02801-INFO
	private Db2S02801Info s02801Info = new Db2S02801Info();
	//Original name: DB2-S02815-PROVIDER-INFO
	private Db2S02815ProviderInfo s02815ProviderInfo = new Db2S02815ProviderInfo();
	//Original name: DB2-S02813-PMT-OR-CLAIM-INFO
	private Db2S02813PmtOrClaimInfo s02813PmtOrClaimInfo = new Db2S02813PmtOrClaimInfo();
	//Original name: DB2-S02809-LINE-INFO
	private Db2S02809LineInfo s02809LineInfo = new Db2S02809LineInfo();
	//Original name: DB2-S02811-CALC1-INFO
	private Db2S02811Calc1Info s02811Calc1Info = new Db2S02811Calc1Info();
	//Original name: DB2-S02811-CALC2-INFO
	private Db2S02811Calc1Info s02811Calc2Info = new Db2S02811Calc1Info();
	//Original name: DB2-S02811-CALC3-INFO
	private Db2S02811Calc1Info s02811Calc3Info = new Db2S02811Calc1Info();
	//Original name: DB2-S02811-CALC4-INFO
	private Db2S02811Calc1Info s02811Calc4Info = new Db2S02811Calc1Info();
	//Original name: DB2-S02811-CALC5-INFO
	private Db2S02811Calc1Info s02811Calc5Info = new Db2S02811Calc1Info();
	//Original name: DB2-S02811-CALC6-INFO
	private Db2S02811Calc1Info s02811Calc6Info = new Db2S02811Calc1Info();
	//Original name: DB2-S02811-CALC7-INFO
	private Db2S02811Calc1Info s02811Calc7Info = new Db2S02811Calc1Info();
	//Original name: DB2-S02811-CALC8-INFO
	private Db2S02811Calc1Info s02811Calc8Info = new Db2S02811Calc1Info();
	//Original name: DB2-S02811-CALC9-INFO
	private Db2S02811Calc1Info s02811Calc9Info = new Db2S02811Calc1Info();
	//Original name: DB2-CAS-CALC1-INFO
	private Db2CasCalc1Info casCalc1Info = new Db2CasCalc1Info();
	//Original name: DB2-CAS-TABLE-INFO
	private Db2CasTableInfo casTableInfo = new Db2CasTableInfo();
	//Original name: DB2-HC-INDUS-CD
	private String hcIndusCd = DefaultValues.stringVal(Len.HC_INDUS_CD);
	//Original name: DB2-S02952-NAME-DEMO-INFO
	private Db2S02952NameDemoInfo s02952NameDemoInfo = new Db2S02952NameDemoInfo();
	//Original name: DB2-PERF-PVDR-NAME
	private String perfPvdrName = DefaultValues.stringVal(Len.PERF_PVDR_NAME);

	//==== METHODS ====
	public void setEobDb2CursorAreaFormatted(String data) {
		byte[] buffer = new byte[Len.EOB_DB2_CURSOR_AREA];
		MarshalByte.writeString(buffer, 1, data, Len.EOB_DB2_CURSOR_AREA);
		setEobDb2CursorAreaBytes(buffer, 1);
	}

	public String getEobDb2CursorAreaFormatted() {
		return MarshalByteExt.bufferToStr(getEobDb2CursorAreaBytes());
	}

	public byte[] getEobDb2CursorAreaBytes() {
		byte[] buffer = new byte[Len.EOB_DB2_CURSOR_AREA];
		return getEobDb2CursorAreaBytes(buffer, 1);
	}

	public void setEobDb2CursorAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		s02801Info.setS02801InfoBytes(buffer, position);
		position += Db2S02801Info.Len.S02801_INFO;
		s02815ProviderInfo.setS02815ProviderInfoBytes(buffer, position);
		position += Db2S02815ProviderInfo.Len.S02815_PROVIDER_INFO;
		s02813PmtOrClaimInfo.setS02813PmtOrClaimInfoBytes(buffer, position);
		position += Db2S02813PmtOrClaimInfo.Len.S02813_PMT_OR_CLAIM_INFO;
		s02809LineInfo.setS02809LineInfoBytes(buffer, position);
		position += Db2S02809LineInfo.Len.S02809_LINE_INFO;
		s02811Calc1Info.setS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc2Info.setS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc3Info.setS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc4Info.setS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc5Info.setS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc6Info.setS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc7Info.setS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc8Info.setS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc9Info.setS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		casCalc1Info.setCasCalc1InfoBytes(buffer, position);
		position += Db2CasCalc1Info.Len.CAS_CALC1_INFO;
		casTableInfo.setCasTableInfoBytes(buffer, position);
		position += Db2CasTableInfo.Len.CAS_TABLE_INFO;
		setS02805ClthCareInfoBytes(buffer, position);
		position += Len.S02805_CLTH_CARE_INFO;
		s02952NameDemoInfo.setS02952NameDemoInfoBytes(buffer, position);
		position += Db2S02952NameDemoInfo.Len.S02952_NAME_DEMO_INFO;
		setS02952PerfNameDemoInfoBytes(buffer, position);
	}

	public byte[] getEobDb2CursorAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		s02801Info.getS02801InfoBytes(buffer, position);
		position += Db2S02801Info.Len.S02801_INFO;
		s02815ProviderInfo.getS02815ProviderInfoBytes(buffer, position);
		position += Db2S02815ProviderInfo.Len.S02815_PROVIDER_INFO;
		s02813PmtOrClaimInfo.getS02813PmtOrClaimInfoBytes(buffer, position);
		position += Db2S02813PmtOrClaimInfo.Len.S02813_PMT_OR_CLAIM_INFO;
		s02809LineInfo.getS02809LineInfoBytes(buffer, position);
		position += Db2S02809LineInfo.Len.S02809_LINE_INFO;
		s02811Calc1Info.getS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc2Info.getS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc3Info.getS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc4Info.getS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc5Info.getS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc6Info.getS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc7Info.getS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc8Info.getS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		s02811Calc9Info.getS02811Calc1InfoBytes(buffer, position);
		position += Db2S02811Calc1Info.Len.S02811_CALC1_INFO;
		casCalc1Info.getCasCalc1InfoBytes(buffer, position);
		position += Db2CasCalc1Info.Len.CAS_CALC1_INFO;
		casTableInfo.getCasTableInfoBytes(buffer, position);
		position += Db2CasTableInfo.Len.CAS_TABLE_INFO;
		getS02805ClthCareInfoBytes(buffer, position);
		position += Len.S02805_CLTH_CARE_INFO;
		s02952NameDemoInfo.getS02952NameDemoInfoBytes(buffer, position);
		position += Db2S02952NameDemoInfo.Len.S02952_NAME_DEMO_INFO;
		getS02952PerfNameDemoInfoBytes(buffer, position);
		return buffer;
	}

	public void setS02805ClthCareInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		hcIndusCd = MarshalByte.readString(buffer, position, Len.HC_INDUS_CD);
	}

	public byte[] getS02805ClthCareInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, hcIndusCd, Len.HC_INDUS_CD);
		return buffer;
	}

	public void setHcIndusCd(String hcIndusCd) {
		this.hcIndusCd = Functions.subString(hcIndusCd, Len.HC_INDUS_CD);
	}

	public String getHcIndusCd() {
		return this.hcIndusCd;
	}

	public void setS02952PerfNameDemoInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		perfPvdrName = MarshalByte.readString(buffer, position, Len.PERF_PVDR_NAME);
	}

	public byte[] getS02952PerfNameDemoInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, perfPvdrName, Len.PERF_PVDR_NAME);
		return buffer;
	}

	public void setPerfPvdrName(String perfPvdrName) {
		this.perfPvdrName = Functions.subString(perfPvdrName, Len.PERF_PVDR_NAME);
	}

	public String getPerfPvdrName() {
		return this.perfPvdrName;
	}

	public Db2CasCalc1Info getCasCalc1Info() {
		return casCalc1Info;
	}

	public Db2CasTableInfo getCasTableInfo() {
		return casTableInfo;
	}

	public Db2S02801Info getS02801Info() {
		return s02801Info;
	}

	public Db2S02809LineInfo getS02809LineInfo() {
		return s02809LineInfo;
	}

	public Db2S02811Calc1Info getS02811Calc1Info() {
		return s02811Calc1Info;
	}

	public Db2S02811Calc1Info getS02811Calc2Info() {
		return s02811Calc2Info;
	}

	public Db2S02811Calc1Info getS02811Calc3Info() {
		return s02811Calc3Info;
	}

	public Db2S02811Calc1Info getS02811Calc4Info() {
		return s02811Calc4Info;
	}

	public Db2S02811Calc1Info getS02811Calc5Info() {
		return s02811Calc5Info;
	}

	public Db2S02811Calc1Info getS02811Calc6Info() {
		return s02811Calc6Info;
	}

	public Db2S02811Calc1Info getS02811Calc7Info() {
		return s02811Calc7Info;
	}

	public Db2S02811Calc1Info getS02811Calc8Info() {
		return s02811Calc8Info;
	}

	public Db2S02811Calc1Info getS02811Calc9Info() {
		return s02811Calc9Info;
	}

	public Db2S02813PmtOrClaimInfo getS02813PmtOrClaimInfo() {
		return s02813PmtOrClaimInfo;
	}

	public Db2S02815ProviderInfo getS02815ProviderInfo() {
		return s02815ProviderInfo;
	}

	public Db2S02952NameDemoInfo getS02952NameDemoInfo() {
		return s02952NameDemoInfo;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HC_INDUS_CD = 7;
		public static final int PERF_PVDR_NAME = 25;
		public static final int S02805_CLTH_CARE_INFO = HC_INDUS_CD;
		public static final int S02952_PERF_NAME_DEMO_INFO = PERF_PVDR_NAME;
		public static final int EOB_DB2_CURSOR_AREA = Db2S02801Info.Len.S02801_INFO + Db2S02815ProviderInfo.Len.S02815_PROVIDER_INFO
				+ Db2S02813PmtOrClaimInfo.Len.S02813_PMT_OR_CLAIM_INFO + Db2S02809LineInfo.Len.S02809_LINE_INFO
				+ Db2S02811Calc1Info.Len.S02811_CALC1_INFO + Db2S02811Calc1Info.Len.S02811_CALC1_INFO + Db2S02811Calc1Info.Len.S02811_CALC1_INFO
				+ Db2S02811Calc1Info.Len.S02811_CALC1_INFO + Db2S02811Calc1Info.Len.S02811_CALC1_INFO + Db2S02811Calc1Info.Len.S02811_CALC1_INFO
				+ Db2S02811Calc1Info.Len.S02811_CALC1_INFO + Db2S02811Calc1Info.Len.S02811_CALC1_INFO + Db2S02811Calc1Info.Len.S02811_CALC1_INFO
				+ Db2CasCalc1Info.Len.CAS_CALC1_INFO + Db2CasTableInfo.Len.CAS_TABLE_INFO + S02805_CLTH_CARE_INFO
				+ Db2S02952NameDemoInfo.Len.S02952_NAME_DEMO_INFO + S02952_PERF_NAME_DEMO_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
