/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [S02652SA, S02682SA, S02713SA, S02800SA, S02809SA, S02813SA, S02993SA, S04315SA, TRIGGER2]
 * 
 */
public interface IS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043 extends BaseSqlTo {

	/**
	 * Host Variable TRIG-GMIS-INDICATOR
	 * 
	 */
	String getTrigGmisIndicator();

	void setTrigGmisIndicator(String trigGmisIndicator);

	/**
	 * Host Variable TRIG-PROD-IND
	 * 
	 */
	String getTrigProdInd();

	void setTrigProdInd(String trigProdInd);

	/**
	 * Host Variable TRIG-HIST-BILL-PROV-NPI-ID
	 * 
	 */
	String getTrigHistBillProvNpiId();

	void setTrigHistBillProvNpiId(String trigHistBillProvNpiId);

	/**
	 * Host Variable TRIG-LOCAL-BILL-PROV-LOB-CD
	 * 
	 */
	char getTrigLocalBillProvLobCd();

	void setTrigLocalBillProvLobCd(char trigLocalBillProvLobCd);

	/**
	 * Host Variable TRIG-LOCAL-BILL-PROV-ID
	 * 
	 */
	String getTrigLocalBillProvId();

	void setTrigLocalBillProvId(String trigLocalBillProvId);

	/**
	 * Host Variable TRIG-BILL-PROV-ID-QLF-CD
	 * 
	 */
	String getTrigBillProvIdQlfCd();

	void setTrigBillProvIdQlfCd(String trigBillProvIdQlfCd);

	/**
	 * Host Variable TRIG-BILL-PROV-SPEC-CD
	 * 
	 */
	String getTrigBillProvSpecCd();

	void setTrigBillProvSpecCd(String trigBillProvSpecCd);

	/**
	 * Host Variable TRIG-HIST-PERF-PROV-NPI-ID
	 * 
	 */
	String getTrigHistPerfProvNpiId();

	void setTrigHistPerfProvNpiId(String trigHistPerfProvNpiId);

	/**
	 * Host Variable TRIG-LOCAL-PERF-PROV-LOB-CD
	 * 
	 */
	char getTrigLocalPerfProvLobCd();

	void setTrigLocalPerfProvLobCd(char trigLocalPerfProvLobCd);

	/**
	 * Host Variable TRIG-LOCAL-PERF-PROV-ID
	 * 
	 */
	String getTrigLocalPerfProvId();

	void setTrigLocalPerfProvId(String trigLocalPerfProvId);

	/**
	 * Host Variable TRIG-INS-ID
	 * 
	 */
	String getTrigInsId();

	void setTrigInsId(String trigInsId);

	/**
	 * Host Variable TRIG-MEMBER-ID
	 * 
	 */
	String getTrigMemberId();

	void setTrigMemberId(String trigMemberId);

	/**
	 * Host Variable TRIG-CORP-RCV-DT
	 * 
	 */
	String getTrigCorpRcvDt();

	void setTrigCorpRcvDt(String trigCorpRcvDt);

	/**
	 * Host Variable TRIG-PMT-ADJD-DT
	 * 
	 */
	String getTrigPmtAdjdDt();

	void setTrigPmtAdjdDt(String trigPmtAdjdDt);

	/**
	 * Host Variable TRIG-PMT-FRM-SERV-DT
	 * 
	 */
	String getTrigPmtFrmServDt();

	void setTrigPmtFrmServDt(String trigPmtFrmServDt);

	/**
	 * Host Variable TRIG-PMT-THR-SERV-DT
	 * 
	 */
	String getTrigPmtThrServDt();

	void setTrigPmtThrServDt(String trigPmtThrServDt);

	/**
	 * Host Variable TRIG-BILL-FRM-DT
	 * 
	 */
	String getTrigBillFrmDt();

	void setTrigBillFrmDt(String trigBillFrmDt);

	/**
	 * Host Variable TRIG-BILL-THR-DT
	 * 
	 */
	String getTrigBillThrDt();

	void setTrigBillThrDt(String trigBillThrDt);

	/**
	 * Host Variable TRIG-ASG-CD
	 * 
	 */
	String getTrigAsgCd();

	void setTrigAsgCd(String trigAsgCd);

	/**
	 * Host Variable WS-TRIG-CLM-PD-AM
	 * 
	 */
	AfDecimal getWsTrigClmPdAm();

	void setWsTrigClmPdAm(AfDecimal wsTrigClmPdAm);

	/**
	 * Host Variable TRIG-ADJ-TYP-CD
	 * 
	 */
	char getTrigAdjTypCd();

	void setTrigAdjTypCd(char trigAdjTypCd);

	/**
	 * Host Variable TRIG-KCAPS-TEAM-NM
	 * 
	 */
	String getTrigKcapsTeamNm();

	void setTrigKcapsTeamNm(String trigKcapsTeamNm);

	/**
	 * Host Variable TRIG-KCAPS-USE-ID
	 * 
	 */
	String getTrigKcapsUseId();

	void setTrigKcapsUseId(String trigKcapsUseId);

	/**
	 * Host Variable TRIG-HIST-LOAD-CD
	 * 
	 */
	char getTrigHistLoadCd();

	void setTrigHistLoadCd(char trigHistLoadCd);

	/**
	 * Host Variable TRIG-PMT-BK-PROD-CD
	 * 
	 */
	char getTrigPmtBkProdCd();

	void setTrigPmtBkProdCd(char trigPmtBkProdCd);

	/**
	 * Host Variable TRIG-DRG-CD
	 * 
	 */
	String getTrigDrgCd();

	void setTrigDrgCd(String trigDrgCd);

	/**
	 * Host Variable WS-TRIG-SCHDL-DRG-ALW-AM
	 * 
	 */
	AfDecimal getWsTrigSchdlDrgAlwAm();

	void setWsTrigSchdlDrgAlwAm(AfDecimal wsTrigSchdlDrgAlwAm);

	/**
	 * Host Variable WS-TRIG-ALT-DRG-ALW-AM
	 * 
	 */
	AfDecimal getWsTrigAltDrgAlwAm();

	void setWsTrigAltDrgAlwAm(AfDecimal wsTrigAltDrgAlwAm);

	/**
	 * Host Variable WS-TRIG-OVER-DRG-ALW-AM
	 * 
	 */
	AfDecimal getWsTrigOverDrgAlwAm();

	void setWsTrigOverDrgAlwAm(AfDecimal wsTrigOverDrgAlwAm);

	/**
	 * Host Variable TRIG-PMT-OI-IN
	 * 
	 */
	char getTrigPmtOiIn();

	void setTrigPmtOiIn(char trigPmtOiIn);

	/**
	 * Host Variable TRIG-PAT-ACT-MED-REC-ID
	 * 
	 */
	String getTrigPatActMedRecId();

	void setTrigPatActMedRecId(String trigPatActMedRecId);

	/**
	 * Host Variable TRIG-PGM-AREA-CD
	 * 
	 */
	String getTrigPgmAreaCd();

	void setTrigPgmAreaCd(String trigPgmAreaCd);

	/**
	 * Host Variable TRIG-FIN-CD
	 * 
	 */
	String getTrigFinCd();

	void setTrigFinCd(String trigFinCd);

	/**
	 * Host Variable TRIG-ADJD-PROV-STAT-CD
	 * 
	 */
	char getTrigAdjdProvStatCd();

	void setTrigAdjdProvStatCd(char trigAdjdProvStatCd);

	/**
	 * Host Variable TRIG-ITS-CLM-TYP-CD
	 * 
	 */
	char getTrigItsClmTypCd();

	void setTrigItsClmTypCd(char trigItsClmTypCd);

	/**
	 * Host Variable TRIG-PRMPT-PAY-DAY-CD
	 * 
	 */
	String getTrigPrmptPayDayCd();

	void setTrigPrmptPayDayCd(String trigPrmptPayDayCd);

	/**
	 * Host Variable TRIG-PRMPT-PAY-OVRD-CD
	 * 
	 */
	char getTrigPrmptPayOvrdCd();

	void setTrigPrmptPayOvrdCd(char trigPrmptPayOvrdCd);

	/**
	 * Host Variable WS-ACCRUED-PRMPT-PAY-INT-AM
	 * 
	 */
	AfDecimal getWsAccruedPrmptPayIntAm();

	void setWsAccruedPrmptPayIntAm(AfDecimal wsAccruedPrmptPayIntAm);

	/**
	 * Host Variable TRIG-PROV-UNWRP-DT
	 * 
	 */
	String getTrigProvUnwrpDt();

	void setTrigProvUnwrpDt(String trigProvUnwrpDt);

	/**
	 * Host Variable TRIG-GRP-ID
	 * 
	 */
	String getTrigGrpId();

	void setTrigGrpId(String trigGrpId);

	/**
	 * Host Variable TRIG-TYP-GRP-CD
	 * 
	 */
	String getTrigTypGrpCd();

	void setTrigTypGrpCd(String trigTypGrpCd);

	/**
	 * Host Variable TRIG-NPI-CD
	 * 
	 */
	char getTrigNpiCd();

	void setTrigNpiCd(char trigNpiCd);

	/**
	 * Host Variable TRIG-CLAIM-LOB-CD
	 * 
	 */
	char getTrigClaimLobCd();

	void setTrigClaimLobCd(char trigClaimLobCd);

	/**
	 * Host Variable TRIG-RATE-CD
	 * 
	 */
	String getTrigRateCd();

	void setTrigRateCd(String trigRateCd);

	/**
	 * Host Variable TRIG-NTWRK-CD
	 * 
	 */
	String getTrigNtwrkCd();

	void setTrigNtwrkCd(String trigNtwrkCd);

	/**
	 * Host Variable TRIG-BASE-CN-ARNG-CD
	 * 
	 */
	String getTrigBaseCnArngCd();

	void setTrigBaseCnArngCd(String trigBaseCnArngCd);

	/**
	 * Host Variable TRIG-PRIM-CN-ARNG-CD
	 * 
	 */
	String getTrigPrimCnArngCd();

	void setTrigPrimCnArngCd(String trigPrimCnArngCd);

	/**
	 * Host Variable TRIG-ENR-CL-CD
	 * 
	 */
	String getTrigEnrClCd();

	void setTrigEnrClCd(String trigEnrClCd);

	/**
	 * Host Variable WS-TRIG-LST-FNL-PMT-PT-ID
	 * 
	 */
	short getWsTrigLstFnlPmtPtId();

	void setWsTrigLstFnlPmtPtId(short wsTrigLstFnlPmtPtId);

	/**
	 * Host Variable TRIG-STAT-ADJ-PREV-PMT
	 * 
	 */
	char getTrigStatAdjPrevPmt();

	void setTrigStatAdjPrevPmt(char trigStatAdjPrevPmt);

	/**
	 * Host Variable TRIG-EFT-IND
	 * 
	 */
	char getTrigEftInd();

	void setTrigEftInd(char trigEftInd);

	/**
	 * Host Variable TRIG-EFT-ACCOUNT-TYPE
	 * 
	 */
	char getTrigEftAccountType();

	void setTrigEftAccountType(char trigEftAccountType);

	/**
	 * Host Variable TRIG-EFT-ACCT
	 * 
	 */
	String getTrigEftAcct();

	void setTrigEftAcct(String trigEftAcct);

	/**
	 * Host Variable TRIG-EFT-TRANS
	 * 
	 */
	String getTrigEftTrans();

	void setTrigEftTrans(String trigEftTrans);

	/**
	 * Host Variable TRIG-ALPH-PRFX-CD
	 * 
	 */
	String getTrigAlphPrfxCd();

	void setTrigAlphPrfxCd(String trigAlphPrfxCd);

	/**
	 * Host Variable TRIG-GL-OFST-ORIG-CD
	 * 
	 */
	String getTrigGlOfstOrigCd();

	void setTrigGlOfstOrigCd(String trigGlOfstOrigCd);

	/**
	 * Host Variable WS-TRIG-GL-SOTE-ORIG-CD
	 * 
	 */
	short getWsTrigGlSoteOrigCd();

	void setWsTrigGlSoteOrigCd(short wsTrigGlSoteOrigCd);

	/**
	 * Host Variable TRIG-VOID-CD
	 * 
	 */
	char getTrigVoidCd();

	void setTrigVoidCd(char trigVoidCd);

	/**
	 * Host Variable TRIG-ITS-INS-ID
	 * 
	 */
	String getTrigItsInsId();

	void setTrigItsInsId(String trigItsInsId);

	/**
	 * Host Variable TRIG-ADJ-RESP-CD
	 * 
	 */
	char getTrigAdjRespCd();

	void setTrigAdjRespCd(char trigAdjRespCd);

	/**
	 * Host Variable TRIG-ADJ-TRCK-CD
	 * 
	 */
	String getTrigAdjTrckCd();

	void setTrigAdjTrckCd(String trigAdjTrckCd);

	/**
	 * Host Variable TRIG-CLMCK-ADJ-STAT-CD
	 * 
	 */
	char getTrigClmckAdjStatCd();

	void setTrigClmckAdjStatCd(char trigClmckAdjStatCd);

	/**
	 * Host Variable TRIG-837-BILL-PROV-NPI-ID
	 * 
	 */
	String getTrig837BillProvNpiId();

	void setTrig837BillProvNpiId(String trig837BillProvNpiId);

	/**
	 * Host Variable TRIG-837-PERF-PROV-NPI-ID
	 * 
	 */
	String getTrig837PerfProvNpiId();

	void setTrig837PerfProvNpiId(String trig837PerfProvNpiId);

	/**
	 * Host Variable TRIG-ITS-CK
	 * 
	 */
	char getTrigItsCk();

	void setTrigItsCk(char trigItsCk);

	/**
	 * Host Variable TRIG-DATE-COVERAGE-LAPSED
	 * 
	 */
	String getTrigDateCoverageLapsed();

	void setTrigDateCoverageLapsed(String trigDateCoverageLapsed);

	/**
	 * Host Variable TRIG-OI-PAY-NM
	 * 
	 */
	String getTrigOiPayNm();

	void setTrigOiPayNm(String trigOiPayNm);

	/**
	 * Host Variable TRIG-CORR-PRIORITY-SUB-ID
	 * 
	 */
	String getTrigCorrPrioritySubId();

	void setTrigCorrPrioritySubId(String trigCorrPrioritySubId);

	/**
	 * Host Variable TRIG-HIPAA-VERSION-FORMAT-ID
	 * 
	 */
	String getTrigHipaaVersionFormatId();

	void setTrigHipaaVersionFormatId(String trigHipaaVersionFormatId);

	/**
	 * Host Variable VBR-IN
	 * 
	 */
	char getVbrIn();

	void setVbrIn(char vbrIn);

	/**
	 * Host Variable MRKT-PKG-CD
	 * 
	 */
	String getMrktPkgCd();

	void setMrktPkgCd(String mrktPkgCd);

	/**
	 * Host Variable CLM-CNTL-ID
	 * 
	 */
	String getClmCntlId();

	void setClmCntlId(String clmCntlId);

	/**
	 * Host Variable CLM-CNTL-SFX-ID
	 * 
	 */
	char getClmCntlSfxId();

	void setClmCntlSfxId(char clmCntlSfxId);

	/**
	 * Host Variable CLM-PMT-ID
	 * 
	 */
	short getClmPmtId();

	void setClmPmtId(short clmPmtId);

	/**
	 * Host Variable MEM-ID
	 * 
	 */
	String getMemId();

	void setMemId(String memId);

	/**
	 * Host Variable CLM-PD-AM
	 * 
	 */
	AfDecimal getClmPdAm();

	void setClmPdAm(AfDecimal clmPdAm);

	/**
	 * Host Variable ADJ-TYP-CD
	 * 
	 */
	char getAdjTypCd();

	void setAdjTypCd(char adjTypCd);

	/**
	 * Host Variable KCAPS-TEAM-NM
	 * 
	 */
	String getKcapsTeamNm();

	void setKcapsTeamNm(String kcapsTeamNm);

	/**
	 * Host Variable KCAPS-USE-ID
	 * 
	 */
	String getKcapsUseId();

	void setKcapsUseId(String kcapsUseId);

	/**
	 * Host Variable HIST-LOAD-CD
	 * 
	 */
	char getHistLoadCd();

	void setHistLoadCd(char histLoadCd);

	/**
	 * Host Variable PGM-AREA-CD
	 * 
	 */
	String getPgmAreaCd();

	void setPgmAreaCd(String pgmAreaCd);

	/**
	 * Host Variable FIN-CD
	 * 
	 */
	String getFinCd();

	void setFinCd(String finCd);

	/**
	 * Host Variable ITS-CLM-TYP-CD
	 * 
	 */
	char getItsClmTypCd();

	void setItsClmTypCd(char itsClmTypCd);

	/**
	 * Host Variable TYP-GRP-CD
	 * 
	 */
	String getTypGrpCd();

	void setTypGrpCd(String typGrpCd);

	/**
	 * Host Variable NPI-CD
	 * 
	 */
	char getNpiCd();

	void setNpiCd(char npiCd);

	/**
	 * Host Variable LOB-CD
	 * 
	 */
	char getLobCd();

	void setLobCd(char lobCd);

	/**
	 * Host Variable NTWRK-CD
	 * 
	 */
	String getNtwrkCd();

	void setNtwrkCd(String ntwrkCd);

	/**
	 * Host Variable ENR-CL-CD
	 * 
	 */
	String getEnrClCd();

	void setEnrClCd(String enrClCd);

	/**
	 * Host Variable GL-OFST-VOID-CD
	 * 
	 */
	String getGlOfstVoidCd();

	void setGlOfstVoidCd(String glOfstVoidCd);

	/**
	 * Host Variable GL-SOTE-VOID-CD
	 * 
	 */
	short getGlSoteVoidCd();

	void setGlSoteVoidCd(short glSoteVoidCd);

	/**
	 * Host Variable VOID-CD
	 * 
	 */
	char getVoidCd();

	void setVoidCd(char voidCd);

	/**
	 * Host Variable DRG-CD
	 * 
	 */
	String getDrgCd();

	void setDrgCd(String drgCd);

	/**
	 * Host Variable SCHDL-DRG-ALW-AM
	 * 
	 */
	AfDecimal getSchdlDrgAlwAm();

	void setSchdlDrgAlwAm(AfDecimal schdlDrgAlwAm);

	/**
	 * Host Variable ADJ-RESP-CD
	 * 
	 */
	char getAdjRespCd();

	void setAdjRespCd(char adjRespCd);

	/**
	 * Host Variable ADJ-TRCK-CD
	 * 
	 */
	String getAdjTrckCd();

	void setAdjTrckCd(String adjTrckCd);

	/**
	 * Host Variable PRMPT-PAY-INT-AM
	 * 
	 */
	AfDecimal getPrmptPayIntAm();

	void setPrmptPayIntAm(AfDecimal prmptPayIntAm);

	/**
	 * Host Variable CLI-ID
	 * 
	 */
	short getCliId();

	void setCliId(short cliId);

	/**
	 * Host Variable PROV-SBMT-LN-NO-ID
	 * 
	 */
	short getProvSbmtLnNoId();

	void setProvSbmtLnNoId(short provSbmtLnNoId);

	/**
	 * Host Variable ADDNL-RMT-LI-ID
	 * 
	 */
	short getAddnlRmtLiId();

	void setAddnlRmtLiId(short addnlRmtLiId);

	/**
	 * Host Variable LI-FRM-SERV-DT
	 * 
	 */
	String getLiFrmServDt();

	void setLiFrmServDt(String liFrmServDt);

	/**
	 * Host Variable LI-THR-SERV-DT
	 * 
	 */
	String getLiThrServDt();

	void setLiThrServDt(String liThrServDt);

	/**
	 * Host Variable EXMN-ACTN-CD
	 * 
	 */
	char getExmnActnCd();

	void setExmnActnCd(char exmnActnCd);

	/**
	 * Host Variable DNL-RMRK-CD
	 * 
	 */
	String getDnlRmrkCd();

	void setDnlRmrkCd(String dnlRmrkCd);

	/**
	 * Host Variable DNL-RMRK-USG-CD
	 * 
	 */
	char getDnlRmrkUsgCd();

	void setDnlRmrkUsgCd(char dnlRmrkUsgCd);

	/**
	 * Host Variable PROC-CD
	 * 
	 */
	String getProcCd();

	void setProcCd(String procCd);

	/**
	 * Host Variable PROC-MOD1-CD
	 * 
	 */
	String getProcMod1Cd();

	void setProcMod1Cd(String procMod1Cd);

	/**
	 * Host Variable PROC-MOD2-CD
	 * 
	 */
	String getProcMod2Cd();

	void setProcMod2Cd(String procMod2Cd);

	/**
	 * Host Variable PROC-MOD3-CD
	 * 
	 */
	String getProcMod3Cd();

	void setProcMod3Cd(String procMod3Cd);

	/**
	 * Host Variable PROC-MOD4-CD
	 * 
	 */
	String getProcMod4Cd();

	void setProcMod4Cd(String procMod4Cd);

	/**
	 * Host Variable REV-CD
	 * 
	 */
	String getRevCd();

	void setRevCd(String revCd);

	/**
	 * Host Variable UNIT-CT
	 * 
	 */
	AfDecimal getUnitCt();

	void setUnitCt(AfDecimal unitCt);

	/**
	 * Host Variable ORIG-SBMT-UNIT-CT
	 * 
	 */
	AfDecimal getOrigSbmtUnitCt();

	void setOrigSbmtUnitCt(AfDecimal origSbmtUnitCt);

	/**
	 * Host Variable ORIG-SBMT-CHG-AM
	 * 
	 */
	AfDecimal getOrigSbmtChgAm();

	void setOrigSbmtChgAm(AfDecimal origSbmtChgAm);

	/**
	 * Host Variable CHG-AM
	 * 
	 */
	AfDecimal getChgAm();

	void setChgAm(AfDecimal chgAm);

	/**
	 * Host Variable LI-ALCT-OI-PD-AM
	 * 
	 */
	AfDecimal getLiAlctOiPdAm();

	void setLiAlctOiPdAm(AfDecimal liAlctOiPdAm);

	/**
	 * Host Variable TS-CD
	 * 
	 */
	String getTsCd();

	void setTsCd(String tsCd);

	/**
	 * Host Variable ADJD-OVRD1-CD
	 * 
	 */
	String getAdjdOvrd1Cd();

	void setAdjdOvrd1Cd(String adjdOvrd1Cd);

	/**
	 * Host Variable ADJD-OVRD2-CD
	 * 
	 */
	String getAdjdOvrd2Cd();

	void setAdjdOvrd2Cd(String adjdOvrd2Cd);

	/**
	 * Host Variable ADJD-OVRD3-CD
	 * 
	 */
	String getAdjdOvrd3Cd();

	void setAdjdOvrd3Cd(String adjdOvrd3Cd);

	/**
	 * Host Variable ADJD-OVRD4-CD
	 * 
	 */
	String getAdjdOvrd4Cd();

	void setAdjdOvrd4Cd(String adjdOvrd4Cd);

	/**
	 * Host Variable ADJD-OVRD5-CD
	 * 
	 */
	String getAdjdOvrd5Cd();

	void setAdjdOvrd5Cd(String adjdOvrd5Cd);

	/**
	 * Host Variable ADJD-OVRD6-CD
	 * 
	 */
	String getAdjdOvrd6Cd();

	void setAdjdOvrd6Cd(String adjdOvrd6Cd);

	/**
	 * Host Variable ADJD-OVRD7-CD
	 * 
	 */
	String getAdjdOvrd7Cd();

	void setAdjdOvrd7Cd(String adjdOvrd7Cd);

	/**
	 * Host Variable ADJD-OVRD8-CD
	 * 
	 */
	String getAdjdOvrd8Cd();

	void setAdjdOvrd8Cd(String adjdOvrd8Cd);

	/**
	 * Host Variable ADJD-OVRD9-CD
	 * 
	 */
	String getAdjdOvrd9Cd();

	void setAdjdOvrd9Cd(String adjdOvrd9Cd);

	/**
	 * Host Variable ADJD-OVRD10-CD
	 * 
	 */
	String getAdjdOvrd10Cd();

	void setAdjdOvrd10Cd(String adjdOvrd10Cd);

	/**
	 * Host Variable CRS-RF-CLM-CNTL-ID
	 * 
	 */
	String getCrsRfClmCntlId();

	void setCrsRfClmCntlId(String crsRfClmCntlId);

	/**
	 * Host Variable CS-RF-CLM-CL-SX-ID
	 * 
	 */
	char getCsRfClmClSxId();

	void setCsRfClmClSxId(char csRfClmClSxId);

	/**
	 * Host Variable CRS-REF-CLM-PMT-ID
	 * 
	 */
	short getCrsRefClmPmtId();

	void setCrsRefClmPmtId(short crsRefClmPmtId);

	/**
	 * Host Variable CRS-REF-CLI-ID
	 * 
	 */
	short getCrsRefCliId();

	void setCrsRefCliId(short crsRefCliId);

	/**
	 * Host Variable CRS-REF-RSN-CD
	 * 
	 */
	char getCrsRefRsnCd();

	void setCrsRefRsnCd(char crsRefRsnCd);

	/**
	 * Host Variable ADJD-PROC-CD
	 * 
	 */
	String getAdjdProcCd();

	void setAdjdProcCd(String adjdProcCd);

	/**
	 * Host Variable ADJD-PROC-MOD1-CD
	 * 
	 */
	String getAdjdProcMod1Cd();

	void setAdjdProcMod1Cd(String adjdProcMod1Cd);

	/**
	 * Host Variable ADJD-PROC-MOD2-CD
	 * 
	 */
	String getAdjdProcMod2Cd();

	void setAdjdProcMod2Cd(String adjdProcMod2Cd);

	/**
	 * Host Variable ADJD-PROC-MOD3-CD
	 * 
	 */
	String getAdjdProcMod3Cd();

	void setAdjdProcMod3Cd(String adjdProcMod3Cd);

	/**
	 * Host Variable ADJD-PROC-MOD4-CD
	 * 
	 */
	String getAdjdProcMod4Cd();

	void setAdjdProcMod4Cd(String adjdProcMod4Cd);

	/**
	 * Host Variable ACT-INACT-CD
	 * 
	 */
	char getActInactCd();

	void setActInactCd(char actInactCd);

	/**
	 * Host Variable WS-VR4-CRS-RF-CLM-CNTL-ID
	 * 
	 */
	String getWsVr4CrsRfClmCntlId();

	void setWsVr4CrsRfClmCntlId(String wsVr4CrsRfClmCntlId);

	/**
	 * Host Variable WS-VR4-CS-RF-CLM-CL-SX-ID
	 * 
	 */
	char getWsVr4CsRfClmClSxId();

	void setWsVr4CsRfClmClSxId(char wsVr4CsRfClmClSxId);

	/**
	 * Host Variable WS-VR4-CRS-REF-CLM-PMT-ID
	 * 
	 */
	String getWsVr4CrsRefClmPmtId();

	void setWsVr4CrsRefClmPmtId(String wsVr4CrsRefClmPmtId);

	/**
	 * Host Variable WS-VR4-CRS-REF-CLI-ID
	 * 
	 */
	String getWsVr4CrsRefCliId();

	void setWsVr4CrsRefCliId(String wsVr4CrsRefCliId);

	/**
	 * Host Variable WS-VR4-CRS-REF-RSN-CD
	 * 
	 */
	char getWsVr4CrsRefRsnCd();

	void setWsVr4CrsRefRsnCd(char wsVr4CrsRefRsnCd);

	/**
	 * Host Variable WS-VR4-DNL-RMRK-CD
	 * 
	 */
	String getWsVr4DnlRmrkCd();

	void setWsVr4DnlRmrkCd(String wsVr4DnlRmrkCd);

	/**
	 * Host Variable WS-VR4-ADJD-PROC-CD
	 * 
	 */
	String getWsVr4AdjdProcCd();

	void setWsVr4AdjdProcCd(String wsVr4AdjdProcCd);

	/**
	 * Host Variable WS-VR4-ADJD-PROC-MOD1-CD
	 * 
	 */
	String getWsVr4AdjdProcMod1Cd();

	void setWsVr4AdjdProcMod1Cd(String wsVr4AdjdProcMod1Cd);

	/**
	 * Host Variable WS-VR4-ADJD-PROC-MOD2-CD
	 * 
	 */
	String getWsVr4AdjdProcMod2Cd();

	void setWsVr4AdjdProcMod2Cd(String wsVr4AdjdProcMod2Cd);

	/**
	 * Host Variable WS-VR4-ADJD-PROC-MOD3-CD
	 * 
	 */
	String getWsVr4AdjdProcMod3Cd();

	void setWsVr4AdjdProcMod3Cd(String wsVr4AdjdProcMod3Cd);

	/**
	 * Host Variable WS-VR4-ADJD-PROC-MOD4-CD
	 * 
	 */
	String getWsVr4AdjdProcMod4Cd();

	void setWsVr4AdjdProcMod4Cd(String wsVr4AdjdProcMod4Cd);

	/**
	 * Host Variable WS-VR4-INIT-CLM-CNTL-ID
	 * 
	 */
	String getWsVr4InitClmCntlId();

	void setWsVr4InitClmCntlId(String wsVr4InitClmCntlId);

	/**
	 * Host Variable WS-VR4-INT-CLM-CNL-SFX-ID
	 * 
	 */
	char getWsVr4IntClmCnlSfxId();

	void setWsVr4IntClmCnlSfxId(char wsVr4IntClmCnlSfxId);

	/**
	 * Host Variable WS-VR4-INIT-CLM-PMT-ID
	 * 
	 */
	String getWsVr4InitClmPmtId();

	void setWsVr4InitClmPmtId(String wsVr4InitClmPmtId);

	/**
	 * Host Variable WS-VR4-ACT-INACT-CD
	 * 
	 */
	char getWsVr4ActInactCd();

	void setWsVr4ActInactCd(char wsVr4ActInactCd);

	/**
	 * Host Variable REF-ID
	 * 
	 */
	String getRefId();

	void setRefId(String refId);

	/**
	 * Host Variable ID-CD
	 * 
	 */
	String getIdCd();

	void setIdCd(String idCd);

	/**
	 * Host Variable LST-ORG-NM
	 * 
	 */
	String getLstOrgNm();

	void setLstOrgNm(String lstOrgNm);

	/**
	 * Host Variable FRST-NM
	 * 
	 */
	String getFrstNm();

	void setFrstNm(String frstNm);

	/**
	 * Host Variable MID-NM
	 * 
	 */
	String getMidNm();

	void setMidNm(String midNm);

	/**
	 * Host Variable SFX-NM
	 * 
	 */
	String getSfxNm();

	void setSfxNm(String sfxNm);

	/**
	 * Host Variable CLM-SBMT-ID
	 * 
	 */
	String getClmSbmtId();

	void setClmSbmtId(String clmSbmtId);

	/**
	 * Host Variable FAC-VL-CD
	 * 
	 */
	String getFacVlCd();

	void setFacVlCd(String facVlCd);

	/**
	 * Host Variable CLM-FREQ-TYP-CD
	 * 
	 */
	char getClmFreqTypCd();

	void setClmFreqTypCd(char clmFreqTypCd);

	/**
	 * Host Variable CLM-ADJ-GRP-CD
	 * 
	 */
	String getClmAdjGrpCd();

	void setClmAdjGrpCd(String clmAdjGrpCd);

	/**
	 * Host Variable CLM-ADJ-RSN-CD
	 * 
	 */
	String getClmAdjRsnCd();

	void setClmAdjRsnCd(String clmAdjRsnCd);

	/**
	 * Host Variable MNTRY-AM
	 * 
	 */
	AfDecimal getMntryAm();

	void setMntryAm(AfDecimal mntryAm);

	/**
	 * Host Variable QNTY-CT
	 * 
	 */
	AfDecimal getQntyCt();

	void setQntyCt(AfDecimal qntyCt);
};
