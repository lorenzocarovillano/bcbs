/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-CLAIM-ID<br>
 * Variable: WS-CLAIM-ID from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsClaimId {

	//==== PROPERTIES ====
	//Original name: WS-CLAIM-LOT-NUM
	private String lotNum = "";
	//Original name: WS-CLAIM-RECEIPT-JULDT
	private String receiptJuldt = "";
	//Original name: FILLER-WS-CLAIM-ID
	private String flr1 = "";

	//==== METHODS ====
	public void setWsClaimIdFormatted(String data) {
		byte[] buffer = new byte[Len.WS_CLAIM_ID];
		MarshalByte.writeString(buffer, 1, data, Len.WS_CLAIM_ID);
		setWsClaimIdBytes(buffer, 1);
	}

	public void setWsClaimIdBytes(byte[] buffer) {
		setWsClaimIdBytes(buffer, 1);
	}

	public void setWsClaimIdBytes(byte[] buffer, int offset) {
		int position = offset;
		lotNum = MarshalByte.readString(buffer, position, Len.LOT_NUM);
		position += Len.LOT_NUM;
		receiptJuldt = MarshalByte.readString(buffer, position, Len.RECEIPT_JULDT);
		position += Len.RECEIPT_JULDT;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public void setLotNum(String lotNum) {
		this.lotNum = Functions.subString(lotNum, Len.LOT_NUM);
	}

	public String getLotNum() {
		return this.lotNum;
	}

	public void setReceiptJuldt(String receiptJuldt) {
		this.receiptJuldt = Functions.subString(receiptJuldt, Len.RECEIPT_JULDT);
	}

	public String getReceiptJuldt() {
		return this.receiptJuldt;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LOT_NUM = 2;
		public static final int RECEIPT_JULDT = 5;
		public static final int FLR1 = 5;
		public static final int WS_CLAIM_ID = LOT_NUM + RECEIPT_JULDT + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
