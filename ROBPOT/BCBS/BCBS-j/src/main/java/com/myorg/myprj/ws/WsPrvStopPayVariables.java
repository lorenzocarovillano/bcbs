/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.occurs.WsPrvStopPayTable;

/**Original name: WS-PRV-STOP-PAY-VARIABLES<br>
 * Variable: WS-PRV-STOP-PAY-VARIABLES from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsPrvStopPayVariables {

	//==== PROPERTIES ====
	public static final int WS_PRV_STOP_PAY_TABLE_MAXOCCURS = 300;
	//Original name: WS-PRV-STOP-PAY-TABLE
	private WsPrvStopPayTable[] wsPrvStopPayTable = new WsPrvStopPayTable[WS_PRV_STOP_PAY_TABLE_MAXOCCURS];
	//Original name: WS-PRV-STOP-PAY-TBL-ENTRIES
	private short wsPrvStopPayTblEntries = DefaultValues.SHORT_VAL;
	//Original name: WS-SUB-PRV-STOP-PAY
	private short wsSubPrvStopPay = DefaultValues.SHORT_VAL;
	//Original name: WS-PRV-STOP-PAY-MAX
	private short wsPrvStopPayMax = ((short) 300);
	//Original name: WS-PRV-STOP-PAY-FINISHED
	private char wsPrvStopPayFinished = DefaultValues.CHAR_VAL;
	//Original name: WS-PRV-STOP-PAY-FOUND
	private char wsPrvStopPayFound = DefaultValues.CHAR_VAL;
	//Original name: WS-PROV-STOP-PAY-CD
	private String wsProvStopPayCd = DefaultValues.stringVal(Len.WS_PROV_STOP_PAY_CD);
	//Original name: HLD-SRS-PROV-ID
	private String hldSrsProvId = DefaultValues.stringVal(Len.HLD_SRS_PROV_ID);
	//Original name: HLD-SRS-PRV-LOB-CD
	private char hldSrsPrvLobCd = DefaultValues.CHAR_VAL;
	//Original name: HLD-SRS-FRM-SERV-DT
	private String hldSrsFrmServDt = DefaultValues.stringVal(Len.HLD_SRS_FRM_SERV_DT);
	//Original name: HLD-SRS-THR-SERV-DT
	private String hldSrsThrServDt = DefaultValues.stringVal(Len.HLD_SRS_THR_SERV_DT);

	//==== CONSTRUCTORS ====
	public WsPrvStopPayVariables() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int wsPrvStopPayTableIdx = 1; wsPrvStopPayTableIdx <= WS_PRV_STOP_PAY_TABLE_MAXOCCURS; wsPrvStopPayTableIdx++) {
			wsPrvStopPayTable[wsPrvStopPayTableIdx - 1] = new WsPrvStopPayTable();
		}
	}

	public void setWsPrvStopPayTblEntries(short wsPrvStopPayTblEntries) {
		this.wsPrvStopPayTblEntries = wsPrvStopPayTblEntries;
	}

	public short getWsPrvStopPayTblEntries() {
		return this.wsPrvStopPayTblEntries;
	}

	public void setWsSubPrvStopPay(short wsSubPrvStopPay) {
		this.wsSubPrvStopPay = wsSubPrvStopPay;
	}

	public short getWsSubPrvStopPay() {
		return this.wsSubPrvStopPay;
	}

	public short getWsPrvStopPayMax() {
		return this.wsPrvStopPayMax;
	}

	public void setWsPrvStopPayFinished(char wsPrvStopPayFinished) {
		this.wsPrvStopPayFinished = wsPrvStopPayFinished;
	}

	public void setWsPrvStopPayFinishedFormatted(String wsPrvStopPayFinished) {
		setWsPrvStopPayFinished(Functions.charAt(wsPrvStopPayFinished, Types.CHAR_SIZE));
	}

	public char getWsPrvStopPayFinished() {
		return this.wsPrvStopPayFinished;
	}

	public void setWsPrvStopPayFound(char wsPrvStopPayFound) {
		this.wsPrvStopPayFound = wsPrvStopPayFound;
	}

	public void setWsPrvStopPayFoundFormatted(String wsPrvStopPayFound) {
		setWsPrvStopPayFound(Functions.charAt(wsPrvStopPayFound, Types.CHAR_SIZE));
	}

	public char getWsPrvStopPayFound() {
		return this.wsPrvStopPayFound;
	}

	public void setWsProvStopPayCd(String wsProvStopPayCd) {
		this.wsProvStopPayCd = Functions.subString(wsProvStopPayCd, Len.WS_PROV_STOP_PAY_CD);
	}

	public String getWsProvStopPayCd() {
		return this.wsProvStopPayCd;
	}

	public void setHldSrsProvId(String hldSrsProvId) {
		this.hldSrsProvId = Functions.subString(hldSrsProvId, Len.HLD_SRS_PROV_ID);
	}

	public String getHldSrsProvId() {
		return this.hldSrsProvId;
	}

	public void setHldSrsPrvLobCd(char hldSrsPrvLobCd) {
		this.hldSrsPrvLobCd = hldSrsPrvLobCd;
	}

	public void setHldSrsPrvLobCdFormatted(String hldSrsPrvLobCd) {
		setHldSrsPrvLobCd(Functions.charAt(hldSrsPrvLobCd, Types.CHAR_SIZE));
	}

	public char getHldSrsPrvLobCd() {
		return this.hldSrsPrvLobCd;
	}

	public void setHldSrsFrmServDt(String hldSrsFrmServDt) {
		this.hldSrsFrmServDt = Functions.subString(hldSrsFrmServDt, Len.HLD_SRS_FRM_SERV_DT);
	}

	public String getHldSrsFrmServDt() {
		return this.hldSrsFrmServDt;
	}

	public void setHldSrsThrServDt(String hldSrsThrServDt) {
		this.hldSrsThrServDt = Functions.subString(hldSrsThrServDt, Len.HLD_SRS_THR_SERV_DT);
	}

	public String getHldSrsThrServDt() {
		return this.hldSrsThrServDt;
	}

	public WsPrvStopPayTable getWsPrvStopPayTable(int idx) {
		return wsPrvStopPayTable[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_PRV_STOP_PAY_TBL_ENTRIES = 4;
		public static final int WS_CNTR_PRV_STOP_PAY = 4;
		public static final int WS_PROV_STOP_PAY_CD = 2;
		public static final int HLD_SRS_PROV_ID = 10;
		public static final int HLD_SRS_FRM_SERV_DT = 10;
		public static final int HLD_SRS_THR_SERV_DT = 10;
		public static final int WS_SRS_STOP_PAY_G = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
