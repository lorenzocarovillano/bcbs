/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: REM-DETL-CONTINUED<br>
 * Variable: REM-DETL-CONTINUED from copybook NF07RMIT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class RemDetlContinued {

	//==== PROPERTIES ====
	//Original name: REM-DETL-ALLOWED-AMT
	private AfDecimal detlAllowedAmt = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: REM-DETL-PVDR-WRITEOFF
	private AfDecimal detlPvdrWriteoff = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: REM-DETL-AMT-OTHER-ADJ
	private AfDecimal detlAmtOtherAdj = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: REM-DETL-AMT-NOT-COVERED
	private AfDecimal detlAmtNotCovered = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: REM-DETL-AMT-COINSURANCE
	private AfDecimal detlAmtCoinsurance = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: REM-DETL-AMT-DEDUCTIBLE
	private AfDecimal detlAmtDeductible = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: REM-DETL-AMT-COPAY
	private AfDecimal detlAmtCopay = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: REM-DETL-AMT-PAT-OWES
	private AfDecimal detlAmtPatOwes = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: REM-DETL-AMT-PAID-WITHOUT-INT
	private AfDecimal detlAmtPaidWithoutInt = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: REM-DETL-BEG-BALANCE-FOR-MSG
	private AfDecimal detlBegBalanceForMsg = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: REM-DETL-AMT-INTEREST-PD
	private AfDecimal detlAmtInterestPd = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: REM-DETL-BEG-OR-END
	private String detlBegOrEnd = DefaultValues.stringVal(Len.DETL_BEG_OR_END);
	//Original name: REM-DETL-RX-NUM
	private String detlRxNum = DefaultValues.stringVal(Len.DETL_RX_NUM);
	//Original name: REM-DETL-TEAM
	private String detlTeam = DefaultValues.stringVal(Len.DETL_TEAM);
	//Original name: REM-DETL-ID
	private String detlId = DefaultValues.stringVal(Len.DETL_ID);
	//Original name: REM-DETL-TTH-SRFC-CD
	private String detlTthSrfcCd = DefaultValues.stringVal(Len.DETL_TTH_SRFC_CD);
	//Original name: REM-DETL-TTH-ID
	private String detlTthId = DefaultValues.stringVal(Len.DETL_TTH_ID);
	//Original name: REM-DETL-OP-PRC-PROC-CD
	private String detlOpPrcProcCd = DefaultValues.stringVal(Len.DETL_OP_PRC_PROC_CD);
	//Original name: REM-DETL-RECEIPT-DATE
	private RemDetlServDate detlReceiptDate = new RemDetlServDate();
	//Original name: REM-DETL-CLASS-OF-CNTR-1
	private String detlClassOfCntr1 = DefaultValues.stringVal(Len.DETL_CLASS_OF_CNTR1);
	//Original name: REM-DETL-CLASS-OF-CNTR-2
	private String detlClassOfCntr2 = DefaultValues.stringVal(Len.DETL_CLASS_OF_CNTR2);
	//Original name: REM-DETL-ADJUST-RSN-CDS
	private RemDetlAdjustRsnCds detlAdjustRsnCds = new RemDetlAdjustRsnCds();
	//Original name: REM-DETL-REMARKS-1
	private String detlRemarks1 = DefaultValues.stringVal(Len.DETL_REMARKS1);
	//Original name: REM-DETL-REMARKS-2
	private String detlRemarks2 = DefaultValues.stringVal(Len.DETL_REMARKS2);
	//Original name: REM-DETL-DENIAL-REMARK-CODE
	private String detlDenialRemarkCode = DefaultValues.stringVal(Len.DETL_DENIAL_REMARK_CODE);
	//Original name: REM-DETL-ADJUST-REASON-CODE
	private String detlAdjustReasonCode = DefaultValues.stringVal(Len.DETL_ADJUST_REASON_CODE);
	//Original name: REM-DETL-ADJUST-RESP-CODE
	private char detlAdjustRespCode = DefaultValues.CHAR_VAL;
	//Original name: REM-DETL-ADJUST-TYPE-CODE
	private char detlAdjustTypeCode = DefaultValues.CHAR_VAL;
	//Original name: REM-TAPE-NPI-PERF-PVDR-NUM
	private String tapeNpiPerfPvdrNum = DefaultValues.stringVal(Len.TAPE_NPI_PERF_PVDR_NUM);
	//Original name: REM-TAPE-LOCAL-PERF-PVDR-NUM
	private String tapeLocalPerfPvdrNum = DefaultValues.stringVal(Len.TAPE_LOCAL_PERF_PVDR_NUM);
	//Original name: REM-TAPE-NETWORK
	private String tapeNetwork = DefaultValues.stringVal(Len.TAPE_NETWORK);
	//Original name: REM-PEND-MESSAGE-1-17
	private String pendMessage117 = DefaultValues.stringVal(Len.PEND_MESSAGE117);
	//Original name: REM-PEND-MESSAGE-18-100
	private String pendMessage18100 = DefaultValues.stringVal(Len.PEND_MESSAGE18100);

	//==== METHODS ====
	public void setDetlContinuedBytes(byte[] buffer, int offset) {
		int position = offset;
		detlAllowedAmt.assign(MarshalByte.readDecimal(buffer, position, Len.Int.DETL_ALLOWED_AMT, Len.Fract.DETL_ALLOWED_AMT));
		position += Len.DETL_ALLOWED_AMT;
		detlPvdrWriteoff.assign(MarshalByte.readDecimal(buffer, position, Len.Int.DETL_PVDR_WRITEOFF, Len.Fract.DETL_PVDR_WRITEOFF));
		position += Len.DETL_PVDR_WRITEOFF;
		detlAmtOtherAdj.assign(MarshalByte.readDecimal(buffer, position, Len.Int.DETL_AMT_OTHER_ADJ, Len.Fract.DETL_AMT_OTHER_ADJ));
		position += Len.DETL_AMT_OTHER_ADJ;
		detlAmtNotCovered.assign(MarshalByte.readDecimal(buffer, position, Len.Int.DETL_AMT_NOT_COVERED, Len.Fract.DETL_AMT_NOT_COVERED));
		position += Len.DETL_AMT_NOT_COVERED;
		detlAmtCoinsurance.assign(MarshalByte.readDecimal(buffer, position, Len.Int.DETL_AMT_COINSURANCE, Len.Fract.DETL_AMT_COINSURANCE));
		position += Len.DETL_AMT_COINSURANCE;
		detlAmtDeductible.assign(MarshalByte.readDecimal(buffer, position, Len.Int.DETL_AMT_DEDUCTIBLE, Len.Fract.DETL_AMT_DEDUCTIBLE));
		position += Len.DETL_AMT_DEDUCTIBLE;
		detlAmtCopay.assign(MarshalByte.readDecimal(buffer, position, Len.Int.DETL_AMT_COPAY, Len.Fract.DETL_AMT_COPAY));
		position += Len.DETL_AMT_COPAY;
		detlAmtPatOwes.assign(MarshalByte.readDecimal(buffer, position, Len.Int.DETL_AMT_PAT_OWES, Len.Fract.DETL_AMT_PAT_OWES));
		position += Len.DETL_AMT_PAT_OWES;
		detlAmtPaidWithoutInt
				.assign(MarshalByte.readDecimal(buffer, position, Len.Int.DETL_AMT_PAID_WITHOUT_INT, Len.Fract.DETL_AMT_PAID_WITHOUT_INT));
		position += Len.DETL_AMT_PAID_WITHOUT_INT;
		detlBegBalanceForMsg.assign(MarshalByte.readDecimal(buffer, position, Len.Int.DETL_BEG_BALANCE_FOR_MSG, Len.Fract.DETL_BEG_BALANCE_FOR_MSG));
		position += Len.DETL_BEG_BALANCE_FOR_MSG;
		detlAmtInterestPd.assign(MarshalByte.readDecimal(buffer, position, Len.Int.DETL_AMT_INTEREST_PD, Len.Fract.DETL_AMT_INTEREST_PD));
		position += Len.DETL_AMT_INTEREST_PD;
		detlBegOrEnd = MarshalByte.readString(buffer, position, Len.DETL_BEG_OR_END);
		position += Len.DETL_BEG_OR_END;
		detlRxNum = MarshalByte.readString(buffer, position, Len.DETL_RX_NUM);
		position += Len.DETL_RX_NUM;
		detlTeam = MarshalByte.readString(buffer, position, Len.DETL_TEAM);
		position += Len.DETL_TEAM;
		detlId = MarshalByte.readString(buffer, position, Len.DETL_ID);
		position += Len.DETL_ID;
		detlTthSrfcCd = MarshalByte.readString(buffer, position, Len.DETL_TTH_SRFC_CD);
		position += Len.DETL_TTH_SRFC_CD;
		detlTthId = MarshalByte.readString(buffer, position, Len.DETL_TTH_ID);
		position += Len.DETL_TTH_ID;
		detlOpPrcProcCd = MarshalByte.readString(buffer, position, Len.DETL_OP_PRC_PROC_CD);
		position += Len.DETL_OP_PRC_PROC_CD;
		detlReceiptDate.setDetlServDateBytes(buffer, position);
		position += RemDetlServDate.Len.DETL_SERV_DATE;
		detlClassOfCntr1 = MarshalByte.readString(buffer, position, Len.DETL_CLASS_OF_CNTR1);
		position += Len.DETL_CLASS_OF_CNTR1;
		detlClassOfCntr2 = MarshalByte.readString(buffer, position, Len.DETL_CLASS_OF_CNTR2);
		position += Len.DETL_CLASS_OF_CNTR2;
		detlAdjustRsnCds.setDetlAdjustRsnCdsBytes(buffer, position);
		position += RemDetlAdjustRsnCds.Len.DETL_ADJUST_RSN_CDS;
		detlRemarks1 = MarshalByte.readString(buffer, position, Len.DETL_REMARKS1);
		position += Len.DETL_REMARKS1;
		detlRemarks2 = MarshalByte.readString(buffer, position, Len.DETL_REMARKS2);
		position += Len.DETL_REMARKS2;
		detlDenialRemarkCode = MarshalByte.readString(buffer, position, Len.DETL_DENIAL_REMARK_CODE);
		position += Len.DETL_DENIAL_REMARK_CODE;
		detlAdjustReasonCode = MarshalByte.readString(buffer, position, Len.DETL_ADJUST_REASON_CODE);
		position += Len.DETL_ADJUST_REASON_CODE;
		detlAdjustRespCode = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		detlAdjustTypeCode = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		tapeNpiPerfPvdrNum = MarshalByte.readString(buffer, position, Len.TAPE_NPI_PERF_PVDR_NUM);
		position += Len.TAPE_NPI_PERF_PVDR_NUM;
		tapeLocalPerfPvdrNum = MarshalByte.readString(buffer, position, Len.TAPE_LOCAL_PERF_PVDR_NUM);
		position += Len.TAPE_LOCAL_PERF_PVDR_NUM;
		tapeNetwork = MarshalByte.readString(buffer, position, Len.TAPE_NETWORK);
		position += Len.TAPE_NETWORK;
		setPendMessageBytes(buffer, position);
	}

	public byte[] getDetlContinuedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeDecimal(buffer, position, detlAllowedAmt.copy());
		position += Len.DETL_ALLOWED_AMT;
		MarshalByte.writeDecimal(buffer, position, detlPvdrWriteoff.copy());
		position += Len.DETL_PVDR_WRITEOFF;
		MarshalByte.writeDecimal(buffer, position, detlAmtOtherAdj.copy());
		position += Len.DETL_AMT_OTHER_ADJ;
		MarshalByte.writeDecimal(buffer, position, detlAmtNotCovered.copy());
		position += Len.DETL_AMT_NOT_COVERED;
		MarshalByte.writeDecimal(buffer, position, detlAmtCoinsurance.copy());
		position += Len.DETL_AMT_COINSURANCE;
		MarshalByte.writeDecimal(buffer, position, detlAmtDeductible.copy());
		position += Len.DETL_AMT_DEDUCTIBLE;
		MarshalByte.writeDecimal(buffer, position, detlAmtCopay.copy());
		position += Len.DETL_AMT_COPAY;
		MarshalByte.writeDecimal(buffer, position, detlAmtPatOwes.copy());
		position += Len.DETL_AMT_PAT_OWES;
		MarshalByte.writeDecimal(buffer, position, detlAmtPaidWithoutInt.copy());
		position += Len.DETL_AMT_PAID_WITHOUT_INT;
		MarshalByte.writeDecimal(buffer, position, detlBegBalanceForMsg.copy());
		position += Len.DETL_BEG_BALANCE_FOR_MSG;
		MarshalByte.writeDecimal(buffer, position, detlAmtInterestPd.copy());
		position += Len.DETL_AMT_INTEREST_PD;
		MarshalByte.writeString(buffer, position, detlBegOrEnd, Len.DETL_BEG_OR_END);
		position += Len.DETL_BEG_OR_END;
		MarshalByte.writeString(buffer, position, detlRxNum, Len.DETL_RX_NUM);
		position += Len.DETL_RX_NUM;
		MarshalByte.writeString(buffer, position, detlTeam, Len.DETL_TEAM);
		position += Len.DETL_TEAM;
		MarshalByte.writeString(buffer, position, detlId, Len.DETL_ID);
		position += Len.DETL_ID;
		MarshalByte.writeString(buffer, position, detlTthSrfcCd, Len.DETL_TTH_SRFC_CD);
		position += Len.DETL_TTH_SRFC_CD;
		MarshalByte.writeString(buffer, position, detlTthId, Len.DETL_TTH_ID);
		position += Len.DETL_TTH_ID;
		MarshalByte.writeString(buffer, position, detlOpPrcProcCd, Len.DETL_OP_PRC_PROC_CD);
		position += Len.DETL_OP_PRC_PROC_CD;
		detlReceiptDate.getDetlServDateBytes(buffer, position);
		position += RemDetlServDate.Len.DETL_SERV_DATE;
		MarshalByte.writeString(buffer, position, detlClassOfCntr1, Len.DETL_CLASS_OF_CNTR1);
		position += Len.DETL_CLASS_OF_CNTR1;
		MarshalByte.writeString(buffer, position, detlClassOfCntr2, Len.DETL_CLASS_OF_CNTR2);
		position += Len.DETL_CLASS_OF_CNTR2;
		detlAdjustRsnCds.getDetlAdjustRsnCdsBytes(buffer, position);
		position += RemDetlAdjustRsnCds.Len.DETL_ADJUST_RSN_CDS;
		MarshalByte.writeString(buffer, position, detlRemarks1, Len.DETL_REMARKS1);
		position += Len.DETL_REMARKS1;
		MarshalByte.writeString(buffer, position, detlRemarks2, Len.DETL_REMARKS2);
		position += Len.DETL_REMARKS2;
		MarshalByte.writeString(buffer, position, detlDenialRemarkCode, Len.DETL_DENIAL_REMARK_CODE);
		position += Len.DETL_DENIAL_REMARK_CODE;
		MarshalByte.writeString(buffer, position, detlAdjustReasonCode, Len.DETL_ADJUST_REASON_CODE);
		position += Len.DETL_ADJUST_REASON_CODE;
		MarshalByte.writeChar(buffer, position, detlAdjustRespCode);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, detlAdjustTypeCode);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tapeNpiPerfPvdrNum, Len.TAPE_NPI_PERF_PVDR_NUM);
		position += Len.TAPE_NPI_PERF_PVDR_NUM;
		MarshalByte.writeString(buffer, position, tapeLocalPerfPvdrNum, Len.TAPE_LOCAL_PERF_PVDR_NUM);
		position += Len.TAPE_LOCAL_PERF_PVDR_NUM;
		MarshalByte.writeString(buffer, position, tapeNetwork, Len.TAPE_NETWORK);
		position += Len.TAPE_NETWORK;
		getPendMessageBytes(buffer, position);
		return buffer;
	}

	public void setDetlAllowedAmt(AfDecimal detlAllowedAmt) {
		this.detlAllowedAmt.assign(detlAllowedAmt);
	}

	public AfDecimal getDetlAllowedAmt() {
		return this.detlAllowedAmt.copy();
	}

	public void setDetlPvdrWriteoff(AfDecimal detlPvdrWriteoff) {
		this.detlPvdrWriteoff.assign(detlPvdrWriteoff);
	}

	public AfDecimal getDetlPvdrWriteoff() {
		return this.detlPvdrWriteoff.copy();
	}

	public void setDetlAmtOtherAdj(AfDecimal detlAmtOtherAdj) {
		this.detlAmtOtherAdj.assign(detlAmtOtherAdj);
	}

	public AfDecimal getDetlAmtOtherAdj() {
		return this.detlAmtOtherAdj.copy();
	}

	public void setDetlAmtNotCovered(AfDecimal detlAmtNotCovered) {
		this.detlAmtNotCovered.assign(detlAmtNotCovered);
	}

	public AfDecimal getDetlAmtNotCovered() {
		return this.detlAmtNotCovered.copy();
	}

	public void setDetlAmtCoinsurance(AfDecimal detlAmtCoinsurance) {
		this.detlAmtCoinsurance.assign(detlAmtCoinsurance);
	}

	public AfDecimal getDetlAmtCoinsurance() {
		return this.detlAmtCoinsurance.copy();
	}

	public void setDetlAmtDeductible(AfDecimal detlAmtDeductible) {
		this.detlAmtDeductible.assign(detlAmtDeductible);
	}

	public AfDecimal getDetlAmtDeductible() {
		return this.detlAmtDeductible.copy();
	}

	public void setDetlAmtCopay(AfDecimal detlAmtCopay) {
		this.detlAmtCopay.assign(detlAmtCopay);
	}

	public AfDecimal getDetlAmtCopay() {
		return this.detlAmtCopay.copy();
	}

	public void setDetlAmtPatOwes(AfDecimal detlAmtPatOwes) {
		this.detlAmtPatOwes.assign(detlAmtPatOwes);
	}

	public AfDecimal getDetlAmtPatOwes() {
		return this.detlAmtPatOwes.copy();
	}

	public void setDetlAmtPaidWithoutInt(AfDecimal detlAmtPaidWithoutInt) {
		this.detlAmtPaidWithoutInt.assign(detlAmtPaidWithoutInt);
	}

	public AfDecimal getDetlAmtPaidWithoutInt() {
		return this.detlAmtPaidWithoutInt.copy();
	}

	public void setDetlBegBalanceForMsg(AfDecimal detlBegBalanceForMsg) {
		this.detlBegBalanceForMsg.assign(detlBegBalanceForMsg);
	}

	public AfDecimal getDetlBegBalanceForMsg() {
		return this.detlBegBalanceForMsg.copy();
	}

	public void setDetlAmtInterestPd(AfDecimal detlAmtInterestPd) {
		this.detlAmtInterestPd.assign(detlAmtInterestPd);
	}

	public AfDecimal getDetlAmtInterestPd() {
		return this.detlAmtInterestPd.copy();
	}

	public void setDetlBegOrEnd(String detlBegOrEnd) {
		this.detlBegOrEnd = Functions.subString(detlBegOrEnd, Len.DETL_BEG_OR_END);
	}

	public String getDetlBegOrEnd() {
		return this.detlBegOrEnd;
	}

	public void setDetlRxNum(String detlRxNum) {
		this.detlRxNum = Functions.subString(detlRxNum, Len.DETL_RX_NUM);
	}

	public String getDetlRxNum() {
		return this.detlRxNum;
	}

	public void setDetlTeam(String detlTeam) {
		this.detlTeam = Functions.subString(detlTeam, Len.DETL_TEAM);
	}

	public String getDetlTeam() {
		return this.detlTeam;
	}

	public void setDetlId(String detlId) {
		this.detlId = Functions.subString(detlId, Len.DETL_ID);
	}

	public String getDetlId() {
		return this.detlId;
	}

	public void setDetlTthSrfcCd(String detlTthSrfcCd) {
		this.detlTthSrfcCd = Functions.subString(detlTthSrfcCd, Len.DETL_TTH_SRFC_CD);
	}

	public String getDetlTthSrfcCd() {
		return this.detlTthSrfcCd;
	}

	public void setDetlTthId(String detlTthId) {
		this.detlTthId = Functions.subString(detlTthId, Len.DETL_TTH_ID);
	}

	public String getDetlTthId() {
		return this.detlTthId;
	}

	public void setDetlOpPrcProcCd(String detlOpPrcProcCd) {
		this.detlOpPrcProcCd = Functions.subString(detlOpPrcProcCd, Len.DETL_OP_PRC_PROC_CD);
	}

	public String getDetlOpPrcProcCd() {
		return this.detlOpPrcProcCd;
	}

	public void setDetlClassOfCntr1(String detlClassOfCntr1) {
		this.detlClassOfCntr1 = Functions.subString(detlClassOfCntr1, Len.DETL_CLASS_OF_CNTR1);
	}

	public String getDetlClassOfCntr1() {
		return this.detlClassOfCntr1;
	}

	public void setDetlClassOfCntr2(String detlClassOfCntr2) {
		this.detlClassOfCntr2 = Functions.subString(detlClassOfCntr2, Len.DETL_CLASS_OF_CNTR2);
	}

	public String getDetlClassOfCntr2() {
		return this.detlClassOfCntr2;
	}

	public void setDetlRemarks1(String detlRemarks1) {
		this.detlRemarks1 = Functions.subString(detlRemarks1, Len.DETL_REMARKS1);
	}

	public String getDetlRemarks1() {
		return this.detlRemarks1;
	}

	public void setDetlRemarks2(String detlRemarks2) {
		this.detlRemarks2 = Functions.subString(detlRemarks2, Len.DETL_REMARKS2);
	}

	public String getDetlRemarks2() {
		return this.detlRemarks2;
	}

	public void setDetlDenialRemarkCode(String detlDenialRemarkCode) {
		this.detlDenialRemarkCode = Functions.subString(detlDenialRemarkCode, Len.DETL_DENIAL_REMARK_CODE);
	}

	public String getDetlDenialRemarkCode() {
		return this.detlDenialRemarkCode;
	}

	public void setDetlAdjustReasonCode(String detlAdjustReasonCode) {
		this.detlAdjustReasonCode = Functions.subString(detlAdjustReasonCode, Len.DETL_ADJUST_REASON_CODE);
	}

	public String getDetlAdjustReasonCode() {
		return this.detlAdjustReasonCode;
	}

	public void setDetlAdjustRespCode(char detlAdjustRespCode) {
		this.detlAdjustRespCode = detlAdjustRespCode;
	}

	public char getDetlAdjustRespCode() {
		return this.detlAdjustRespCode;
	}

	public void setDetlAdjustTypeCode(char detlAdjustTypeCode) {
		this.detlAdjustTypeCode = detlAdjustTypeCode;
	}

	public char getDetlAdjustTypeCode() {
		return this.detlAdjustTypeCode;
	}

	public void setTapeNpiPerfPvdrNum(String tapeNpiPerfPvdrNum) {
		this.tapeNpiPerfPvdrNum = Functions.subString(tapeNpiPerfPvdrNum, Len.TAPE_NPI_PERF_PVDR_NUM);
	}

	public String getTapeNpiPerfPvdrNum() {
		return this.tapeNpiPerfPvdrNum;
	}

	public void setTapeLocalPerfPvdrNum(String tapeLocalPerfPvdrNum) {
		this.tapeLocalPerfPvdrNum = Functions.subString(tapeLocalPerfPvdrNum, Len.TAPE_LOCAL_PERF_PVDR_NUM);
	}

	public String getTapeLocalPerfPvdrNum() {
		return this.tapeLocalPerfPvdrNum;
	}

	public void setTapeNetwork(String tapeNetwork) {
		this.tapeNetwork = Functions.subString(tapeNetwork, Len.TAPE_NETWORK);
	}

	public String getTapeNetwork() {
		return this.tapeNetwork;
	}

	public void setPendMessageBytes(byte[] buffer, int offset) {
		int position = offset;
		pendMessage117 = MarshalByte.readString(buffer, position, Len.PEND_MESSAGE117);
		position += Len.PEND_MESSAGE117;
		pendMessage18100 = MarshalByte.readString(buffer, position, Len.PEND_MESSAGE18100);
	}

	public byte[] getPendMessageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, pendMessage117, Len.PEND_MESSAGE117);
		position += Len.PEND_MESSAGE117;
		MarshalByte.writeString(buffer, position, pendMessage18100, Len.PEND_MESSAGE18100);
		return buffer;
	}

	public void setPendMessage117(String pendMessage117) {
		this.pendMessage117 = Functions.subString(pendMessage117, Len.PEND_MESSAGE117);
	}

	public String getPendMessage117() {
		return this.pendMessage117;
	}

	public void setPendMessage18100(String pendMessage18100) {
		this.pendMessage18100 = Functions.subString(pendMessage18100, Len.PEND_MESSAGE18100);
	}

	public String getPendMessage18100() {
		return this.pendMessage18100;
	}

	public RemDetlAdjustRsnCds getDetlAdjustRsnCds() {
		return detlAdjustRsnCds;
	}

	public RemDetlServDate getDetlReceiptDate() {
		return detlReceiptDate;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DETL_ALLOWED_AMT = 9;
		public static final int DETL_PVDR_WRITEOFF = 9;
		public static final int DETL_AMT_OTHER_ADJ = 9;
		public static final int DETL_AMT_NOT_COVERED = 9;
		public static final int DETL_AMT_COINSURANCE = 9;
		public static final int DETL_AMT_DEDUCTIBLE = 9;
		public static final int DETL_AMT_COPAY = 9;
		public static final int DETL_AMT_PAT_OWES = 9;
		public static final int DETL_AMT_PAID_WITHOUT_INT = 9;
		public static final int DETL_BEG_BALANCE_FOR_MSG = 9;
		public static final int DETL_AMT_INTEREST_PD = 9;
		public static final int DETL_BEG_OR_END = 3;
		public static final int DETL_RX_NUM = 7;
		public static final int DETL_TEAM = 5;
		public static final int DETL_ID = 3;
		public static final int DETL_TTH_SRFC_CD = 5;
		public static final int DETL_TTH_ID = 2;
		public static final int DETL_OP_PRC_PROC_CD = 5;
		public static final int DETL_CLASS_OF_CNTR1 = 2;
		public static final int DETL_CLASS_OF_CNTR2 = 2;
		public static final int DETL_REMARKS1 = 5;
		public static final int DETL_REMARKS2 = 5;
		public static final int DETL_DENIAL_REMARK_CODE = 2;
		public static final int DETL_ADJUST_REASON_CODE = 2;
		public static final int DETL_ADJUST_RESP_CODE = 1;
		public static final int DETL_ADJUST_TYPE_CODE = 1;
		public static final int TAPE_NPI_PERF_PVDR_NUM = 10;
		public static final int TAPE_LOCAL_PERF_PVDR_NUM = 10;
		public static final int TAPE_NETWORK = 5;
		public static final int PEND_MESSAGE117 = 17;
		public static final int PEND_MESSAGE18100 = 83;
		public static final int PEND_MESSAGE = PEND_MESSAGE117 + PEND_MESSAGE18100;
		public static final int DETL_CONTINUED = DETL_ALLOWED_AMT + DETL_PVDR_WRITEOFF + DETL_AMT_OTHER_ADJ + DETL_AMT_NOT_COVERED
				+ DETL_AMT_COINSURANCE + DETL_AMT_DEDUCTIBLE + DETL_AMT_COPAY + DETL_AMT_PAT_OWES + DETL_AMT_PAID_WITHOUT_INT
				+ DETL_BEG_BALANCE_FOR_MSG + DETL_AMT_INTEREST_PD + DETL_BEG_OR_END + DETL_RX_NUM + DETL_TEAM + DETL_ID + DETL_TTH_SRFC_CD
				+ DETL_TTH_ID + DETL_OP_PRC_PROC_CD + RemDetlServDate.Len.DETL_SERV_DATE + DETL_CLASS_OF_CNTR1 + DETL_CLASS_OF_CNTR2
				+ RemDetlAdjustRsnCds.Len.DETL_ADJUST_RSN_CDS + DETL_REMARKS1 + DETL_REMARKS2 + DETL_DENIAL_REMARK_CODE + DETL_ADJUST_REASON_CODE
				+ DETL_ADJUST_RESP_CODE + DETL_ADJUST_TYPE_CODE + TAPE_NPI_PERF_PVDR_NUM + TAPE_LOCAL_PERF_PVDR_NUM + TAPE_NETWORK + PEND_MESSAGE;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int DETL_ALLOWED_AMT = 7;
			public static final int DETL_PVDR_WRITEOFF = 7;
			public static final int DETL_AMT_OTHER_ADJ = 7;
			public static final int DETL_AMT_NOT_COVERED = 7;
			public static final int DETL_AMT_COINSURANCE = 7;
			public static final int DETL_AMT_DEDUCTIBLE = 7;
			public static final int DETL_AMT_COPAY = 7;
			public static final int DETL_AMT_PAT_OWES = 7;
			public static final int DETL_AMT_PAID_WITHOUT_INT = 7;
			public static final int DETL_BEG_BALANCE_FOR_MSG = 7;
			public static final int DETL_AMT_INTEREST_PD = 7;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int DETL_ALLOWED_AMT = 2;
			public static final int DETL_PVDR_WRITEOFF = 2;
			public static final int DETL_AMT_OTHER_ADJ = 2;
			public static final int DETL_AMT_NOT_COVERED = 2;
			public static final int DETL_AMT_COINSURANCE = 2;
			public static final int DETL_AMT_DEDUCTIBLE = 2;
			public static final int DETL_AMT_COPAY = 2;
			public static final int DETL_AMT_PAT_OWES = 2;
			public static final int DETL_AMT_PAID_WITHOUT_INT = 2;
			public static final int DETL_BEG_BALANCE_FOR_MSG = 2;
			public static final int DETL_AMT_INTEREST_PD = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
