/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.data.fao;

import com.bphx.ctu.af.io.file.AbstractInputOutputDAO;
import com.bphx.ctu.af.io.file.FileAccessStatus;
import com.myorg.myprj.data.fto.TriggerInTO;

/**Original name: TRIGGER-IN/TRIGERIN/TRIGERIN[NF0533ML]<br>*/
public class TriggerInDAO extends AbstractInputOutputDAO<TriggerInTO> {

	//==== PROPERTIES ====
	public static final int RECORD_SIZE = 400;

	//==== CONSTRUCTORS ====
	public TriggerInDAO(FileAccessStatus fileStatus) {
		super(fileStatus);
	}

	//==== METHODS ====
	@Override
	public String getName() {
		return "TRIGERIN";
	}

	@Override
	public int getRecordSize() {
		return RECORD_SIZE;
	}

	@Override
	public TriggerInTO createTo() {
		return new TriggerInTO();
	}

	@Override
	public TriggerInTO read() {
		TriggerInTO triggerInTO = new TriggerInTO();
		return read(triggerInTO);
	}
}
