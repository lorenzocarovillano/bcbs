/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: FORMATTED-TIME<br>
 * Variable: FORMATTED-TIME from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class FormattedTime {

	//==== PROPERTIES ====
	//Original name: FORMAT-HH
	private String formatHh = DefaultValues.stringVal(Len.FORMAT_HH);
	//Original name: SEPARATOR-1
	private char separator1 = ':';
	//Original name: FORMAT-MM
	private String formatMm = DefaultValues.stringVal(Len.FORMAT_MM);
	//Original name: SEPARATOR-2
	private char separator2 = ':';
	//Original name: FORMAT-SS
	private String formatSs = DefaultValues.stringVal(Len.FORMAT_SS);

	//==== METHODS ====
	public String getFormattedTimeFormatted() {
		return MarshalByteExt.bufferToStr(getFormattedTimeBytes());
	}

	public byte[] getFormattedTimeBytes() {
		byte[] buffer = new byte[Len.FORMATTED_TIME];
		return getFormattedTimeBytes(buffer, 1);
	}

	public byte[] getFormattedTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, formatHh, Len.FORMAT_HH);
		position += Len.FORMAT_HH;
		MarshalByte.writeChar(buffer, position, separator1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, formatMm, Len.FORMAT_MM);
		position += Len.FORMAT_MM;
		MarshalByte.writeChar(buffer, position, separator2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, formatSs, Len.FORMAT_SS);
		return buffer;
	}

	public void setFormatHhFormatted(String formatHh) {
		this.formatHh = Trunc.toUnsignedNumeric(formatHh, Len.FORMAT_HH);
	}

	public short getFormatHh() {
		return NumericDisplay.asShort(this.formatHh);
	}

	public char getSeparator1() {
		return this.separator1;
	}

	public void setFormatMmFormatted(String formatMm) {
		this.formatMm = Trunc.toUnsignedNumeric(formatMm, Len.FORMAT_MM);
	}

	public short getFormatMm() {
		return NumericDisplay.asShort(this.formatMm);
	}

	public char getSeparator2() {
		return this.separator2;
	}

	public void setFormatSsFormatted(String formatSs) {
		this.formatSs = Trunc.toUnsignedNumeric(formatSs, Len.FORMAT_SS);
	}

	public short getFormatSs() {
		return NumericDisplay.asShort(this.formatSs);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FORMAT_HH = 2;
		public static final int FORMAT_MM = 2;
		public static final int FORMAT_SS = 2;
		public static final int SEPARATOR1 = 1;
		public static final int SEPARATOR2 = 1;
		public static final int FORMATTED_TIME = FORMAT_HH + SEPARATOR1 + FORMAT_MM + SEPARATOR2 + FORMAT_SS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
