/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: CSR-ASG-CD<br>
 * Variable: CSR-ASG-CD from copybook NF07AREA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CsrAsgCd {

	//==== PROPERTIES ====
	private String value = "";
	public static final String PROV = "Y1";
	public static final String MEMBER = "N1";
	public static final String PAY3RD_PARTY = "N2";

	//==== METHODS ====
	public void setAsgCd(String asgCd) {
		this.value = Functions.subString(asgCd, Len.ASG_CD);
	}

	public String getAsgCd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ASG_CD = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
