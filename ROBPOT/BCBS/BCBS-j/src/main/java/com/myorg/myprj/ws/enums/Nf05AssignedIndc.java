/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: NF05-ASSIGNED-INDC<br>
 * Variable: NF05-ASSIGNED-INDC from copybook NF05EOB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Nf05AssignedIndc {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.NF05_ASSIGNED_INDC);
	public static final String CONTRACTING_PVDR = "Y1";
	public static final String SUBSCRIBER = "N1";
	public static final String THIRD_PARTY = "N2";

	//==== METHODS ====
	public void setNf05AssignedIndc(String nf05AssignedIndc) {
		this.value = Functions.subString(nf05AssignedIndc, Len.NF05_ASSIGNED_INDC);
	}

	public String getNf05AssignedIndc() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NF05_ASSIGNED_INDC = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
