/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: NF05-CROSS-REIMBURSE-INDC<br>
 * Variable: NF05-CROSS-REIMBURSE-INDC from copybook NF05EOB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Nf05CrossReimburseIndc {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.NF05_CROSS_REIMBURSE_INDC);
	public static final String CPR = "CP";
	public static final String CCR = "CC";
	public static final String CPLS = "CS";
	public static final String CHC = "CH";
	public static final String PL65 = "PL";
	public static final String SWCC = "SW";
	public static final String WESLEY = "WR";

	//==== METHODS ====
	public void setNf05CrossReimburseIndc(String nf05CrossReimburseIndc) {
		this.value = Functions.subString(nf05CrossReimburseIndc, Len.NF05_CROSS_REIMBURSE_INDC);
	}

	public String getNf05CrossReimburseIndc() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NF05_CROSS_REIMBURSE_INDC = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
