/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.enums.RestartIndicator;

/**Original name: WS-RESTART-STUFF<br>
 * Variable: WS-RESTART-STUFF from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsRestartStuff {

	//==== PROPERTIES ====
	//Original name: WS-RESTART-JOB-PARMS
	private WsRestartJobParms wsRestartJobParms = new WsRestartJobParms();
	//Original name: WS-RESTART-JOB-SEQ-PACKED
	private short wsRestartJobSeqPacked = ((short) 0);
	//Original name: RESTART-INDICATOR
	private RestartIndicator restartIndicator = new RestartIndicator();
	//Original name: RESTART-CLAIM-ID
	private String restartClaimId = DefaultValues.stringVal(Len.RESTART_CLAIM_ID);
	//Original name: RESTART-CLAIM-SFX
	private char restartClaimSfx = DefaultValues.CHAR_VAL;
	//Original name: RESTART-PMT-ID
	private String restartPmtId = DefaultValues.stringVal(Len.RESTART_PMT_ID);

	//==== METHODS ====
	public void setWsRestartJobSeqPacked(short wsRestartJobSeqPacked) {
		this.wsRestartJobSeqPacked = wsRestartJobSeqPacked;
	}

	public short getWsRestartJobSeqPacked() {
		return this.wsRestartJobSeqPacked;
	}

	public void setWsRestartTxt1Formatted(String data) {
		byte[] buffer = new byte[Len.WS_RESTART_TXT1];
		MarshalByte.writeString(buffer, 1, data, Len.WS_RESTART_TXT1);
		setWsRestartTxt1Bytes(buffer, 1);
	}

	public String getWsRestartTxt1Formatted() {
		return MarshalByteExt.bufferToStr(getWsRestartTxt1Bytes());
	}

	/**Original name: WS-RESTART-TXT1<br>*/
	public byte[] getWsRestartTxt1Bytes() {
		byte[] buffer = new byte[Len.WS_RESTART_TXT1];
		return getWsRestartTxt1Bytes(buffer, 1);
	}

	public void setWsRestartTxt1Bytes(byte[] buffer, int offset) {
		int position = offset;
		restartIndicator.setRestartIndicator(MarshalByte.readString(buffer, position, RestartIndicator.Len.RESTART_INDICATOR));
		position += RestartIndicator.Len.RESTART_INDICATOR;
		setRestartKeyBytes(buffer, position);
	}

	public byte[] getWsRestartTxt1Bytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, restartIndicator.getRestartIndicator(), RestartIndicator.Len.RESTART_INDICATOR);
		position += RestartIndicator.Len.RESTART_INDICATOR;
		getRestartKeyBytes(buffer, position);
		return buffer;
	}

	public String getRestartKeyFormatted() {
		return MarshalByteExt.bufferToStr(getRestartKeyBytes());
	}

	public void setRestartKeyBytes(byte[] buffer) {
		setRestartKeyBytes(buffer, 1);
	}

	/**Original name: RESTART-KEY<br>*/
	public byte[] getRestartKeyBytes() {
		byte[] buffer = new byte[Len.RESTART_KEY];
		return getRestartKeyBytes(buffer, 1);
	}

	public void setRestartKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		restartClaimId = MarshalByte.readString(buffer, position, Len.RESTART_CLAIM_ID);
		position += Len.RESTART_CLAIM_ID;
		restartClaimSfx = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		restartPmtId = MarshalByte.readFixedString(buffer, position, Len.RESTART_PMT_ID);
	}

	public byte[] getRestartKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, restartClaimId, Len.RESTART_CLAIM_ID);
		position += Len.RESTART_CLAIM_ID;
		MarshalByte.writeChar(buffer, position, restartClaimSfx);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, restartPmtId, Len.RESTART_PMT_ID);
		return buffer;
	}

	public void setRestartClaimId(String restartClaimId) {
		this.restartClaimId = Functions.subString(restartClaimId, Len.RESTART_CLAIM_ID);
	}

	public String getRestartClaimId() {
		return this.restartClaimId;
	}

	public String getRestartClaimIdFormatted() {
		return Functions.padBlanks(getRestartClaimId(), Len.RESTART_CLAIM_ID);
	}

	public void setRestartClaimSfx(char restartClaimSfx) {
		this.restartClaimSfx = restartClaimSfx;
	}

	public char getRestartClaimSfx() {
		return this.restartClaimSfx;
	}

	public String getRestartPmtIdFormatted() {
		return this.restartPmtId;
	}

	public String getRestartPmtIdAsString() {
		return getRestartPmtIdFormatted();
	}

	public RestartIndicator getRestartIndicator() {
		return restartIndicator;
	}

	public WsRestartJobParms getWsRestartJobParms() {
		return wsRestartJobParms;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RESTART_CLAIM_ID = 12;
		public static final int RESTART_PMT_ID = 2;
		public static final int RESTART_CLAIM_SFX = 1;
		public static final int RESTART_KEY = RESTART_CLAIM_ID + RESTART_CLAIM_SFX + RESTART_PMT_ID;
		public static final int WS_RESTART_TXT1 = RestartIndicator.Len.RESTART_INDICATOR + RESTART_KEY;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
