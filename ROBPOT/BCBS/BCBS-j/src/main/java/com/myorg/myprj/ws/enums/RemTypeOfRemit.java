/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: REM-TYPE-OF-REMIT<br>
 * Variable: REM-TYPE-OF-REMIT from copybook NF07RMIT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class RemTypeOfRemit {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.REM_TYPE_OF_REMIT);
	public static final String PAY_PROVIDER = "PAYP";
	public static final String PEND_SUSPEND = "PEND";
	public static final String WRITE_OFF = "WOFF";

	//==== METHODS ====
	public void setRemTypeOfRemit(String remTypeOfRemit) {
		this.value = Functions.subString(remTypeOfRemit, Len.REM_TYPE_OF_REMIT);
	}

	public String getRemTypeOfRemit() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REM_TYPE_OF_REMIT = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
