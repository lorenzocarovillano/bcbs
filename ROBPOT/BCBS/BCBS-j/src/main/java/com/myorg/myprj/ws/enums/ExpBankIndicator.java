/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: EXP-BANK-INDICATOR<br>
 * Variable: EXP-BANK-INDICATOR from copybook NF06EXP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class ExpBankIndicator {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char BANKIV = 'B';
	public static final char FEP = 'F';

	//==== METHODS ====
	public void setExpBankIndicator(char expBankIndicator) {
		this.value = expBankIndicator;
	}

	public char getExpBankIndicator() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EXP_BANK_INDICATOR = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
