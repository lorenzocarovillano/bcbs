/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: CSR-RECORD-TYPE<br>
 * Variable: CSR-RECORD-TYPE from copybook NF07AREA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CsrRecordType {

	//==== PROPERTIES ====
	private String value = "";
	public static final String ORIGINAL_PAYMENT = "ORIG";
	public static final String REPAY = "REPAY";
	public static final String VOID_FLD = "VOID";

	//==== METHODS ====
	public void setRecordType(String recordType) {
		this.value = Functions.subString(recordType, Len.RECORD_TYPE);
	}

	public String getRecordType() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RECORD_TYPE = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
