/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TRIG-RECORD-LAYOUT<br>
 * Variable: TRIG-RECORD-LAYOUT from program NF0735ML<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TrigRecordLayout extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final String TRIG_BLUE_CROSS = "BCS";
	public static final String TRIG_FEP = "FEP";
	public static final String TRIG_KSS = "KSS";
	public static final char TRIG_BILL_PROV_CROSS = '1';
	public static final char TRIG_BILL_PROV_SHIELD = '3';
	public static final char TRIG_PERF_PROV_CROSS = '1';
	public static final char TRIG_PERF_PROV_SHIELD = '3';
	public static final String TRIG_PAY_PROV = "Y1";
	public static final String TRIG_PAY_MEMBER = "N1";
	public static final String TRIG_PAY3RD_PARTY = "N2";
	public static final char TRIG_CLAIM_HOST = 'S';
	public static final char TRIG_CLAIM_HOME = 'M';
	public static final char TRIG_CLAIM_BCBSK = 'K';
	public static final char TRIG_PRPAY_OVRD_BY_PROCESSOR = 'P';
	public static final char TRIG_PRPAY_OVRD_BY_ONLINE = 'O';
	public static final char TRIG_PRPAY_OVRD_BY_BATCH = 'B';
	public static final char TRIG_PRPAY_OVRD_BY_CONVERSION = 'C';
	public static final String TRIG_GMIS_CLAIM = "GMIS";
	public static final String TRIG_NOT_A_GMIS_CLAIM = "NOTGMIS";

	//==== CONSTRUCTORS ====
	public TrigRecordLayout() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.TRIG_RECORD_LAYOUT;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "", Len.TRIG_RECORD_LAYOUT);
	}

	public void setTrigRecordLayoutFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.TRIG_RECORD_LAYOUT, Pos.TRIG_RECORD_LAYOUT);
	}

	public void setTrigRecordLayoutFromBuffer(byte[] buffer) {
		setTrigRecordLayoutFromBuffer(buffer, 1);
	}

	/**Original name: TRIG-HEADER-REC<br>*/
	public byte[] getTrigHeaderRecBytes() {
		byte[] buffer = new byte[Len.TRIG_HEADER_REC];
		return getTrigHeaderRecBytes(buffer, 1);
	}

	public byte[] getTrigHeaderRecBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.TRIG_HEADER_REC, Pos.TRIG_HEADER_REC);
		return buffer;
	}

	public String getTrigBusDateFormatted() {
		return readFixedString(Pos.TRIG_BUS_DATE, Len.TRIG_BUS_DATE);
	}

	/**Original name: TRIG-PIP-FLAG<br>*/
	public char getTrigPipFlag() {
		return readChar(Pos.TRIG_PIP_FLAG);
	}

	/**Original name: TRIG-EOM-FLAG<br>*/
	public char getTrigEomFlag() {
		return readChar(Pos.TRIG_EOM_FLAG);
	}

	public void setTrigProdInd(String trigProdInd) {
		writeString(Pos.TRIG_PROD_IND, trigProdInd, Len.TRIG_PROD_IND);
	}

	/**Original name: TRIG-PROD-IND<br>*/
	public String getTrigProdInd() {
		return readString(Pos.TRIG_PROD_IND, Len.TRIG_PROD_IND);
	}

	public void setTrigHistBillProvNpiId(String trigHistBillProvNpiId) {
		writeString(Pos.TRIG_HIST_BILL_PROV_NPI_ID, trigHistBillProvNpiId, Len.TRIG_HIST_BILL_PROV_NPI_ID);
	}

	/**Original name: TRIG-HIST-BILL-PROV-NPI-ID<br>*/
	public String getTrigHistBillProvNpiId() {
		return readString(Pos.TRIG_HIST_BILL_PROV_NPI_ID, Len.TRIG_HIST_BILL_PROV_NPI_ID);
	}

	public void setTrigLocalBillProvLobCd(char trigLocalBillProvLobCd) {
		writeChar(Pos.TRIG_LOCAL_BILL_PROV_LOB_CD, trigLocalBillProvLobCd);
	}

	/**Original name: TRIG-LOCAL-BILL-PROV-LOB-CD<br>*/
	public char getTrigLocalBillProvLobCd() {
		return readChar(Pos.TRIG_LOCAL_BILL_PROV_LOB_CD);
	}

	public void setTrigLocalBillProvId(String trigLocalBillProvId) {
		writeString(Pos.TRIG_LOCAL_BILL_PROV_ID, trigLocalBillProvId, Len.TRIG_LOCAL_BILL_PROV_ID);
	}

	/**Original name: TRIG-LOCAL-BILL-PROV-ID<br>*/
	public String getTrigLocalBillProvId() {
		return readString(Pos.TRIG_LOCAL_BILL_PROV_ID, Len.TRIG_LOCAL_BILL_PROV_ID);
	}

	public void setTrigBillProvIdQlfCd(String trigBillProvIdQlfCd) {
		writeString(Pos.TRIG_BILL_PROV_ID_QLF_CD, trigBillProvIdQlfCd, Len.TRIG_BILL_PROV_ID_QLF_CD);
	}

	/**Original name: TRIG-BILL-PROV-ID-QLF-CD<br>*/
	public String getTrigBillProvIdQlfCd() {
		return readString(Pos.TRIG_BILL_PROV_ID_QLF_CD, Len.TRIG_BILL_PROV_ID_QLF_CD);
	}

	public void setTrigBillProvSpecCd(String trigBillProvSpecCd) {
		writeString(Pos.TRIG_BILL_PROV_SPEC_CD, trigBillProvSpecCd, Len.TRIG_BILL_PROV_SPEC_CD);
	}

	/**Original name: TRIG-BILL-PROV-SPEC-CD<br>*/
	public String getTrigBillProvSpecCd() {
		return readString(Pos.TRIG_BILL_PROV_SPEC_CD, Len.TRIG_BILL_PROV_SPEC_CD);
	}

	public void setTrigClmCntlId(String trigClmCntlId) {
		writeString(Pos.TRIG_CLM_CNTL_ID, trigClmCntlId, Len.TRIG_CLM_CNTL_ID);
	}

	/**Original name: TRIG-CLM-CNTL-ID<br>*/
	public String getTrigClmCntlId() {
		return readString(Pos.TRIG_CLM_CNTL_ID, Len.TRIG_CLM_CNTL_ID);
	}

	public void setTrigClmCntlSfx(char trigClmCntlSfx) {
		writeChar(Pos.TRIG_CLM_CNTL_SFX, trigClmCntlSfx);
	}

	/**Original name: TRIG-CLM-CNTL-SFX<br>*/
	public char getTrigClmCntlSfx() {
		return readChar(Pos.TRIG_CLM_CNTL_SFX);
	}

	/**Original name: TRIG-CLM-PMT-ID<br>*/
	public short getTrigClmPmtId() {
		return readNumDispUnsignedShort(Pos.TRIG_CLM_PMT_ID, Len.TRIG_CLM_PMT_ID);
	}

	public void setTrigHistPerfProvNpiId(String trigHistPerfProvNpiId) {
		writeString(Pos.TRIG_HIST_PERF_PROV_NPI_ID, trigHistPerfProvNpiId, Len.TRIG_HIST_PERF_PROV_NPI_ID);
	}

	/**Original name: TRIG-HIST-PERF-PROV-NPI-ID<br>*/
	public String getTrigHistPerfProvNpiId() {
		return readString(Pos.TRIG_HIST_PERF_PROV_NPI_ID, Len.TRIG_HIST_PERF_PROV_NPI_ID);
	}

	public void setTrigLocalPerfProvLobCd(char trigLocalPerfProvLobCd) {
		writeChar(Pos.TRIG_LOCAL_PERF_PROV_LOB_CD, trigLocalPerfProvLobCd);
	}

	/**Original name: TRIG-LOCAL-PERF-PROV-LOB-CD<br>*/
	public char getTrigLocalPerfProvLobCd() {
		return readChar(Pos.TRIG_LOCAL_PERF_PROV_LOB_CD);
	}

	public void setTrigLocalPerfProvId(String trigLocalPerfProvId) {
		writeString(Pos.TRIG_LOCAL_PERF_PROV_ID, trigLocalPerfProvId, Len.TRIG_LOCAL_PERF_PROV_ID);
	}

	/**Original name: TRIG-LOCAL-PERF-PROV-ID<br>*/
	public String getTrigLocalPerfProvId() {
		return readString(Pos.TRIG_LOCAL_PERF_PROV_ID, Len.TRIG_LOCAL_PERF_PROV_ID);
	}

	public void setTrigInsId(String trigInsId) {
		writeString(Pos.TRIG_INS_ID, trigInsId, Len.TRIG_INS_ID);
	}

	/**Original name: TRIG-INS-ID<br>*/
	public String getTrigInsId() {
		return readString(Pos.TRIG_INS_ID, Len.TRIG_INS_ID);
	}

	public void setTrigMemberId(String trigMemberId) {
		writeString(Pos.TRIG_MEMBER_ID, trigMemberId, Len.TRIG_MEMBER_ID);
	}

	/**Original name: TRIG-MEMBER-ID<br>*/
	public String getTrigMemberId() {
		return readString(Pos.TRIG_MEMBER_ID, Len.TRIG_MEMBER_ID);
	}

	public void setTrigCorpRcvDt(String trigCorpRcvDt) {
		writeString(Pos.TRIG_CORP_RCV_DT, trigCorpRcvDt, Len.TRIG_CORP_RCV_DT);
	}

	/**Original name: TRIG-CORP-RCV-DT<br>*/
	public String getTrigCorpRcvDt() {
		return readString(Pos.TRIG_CORP_RCV_DT, Len.TRIG_CORP_RCV_DT);
	}

	public String getTrigCorpRcvDtFormatted() {
		return Functions.padBlanks(getTrigCorpRcvDt(), Len.TRIG_CORP_RCV_DT);
	}

	public void setTrigPmtAdjdDt(String trigPmtAdjdDt) {
		writeString(Pos.TRIG_PMT_ADJD_DT, trigPmtAdjdDt, Len.TRIG_PMT_ADJD_DT);
	}

	/**Original name: TRIG-PMT-ADJD-DT<br>*/
	public String getTrigPmtAdjdDt() {
		return readString(Pos.TRIG_PMT_ADJD_DT, Len.TRIG_PMT_ADJD_DT);
	}

	public String getTrigPmtAdjdDtFormatted() {
		return Functions.padBlanks(getTrigPmtAdjdDt(), Len.TRIG_PMT_ADJD_DT);
	}

	public void setTrigPmtFrmServDt(String trigPmtFrmServDt) {
		writeString(Pos.TRIG_PMT_FRM_SERV_DT, trigPmtFrmServDt, Len.TRIG_PMT_FRM_SERV_DT);
	}

	/**Original name: TRIG-PMT-FRM-SERV-DT<br>*/
	public String getTrigPmtFrmServDt() {
		return readString(Pos.TRIG_PMT_FRM_SERV_DT, Len.TRIG_PMT_FRM_SERV_DT);
	}

	public String getTrigPmtFrmServDtFormatted() {
		return Functions.padBlanks(getTrigPmtFrmServDt(), Len.TRIG_PMT_FRM_SERV_DT);
	}

	public void setTrigPmtThrServDt(String trigPmtThrServDt) {
		writeString(Pos.TRIG_PMT_THR_SERV_DT, trigPmtThrServDt, Len.TRIG_PMT_THR_SERV_DT);
	}

	/**Original name: TRIG-PMT-THR-SERV-DT<br>*/
	public String getTrigPmtThrServDt() {
		return readString(Pos.TRIG_PMT_THR_SERV_DT, Len.TRIG_PMT_THR_SERV_DT);
	}

	public String getTrigPmtThrServDtFormatted() {
		return Functions.padBlanks(getTrigPmtThrServDt(), Len.TRIG_PMT_THR_SERV_DT);
	}

	public void setTrigBillFrmDt(String trigBillFrmDt) {
		writeString(Pos.TRIG_BILL_FRM_DT, trigBillFrmDt, Len.TRIG_BILL_FRM_DT);
	}

	/**Original name: TRIG-BILL-FRM-DT<br>*/
	public String getTrigBillFrmDt() {
		return readString(Pos.TRIG_BILL_FRM_DT, Len.TRIG_BILL_FRM_DT);
	}

	public String getTrigBillFrmDtFormatted() {
		return Functions.padBlanks(getTrigBillFrmDt(), Len.TRIG_BILL_FRM_DT);
	}

	public void setTrigBillThrDt(String trigBillThrDt) {
		writeString(Pos.TRIG_BILL_THR_DT, trigBillThrDt, Len.TRIG_BILL_THR_DT);
	}

	/**Original name: TRIG-BILL-THR-DT<br>*/
	public String getTrigBillThrDt() {
		return readString(Pos.TRIG_BILL_THR_DT, Len.TRIG_BILL_THR_DT);
	}

	public String getTrigBillThrDtFormatted() {
		return Functions.padBlanks(getTrigBillThrDt(), Len.TRIG_BILL_THR_DT);
	}

	public void setTrigAsgCd(String trigAsgCd) {
		writeString(Pos.TRIG_ASG_CD, trigAsgCd, Len.TRIG_ASG_CD);
	}

	/**Original name: TRIG-ASG-CD<br>*/
	public String getTrigAsgCd() {
		return readString(Pos.TRIG_ASG_CD, Len.TRIG_ASG_CD);
	}

	/**Original name: TRIG-CLM-PD-AM<br>*/
	public AfDecimal getTrigClmPdAm() {
		return readDecimal(Pos.TRIG_CLM_PD_AM, Len.Int.TRIG_CLM_PD_AM, Len.Fract.TRIG_CLM_PD_AM);
	}

	public void setTrigAdjTypCd(char trigAdjTypCd) {
		writeChar(Pos.TRIG_ADJ_TYP_CD, trigAdjTypCd);
	}

	/**Original name: TRIG-ADJ-TYP-CD<br>*/
	public char getTrigAdjTypCd() {
		return readChar(Pos.TRIG_ADJ_TYP_CD);
	}

	public void setTrigKcapsTeamNm(String trigKcapsTeamNm) {
		writeString(Pos.TRIG_KCAPS_TEAM_NM, trigKcapsTeamNm, Len.TRIG_KCAPS_TEAM_NM);
	}

	/**Original name: TRIG-KCAPS-TEAM-NM<br>*/
	public String getTrigKcapsTeamNm() {
		return readString(Pos.TRIG_KCAPS_TEAM_NM, Len.TRIG_KCAPS_TEAM_NM);
	}

	public void setTrigKcapsUseId(String trigKcapsUseId) {
		writeString(Pos.TRIG_KCAPS_USE_ID, trigKcapsUseId, Len.TRIG_KCAPS_USE_ID);
	}

	/**Original name: TRIG-KCAPS-USE-ID<br>*/
	public String getTrigKcapsUseId() {
		return readString(Pos.TRIG_KCAPS_USE_ID, Len.TRIG_KCAPS_USE_ID);
	}

	public void setTrigHistLoadCd(char trigHistLoadCd) {
		writeChar(Pos.TRIG_HIST_LOAD_CD, trigHistLoadCd);
	}

	/**Original name: TRIG-HIST-LOAD-CD<br>*/
	public char getTrigHistLoadCd() {
		return readChar(Pos.TRIG_HIST_LOAD_CD);
	}

	public void setTrigPmtBkProdCd(char trigPmtBkProdCd) {
		writeChar(Pos.TRIG_PMT_BK_PROD_CD, trigPmtBkProdCd);
	}

	/**Original name: TRIG-PMT-BK-PROD-CD<br>*/
	public char getTrigPmtBkProdCd() {
		return readChar(Pos.TRIG_PMT_BK_PROD_CD);
	}

	public void setTrigDrgCd(String trigDrgCd) {
		writeString(Pos.TRIG_DRG_CD, trigDrgCd, Len.TRIG_DRG_CD);
	}

	/**Original name: TRIG-DRG-CD<br>*/
	public String getTrigDrgCd() {
		return readString(Pos.TRIG_DRG_CD, Len.TRIG_DRG_CD);
	}

	public void setTrigPmtOiIn(char trigPmtOiIn) {
		writeChar(Pos.TRIG_PMT_OI_IN, trigPmtOiIn);
	}

	/**Original name: TRIG-PMT-OI-IN<br>*/
	public char getTrigPmtOiIn() {
		return readChar(Pos.TRIG_PMT_OI_IN);
	}

	public void setTrigPatActMedRecId(String trigPatActMedRecId) {
		writeString(Pos.TRIG_PAT_ACT_MED_REC_ID, trigPatActMedRecId, Len.TRIG_PAT_ACT_MED_REC_ID);
	}

	/**Original name: TRIG-PAT-ACT-MED-REC-ID<br>*/
	public String getTrigPatActMedRecId() {
		return readString(Pos.TRIG_PAT_ACT_MED_REC_ID, Len.TRIG_PAT_ACT_MED_REC_ID);
	}

	public void setTrigPgmAreaCd(String trigPgmAreaCd) {
		writeString(Pos.TRIG_PGM_AREA_CD, trigPgmAreaCd, Len.TRIG_PGM_AREA_CD);
	}

	/**Original name: TRIG-PGM-AREA-CD<br>*/
	public String getTrigPgmAreaCd() {
		return readString(Pos.TRIG_PGM_AREA_CD, Len.TRIG_PGM_AREA_CD);
	}

	public void setTrigFinCd(String trigFinCd) {
		writeString(Pos.TRIG_FIN_CD, trigFinCd, Len.TRIG_FIN_CD);
	}

	/**Original name: TRIG-FIN-CD<br>*/
	public String getTrigFinCd() {
		return readString(Pos.TRIG_FIN_CD, Len.TRIG_FIN_CD);
	}

	public void setTrigAdjdProvStatCd(char trigAdjdProvStatCd) {
		writeChar(Pos.TRIG_ADJD_PROV_STAT_CD, trigAdjdProvStatCd);
	}

	/**Original name: TRIG-ADJD-PROV-STAT-CD<br>*/
	public char getTrigAdjdProvStatCd() {
		return readChar(Pos.TRIG_ADJD_PROV_STAT_CD);
	}

	public void setTrigItsClmTypCd(char trigItsClmTypCd) {
		writeChar(Pos.TRIG_ITS_CLM_TYP_CD, trigItsClmTypCd);
	}

	/**Original name: TRIG-ITS-CLM-TYP-CD<br>*/
	public char getTrigItsClmTypCd() {
		return readChar(Pos.TRIG_ITS_CLM_TYP_CD);
	}

	public void setTrigPrmptPayDayCd(String trigPrmptPayDayCd) {
		writeString(Pos.TRIG_PRMPT_PAY_DAY_CD, trigPrmptPayDayCd, Len.TRIG_PRMPT_PAY_DAY_CD);
	}

	/**Original name: TRIG-PRMPT-PAY-DAY-CD<br>*/
	public String getTrigPrmptPayDayCd() {
		return readString(Pos.TRIG_PRMPT_PAY_DAY_CD, Len.TRIG_PRMPT_PAY_DAY_CD);
	}

	public void setTrigPrmptPayOvrdCd(char trigPrmptPayOvrdCd) {
		writeChar(Pos.TRIG_PRMPT_PAY_OVRD_CD, trigPrmptPayOvrdCd);
	}

	/**Original name: TRIG-PRMPT-PAY-OVRD-CD<br>*/
	public char getTrigPrmptPayOvrdCd() {
		return readChar(Pos.TRIG_PRMPT_PAY_OVRD_CD);
	}

	/**Original name: TRIG-ACCRUED-PRMPT-PAY-INT-AM<br>*/
	public AfDecimal getTrigAccruedPrmptPayIntAm() {
		return readDecimal(Pos.TRIG_ACCRUED_PRMPT_PAY_INT_AM, Len.Int.TRIG_ACCRUED_PRMPT_PAY_INT_AM, Len.Fract.TRIG_ACCRUED_PRMPT_PAY_INT_AM,
				SignType.NO_SIGN);
	}

	public void setTrigProvUnwrpDt(String trigProvUnwrpDt) {
		writeString(Pos.TRIG_PROV_UNWRP_DT, trigProvUnwrpDt, Len.TRIG_PROV_UNWRP_DT);
	}

	/**Original name: TRIG-PROV-UNWRP-DT<br>*/
	public String getTrigProvUnwrpDt() {
		return readString(Pos.TRIG_PROV_UNWRP_DT, Len.TRIG_PROV_UNWRP_DT);
	}

	public String getTrigProvUnwrpDtFormatted() {
		return Functions.padBlanks(getTrigProvUnwrpDt(), Len.TRIG_PROV_UNWRP_DT);
	}

	public void setTrigGrpId(String trigGrpId) {
		writeString(Pos.TRIG_GRP_ID, trigGrpId, Len.TRIG_GRP_ID);
	}

	/**Original name: TRIG-GRP-ID<br>*/
	public String getTrigGrpId() {
		return readString(Pos.TRIG_GRP_ID, Len.TRIG_GRP_ID);
	}

	public void setTrigClaimLobCd(char trigClaimLobCd) {
		writeChar(Pos.TRIG_CLAIM_LOB_CD, trigClaimLobCd);
	}

	/**Original name: TRIG-CLAIM-LOB-CD<br>*/
	public char getTrigClaimLobCd() {
		return readChar(Pos.TRIG_CLAIM_LOB_CD);
	}

	public void setTrigRateCd(String trigRateCd) {
		writeString(Pos.TRIG_RATE_CD, trigRateCd, Len.TRIG_RATE_CD);
	}

	/**Original name: TRIG-RATE-CD<br>*/
	public String getTrigRateCd() {
		return readString(Pos.TRIG_RATE_CD, Len.TRIG_RATE_CD);
	}

	public void setTrigNtwrkCd(String trigNtwrkCd) {
		writeString(Pos.TRIG_NTWRK_CD, trigNtwrkCd, Len.TRIG_NTWRK_CD);
	}

	/**Original name: TRIG-NTWRK-CD<br>*/
	public String getTrigNtwrkCd() {
		return readString(Pos.TRIG_NTWRK_CD, Len.TRIG_NTWRK_CD);
	}

	public void setTrigBaseCnArngCd(String trigBaseCnArngCd) {
		writeString(Pos.TRIG_BASE_CN_ARNG_CD, trigBaseCnArngCd, Len.TRIG_BASE_CN_ARNG_CD);
	}

	/**Original name: TRIG-BASE-CN-ARNG-CD<br>*/
	public String getTrigBaseCnArngCd() {
		return readString(Pos.TRIG_BASE_CN_ARNG_CD, Len.TRIG_BASE_CN_ARNG_CD);
	}

	public void setTrigPrimCnArngCd(String trigPrimCnArngCd) {
		writeString(Pos.TRIG_PRIM_CN_ARNG_CD, trigPrimCnArngCd, Len.TRIG_PRIM_CN_ARNG_CD);
	}

	/**Original name: TRIG-PRIM-CN-ARNG-CD<br>*/
	public String getTrigPrimCnArngCd() {
		return readString(Pos.TRIG_PRIM_CN_ARNG_CD, Len.TRIG_PRIM_CN_ARNG_CD);
	}

	public void setTrigEnrClCd(String trigEnrClCd) {
		writeString(Pos.TRIG_ENR_CL_CD, trigEnrClCd, Len.TRIG_ENR_CL_CD);
	}

	/**Original name: TRIG-ENR-CL-CD<br>*/
	public String getTrigEnrClCd() {
		return readString(Pos.TRIG_ENR_CL_CD, Len.TRIG_ENR_CL_CD);
	}

	/**Original name: TRIG-LST-FNL-PMT-PT-ID<br>*/
	public short getTrigLstFnlPmtPtId() {
		return readNumDispUnsignedShort(Pos.TRIG_LST_FNL_PMT_PT_ID, Len.TRIG_LST_FNL_PMT_PT_ID);
	}

	public void setTrigStatAdjPrevPmt(char trigStatAdjPrevPmt) {
		writeChar(Pos.TRIG_STAT_ADJ_PREV_PMT, trigStatAdjPrevPmt);
	}

	/**Original name: TRIG-STAT-ADJ-PREV-PMT<br>*/
	public char getTrigStatAdjPrevPmt() {
		return readChar(Pos.TRIG_STAT_ADJ_PREV_PMT);
	}

	public void setTrigEftInd(char trigEftInd) {
		writeChar(Pos.TRIG_EFT_IND, trigEftInd);
	}

	/**Original name: TRIG-EFT-IND<br>*/
	public char getTrigEftInd() {
		return readChar(Pos.TRIG_EFT_IND);
	}

	public void setTrigEftAccountType(char trigEftAccountType) {
		writeChar(Pos.TRIG_EFT_ACCOUNT_TYPE, trigEftAccountType);
	}

	/**Original name: TRIG-EFT-ACCOUNT-TYPE<br>*/
	public char getTrigEftAccountType() {
		return readChar(Pos.TRIG_EFT_ACCOUNT_TYPE);
	}

	public void setTrigEftAcct(String trigEftAcct) {
		writeString(Pos.TRIG_EFT_ACCT, trigEftAcct, Len.TRIG_EFT_ACCT);
	}

	/**Original name: TRIG-EFT-ACCT<br>*/
	public String getTrigEftAcct() {
		return readString(Pos.TRIG_EFT_ACCT, Len.TRIG_EFT_ACCT);
	}

	public void setTrigEftTrans(String trigEftTrans) {
		writeString(Pos.TRIG_EFT_TRANS, trigEftTrans, Len.TRIG_EFT_TRANS);
	}

	/**Original name: TRIG-EFT-TRANS<br>*/
	public String getTrigEftTrans() {
		return readString(Pos.TRIG_EFT_TRANS, Len.TRIG_EFT_TRANS);
	}

	public void setTrigAlphPrfxCd(String trigAlphPrfxCd) {
		writeString(Pos.TRIG_ALPH_PRFX_CD, trigAlphPrfxCd, Len.TRIG_ALPH_PRFX_CD);
	}

	/**Original name: TRIG-ALPH-PRFX-CD<br>*/
	public String getTrigAlphPrfxCd() {
		return readString(Pos.TRIG_ALPH_PRFX_CD, Len.TRIG_ALPH_PRFX_CD);
	}

	/**Original name: TRIG-SCHDL-DRG-ALW-AM<br>*/
	public AfDecimal getTrigSchdlDrgAlwAm() {
		return readDecimal(Pos.TRIG_SCHDL_DRG_ALW_AM, Len.Int.TRIG_SCHDL_DRG_ALW_AM, Len.Fract.TRIG_SCHDL_DRG_ALW_AM);
	}

	/**Original name: TRIG-ALT-DRG-ALW-AM<br>*/
	public AfDecimal getTrigAltDrgAlwAm() {
		return readDecimal(Pos.TRIG_ALT_DRG_ALW_AM, Len.Int.TRIG_ALT_DRG_ALW_AM, Len.Fract.TRIG_ALT_DRG_ALW_AM);
	}

	/**Original name: TRIG-OVER-DRG-ALW-AM<br>*/
	public AfDecimal getTrigOverDrgAlwAm() {
		return readDecimal(Pos.TRIG_OVER_DRG_ALW_AM, Len.Int.TRIG_OVER_DRG_ALW_AM, Len.Fract.TRIG_OVER_DRG_ALW_AM);
	}

	public void setTrigGmisIndicator(String trigGmisIndicator) {
		writeString(Pos.TRIG_GMIS_INDICATOR, trigGmisIndicator, Len.TRIG_GMIS_INDICATOR);
	}

	/**Original name: TRIG-GMIS-INDICATOR<br>*/
	public String getTrigGmisIndicator() {
		return readString(Pos.TRIG_GMIS_INDICATOR, Len.TRIG_GMIS_INDICATOR);
	}

	public void setTrigGlOfstOrigCd(String trigGlOfstOrigCd) {
		writeString(Pos.TRIG_GL_OFST_ORIG_CD, trigGlOfstOrigCd, Len.TRIG_GL_OFST_ORIG_CD);
	}

	/**Original name: TRIG-GL-OFST-ORIG-CD<br>*/
	public String getTrigGlOfstOrigCd() {
		return readString(Pos.TRIG_GL_OFST_ORIG_CD, Len.TRIG_GL_OFST_ORIG_CD);
	}

	/**Original name: TRIG-GL-SOTE-ORIG-CD<br>*/
	public short getTrigGlSoteOrigCd() {
		return readNumDispUnsignedShort(Pos.TRIG_GL_SOTE_ORIG_CD, Len.TRIG_GL_SOTE_ORIG_CD);
	}

	public void setTrigVoidCd(char trigVoidCd) {
		writeChar(Pos.TRIG_VOID_CD, trigVoidCd);
	}

	/**Original name: TRIG-VOID-CD<br>*/
	public char getTrigVoidCd() {
		return readChar(Pos.TRIG_VOID_CD);
	}

	public void setTrigItsInsId(String trigItsInsId) {
		writeString(Pos.TRIG_ITS_INS_ID, trigItsInsId, Len.TRIG_ITS_INS_ID);
	}

	/**Original name: TRIG-ITS-INS-ID<br>*/
	public String getTrigItsInsId() {
		return readString(Pos.TRIG_ITS_INS_ID, Len.TRIG_ITS_INS_ID);
	}

	public String getTrigItsInsIdFormatted() {
		return Functions.padBlanks(getTrigItsInsId(), Len.TRIG_ITS_INS_ID);
	}

	public void setTrigAdjRespCd(char trigAdjRespCd) {
		writeChar(Pos.TRIG_ADJ_RESP_CD, trigAdjRespCd);
	}

	/**Original name: TRIG-ADJ-RESP-CD<br>*/
	public char getTrigAdjRespCd() {
		return readChar(Pos.TRIG_ADJ_RESP_CD);
	}

	public void setTrigAdjTrckCd(String trigAdjTrckCd) {
		writeString(Pos.TRIG_ADJ_TRCK_CD, trigAdjTrckCd, Len.TRIG_ADJ_TRCK_CD);
	}

	/**Original name: TRIG-ADJ-TRCK-CD<br>*/
	public String getTrigAdjTrckCd() {
		return readString(Pos.TRIG_ADJ_TRCK_CD, Len.TRIG_ADJ_TRCK_CD);
	}

	public void setTrigClmckAdjStatCd(char trigClmckAdjStatCd) {
		writeChar(Pos.TRIG_CLMCK_ADJ_STAT_CD, trigClmckAdjStatCd);
	}

	/**Original name: TRIG-CLMCK-ADJ-STAT-CD<br>*/
	public char getTrigClmckAdjStatCd() {
		return readChar(Pos.TRIG_CLMCK_ADJ_STAT_CD);
	}

	public void setTrig837BillProvNpiId(String trig837BillProvNpiId) {
		writeString(Pos.TRIG837_BILL_PROV_NPI_ID, trig837BillProvNpiId, Len.TRIG837_BILL_PROV_NPI_ID);
	}

	/**Original name: TRIG-837-BILL-PROV-NPI-ID<br>*/
	public String getTrig837BillProvNpiId() {
		return readString(Pos.TRIG837_BILL_PROV_NPI_ID, Len.TRIG837_BILL_PROV_NPI_ID);
	}

	public void setTrig837PerfProvNpiId(String trig837PerfProvNpiId) {
		writeString(Pos.TRIG837_PERF_PROV_NPI_ID, trig837PerfProvNpiId, Len.TRIG837_PERF_PROV_NPI_ID);
	}

	/**Original name: TRIG-837-PERF-PROV-NPI-ID<br>*/
	public String getTrig837PerfProvNpiId() {
		return readString(Pos.TRIG837_PERF_PROV_NPI_ID, Len.TRIG837_PERF_PROV_NPI_ID);
	}

	public void setTrigItsCk(char trigItsCk) {
		writeChar(Pos.TRIG_ITS_CK, trigItsCk);
	}

	/**Original name: TRIG-ITS-CK<br>*/
	public char getTrigItsCk() {
		return readChar(Pos.TRIG_ITS_CK);
	}

	public void setTrigDateCoverageLapsed(String trigDateCoverageLapsed) {
		writeString(Pos.TRIG_DATE_COVERAGE_LAPSED, trigDateCoverageLapsed, Len.TRIG_DATE_COVERAGE_LAPSED);
	}

	/**Original name: TRIG-DATE-COVERAGE-LAPSED<br>*/
	public String getTrigDateCoverageLapsed() {
		return readString(Pos.TRIG_DATE_COVERAGE_LAPSED, Len.TRIG_DATE_COVERAGE_LAPSED);
	}

	public String getTrigDateCoverageLapsedFormatted() {
		return Functions.padBlanks(getTrigDateCoverageLapsed(), Len.TRIG_DATE_COVERAGE_LAPSED);
	}

	public void setTrigOiPayNm(String trigOiPayNm) {
		writeString(Pos.TRIG_OI_PAY_NM, trigOiPayNm, Len.TRIG_OI_PAY_NM);
	}

	/**Original name: TRIG-OI-PAY-NM<br>*/
	public String getTrigOiPayNm() {
		return readString(Pos.TRIG_OI_PAY_NM, Len.TRIG_OI_PAY_NM);
	}

	public void setTrigCorrPrioritySubId(String trigCorrPrioritySubId) {
		writeString(Pos.TRIG_CORR_PRIORITY_SUB_ID, trigCorrPrioritySubId, Len.TRIG_CORR_PRIORITY_SUB_ID);
	}

	/**Original name: TRIG-CORR-PRIORITY-SUB-ID<br>*/
	public String getTrigCorrPrioritySubId() {
		return readString(Pos.TRIG_CORR_PRIORITY_SUB_ID, Len.TRIG_CORR_PRIORITY_SUB_ID);
	}

	public void setTrigHipaaVersionFormatId(String trigHipaaVersionFormatId) {
		writeString(Pos.TRIG_HIPAA_VERSION_FORMAT_ID, trigHipaaVersionFormatId, Len.TRIG_HIPAA_VERSION_FORMAT_ID);
	}

	/**Original name: TRIG-HIPAA-VERSION-FORMAT-ID<br>*/
	public String getTrigHipaaVersionFormatId() {
		return readString(Pos.TRIG_HIPAA_VERSION_FORMAT_ID, Len.TRIG_HIPAA_VERSION_FORMAT_ID);
	}

	public void setTrigTypGrpCd(String trigTypGrpCd) {
		writeString(Pos.TRIG_TYP_GRP_CD, trigTypGrpCd, Len.TRIG_TYP_GRP_CD);
	}

	/**Original name: TRIG-TYP-GRP-CD<br>*/
	public String getTrigTypGrpCd() {
		return readString(Pos.TRIG_TYP_GRP_CD, Len.TRIG_TYP_GRP_CD);
	}

	public void setTrigVbrIn(char trigVbrIn) {
		writeChar(Pos.TRIG_VBR_IN, trigVbrIn);
	}

	/**Original name: TRIG-VBR-IN<br>*/
	public char getTrigVbrIn() {
		return readChar(Pos.TRIG_VBR_IN);
	}

	public void setTrigMrktPkgCd(String trigMrktPkgCd) {
		writeString(Pos.TRIG_MRKT_PKG_CD, trigMrktPkgCd, Len.TRIG_MRKT_PKG_CD);
	}

	/**Original name: TRIG-MRKT-PKG-CD<br>*/
	public String getTrigMrktPkgCd() {
		return readString(Pos.TRIG_MRKT_PKG_CD, Len.TRIG_MRKT_PKG_CD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int TRIG_RECORD_LAYOUT = 1;
		public static final int TRIG_HEADER_REC = 1;
		public static final int TRIG_HDR_SORT_KEY = TRIG_HEADER_REC;
		public static final int FLR1 = TRIG_HDR_SORT_KEY + Len.TRIG_HDR_SORT_KEY;
		public static final int TRIG_BUS_DATE_LIT = FLR1 + Len.FLR1;
		public static final int TRIG_BUS_DATE = TRIG_BUS_DATE_LIT + Len.TRIG_BUS_DATE_LIT;
		public static final int TRIG_BUS_DATE_CCYY = TRIG_BUS_DATE;
		public static final int FLR2 = TRIG_BUS_DATE_CCYY + Len.TRIG_BUS_DATE_CCYY;
		public static final int TRIG_BUS_DATE_MM = FLR2 + Len.FLR2;
		public static final int FLR3 = TRIG_BUS_DATE_MM + Len.TRIG_BUS_DATE_MM;
		public static final int TRIG_BUS_DATE_DD = FLR3 + Len.FLR2;
		public static final int FLR4 = TRIG_BUS_DATE_DD + Len.TRIG_BUS_DATE_DD;
		public static final int TRIG_PIP_FLAG_LIT = FLR4 + Len.FLR1;
		public static final int TRIG_PIP_FLAG = TRIG_PIP_FLAG_LIT + Len.TRIG_PIP_FLAG_LIT;
		public static final int FLR5 = TRIG_PIP_FLAG + Len.TRIG_PIP_FLAG;
		public static final int TRIG_EOM_LIT = FLR5 + Len.FLR1;
		public static final int TRIG_EOM_FLAG = TRIG_EOM_LIT + Len.TRIG_EOM_LIT;
		public static final int FLR6 = TRIG_EOM_FLAG + Len.TRIG_EOM_FLAG;
		public static final int TRIG_DETAIL_REC = 1;
		public static final int TRIG_PROD_IND = TRIG_DETAIL_REC;
		public static final int FILLER1 = TRIG_PROD_IND + Len.TRIG_PROD_IND;
		public static final int TRIG_BILLING_PROVIDER_INFO = FILLER1 + Len.FILLER1;
		public static final int TRIG_HIST_BILL_PROV_NPI_ID = TRIG_BILLING_PROVIDER_INFO;
		public static final int TRIG_LOCAL_BILL_PROV_INFO = TRIG_HIST_BILL_PROV_NPI_ID + Len.TRIG_HIST_BILL_PROV_NPI_ID;
		public static final int TRIG_LOCAL_BILL_PROV_LOB_CD = TRIG_LOCAL_BILL_PROV_INFO;
		public static final int TRIG_LOCAL_BILL_PROV_ID = TRIG_LOCAL_BILL_PROV_LOB_CD + Len.TRIG_LOCAL_BILL_PROV_LOB_CD;
		public static final int TRIG_BILL_PROV_ID_QLF_CD = TRIG_LOCAL_BILL_PROV_ID + Len.TRIG_LOCAL_BILL_PROV_ID;
		public static final int TRIG_BILL_PROV_SPEC_CD = TRIG_BILL_PROV_ID_QLF_CD + Len.TRIG_BILL_PROV_ID_QLF_CD;
		public static final int TRIG_CLM_KEY = TRIG_BILL_PROV_SPEC_CD + Len.TRIG_BILL_PROV_SPEC_CD;
		public static final int TRIG_CLM_CNTL_ID = TRIG_CLM_KEY;
		public static final int TRIG_CLM_CNTL_SFX = TRIG_CLM_CNTL_ID + Len.TRIG_CLM_CNTL_ID;
		public static final int TRIG_CLM_PMT_ID = TRIG_CLM_CNTL_SFX + Len.TRIG_CLM_CNTL_SFX;
		public static final int TRIG_STAT_CATG_CD = TRIG_CLM_PMT_ID + Len.TRIG_CLM_PMT_ID;
		public static final int TRIG_FNL_BUS_DT = TRIG_STAT_CATG_CD + Len.TRIG_STAT_CATG_CD;
		public static final int TRIG_INFO_SSYST_ID = TRIG_FNL_BUS_DT + Len.TRIG_FNL_BUS_DT;
		public static final int TRIG_PERFORMING_PROVIDER_INFO = TRIG_INFO_SSYST_ID + Len.TRIG_INFO_SSYST_ID;
		public static final int TRIG_HIST_PERF_PROV_NPI_ID = TRIG_PERFORMING_PROVIDER_INFO;
		public static final int TRIG_LOCAL_PERF_PROV_LOB_CD = TRIG_HIST_PERF_PROV_NPI_ID + Len.TRIG_HIST_PERF_PROV_NPI_ID;
		public static final int TRIG_LOCAL_PERF_PROV_ID = TRIG_LOCAL_PERF_PROV_LOB_CD + Len.TRIG_LOCAL_PERF_PROV_LOB_CD;
		public static final int TRIG_INS_ID = TRIG_LOCAL_PERF_PROV_ID + Len.TRIG_LOCAL_PERF_PROV_ID;
		public static final int TRIG_MEMBER_ID = TRIG_INS_ID + Len.TRIG_INS_ID;
		public static final int TRIG_CORP_RCV_DT = TRIG_MEMBER_ID + Len.TRIG_MEMBER_ID;
		public static final int TRIG_PMT_ADJD_DT = TRIG_CORP_RCV_DT + Len.TRIG_CORP_RCV_DT;
		public static final int TRIG_PMT_FRM_SERV_DT = TRIG_PMT_ADJD_DT + Len.TRIG_PMT_ADJD_DT;
		public static final int TRIG_PMT_THR_SERV_DT = TRIG_PMT_FRM_SERV_DT + Len.TRIG_PMT_FRM_SERV_DT;
		public static final int TRIG_BILL_FRM_DT = TRIG_PMT_THR_SERV_DT + Len.TRIG_PMT_THR_SERV_DT;
		public static final int TRIG_BILL_THR_DT = TRIG_BILL_FRM_DT + Len.TRIG_BILL_FRM_DT;
		public static final int TRIG_ASG_CD = TRIG_BILL_THR_DT + Len.TRIG_BILL_THR_DT;
		public static final int TRIG_CLM_PD_AM = TRIG_ASG_CD + Len.TRIG_ASG_CD;
		public static final int TRIG_ADJ_TYP_CD = TRIG_CLM_PD_AM + Len.TRIG_CLM_PD_AM;
		public static final int TRIG_KCAPS_TEAM_NM = TRIG_ADJ_TYP_CD + Len.TRIG_ADJ_TYP_CD;
		public static final int TRIG_KCAPS_USE_ID = TRIG_KCAPS_TEAM_NM + Len.TRIG_KCAPS_TEAM_NM;
		public static final int TRIG_HIST_LOAD_CD = TRIG_KCAPS_USE_ID + Len.TRIG_KCAPS_USE_ID;
		public static final int TRIG_PMT_BK_PROD_CD = TRIG_HIST_LOAD_CD + Len.TRIG_HIST_LOAD_CD;
		public static final int TRIG_DRG_CD = TRIG_PMT_BK_PROD_CD + Len.TRIG_PMT_BK_PROD_CD;
		public static final int TRIG_PMT_OI_IN = TRIG_DRG_CD + Len.TRIG_DRG_CD;
		public static final int TRIG_PAT_ACT_MED_REC_ID = TRIG_PMT_OI_IN + Len.TRIG_PMT_OI_IN;
		public static final int TRIG_PGM_AREA_CD = TRIG_PAT_ACT_MED_REC_ID + Len.TRIG_PAT_ACT_MED_REC_ID;
		public static final int TRIG_FIN_CD = TRIG_PGM_AREA_CD + Len.TRIG_PGM_AREA_CD;
		public static final int TRIG_ADJD_PROV_STAT_CD = TRIG_FIN_CD + Len.TRIG_FIN_CD;
		public static final int TRIG_ITS_CLM_TYP_CD = TRIG_ADJD_PROV_STAT_CD + Len.TRIG_ADJD_PROV_STAT_CD;
		public static final int TRIG_PRMPT_PAY_DAY_CD = TRIG_ITS_CLM_TYP_CD + Len.TRIG_ITS_CLM_TYP_CD;
		public static final int TRIG_PRMPT_PAY_OVRD_CD = TRIG_PRMPT_PAY_DAY_CD + Len.TRIG_PRMPT_PAY_DAY_CD;
		public static final int TRIG_ACCRUED_PRMPT_PAY_INT_AM = TRIG_PRMPT_PAY_OVRD_CD + Len.TRIG_PRMPT_PAY_OVRD_CD;
		public static final int TRIG_PROV_UNWRP_DT = TRIG_ACCRUED_PRMPT_PAY_INT_AM + Len.TRIG_ACCRUED_PRMPT_PAY_INT_AM;
		public static final int TRIG_GRP_ID = TRIG_PROV_UNWRP_DT + Len.TRIG_PROV_UNWRP_DT;
		public static final int TRIG_FILLER = TRIG_GRP_ID + Len.TRIG_GRP_ID;
		public static final int TRIG_CLAIM_LOB_CD = TRIG_FILLER + Len.TRIG_FILLER;
		public static final int TRIG_RATE_CD = TRIG_CLAIM_LOB_CD + Len.TRIG_CLAIM_LOB_CD;
		public static final int TRIG_NTWRK_CD = TRIG_RATE_CD + Len.TRIG_RATE_CD;
		public static final int FLR7 = TRIG_NTWRK_CD + Len.TRIG_NTWRK_CD;
		public static final int TRIG_BASE_CN_ARNG_CD = FLR7 + Len.FLR7;
		public static final int TRIG_PRIM_CN_ARNG_CD = TRIG_BASE_CN_ARNG_CD + Len.TRIG_BASE_CN_ARNG_CD;
		public static final int TRIG_PRC_INST_REIMB_CD = TRIG_PRIM_CN_ARNG_CD + Len.TRIG_PRIM_CN_ARNG_CD;
		public static final int TRIG_PRC_PROF_REIMB_CD = TRIG_PRC_INST_REIMB_CD + Len.TRIG_PRC_INST_REIMB_CD;
		public static final int TRIG_ELIG_INST_REIMB_CD = TRIG_PRC_PROF_REIMB_CD + Len.TRIG_PRC_PROF_REIMB_CD;
		public static final int TRIG_ELIG_PROF_REIMB_CD = TRIG_ELIG_INST_REIMB_CD + Len.TRIG_ELIG_INST_REIMB_CD;
		public static final int TRIG_ENR_CL_CD = TRIG_ELIG_PROF_REIMB_CD + Len.TRIG_ELIG_PROF_REIMB_CD;
		public static final int TRIG_LST_FNL_PMT_PT_ID = TRIG_ENR_CL_CD + Len.TRIG_ENR_CL_CD;
		public static final int TRIG_CURR_FNL_PMT_PT_ID = TRIG_LST_FNL_PMT_PT_ID + Len.TRIG_LST_FNL_PMT_PT_ID;
		public static final int TRIG_UPDT_LST_FNL_PMT_SW = TRIG_CURR_FNL_PMT_PT_ID + Len.TRIG_CURR_FNL_PMT_PT_ID;
		public static final int TRIG_STAT_ADJ_PREV_PMT = TRIG_UPDT_LST_FNL_PMT_SW + Len.TRIG_UPDT_LST_FNL_PMT_SW;
		public static final int TRIG_EFT_INFO = TRIG_STAT_ADJ_PREV_PMT + Len.TRIG_STAT_ADJ_PREV_PMT;
		public static final int TRIG_EFT_IND = TRIG_EFT_INFO;
		public static final int TRIG_EFT_ACCOUNT_TYPE = TRIG_EFT_IND + Len.TRIG_EFT_IND;
		public static final int TRIG_EFT_ACCT = TRIG_EFT_ACCOUNT_TYPE + Len.TRIG_EFT_ACCOUNT_TYPE;
		public static final int TRIG_EFT_TRANS = TRIG_EFT_ACCT + Len.TRIG_EFT_ACCT;
		public static final int FLR8 = TRIG_EFT_TRANS + Len.TRIG_EFT_TRANS;
		public static final int TRIG_ALPH_PRFX_CD = FLR8 + Len.FLR7;
		public static final int TRIG_SCHDL_DRG_ALW_AM = TRIG_ALPH_PRFX_CD + Len.TRIG_ALPH_PRFX_CD;
		public static final int TRIG_ALT_DRG_ALW_AM = TRIG_SCHDL_DRG_ALW_AM + Len.TRIG_SCHDL_DRG_ALW_AM;
		public static final int TRIG_OVER_DRG_ALW_AM = TRIG_ALT_DRG_ALW_AM + Len.TRIG_ALT_DRG_ALW_AM;
		public static final int TRIG_GMIS_INDICATOR = TRIG_OVER_DRG_ALW_AM + Len.TRIG_OVER_DRG_ALW_AM;
		public static final int TRIG_GL_OFST_ORIG_CD = TRIG_GMIS_INDICATOR + Len.TRIG_GMIS_INDICATOR;
		public static final int TRIG_GL_SOTE_ORIG_CD = TRIG_GL_OFST_ORIG_CD + Len.TRIG_GL_OFST_ORIG_CD;
		public static final int TRIG_VOID_CD = TRIG_GL_SOTE_ORIG_CD + Len.TRIG_GL_SOTE_ORIG_CD;
		public static final int TRIG_ITS_INS_ID = TRIG_VOID_CD + Len.TRIG_VOID_CD;
		public static final int TRIG_GL_OFST_VOID_CD = TRIG_ITS_INS_ID + Len.TRIG_ITS_INS_ID;
		public static final int TRIG_ADJ_RESP_CD = TRIG_GL_OFST_VOID_CD + Len.TRIG_GL_OFST_VOID_CD;
		public static final int TRIG_ADJ_TRCK_CD = TRIG_ADJ_RESP_CD + Len.TRIG_ADJ_RESP_CD;
		public static final int TRIG_CLMCK_ADJ_STAT_CD = TRIG_ADJ_TRCK_CD + Len.TRIG_ADJ_TRCK_CD;
		public static final int TRIG837_BILL_PROV_NPI_ID = TRIG_CLMCK_ADJ_STAT_CD + Len.TRIG_CLMCK_ADJ_STAT_CD;
		public static final int TRIG837_PERF_PROV_NPI_ID = TRIG837_BILL_PROV_NPI_ID + Len.TRIG837_BILL_PROV_NPI_ID;
		public static final int TRIG_ITS_CK = TRIG837_PERF_PROV_NPI_ID + Len.TRIG837_PERF_PROV_NPI_ID;
		public static final int TRIG_DATE_COVERAGE_LAPSED = TRIG_ITS_CK + Len.TRIG_ITS_CK;
		public static final int TRIG_OI_PAY_NM = TRIG_DATE_COVERAGE_LAPSED + Len.TRIG_DATE_COVERAGE_LAPSED;
		public static final int TRIG_CORR_PRIORITY_SUB_ID = TRIG_OI_PAY_NM + Len.TRIG_OI_PAY_NM;
		public static final int TRIG_HIPAA_VERSION_FORMAT_ID = TRIG_CORR_PRIORITY_SUB_ID + Len.TRIG_CORR_PRIORITY_SUB_ID;
		public static final int TRIG_TYP_GRP_CD = TRIG_HIPAA_VERSION_FORMAT_ID + Len.TRIG_HIPAA_VERSION_FORMAT_ID;
		public static final int TRIG_CLM_SYST_ID = TRIG_TYP_GRP_CD + Len.TRIG_TYP_GRP_CD;
		public static final int TRIG_VBR_IN = TRIG_CLM_SYST_ID + Len.TRIG_CLM_SYST_ID;
		public static final int TRIG_MRKT_PKG_CD = TRIG_VBR_IN + Len.TRIG_VBR_IN;
		public static final int FILLER_LEFT = TRIG_MRKT_PKG_CD + Len.TRIG_MRKT_PKG_CD;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int TRIG_HDR_SORT_KEY = 3;
		public static final int FLR1 = 2;
		public static final int TRIG_BUS_DATE_LIT = 11;
		public static final int TRIG_BUS_DATE_CCYY = 4;
		public static final int FLR2 = 1;
		public static final int TRIG_BUS_DATE_MM = 2;
		public static final int TRIG_BUS_DATE_DD = 2;
		public static final int TRIG_PIP_FLAG_LIT = 12;
		public static final int TRIG_PIP_FLAG = 1;
		public static final int TRIG_EOM_LIT = 15;
		public static final int TRIG_EOM_FLAG = 1;
		public static final int TRIG_PROD_IND = 3;
		public static final int FILLER1 = 1;
		public static final int TRIG_HIST_BILL_PROV_NPI_ID = 10;
		public static final int TRIG_LOCAL_BILL_PROV_LOB_CD = 1;
		public static final int TRIG_LOCAL_BILL_PROV_ID = 10;
		public static final int TRIG_BILL_PROV_ID_QLF_CD = 3;
		public static final int TRIG_BILL_PROV_SPEC_CD = 4;
		public static final int TRIG_CLM_CNTL_ID = 12;
		public static final int TRIG_CLM_CNTL_SFX = 1;
		public static final int TRIG_CLM_PMT_ID = 2;
		public static final int TRIG_STAT_CATG_CD = 3;
		public static final int TRIG_FNL_BUS_DT = 10;
		public static final int TRIG_INFO_SSYST_ID = 4;
		public static final int TRIG_HIST_PERF_PROV_NPI_ID = 10;
		public static final int TRIG_LOCAL_PERF_PROV_LOB_CD = 1;
		public static final int TRIG_LOCAL_PERF_PROV_ID = 10;
		public static final int TRIG_INS_ID = 12;
		public static final int TRIG_MEMBER_ID = 14;
		public static final int TRIG_CORP_RCV_DT = 10;
		public static final int TRIG_PMT_ADJD_DT = 10;
		public static final int TRIG_PMT_FRM_SERV_DT = 10;
		public static final int TRIG_PMT_THR_SERV_DT = 10;
		public static final int TRIG_BILL_FRM_DT = 10;
		public static final int TRIG_BILL_THR_DT = 10;
		public static final int TRIG_ASG_CD = 2;
		public static final int TRIG_CLM_PD_AM = 11;
		public static final int TRIG_ADJ_TYP_CD = 1;
		public static final int TRIG_KCAPS_TEAM_NM = 5;
		public static final int TRIG_KCAPS_USE_ID = 3;
		public static final int TRIG_HIST_LOAD_CD = 1;
		public static final int TRIG_PMT_BK_PROD_CD = 1;
		public static final int TRIG_DRG_CD = 4;
		public static final int TRIG_PMT_OI_IN = 1;
		public static final int TRIG_PAT_ACT_MED_REC_ID = 25;
		public static final int TRIG_PGM_AREA_CD = 3;
		public static final int TRIG_FIN_CD = 3;
		public static final int TRIG_ADJD_PROV_STAT_CD = 1;
		public static final int TRIG_ITS_CLM_TYP_CD = 1;
		public static final int TRIG_PRMPT_PAY_DAY_CD = 2;
		public static final int TRIG_PRMPT_PAY_OVRD_CD = 1;
		public static final int TRIG_ACCRUED_PRMPT_PAY_INT_AM = 11;
		public static final int TRIG_PROV_UNWRP_DT = 10;
		public static final int TRIG_GRP_ID = 12;
		public static final int TRIG_FILLER = 1;
		public static final int TRIG_CLAIM_LOB_CD = 1;
		public static final int TRIG_RATE_CD = 2;
		public static final int TRIG_NTWRK_CD = 3;
		public static final int FLR7 = 5;
		public static final int TRIG_BASE_CN_ARNG_CD = 5;
		public static final int TRIG_PRIM_CN_ARNG_CD = 5;
		public static final int TRIG_PRC_INST_REIMB_CD = 2;
		public static final int TRIG_PRC_PROF_REIMB_CD = 2;
		public static final int TRIG_ELIG_INST_REIMB_CD = 2;
		public static final int TRIG_ELIG_PROF_REIMB_CD = 2;
		public static final int TRIG_ENR_CL_CD = 3;
		public static final int TRIG_LST_FNL_PMT_PT_ID = 2;
		public static final int TRIG_CURR_FNL_PMT_PT_ID = 2;
		public static final int TRIG_UPDT_LST_FNL_PMT_SW = 1;
		public static final int TRIG_STAT_ADJ_PREV_PMT = 1;
		public static final int TRIG_EFT_IND = 1;
		public static final int TRIG_EFT_ACCOUNT_TYPE = 1;
		public static final int TRIG_EFT_ACCT = 17;
		public static final int TRIG_EFT_TRANS = 9;
		public static final int TRIG_ALPH_PRFX_CD = 3;
		public static final int TRIG_SCHDL_DRG_ALW_AM = 11;
		public static final int TRIG_ALT_DRG_ALW_AM = 11;
		public static final int TRIG_OVER_DRG_ALW_AM = 11;
		public static final int TRIG_GMIS_INDICATOR = 7;
		public static final int TRIG_GL_OFST_ORIG_CD = 2;
		public static final int TRIG_GL_SOTE_ORIG_CD = 2;
		public static final int TRIG_VOID_CD = 1;
		public static final int TRIG_ITS_INS_ID = 17;
		public static final int TRIG_GL_OFST_VOID_CD = 2;
		public static final int TRIG_ADJ_RESP_CD = 1;
		public static final int TRIG_ADJ_TRCK_CD = 2;
		public static final int TRIG_CLMCK_ADJ_STAT_CD = 1;
		public static final int TRIG837_BILL_PROV_NPI_ID = 10;
		public static final int TRIG837_PERF_PROV_NPI_ID = 10;
		public static final int TRIG_ITS_CK = 1;
		public static final int TRIG_DATE_COVERAGE_LAPSED = 10;
		public static final int TRIG_OI_PAY_NM = 35;
		public static final int TRIG_CORR_PRIORITY_SUB_ID = 30;
		public static final int TRIG_HIPAA_VERSION_FORMAT_ID = 10;
		public static final int TRIG_TYP_GRP_CD = 3;
		public static final int TRIG_CLM_SYST_ID = 6;
		public static final int TRIG_VBR_IN = 1;
		public static final int TRIG_MRKT_PKG_CD = 30;
		public static final int TRIG_RECORD_LAYOUT = 600;
		public static final int TRIG_BUS_DATE = TRIG_BUS_DATE_CCYY + TRIG_BUS_DATE_MM + TRIG_BUS_DATE_DD + 2 * FLR2;
		public static final int FLR6 = 541;
		public static final int TRIG_HEADER_REC = TRIG_HDR_SORT_KEY + TRIG_BUS_DATE_LIT + TRIG_BUS_DATE + TRIG_PIP_FLAG_LIT + TRIG_PIP_FLAG
				+ TRIG_EOM_LIT + TRIG_EOM_FLAG + 3 * FLR1 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int TRIG_CLM_PD_AM = 9;
			public static final int TRIG_ACCRUED_PRMPT_PAY_INT_AM = 9;
			public static final int TRIG_SCHDL_DRG_ALW_AM = 9;
			public static final int TRIG_ALT_DRG_ALW_AM = 9;
			public static final int TRIG_OVER_DRG_ALW_AM = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int TRIG_CLM_PD_AM = 2;
			public static final int TRIG_ACCRUED_PRMPT_PAY_INT_AM = 2;
			public static final int TRIG_SCHDL_DRG_ALW_AM = 2;
			public static final int TRIG_ALT_DRG_ALW_AM = 2;
			public static final int TRIG_OVER_DRG_ALW_AM = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
