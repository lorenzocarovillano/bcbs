/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: VOID-S02811-CALC1-INFO<br>
 * Variable: VOID-S02811-CALC1-INFO from copybook NF05VOID<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class VoidS02811Calc1Info {

	//==== PROPERTIES ====
	//Original name: VOID-C1-CALC-EXPLN-CD
	private char c1CalcExplnCd = DefaultValues.CHAR_VAL;
	//Original name: VOID-C1-PD-AM
	private AfDecimal c1PdAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: VOID-CAS-EOB-DED-AM
	private AfDecimal asEobDedAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: VOID-CAS-EOB-COINS-AM
	private AfDecimal asEobCoinsAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: VOID-CAS-EOB-COPAY-AM
	private AfDecimal asEobCopayAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: VOID-C1-GL-ACCT-ID
	private String c1GlAcctId = DefaultValues.stringVal(Len.C1_GL_ACCT_ID);
	//Original name: VOID-C1-CORP-CD
	private char c1CorpCd = DefaultValues.CHAR_VAL;
	//Original name: VOID-C1-PROD-CD
	private String c1ProdCd = DefaultValues.stringVal(Len.C1_PROD_CD);

	//==== METHODS ====
	public void setC1CalcExplnCd(char c1CalcExplnCd) {
		this.c1CalcExplnCd = c1CalcExplnCd;
	}

	public char getC1CalcExplnCd() {
		return this.c1CalcExplnCd;
	}

	public void setC1PdAm(AfDecimal c1PdAm) {
		this.c1PdAm.assign(c1PdAm);
	}

	public AfDecimal getC1PdAm() {
		return this.c1PdAm.copy();
	}

	public void setAsEobDedAm(AfDecimal asEobDedAm) {
		this.asEobDedAm.assign(asEobDedAm);
	}

	public AfDecimal getAsEobDedAm() {
		return this.asEobDedAm.copy();
	}

	public void setAsEobCoinsAm(AfDecimal asEobCoinsAm) {
		this.asEobCoinsAm.assign(asEobCoinsAm);
	}

	public AfDecimal getAsEobCoinsAm() {
		return this.asEobCoinsAm.copy();
	}

	public void setAsEobCopayAm(AfDecimal asEobCopayAm) {
		this.asEobCopayAm.assign(asEobCopayAm);
	}

	public AfDecimal getAsEobCopayAm() {
		return this.asEobCopayAm.copy();
	}

	public void setC1GlAcctId(String c1GlAcctId) {
		this.c1GlAcctId = Functions.subString(c1GlAcctId, Len.C1_GL_ACCT_ID);
	}

	public String getC1GlAcctId() {
		return this.c1GlAcctId;
	}

	public void setC1CorpCd(char c1CorpCd) {
		this.c1CorpCd = c1CorpCd;
	}

	public char getC1CorpCd() {
		return this.c1CorpCd;
	}

	public void setC1ProdCd(String c1ProdCd) {
		this.c1ProdCd = Functions.subString(c1ProdCd, Len.C1_PROD_CD);
	}

	public String getC1ProdCd() {
		return this.c1ProdCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int C1_GL_ACCT_ID = 8;
		public static final int C1_PROD_CD = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
