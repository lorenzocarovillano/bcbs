/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-HOLD-TRIG-HEADER-AREA<br>
 * Variable: WS-HOLD-TRIG-HEADER-AREA from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsHoldTrigHeaderArea {

	//==== PROPERTIES ====
	//Original name: HOLD-HDR-SORT-KEY
	private String hdrSortKey = DefaultValues.stringVal(Len.HDR_SORT_KEY);
	//Original name: FILLER-WS-HOLD-TRIG-HEADER-AREA
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: HOLD-BUS-DATE-LIT
	private String busDateLit = DefaultValues.stringVal(Len.BUS_DATE_LIT);
	//Original name: HOLD-BUS-DATE
	private String busDate = DefaultValues.stringVal(Len.BUS_DATE);
	//Original name: FILLER-WS-HOLD-TRIG-HEADER-AREA-1
	private String flr2 = DefaultValues.stringVal(Len.FLR1);
	//Original name: HOLD-PIP-FLAG-LIT
	private String pipFlagLit = DefaultValues.stringVal(Len.PIP_FLAG_LIT);
	//Original name: HOLD-PIP-FLAG
	private char pipFlag = DefaultValues.CHAR_VAL;
	//Original name: FILLER-WS-HOLD-TRIG-HEADER-AREA-2
	private String flr3 = DefaultValues.stringVal(Len.FLR1);
	//Original name: HOLD-EOM-LIT
	private String eomLit = DefaultValues.stringVal(Len.EOM_LIT);
	//Original name: HOLD-EOM-FLAG
	private char eomFlag = DefaultValues.CHAR_VAL;
	//Original name: FILLER-WS-HOLD-TRIG-HEADER-AREA-3
	private String flr4 = "";

	//==== METHODS ====
	public String getWsHoldTrigHeaderAreaFormatted() {
		return MarshalByteExt.bufferToStr(getWsHoldTrigHeaderAreaBytes());
	}

	public void setWsHoldTrigHeaderAreaBytes(byte[] buffer) {
		setWsHoldTrigHeaderAreaBytes(buffer, 1);
	}

	public byte[] getWsHoldTrigHeaderAreaBytes() {
		byte[] buffer = new byte[Len.WS_HOLD_TRIG_HEADER_AREA];
		return getWsHoldTrigHeaderAreaBytes(buffer, 1);
	}

	public void setWsHoldTrigHeaderAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		hdrSortKey = MarshalByte.readString(buffer, position, Len.HDR_SORT_KEY);
		position += Len.HDR_SORT_KEY;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
		position += Len.FLR1;
		busDateLit = MarshalByte.readString(buffer, position, Len.BUS_DATE_LIT);
		position += Len.BUS_DATE_LIT;
		busDate = MarshalByte.readString(buffer, position, Len.BUS_DATE);
		position += Len.BUS_DATE;
		flr2 = MarshalByte.readString(buffer, position, Len.FLR1);
		position += Len.FLR1;
		pipFlagLit = MarshalByte.readString(buffer, position, Len.PIP_FLAG_LIT);
		position += Len.PIP_FLAG_LIT;
		pipFlag = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		flr3 = MarshalByte.readString(buffer, position, Len.FLR1);
		position += Len.FLR1;
		eomLit = MarshalByte.readString(buffer, position, Len.EOM_LIT);
		position += Len.EOM_LIT;
		eomFlag = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		flr4 = MarshalByte.readString(buffer, position, Len.FLR4);
	}

	public byte[] getWsHoldTrigHeaderAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, hdrSortKey, Len.HDR_SORT_KEY);
		position += Len.HDR_SORT_KEY;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, busDateLit, Len.BUS_DATE_LIT);
		position += Len.BUS_DATE_LIT;
		MarshalByte.writeString(buffer, position, busDate, Len.BUS_DATE);
		position += Len.BUS_DATE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, pipFlagLit, Len.PIP_FLAG_LIT);
		position += Len.PIP_FLAG_LIT;
		MarshalByte.writeChar(buffer, position, pipFlag);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, eomLit, Len.EOM_LIT);
		position += Len.EOM_LIT;
		MarshalByte.writeChar(buffer, position, eomFlag);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		return buffer;
	}

	public void setHdrSortKey(String hdrSortKey) {
		this.hdrSortKey = Functions.subString(hdrSortKey, Len.HDR_SORT_KEY);
	}

	public String getHdrSortKey() {
		return this.hdrSortKey;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setBusDateLit(String busDateLit) {
		this.busDateLit = Functions.subString(busDateLit, Len.BUS_DATE_LIT);
	}

	public String getBusDateLit() {
		return this.busDateLit;
	}

	public void setBusDate(String busDate) {
		this.busDate = Functions.subString(busDate, Len.BUS_DATE);
	}

	public String getBusDate() {
		return this.busDate;
	}

	public void setFlr2(String flr2) {
		this.flr2 = Functions.subString(flr2, Len.FLR1);
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setPipFlagLit(String pipFlagLit) {
		this.pipFlagLit = Functions.subString(pipFlagLit, Len.PIP_FLAG_LIT);
	}

	public String getPipFlagLit() {
		return this.pipFlagLit;
	}

	public void setPipFlag(char pipFlag) {
		this.pipFlag = pipFlag;
	}

	public char getPipFlag() {
		return this.pipFlag;
	}

	public void setFlr3(String flr3) {
		this.flr3 = Functions.subString(flr3, Len.FLR1);
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setEomLit(String eomLit) {
		this.eomLit = Functions.subString(eomLit, Len.EOM_LIT);
	}

	public String getEomLit() {
		return this.eomLit;
	}

	public void setEomFlag(char eomFlag) {
		this.eomFlag = eomFlag;
	}

	public char getEomFlag() {
		return this.eomFlag;
	}

	public void setFlr4(String flr4) {
		this.flr4 = Functions.subString(flr4, Len.FLR4);
	}

	public String getFlr4() {
		return this.flr4;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HDR_SORT_KEY = 3;
		public static final int FLR1 = 2;
		public static final int BUS_DATE_LIT = 11;
		public static final int BUS_DATE = 10;
		public static final int PIP_FLAG_LIT = 12;
		public static final int EOM_LIT = 15;
		public static final int FLR4 = 3441;
		public static final int PIP_FLAG = 1;
		public static final int EOM_FLAG = 1;
		public static final int WS_HOLD_TRIG_HEADER_AREA = HDR_SORT_KEY + BUS_DATE_LIT + BUS_DATE + PIP_FLAG_LIT + PIP_FLAG + EOM_LIT + EOM_FLAG
				+ 3 * FLR1 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
