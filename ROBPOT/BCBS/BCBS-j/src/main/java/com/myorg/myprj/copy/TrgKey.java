/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: TRG-KEY<br>
 * Variable: TRG-KEY from copybook NF05TRIG<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TrgKey {

	//==== PROPERTIES ====
	//Original name: TRG-CLM-CNTL-ID
	private String cntlId = DefaultValues.stringVal(Len.CNTL_ID);
	//Original name: TRG-CLM-CNTL-SFX-ID
	private char cntlSfxId = DefaultValues.CHAR_VAL;
	//Original name: TRG-CLM-PMT-ID
	private String pmtId = DefaultValues.stringVal(Len.PMT_ID);

	//==== METHODS ====
	public byte[] getKeyBytes() {
		byte[] buffer = new byte[Len.KEY];
		return getKeyBytes(buffer, 1);
	}

	public void setKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		cntlId = MarshalByte.readString(buffer, position, Len.CNTL_ID);
		position += Len.CNTL_ID;
		cntlSfxId = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pmtId = MarshalByte.readFixedString(buffer, position, Len.PMT_ID);
	}

	public byte[] getKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cntlId, Len.CNTL_ID);
		position += Len.CNTL_ID;
		MarshalByte.writeChar(buffer, position, cntlSfxId);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pmtId, Len.PMT_ID);
		return buffer;
	}

	public void setCntlId(String cntlId) {
		this.cntlId = Functions.subString(cntlId, Len.CNTL_ID);
	}

	public String getCntlId() {
		return this.cntlId;
	}

	public String getCntlIdFormatted() {
		return Functions.padBlanks(getCntlId(), Len.CNTL_ID);
	}

	public void setCntlSfxId(char cntlSfxId) {
		this.cntlSfxId = cntlSfxId;
	}

	public char getCntlSfxId() {
		return this.cntlSfxId;
	}

	public short getPmtId() {
		return NumericDisplay.asShort(this.pmtId);
	}

	public String getPmtIdFormatted() {
		return this.pmtId;
	}

	public String getPmtIdAsString() {
		return getPmtIdFormatted();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CNTL_ID = 12;
		public static final int CNTL_SFX_ID = 1;
		public static final int PMT_ID = 2;
		public static final int KEY = CNTL_ID + CNTL_SFX_ID + PMT_ID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
