/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [S02800CA, S02809SA, S02811SA, S02813SA]
 * 
 */
public interface IS02800caS02809saS02811saS02813sa extends BaseSqlTo {

	/**
	 * Host Variable VOID-CLM-CNTL-ID
	 * 
	 */
	String getClmCntlId();

	void setClmCntlId(String clmCntlId);

	/**
	 * Host Variable VOID-CLM-CNTL-SFX-ID
	 * 
	 */
	char getClmCntlSfxId();

	void setClmCntlSfxId(char clmCntlSfxId);

	/**
	 * Host Variable VOID-CLM-PMT-ID
	 * 
	 */
	short getClmPmtId();

	void setClmPmtId(short clmPmtId);

	/**
	 * Host Variable VOID-PMT-ADJ-TYP-CD
	 * 
	 */
	char getPmtAdjTypCd();

	void setPmtAdjTypCd(char pmtAdjTypCd);

	/**
	 * Host Variable VOID-CLM-PD-AM
	 * 
	 */
	AfDecimal getClmPdAm();

	void setClmPdAm(AfDecimal clmPdAm);

	/**
	 * Host Variable VOID-PMT-HIST-LOAD-CD
	 * 
	 */
	char getPmtHistLoadCd();

	void setPmtHistLoadCd(char pmtHistLoadCd);

	/**
	 * Host Variable VOID-PMT-KCAPS-TEAM-NM
	 * 
	 */
	String getPmtKcapsTeamNm();

	void setPmtKcapsTeamNm(String pmtKcapsTeamNm);

	/**
	 * Host Variable VOID-PMT-KCAPS-USE-ID
	 * 
	 */
	String getPmtKcapsUseId();

	void setPmtKcapsUseId(String pmtKcapsUseId);

	/**
	 * Host Variable VOID-PMT-PGM-AREA-CD
	 * 
	 */
	String getPmtPgmAreaCd();

	void setPmtPgmAreaCd(String pmtPgmAreaCd);

	/**
	 * Host Variable VOID-PMT-ENR-CL-CD
	 * 
	 */
	String getPmtEnrClCd();

	void setPmtEnrClCd(String pmtEnrClCd);

	/**
	 * Host Variable VOID-PMT-FIN-CD
	 * 
	 */
	String getPmtFinCd();

	void setPmtFinCd(String pmtFinCd);

	/**
	 * Host Variable VOID-PMT-NTWRK-CD
	 * 
	 */
	String getPmtNtwrkCd();

	void setPmtNtwrkCd(String pmtNtwrkCd);

	/**
	 * Host Variable VOID-CLI-ID
	 * 
	 */
	short getCliId();

	void setCliId(short cliId);

	/**
	 * Host Variable VOID-LI-CHG-AM
	 * 
	 */
	AfDecimal getLiChgAm();

	void setLiChgAm(AfDecimal liChgAm);

	/**
	 * Host Variable VOID-LI-PAT-RESP-AM
	 * 
	 */
	AfDecimal getLiPatRespAm();

	void setLiPatRespAm(AfDecimal liPatRespAm);

	/**
	 * Host Variable VOID-LI-PWO-AM
	 * 
	 */
	AfDecimal getLiPwoAm();

	void setLiPwoAm(AfDecimal liPwoAm);

	/**
	 * Host Variable VOID-LI-ALCT-OI-PD-AM
	 * 
	 */
	AfDecimal getLiAlctOiPdAm();

	void setLiAlctOiPdAm(AfDecimal liAlctOiPdAm);

	/**
	 * Host Variable VOID-LI-DNL-RMRK-CD
	 * 
	 */
	String getLiDnlRmrkCd();

	void setLiDnlRmrkCd(String liDnlRmrkCd);

	/**
	 * Host Variable VOID-LI-EXPLN-CD
	 * 
	 */
	char getLiExplnCd();

	void setLiExplnCd(char liExplnCd);

	/**
	 * Host Variable VOID-C1-CALC-EXPLN-CD
	 * 
	 */
	char getC1CalcExplnCd();

	void setC1CalcExplnCd(char c1CalcExplnCd);

	/**
	 * Host Variable VOID-C1-PD-AM
	 * 
	 */
	AfDecimal getC1PdAm();

	void setC1PdAm(AfDecimal c1PdAm);

	/**
	 * Host Variable VOID-C1-GL-ACCT-ID
	 * 
	 */
	String getC1GlAcctId();

	void setC1GlAcctId(String c1GlAcctId);

	/**
	 * Host Variable VOID-C1-CORP-CD
	 * 
	 */
	char getC1CorpCd();

	void setC1CorpCd(char c1CorpCd);

	/**
	 * Host Variable VOID-C1-PROD-CD
	 * 
	 */
	String getC1ProdCd();

	void setC1ProdCd(String c1ProdCd);

	/**
	 * Host Variable VOID-C2-CALC-EXPLN-CD
	 * 
	 */
	char getC2CalcExplnCd();

	void setC2CalcExplnCd(char c2CalcExplnCd);

	/**
	 * Host Variable VOID-C2-PD-AM
	 * 
	 */
	AfDecimal getC2PdAm();

	void setC2PdAm(AfDecimal c2PdAm);

	/**
	 * Host Variable VOID-C2-GL-ACCT-ID
	 * 
	 */
	String getC2GlAcctId();

	void setC2GlAcctId(String c2GlAcctId);

	/**
	 * Host Variable VOID-C2-CORP-CD
	 * 
	 */
	char getC2CorpCd();

	void setC2CorpCd(char c2CorpCd);

	/**
	 * Host Variable VOID-C2-PROD-CD
	 * 
	 */
	String getC2ProdCd();

	void setC2ProdCd(String c2ProdCd);

	/**
	 * Host Variable VOID-C3-CALC-EXPLN-CD
	 * 
	 */
	char getC3CalcExplnCd();

	void setC3CalcExplnCd(char c3CalcExplnCd);

	/**
	 * Host Variable VOID-C3-PD-AM
	 * 
	 */
	AfDecimal getC3PdAm();

	void setC3PdAm(AfDecimal c3PdAm);

	/**
	 * Host Variable VOID-C3-GL-ACCT-ID
	 * 
	 */
	String getC3GlAcctId();

	void setC3GlAcctId(String c3GlAcctId);

	/**
	 * Host Variable VOID-C3-CORP-CD
	 * 
	 */
	char getC3CorpCd();

	void setC3CorpCd(char c3CorpCd);

	/**
	 * Host Variable VOID-C3-PROD-CD
	 * 
	 */
	String getC3ProdCd();

	void setC3ProdCd(String c3ProdCd);

	/**
	 * Host Variable VOID-C4-CALC-EXPLN-CD
	 * 
	 */
	char getC4CalcExplnCd();

	void setC4CalcExplnCd(char c4CalcExplnCd);

	/**
	 * Host Variable VOID-C4-PD-AM
	 * 
	 */
	AfDecimal getC4PdAm();

	void setC4PdAm(AfDecimal c4PdAm);

	/**
	 * Host Variable VOID-C4-GL-ACCT-ID
	 * 
	 */
	String getC4GlAcctId();

	void setC4GlAcctId(String c4GlAcctId);

	/**
	 * Host Variable VOID-C4-CORP-CD
	 * 
	 */
	char getC4CorpCd();

	void setC4CorpCd(char c4CorpCd);

	/**
	 * Host Variable VOID-C4-PROD-CD
	 * 
	 */
	String getC4ProdCd();

	void setC4ProdCd(String c4ProdCd);

	/**
	 * Host Variable VOID-C5-CALC-EXPLN-CD
	 * 
	 */
	char getC5CalcExplnCd();

	void setC5CalcExplnCd(char c5CalcExplnCd);

	/**
	 * Host Variable VOID-C5-PD-AM
	 * 
	 */
	AfDecimal getC5PdAm();

	void setC5PdAm(AfDecimal c5PdAm);

	/**
	 * Host Variable VOID-C5-GL-ACCT-ID
	 * 
	 */
	String getC5GlAcctId();

	void setC5GlAcctId(String c5GlAcctId);

	/**
	 * Host Variable VOID-C5-CORP-CD
	 * 
	 */
	char getC5CorpCd();

	void setC5CorpCd(char c5CorpCd);

	/**
	 * Host Variable VOID-C5-PROD-CD
	 * 
	 */
	String getC5ProdCd();

	void setC5ProdCd(String c5ProdCd);

	/**
	 * Host Variable VOID-C6-CALC-EXPLN-CD
	 * 
	 */
	char getC6CalcExplnCd();

	void setC6CalcExplnCd(char c6CalcExplnCd);

	/**
	 * Host Variable VOID-C6-PD-AM
	 * 
	 */
	AfDecimal getC6PdAm();

	void setC6PdAm(AfDecimal c6PdAm);

	/**
	 * Host Variable VOID-C6-GL-ACCT-ID
	 * 
	 */
	String getC6GlAcctId();

	void setC6GlAcctId(String c6GlAcctId);

	/**
	 * Host Variable VOID-C6-CORP-CD
	 * 
	 */
	char getC6CorpCd();

	void setC6CorpCd(char c6CorpCd);

	/**
	 * Host Variable VOID-C6-PROD-CD
	 * 
	 */
	String getC6ProdCd();

	void setC6ProdCd(String c6ProdCd);

	/**
	 * Host Variable VOID-C7-CALC-EXPLN-CD
	 * 
	 */
	char getC7CalcExplnCd();

	void setC7CalcExplnCd(char c7CalcExplnCd);

	/**
	 * Host Variable VOID-C7-PD-AM
	 * 
	 */
	AfDecimal getC7PdAm();

	void setC7PdAm(AfDecimal c7PdAm);

	/**
	 * Host Variable VOID-C7-GL-ACCT-ID
	 * 
	 */
	String getC7GlAcctId();

	void setC7GlAcctId(String c7GlAcctId);

	/**
	 * Host Variable VOID-C7-CORP-CD
	 * 
	 */
	char getC7CorpCd();

	void setC7CorpCd(char c7CorpCd);

	/**
	 * Host Variable VOID-C7-PROD-CD
	 * 
	 */
	String getC7ProdCd();

	void setC7ProdCd(String c7ProdCd);

	/**
	 * Host Variable VOID-C8-CALC-EXPLN-CD
	 * 
	 */
	char getC8CalcExplnCd();

	void setC8CalcExplnCd(char c8CalcExplnCd);

	/**
	 * Host Variable VOID-C8-PD-AM
	 * 
	 */
	AfDecimal getC8PdAm();

	void setC8PdAm(AfDecimal c8PdAm);

	/**
	 * Host Variable VOID-C8-GL-ACCT-ID
	 * 
	 */
	String getC8GlAcctId();

	void setC8GlAcctId(String c8GlAcctId);

	/**
	 * Host Variable VOID-C8-CORP-CD
	 * 
	 */
	char getC8CorpCd();

	void setC8CorpCd(char c8CorpCd);

	/**
	 * Host Variable VOID-C8-PROD-CD
	 * 
	 */
	String getC8ProdCd();

	void setC8ProdCd(String c8ProdCd);

	/**
	 * Host Variable VOID-C9-CALC-EXPLN-CD
	 * 
	 */
	char getC9CalcExplnCd();

	void setC9CalcExplnCd(char c9CalcExplnCd);

	/**
	 * Host Variable VOID-C9-PD-AM
	 * 
	 */
	AfDecimal getC9PdAm();

	void setC9PdAm(AfDecimal c9PdAm);

	/**
	 * Host Variable VOID-C9-GL-ACCT-ID
	 * 
	 */
	String getC9GlAcctId();

	void setC9GlAcctId(String c9GlAcctId);

	/**
	 * Host Variable VOID-C9-CORP-CD
	 * 
	 */
	char getC9CorpCd();

	void setC9CorpCd(char c9CorpCd);

	/**
	 * Host Variable VOID-C9-PROD-CD
	 * 
	 */
	String getC9ProdCd();

	void setC9ProdCd(String c9ProdCd);

	/**
	 * Host Variable VOID-LI-NCOV-AM
	 * 
	 */
	AfDecimal getLiNcovAm();

	void setLiNcovAm(AfDecimal liNcovAm);

	/**
	 * Host Variable VOID-CAS-EOB-DED-AM
	 * 
	 */
	AfDecimal getCasEobDedAm();

	void setCasEobDedAm(AfDecimal casEobDedAm);

	/**
	 * Host Variable VOID-CAS-EOB-COINS-AM
	 * 
	 */
	AfDecimal getCasEobCoinsAm();

	void setCasEobCoinsAm(AfDecimal casEobCoinsAm);

	/**
	 * Host Variable VOID-CAS-EOB-COPAY-AM
	 * 
	 */
	AfDecimal getCasEobCopayAm();

	void setCasEobCopayAm(AfDecimal casEobCopayAm);
};
