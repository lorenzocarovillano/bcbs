/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.data.fao;

import com.bphx.ctu.af.io.file.AbstractInputOutputDAO;
import com.bphx.ctu.af.io.file.FileAccessStatus;
import com.myorg.myprj.data.fto.ErrFileTO;

/**Original name: ERR-FILE/ERRORS/ERRORS[NF0533ML]<br>*/
public class ErrFileDAO extends AbstractInputOutputDAO<ErrFileTO> {

	//==== PROPERTIES ====
	public static final int RECORD_SIZE = 200;

	//==== CONSTRUCTORS ====
	public ErrFileDAO(FileAccessStatus fileStatus) {
		super(fileStatus);
	}

	//==== METHODS ====
	@Override
	public String getName() {
		return "ERRORS";
	}

	@Override
	public int getRecordSize() {
		return RECORD_SIZE;
	}

	@Override
	public ErrFileTO createTo() {
		return new ErrFileTO();
	}

	@Override
	public ErrFileTO read() {
		ErrFileTO errFileTO = new ErrFileTO();
		return read(errFileTO);
	}
}
