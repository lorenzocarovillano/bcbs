/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.myorg.myprj.commons.data.to.IS03086sa;

/**Original name: DCLS03086SA<br>
 * Variable: DCLS03086SA from copybook S03086SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls03086sa implements IS03086sa {

	//==== PROPERTIES ====
	//Original name: JOB-NM
	private String jobNm = DefaultValues.stringVal(Len.JOB_NM);
	//Original name: JBSTP-NM
	private String jbstpNm = DefaultValues.stringVal(Len.JBSTP_NM);
	//Original name: JBSTP-SEQ-ID
	private short jbstpSeqId = DefaultValues.SHORT_VAL;
	//Original name: INFO-CHG-TS
	private String infoChgTs = DefaultValues.stringVal(Len.INFO_CHG_TS);
	//Original name: APPL-TRM-CD
	private char applTrmCd = DefaultValues.CHAR_VAL;
	//Original name: RSTRT-HIST-IN
	private char rstrtHistIn = DefaultValues.CHAR_VAL;
	//Original name: ROW-FREQ-CMT-CT
	private int rowFreqCmtCt = DefaultValues.INT_VAL;
	//Original name: CMT-TM-INT-TM
	private String cmtTmIntTm = DefaultValues.stringVal(Len.CMT_TM_INT_TM);
	//Original name: CMT-CT
	private int cmtCt = DefaultValues.INT_VAL;
	//Original name: CMT-TS
	private String cmtTs = DefaultValues.stringVal(Len.CMT_TS);
	//Original name: RSTRT1-TX
	private String rstrt1Tx = DefaultValues.stringVal(Len.RSTRT1_TX);
	//Original name: RSTRT2-TX
	private String rstrt2Tx = DefaultValues.stringVal(Len.RSTRT2_TX);
	//Original name: INFO-CHG-ID
	private String infoChgId = DefaultValues.stringVal(Len.INFO_CHG_ID);

	//==== METHODS ====
	@Override
	public void setJobNm(String jobNm) {
		this.jobNm = Functions.subString(jobNm, Len.JOB_NM);
	}

	@Override
	public String getJobNm() {
		return this.jobNm;
	}

	public String getJobNmFormatted() {
		return Functions.padBlanks(getJobNm(), Len.JOB_NM);
	}

	@Override
	public void setJbstpNm(String jbstpNm) {
		this.jbstpNm = Functions.subString(jbstpNm, Len.JBSTP_NM);
	}

	@Override
	public String getJbstpNm() {
		return this.jbstpNm;
	}

	public String getJbstpNmFormatted() {
		return Functions.padBlanks(getJbstpNm(), Len.JBSTP_NM);
	}

	@Override
	public void setJbstpSeqId(short jbstpSeqId) {
		this.jbstpSeqId = jbstpSeqId;
	}

	@Override
	public short getJbstpSeqId() {
		return this.jbstpSeqId;
	}

	public String getJbstpSeqIdFormatted() {
		return PicFormatter.display(new PicParams("S9(3)V").setUsage(PicUsage.PACKED)).format(getJbstpSeqId()).toString();
	}

	public String getJbstpSeqIdAsString() {
		return getJbstpSeqIdFormatted();
	}

	@Override
	public void setInfoChgTs(String infoChgTs) {
		this.infoChgTs = Functions.subString(infoChgTs, Len.INFO_CHG_TS);
	}

	@Override
	public String getInfoChgTs() {
		return this.infoChgTs;
	}

	public String getInfoChgTsFormatted() {
		return Functions.padBlanks(getInfoChgTs(), Len.INFO_CHG_TS);
	}

	@Override
	public void setApplTrmCd(char applTrmCd) {
		this.applTrmCd = applTrmCd;
	}

	@Override
	public char getApplTrmCd() {
		return this.applTrmCd;
	}

	@Override
	public void setRstrtHistIn(char rstrtHistIn) {
		this.rstrtHistIn = rstrtHistIn;
	}

	@Override
	public char getRstrtHistIn() {
		return this.rstrtHistIn;
	}

	@Override
	public void setRowFreqCmtCt(int rowFreqCmtCt) {
		this.rowFreqCmtCt = rowFreqCmtCt;
	}

	@Override
	public int getRowFreqCmtCt() {
		return this.rowFreqCmtCt;
	}

	public String getRowFreqCmtCtFormatted() {
		return PicFormatter.display(new PicParams("S9(9)V").setUsage(PicUsage.PACKED)).format(getRowFreqCmtCt()).toString();
	}

	public String getRowFreqCmtCtAsString() {
		return getRowFreqCmtCtFormatted();
	}

	@Override
	public void setCmtTmIntTm(String cmtTmIntTm) {
		this.cmtTmIntTm = Functions.subString(cmtTmIntTm, Len.CMT_TM_INT_TM);
	}

	@Override
	public String getCmtTmIntTm() {
		return this.cmtTmIntTm;
	}

	public String getCmtTmIntTmFormatted() {
		return Functions.padBlanks(getCmtTmIntTm(), Len.CMT_TM_INT_TM);
	}

	@Override
	public void setCmtCt(int cmtCt) {
		this.cmtCt = cmtCt;
	}

	@Override
	public int getCmtCt() {
		return this.cmtCt;
	}

	public String getCmtCtFormatted() {
		return PicFormatter.display(new PicParams("S9(9)V").setUsage(PicUsage.PACKED)).format(getCmtCt()).toString();
	}

	public String getCmtCtAsString() {
		return getCmtCtFormatted();
	}

	@Override
	public void setCmtTs(String cmtTs) {
		this.cmtTs = Functions.subString(cmtTs, Len.CMT_TS);
	}

	@Override
	public String getCmtTs() {
		return this.cmtTs;
	}

	public String getCmtTsFormatted() {
		return Functions.padBlanks(getCmtTs(), Len.CMT_TS);
	}

	@Override
	public void setRstrt1Tx(String rstrt1Tx) {
		this.rstrt1Tx = Functions.subString(rstrt1Tx, Len.RSTRT1_TX);
	}

	@Override
	public String getRstrt1Tx() {
		return this.rstrt1Tx;
	}

	public String getRstrt1TxFormatted() {
		return Functions.padBlanks(getRstrt1Tx(), Len.RSTRT1_TX);
	}

	@Override
	public void setRstrt2Tx(String rstrt2Tx) {
		this.rstrt2Tx = Functions.subString(rstrt2Tx, Len.RSTRT2_TX);
	}

	@Override
	public String getRstrt2Tx() {
		return this.rstrt2Tx;
	}

	public String getRstrt2TxFormatted() {
		return Functions.padBlanks(getRstrt2Tx(), Len.RSTRT2_TX);
	}

	@Override
	public void setInfoChgId(String infoChgId) {
		this.infoChgId = Functions.subString(infoChgId, Len.INFO_CHG_ID);
	}

	@Override
	public String getInfoChgId() {
		return this.infoChgId;
	}

	public String getInfoChgIdFormatted() {
		return Functions.padBlanks(getInfoChgId(), Len.INFO_CHG_ID);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int JOB_NM = 8;
		public static final int JBSTP_NM = 8;
		public static final int INFO_CHG_TS = 26;
		public static final int CMT_TM_INT_TM = 8;
		public static final int CMT_TS = 26;
		public static final int RSTRT1_TX = 250;
		public static final int RSTRT2_TX = 250;
		public static final int INFO_CHG_ID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
