/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.ws.Nf05GroupAndAccount12;
import com.myorg.myprj.ws.enums.Nf05AssignedIndc;
import com.myorg.myprj.ws.enums.Nf05ClaimLob;
import com.myorg.myprj.ws.enums.Nf05CrossReimburseIndc;
import com.myorg.myprj.ws.enums.Nf05HistoryLoadIndc;
import com.myorg.myprj.ws.enums.Nf05ItsClaimType;
import com.myorg.myprj.ws.enums.Nf05NationProgramIndc;
import com.myorg.myprj.ws.enums.Nf05OtherInsureCode;
import com.myorg.myprj.ws.enums.Nf05RecordType;
import com.myorg.myprj.ws.enums.Nf05ReviewIndc;

/**Original name: NF05-HEADER-PART-OF-RECORD<br>
 * Variable: NF05-HEADER-PART-OF-RECORD from copybook NF05EOB<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Nf05HeaderPartOfRecord {

	//==== PROPERTIES ====
	//Original name: NF05-PROD-IND
	private String nf05ProdInd = DefaultValues.stringVal(Len.NF05_PROD_IND);
	//Original name: NF05-CLAIM-NUM
	private Nf05ClaimNum nf05ClaimNum = new Nf05ClaimNum();
	//Original name: NF05-CLAIM-SFX
	private char nf05ClaimSfx = DefaultValues.CHAR_VAL;
	//Original name: NF05-PMT-NUM
	private String nf05PmtNum = DefaultValues.stringVal(Len.NF05_PMT_NUM);
	//Original name: NF05-LINE-NUM
	private String nf05LineNum = DefaultValues.stringVal(Len.NF05_LINE_NUM);
	//Original name: NF05-RECORD-TYPE
	private Nf05RecordType nf05RecordType = new Nf05RecordType();
	//Original name: NF05-CLAIM-LOB
	private Nf05ClaimLob nf05ClaimLob = new Nf05ClaimLob();
	//Original name: NF05-ITS-CLAIM-TYPE
	private Nf05ItsClaimType nf05ItsClaimType = new Nf05ItsClaimType();
	//Original name: NF05-REVIEW-INDC
	private Nf05ReviewIndc nf05ReviewIndc = new Nf05ReviewIndc();
	//Original name: NF05-PROV-TYP-CN-CD
	private String nf05ProvTypCnCd = DefaultValues.stringVal(Len.NF05_PROV_TYP_CN_CD);
	//Original name: NF05-INST-REIMB-MTHD-CD
	private String nf05InstReimbMthdCd = DefaultValues.stringVal(Len.NF05_INST_REIMB_MTHD_CD);
	//Original name: NF05-PROF-REIMB-MTHD-CD
	private String nf05ProfReimbMthdCd = DefaultValues.stringVal(Len.NF05_PROF_REIMB_MTHD_CD);
	//Original name: NF05-FILLER
	private String nf05Filler = DefaultValues.stringVal(Len.NF05_FILLER);
	//Original name: NF05-TYPE-GROUP
	private String nf05TypeGroup = DefaultValues.stringVal(Len.NF05_TYPE_GROUP);
	//Original name: NF05-PLACE-SERVICE-WHY
	private String nf05PlaceServiceWhy = DefaultValues.stringVal(Len.NF05_PLACE_SERVICE_WHY);
	//Original name: NF05-FEP-REFUND-WITHHELD-AMT
	private AfDecimal nf05FepRefundWithheldAmt = new AfDecimal(DefaultValues.DEC_VAL, 8, 2);
	//Original name: NF05-FEP-DBR-PRV-RNT-DT
	private int nf05FepDbrPrvRntDt = DefaultValues.INT_VAL;
	//Original name: NF05-FEP-MANAGED-CARE-AMT
	private AfDecimal nf05FepManagedCareAmt = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: NF05-HISTORY-LOAD-INDC
	private Nf05HistoryLoadIndc nf05HistoryLoadIndc = new Nf05HistoryLoadIndc();
	//Original name: NF05-TEAM
	private String nf05Team = DefaultValues.stringVal(Len.NF05_TEAM);
	//Original name: NF05-ID
	private String nf05Id = DefaultValues.stringVal(Len.NF05_ID);
	//Original name: NF05-ASSIGNED-INDC
	private Nf05AssignedIndc nf05AssignedIndc = new Nf05AssignedIndc();
	//Original name: NF05-GROUP-AND-ACCOUNT-12
	private Nf05GroupAndAccount12 nf05GroupAndAccount12 = new Nf05GroupAndAccount12();
	//Original name: NF05-DATE-RECEIVED
	private String nf05DateReceived = DefaultValues.stringVal(Len.NF05_DATE_RECEIVED);
	//Original name: NF05-DATE-PAID
	private String nf05DatePaid = DefaultValues.stringVal(Len.NF05_DATE_PAID);
	//Original name: NF05-GL-OFFST-CD
	private String nf05GlOffstCd = DefaultValues.stringVal(Len.NF05_GL_OFFST_CD);
	//Original name: NF05-INSURED-ID-PREFIX
	private String nf05InsuredIdPrefix = DefaultValues.stringVal(Len.NF05_INSURED_ID_PREFIX);
	//Original name: NF05-INSURED-ID-DIGITS
	private String nf05InsuredIdDigits = DefaultValues.stringVal(Len.NF05_INSURED_ID_DIGITS);
	//Original name: NF05-SUBSCRIBER-NAME
	private Nf05SubscriberName nf05SubscriberName = new Nf05SubscriberName();
	//Original name: NF05-ADDRESS
	private Nf05Address nf05Address = new Nf05Address();
	//Original name: NF05-MEMBER-ID
	private String nf05MemberId = DefaultValues.stringVal(Len.NF05_MEMBER_ID);
	//Original name: NF05-3RD-PTY-PAYEE-DATA
	private Nf053rdPtyPayeeData nf053rdPtyPayeeData = new Nf053rdPtyPayeeData();
	//Original name: NF05-PROVIDER-NAME
	private String nf05ProviderName = DefaultValues.stringVal(Len.NF05_PROVIDER_NAME);
	//Original name: FILLER-NF05-HEADER-PART-OF-RECORD
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: NF05-CLFM-RUN
	private String nf05ClfmRun = DefaultValues.stringVal(Len.NF05_CLFM_RUN);
	//Original name: NF05-CLFM-ST
	private char nf05ClfmSt = DefaultValues.CHAR_VAL;
	//Original name: NF05-AMOUNT-PAID
	private AfDecimal nf05AmountPaid = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: NF05-PRMPT-PAY-INT-AM
	private AfDecimal nf05PrmptPayIntAm = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: NF05-PATIENT-ACCOUNT-NUM
	private String nf05PatientAccountNum = DefaultValues.stringVal(Len.NF05_PATIENT_ACCOUNT_NUM);
	//Original name: NF05-PATIENT-RELATION
	private String nf05PatientRelation = DefaultValues.stringVal(Len.NF05_PATIENT_RELATION);
	//Original name: NF05-LINE-PROV-WRITEOFF
	private AfDecimal nf05LineProvWriteoff = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: NF05-LINE-PAT-RESPONS
	private AfDecimal nf05LinePatRespons = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: NF05-CROSS-REIMBURSE-INDC
	private Nf05CrossReimburseIndc nf05CrossReimburseIndc = new Nf05CrossReimburseIndc();
	//Original name: NF05-SHIELD-REIMBURSE-INDC
	private Nf05CrossReimburseIndc nf05ShieldReimburseIndc = new Nf05CrossReimburseIndc();
	//Original name: NF05-NATION-PROGRAM-INDC
	private Nf05NationProgramIndc nf05NationProgramIndc = new Nf05NationProgramIndc();
	//Original name: NF05-PERF-PROVIDER-NUM
	private String nf05PerfProviderNum = DefaultValues.stringVal(Len.NF05_PERF_PROVIDER_NUM);
	//Original name: NF05-DIAGNOSIS-CODES-DATA
	private Nf05DiagnosisCodesData nf05DiagnosisCodesData = new Nf05DiagnosisCodesData();
	//Original name: NF05-BAL-BILL-AM
	private AfDecimal nf05BalBillAm = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: NF05-PATIENT-NAME
	private Nf05SubscriberName nf05PatientName = new Nf05SubscriberName();
	//Original name: NF05-RECIEPT-DATE
	private Nf05RecieptDate nf05RecieptDate = new Nf05RecieptDate();
	//Original name: NF05-ADJUDICATION-DATE
	private Nf05RecieptDate nf05AdjudicationDate = new Nf05RecieptDate();
	//Original name: NF05-BILLING-FROM-DATE
	private Nf05RecieptDate nf05BillingFromDate = new Nf05RecieptDate();
	//Original name: NF05-BILLING-THRU-DATE
	private Nf05RecieptDate nf05BillingThruDate = new Nf05RecieptDate();
	//Original name: NF05-MT-QT-INDICATOR
	private char nf05MtQtIndicator = DefaultValues.CHAR_VAL;
	//Original name: NF05-ADJ-TYP-CD
	private char nf05AdjTypCd = DefaultValues.CHAR_VAL;
	//Original name: NF05-BASE-CA-CD
	private String nf05BaseCaCd = DefaultValues.stringVal(Len.NF05_BASE_CA_CD);
	//Original name: NF05-RATE-CD
	private String nf05RateCd = DefaultValues.stringVal(Len.NF05_RATE_CD);
	//Original name: NF05-OTHER-INSURE-CODE
	private Nf05OtherInsureCode nf05OtherInsureCode = new Nf05OtherInsureCode();
	//Original name: NF05-PROV-STOP-PAY-CD
	private String nf05ProvStopPayCd = DefaultValues.stringVal(Len.NF05_PROV_STOP_PAY_CD);
	//Original name: NF05-WAS-AN-N2-CLAIM
	private char nf05WasAnN2Claim = DefaultValues.CHAR_VAL;
	//Original name: NF05-TYP-GRP-CD
	private String nf05TypGrpCd = DefaultValues.stringVal(Len.NF05_TYP_GRP_CD);
	//Original name: NF05-CLM-SYST-VER-ID
	private String nf05ClmSystVerId = DefaultValues.stringVal(Len.NF05_CLM_SYST_VER_ID);
	//Original name: NF05-CLM-CORP-CODE
	private char nf05ClmCorpCode = DefaultValues.CHAR_VAL;
	//Original name: FILLER-NF05-HEADER-PART-OF-RECORD-1
	private String flr2 = DefaultValues.stringVal(Len.FLR2);

	//==== METHODS ====
	public byte[] getNf05HeaderPartOfRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, nf05ProdInd, Len.NF05_PROD_IND);
		position += Len.NF05_PROD_IND;
		nf05ClaimNum.getNf05ClaimNumBytes(buffer, position);
		position += Nf05ClaimNum.Len.NF05_CLAIM_NUM;
		MarshalByte.writeChar(buffer, position, nf05ClaimSfx);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nf05PmtNum, Len.NF05_PMT_NUM);
		position += Len.NF05_PMT_NUM;
		MarshalByte.writeString(buffer, position, nf05LineNum, Len.NF05_LINE_NUM);
		position += Len.NF05_LINE_NUM;
		MarshalByte.writeChar(buffer, position, nf05RecordType.getNf05RecordType());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, nf05ClaimLob.getNf05ClaimLob());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, nf05ItsClaimType.getNf05ItsClaimType());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, nf05ReviewIndc.getNf05ReviewIndc());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nf05ProvTypCnCd, Len.NF05_PROV_TYP_CN_CD);
		position += Len.NF05_PROV_TYP_CN_CD;
		MarshalByte.writeString(buffer, position, nf05InstReimbMthdCd, Len.NF05_INST_REIMB_MTHD_CD);
		position += Len.NF05_INST_REIMB_MTHD_CD;
		MarshalByte.writeString(buffer, position, nf05ProfReimbMthdCd, Len.NF05_PROF_REIMB_MTHD_CD);
		position += Len.NF05_PROF_REIMB_MTHD_CD;
		MarshalByte.writeString(buffer, position, nf05Filler, Len.NF05_FILLER);
		position += Len.NF05_FILLER;
		MarshalByte.writeString(buffer, position, nf05TypeGroup, Len.NF05_TYPE_GROUP);
		position += Len.NF05_TYPE_GROUP;
		MarshalByte.writeString(buffer, position, nf05PlaceServiceWhy, Len.NF05_PLACE_SERVICE_WHY);
		position += Len.NF05_PLACE_SERVICE_WHY;
		MarshalByte.writeDecimal(buffer, position, nf05FepRefundWithheldAmt.copy());
		position += Len.NF05_FEP_REFUND_WITHHELD_AMT;
		MarshalByte.writeInt(buffer, position, nf05FepDbrPrvRntDt, Len.NF05_FEP_DBR_PRV_RNT_DT);
		position += Len.NF05_FEP_DBR_PRV_RNT_DT;
		MarshalByte.writeDecimal(buffer, position, nf05FepManagedCareAmt.copy());
		position += Len.NF05_FEP_MANAGED_CARE_AMT;
		MarshalByte.writeChar(buffer, position, nf05HistoryLoadIndc.getNf05HistoryLoadIndc());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nf05Team, Len.NF05_TEAM);
		position += Len.NF05_TEAM;
		MarshalByte.writeString(buffer, position, nf05Id, Len.NF05_ID);
		position += Len.NF05_ID;
		MarshalByte.writeString(buffer, position, nf05AssignedIndc.getNf05AssignedIndc(), Nf05AssignedIndc.Len.NF05_ASSIGNED_INDC);
		position += Nf05AssignedIndc.Len.NF05_ASSIGNED_INDC;
		nf05GroupAndAccount12.getNf05GroupAndAccount12Bytes(buffer, position);
		position += Nf05GroupAndAccount12.Len.NF05_GROUP_AND_ACCOUNT12;
		MarshalByte.writeString(buffer, position, nf05DateReceived, Len.NF05_DATE_RECEIVED);
		position += Len.NF05_DATE_RECEIVED;
		MarshalByte.writeString(buffer, position, nf05DatePaid, Len.NF05_DATE_PAID);
		position += Len.NF05_DATE_PAID;
		MarshalByte.writeString(buffer, position, nf05GlOffstCd, Len.NF05_GL_OFFST_CD);
		position += Len.NF05_GL_OFFST_CD;
		getNf05InsuredIdBytes(buffer, position);
		position += Len.NF05_INSURED_ID;
		nf05SubscriberName.getDetlPatientNameBytes(buffer, position);
		position += Nf05SubscriberName.Len.DETL_PATIENT_NAME;
		nf05Address.getNf05AddressBytes(buffer, position);
		position += Nf05Address.Len.NF05_ADDRESS;
		MarshalByte.writeString(buffer, position, nf05MemberId, Len.NF05_MEMBER_ID);
		position += Len.NF05_MEMBER_ID;
		nf053rdPtyPayeeData.getNf053rdPtyPayeeDataBytes(buffer, position);
		position += Nf053rdPtyPayeeData.Len.NF053RD_PTY_PAYEE_DATA;
		MarshalByte.writeString(buffer, position, nf05ProviderName, Len.NF05_PROVIDER_NAME);
		position += Len.NF05_PROVIDER_NAME;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		getNf05ClaimFormInsertBytes(buffer, position);
		position += Len.NF05_CLAIM_FORM_INSERT;
		MarshalByte.writeDecimal(buffer, position, nf05AmountPaid.copy());
		position += Len.NF05_AMOUNT_PAID;
		MarshalByte.writeDecimal(buffer, position, nf05PrmptPayIntAm.copy());
		position += Len.NF05_PRMPT_PAY_INT_AM;
		MarshalByte.writeString(buffer, position, nf05PatientAccountNum, Len.NF05_PATIENT_ACCOUNT_NUM);
		position += Len.NF05_PATIENT_ACCOUNT_NUM;
		MarshalByte.writeString(buffer, position, nf05PatientRelation, Len.NF05_PATIENT_RELATION);
		position += Len.NF05_PATIENT_RELATION;
		MarshalByte.writeDecimal(buffer, position, nf05LineProvWriteoff.copy());
		position += Len.NF05_LINE_PROV_WRITEOFF;
		MarshalByte.writeDecimal(buffer, position, nf05LinePatRespons.copy());
		position += Len.NF05_LINE_PAT_RESPONS;
		MarshalByte.writeString(buffer, position, nf05CrossReimburseIndc.getNf05CrossReimburseIndc(),
				Nf05CrossReimburseIndc.Len.NF05_CROSS_REIMBURSE_INDC);
		position += Nf05CrossReimburseIndc.Len.NF05_CROSS_REIMBURSE_INDC;
		MarshalByte.writeString(buffer, position, nf05ShieldReimburseIndc.getNf05CrossReimburseIndc(),
				Nf05CrossReimburseIndc.Len.NF05_CROSS_REIMBURSE_INDC);
		position += Nf05CrossReimburseIndc.Len.NF05_CROSS_REIMBURSE_INDC;
		MarshalByte.writeChar(buffer, position, nf05NationProgramIndc.getNf05NationProgramIndc());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nf05PerfProviderNum, Len.NF05_PERF_PROVIDER_NUM);
		position += Len.NF05_PERF_PROVIDER_NUM;
		nf05DiagnosisCodesData.getNf05DiagnosisCodesDataBytes(buffer, position);
		position += Nf05DiagnosisCodesData.Len.NF05_DIAGNOSIS_CODES_DATA;
		MarshalByte.writeDecimal(buffer, position, nf05BalBillAm.copy(), SignType.NO_SIGN);
		position += Len.NF05_BAL_BILL_AM;
		nf05PatientName.getDetlPatientNameBytes(buffer, position);
		position += Nf05SubscriberName.Len.DETL_PATIENT_NAME;
		nf05RecieptDate.getExpWrapDateCcyyMmDdBytes(buffer, position);
		position += Nf05RecieptDate.Len.NF05_RECIEPT_DATE;
		nf05AdjudicationDate.getExpWrapDateCcyyMmDdBytes(buffer, position);
		position += Nf05RecieptDate.Len.NF05_RECIEPT_DATE;
		nf05BillingFromDate.getExpWrapDateCcyyMmDdBytes(buffer, position);
		position += Nf05RecieptDate.Len.NF05_RECIEPT_DATE;
		nf05BillingThruDate.getExpWrapDateCcyyMmDdBytes(buffer, position);
		position += Nf05RecieptDate.Len.NF05_RECIEPT_DATE;
		MarshalByte.writeChar(buffer, position, nf05MtQtIndicator);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, nf05AdjTypCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nf05BaseCaCd, Len.NF05_BASE_CA_CD);
		position += Len.NF05_BASE_CA_CD;
		MarshalByte.writeString(buffer, position, nf05RateCd, Len.NF05_RATE_CD);
		position += Len.NF05_RATE_CD;
		MarshalByte.writeChar(buffer, position, nf05OtherInsureCode.getNf05OtherInsureCode());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nf05ProvStopPayCd, Len.NF05_PROV_STOP_PAY_CD);
		position += Len.NF05_PROV_STOP_PAY_CD;
		MarshalByte.writeChar(buffer, position, nf05WasAnN2Claim);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nf05TypGrpCd, Len.NF05_TYP_GRP_CD);
		position += Len.NF05_TYP_GRP_CD;
		MarshalByte.writeString(buffer, position, nf05ClmSystVerId, Len.NF05_CLM_SYST_VER_ID);
		position += Len.NF05_CLM_SYST_VER_ID;
		MarshalByte.writeChar(buffer, position, nf05ClmCorpCode);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		return buffer;
	}

	public void setNf05ProdInd(String nf05ProdInd) {
		this.nf05ProdInd = Functions.subString(nf05ProdInd, Len.NF05_PROD_IND);
	}

	public String getNf05ProdInd() {
		return this.nf05ProdInd;
	}

	public void setNf05ClaimSfx(char nf05ClaimSfx) {
		this.nf05ClaimSfx = nf05ClaimSfx;
	}

	public char getNf05ClaimSfx() {
		return this.nf05ClaimSfx;
	}

	public void setNf05PmtNum(short nf05PmtNum) {
		this.nf05PmtNum = NumericDisplay.asString(nf05PmtNum, Len.NF05_PMT_NUM);
	}

	public void setNf05PmtNumFormatted(String nf05PmtNum) {
		this.nf05PmtNum = Trunc.toUnsignedNumeric(nf05PmtNum, Len.NF05_PMT_NUM);
	}

	public short getNf05PmtNum() {
		return NumericDisplay.asShort(this.nf05PmtNum);
	}

	public String getNf05PmtNumFormatted() {
		return this.nf05PmtNum;
	}

	public String getNf05PmtNumAsString() {
		return getNf05PmtNumFormatted();
	}

	public void setNf05LineNum(int nf05LineNum) {
		this.nf05LineNum = NumericDisplay.asString(nf05LineNum, Len.NF05_LINE_NUM);
	}

	public void setNf05LineNumFormatted(String nf05LineNum) {
		this.nf05LineNum = Trunc.toUnsignedNumeric(nf05LineNum, Len.NF05_LINE_NUM);
	}

	public int getNf05LineNum() {
		return NumericDisplay.asInt(this.nf05LineNum);
	}

	public void setNf05ProvTypCnCd(String nf05ProvTypCnCd) {
		this.nf05ProvTypCnCd = Functions.subString(nf05ProvTypCnCd, Len.NF05_PROV_TYP_CN_CD);
	}

	public String getNf05ProvTypCnCd() {
		return this.nf05ProvTypCnCd;
	}

	public void setNf05InstReimbMthdCd(String nf05InstReimbMthdCd) {
		this.nf05InstReimbMthdCd = Functions.subString(nf05InstReimbMthdCd, Len.NF05_INST_REIMB_MTHD_CD);
	}

	public String getNf05InstReimbMthdCd() {
		return this.nf05InstReimbMthdCd;
	}

	public void setNf05ProfReimbMthdCd(String nf05ProfReimbMthdCd) {
		this.nf05ProfReimbMthdCd = Functions.subString(nf05ProfReimbMthdCd, Len.NF05_PROF_REIMB_MTHD_CD);
	}

	public String getNf05ProfReimbMthdCd() {
		return this.nf05ProfReimbMthdCd;
	}

	public void setNf05Filler(String nf05Filler) {
		this.nf05Filler = Functions.subString(nf05Filler, Len.NF05_FILLER);
	}

	public String getNf05Filler() {
		return this.nf05Filler;
	}

	public void setNf05TypeGroup(String nf05TypeGroup) {
		this.nf05TypeGroup = Functions.subString(nf05TypeGroup, Len.NF05_TYPE_GROUP);
	}

	public String getNf05TypeGroup() {
		return this.nf05TypeGroup;
	}

	public void setNf05PlaceServiceWhy(String nf05PlaceServiceWhy) {
		this.nf05PlaceServiceWhy = Functions.subString(nf05PlaceServiceWhy, Len.NF05_PLACE_SERVICE_WHY);
	}

	public String getNf05PlaceServiceWhy() {
		return this.nf05PlaceServiceWhy;
	}

	public void setNf05FepRefundWithheldAmt(AfDecimal nf05FepRefundWithheldAmt) {
		this.nf05FepRefundWithheldAmt.assign(nf05FepRefundWithheldAmt);
	}

	public AfDecimal getNf05FepRefundWithheldAmt() {
		return this.nf05FepRefundWithheldAmt.copy();
	}

	public void setNf05FepDbrPrvRntDt(int nf05FepDbrPrvRntDt) {
		this.nf05FepDbrPrvRntDt = nf05FepDbrPrvRntDt;
	}

	public int getNf05FepDbrPrvRntDt() {
		return this.nf05FepDbrPrvRntDt;
	}

	public void setNf05FepManagedCareAmt(AfDecimal nf05FepManagedCareAmt) {
		this.nf05FepManagedCareAmt.assign(nf05FepManagedCareAmt);
	}

	public AfDecimal getNf05FepManagedCareAmt() {
		return this.nf05FepManagedCareAmt.copy();
	}

	public void setNf05Team(String nf05Team) {
		this.nf05Team = Functions.subString(nf05Team, Len.NF05_TEAM);
	}

	public String getNf05Team() {
		return this.nf05Team;
	}

	public void setNf05Id(String nf05Id) {
		this.nf05Id = Functions.subString(nf05Id, Len.NF05_ID);
	}

	public String getNf05Id() {
		return this.nf05Id;
	}

	public void setNf05DateReceived(String nf05DateReceived) {
		this.nf05DateReceived = Functions.subString(nf05DateReceived, Len.NF05_DATE_RECEIVED);
	}

	public String getNf05DateReceived() {
		return this.nf05DateReceived;
	}

	public void setNf05DatePaid(String nf05DatePaid) {
		this.nf05DatePaid = Functions.subString(nf05DatePaid, Len.NF05_DATE_PAID);
	}

	public String getNf05DatePaid() {
		return this.nf05DatePaid;
	}

	public void setNf05GlOffstCd(String nf05GlOffstCd) {
		this.nf05GlOffstCd = Functions.subString(nf05GlOffstCd, Len.NF05_GL_OFFST_CD);
	}

	public String getNf05GlOffstCd() {
		return this.nf05GlOffstCd;
	}

	public byte[] getNf05InsuredIdBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, nf05InsuredIdPrefix, Len.NF05_INSURED_ID_PREFIX);
		position += Len.NF05_INSURED_ID_PREFIX;
		MarshalByte.writeString(buffer, position, nf05InsuredIdDigits, Len.NF05_INSURED_ID_DIGITS);
		return buffer;
	}

	public void setNf05InsuredIdPrefix(String nf05InsuredIdPrefix) {
		this.nf05InsuredIdPrefix = Functions.subString(nf05InsuredIdPrefix, Len.NF05_INSURED_ID_PREFIX);
	}

	public String getNf05InsuredIdPrefix() {
		return this.nf05InsuredIdPrefix;
	}

	public void setNf05InsuredIdDigits(String nf05InsuredIdDigits) {
		this.nf05InsuredIdDigits = Functions.subString(nf05InsuredIdDigits, Len.NF05_INSURED_ID_DIGITS);
	}

	public String getNf05InsuredIdDigits() {
		return this.nf05InsuredIdDigits;
	}

	public void setNf05MemberId(String nf05MemberId) {
		this.nf05MemberId = Functions.subString(nf05MemberId, Len.NF05_MEMBER_ID);
	}

	public String getNf05MemberId() {
		return this.nf05MemberId;
	}

	public void setNf05ProviderName(String nf05ProviderName) {
		this.nf05ProviderName = Functions.subString(nf05ProviderName, Len.NF05_PROVIDER_NAME);
	}

	public String getNf05ProviderName() {
		return this.nf05ProviderName;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setNf05ClaimFormInsertFormatted(String data) {
		byte[] buffer = new byte[Len.NF05_CLAIM_FORM_INSERT];
		MarshalByte.writeString(buffer, 1, data, Len.NF05_CLAIM_FORM_INSERT);
		setNf05ClaimFormInsertBytes(buffer, 1);
	}

	public void setNf05ClaimFormInsertBytes(byte[] buffer, int offset) {
		int position = offset;
		nf05ClfmRun = MarshalByte.readString(buffer, position, Len.NF05_CLFM_RUN);
		position += Len.NF05_CLFM_RUN;
		nf05ClfmSt = MarshalByte.readChar(buffer, position);
	}

	public byte[] getNf05ClaimFormInsertBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, nf05ClfmRun, Len.NF05_CLFM_RUN);
		position += Len.NF05_CLFM_RUN;
		MarshalByte.writeChar(buffer, position, nf05ClfmSt);
		return buffer;
	}

	public void setNf05ClfmRun(String nf05ClfmRun) {
		this.nf05ClfmRun = Functions.subString(nf05ClfmRun, Len.NF05_CLFM_RUN);
	}

	public String getNf05ClfmRun() {
		return this.nf05ClfmRun;
	}

	public void setNf05ClfmSt(char nf05ClfmSt) {
		this.nf05ClfmSt = nf05ClfmSt;
	}

	public char getNf05ClfmSt() {
		return this.nf05ClfmSt;
	}

	public void setNf05AmountPaid(AfDecimal nf05AmountPaid) {
		this.nf05AmountPaid.assign(nf05AmountPaid);
	}

	public AfDecimal getNf05AmountPaid() {
		return this.nf05AmountPaid.copy();
	}

	public void setNf05PrmptPayIntAm(AfDecimal nf05PrmptPayIntAm) {
		this.nf05PrmptPayIntAm.assign(nf05PrmptPayIntAm);
	}

	public AfDecimal getNf05PrmptPayIntAm() {
		return this.nf05PrmptPayIntAm.copy();
	}

	public void setNf05PatientAccountNum(String nf05PatientAccountNum) {
		this.nf05PatientAccountNum = Functions.subString(nf05PatientAccountNum, Len.NF05_PATIENT_ACCOUNT_NUM);
	}

	public String getNf05PatientAccountNum() {
		return this.nf05PatientAccountNum;
	}

	public void setNf05PatientRelation(String nf05PatientRelation) {
		this.nf05PatientRelation = Functions.subString(nf05PatientRelation, Len.NF05_PATIENT_RELATION);
	}

	public String getNf05PatientRelation() {
		return this.nf05PatientRelation;
	}

	public void setNf05LineProvWriteoff(AfDecimal nf05LineProvWriteoff) {
		this.nf05LineProvWriteoff.assign(nf05LineProvWriteoff);
	}

	public AfDecimal getNf05LineProvWriteoff() {
		return this.nf05LineProvWriteoff.copy();
	}

	public void setNf05LinePatRespons(AfDecimal nf05LinePatRespons) {
		this.nf05LinePatRespons.assign(nf05LinePatRespons);
	}

	public AfDecimal getNf05LinePatRespons() {
		return this.nf05LinePatRespons.copy();
	}

	public void setNf05PerfProviderNum(String nf05PerfProviderNum) {
		this.nf05PerfProviderNum = Functions.subString(nf05PerfProviderNum, Len.NF05_PERF_PROVIDER_NUM);
	}

	public String getNf05PerfProviderNum() {
		return this.nf05PerfProviderNum;
	}

	public void setNf05BalBillAm(AfDecimal nf05BalBillAm) {
		this.nf05BalBillAm.assign(nf05BalBillAm);
	}

	public AfDecimal getNf05BalBillAm() {
		return this.nf05BalBillAm.copy();
	}

	public void setNf05MtQtIndicator(char nf05MtQtIndicator) {
		this.nf05MtQtIndicator = nf05MtQtIndicator;
	}

	public char getNf05MtQtIndicator() {
		return this.nf05MtQtIndicator;
	}

	public void setNf05AdjTypCd(char nf05AdjTypCd) {
		this.nf05AdjTypCd = nf05AdjTypCd;
	}

	public char getNf05AdjTypCd() {
		return this.nf05AdjTypCd;
	}

	public void setNf05BaseCaCd(String nf05BaseCaCd) {
		this.nf05BaseCaCd = Functions.subString(nf05BaseCaCd, Len.NF05_BASE_CA_CD);
	}

	public String getNf05BaseCaCd() {
		return this.nf05BaseCaCd;
	}

	public void setNf05RateCd(String nf05RateCd) {
		this.nf05RateCd = Functions.subString(nf05RateCd, Len.NF05_RATE_CD);
	}

	public String getNf05RateCd() {
		return this.nf05RateCd;
	}

	public void setNf05ProvStopPayCd(String nf05ProvStopPayCd) {
		this.nf05ProvStopPayCd = Functions.subString(nf05ProvStopPayCd, Len.NF05_PROV_STOP_PAY_CD);
	}

	public String getNf05ProvStopPayCd() {
		return this.nf05ProvStopPayCd;
	}

	public void setNf05WasAnN2Claim(char nf05WasAnN2Claim) {
		this.nf05WasAnN2Claim = nf05WasAnN2Claim;
	}

	public char getNf05WasAnN2Claim() {
		return this.nf05WasAnN2Claim;
	}

	public void setNf05TypGrpCd(String nf05TypGrpCd) {
		this.nf05TypGrpCd = Functions.subString(nf05TypGrpCd, Len.NF05_TYP_GRP_CD);
	}

	public String getNf05TypGrpCd() {
		return this.nf05TypGrpCd;
	}

	public void setNf05ClmSystVerId(String nf05ClmSystVerId) {
		this.nf05ClmSystVerId = Functions.subString(nf05ClmSystVerId, Len.NF05_CLM_SYST_VER_ID);
	}

	public String getNf05ClmSystVerId() {
		return this.nf05ClmSystVerId;
	}

	public void setNf05ClmCorpCode(char nf05ClmCorpCode) {
		this.nf05ClmCorpCode = nf05ClmCorpCode;
	}

	public char getNf05ClmCorpCode() {
		return this.nf05ClmCorpCode;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public Nf053rdPtyPayeeData getNf053rdPtyPayeeData() {
		return nf053rdPtyPayeeData;
	}

	public Nf05Address getNf05Address() {
		return nf05Address;
	}

	public Nf05RecieptDate getNf05AdjudicationDate() {
		return nf05AdjudicationDate;
	}

	public Nf05AssignedIndc getNf05AssignedIndc() {
		return nf05AssignedIndc;
	}

	public Nf05RecieptDate getNf05BillingFromDate() {
		return nf05BillingFromDate;
	}

	public Nf05RecieptDate getNf05BillingThruDate() {
		return nf05BillingThruDate;
	}

	public Nf05ClaimLob getNf05ClaimLob() {
		return nf05ClaimLob;
	}

	public Nf05ClaimNum getNf05ClaimNum() {
		return nf05ClaimNum;
	}

	public Nf05CrossReimburseIndc getNf05CrossReimburseIndc() {
		return nf05CrossReimburseIndc;
	}

	public Nf05DiagnosisCodesData getNf05DiagnosisCodesData() {
		return nf05DiagnosisCodesData;
	}

	public Nf05GroupAndAccount12 getNf05GroupAndAccount12() {
		return nf05GroupAndAccount12;
	}

	public Nf05HistoryLoadIndc getNf05HistoryLoadIndc() {
		return nf05HistoryLoadIndc;
	}

	public Nf05ItsClaimType getNf05ItsClaimType() {
		return nf05ItsClaimType;
	}

	public Nf05NationProgramIndc getNf05NationProgramIndc() {
		return nf05NationProgramIndc;
	}

	public Nf05OtherInsureCode getNf05OtherInsureCode() {
		return nf05OtherInsureCode;
	}

	public Nf05SubscriberName getNf05PatientName() {
		return nf05PatientName;
	}

	public Nf05RecieptDate getNf05RecieptDate() {
		return nf05RecieptDate;
	}

	public Nf05RecordType getNf05RecordType() {
		return nf05RecordType;
	}

	public Nf05ReviewIndc getNf05ReviewIndc() {
		return nf05ReviewIndc;
	}

	public Nf05CrossReimburseIndc getNf05ShieldReimburseIndc() {
		return nf05ShieldReimburseIndc;
	}

	public Nf05SubscriberName getNf05SubscriberName() {
		return nf05SubscriberName;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NF05_PROD_IND = 3;
		public static final int NF05_PMT_NUM = 2;
		public static final int NF05_LINE_NUM = 5;
		public static final int NF05_PROV_TYP_CN_CD = 5;
		public static final int NF05_INST_REIMB_MTHD_CD = 2;
		public static final int NF05_PROF_REIMB_MTHD_CD = 2;
		public static final int NF05_FILLER = 5;
		public static final int NF05_TYPE_GROUP = 3;
		public static final int NF05_PLACE_SERVICE_WHY = 6;
		public static final int NF05_TEAM = 5;
		public static final int NF05_ID = 3;
		public static final int NF05_DATE_RECEIVED = 10;
		public static final int NF05_DATE_PAID = 10;
		public static final int NF05_GL_OFFST_CD = 2;
		public static final int NF05_INSURED_ID_PREFIX = 3;
		public static final int NF05_INSURED_ID_DIGITS = 14;
		public static final int NF05_MEMBER_ID = 14;
		public static final int NF05_PROVIDER_NAME = 25;
		public static final int FLR1 = 7;
		public static final int NF05_CLFM_RUN = 2;
		public static final int NF05_PATIENT_ACCOUNT_NUM = 17;
		public static final int NF05_PATIENT_RELATION = 14;
		public static final int NF05_PERF_PROVIDER_NUM = 10;
		public static final int NF05_BASE_CA_CD = 5;
		public static final int NF05_RATE_CD = 2;
		public static final int NF05_PROV_STOP_PAY_CD = 2;
		public static final int NF05_TYP_GRP_CD = 3;
		public static final int NF05_CLM_SYST_VER_ID = 6;
		public static final int FLR2 = 6;
		public static final int NF05_CLFM_ST = 1;
		public static final int NF05_CLAIM_FORM_INSERT = NF05_CLFM_RUN + NF05_CLFM_ST;
		public static final int NF05_CLAIM_SFX = 1;
		public static final int NF05_FEP_REFUND_WITHHELD_AMT = 8;
		public static final int NF05_FEP_DBR_PRV_RNT_DT = 5;
		public static final int NF05_FEP_MANAGED_CARE_AMT = 9;
		public static final int NF05_INSURED_ID = NF05_INSURED_ID_PREFIX + NF05_INSURED_ID_DIGITS;
		public static final int NF05_AMOUNT_PAID = 9;
		public static final int NF05_PRMPT_PAY_INT_AM = 9;
		public static final int NF05_LINE_PROV_WRITEOFF = 9;
		public static final int NF05_LINE_PAT_RESPONS = 9;
		public static final int NF05_BAL_BILL_AM = 9;
		public static final int NF05_MT_QT_INDICATOR = 1;
		public static final int NF05_ADJ_TYP_CD = 1;
		public static final int NF05_WAS_AN_N2_CLAIM = 1;
		public static final int NF05_CLM_CORP_CODE = 1;
		public static final int NF05_HEADER_PART_OF_RECORD = NF05_PROD_IND + Nf05ClaimNum.Len.NF05_CLAIM_NUM + NF05_CLAIM_SFX + NF05_PMT_NUM
				+ NF05_LINE_NUM + Nf05RecordType.Len.NF05_RECORD_TYPE + Nf05ClaimLob.Len.NF05_CLAIM_LOB + Nf05ItsClaimType.Len.NF05_ITS_CLAIM_TYPE
				+ Nf05ReviewIndc.Len.NF05_REVIEW_INDC + NF05_PROV_TYP_CN_CD + NF05_INST_REIMB_MTHD_CD + NF05_PROF_REIMB_MTHD_CD + NF05_FILLER
				+ NF05_TYPE_GROUP + NF05_PLACE_SERVICE_WHY + NF05_FEP_REFUND_WITHHELD_AMT + NF05_FEP_DBR_PRV_RNT_DT + NF05_FEP_MANAGED_CARE_AMT
				+ Nf05HistoryLoadIndc.Len.NF05_HISTORY_LOAD_INDC + NF05_TEAM + NF05_ID + Nf05AssignedIndc.Len.NF05_ASSIGNED_INDC
				+ Nf05GroupAndAccount12.Len.NF05_GROUP_AND_ACCOUNT12 + NF05_DATE_RECEIVED + NF05_DATE_PAID + NF05_GL_OFFST_CD + NF05_INSURED_ID
				+ Nf05SubscriberName.Len.DETL_PATIENT_NAME + Nf05Address.Len.NF05_ADDRESS + NF05_MEMBER_ID
				+ Nf053rdPtyPayeeData.Len.NF053RD_PTY_PAYEE_DATA + NF05_PROVIDER_NAME + NF05_CLAIM_FORM_INSERT + NF05_AMOUNT_PAID
				+ NF05_PRMPT_PAY_INT_AM + NF05_PATIENT_ACCOUNT_NUM + NF05_PATIENT_RELATION + NF05_LINE_PROV_WRITEOFF + NF05_LINE_PAT_RESPONS
				+ Nf05CrossReimburseIndc.Len.NF05_CROSS_REIMBURSE_INDC + Nf05CrossReimburseIndc.Len.NF05_CROSS_REIMBURSE_INDC
				+ Nf05NationProgramIndc.Len.NF05_NATION_PROGRAM_INDC + NF05_PERF_PROVIDER_NUM + Nf05DiagnosisCodesData.Len.NF05_DIAGNOSIS_CODES_DATA
				+ NF05_BAL_BILL_AM + Nf05SubscriberName.Len.DETL_PATIENT_NAME + Nf05RecieptDate.Len.NF05_RECIEPT_DATE
				+ Nf05RecieptDate.Len.NF05_RECIEPT_DATE + Nf05RecieptDate.Len.NF05_RECIEPT_DATE + Nf05RecieptDate.Len.NF05_RECIEPT_DATE
				+ NF05_MT_QT_INDICATOR + NF05_ADJ_TYP_CD + NF05_BASE_CA_CD + NF05_RATE_CD + Nf05OtherInsureCode.Len.NF05_OTHER_INSURE_CODE
				+ NF05_PROV_STOP_PAY_CD + NF05_WAS_AN_N2_CLAIM + NF05_TYP_GRP_CD + NF05_CLM_SYST_VER_ID + NF05_CLM_CORP_CODE + FLR1 + FLR2;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int NF05_FEP_REFUND_WITHHELD_AMT = 6;
			public static final int NF05_FEP_MANAGED_CARE_AMT = 7;
			public static final int NF05_AMOUNT_PAID = 7;
			public static final int NF05_PRMPT_PAY_INT_AM = 7;
			public static final int NF05_LINE_PROV_WRITEOFF = 7;
			public static final int NF05_LINE_PAT_RESPONS = 7;
			public static final int NF05_BAL_BILL_AM = 7;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int NF05_FEP_REFUND_WITHHELD_AMT = 2;
			public static final int NF05_FEP_MANAGED_CARE_AMT = 2;
			public static final int NF05_AMOUNT_PAID = 2;
			public static final int NF05_PRMPT_PAY_INT_AM = 2;
			public static final int NF05_LINE_PROV_WRITEOFF = 2;
			public static final int NF05_LINE_PAT_RESPONS = 2;
			public static final int NF05_BAL_BILL_AM = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
