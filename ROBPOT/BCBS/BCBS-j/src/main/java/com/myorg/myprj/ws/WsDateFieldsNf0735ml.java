/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-DATE-FIELDS<br>
 * Variable: WS-DATE-FIELDS from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsDateFieldsNf0735ml {

	//==== PROPERTIES ====
	//Original name: GREGORN
	private GregornNf0735ml gregorn = new GregornNf0735ml();
	//Original name: GREG-HH-MI-SS
	private GregHhMiSs gregHhMiSs = new GregHhMiSs();
	//Original name: SAVE-DATE-TIME
	private GregornNf0735ml saveDateTime = new GregornNf0735ml();
	//Original name: SAVE-HH-MI-SS
	private GregHhMiSs saveHhMiSs = new GregHhMiSs();
	//Original name: DIFFERENCE
	private int difference = 0;
	//Original name: DIFF-SIGN
	private char diffSign = Types.SPACE_CHAR;
	//Original name: DIFFERENCE-DISP
	private int differenceDisp = 0;

	//==== METHODS ====
	public void setDifference(int difference) {
		this.difference = difference;
	}

	public int getDifference() {
		return this.difference;
	}

	public void setDiffSign(char diffSign) {
		this.diffSign = diffSign;
	}

	public void setDiffSignFormatted(String diffSign) {
		setDiffSign(Functions.charAt(diffSign, Types.CHAR_SIZE));
	}

	public char getDiffSign() {
		return this.diffSign;
	}

	public void setDifferenceDisp(int differenceDisp) {
		this.differenceDisp = differenceDisp;
	}

	public int getDifferenceDisp() {
		return this.differenceDisp;
	}

	public GregHhMiSs getGregHhMiSs() {
		return gregHhMiSs;
	}

	public GregornNf0735ml getGregorn() {
		return gregorn;
	}

	public GregornNf0735ml getSaveDateTime() {
		return saveDateTime;
	}

	public GregHhMiSs getSaveHhMiSs() {
		return saveHhMiSs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_INTRINSIC_DATE = 8;
		public static final int WS_OUTPUT_DATE_CCYYDDD = 7;
		public static final int DIFFERENCE_DISP = 6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
