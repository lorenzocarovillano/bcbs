/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.enums.RemTapeSubtotIndc;

/**Original name: HEADER-PART-OF-RECORD<br>
 * Variable: HEADER-PART-OF-RECORD from copybook NF07RMIT<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class HeaderPartOfRecord {

	//==== PROPERTIES ====
	//Original name: REM-TAPE-NPI-BILL-PVDR-NUM
	private String tapeNpiBillPvdrNum = DefaultValues.stringVal(Len.TAPE_NPI_BILL_PVDR_NUM);
	//Original name: REM-TAPE-LOCAL-BILL-PVDR-LOB
	private char tapeLocalBillPvdrLob = DefaultValues.CHAR_VAL;
	//Original name: REM-TAPE-LOCAL-BILL-PVDR-NUM
	private String tapeLocalBillPvdrNum = DefaultValues.stringVal(Len.TAPE_LOCAL_BILL_PVDR_NUM);
	//Original name: REM-TAPE-RA-SEQUENCE-NUMBER
	private String tapeRaSequenceNumber = DefaultValues.stringVal(Len.TAPE_RA_SEQUENCE_NUMBER);
	//Original name: REM-TAPE-RA-ADDR-NAME
	private RemTapeRaAddrName tapeRaAddrName = new RemTapeRaAddrName();
	//Original name: REM-TAPE-RA-ADDR-1
	private String tapeRaAddr1 = DefaultValues.stringVal(Len.TAPE_RA_ADDR1);
	//Original name: REM-TAPE-RA-ADDR-2
	private String tapeRaAddr2 = DefaultValues.stringVal(Len.TAPE_RA_ADDR2);
	//Original name: REM-TAPE-ADDR-CITY
	private String tapeAddrCity = DefaultValues.stringVal(Len.TAPE_ADDR_CITY);
	//Original name: REM-TAPE-ADDR-ST
	private String tapeAddrSt = DefaultValues.stringVal(Len.TAPE_ADDR_ST);
	//Original name: REM-TAPE-ADDR-ZIP5
	private String tapeAddrZip5 = DefaultValues.stringVal(Len.TAPE_ADDR_ZIP5);
	//Original name: REM-TAPE-ADDR-ZIP4
	private String tapeAddrZip4 = DefaultValues.stringVal(Len.TAPE_ADDR_ZIP4);
	//Original name: FILLER-REM-TAPE-ADDR-ZIP
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: REM-TAPE-CLAIM-LOB
	private char tapeClaimLob = DefaultValues.CHAR_VAL;
	//Original name: REM-TAPE-SUBTOT-INDC
	private RemTapeSubtotIndc tapeSubtotIndc = new RemTapeSubtotIndc();
	//Original name: REM-VOID-IND
	private char voidInd = DefaultValues.CHAR_VAL;
	//Original name: REM-COMPANY
	private String company = DefaultValues.stringVal(Len.COMPANY);
	//Original name: REM-TAPE-BANK-INDICATOR
	private char tapeBankIndicator = DefaultValues.CHAR_VAL;
	//Original name: REM-ITS-CLAIM-TYPE
	private char itsClaimType = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setHeaderPartOfRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		tapeNpiBillPvdrNum = MarshalByte.readString(buffer, position, Len.TAPE_NPI_BILL_PVDR_NUM);
		position += Len.TAPE_NPI_BILL_PVDR_NUM;
		setTapeLocalLobPvdrBytes(buffer, position);
		position += Len.TAPE_LOCAL_LOB_PVDR;
		tapeRaSequenceNumber = MarshalByte.readString(buffer, position, Len.TAPE_RA_SEQUENCE_NUMBER);
		position += Len.TAPE_RA_SEQUENCE_NUMBER;
		tapeRaAddrName.setTapeRaAddrNameBytes(buffer, position);
		position += RemTapeRaAddrName.Len.TAPE_RA_ADDR_NAME;
		tapeRaAddr1 = MarshalByte.readString(buffer, position, Len.TAPE_RA_ADDR1);
		position += Len.TAPE_RA_ADDR1;
		tapeRaAddr2 = MarshalByte.readString(buffer, position, Len.TAPE_RA_ADDR2);
		position += Len.TAPE_RA_ADDR2;
		setTapeRaAddr3Bytes(buffer, position);
		position += Len.TAPE_RA_ADDR3;
		tapeClaimLob = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		tapeSubtotIndc.setTapeSubtotIndc(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		voidInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		company = MarshalByte.readString(buffer, position, Len.COMPANY);
		position += Len.COMPANY;
		tapeBankIndicator = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		itsClaimType = MarshalByte.readChar(buffer, position);
	}

	public byte[] getHeaderPartOfRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, tapeNpiBillPvdrNum, Len.TAPE_NPI_BILL_PVDR_NUM);
		position += Len.TAPE_NPI_BILL_PVDR_NUM;
		getTapeLocalLobPvdrBytes(buffer, position);
		position += Len.TAPE_LOCAL_LOB_PVDR;
		MarshalByte.writeString(buffer, position, tapeRaSequenceNumber, Len.TAPE_RA_SEQUENCE_NUMBER);
		position += Len.TAPE_RA_SEQUENCE_NUMBER;
		tapeRaAddrName.getTapeRaAddrNameBytes(buffer, position);
		position += RemTapeRaAddrName.Len.TAPE_RA_ADDR_NAME;
		MarshalByte.writeString(buffer, position, tapeRaAddr1, Len.TAPE_RA_ADDR1);
		position += Len.TAPE_RA_ADDR1;
		MarshalByte.writeString(buffer, position, tapeRaAddr2, Len.TAPE_RA_ADDR2);
		position += Len.TAPE_RA_ADDR2;
		getTapeRaAddr3Bytes(buffer, position);
		position += Len.TAPE_RA_ADDR3;
		MarshalByte.writeChar(buffer, position, tapeClaimLob);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, tapeSubtotIndc.getTapeSubtotIndc());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, voidInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, company, Len.COMPANY);
		position += Len.COMPANY;
		MarshalByte.writeChar(buffer, position, tapeBankIndicator);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, itsClaimType);
		return buffer;
	}

	public void initHeaderPartOfRecordSpaces() {
		tapeNpiBillPvdrNum = "";
		initTapeLocalLobPvdrSpaces();
		tapeRaSequenceNumber = "";
		tapeRaAddrName.initTapeRaAddrNameSpaces();
		tapeRaAddr1 = "";
		tapeRaAddr2 = "";
		initTapeRaAddr3Spaces();
		tapeClaimLob = Types.SPACE_CHAR;
		tapeSubtotIndc.setTapeSubtotIndc(Types.SPACE_CHAR);
		voidInd = Types.SPACE_CHAR;
		company = "";
		tapeBankIndicator = Types.SPACE_CHAR;
		itsClaimType = Types.SPACE_CHAR;
	}

	public void setTapeNpiBillPvdrNum(String tapeNpiBillPvdrNum) {
		this.tapeNpiBillPvdrNum = Functions.subString(tapeNpiBillPvdrNum, Len.TAPE_NPI_BILL_PVDR_NUM);
	}

	public String getTapeNpiBillPvdrNum() {
		return this.tapeNpiBillPvdrNum;
	}

	public void setTapeLocalLobPvdrBytes(byte[] buffer, int offset) {
		int position = offset;
		tapeLocalBillPvdrLob = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		tapeLocalBillPvdrNum = MarshalByte.readString(buffer, position, Len.TAPE_LOCAL_BILL_PVDR_NUM);
	}

	public byte[] getTapeLocalLobPvdrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, tapeLocalBillPvdrLob);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tapeLocalBillPvdrNum, Len.TAPE_LOCAL_BILL_PVDR_NUM);
		return buffer;
	}

	public void initTapeLocalLobPvdrSpaces() {
		tapeLocalBillPvdrLob = Types.SPACE_CHAR;
		tapeLocalBillPvdrNum = "";
	}

	public void setTapeLocalBillPvdrLob(char tapeLocalBillPvdrLob) {
		this.tapeLocalBillPvdrLob = tapeLocalBillPvdrLob;
	}

	public char getTapeLocalBillPvdrLob() {
		return this.tapeLocalBillPvdrLob;
	}

	public void setTapeLocalBillPvdrNum(String tapeLocalBillPvdrNum) {
		this.tapeLocalBillPvdrNum = Functions.subString(tapeLocalBillPvdrNum, Len.TAPE_LOCAL_BILL_PVDR_NUM);
	}

	public String getTapeLocalBillPvdrNum() {
		return this.tapeLocalBillPvdrNum;
	}

	public void setTapeRaSequenceNumber(String tapeRaSequenceNumber) {
		this.tapeRaSequenceNumber = Functions.subString(tapeRaSequenceNumber, Len.TAPE_RA_SEQUENCE_NUMBER);
	}

	public String getTapeRaSequenceNumber() {
		return this.tapeRaSequenceNumber;
	}

	public void setTapeRaAddr1(String tapeRaAddr1) {
		this.tapeRaAddr1 = Functions.subString(tapeRaAddr1, Len.TAPE_RA_ADDR1);
	}

	public String getTapeRaAddr1() {
		return this.tapeRaAddr1;
	}

	public void setTapeRaAddr2(String tapeRaAddr2) {
		this.tapeRaAddr2 = Functions.subString(tapeRaAddr2, Len.TAPE_RA_ADDR2);
	}

	public String getTapeRaAddr2() {
		return this.tapeRaAddr2;
	}

	public void setTapeRaAddr3Bytes(byte[] buffer) {
		setTapeRaAddr3Bytes(buffer, 1);
	}

	public void setTapeRaAddr3Bytes(byte[] buffer, int offset) {
		int position = offset;
		setTapeAddrCtystBytes(buffer, position);
		position += Len.TAPE_ADDR_CTYST;
		setTapeAddrZipBytes(buffer, position);
	}

	public byte[] getTapeRaAddr3Bytes(byte[] buffer, int offset) {
		int position = offset;
		getTapeAddrCtystBytes(buffer, position);
		position += Len.TAPE_ADDR_CTYST;
		getTapeAddrZipBytes(buffer, position);
		return buffer;
	}

	public void initTapeRaAddr3Spaces() {
		initTapeAddrCtystSpaces();
		initTapeAddrZipSpaces();
	}

	public void setTapeAddrCtystBytes(byte[] buffer, int offset) {
		int position = offset;
		tapeAddrCity = MarshalByte.readString(buffer, position, Len.TAPE_ADDR_CITY);
		position += Len.TAPE_ADDR_CITY;
		tapeAddrSt = MarshalByte.readString(buffer, position, Len.TAPE_ADDR_ST);
	}

	public byte[] getTapeAddrCtystBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, tapeAddrCity, Len.TAPE_ADDR_CITY);
		position += Len.TAPE_ADDR_CITY;
		MarshalByte.writeString(buffer, position, tapeAddrSt, Len.TAPE_ADDR_ST);
		return buffer;
	}

	public void initTapeAddrCtystSpaces() {
		tapeAddrCity = "";
		tapeAddrSt = "";
	}

	public void setTapeAddrCity(String tapeAddrCity) {
		this.tapeAddrCity = Functions.subString(tapeAddrCity, Len.TAPE_ADDR_CITY);
	}

	public String getTapeAddrCity() {
		return this.tapeAddrCity;
	}

	public void setTapeAddrSt(String tapeAddrSt) {
		this.tapeAddrSt = Functions.subString(tapeAddrSt, Len.TAPE_ADDR_ST);
	}

	public String getTapeAddrSt() {
		return this.tapeAddrSt;
	}

	public void setTapeAddrZipFormatted(String data) {
		byte[] buffer = new byte[Len.TAPE_ADDR_ZIP];
		MarshalByte.writeString(buffer, 1, data, Len.TAPE_ADDR_ZIP);
		setTapeAddrZipBytes(buffer, 1);
	}

	public void setTapeAddrZipBytes(byte[] buffer, int offset) {
		int position = offset;
		tapeAddrZip5 = MarshalByte.readString(buffer, position, Len.TAPE_ADDR_ZIP5);
		position += Len.TAPE_ADDR_ZIP5;
		tapeAddrZip4 = MarshalByte.readString(buffer, position, Len.TAPE_ADDR_ZIP4);
		position += Len.TAPE_ADDR_ZIP4;
		flr1 = MarshalByte.readChar(buffer, position);
	}

	public byte[] getTapeAddrZipBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, tapeAddrZip5, Len.TAPE_ADDR_ZIP5);
		position += Len.TAPE_ADDR_ZIP5;
		MarshalByte.writeString(buffer, position, tapeAddrZip4, Len.TAPE_ADDR_ZIP4);
		position += Len.TAPE_ADDR_ZIP4;
		MarshalByte.writeChar(buffer, position, flr1);
		return buffer;
	}

	public void initTapeAddrZipSpaces() {
		tapeAddrZip5 = "";
		tapeAddrZip4 = "";
		flr1 = Types.SPACE_CHAR;
	}

	public void setTapeAddrZip5(String tapeAddrZip5) {
		this.tapeAddrZip5 = Functions.subString(tapeAddrZip5, Len.TAPE_ADDR_ZIP5);
	}

	public String getTapeAddrZip5() {
		return this.tapeAddrZip5;
	}

	public void setTapeAddrZip4(String tapeAddrZip4) {
		this.tapeAddrZip4 = Functions.subString(tapeAddrZip4, Len.TAPE_ADDR_ZIP4);
	}

	public String getTapeAddrZip4() {
		return this.tapeAddrZip4;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setTapeClaimLob(char tapeClaimLob) {
		this.tapeClaimLob = tapeClaimLob;
	}

	public char getTapeClaimLob() {
		return this.tapeClaimLob;
	}

	public void setVoidInd(char voidInd) {
		this.voidInd = voidInd;
	}

	public char getVoidInd() {
		return this.voidInd;
	}

	public void setCompany(String company) {
		this.company = Functions.subString(company, Len.COMPANY);
	}

	public String getCompany() {
		return this.company;
	}

	public void setTapeBankIndicator(char tapeBankIndicator) {
		this.tapeBankIndicator = tapeBankIndicator;
	}

	public char getTapeBankIndicator() {
		return this.tapeBankIndicator;
	}

	public void setItsClaimType(char itsClaimType) {
		this.itsClaimType = itsClaimType;
	}

	public char getItsClaimType() {
		return this.itsClaimType;
	}

	public RemTapeRaAddrName getTapeRaAddrName() {
		return tapeRaAddrName;
	}

	public RemTapeSubtotIndc getTapeSubtotIndc() {
		return tapeSubtotIndc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TAPE_NPI_BILL_PVDR_NUM = 10;
		public static final int TAPE_LOCAL_BILL_PVDR_LOB = 1;
		public static final int TAPE_LOCAL_BILL_PVDR_NUM = 10;
		public static final int TAPE_LOCAL_LOB_PVDR = TAPE_LOCAL_BILL_PVDR_LOB + TAPE_LOCAL_BILL_PVDR_NUM;
		public static final int TAPE_RA_SEQUENCE_NUMBER = 5;
		public static final int TAPE_RA_ADDR1 = 25;
		public static final int TAPE_RA_ADDR2 = 25;
		public static final int TAPE_ADDR_CITY = 25;
		public static final int TAPE_ADDR_ST = 2;
		public static final int TAPE_ADDR_CTYST = TAPE_ADDR_CITY + TAPE_ADDR_ST;
		public static final int TAPE_ADDR_ZIP5 = 5;
		public static final int TAPE_ADDR_ZIP4 = 4;
		public static final int FLR1 = 1;
		public static final int TAPE_ADDR_ZIP = TAPE_ADDR_ZIP5 + TAPE_ADDR_ZIP4 + FLR1;
		public static final int TAPE_RA_ADDR3 = TAPE_ADDR_CTYST + TAPE_ADDR_ZIP;
		public static final int TAPE_CLAIM_LOB = 1;
		public static final int VOID_IND = 1;
		public static final int COMPANY = 3;
		public static final int TAPE_BANK_INDICATOR = 1;
		public static final int ITS_CLAIM_TYPE = 1;
		public static final int HEADER_PART_OF_RECORD = TAPE_NPI_BILL_PVDR_NUM + TAPE_LOCAL_LOB_PVDR + TAPE_RA_SEQUENCE_NUMBER
				+ RemTapeRaAddrName.Len.TAPE_RA_ADDR_NAME + TAPE_RA_ADDR1 + TAPE_RA_ADDR2 + TAPE_RA_ADDR3 + TAPE_CLAIM_LOB
				+ RemTapeSubtotIndc.Len.TAPE_SUBTOT_INDC + VOID_IND + COMPANY + TAPE_BANK_INDICATOR + ITS_CLAIM_TYPE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
