      ******************************************************************
      * DCLGEN TABLE(SR01472)                                          *
      *        LIBRARY(DSSP.ZQBS.COPYLIB(SR01472))                     *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        APOST                                                   *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE SR01472 TABLE
           ( PROV_AD_CD                     CHAR(5) NOT NULL,
             PROV_AD_DS                     CHAR(30) NOT NULL,
             INFO_CHG_ID                    CHAR(8) NOT NULL,
             INFO_CHG_TS                    TIMESTAMP NOT NULL,
             PROV_AD_STAT_CD                CHAR(1) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE SR01472                            *
      ******************************************************************
       01  DCLSR01472.
           10 PROV-AD-CD           PIC X(5).
           10 PROV-AD-DS           PIC X(30).
           10 INFO-CHG-ID          PIC X(8).
           10 INFO-CHG-TS          PIC X(26).
           10 PROV-AD-STAT-CD      PIC X(1).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 5       *
      ******************************************************************
