000100* *---------------------                                          
000200* *             NF07RMIT                RECORD LENGTH:650         
000300* *   REMITTANCE ADVICE FILE LAYOUT                               
000400* *   COPYBOOK   :  NFREMIT                                       
000500* *   LAST UPDATE:  07/97 EXPAND PROVIDER NBR, ADDRESS, ETC       
000600* *                 07/97 ADD TOOTH SVC CODE & ID FIELDS          
000700* *                 06/99 ADD A-INVALID-SW                        
000800* *                 05/00 GMIS: RENAME REM-DETL-DENTAL-EXPL TO    
000900* *                       REM-DETL-REASON-EXPL.                   
001000* *                 12/00 ADD: PROMPT PAY INTEREST FIELD          
001100* *                       AND RECEIPT DATE.                       
001200* *                 02/02 CHANGES FOR THE HIPAA PROJECT           
001300* *                 09/04 CHANGES FOR AUTO DEDUCT MANAGEMENT RPT  
001400* *                 11/06 ADD FIELDS FOR NPI NUMBERS.             
001500* *   LENGTH     :  650                                           
001600* *---------------------------------                              
001700 01  REMITT-TAPE-RECORD.                                          
001800                                                                  
001900     05  HEADER-PART-OF-RECORD.                                   
002000                                                                  
002100         10  REM-TAPE-NPI-BILL-PVDR-NUM       PIC  X(10).         
002200                                                                  
002300         10  REM-TAPE-LOCAL-LOB-PVDR.                             
002400             15  REM-TAPE-LOCAL-BILL-PVDR-LOB PIC  X(01).         
002500             15  REM-TAPE-LOCAL-BILL-PVDR-NUM PIC  X(10).         
002600                                                                  
002700         10  REM-TAPE-RA-SEQUENCE-NUMBER      PIC  X(05).         
002800                                                                  
002900         10  REM-TAPE-RA-ADDR-NAME.                               
003000             15  REM-TAPE-RA-ADDR-NAME-LAST   PIC  X(35).         
003100             15  REM-TAPE-RA-ADDR-NAME-FIRST  PIC  X(25).         
003200             15  REM-TAPE-RA-ADDR-NAME-MID    PIC  X(15).         
003300                                                                  
003400         10  REM-TAPE-RA-ADDR-1               PIC  X(25).         
003500         10  REM-TAPE-RA-ADDR-2               PIC  X(25).         
003600                                                                  
003700         10  REM-TAPE-RA-ADDR-3.                                  
003800             15  REM-TAPE-ADDR-CTYST.                             
003900                 20  REM-TAPE-ADDR-CITY       PIC  X(25).         
004000                 20  REM-TAPE-ADDR-ST         PIC  X(02).         
004100                                                                  
004200             15  REM-TAPE-ADDR-ZIP.                               
004300                 20  REM-TAPE-ADDR-ZIP5       PIC  X(05).         
004400                 20  REM-TAPE-ADDR-ZIP4       PIC  X(04).         
004500                 20  FILLER                   PIC  X(01).         
004600                                                                  
004700         10  REM-TAPE-CLAIM-LOB               PIC  X(01).         
004800                                                                  
004900         10  REM-TAPE-SUBTOT-INDC             PIC  X(01).         
005000             88  REM-TAPE-PAGE-CTR            VALUE 'A'.          
005100             88  REM-TAPE-PIP-BEGIN           VALUE 'B'.          
005200             88  REM-TAPE-LEVEL-PAY           VALUE 'C'.          
005300             88  REM-TAPE-DETAIL-LINE         VALUE 'D'.          
005400             88  REM-TAPE-PIP-END             VALUE 'E'.          
005500             88  REM-TAPE-CAPITATION-LINE     VALUE 'Q'.          
005510             88  REM-TAPE-SUBTOT-LINE         VALUE 'S'.          
005600             88  REM-TAPE-TOTAL-LINE          VALUE 'T'.          
005700             88  REM-TAPE-PEND-LINE           VALUE 'Y'.          
005800             88  REM-TAPE-WO-LINE             VALUE 'Z'.          
005900             88  REM-TAPE-LINE-CTR            VALUE 'B'           
006000                                                    'C'           
006100                                                    'E'           
006200                                                    'S'.          
006300                                                                  
006400         10  REM-VOID-IND                     PIC  X(01).         
006500         10  REM-COMPANY                      PIC  X(03).         
006600         10  REM-TAPE-BANK-INDICATOR          PIC  X(01).         
006700         10  REM-ITS-CLAIM-TYPE               PIC  X(01).         
006800                                                                  
006900     05  DETAIL-PART-OF-RECORD.                                   
007000                                                                  
007200         10  REM-DETL-SUBSCRIBER-ID.                              
007300             15  REM-TAPE-GRP-ALP-PRFX-CD     PIC  X(03).         
007400             15  REM-DETL-ID-NUM              PIC  X(14).         
007500                                                                  
007600         10  REM-DETL-SUBSCRIBER-DIGITS       PIC  X(14).         
007700                                                                  
007800         10  REM-DETL-CLAIM-KEY.                                  
007900             15  REM-DETL-CLAIM-ID            PIC  X(12).         
008000             15  REM-DETL-CLAIM-ID-SUFFIX     PIC  X(01).         
008100                                                                  
008200         10  REM-REL-PAYMENT                  PIC  9(04).         
008300         10  REM-REL-PAYMENT-X                REDEFINES           
008400             REM-REL-PAYMENT                  PIC  X(04).         
008500                                                                  
008600         10  REM-LINE-NUM                     PIC  9(03).         
008700         10  REM-LINE-NUM-X                   REDEFINES           
008800             REM-LINE-NUM                     PIC  X(03).         
008900                                                                  
009000         10  REM-DETL-PATIENT-NAME.                               
009100             15  REM-DETL-PATIENT-NAME-LAST   PIC  X(20).         
009200             15  REM-DETL-PATIENT-NAME-TITLE  PIC  X(03).         
009300             15  REM-DETL-PATIENT-NAME-FIRST  PIC  X(15).         
009400             15  REM-DETL-PATIENT-NAME-MI     PIC  X(01).         
009500                                                                  
009600         10  REM-DETL-PATIENT-ACCT            PIC  X(17).         
009700                                                                  
009800         10  REM-DETL-POS                     PIC  X(02).         
009900         10  REM-DETL-OTLR                    PIC  X(01).         
010000                                                                  
010100         10  REM-DETL-SERV-DATE.                                  
010200             15  REM-DETL-SERV-CC             PIC  9(02).         
010300             15  REM-DETL-SERV-YY             PIC  9(02).         
010400             15  REM-DETL-SERV-MM             PIC  9(02).         
010500             15  REM-DETL-SERV-DD             PIC  9(02).         
010600                                                                  
010700         10  REM-ADJU-DRG-PROC.                                   
010800             15  REM-ADJU-PROC-CODE           PIC  X(05).         
010900             15  REM-ADJU-PROC-MOD1           PIC  X(02).         
011000             15  REM-ADJU-PROC-MOD2           PIC  X(02).         
011100                                                                  
011200         10  REM-SUBM-DRG-PROC.                                   
011300             15  REM-SUBM-PROC-CODE           PIC  X(05).         
011400             15  REM-SUBM-PROC-MOD1           PIC  X(02).         
011500             15  REM-SUBM-PROC-MOD2           PIC  X(02).         
011600                                                                  
011700         10  REM-DETL-DAYS                    PIC  9(03).         
011800         10  REM-DETL-TOTAL-CHARGE            PIC S9(07)V99.      
011900                                                                  
012000       07  REM-DETL-CONTINUED.                                    
012100         10  REM-DETL-ALLOWED-AMT             PIC S9(07)V99.      
012200         10  REM-DETL-PVDR-WRITEOFF           PIC S9(07)V99.      
012300         10  REM-DETL-AMT-OTHER-ADJ           PIC S9(07)V99.      
012400         10  REM-DETL-AMT-NOT-COVERED         PIC S9(07)V99.      
012500         10  REM-DETL-AMT-COINSURANCE         PIC S9(07)V99.      
012600         10  REM-DETL-AMT-DEDUCTIBLE          PIC S9(07)V99.      
012700         10  REM-DETL-AMT-COPAY               PIC S9(07)V99.      
012800         10  REM-DETL-AMT-PAT-OWES            PIC S9(07)V99.      
012900         10  REM-DETL-AMT-PAID-WITHOUT-INT    PIC S9(07)V99.      
013000         10  REM-DETL-BEG-BALANCE-FOR-MSG     PIC S9(07)V99.      
013100         10  REM-DETL-AMT-INTEREST-PD         PIC S9(07)V99.      
013200         10  REM-DETL-BEG-OR-END              PIC  X(03).         
013300         10  REM-DETL-RX-NUM                  PIC  X(07).         
013400         10  REM-DETL-TEAM                    PIC  X(05).         
013500         10  REM-DETL-ID                      PIC  X(03).         
013600         10  REM-DETL-TTH-SRFC-CD             PIC  X(05).         
013700         10  REM-DETL-TTH-ID                  PIC  X(02).         
013800         10  REM-DETL-OP-PRC-PROC-CD          PIC  X(05).         
013900                                                                  
014000         10  REM-DETL-RECEIPT-DATE.                               
014100             15 REM-DETL-RECEIPT-CC           PIC  9(02).         
014200             15 REM-DETL-RECEIPT-YY           PIC  9(02).         
014300             15 REM-DETL-RECEIPT-MM           PIC  9(02).         
014400             15 REM-DETL-RECEIPT-DD           PIC  9(02).         
014500                                                                  
014600         10  REM-DETL-CLASS-OF-CNTR-1         PIC  X(02).         
014700         10  REM-DETL-CLASS-OF-CNTR-2         PIC  X(02).         
014800                                                                  
014900         10  REM-DETL-ADJUST-RSN-CDS.                             
015000             15 REM-DETL-ADJ-RSN-CD-1         PIC  X(03).         
015100             15 REM-DETL-ADJ-RSN-CD-2         PIC  X(03).         
015200             15 REM-DETL-ADJ-RSN-CD-3         PIC  X(03).         
015300             15 REM-DETL-ADJ-RSN-CD-4         PIC  X(03).         
015400                                                                  
015500         10  REM-DETL-REMARKS-1               PIC  X(05).         
015600         10  REM-DETL-REMARKS-2               PIC  X(05).         
015700         10  REM-DETL-DENIAL-REMARK-CODE      PIC  X(02).         
015800         10  REM-DETL-ADJUST-REASON-CODE      PIC  X(02).         
015900         10  REM-DETL-ADJUST-RESP-CODE        PIC  X(01).         
016000         10  REM-DETL-ADJUST-TYPE-CODE        PIC  X(01).         
016100         10  REM-TAPE-NPI-PERF-PVDR-NUM       PIC  X(10).         
016200         10  REM-TAPE-LOCAL-PERF-PVDR-NUM     PIC  X(10).         
016300         10  REM-TAPE-NETWORK                 PIC  X(05).         
016400                                                                  
016500         10  REM-PEND-MESSAGE.                                    
016600             15  REM-PEND-MESSAGE-1-17        PIC  X(17).         
016700             15  REM-PEND-MESSAGE-18-100      PIC  X(83).         
016800                                                                  
017100     05  TOTAL-PART-OF-RECORD REDEFINES DETAIL-PART-OF-RECORD.    
017200                                                                  
017300         10  TOT-PVDR-NUM                     PIC  X(10).         
017400         10  TOT-DAYS                         PIC  9(07).         
017500         10  TOT-CHARGES                      PIC S9(10)V99.      
017600         10  TOT-ALLOWED                      PIC S9(10)V99.      
017700         10  TOT-WRITEOFF                     PIC S9(10)V99.      
017800         10  TOT-OTHER-ADJ                    PIC S9(10)V99.      
017900         10  TOT-NOT-COVERED                  PIC S9(10)V99.      
018000         10  TOT-COINSURANCE                  PIC S9(10)V99.      
018100         10  TOT-DEDUCTIBLE                   PIC S9(10)V99.      
018200         10  TOT-COPAY                        PIC S9(10)V99.      
018300         10  TOT-PATIENT-OWES                 PIC S9(10)V99.      
018400         10  TOT-PAID-INCLUDING-INTEREST      PIC S9(10)V99.      
018500         10  TOT-CHECK-NUMBER                 PIC  X(15).         
018600                                                                  
018700         10  TOT-DATE-PAID.                                       
018800             15  TOT-DATE-PAID-MM             PIC  9(02).         
018900             15  TOT-DATE-PAID-DD             PIC  9(02).         
019000             15  TOT-DATE-PAID-CC             PIC  9(02).         
019100             15  TOT-DATE-PAID-YY             PIC  9(02).         
019200                                                                  
019300         10  TOT-PROVIDER-CK-NAME             PIC  X(75).         
019400         10  TOT-CHECK-ADDR-1                 PIC  X(25).         
019500         10  TOT-CHECK-ADDR-2                 PIC  X(25).         
019600                                                                  
019700         10  TOT-CHECK-ADDR-3.                                    
019800             15  TOT-CHECK-ADDR-CITY          PIC  X(25).         
019900             15  TOT-CHECK-ADDR-ST            PIC  X(02).         
020000             15  TOT-CHECK-ADDR-ZIP           PIC  X(10).         
020100                                                                  
020200         10  REM-TAPE-SAME-ADDRESS            PIC  X(01).         
020300         10  FILLER-5                         PIC  X(119).        
020400                                                                  
020500*    ------------------------------------------------------------ 
020600*    --  A-TOTAL-PAGES = SUM OF RA, SUSP & WRITEOFF PAGES.        
020700*    --  A-STACKED-TOTAL-PAGES = SUM OF ALL REMIT PAGES FOR A     
020800*    --  GIVEN PROVIDER.  IN MOST CASES THIS WILL BE THE SAME     
020900*    --  VALUE AS A-TOTAL-PAGES.  HOWEVER, IF SEVERAL LOCAL       
021000*    --  NUMBERS HAVE THE SAME NPI NUMBER, THEN THE TOTAL OF      
021100*    --  ALL PAGES FOR ALL LOCAL PROVIDERS WILL BE IN THE         
021200*    --  A-STACKED-TOTAL-PAGES FIELD. THE REMITS FOR THE LOCAL    
021300*    --  PROVIDER NUMBERS WILL BE "STACKED" TOGETHER IN ON/DEMAND 
021400*    --  UNDER THE SINGLE NPI NUMBER.                             
021500*    ------------------------------------------------------------ 
021600                                                                  
021700     05  A-PAGE-CTR-RECORD REDEFINES DETAIL-PART-OF-RECORD.       
021800         10  A-TOTAL-PAGES                    PIC  9(04).         
021900         10  A-RA-PAGES                       PIC  9(04).         
022000         10  A-SUSP-PAGES                     PIC  9(04).         
022100         10  A-WRITEOFF-PAGES                 PIC  9(04).         
022200         10  A-ADDRESS-FLAG                   PIC  X(01).         
022300         10  A-DIVERT-FLAG                    PIC  X(01).         
022400         10  A-CHECK-FLAG                     PIC  X(01).         
022500         10  A-INVALID-SW                     PIC  X(01).         
022600                                                                  
022700*        -----------------------------------------------------    
022800*        -  THE A-STACKED-XXX FIELDS REPRESENT THE TOTAL          
022900*        -  # OF PAGES FOR ALL LOCAL PROVIDER NUMBERS THAT        
023000*        -  HAVE THE SAME NPI NUMBER.  SEPARATE PAGE COUNTS       
023100*        -  ARE CALCULATED FOR PAID VS SUSPENDED VS PAY INSURED   
023200*        -  WITH WRITEOFF CLAIMS, AND WITHIN THOSE CATEGORIES     
023300*        -  THERE ARE COUNTS FOR PRINTED VS NON-PRINTED REMITS    
023400*        -  AND REMITS WITH 6 OR LESS PAGES VS REMITS WITH        
023500*        -  GREATER THAN 6 PAGES.                                 
023600*        -  THESE PAGE COUNTS WILL BE USED TO DETERMINE WHAT      
023700*        -  VALUE FOR "NUMBER OF PAGES" IS DISPLAYED ON THE       
023800*        -  ON DEMAND SCREEN AFTER A SEARCH IS EXECUTED TO        
023900*        -  FIND REMITS FOR A PROVIDER NUMBER.                    
024000*        -----------------------------------------------------    
024100                                                                  
024200         10  A-STACKED-TOTAL-PAGES            PIC  9(06).         
024300         10  A-STACKED-TOT-PGS-PAID-CLMS      PIC  9(06).         
024400         10  A-STACKED-TOT-PGS-SUSP-CLMS      PIC  9(06).         
024500         10  A-STACKED-TOT-PGS-WOFF-CLMS      PIC  9(06).         
024600         10  A-STACKED-PAID-PGS-LT6-PRT       PIC  9(06).         
024700         10  A-STACKED-PAID-PGS-LT6-NOPRT     PIC  9(06).         
024800         10  A-STACKED-PAID-PGS-GT6-PRT       PIC  9(06).         
024900         10  A-STACKED-PAID-PGS-GT6-NOPRT     PIC  9(06).         
025000         10  A-STACKED-SUSP-PGS-LT6-PRT       PIC  9(06).         
025100         10  A-STACKED-SUSP-PGS-LT6-NOPRT     PIC  9(06).         
025200         10  A-STACKED-SUSP-PGS-GT6-PRT       PIC  9(06).         
025300         10  A-STACKED-SUSP-PGS-GT6-NOPRT     PIC  9(06).         
025400         10  A-STACKED-WOFF-PGS-LT6-PRT       PIC  9(06).         
025500         10  A-STACKED-WOFF-PGS-LT6-NOPRT     PIC  9(06).         
025600         10  A-STACKED-WOFF-PGS-GT6-PRT       PIC  9(06).         
025700         10  A-STACKED-WOFF-PGS-GT6-NOPRT     PIC  9(06).         
025800         10  FILLER-6                         PIC  X(326).        
025900                                                                  
021700     05  TRAILER-AREA.                                            
               10  REM-LINE-OF-BUSINESS             PIC  X(03).         
                   88  BCS                          VALUE  'BCS'.       
                   88  FEP                          VALUE  'FEP'.       
                   88  KANSAS-SOLUTIONS             VALUE  'KSS'.       
                                                                        
               10  REM-PRINT-NO-PRINT               PIC  X(05).         
                   88  PAPER-REMIT                  VALUE  'PAPER'.     
                   88  NO-PAPER-REMIT               VALUE  'NOPRT'.     
                                                                        
               10  REM-TYPE-OF-REMIT                PIC  X(04).         
                   88  PAY-PROVIDER                 VALUE  'PAYP'.      
                   88  PEND-SUSPEND                 VALUE  'PEND'.      
                   88  WRITE-OFF                    VALUE  'WOFF'.      
025900                                                                  
