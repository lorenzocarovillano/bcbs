      ******************************************************************
      * DCLGEN TABLE(DBZWBK.TA01435)                                   *
      *        LIBRARY(DSSA.ZQBS.COPYLIB(SR01435))                     *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        QUOTE                                                   *
      *        DBCSDELIM(NO)                                           *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE SR01435 TABLE
           ( PROV_ID                        CHAR(10) NOT NULL,
             PROV_LOB_CD                    CHAR(1) NOT NULL,
             PROV_CN_ARNG_CD                CHAR(5) NOT NULL,
             COMM_PAY_EFF_DT                DATE NOT NULL,
             COMM_PAY_ID                    CHAR(10) NOT NULL,
             COMM_PAY_LOB_CD                CHAR(1) NOT NULL,
             COMM_PAY_TERM_DT               DATE NOT NULL,
             INFO_CHG_ID                    CHAR(8) NOT NULL,
             INFO_CHG_TS                    TIMESTAMP NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE DBZWBK.TA01435                     *
      ******************************************************************
       01  DCLSR01435.
           10 PROV-ID              PIC X(10).
           10 PROV-LOB-CD          PIC X(1).
           10 PROV-CN-ARNG-CD      PIC X(5).
           10 COMM-PAY-EFF-DT      PIC X(10).
           10 COMM-PAY-ID          PIC X(10).
           10 COMM-PAY-LOB-CD      PIC X(1).
           10 COMM-PAY-TERM-DT     PIC X(10).
           10 INFO-CHG-ID          PIC X(8).
           10 INFO-CHG-TS          PIC X(26).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 9       *
      ******************************************************************
