CREATE TABLE S02682SA 
   ( CLM_CNTL_ID                    CHAR(12) NOT NULL,
     CLM_CNTL_SFX_ID                CHAR(1) NOT NULL,
     LOOP_ID                        CHAR(6) NOT NULL,
     LOOP_SEQ_CT                    DECIMAL(4, 0) NOT NULL,
     SEG_ID                         CHAR(3) NOT NULL,
     SEG_SEQ_CT                     DECIMAL(4, 0) NOT NULL,
     PROV_SBMT_LN_NO_ID             DECIMAL(3, 0) NOT NULL,
     ENTY_ID_CD                     CHAR(3) NOT NULL,
     ENTY_TYP_QLF_CD                CHAR(1) NOT NULL,
     LST_ORG_NM                     CHAR(60) NOT NULL,
     FRST_NM                        CHAR(35) NOT NULL,
     MID_NM                         CHAR(25) NOT NULL,
     SFX_NM                         CHAR(10) NOT NULL,
     ID_QLF_CD                      CHAR(2) NOT NULL,
     ID_CD                          CHAR(80) NOT NULL,
     INFO_CHG_ID                    CHAR(8) NOT NULL,
     INFO_CHG_DT                    DATE NOT NULL
   );