/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [SR01451]
 * 
 */
public interface ISr01451 extends BaseSqlTo {

	/**
	 * Host Variable PROV-ID
	 * 
	 */
	String getOvId();

	void setOvId(String ovId);

	/**
	 * Host Variable PROV-LOB-CD
	 * 
	 */
	char getOvLobCd();

	void setOvLobCd(char ovLobCd);

	/**
	 * Host Variable PROV-CN-ARNG-EF-DT
	 * 
	 */
	String getOvCnArngEfDt();

	void setOvCnArngEfDt(String ovCnArngEfDt);

	/**
	 * Host Variable PRV-CN-ARNG-TRM-DT
	 * 
	 */
	String getvCnArngTrmDt();

	void setvCnArngTrmDt(String vCnArngTrmDt);
};
