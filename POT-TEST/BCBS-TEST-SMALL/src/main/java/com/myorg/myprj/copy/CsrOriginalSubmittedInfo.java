/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: CSR-ORIGINAL-SUBMITTED-INFO<br>
 * Variable: CSR-ORIGINAL-SUBMITTED-INFO from copybook NF07AREA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CsrOriginalSubmittedInfo {

	//==== PROPERTIES ====
	//Original name: CSR-ORIG-REVENUE-SRV-ID
	private String revenueSrvId = "";
	//Original name: CSR-ORIG-PROCEDURE-SRV-ID
	private String procedureSrvId = "";
	//Original name: CSR-ORIG-ID-QLF-CD
	private String idQlfCd = "";
	//Original name: CSR-ORIG-PROCEDURE-MOD-1
	private String procedureMod1 = "";
	//Original name: CSR-ORIG-PROCEDURE-MOD-2
	private String procedureMod2 = "";
	//Original name: CSR-ORIG-PROCEDURE-MOD-3
	private String procedureMod3 = "";
	//Original name: CSR-ORIG-PROCEDURE-MOD-4
	private String procedureMod4 = "";
	//Original name: CSR-ORIG-MONETARY-AMOUNT
	private AfDecimal monetaryAmount = new AfDecimal("0", 11, 2);
	//Original name: CSR-ORIG-QUANTITY-COUNT
	private AfDecimal quantityCount = new AfDecimal("0", 7, 2);

	//==== METHODS ====
	public byte[] getOriginalSubmittedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, revenueSrvId, Len.REVENUE_SRV_ID);
		position += Len.REVENUE_SRV_ID;
		MarshalByte.writeString(buffer, position, procedureSrvId, Len.PROCEDURE_SRV_ID);
		position += Len.PROCEDURE_SRV_ID;
		MarshalByte.writeString(buffer, position, idQlfCd, Len.ID_QLF_CD);
		position += Len.ID_QLF_CD;
		MarshalByte.writeString(buffer, position, procedureMod1, Len.PROCEDURE_MOD1);
		position += Len.PROCEDURE_MOD1;
		MarshalByte.writeString(buffer, position, procedureMod2, Len.PROCEDURE_MOD2);
		position += Len.PROCEDURE_MOD2;
		MarshalByte.writeString(buffer, position, procedureMod3, Len.PROCEDURE_MOD3);
		position += Len.PROCEDURE_MOD3;
		MarshalByte.writeString(buffer, position, procedureMod4, Len.PROCEDURE_MOD4);
		position += Len.PROCEDURE_MOD4;
		MarshalByte.writeDecimalAsPacked(buffer, position, monetaryAmount.copy());
		position += Len.MONETARY_AMOUNT;
		MarshalByte.writeDecimalAsPacked(buffer, position, quantityCount.copy());
		return buffer;
	}

	public void initOriginalSubmittedInfoSpaces() {
		revenueSrvId = "";
		procedureSrvId = "";
		idQlfCd = "";
		procedureMod1 = "";
		procedureMod2 = "";
		procedureMod3 = "";
		procedureMod4 = "";
		monetaryAmount.setNaN();
		quantityCount.setNaN();
	}

	public void setRevenueSrvId(String revenueSrvId) {
		this.revenueSrvId = Functions.subString(revenueSrvId, Len.REVENUE_SRV_ID);
	}

	public String getRevenueSrvId() {
		return this.revenueSrvId;
	}

	public void setProcedureSrvId(String procedureSrvId) {
		this.procedureSrvId = Functions.subString(procedureSrvId, Len.PROCEDURE_SRV_ID);
	}

	public String getProcedureSrvId() {
		return this.procedureSrvId;
	}

	public void setIdQlfCd(String idQlfCd) {
		this.idQlfCd = Functions.subString(idQlfCd, Len.ID_QLF_CD);
	}

	public String getIdQlfCd() {
		return this.idQlfCd;
	}

	public void setProcedureMod1(String procedureMod1) {
		this.procedureMod1 = Functions.subString(procedureMod1, Len.PROCEDURE_MOD1);
	}

	public String getProcedureMod1() {
		return this.procedureMod1;
	}

	public void setProcedureMod2(String procedureMod2) {
		this.procedureMod2 = Functions.subString(procedureMod2, Len.PROCEDURE_MOD2);
	}

	public String getProcedureMod2() {
		return this.procedureMod2;
	}

	public void setProcedureMod3(String procedureMod3) {
		this.procedureMod3 = Functions.subString(procedureMod3, Len.PROCEDURE_MOD3);
	}

	public String getProcedureMod3() {
		return this.procedureMod3;
	}

	public void setProcedureMod4(String procedureMod4) {
		this.procedureMod4 = Functions.subString(procedureMod4, Len.PROCEDURE_MOD4);
	}

	public String getProcedureMod4() {
		return this.procedureMod4;
	}

	public void setMonetaryAmount(AfDecimal monetaryAmount) {
		this.monetaryAmount.assign(monetaryAmount);
	}

	public AfDecimal getMonetaryAmount() {
		return this.monetaryAmount.copy();
	}

	public void setQuantityCount(AfDecimal quantityCount) {
		this.quantityCount.assign(quantityCount);
	}

	public AfDecimal getQuantityCount() {
		return this.quantityCount.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REVENUE_SRV_ID = 48;
		public static final int PROCEDURE_SRV_ID = 48;
		public static final int ID_QLF_CD = 2;
		public static final int PROCEDURE_MOD1 = 2;
		public static final int PROCEDURE_MOD2 = 2;
		public static final int PROCEDURE_MOD3 = 2;
		public static final int PROCEDURE_MOD4 = 2;
		public static final int MONETARY_AMOUNT = 6;
		public static final int QUANTITY_COUNT = 4;
		public static final int ORIGINAL_SUBMITTED_INFO = REVENUE_SRV_ID + PROCEDURE_SRV_ID + ID_QLF_CD + PROCEDURE_MOD1 + PROCEDURE_MOD2
				+ PROCEDURE_MOD3 + PROCEDURE_MOD4 + MONETARY_AMOUNT + QUANTITY_COUNT;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int MONETARY_AMOUNT = 9;
			public static final int QUANTITY_COUNT = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int MONETARY_AMOUNT = 2;
			public static final int QUANTITY_COUNT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
