/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: CAS-SUBSCRIPTS<br>
 * Variable: CAS-SUBSCRIPTS from program NF0735ML<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class CasSubscripts {

	//==== PROPERTIES ====
	//Original name: CAS-MAX-CO-18
	private short casMaxCo18 = ((short) 18);
	//Original name: CAS-MAX-PR-24
	private short casMaxPr24 = ((short) 24);
	//Original name: CAS-MAX-OA-18
	private short casMaxOa18 = ((short) 18);
	//Original name: CAS-MAX-PI-6
	private short casMaxPi6 = ((short) 6);
	//Original name: CAS-SUB
	private short casSub = ((short) 0);
	//Original name: PMT-CAS-SUBS
	private PmtCasSubs pmtCasSubs = new PmtCasSubs();
	//Original name: VOID-CAS-SUBS
	private VoidCasSubs voidCasSubs = new VoidCasSubs();

	//==== METHODS ====
	public short getCasMaxCo18() {
		return this.casMaxCo18;
	}

	public String getCasMaxCo18Formatted() {
		return PicFormatter.display(new PicParams("9(4)").setUsage(PicUsage.PACKED)).format(getCasMaxCo18()).toString();
	}

	public String getCasMaxCo18AsString() {
		return getCasMaxCo18Formatted();
	}

	public short getCasMaxPr24() {
		return this.casMaxPr24;
	}

	public String getCasMaxPr24Formatted() {
		return PicFormatter.display(new PicParams("9(4)").setUsage(PicUsage.PACKED)).format(getCasMaxPr24()).toString();
	}

	public String getCasMaxPr24AsString() {
		return getCasMaxPr24Formatted();
	}

	public short getCasMaxOa18() {
		return this.casMaxOa18;
	}

	public String getCasMaxOa18Formatted() {
		return PicFormatter.display(new PicParams("9(4)").setUsage(PicUsage.PACKED)).format(getCasMaxOa18()).toString();
	}

	public String getCasMaxOa18AsString() {
		return getCasMaxOa18Formatted();
	}

	public short getCasMaxPi6() {
		return this.casMaxPi6;
	}

	public String getCasMaxPi6Formatted() {
		return PicFormatter.display(new PicParams("9(4)").setUsage(PicUsage.PACKED)).format(getCasMaxPi6()).toString();
	}

	public String getCasMaxPi6AsString() {
		return getCasMaxPi6Formatted();
	}

	public void setCasSub(short casSub) {
		this.casSub = casSub;
	}

	public short getCasSub() {
		return this.casSub;
	}

	public PmtCasSubs getPmtCasSubs() {
		return pmtCasSubs;
	}

	public VoidCasSubs getVoidCasSubs() {
		return voidCasSubs;
	}
}
