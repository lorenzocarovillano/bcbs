/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS02809sa;

/**
 * Data Access Object(DAO) for table [S02809SA]
 * 
 */
public class S02809saDao extends BaseSqlDao<IS02809sa> {

	public S02809saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02809sa> getToClass() {
		return IS02809sa.class;
	}

	public String selectRec(String cntlId, char cntlSfxId, short pmtId, String dft) {
		return buildQuery("selectRec").bind("cntlId", cntlId).bind("cntlSfxId", String.valueOf(cntlSfxId)).bind("pmtId", pmtId)
				.scalarResultString(dft);
	}
}
