/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-COUNTERS<br>
 * Variable: WS-COUNTERS from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsCounters {

	//==== PROPERTIES ====
	//Original name: WS-FETCH-CNTR
	private String wsFetchCntr = DefaultValues.stringVal(Len.WS_FETCH_CNTR);
	//Original name: WS-TRIGGER-IN
	private String wsTriggerIn = DefaultValues.stringVal(Len.WS_TRIGGER_IN);
	//Original name: WS-TRIGGER-LOADED
	private String wsTriggerLoaded = DefaultValues.stringVal(Len.WS_TRIGGER_LOADED);
	//Original name: WS-BYPASS-PMTS
	private String wsBypassPmts = DefaultValues.stringVal(Len.WS_BYPASS_PMTS);
	//Original name: WS-BYPASS-PROV
	private String wsBypassProv = DefaultValues.stringVal(Len.WS_BYPASS_PROV);
	//Original name: WS-EOB-REG-OUT
	private String wsEobRegOut = DefaultValues.stringVal(Len.WS_EOB_REG_OUT);
	//Original name: WS-LINES-REG-OUT
	private String wsLinesRegOut = DefaultValues.stringVal(Len.WS_LINES_REG_OUT);
	//Original name: WS-LINES-KSS-OUT
	private String wsLinesKssOut = DefaultValues.stringVal(Len.WS_LINES_KSS_OUT);
	//Original name: WS-LINES-KSS-EXP-OUT
	private String wsLinesKssExpOut = DefaultValues.stringVal(Len.WS_LINES_KSS_EXP_OUT);
	//Original name: WS-ITS-Y1-CNT
	private String wsItsY1Cnt = DefaultValues.stringVal(Len.WS_ITS_Y1_CNT);
	//Original name: WS-S02813-UPDATE
	private String wsS02813Update = DefaultValues.stringVal(Len.WS_S02813_UPDATE);
	//Original name: WS-S04315-UPDATES
	private String wsS04315Updates = "0000000";
	//Original name: WS-S02813-VOID-UPDATE
	private String wsS02813VoidUpdate = DefaultValues.stringVal(Len.WS_S02813_VOID_UPDATE);
	//Original name: WS-S02801-INSERTED
	private String wsS02801Inserted = DefaultValues.stringVal(Len.WS_S02801_INSERTED);
	//Original name: WS-S02814-INSERTED
	private String wsS02814Inserted = DefaultValues.stringVal(Len.WS_S02814_INSERTED);
	//Original name: WS-S02814-MTH-QTR
	private String wsS02814MthQtr = DefaultValues.stringVal(Len.WS_S02814_MTH_QTR);
	//Original name: WS-MTH-QTR
	private String wsMthQtr = DefaultValues.stringVal(Len.WS_MTH_QTR);
	//Original name: COMMIT-COUNTER
	private String commitCounter = DefaultValues.stringVal(Len.COMMIT_COUNTER);
	//Original name: ERR-FILE-CNT
	private String errFileCnt = DefaultValues.stringVal(Len.ERR_FILE_CNT);
	//Original name: SRS-PRV-MATCH-CNTR
	private String srsPrvMatchCntr = DefaultValues.stringVal(Len.SRS_PRV_MATCH_CNTR);
	//Original name: SRS-DATE-BYPASS-CNTR
	private String srsDateBypassCntr = DefaultValues.stringVal(Len.SRS_DATE_BYPASS_CNTR);
	//Original name: SRS-CLAIM-CNTR
	private String srsClaimCntr = DefaultValues.stringVal(Len.SRS_CLAIM_CNTR);

	//==== METHODS ====
	public void setWsCountersBytes(byte[] buffer, int offset) {
		int position = offset;
		wsFetchCntr = MarshalByte.readFixedString(buffer, position, Len.WS_FETCH_CNTR);
		position += Len.WS_FETCH_CNTR;
		wsTriggerIn = MarshalByte.readFixedString(buffer, position, Len.WS_TRIGGER_IN);
		position += Len.WS_TRIGGER_IN;
		wsTriggerLoaded = MarshalByte.readFixedString(buffer, position, Len.WS_TRIGGER_LOADED);
		position += Len.WS_TRIGGER_LOADED;
		wsBypassPmts = MarshalByte.readFixedString(buffer, position, Len.WS_BYPASS_PMTS);
		position += Len.WS_BYPASS_PMTS;
		wsBypassProv = MarshalByte.readFixedString(buffer, position, Len.WS_BYPASS_PROV);
		position += Len.WS_BYPASS_PROV;
		wsEobRegOut = MarshalByte.readFixedString(buffer, position, Len.WS_EOB_REG_OUT);
		position += Len.WS_EOB_REG_OUT;
		wsLinesRegOut = MarshalByte.readFixedString(buffer, position, Len.WS_LINES_REG_OUT);
		position += Len.WS_LINES_REG_OUT;
		wsLinesKssOut = MarshalByte.readFixedString(buffer, position, Len.WS_LINES_KSS_OUT);
		position += Len.WS_LINES_KSS_OUT;
		wsLinesKssExpOut = MarshalByte.readFixedString(buffer, position, Len.WS_LINES_KSS_EXP_OUT);
		position += Len.WS_LINES_KSS_EXP_OUT;
		wsItsY1Cnt = MarshalByte.readFixedString(buffer, position, Len.WS_ITS_Y1_CNT);
		position += Len.WS_ITS_Y1_CNT;
		wsS02813Update = MarshalByte.readFixedString(buffer, position, Len.WS_S02813_UPDATE);
		position += Len.WS_S02813_UPDATE;
		wsS04315Updates = MarshalByte.readFixedString(buffer, position, Len.WS_S04315_UPDATES);
		position += Len.WS_S04315_UPDATES;
		wsS02813VoidUpdate = MarshalByte.readFixedString(buffer, position, Len.WS_S02813_VOID_UPDATE);
		position += Len.WS_S02813_VOID_UPDATE;
		wsS02801Inserted = MarshalByte.readFixedString(buffer, position, Len.WS_S02801_INSERTED);
		position += Len.WS_S02801_INSERTED;
		wsS02814Inserted = MarshalByte.readFixedString(buffer, position, Len.WS_S02814_INSERTED);
		position += Len.WS_S02814_INSERTED;
		wsS02814MthQtr = MarshalByte.readFixedString(buffer, position, Len.WS_S02814_MTH_QTR);
		position += Len.WS_S02814_MTH_QTR;
		wsMthQtr = MarshalByte.readFixedString(buffer, position, Len.WS_MTH_QTR);
		position += Len.WS_MTH_QTR;
		commitCounter = MarshalByte.readFixedString(buffer, position, Len.COMMIT_COUNTER);
		position += Len.COMMIT_COUNTER;
		errFileCnt = MarshalByte.readFixedString(buffer, position, Len.ERR_FILE_CNT);
		position += Len.ERR_FILE_CNT;
		srsPrvMatchCntr = MarshalByte.readFixedString(buffer, position, Len.SRS_PRV_MATCH_CNTR);
		position += Len.SRS_PRV_MATCH_CNTR;
		srsDateBypassCntr = MarshalByte.readFixedString(buffer, position, Len.SRS_DATE_BYPASS_CNTR);
		position += Len.SRS_DATE_BYPASS_CNTR;
		srsClaimCntr = MarshalByte.readFixedString(buffer, position, Len.SRS_CLAIM_CNTR);
	}

	public byte[] getWsCountersBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, wsFetchCntr, Len.WS_FETCH_CNTR);
		position += Len.WS_FETCH_CNTR;
		MarshalByte.writeString(buffer, position, wsTriggerIn, Len.WS_TRIGGER_IN);
		position += Len.WS_TRIGGER_IN;
		MarshalByte.writeString(buffer, position, wsTriggerLoaded, Len.WS_TRIGGER_LOADED);
		position += Len.WS_TRIGGER_LOADED;
		MarshalByte.writeString(buffer, position, wsBypassPmts, Len.WS_BYPASS_PMTS);
		position += Len.WS_BYPASS_PMTS;
		MarshalByte.writeString(buffer, position, wsBypassProv, Len.WS_BYPASS_PROV);
		position += Len.WS_BYPASS_PROV;
		MarshalByte.writeString(buffer, position, wsEobRegOut, Len.WS_EOB_REG_OUT);
		position += Len.WS_EOB_REG_OUT;
		MarshalByte.writeString(buffer, position, wsLinesRegOut, Len.WS_LINES_REG_OUT);
		position += Len.WS_LINES_REG_OUT;
		MarshalByte.writeString(buffer, position, wsLinesKssOut, Len.WS_LINES_KSS_OUT);
		position += Len.WS_LINES_KSS_OUT;
		MarshalByte.writeString(buffer, position, wsLinesKssExpOut, Len.WS_LINES_KSS_EXP_OUT);
		position += Len.WS_LINES_KSS_EXP_OUT;
		MarshalByte.writeString(buffer, position, wsItsY1Cnt, Len.WS_ITS_Y1_CNT);
		position += Len.WS_ITS_Y1_CNT;
		MarshalByte.writeString(buffer, position, wsS02813Update, Len.WS_S02813_UPDATE);
		position += Len.WS_S02813_UPDATE;
		MarshalByte.writeString(buffer, position, wsS04315Updates, Len.WS_S04315_UPDATES);
		position += Len.WS_S04315_UPDATES;
		MarshalByte.writeString(buffer, position, wsS02813VoidUpdate, Len.WS_S02813_VOID_UPDATE);
		position += Len.WS_S02813_VOID_UPDATE;
		MarshalByte.writeString(buffer, position, wsS02801Inserted, Len.WS_S02801_INSERTED);
		position += Len.WS_S02801_INSERTED;
		MarshalByte.writeString(buffer, position, wsS02814Inserted, Len.WS_S02814_INSERTED);
		position += Len.WS_S02814_INSERTED;
		MarshalByte.writeString(buffer, position, wsS02814MthQtr, Len.WS_S02814_MTH_QTR);
		position += Len.WS_S02814_MTH_QTR;
		MarshalByte.writeString(buffer, position, wsMthQtr, Len.WS_MTH_QTR);
		position += Len.WS_MTH_QTR;
		MarshalByte.writeString(buffer, position, commitCounter, Len.COMMIT_COUNTER);
		position += Len.COMMIT_COUNTER;
		MarshalByte.writeString(buffer, position, errFileCnt, Len.ERR_FILE_CNT);
		position += Len.ERR_FILE_CNT;
		MarshalByte.writeString(buffer, position, srsPrvMatchCntr, Len.SRS_PRV_MATCH_CNTR);
		position += Len.SRS_PRV_MATCH_CNTR;
		MarshalByte.writeString(buffer, position, srsDateBypassCntr, Len.SRS_DATE_BYPASS_CNTR);
		position += Len.SRS_DATE_BYPASS_CNTR;
		MarshalByte.writeString(buffer, position, srsClaimCntr, Len.SRS_CLAIM_CNTR);
		return buffer;
	}

	public void setWsFetchCntr(int wsFetchCntr) {
		this.wsFetchCntr = NumericDisplay.asString(wsFetchCntr, Len.WS_FETCH_CNTR);
	}

	public void setWsFetchCntrFormatted(String wsFetchCntr) {
		this.wsFetchCntr = Trunc.toUnsignedNumeric(wsFetchCntr, Len.WS_FETCH_CNTR);
	}

	public int getWsFetchCntr() {
		return NumericDisplay.asInt(this.wsFetchCntr);
	}

	public String getWsFetchCntrFormatted() {
		return this.wsFetchCntr;
	}

	public String getWsFetchCntrAsString() {
		return getWsFetchCntrFormatted();
	}

	public void setWsTriggerIn(int wsTriggerIn) {
		this.wsTriggerIn = NumericDisplay.asString(wsTriggerIn, Len.WS_TRIGGER_IN);
	}

	public void setWsTriggerInFormatted(String wsTriggerIn) {
		this.wsTriggerIn = Trunc.toUnsignedNumeric(wsTriggerIn, Len.WS_TRIGGER_IN);
	}

	public int getWsTriggerIn() {
		return NumericDisplay.asInt(this.wsTriggerIn);
	}

	public String getWsTriggerInFormatted() {
		return this.wsTriggerIn;
	}

	public String getWsTriggerInAsString() {
		return getWsTriggerInFormatted();
	}

	public void setWsTriggerLoaded(int wsTriggerLoaded) {
		this.wsTriggerLoaded = NumericDisplay.asString(wsTriggerLoaded, Len.WS_TRIGGER_LOADED);
	}

	public void setWsTriggerLoadedFormatted(String wsTriggerLoaded) {
		this.wsTriggerLoaded = Trunc.toUnsignedNumeric(wsTriggerLoaded, Len.WS_TRIGGER_LOADED);
	}

	public int getWsTriggerLoaded() {
		return NumericDisplay.asInt(this.wsTriggerLoaded);
	}

	public String getWsTriggerLoadedFormatted() {
		return this.wsTriggerLoaded;
	}

	public String getWsTriggerLoadedAsString() {
		return getWsTriggerLoadedFormatted();
	}

	public void setWsBypassPmts(int wsBypassPmts) {
		this.wsBypassPmts = NumericDisplay.asString(wsBypassPmts, Len.WS_BYPASS_PMTS);
	}

	public void setWsBypassPmtsFormatted(String wsBypassPmts) {
		this.wsBypassPmts = Trunc.toUnsignedNumeric(wsBypassPmts, Len.WS_BYPASS_PMTS);
	}

	public int getWsBypassPmts() {
		return NumericDisplay.asInt(this.wsBypassPmts);
	}

	public String getWsBypassPmtsFormatted() {
		return this.wsBypassPmts;
	}

	public String getWsBypassPmtsAsString() {
		return getWsBypassPmtsFormatted();
	}

	public void setWsBypassProv(int wsBypassProv) {
		this.wsBypassProv = NumericDisplay.asString(wsBypassProv, Len.WS_BYPASS_PROV);
	}

	public void setWsBypassProvFormatted(String wsBypassProv) {
		this.wsBypassProv = Trunc.toUnsignedNumeric(wsBypassProv, Len.WS_BYPASS_PROV);
	}

	public int getWsBypassProv() {
		return NumericDisplay.asInt(this.wsBypassProv);
	}

	public String getWsBypassProvFormatted() {
		return this.wsBypassProv;
	}

	public String getWsBypassProvAsString() {
		return getWsBypassProvFormatted();
	}

	public void setWsEobRegOut(int wsEobRegOut) {
		this.wsEobRegOut = NumericDisplay.asString(wsEobRegOut, Len.WS_EOB_REG_OUT);
	}

	public void setWsEobRegOutFormatted(String wsEobRegOut) {
		this.wsEobRegOut = Trunc.toUnsignedNumeric(wsEobRegOut, Len.WS_EOB_REG_OUT);
	}

	public int getWsEobRegOut() {
		return NumericDisplay.asInt(this.wsEobRegOut);
	}

	public String getWsEobRegOutFormatted() {
		return this.wsEobRegOut;
	}

	public String getWsEobRegOutAsString() {
		return getWsEobRegOutFormatted();
	}

	public void setWsLinesRegOut(int wsLinesRegOut) {
		this.wsLinesRegOut = NumericDisplay.asString(wsLinesRegOut, Len.WS_LINES_REG_OUT);
	}

	public void setWsLinesRegOutFormatted(String wsLinesRegOut) {
		this.wsLinesRegOut = Trunc.toUnsignedNumeric(wsLinesRegOut, Len.WS_LINES_REG_OUT);
	}

	public int getWsLinesRegOut() {
		return NumericDisplay.asInt(this.wsLinesRegOut);
	}

	public String getWsLinesRegOutFormatted() {
		return this.wsLinesRegOut;
	}

	public String getWsLinesRegOutAsString() {
		return getWsLinesRegOutFormatted();
	}

	public void setWsLinesKssOutFormatted(String wsLinesKssOut) {
		this.wsLinesKssOut = Trunc.toUnsignedNumeric(wsLinesKssOut, Len.WS_LINES_KSS_OUT);
	}

	public int getWsLinesKssOut() {
		return NumericDisplay.asInt(this.wsLinesKssOut);
	}

	public void setWsLinesKssExpOutFormatted(String wsLinesKssExpOut) {
		this.wsLinesKssExpOut = Trunc.toUnsignedNumeric(wsLinesKssExpOut, Len.WS_LINES_KSS_EXP_OUT);
	}

	public int getWsLinesKssExpOut() {
		return NumericDisplay.asInt(this.wsLinesKssExpOut);
	}

	public void setWsItsY1Cnt(int wsItsY1Cnt) {
		this.wsItsY1Cnt = NumericDisplay.asString(wsItsY1Cnt, Len.WS_ITS_Y1_CNT);
	}

	public void setWsItsY1CntFormatted(String wsItsY1Cnt) {
		this.wsItsY1Cnt = Trunc.toUnsignedNumeric(wsItsY1Cnt, Len.WS_ITS_Y1_CNT);
	}

	public int getWsItsY1Cnt() {
		return NumericDisplay.asInt(this.wsItsY1Cnt);
	}

	public String getWsItsY1CntFormatted() {
		return this.wsItsY1Cnt;
	}

	public String getWsItsY1CntAsString() {
		return getWsItsY1CntFormatted();
	}

	public void setWsS02813Update(int wsS02813Update) {
		this.wsS02813Update = NumericDisplay.asString(wsS02813Update, Len.WS_S02813_UPDATE);
	}

	public void setWsS02813UpdateFormatted(String wsS02813Update) {
		this.wsS02813Update = Trunc.toUnsignedNumeric(wsS02813Update, Len.WS_S02813_UPDATE);
	}

	public int getWsS02813Update() {
		return NumericDisplay.asInt(this.wsS02813Update);
	}

	public String getWsS02813UpdateFormatted() {
		return this.wsS02813Update;
	}

	public String getWsS02813UpdateAsString() {
		return getWsS02813UpdateFormatted();
	}

	public void setWsS04315Updates(int wsS04315Updates) {
		this.wsS04315Updates = NumericDisplay.asString(wsS04315Updates, Len.WS_S04315_UPDATES);
	}

	public void setWsS04315UpdatesFormatted(String wsS04315Updates) {
		this.wsS04315Updates = Trunc.toUnsignedNumeric(wsS04315Updates, Len.WS_S04315_UPDATES);
	}

	public int getWsS04315Updates() {
		return NumericDisplay.asInt(this.wsS04315Updates);
	}

	public String getWsS04315UpdatesFormatted() {
		return this.wsS04315Updates;
	}

	public String getWsS04315UpdatesAsString() {
		return getWsS04315UpdatesFormatted();
	}

	public void setWsS02813VoidUpdate(int wsS02813VoidUpdate) {
		this.wsS02813VoidUpdate = NumericDisplay.asString(wsS02813VoidUpdate, Len.WS_S02813_VOID_UPDATE);
	}

	public void setWsS02813VoidUpdateFormatted(String wsS02813VoidUpdate) {
		this.wsS02813VoidUpdate = Trunc.toUnsignedNumeric(wsS02813VoidUpdate, Len.WS_S02813_VOID_UPDATE);
	}

	public int getWsS02813VoidUpdate() {
		return NumericDisplay.asInt(this.wsS02813VoidUpdate);
	}

	public String getWsS02813VoidUpdateFormatted() {
		return this.wsS02813VoidUpdate;
	}

	public String getWsS02813VoidUpdateAsString() {
		return getWsS02813VoidUpdateFormatted();
	}

	public void setWsS02801Inserted(int wsS02801Inserted) {
		this.wsS02801Inserted = NumericDisplay.asString(wsS02801Inserted, Len.WS_S02801_INSERTED);
	}

	public void setWsS02801InsertedFormatted(String wsS02801Inserted) {
		this.wsS02801Inserted = Trunc.toUnsignedNumeric(wsS02801Inserted, Len.WS_S02801_INSERTED);
	}

	public int getWsS02801Inserted() {
		return NumericDisplay.asInt(this.wsS02801Inserted);
	}

	public String getWsS02801InsertedFormatted() {
		return this.wsS02801Inserted;
	}

	public String getWsS02801InsertedAsString() {
		return getWsS02801InsertedFormatted();
	}

	public void setWsS02814Inserted(int wsS02814Inserted) {
		this.wsS02814Inserted = NumericDisplay.asString(wsS02814Inserted, Len.WS_S02814_INSERTED);
	}

	public void setWsS02814InsertedFormatted(String wsS02814Inserted) {
		this.wsS02814Inserted = Trunc.toUnsignedNumeric(wsS02814Inserted, Len.WS_S02814_INSERTED);
	}

	public int getWsS02814Inserted() {
		return NumericDisplay.asInt(this.wsS02814Inserted);
	}

	public String getWsS02814InsertedFormatted() {
		return this.wsS02814Inserted;
	}

	public String getWsS02814InsertedAsString() {
		return getWsS02814InsertedFormatted();
	}

	public void setWsS02814MthQtr(int wsS02814MthQtr) {
		this.wsS02814MthQtr = NumericDisplay.asString(wsS02814MthQtr, Len.WS_S02814_MTH_QTR);
	}

	public void setWsS02814MthQtrFormatted(String wsS02814MthQtr) {
		this.wsS02814MthQtr = Trunc.toUnsignedNumeric(wsS02814MthQtr, Len.WS_S02814_MTH_QTR);
	}

	public int getWsS02814MthQtr() {
		return NumericDisplay.asInt(this.wsS02814MthQtr);
	}

	public String getWsS02814MthQtrFormatted() {
		return this.wsS02814MthQtr;
	}

	public String getWsS02814MthQtrAsString() {
		return getWsS02814MthQtrFormatted();
	}

	public void setWsMthQtr(int wsMthQtr) {
		this.wsMthQtr = NumericDisplay.asString(wsMthQtr, Len.WS_MTH_QTR);
	}

	public void setWsMthQtrFormatted(String wsMthQtr) {
		this.wsMthQtr = Trunc.toUnsignedNumeric(wsMthQtr, Len.WS_MTH_QTR);
	}

	public int getWsMthQtr() {
		return NumericDisplay.asInt(this.wsMthQtr);
	}

	public String getWsMthQtrFormatted() {
		return this.wsMthQtr;
	}

	public String getWsMthQtrAsString() {
		return getWsMthQtrFormatted();
	}

	public void setCommitCounter(int commitCounter) {
		this.commitCounter = NumericDisplay.asString(commitCounter, Len.COMMIT_COUNTER);
	}

	public void setCommitCounterFormatted(String commitCounter) {
		this.commitCounter = Trunc.toUnsignedNumeric(commitCounter, Len.COMMIT_COUNTER);
	}

	public int getCommitCounter() {
		return NumericDisplay.asInt(this.commitCounter);
	}

	public String getCommitCounterFormatted() {
		return this.commitCounter;
	}

	public String getCommitCounterAsString() {
		return getCommitCounterFormatted();
	}

	public void setErrFileCnt(int errFileCnt) {
		this.errFileCnt = NumericDisplay.asString(errFileCnt, Len.ERR_FILE_CNT);
	}

	public void setErrFileCntFormatted(String errFileCnt) {
		this.errFileCnt = Trunc.toUnsignedNumeric(errFileCnt, Len.ERR_FILE_CNT);
	}

	public int getErrFileCnt() {
		return NumericDisplay.asInt(this.errFileCnt);
	}

	public void setSrsPrvMatchCntr(int srsPrvMatchCntr) {
		this.srsPrvMatchCntr = NumericDisplay.asString(srsPrvMatchCntr, Len.SRS_PRV_MATCH_CNTR);
	}

	public void setSrsPrvMatchCntrFormatted(String srsPrvMatchCntr) {
		this.srsPrvMatchCntr = Trunc.toUnsignedNumeric(srsPrvMatchCntr, Len.SRS_PRV_MATCH_CNTR);
	}

	public int getSrsPrvMatchCntr() {
		return NumericDisplay.asInt(this.srsPrvMatchCntr);
	}

	public void setSrsDateBypassCntrFormatted(String srsDateBypassCntr) {
		this.srsDateBypassCntr = Trunc.toUnsignedNumeric(srsDateBypassCntr, Len.SRS_DATE_BYPASS_CNTR);
	}

	public int getSrsDateBypassCntr() {
		return NumericDisplay.asInt(this.srsDateBypassCntr);
	}

	public String getSrsDateBypassCntrFormatted() {
		return this.srsDateBypassCntr;
	}

	public String getSrsDateBypassCntrAsString() {
		return getSrsDateBypassCntrFormatted();
	}

	public void setSrsClaimCntrFormatted(String srsClaimCntr) {
		this.srsClaimCntr = Trunc.toUnsignedNumeric(srsClaimCntr, Len.SRS_CLAIM_CNTR);
	}

	public int getSrsClaimCntr() {
		return NumericDisplay.asInt(this.srsClaimCntr);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_FETCH_CNTR = 7;
		public static final int WS_TRIGGER_IN = 7;
		public static final int WS_TRIGGER_LOADED = 7;
		public static final int WS_BYPASS_PMTS = 7;
		public static final int WS_BYPASS_PROV = 7;
		public static final int WS_EOB_REG_OUT = 7;
		public static final int WS_LINES_REG_OUT = 7;
		public static final int WS_LINES_KSS_OUT = 7;
		public static final int WS_LINES_KSS_EXP_OUT = 7;
		public static final int WS_ITS_Y1_CNT = 7;
		public static final int WS_S02813_UPDATE = 7;
		public static final int WS_S02813_VOID_UPDATE = 7;
		public static final int WS_S02801_INSERTED = 7;
		public static final int WS_S02814_INSERTED = 7;
		public static final int WS_S02814_MTH_QTR = 7;
		public static final int WS_MTH_QTR = 7;
		public static final int COMMIT_COUNTER = 7;
		public static final int ERR_FILE_CNT = 7;
		public static final int SRS_PRV_MATCH_CNTR = 7;
		public static final int SRS_DATE_BYPASS_CNTR = 7;
		public static final int SRS_CLAIM_CNTR = 7;
		public static final int WS_S04315_UPDATES = 7;
		public static final int WS_COUNTERS = WS_FETCH_CNTR + WS_TRIGGER_IN + WS_TRIGGER_LOADED + WS_BYPASS_PMTS + WS_BYPASS_PROV + WS_EOB_REG_OUT
				+ WS_LINES_REG_OUT + WS_LINES_KSS_OUT + WS_LINES_KSS_EXP_OUT + WS_ITS_Y1_CNT + WS_S02813_UPDATE + WS_S04315_UPDATES
				+ WS_S02813_VOID_UPDATE + WS_S02801_INSERTED + WS_S02814_INSERTED + WS_S02814_MTH_QTR + WS_MTH_QTR + COMMIT_COUNTER + ERR_FILE_CNT
				+ SRS_PRV_MATCH_CNTR + SRS_DATE_BYPASS_CNTR + SRS_CLAIM_CNTR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
