/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.enums.CsrLocalBillProvLobCd;

/**Original name: CSR-BILLING-PROVIDER<br>
 * Variable: CSR-BILLING-PROVIDER from copybook NF07AREA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CsrBillingProvider {

	//==== PROPERTIES ====
	//Original name: CSR-LOCAL-BILL-PROV-LOB-CD
	private CsrLocalBillProvLobCd localBillProvLobCd = new CsrLocalBillProvLobCd();
	//Original name: CSR-HIST-BILL-PROV-NPI-ID
	private String histBillProvNpiId = "";
	//Original name: CSR-LOCAL-BILL-PROV-ID
	private String localBillProvId = "";
	//Original name: CSR-MEDICARE-XOVER
	private char medicareXover = Types.SPACE_CHAR;

	//==== METHODS ====
	public byte[] getBillingProviderBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, localBillProvLobCd.getLocalBillProvLobCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, histBillProvNpiId, Len.HIST_BILL_PROV_NPI_ID);
		position += Len.HIST_BILL_PROV_NPI_ID;
		MarshalByte.writeString(buffer, position, localBillProvId, Len.LOCAL_BILL_PROV_ID);
		position += Len.LOCAL_BILL_PROV_ID;
		MarshalByte.writeChar(buffer, position, medicareXover);
		return buffer;
	}

	public void initBillingProviderSpaces() {
		localBillProvLobCd.setLocalBillProvLobCd(Types.SPACE_CHAR);
		histBillProvNpiId = "";
		localBillProvId = "";
		medicareXover = Types.SPACE_CHAR;
	}

	public void setHistBillProvNpiId(String histBillProvNpiId) {
		this.histBillProvNpiId = Functions.subString(histBillProvNpiId, Len.HIST_BILL_PROV_NPI_ID);
	}

	public String getHistBillProvNpiId() {
		return this.histBillProvNpiId;
	}

	public void setLocalBillProvId(String localBillProvId) {
		this.localBillProvId = Functions.subString(localBillProvId, Len.LOCAL_BILL_PROV_ID);
	}

	public String getLocalBillProvId() {
		return this.localBillProvId;
	}

	public void setMedicareXover(char medicareXover) {
		this.medicareXover = medicareXover;
	}

	public void setMedicareXoverFormatted(String medicareXover) {
		setMedicareXover(Functions.charAt(medicareXover, Types.CHAR_SIZE));
	}

	public char getMedicareXover() {
		return this.medicareXover;
	}

	public CsrLocalBillProvLobCd getLocalBillProvLobCd() {
		return localBillProvLobCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HIST_BILL_PROV_NPI_ID = 10;
		public static final int LOCAL_BILL_PROV_ID = 10;
		public static final int MEDICARE_XOVER = 1;
		public static final int BILLING_PROVIDER = CsrLocalBillProvLobCd.Len.VALUE + HIST_BILL_PROV_NPI_ID + LOCAL_BILL_PROV_ID + MEDICARE_XOVER;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
