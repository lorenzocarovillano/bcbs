/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: DCLS02813SA<br>
 * Variable: DCLS02813SA from copybook S02813SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02813sa {

	//==== PROPERTIES ====
	//Original name: CLM-CNTL-ID
	private String clmCntlId = DefaultValues.stringVal(Len.CLM_CNTL_ID);
	//Original name: CLM-CNTL-SFX-ID
	private char clmCntlSfxId = DefaultValues.CHAR_VAL;
	//Original name: CLM-PMT-ID
	private short clmPmtId = DefaultValues.SHORT_VAL;
	//Original name: CK-DT
	private String ckDt = DefaultValues.stringVal(Len.CK_DT);
	//Original name: CK-ID
	private String ckId = DefaultValues.stringVal(Len.CK_ID);
	//Original name: CLM-PD-AM
	private AfDecimal clmPdAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: ADJ-TYP-CD
	private char adjTypCd = DefaultValues.CHAR_VAL;
	//Original name: KCAPS-TEAM-NM
	private String kcapsTeamNm = DefaultValues.stringVal(Len.KCAPS_TEAM_NM);
	//Original name: KCAPS-USE-ID
	private String kcapsUseId = DefaultValues.stringVal(Len.KCAPS_USE_ID);
	//Original name: HIST-LOAD-CD
	private char histLoadCd = DefaultValues.CHAR_VAL;
	//Original name: DRG-CD
	private String drgCd = DefaultValues.stringVal(Len.DRG_CD);
	//Original name: SCHDL-DRG-ALW-AM
	private AfDecimal schdlDrgAlwAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: PGM-AREA-CD
	private String pgmAreaCd = DefaultValues.stringVal(Len.PGM_AREA_CD);
	//Original name: FIN-CD
	private String finCd = DefaultValues.stringVal(Len.FIN_CD);
	//Original name: VOID-CD
	private char voidCd = DefaultValues.CHAR_VAL;
	//Original name: ITS-CLM-TYP-CD
	private char itsClmTypCd = DefaultValues.CHAR_VAL;
	//Original name: PRMPT-PAY-INT-AM
	private AfDecimal prmptPayIntAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: NPI-CD
	private char npiCd = DefaultValues.CHAR_VAL;
	//Original name: LOB-CD
	private char lobCd = DefaultValues.CHAR_VAL;
	//Original name: TYP-GRP-CD
	private String typGrpCd = DefaultValues.stringVal(Len.TYP_GRP_CD);
	//Original name: NTWRK-CD
	private String ntwrkCd = DefaultValues.stringVal(Len.NTWRK_CD);
	//Original name: ENR-CL-CD
	private String enrClCd = DefaultValues.stringVal(Len.ENR_CL_CD);
	//Original name: GL-OFST-VOID-CD
	private String glOfstVoidCd = DefaultValues.stringVal(Len.GL_OFST_VOID_CD);
	//Original name: GL-SOTE-VOID-CD
	private short glSoteVoidCd = DefaultValues.SHORT_VAL;
	//Original name: ADJ-RESP-CD
	private char adjRespCd = DefaultValues.CHAR_VAL;
	//Original name: ADJ-TRCK-CD
	private String adjTrckCd = DefaultValues.stringVal(Len.ADJ_TRCK_CD);
	//Original name: ACR-PRMT-PA-INT-AM
	private AfDecimal acrPrmtPaIntAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: MEM-ID
	private String memId = DefaultValues.stringVal(Len.MEM_ID);
	//Original name: VBR-IN
	private char vbrIn = DefaultValues.CHAR_VAL;
	//Original name: MRKT-PKG-CD
	private String mrktPkgCd = DefaultValues.stringVal(Len.MRKT_PKG_CD);

	//==== METHODS ====
	public void setClmCntlId(String clmCntlId) {
		this.clmCntlId = Functions.subString(clmCntlId, Len.CLM_CNTL_ID);
	}

	public String getClmCntlId() {
		return this.clmCntlId;
	}

	public String getClmCntlIdFormatted() {
		return Functions.padBlanks(getClmCntlId(), Len.CLM_CNTL_ID);
	}

	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.clmCntlSfxId = clmCntlSfxId;
	}

	public char getClmCntlSfxId() {
		return this.clmCntlSfxId;
	}

	public void setClmPmtId(short clmPmtId) {
		this.clmPmtId = clmPmtId;
	}

	public short getClmPmtId() {
		return this.clmPmtId;
	}

	public String getClmPmtIdFormatted() {
		return PicFormatter.display(new PicParams("S9(2)V").setUsage(PicUsage.PACKED)).format(getClmPmtId()).toString();
	}

	public String getClmPmtIdAsString() {
		return getClmPmtIdFormatted();
	}

	public void setCkDt(String ckDt) {
		this.ckDt = Functions.subString(ckDt, Len.CK_DT);
	}

	public String getCkDt() {
		return this.ckDt;
	}

	public void setCkId(String ckId) {
		this.ckId = Functions.subString(ckId, Len.CK_ID);
	}

	public String getCkId() {
		return this.ckId;
	}

	public void setClmPdAm(AfDecimal clmPdAm) {
		this.clmPdAm.assign(clmPdAm);
	}

	public AfDecimal getClmPdAm() {
		return this.clmPdAm.copy();
	}

	public void setAdjTypCd(char adjTypCd) {
		this.adjTypCd = adjTypCd;
	}

	public char getAdjTypCd() {
		return this.adjTypCd;
	}

	public void setKcapsTeamNm(String kcapsTeamNm) {
		this.kcapsTeamNm = Functions.subString(kcapsTeamNm, Len.KCAPS_TEAM_NM);
	}

	public String getKcapsTeamNm() {
		return this.kcapsTeamNm;
	}

	public void setKcapsUseId(String kcapsUseId) {
		this.kcapsUseId = Functions.subString(kcapsUseId, Len.KCAPS_USE_ID);
	}

	public String getKcapsUseId() {
		return this.kcapsUseId;
	}

	public void setHistLoadCd(char histLoadCd) {
		this.histLoadCd = histLoadCd;
	}

	public char getHistLoadCd() {
		return this.histLoadCd;
	}

	public void setDrgCd(String drgCd) {
		this.drgCd = Functions.subString(drgCd, Len.DRG_CD);
	}

	public String getDrgCd() {
		return this.drgCd;
	}

	public void setSchdlDrgAlwAm(AfDecimal schdlDrgAlwAm) {
		this.schdlDrgAlwAm.assign(schdlDrgAlwAm);
	}

	public AfDecimal getSchdlDrgAlwAm() {
		return this.schdlDrgAlwAm.copy();
	}

	public void setPgmAreaCd(String pgmAreaCd) {
		this.pgmAreaCd = Functions.subString(pgmAreaCd, Len.PGM_AREA_CD);
	}

	public String getPgmAreaCd() {
		return this.pgmAreaCd;
	}

	public void setFinCd(String finCd) {
		this.finCd = Functions.subString(finCd, Len.FIN_CD);
	}

	public String getFinCd() {
		return this.finCd;
	}

	public void setVoidCd(char voidCd) {
		this.voidCd = voidCd;
	}

	public char getVoidCd() {
		return this.voidCd;
	}

	public void setItsClmTypCd(char itsClmTypCd) {
		this.itsClmTypCd = itsClmTypCd;
	}

	public char getItsClmTypCd() {
		return this.itsClmTypCd;
	}

	public void setPrmptPayIntAm(AfDecimal prmptPayIntAm) {
		this.prmptPayIntAm.assign(prmptPayIntAm);
	}

	public AfDecimal getPrmptPayIntAm() {
		return this.prmptPayIntAm.copy();
	}

	public void setNpiCd(char npiCd) {
		this.npiCd = npiCd;
	}

	public char getNpiCd() {
		return this.npiCd;
	}

	public void setLobCd(char lobCd) {
		this.lobCd = lobCd;
	}

	public char getLobCd() {
		return this.lobCd;
	}

	public void setTypGrpCd(String typGrpCd) {
		this.typGrpCd = Functions.subString(typGrpCd, Len.TYP_GRP_CD);
	}

	public String getTypGrpCd() {
		return this.typGrpCd;
	}

	public void setNtwrkCd(String ntwrkCd) {
		this.ntwrkCd = Functions.subString(ntwrkCd, Len.NTWRK_CD);
	}

	public String getNtwrkCd() {
		return this.ntwrkCd;
	}

	public void setEnrClCd(String enrClCd) {
		this.enrClCd = Functions.subString(enrClCd, Len.ENR_CL_CD);
	}

	public String getEnrClCd() {
		return this.enrClCd;
	}

	public void setGlOfstVoidCd(String glOfstVoidCd) {
		this.glOfstVoidCd = Functions.subString(glOfstVoidCd, Len.GL_OFST_VOID_CD);
	}

	public String getGlOfstVoidCd() {
		return this.glOfstVoidCd;
	}

	public void setGlSoteVoidCd(short glSoteVoidCd) {
		this.glSoteVoidCd = glSoteVoidCd;
	}

	public short getGlSoteVoidCd() {
		return this.glSoteVoidCd;
	}

	public void setAdjRespCd(char adjRespCd) {
		this.adjRespCd = adjRespCd;
	}

	public char getAdjRespCd() {
		return this.adjRespCd;
	}

	public void setAdjTrckCd(String adjTrckCd) {
		this.adjTrckCd = Functions.subString(adjTrckCd, Len.ADJ_TRCK_CD);
	}

	public String getAdjTrckCd() {
		return this.adjTrckCd;
	}

	public void setAcrPrmtPaIntAm(AfDecimal acrPrmtPaIntAm) {
		this.acrPrmtPaIntAm.assign(acrPrmtPaIntAm);
	}

	public AfDecimal getAcrPrmtPaIntAm() {
		return this.acrPrmtPaIntAm.copy();
	}

	public void setMemId(String memId) {
		this.memId = Functions.subString(memId, Len.MEM_ID);
	}

	public String getMemId() {
		return this.memId;
	}

	public void setVbrIn(char vbrIn) {
		this.vbrIn = vbrIn;
	}

	public char getVbrIn() {
		return this.vbrIn;
	}

	public void setMrktPkgCd(String mrktPkgCd) {
		this.mrktPkgCd = Functions.subString(mrktPkgCd, Len.MRKT_PKG_CD);
	}

	public String getMrktPkgCd() {
		return this.mrktPkgCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_CNTL_ID = 12;
		public static final int CK_DT = 10;
		public static final int CK_ID = 15;
		public static final int ALPH_PRFX_CD = 3;
		public static final int INS_ID = 12;
		public static final int INS_DEP_ID = 2;
		public static final int NATL_PLN_CD = 3;
		public static final int ORIG_CLM_CNTL_ID = 12;
		public static final int CORP_RCV_DT = 10;
		public static final int PMT_ADJD_DT = 10;
		public static final int EXT_ADJD_DT = 10;
		public static final int PMT_FRM_SERV_DT = 10;
		public static final int PMT_THR_SERV_DT = 10;
		public static final int BILL_FRM_DT = 10;
		public static final int BILL_THR_DT = 10;
		public static final int IRC_CD = 2;
		public static final int PND_LCT_CD = 5;
		public static final int AM_DT = 10;
		public static final int ASG_CD = 2;
		public static final int PAT_STAT_CD = 2;
		public static final int PAT_ADM_HR_CD = 2;
		public static final int PAT_DCHG_HR_CD = 2;
		public static final int TS_CNVRSN_CD = 6;
		public static final int GRP_CL_CD = 2;
		public static final int KCAPS_COV_ID = 8;
		public static final int KCAPS_TEAM_NM = 5;
		public static final int KCAPS_USE_ID = 3;
		public static final int DRG_CD = 4;
		public static final int MED_HIC_ID = 12;
		public static final int MED_ICN_ID = 15;
		public static final int PAT_ACT_MED_REC_ID = 25;
		public static final int PGM_AREA_CD = 3;
		public static final int PROV_RSK_POOL_CD = 3;
		public static final int FIN_CD = 3;
		public static final int SUB_CL_CD = 3;
		public static final int ASSN_CD = 3;
		public static final int SI_CD = 5;
		public static final int NTWRK_PROV_ARNG_CD = 3;
		public static final int FEP_BCH_CRT_DT = 10;
		public static final int FEP_BCH_DT = 10;
		public static final int FEP_BCH_ID = 5;
		public static final int FEP_PLN_CD = 3;
		public static final int FEP_DBR_PROV_RT_DT = 10;
		public static final int FEP_CL_CT_TYPCR_CD = 2;
		public static final int ITS_INS_ID = 17;
		public static final int ITS_CLM_ID = 17;
		public static final int ITS_SCCF_CRS_RF_ID = 17;
		public static final int ITS_NN_FT_AJ_RN_CD = 2;
		public static final int ITS_INCORR_INS_ID = 17;
		public static final int OP_PRC_PROC_CD = 5;
		public static final int NTWRK_PROV_TYP_CD = 2;
		public static final int INVSTGT_DAY_EFF_DT = 10;
		public static final int CRV_OUT_EFF_DT = 10;
		public static final int BILL_TYP_CD = 3;
		public static final int PRMPT_PAY_DAY_CD = 2;
		public static final int ADS_CD = 3;
		public static final int BILL_PROV_CNTY_CD = 3;
		public static final int PROV_UNWRP_DT = 10;
		public static final int PRIM_PSS_PR_GRP_ID = 3;
		public static final int GRP_ID = 9;
		public static final int PND_CD = 2;
		public static final int RATE_CD = 2;
		public static final int CLNT_RPT_CATG_ID = 30;
		public static final int TYP_GRP_CD = 3;
		public static final int PFM_PROV_NTWRK_CD = 3;
		public static final int NTWRK_CD = 3;
		public static final int BASE_CN_ARNG_CD = 5;
		public static final int PRIM_CN_ARNG_CD = 5;
		public static final int ADDNL_CN_ARNG_CD = 5;
		public static final int PCP_CN_ARNG_CD = 5;
		public static final int PRC_INST_REIMB_CD = 2;
		public static final int PRC_PROF_REIMB_CD = 2;
		public static final int ELIG_INST_REIMB_CD = 2;
		public static final int ELIG_PROF_REIMB_CD = 2;
		public static final int ENR_CL_CD = 3;
		public static final int GL_OFST_ORIG_CD = 2;
		public static final int GL_OFST_VOID_CD = 2;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_TS = 26;
		public static final int ADJ_TRCK_CD = 2;
		public static final int ADM_DT = 10;
		public static final int DCHG_DT = 10;
		public static final int CLNT_LTR_ID = 10;
		public static final int HIPAA_VER_FRMT_ID = 10;
		public static final int COV_LPSE_DT = 10;
		public static final int ACC_ST_CD = 2;
		public static final int PAUTH_ID = 50;
		public static final int MSTR_PLCY_ID = 30;
		public static final int MEM_ID = 14;
		public static final int CLM_PROD_CD = 2;
		public static final int CLM_INS_LN_CD = 3;
		public static final int CLM_GL_ACCT_ID = 8;
		public static final int PA_ID_TO_ACUM_ID = 12;
		public static final int MEM_ID_TO_ACUM_ID = 14;
		public static final int CLM_SYST_VER_ID = 6;
		public static final int GRP_NM = 45;
		public static final int MSTR_PLCY_NM = 50;
		public static final int BEN_PLN_AS_DT = 10;
		public static final int EMP_STAT_CD = 2;
		public static final int VND_PERS_CD = 2;
		public static final int LMP_DT = 10;
		public static final int LMP_DT_TM_QLF_CD = 3;
		public static final int ACC_DT_TM_QLF_CD = 3;
		public static final int ORIG_REF_ID = 18;
		public static final int ADDNL_CLM_INFO_TX = 71;
		public static final int OI_GRP_ID = 30;
		public static final int SPEC_PRC_COND_CD = 3;
		public static final int MRKT_PKG_CD = 30;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
