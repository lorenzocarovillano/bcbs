/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: DCLS02809SA<br>
 * Variable: DCLS02809SA from copybook S02809SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02809sa {

	//==== PROPERTIES ====
	//Original name: CLM-CNTL-ID
	private String clmCntlId = DefaultValues.stringVal(Len.CLM_CNTL_ID);
	//Original name: CLM-CNTL-SFX-ID
	private char clmCntlSfxId = DefaultValues.CHAR_VAL;
	//Original name: CLM-PMT-ID
	private short clmPmtId = DefaultValues.SHORT_VAL;
	//Original name: CLI-ID
	private short cliId = DefaultValues.SHORT_VAL;
	//Original name: PROV-SBMT-LN-NO-ID
	private short provSbmtLnNoId = DefaultValues.SHORT_VAL;
	//Original name: ADDNL-RMT-LI-ID
	private short addnlRmtLiId = DefaultValues.SHORT_VAL;
	//Original name: LI-FRM-SERV-DT
	private String liFrmServDt = DefaultValues.stringVal(Len.LI_FRM_SERV_DT);
	//Original name: LI-THR-SERV-DT
	private String liThrServDt = DefaultValues.stringVal(Len.LI_THR_SERV_DT);
	//Original name: BEN-TYP-CD
	private String benTypCd = DefaultValues.stringVal(Len.BEN_TYP_CD);
	//Original name: EXMN-ACTN-CD
	private char exmnActnCd = DefaultValues.CHAR_VAL;
	//Original name: DNL-RMRK-CD
	private String dnlRmrkCd = DefaultValues.stringVal(Len.DNL_RMRK_CD);
	//Original name: LI-EXPLN-CD
	private char liExplnCd = DefaultValues.CHAR_VAL;
	//Original name: RVW-BY-CD
	private String rvwByCd = DefaultValues.stringVal(Len.RVW_BY_CD);
	//Original name: PROC-CD
	private String procCd = DefaultValues.stringVal(Len.PROC_CD);
	//Original name: PROC-MOD1-CD
	private String procMod1Cd = DefaultValues.stringVal(Len.PROC_MOD1_CD);
	//Original name: PROC-MOD2-CD
	private String procMod2Cd = DefaultValues.stringVal(Len.PROC_MOD2_CD);
	//Original name: PROC-MOD3-CD
	private String procMod3Cd = DefaultValues.stringVal(Len.PROC_MOD3_CD);
	//Original name: PROC-MOD4-CD
	private String procMod4Cd = DefaultValues.stringVal(Len.PROC_MOD4_CD);
	//Original name: REV-CD
	private String revCd = DefaultValues.stringVal(Len.REV_CD);
	//Original name: UNIT-CT
	private AfDecimal unitCt = new AfDecimal(DefaultValues.DEC_VAL, 7, 2);
	//Original name: ORIG-SBMT-UNIT-CT
	private AfDecimal origSbmtUnitCt = new AfDecimal(DefaultValues.DEC_VAL, 7, 2);
	//Original name: POS-CD
	private String posCd = DefaultValues.stringVal(Len.POS_CD);
	//Original name: TS-CD
	private String tsCd = DefaultValues.stringVal(Len.TS_CD);
	//Original name: ORIG-SBMT-CHG-AM
	private AfDecimal origSbmtChgAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: CHG-AM
	private AfDecimal chgAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: LI-ALCT-OI-PD-AM
	private AfDecimal liAlctOiPdAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: LI-ALCT-OI-COV-AM
	private AfDecimal liAlctOiCovAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: LI-ALCT-OI-SAVE-AM
	private AfDecimal liAlctOiSaveAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: COV-PMT-LVL-STT-CD
	private String covPmtLvlSttCd = DefaultValues.stringVal(Len.COV_PMT_LVL_STT_CD);
	//Original name: LI-ALW-CHG-AM
	private AfDecimal liAlwChgAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: FEP-EDT-OVRD1-CD
	private String fepEdtOvrd1Cd = DefaultValues.stringVal(Len.FEP_EDT_OVRD1_CD);
	//Original name: FEP-EDT-OVRD2-CD
	private String fepEdtOvrd2Cd = DefaultValues.stringVal(Len.FEP_EDT_OVRD2_CD);
	//Original name: FEP-EDT-OVRD3-CD
	private String fepEdtOvrd3Cd = DefaultValues.stringVal(Len.FEP_EDT_OVRD3_CD);
	//Original name: ADJD-OVRD1-CD
	private String adjdOvrd1Cd = DefaultValues.stringVal(Len.ADJD_OVRD1_CD);
	//Original name: ADJD-OVRD2-CD
	private String adjdOvrd2Cd = DefaultValues.stringVal(Len.ADJD_OVRD2_CD);
	//Original name: ADJD-OVRD3-CD
	private String adjdOvrd3Cd = DefaultValues.stringVal(Len.ADJD_OVRD3_CD);
	//Original name: ADJD-OVRD4-CD
	private String adjdOvrd4Cd = DefaultValues.stringVal(Len.ADJD_OVRD4_CD);
	//Original name: ADJD-OVRD5-CD
	private String adjdOvrd5Cd = DefaultValues.stringVal(Len.ADJD_OVRD5_CD);
	//Original name: ADJD-OVRD6-CD
	private String adjdOvrd6Cd = DefaultValues.stringVal(Len.ADJD_OVRD6_CD);
	//Original name: ADJD-OVRD7-CD
	private String adjdOvrd7Cd = DefaultValues.stringVal(Len.ADJD_OVRD7_CD);
	//Original name: ADJD-OVRD8-CD
	private String adjdOvrd8Cd = DefaultValues.stringVal(Len.ADJD_OVRD8_CD);
	//Original name: ADJD-OVRD9-CD
	private String adjdOvrd9Cd = DefaultValues.stringVal(Len.ADJD_OVRD9_CD);
	//Original name: ADJD-OVRD10-CD
	private String adjdOvrd10Cd = DefaultValues.stringVal(Len.ADJD_OVRD10_CD);

	//==== METHODS ====
	public void setClmCntlId(String clmCntlId) {
		this.clmCntlId = Functions.subString(clmCntlId, Len.CLM_CNTL_ID);
	}

	public String getClmCntlId() {
		return this.clmCntlId;
	}

	public String getClmCntlIdFormatted() {
		return Functions.padBlanks(getClmCntlId(), Len.CLM_CNTL_ID);
	}

	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.clmCntlSfxId = clmCntlSfxId;
	}

	public char getClmCntlSfxId() {
		return this.clmCntlSfxId;
	}

	public void setClmPmtId(short clmPmtId) {
		this.clmPmtId = clmPmtId;
	}

	public short getClmPmtId() {
		return this.clmPmtId;
	}

	public String getClmPmtIdFormatted() {
		return PicFormatter.display(new PicParams("S9(2)V").setUsage(PicUsage.PACKED)).format(getClmPmtId()).toString();
	}

	public String getClmPmtIdAsString() {
		return getClmPmtIdFormatted();
	}

	public void setCliId(short cliId) {
		this.cliId = cliId;
	}

	public short getCliId() {
		return this.cliId;
	}

	public String getCliIdFormatted() {
		return PicFormatter.display(new PicParams("S9(3)V").setUsage(PicUsage.PACKED)).format(getCliId()).toString();
	}

	public String getCliIdAsString() {
		return getCliIdFormatted();
	}

	public void setProvSbmtLnNoId(short provSbmtLnNoId) {
		this.provSbmtLnNoId = provSbmtLnNoId;
	}

	public short getProvSbmtLnNoId() {
		return this.provSbmtLnNoId;
	}

	public void setAddnlRmtLiId(short addnlRmtLiId) {
		this.addnlRmtLiId = addnlRmtLiId;
	}

	public short getAddnlRmtLiId() {
		return this.addnlRmtLiId;
	}

	public void setLiFrmServDt(String liFrmServDt) {
		this.liFrmServDt = Functions.subString(liFrmServDt, Len.LI_FRM_SERV_DT);
	}

	public String getLiFrmServDt() {
		return this.liFrmServDt;
	}

	public String getLiFrmServDtFormatted() {
		return Functions.padBlanks(getLiFrmServDt(), Len.LI_FRM_SERV_DT);
	}

	public void setLiThrServDt(String liThrServDt) {
		this.liThrServDt = Functions.subString(liThrServDt, Len.LI_THR_SERV_DT);
	}

	public String getLiThrServDt() {
		return this.liThrServDt;
	}

	public String getLiThrServDtFormatted() {
		return Functions.padBlanks(getLiThrServDt(), Len.LI_THR_SERV_DT);
	}

	public void setBenTypCd(String benTypCd) {
		this.benTypCd = Functions.subString(benTypCd, Len.BEN_TYP_CD);
	}

	public String getBenTypCd() {
		return this.benTypCd;
	}

	public void setExmnActnCd(char exmnActnCd) {
		this.exmnActnCd = exmnActnCd;
	}

	public char getExmnActnCd() {
		return this.exmnActnCd;
	}

	public void setDnlRmrkCd(String dnlRmrkCd) {
		this.dnlRmrkCd = Functions.subString(dnlRmrkCd, Len.DNL_RMRK_CD);
	}

	public String getDnlRmrkCd() {
		return this.dnlRmrkCd;
	}

	public void setLiExplnCd(char liExplnCd) {
		this.liExplnCd = liExplnCd;
	}

	public char getLiExplnCd() {
		return this.liExplnCd;
	}

	public void setRvwByCd(String rvwByCd) {
		this.rvwByCd = Functions.subString(rvwByCd, Len.RVW_BY_CD);
	}

	public String getRvwByCd() {
		return this.rvwByCd;
	}

	public void setProcCd(String procCd) {
		this.procCd = Functions.subString(procCd, Len.PROC_CD);
	}

	public String getProcCd() {
		return this.procCd;
	}

	public void setProcMod1Cd(String procMod1Cd) {
		this.procMod1Cd = Functions.subString(procMod1Cd, Len.PROC_MOD1_CD);
	}

	public String getProcMod1Cd() {
		return this.procMod1Cd;
	}

	public void setProcMod2Cd(String procMod2Cd) {
		this.procMod2Cd = Functions.subString(procMod2Cd, Len.PROC_MOD2_CD);
	}

	public String getProcMod2Cd() {
		return this.procMod2Cd;
	}

	public void setProcMod3Cd(String procMod3Cd) {
		this.procMod3Cd = Functions.subString(procMod3Cd, Len.PROC_MOD3_CD);
	}

	public String getProcMod3Cd() {
		return this.procMod3Cd;
	}

	public void setProcMod4Cd(String procMod4Cd) {
		this.procMod4Cd = Functions.subString(procMod4Cd, Len.PROC_MOD4_CD);
	}

	public String getProcMod4Cd() {
		return this.procMod4Cd;
	}

	public void setRevCd(String revCd) {
		this.revCd = Functions.subString(revCd, Len.REV_CD);
	}

	public String getRevCd() {
		return this.revCd;
	}

	public void setUnitCt(AfDecimal unitCt) {
		this.unitCt.assign(unitCt);
	}

	public AfDecimal getUnitCt() {
		return this.unitCt.copy();
	}

	public void setOrigSbmtUnitCt(AfDecimal origSbmtUnitCt) {
		this.origSbmtUnitCt.assign(origSbmtUnitCt);
	}

	public AfDecimal getOrigSbmtUnitCt() {
		return this.origSbmtUnitCt.copy();
	}

	public void setPosCd(String posCd) {
		this.posCd = Functions.subString(posCd, Len.POS_CD);
	}

	public String getPosCd() {
		return this.posCd;
	}

	public void setTsCd(String tsCd) {
		this.tsCd = Functions.subString(tsCd, Len.TS_CD);
	}

	public String getTsCd() {
		return this.tsCd;
	}

	public void setOrigSbmtChgAm(AfDecimal origSbmtChgAm) {
		this.origSbmtChgAm.assign(origSbmtChgAm);
	}

	public AfDecimal getOrigSbmtChgAm() {
		return this.origSbmtChgAm.copy();
	}

	public void setChgAm(AfDecimal chgAm) {
		this.chgAm.assign(chgAm);
	}

	public AfDecimal getChgAm() {
		return this.chgAm.copy();
	}

	public void setLiAlctOiPdAm(AfDecimal liAlctOiPdAm) {
		this.liAlctOiPdAm.assign(liAlctOiPdAm);
	}

	public AfDecimal getLiAlctOiPdAm() {
		return this.liAlctOiPdAm.copy();
	}

	public void setLiAlctOiCovAm(AfDecimal liAlctOiCovAm) {
		this.liAlctOiCovAm.assign(liAlctOiCovAm);
	}

	public AfDecimal getLiAlctOiCovAm() {
		return this.liAlctOiCovAm.copy();
	}

	public void setLiAlctOiSaveAm(AfDecimal liAlctOiSaveAm) {
		this.liAlctOiSaveAm.assign(liAlctOiSaveAm);
	}

	public AfDecimal getLiAlctOiSaveAm() {
		return this.liAlctOiSaveAm.copy();
	}

	public void setCovPmtLvlSttCd(String covPmtLvlSttCd) {
		this.covPmtLvlSttCd = Functions.subString(covPmtLvlSttCd, Len.COV_PMT_LVL_STT_CD);
	}

	public String getCovPmtLvlSttCd() {
		return this.covPmtLvlSttCd;
	}

	public void setLiAlwChgAm(AfDecimal liAlwChgAm) {
		this.liAlwChgAm.assign(liAlwChgAm);
	}

	public AfDecimal getLiAlwChgAm() {
		return this.liAlwChgAm.copy();
	}

	public void setFepEdtOvrd1Cd(String fepEdtOvrd1Cd) {
		this.fepEdtOvrd1Cd = Functions.subString(fepEdtOvrd1Cd, Len.FEP_EDT_OVRD1_CD);
	}

	public String getFepEdtOvrd1Cd() {
		return this.fepEdtOvrd1Cd;
	}

	public void setFepEdtOvrd2Cd(String fepEdtOvrd2Cd) {
		this.fepEdtOvrd2Cd = Functions.subString(fepEdtOvrd2Cd, Len.FEP_EDT_OVRD2_CD);
	}

	public String getFepEdtOvrd2Cd() {
		return this.fepEdtOvrd2Cd;
	}

	public void setFepEdtOvrd3Cd(String fepEdtOvrd3Cd) {
		this.fepEdtOvrd3Cd = Functions.subString(fepEdtOvrd3Cd, Len.FEP_EDT_OVRD3_CD);
	}

	public String getFepEdtOvrd3Cd() {
		return this.fepEdtOvrd3Cd;
	}

	public void setAdjdOvrd1Cd(String adjdOvrd1Cd) {
		this.adjdOvrd1Cd = Functions.subString(adjdOvrd1Cd, Len.ADJD_OVRD1_CD);
	}

	public String getAdjdOvrd1Cd() {
		return this.adjdOvrd1Cd;
	}

	public void setAdjdOvrd2Cd(String adjdOvrd2Cd) {
		this.adjdOvrd2Cd = Functions.subString(adjdOvrd2Cd, Len.ADJD_OVRD2_CD);
	}

	public String getAdjdOvrd2Cd() {
		return this.adjdOvrd2Cd;
	}

	public void setAdjdOvrd3Cd(String adjdOvrd3Cd) {
		this.adjdOvrd3Cd = Functions.subString(adjdOvrd3Cd, Len.ADJD_OVRD3_CD);
	}

	public String getAdjdOvrd3Cd() {
		return this.adjdOvrd3Cd;
	}

	public void setAdjdOvrd4Cd(String adjdOvrd4Cd) {
		this.adjdOvrd4Cd = Functions.subString(adjdOvrd4Cd, Len.ADJD_OVRD4_CD);
	}

	public String getAdjdOvrd4Cd() {
		return this.adjdOvrd4Cd;
	}

	public void setAdjdOvrd5Cd(String adjdOvrd5Cd) {
		this.adjdOvrd5Cd = Functions.subString(adjdOvrd5Cd, Len.ADJD_OVRD5_CD);
	}

	public String getAdjdOvrd5Cd() {
		return this.adjdOvrd5Cd;
	}

	public void setAdjdOvrd6Cd(String adjdOvrd6Cd) {
		this.adjdOvrd6Cd = Functions.subString(adjdOvrd6Cd, Len.ADJD_OVRD6_CD);
	}

	public String getAdjdOvrd6Cd() {
		return this.adjdOvrd6Cd;
	}

	public void setAdjdOvrd7Cd(String adjdOvrd7Cd) {
		this.adjdOvrd7Cd = Functions.subString(adjdOvrd7Cd, Len.ADJD_OVRD7_CD);
	}

	public String getAdjdOvrd7Cd() {
		return this.adjdOvrd7Cd;
	}

	public void setAdjdOvrd8Cd(String adjdOvrd8Cd) {
		this.adjdOvrd8Cd = Functions.subString(adjdOvrd8Cd, Len.ADJD_OVRD8_CD);
	}

	public String getAdjdOvrd8Cd() {
		return this.adjdOvrd8Cd;
	}

	public void setAdjdOvrd9Cd(String adjdOvrd9Cd) {
		this.adjdOvrd9Cd = Functions.subString(adjdOvrd9Cd, Len.ADJD_OVRD9_CD);
	}

	public String getAdjdOvrd9Cd() {
		return this.adjdOvrd9Cd;
	}

	public void setAdjdOvrd10Cd(String adjdOvrd10Cd) {
		this.adjdOvrd10Cd = Functions.subString(adjdOvrd10Cd, Len.ADJD_OVRD10_CD);
	}

	public String getAdjdOvrd10Cd() {
		return this.adjdOvrd10Cd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_CNTL_ID = 12;
		public static final int LI_FRM_SERV_DT = 10;
		public static final int LI_THR_SERV_DT = 10;
		public static final int BEN_TYP_CD = 3;
		public static final int NPD_UNR_TYP_BEN_CD = 3;
		public static final int DNL_RMRK_CD = 3;
		public static final int RVW_BY_CD = 3;
		public static final int HCC_STAT1_CD = 3;
		public static final int HCC_STAT2_CD = 3;
		public static final int HCC_STAT3_CD = 3;
		public static final int PROC_CD = 5;
		public static final int PROC_MOD1_CD = 2;
		public static final int PROC_MOD2_CD = 2;
		public static final int PROC_MOD3_CD = 2;
		public static final int PROC_MOD4_CD = 2;
		public static final int REV_CD = 4;
		public static final int POS_CD = 2;
		public static final int TS_CD = 2;
		public static final int CMNT_CD = 2;
		public static final int CLM_PRS_FRM_LTR_CD = 2;
		public static final int RFR_CNTL_DT = 10;
		public static final int RFR_CNTL_CMNT_TX = 10;
		public static final int PCERT_CNTL_DT = 10;
		public static final int PCERT_CNTL_CMNT_TX = 10;
		public static final int RVW_SEQ_ID = 4;
		public static final int COV_PMT_LVL_STT_CD = 3;
		public static final int REV_LN_PRC_MTHD_CD = 2;
		public static final int LN_RULE_CD = 6;
		public static final int LN_SEC_RULE_CD = 6;
		public static final int TH_ID = 2;
		public static final int TH_SRFC1_CD = 2;
		public static final int TH_SRFC2_CD = 2;
		public static final int TH_SRFC3_CD = 2;
		public static final int TH_SRFC4_CD = 2;
		public static final int TH_SRFC5_CD = 2;
		public static final int GMIS_RSN_CD = 3;
		public static final int FEP_EDT_OVRD1_CD = 4;
		public static final int FEP_EDT_OVRD2_CD = 4;
		public static final int FEP_EDT_OVRD3_CD = 4;
		public static final int FRMLRY_CD = 3;
		public static final int SRO_RMRK_CD = 2;
		public static final int PB_FND_ALCT_TYP_CD = 3;
		public static final int BASE_SCHDL_NO_ID = 3;
		public static final int PRIM_SCHDL_NO_ID = 3;
		public static final int ADDNL_SCHDL_NO_ID = 3;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_TS = 26;
		public static final int ADJD_OVRD1_CD = 3;
		public static final int ADJD_OVRD2_CD = 3;
		public static final int ADJD_OVRD3_CD = 3;
		public static final int ADJD_OVRD4_CD = 3;
		public static final int ADJD_OVRD5_CD = 3;
		public static final int ADJD_OVRD6_CD = 3;
		public static final int ADJD_OVRD7_CD = 3;
		public static final int ADJD_OVRD8_CD = 3;
		public static final int ADJD_OVRD9_CD = 3;
		public static final int ADJD_OVRD10_CD = 3;
		public static final int FAC_VL_CD = 2;
		public static final int FAC_QLF_CD = 2;
		public static final int DIAG_CATG_CD = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
