/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [S02952SA]
 * 
 */
public interface IS02952sa extends BaseSqlTo {

	/**
	 * Host Variable LST-ORG-NM
	 * 
	 */
	String getLstOrgNm();

	void setLstOrgNm(String lstOrgNm);

	/**
	 * Host Variable FRST-NM
	 * 
	 */
	String getFrstNm();

	void setFrstNm(String frstNm);

	/**
	 * Host Variable MID-NM
	 * 
	 */
	String getMidNm();

	void setMidNm(String midNm);

	/**
	 * Host Variable SFX-NM
	 * 
	 */
	String getSfxNm();

	void setSfxNm(String sfxNm);

	/**
	 * Host Variable SSN-ID
	 * 
	 */
	String getSsnId();

	void setSsnId(String ssnId);

	/**
	 * Host Variable ENTY-ID-CD
	 * 
	 */
	String getEntyIdCd();

	void setEntyIdCd(String entyIdCd);

	/**
	 * Host Variable ENTY-TYP-QLF-CD
	 * 
	 */
	char getEntyTypQlfCd();

	void setEntyTypQlfCd(char entyTypQlfCd);
};
