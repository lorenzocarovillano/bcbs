/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.data.fto;

import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.io.file.FileRecord;
import com.myorg.myprj.copy.DateRecord;

/**Original name: BUSINESS-DATE<br>
 * File: BUSINESS-DATE from program NF0533ML<br>
 * Generated as a class for rule FTO.<br>*/
public class BusinessDateTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: DATE-RECORD
	private DateRecord dateRecord = new DateRecord();

	//==== METHODS ====
	@Override
	public void getData(byte[] destination, int offset) {
		dateRecord.getDateRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		dateRecord.setDateRecordBytes(data, offset);
	}

	public DateRecord getDateRecord() {
		return dateRecord;
	}

	@Override
	public int getLength() {
		return DateRecord.Len.DATE_RECORD;
	}

	@Override
	public IBuffer copy() {
		BusinessDateTO copyTO = new BusinessDateTO();
		copyTO.assign(this);
		return copyTO;
	}
}
