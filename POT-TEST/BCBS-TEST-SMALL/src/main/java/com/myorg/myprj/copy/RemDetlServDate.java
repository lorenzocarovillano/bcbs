/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: REM-DETL-SERV-DATE<br>
 * Variable: REM-DETL-SERV-DATE from copybook NF07RMIT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class RemDetlServDate {

	//==== PROPERTIES ====
	//Original name: REM-DETL-SERV-CC
	private String cc = DefaultValues.stringVal(Len.CC);
	//Original name: REM-DETL-SERV-YY
	private String yy = DefaultValues.stringVal(Len.YY);
	//Original name: REM-DETL-SERV-MM
	private String mm = DefaultValues.stringVal(Len.MM);
	//Original name: REM-DETL-SERV-DD
	private String dd = DefaultValues.stringVal(Len.DD);

	//==== METHODS ====
	public void setDetlServDateBytes(byte[] buffer, int offset) {
		int position = offset;
		cc = MarshalByte.readFixedString(buffer, position, Len.CC);
		position += Len.CC;
		yy = MarshalByte.readFixedString(buffer, position, Len.YY);
		position += Len.YY;
		mm = MarshalByte.readFixedString(buffer, position, Len.MM);
		position += Len.MM;
		dd = MarshalByte.readFixedString(buffer, position, Len.DD);
	}

	public byte[] getDetlServDateBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cc, Len.CC);
		position += Len.CC;
		MarshalByte.writeString(buffer, position, yy, Len.YY);
		position += Len.YY;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position += Len.MM;
		MarshalByte.writeString(buffer, position, dd, Len.DD);
		return buffer;
	}

	public void initDetlServDateSpaces() {
		cc = "";
		yy = "";
		mm = "";
		dd = "";
	}

	public void setCcFormatted(String cc) {
		this.cc = Trunc.toUnsignedNumeric(cc, Len.CC);
	}

	public short getCc() {
		return NumericDisplay.asShort(this.cc);
	}

	public void setYyFormatted(String yy) {
		this.yy = Trunc.toUnsignedNumeric(yy, Len.YY);
	}

	public short getYy() {
		return NumericDisplay.asShort(this.yy);
	}

	public void setMmFormatted(String mm) {
		this.mm = Trunc.toUnsignedNumeric(mm, Len.MM);
	}

	public short getMm() {
		return NumericDisplay.asShort(this.mm);
	}

	public void setDdFormatted(String dd) {
		this.dd = Trunc.toUnsignedNumeric(dd, Len.DD);
	}

	public short getDd() {
		return NumericDisplay.asShort(this.dd);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CC = 2;
		public static final int YY = 2;
		public static final int MM = 2;
		public static final int DD = 2;
		public static final int DETL_SERV_DATE = CC + YY + MM + DD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
