/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import com.myorg.myprj.commons.data.to.IS02800caS02809saS02811saS02813sa;

/**
 * Data Access Object(DAO) for tables [S02800CA, S02809SA, S02811SA, S02813SA]
 * 
 */
public class S02800caS02809saS02811saS02813saDao extends BaseSqlDao<IS02800caS02809saS02811saS02813sa> {

	private final IRowMapper<IS02800caS02809saS02811saS02813sa> selectRecRm = buildNamedRowMapper(IS02800caS02809saS02811saS02813sa.class,
			"clmCntlId", "clmCntlSfxId", "clmPmtId", "pmtAdjTypCd", "clmPdAm", "pmtHistLoadCd", "pmtKcapsTeamNm", "pmtKcapsUseId", "pmtPgmAreaCd",
			"pmtEnrClCd", "pmtFinCd", "pmtNtwrkCd", "cliId", "liChgAm", "liPatRespAm", "liPwoAm", "liAlctOiPdAm", "liDnlRmrkCd", "liExplnCd",
			"c1CalcExplnCd", "c1PdAm", "c1GlAcctId", "c1CorpCd", "c1ProdCd", "c2CalcExplnCd", "c2PdAm", "c2GlAcctId", "c2CorpCd", "c2ProdCd",
			"c3CalcExplnCd", "c3PdAm", "c3GlAcctId", "c3CorpCd", "c3ProdCd", "c4CalcExplnCd", "c4PdAm", "c4GlAcctId", "c4CorpCd", "c4ProdCd",
			"c5CalcExplnCd", "c5PdAm", "c5GlAcctId", "c5CorpCd", "c5ProdCd", "c6CalcExplnCd", "c6PdAm", "c6GlAcctId", "c6CorpCd", "c6ProdCd",
			"c7CalcExplnCd", "c7PdAm", "c7GlAcctId", "c7CorpCd", "c7ProdCd", "c8CalcExplnCd", "c8PdAm", "c8GlAcctId", "c8CorpCd", "c8ProdCd",
			"c9CalcExplnCd", "c9PdAm", "c9GlAcctId", "c9CorpCd", "c9ProdCd", "liNcovAm", "casEobDedAm", "casEobCoinsAm", "casEobCopayAm");

	public S02800caS02809saS02811saS02813saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02800caS02809saS02811saS02813sa> getToClass() {
		return IS02800caS02809saS02811saS02813sa.class;
	}

	public IS02800caS02809saS02811saS02813sa selectRec(short wsLineNum, String db2ClmCntlId, char db2ClmCntlSfxId, short prevClmPmtId,
			IS02800caS02809saS02811saS02813sa iS02800caS02809saS02811saS02813sa) {
		return buildQuery("selectRec").bind("wsLineNum", wsLineNum).bind("db2ClmCntlId", db2ClmCntlId)
				.bind("db2ClmCntlSfxId", String.valueOf(db2ClmCntlSfxId)).bind("prevClmPmtId", prevClmPmtId).rowMapper(selectRecRm)
				.singleResult(iS02800caS02809saS02811saS02813sa);
	}
}
