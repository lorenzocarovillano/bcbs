/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: VOID-S02813-INFO<br>
 * Variable: VOID-S02813-INFO from copybook NF05VOID<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class VoidS02813Info {

	//==== PROPERTIES ====
	//Original name: VOID-CLM-CNTL-ID
	private String clmCntlId = DefaultValues.stringVal(Len.CLM_CNTL_ID);
	//Original name: VOID-CLM-CNTL-SFX-ID
	private char clmCntlSfxId = DefaultValues.CHAR_VAL;
	//Original name: VOID-CLM-PMT-ID
	private short clmPmtId = DefaultValues.SHORT_VAL;
	//Original name: VOID-PMT-ASG-CD
	private String pmtAsgCd = DefaultValues.stringVal(Len.PMT_ASG_CD);
	//Original name: VOID-PMT-ADJ-TYP-CD
	private char pmtAdjTypCd = DefaultValues.CHAR_VAL;
	//Original name: VOID-CLM-PD-AM
	private AfDecimal clmPdAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: VOID-PMT-ELIG-INST-REIMB
	private String pmtEligInstReimb = DefaultValues.stringVal(Len.PMT_ELIG_INST_REIMB);
	//Original name: VOID-PMT-ELIG-PROF-REIMB
	private String pmtEligProfReimb = DefaultValues.stringVal(Len.PMT_ELIG_PROF_REIMB);
	//Original name: VOID-PMT-HIST-LOAD-CD
	private char pmtHistLoadCd = DefaultValues.CHAR_VAL;
	//Original name: VOID-PMT-KCAPS-TEAM-NM
	private String pmtKcapsTeamNm = DefaultValues.stringVal(Len.PMT_KCAPS_TEAM_NM);
	//Original name: VOID-PMT-KCAPS-USE-ID
	private String pmtKcapsUseId = DefaultValues.stringVal(Len.PMT_KCAPS_USE_ID);
	//Original name: VOID-PMT-PGM-AREA-CD
	private String pmtPgmAreaCd = DefaultValues.stringVal(Len.PMT_PGM_AREA_CD);
	//Original name: VOID-PMT-FIN-CD
	private String pmtFinCd = DefaultValues.stringVal(Len.PMT_FIN_CD);
	//Original name: VOID-PMT-ENR-CL-CD
	private String pmtEnrClCd = DefaultValues.stringVal(Len.PMT_ENR_CL_CD);
	//Original name: VOID-PMT-NTWRK-CD
	private String pmtNtwrkCd = DefaultValues.stringVal(Len.PMT_NTWRK_CD);

	//==== METHODS ====
	public void setClmCntlId(String clmCntlId) {
		this.clmCntlId = Functions.subString(clmCntlId, Len.CLM_CNTL_ID);
	}

	public String getClmCntlId() {
		return this.clmCntlId;
	}

	public String getClmCntlIdFormatted() {
		return Functions.padBlanks(getClmCntlId(), Len.CLM_CNTL_ID);
	}

	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.clmCntlSfxId = clmCntlSfxId;
	}

	public char getClmCntlSfxId() {
		return this.clmCntlSfxId;
	}

	public void setClmPmtId(short clmPmtId) {
		this.clmPmtId = clmPmtId;
	}

	public short getClmPmtId() {
		return this.clmPmtId;
	}

	public void setPmtAsgCd(String pmtAsgCd) {
		this.pmtAsgCd = Functions.subString(pmtAsgCd, Len.PMT_ASG_CD);
	}

	public String getPmtAsgCd() {
		return this.pmtAsgCd;
	}

	public void setPmtAdjTypCd(char pmtAdjTypCd) {
		this.pmtAdjTypCd = pmtAdjTypCd;
	}

	public char getPmtAdjTypCd() {
		return this.pmtAdjTypCd;
	}

	public void setClmPdAm(AfDecimal clmPdAm) {
		this.clmPdAm.assign(clmPdAm);
	}

	public AfDecimal getClmPdAm() {
		return this.clmPdAm.copy();
	}

	public void setPmtEligInstReimb(String pmtEligInstReimb) {
		this.pmtEligInstReimb = Functions.subString(pmtEligInstReimb, Len.PMT_ELIG_INST_REIMB);
	}

	public String getPmtEligInstReimb() {
		return this.pmtEligInstReimb;
	}

	public void setPmtEligProfReimb(String pmtEligProfReimb) {
		this.pmtEligProfReimb = Functions.subString(pmtEligProfReimb, Len.PMT_ELIG_PROF_REIMB);
	}

	public String getPmtEligProfReimb() {
		return this.pmtEligProfReimb;
	}

	public void setPmtHistLoadCd(char pmtHistLoadCd) {
		this.pmtHistLoadCd = pmtHistLoadCd;
	}

	public char getPmtHistLoadCd() {
		return this.pmtHistLoadCd;
	}

	public void setPmtKcapsTeamNm(String pmtKcapsTeamNm) {
		this.pmtKcapsTeamNm = Functions.subString(pmtKcapsTeamNm, Len.PMT_KCAPS_TEAM_NM);
	}

	public String getPmtKcapsTeamNm() {
		return this.pmtKcapsTeamNm;
	}

	public void setPmtKcapsUseId(String pmtKcapsUseId) {
		this.pmtKcapsUseId = Functions.subString(pmtKcapsUseId, Len.PMT_KCAPS_USE_ID);
	}

	public String getPmtKcapsUseId() {
		return this.pmtKcapsUseId;
	}

	public void setPmtPgmAreaCd(String pmtPgmAreaCd) {
		this.pmtPgmAreaCd = Functions.subString(pmtPgmAreaCd, Len.PMT_PGM_AREA_CD);
	}

	public String getPmtPgmAreaCd() {
		return this.pmtPgmAreaCd;
	}

	public void setPmtFinCd(String pmtFinCd) {
		this.pmtFinCd = Functions.subString(pmtFinCd, Len.PMT_FIN_CD);
	}

	public String getPmtFinCd() {
		return this.pmtFinCd;
	}

	public void setPmtEnrClCd(String pmtEnrClCd) {
		this.pmtEnrClCd = Functions.subString(pmtEnrClCd, Len.PMT_ENR_CL_CD);
	}

	public String getPmtEnrClCd() {
		return this.pmtEnrClCd;
	}

	public void setPmtNtwrkCd(String pmtNtwrkCd) {
		this.pmtNtwrkCd = Functions.subString(pmtNtwrkCd, Len.PMT_NTWRK_CD);
	}

	public String getPmtNtwrkCd() {
		return this.pmtNtwrkCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_CNTL_ID = 12;
		public static final int PMT_KCAPS_TEAM_NM = 5;
		public static final int PMT_KCAPS_USE_ID = 3;
		public static final int PMT_PGM_AREA_CD = 3;
		public static final int PMT_FIN_CD = 3;
		public static final int PMT_ENR_CL_CD = 3;
		public static final int PMT_NTWRK_CD = 3;
		public static final int PMT_ASG_CD = 2;
		public static final int PMT_ELIG_INST_REIMB = 2;
		public static final int PMT_ELIG_PROF_REIMB = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
