/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WORK-DATE<br>
 * Variable: WORK-DATE from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkDate {

	//==== PROPERTIES ====
	//Original name: WORK-CENTURY
	private String century = DefaultValues.stringVal(Len.CENTURY);
	//Original name: WORK-YEAR
	private String year = DefaultValues.stringVal(Len.YEAR);
	//Original name: FILLER-WORK-DATE
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: WORK-MONTH
	private String month = DefaultValues.stringVal(Len.MONTH);
	//Original name: FILLER-WORK-DATE-1
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: WORK-DAY
	private String day = DefaultValues.stringVal(Len.DAY);

	//==== METHODS ====
	public void setWorkDateFormatted(String data) {
		byte[] buffer = new byte[Len.WORK_DATE];
		MarshalByte.writeString(buffer, 1, data, Len.WORK_DATE);
		setWorkDateBytes(buffer, 1);
	}

	public void setWorkDateBytes(byte[] buffer, int offset) {
		int position = offset;
		century = MarshalByte.readFixedString(buffer, position, Len.CENTURY);
		position += Len.CENTURY;
		year = MarshalByte.readFixedString(buffer, position, Len.YEAR);
		position += Len.YEAR;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		month = MarshalByte.readFixedString(buffer, position, Len.MONTH);
		position += Len.MONTH;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		day = MarshalByte.readFixedString(buffer, position, Len.DAY);
	}

	public String getCenturyFormatted() {
		return this.century;
	}

	public String getYearFormatted() {
		return this.year;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getMonthFormatted() {
		return this.month;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public String getDayFormatted() {
		return this.day;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CENTURY = 2;
		public static final int YEAR = 2;
		public static final int MONTH = 2;
		public static final int DAY = 2;
		public static final int FLR1 = 1;
		public static final int WORK_DATE = CENTURY + YEAR + MONTH + DAY + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
