/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.myorg.myprj.ws.redefines.Nf05LineLevelRecord;

/**Original name: NF05-EOB-RECORD<br>
 * Variable: NF05-EOB-RECORD from copybook NF05EOB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Nf05EobRecord {

	//==== PROPERTIES ====
	//Original name: NF05-HEADER-PART-OF-RECORD
	private Nf05HeaderPartOfRecord nf05HeaderPartOfRecord = new Nf05HeaderPartOfRecord();
	//Original name: NF05-LINE-LEVEL-RECORD
	private Nf05LineLevelRecord nf05LineLevelRecord = new Nf05LineLevelRecord();

	//==== METHODS ====
	public String getNf05EobRecordFormatted() {
		return MarshalByteExt.bufferToStr(getNf05EobRecordBytes());
	}

	public byte[] getNf05EobRecordBytes() {
		byte[] buffer = new byte[Len.NF05_EOB_RECORD];
		return getNf05EobRecordBytes(buffer, 1);
	}

	public byte[] getNf05EobRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		nf05HeaderPartOfRecord.getNf05HeaderPartOfRecordBytes(buffer, position);
		position += Nf05HeaderPartOfRecord.Len.NF05_HEADER_PART_OF_RECORD;
		nf05LineLevelRecord.getNf05TotalRecordBytes(buffer, position);
		return buffer;
	}

	public Nf05HeaderPartOfRecord getNf05HeaderPartOfRecord() {
		return nf05HeaderPartOfRecord;
	}

	public Nf05LineLevelRecord getNf05LineLevelRecord() {
		return nf05LineLevelRecord;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NF05_EOB_RECORD = Nf05HeaderPartOfRecord.Len.NF05_HEADER_PART_OF_RECORD + Nf05LineLevelRecord.Len.NF05_TOTAL_RECORD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
