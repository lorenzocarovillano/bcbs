/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-HOLD-NOTE-AREA<br>
 * Variable: WS-HOLD-NOTE-AREA from program NF0533ML<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class WsHoldNoteArea {

	//==== PROPERTIES ====
	//Original name: WS-NOTE-1
	private String note1 = DefaultValues.stringVal(Len.NOTE1);
	//Original name: WS-WHICH-NOTES-TABLE-1
	private String whichNotesTable1 = DefaultValues.stringVal(Len.WHICH_NOTES_TABLE1);
	//Original name: WS-NOTE-2
	private String note2 = DefaultValues.stringVal(Len.NOTE2);
	//Original name: WS-WHICH-NOTES-TABLE-2
	private String whichNotesTable2 = DefaultValues.stringVal(Len.WHICH_NOTES_TABLE2);
	//Original name: WS-NOTE-3
	private String note3 = DefaultValues.stringVal(Len.NOTE3);
	//Original name: WS-WHICH-NOTES-TABLE-3
	private String whichNotesTable3 = DefaultValues.stringVal(Len.WHICH_NOTES_TABLE3);
	//Original name: WS-NOTE-4
	private String note4 = DefaultValues.stringVal(Len.NOTE4);
	//Original name: WS-WHICH-NOTES-TABLE-4
	private String whichNotesTable4 = DefaultValues.stringVal(Len.WHICH_NOTES_TABLE4);
	//Original name: WS-NOTE-5
	private String note5 = DefaultValues.stringVal(Len.NOTE5);
	//Original name: WS-WHICH-NOTES-TABLE-5
	private String whichNotesTable5 = DefaultValues.stringVal(Len.WHICH_NOTES_TABLE5);

	//==== METHODS ====
	public void setNote1(String note1) {
		this.note1 = Functions.subString(note1, Len.NOTE1);
	}

	public String getNote1() {
		return this.note1;
	}

	public void setWhichNotesTable1(String whichNotesTable1) {
		this.whichNotesTable1 = Functions.subString(whichNotesTable1, Len.WHICH_NOTES_TABLE1);
	}

	public String getWhichNotesTable1() {
		return this.whichNotesTable1;
	}

	public void setNote2(String note2) {
		this.note2 = Functions.subString(note2, Len.NOTE2);
	}

	public String getNote2() {
		return this.note2;
	}

	public void setWhichNotesTable2(String whichNotesTable2) {
		this.whichNotesTable2 = Functions.subString(whichNotesTable2, Len.WHICH_NOTES_TABLE2);
	}

	public String getWhichNotesTable2() {
		return this.whichNotesTable2;
	}

	public void setNote3(String note3) {
		this.note3 = Functions.subString(note3, Len.NOTE3);
	}

	public String getNote3() {
		return this.note3;
	}

	public void setWhichNotesTable3(String whichNotesTable3) {
		this.whichNotesTable3 = Functions.subString(whichNotesTable3, Len.WHICH_NOTES_TABLE3);
	}

	public String getWhichNotesTable3() {
		return this.whichNotesTable3;
	}

	public void setNote4(String note4) {
		this.note4 = Functions.subString(note4, Len.NOTE4);
	}

	public String getNote4() {
		return this.note4;
	}

	public void setWhichNotesTable4(String whichNotesTable4) {
		this.whichNotesTable4 = Functions.subString(whichNotesTable4, Len.WHICH_NOTES_TABLE4);
	}

	public String getWhichNotesTable4() {
		return this.whichNotesTable4;
	}

	public void setNote5(String note5) {
		this.note5 = Functions.subString(note5, Len.NOTE5);
	}

	public String getNote5() {
		return this.note5;
	}

	public void setWhichNotesTable5(String whichNotesTable5) {
		this.whichNotesTable5 = Functions.subString(whichNotesTable5, Len.WHICH_NOTES_TABLE5);
	}

	public String getWhichNotesTable5() {
		return this.whichNotesTable5;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NOTE1 = 2;
		public static final int WHICH_NOTES_TABLE1 = 16;
		public static final int NOTE2 = 2;
		public static final int WHICH_NOTES_TABLE2 = 16;
		public static final int NOTE3 = 2;
		public static final int WHICH_NOTES_TABLE3 = 16;
		public static final int NOTE4 = 2;
		public static final int WHICH_NOTES_TABLE4 = 16;
		public static final int NOTE5 = 2;
		public static final int WHICH_NOTES_TABLE5 = 16;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
