/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [S02800CA]
 * 
 */
public interface IS02800ca extends BaseSqlTo {

	/**
	 * Host Variable DB2-CAS-NCOV-AM
	 * 
	 */
	AfDecimal getNcovAm();

	void setNcovAm(AfDecimal ncovAm);

	/**
	 * Host Variable DB2-CAS-EOB-DED-AM
	 * 
	 */
	AfDecimal getEobDedAm();

	void setEobDedAm(AfDecimal eobDedAm);

	/**
	 * Host Variable DB2-CAS-EOB-COINS-AM
	 * 
	 */
	AfDecimal getEobCoinsAm();

	void setEobCoinsAm(AfDecimal eobCoinsAm);

	/**
	 * Host Variable DB2-CAS-EOB-COPAY-AM
	 * 
	 */
	AfDecimal getEobCopayAm();

	void setEobCopayAm(AfDecimal eobCopayAm);

	/**
	 * Host Variable DB2-CAS-EOB-PENALTY-AM
	 * 
	 */
	AfDecimal getEobPenaltyAm();

	void setEobPenaltyAm(AfDecimal eobPenaltyAm);
};
