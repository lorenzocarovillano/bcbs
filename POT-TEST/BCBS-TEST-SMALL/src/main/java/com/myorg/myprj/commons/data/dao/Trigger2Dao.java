/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.ITrigger2;

/**
 * Data Access Object(DAO) for table [TRIGGER2]
 * 
 */
public class Trigger2Dao extends BaseSqlDao<ITrigger2> {

	public Trigger2Dao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<ITrigger2> getToClass() {
		return ITrigger2.class;
	}

	public DbAccessStatus insertRec(ITrigger2 iTrigger2) {
		return buildQuery("insertRec").bind(iTrigger2).executeInsert();
	}
}
