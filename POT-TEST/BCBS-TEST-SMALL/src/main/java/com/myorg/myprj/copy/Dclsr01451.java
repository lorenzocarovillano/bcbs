/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.commons.data.to.ISr01451;

/**Original name: DCLSR01451<br>
 * Variable: DCLSR01451 from copybook SR01451<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dclsr01451 implements ISr01451 {

	//==== PROPERTIES ====
	//Original name: PROV-ID
	private String provId = DefaultValues.stringVal(Len.PROV_ID);
	//Original name: PROV-LOB-CD
	private char provLobCd = DefaultValues.CHAR_VAL;
	//Original name: PROV-CN-ARNG-EF-DT
	private String provCnArngEfDt = DefaultValues.stringVal(Len.PROV_CN_ARNG_EF_DT);
	//Original name: PRV-CN-ARNG-TRM-DT
	private String prvCnArngTrmDt = DefaultValues.stringVal(Len.PRV_CN_ARNG_TRM_DT);

	//==== METHODS ====
	public void setProvId(String provId) {
		this.provId = Functions.subString(provId, Len.PROV_ID);
	}

	public String getProvId() {
		return this.provId;
	}

	public void setProvLobCd(char provLobCd) {
		this.provLobCd = provLobCd;
	}

	public char getProvLobCd() {
		return this.provLobCd;
	}

	public void setProvCnArngEfDt(String provCnArngEfDt) {
		this.provCnArngEfDt = Functions.subString(provCnArngEfDt, Len.PROV_CN_ARNG_EF_DT);
	}

	public String getProvCnArngEfDt() {
		return this.provCnArngEfDt;
	}

	public void setPrvCnArngTrmDt(String prvCnArngTrmDt) {
		this.prvCnArngTrmDt = Functions.subString(prvCnArngTrmDt, Len.PRV_CN_ARNG_TRM_DT);
	}

	public String getPrvCnArngTrmDt() {
		return this.prvCnArngTrmDt;
	}

	@Override
	public String getOvCnArngEfDt() {
		return getProvCnArngEfDt();
	}

	@Override
	public void setOvCnArngEfDt(String ovCnArngEfDt) {
		this.setProvCnArngEfDt(ovCnArngEfDt);
	}

	@Override
	public String getOvId() {
		return getProvId();
	}

	@Override
	public void setOvId(String ovId) {
		this.setProvId(ovId);
	}

	@Override
	public char getOvLobCd() {
		return getProvLobCd();
	}

	@Override
	public void setOvLobCd(char ovLobCd) {
		this.setProvLobCd(ovLobCd);
	}

	@Override
	public String getvCnArngTrmDt() {
		return getPrvCnArngTrmDt();
	}

	@Override
	public void setvCnArngTrmDt(String vCnArngTrmDt) {
		this.setPrvCnArngTrmDt(vCnArngTrmDt);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROV_ID = 10;
		public static final int PROV_CN_ARNG_EF_DT = 10;
		public static final int PRV_CN_ARNG_TRM_DT = 10;
		public static final int PROV_CN_ARNG_CD = 5;
		public static final int PROV_STOP_PAY_CD = 2;
		public static final int CLM_CNTY_CD = 3;
		public static final int NTWRK_CD = 3;
		public static final int PGM_AREA_CD = 3;
		public static final int PROV_PAYEE_NM = 30;
		public static final int PROV_RISK_POOL_CD = 3;
		public static final int PROV_LCT_CD = 3;
		public static final int FED_RSRV_TRNST_ID = 11;
		public static final int FIN_INST_ACCT_ID = 17;
		public static final int BCKUP_WTHLD_EFF_DT = 10;
		public static final int BCKUP_WTHLD_CNC_DT = 10;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_TS = 26;
		public static final int CSRN_CD = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
