/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: CSR-PRMPT-PAY-OVRD<br>
 * Variable: CSR-PRMPT-PAY-OVRD from copybook NF07AREA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CsrPrmptPayOvrd {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char PROCESSOR = 'P';
	public static final char ONLINE = 'O';
	public static final char BATCH = 'B';
	public static final char CONVERSION = 'C';

	//==== METHODS ====
	public void setPrmptPayOvrd(char prmptPayOvrd) {
		this.value = prmptPayOvrd;
	}

	public char getPrmptPayOvrd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
