/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

/**Original name: PROGRAM-SUBSCRIPTS<br>
 * Variable: PROGRAM-SUBSCRIPTS from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ProgramSubscripts {

	//==== PROPERTIES ====
	//Original name: CAS-SUBSCRIPTS
	private CasSubscripts casSubscripts = new CasSubscripts();
	//Original name: X-SUB
	private short xSub = ((short) 0);
	//Original name: CALC-SUB
	private short calcSub = ((short) 0);
	//Original name: VOID-SUB
	private short voidSub = ((short) 0);

	//==== METHODS ====
	public void setxSub(short xSub) {
		this.xSub = xSub;
	}

	public short getxSub() {
		return this.xSub;
	}

	public void setCalcSub(short calcSub) {
		this.calcSub = calcSub;
	}

	public short getCalcSub() {
		return this.calcSub;
	}

	public void setVoidSub(short voidSub) {
		this.voidSub = voidSub;
	}

	public short getVoidSub() {
		return this.voidSub;
	}

	public CasSubscripts getCasSubscripts() {
		return casSubscripts;
	}
}
