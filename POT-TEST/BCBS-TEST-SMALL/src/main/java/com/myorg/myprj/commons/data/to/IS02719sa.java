/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [S02719SA]
 * 
 */
public interface IS02719sa extends BaseSqlTo {

	/**
	 * Host Variable PROD-SRV-ID
	 * 
	 */
	String getProdSrvId();

	void setProdSrvId(String prodSrvId);

	/**
	 * Host Variable PROD-SRV-ID-QLF-CD
	 * 
	 */
	String getProdSrvIdQlfCd();

	void setProdSrvIdQlfCd(String prodSrvIdQlfCd);

	/**
	 * Host Variable SV3013-PROC-MOD-CD
	 * 
	 */
	String getSv3013ProcModCd();

	void setSv3013ProcModCd(String sv3013ProcModCd);

	/**
	 * Host Variable SV3014-PROC-MOD-CD
	 * 
	 */
	String getSv3014ProcModCd();

	void setSv3014ProcModCd(String sv3014ProcModCd);

	/**
	 * Host Variable SV3015-PROC-MOD-CD
	 * 
	 */
	String getSv3015ProcModCd();

	void setSv3015ProcModCd(String sv3015ProcModCd);

	/**
	 * Host Variable SV3016-PROC-MOD-CD
	 * 
	 */
	String getSv3016ProcModCd();

	void setSv3016ProcModCd(String sv3016ProcModCd);

	/**
	 * Host Variable MNTRY-AM
	 * 
	 */
	AfDecimal getMntryAm();

	void setMntryAm(AfDecimal mntryAm);

	/**
	 * Host Variable QNTY-CT
	 * 
	 */
	AfDecimal getQntyCt();

	void setQntyCt(AfDecimal qntyCt);
};
