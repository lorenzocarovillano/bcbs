/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-BUSINESS-DATE<br>
 * Variable: WS-BUSINESS-DATE from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsBusinessDate {

	//==== PROPERTIES ====
	//Original name: WS-BUSINESS-CCYY
	private String ccyy = DefaultValues.stringVal(Len.CCYY);
	//Original name: FILLER-WS-BUSINESS-DATE
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: WS-BUSINESS-MM
	private String mm = DefaultValues.stringVal(Len.MM);
	//Original name: FILLER-WS-BUSINESS-DATE-1
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: WS-BUSINESS-DD
	private String dd = DefaultValues.stringVal(Len.DD);

	//==== METHODS ====
	public void setWsBusinessDateFormatted(String data) {
		byte[] buffer = new byte[Len.NEXT_BUSINESS_DATE];
		MarshalByte.writeString(buffer, 1, data, Len.NEXT_BUSINESS_DATE);
		setNextBusinessDateBytes(buffer, 1);
	}

	public String getWsBusinessDateFormatted() {
		return MarshalByteExt.bufferToStr(getWsBusinessDateBytes());
	}

	public byte[] getWsBusinessDateBytes() {
		byte[] buffer = new byte[Len.NEXT_BUSINESS_DATE];
		return getNextBusinessDateBytes(buffer, 1);
	}

	public void setNextBusinessDateBytes(byte[] buffer, int offset) {
		int position = offset;
		ccyy = MarshalByte.readString(buffer, position, Len.CCYY);
		position += Len.CCYY;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mm = MarshalByte.readString(buffer, position, Len.MM);
		position += Len.MM;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dd = MarshalByte.readString(buffer, position, Len.DD);
	}

	public byte[] getNextBusinessDateBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ccyy, Len.CCYY);
		position += Len.CCYY;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position += Len.MM;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dd, Len.DD);
		return buffer;
	}

	public void setYear(String year) {
		this.ccyy = Functions.subString(year, Len.CCYY);
	}

	public String getYear() {
		return this.ccyy;
	}

	public void setDash1(char dash1) {
		this.flr1 = dash1;
	}

	public char getDash1() {
		return this.flr1;
	}

	public void setMonth(String month) {
		this.mm = Functions.subString(month, Len.MM);
	}

	public String getMonth() {
		return this.mm;
	}

	public void setDash2(char dash2) {
		this.flr2 = dash2;
	}

	public char getDash2() {
		return this.flr2;
	}

	public void setDay(String day) {
		this.dd = Functions.subString(day, Len.DD);
	}

	public String getDay() {
		return this.dd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CCYY = 4;
		public static final int FLR1 = 1;
		public static final int MM = 2;
		public static final int FLR2 = 1;
		public static final int DD = 2;
		public static final int NEXT_BUSINESS_DATE = CCYY + FLR1 + MM + FLR2 + DD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
