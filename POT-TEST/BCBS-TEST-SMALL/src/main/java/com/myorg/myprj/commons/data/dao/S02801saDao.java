/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS02801sa;

/**
 * Data Access Object(DAO) for table [S02801SA]
 * 
 */
public class S02801saDao extends BaseSqlDao<IS02801sa> {

	public S02801saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02801sa> getToClass() {
		return IS02801sa.class;
	}

	public DbAccessStatus insertRec(IS02801sa iS02801sa) {
		return buildQuery("insertRec").bind(iS02801sa).executeInsert();
	}
}
