/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: PREVIOUS-DATE-FLAG<br>
 * Variable: PREVIOUS-DATE-FLAG from copybook NN02DATE<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class PreviousDateFlag {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.VALUE);
	public static final String FIRST_OF_MONTH = "FOM";
	public static final String END_OF_MONTH = "EOM";

	//==== METHODS ====
	public void setPreviousDateFlag(String previousDateFlag) {
		this.value = Functions.subString(previousDateFlag, Len.VALUE);
	}

	public String getPreviousDateFlag() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
