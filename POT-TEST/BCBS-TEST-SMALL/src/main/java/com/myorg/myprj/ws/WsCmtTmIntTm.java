/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WS-CMT-TM-INT-TM<br>
 * Variable: WS-CMT-TM-INT-TM from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsCmtTmIntTm {

	//==== PROPERTIES ====
	//Original name: WS-CMT-TM-INT-TM-HH
	private String hh = "00";
	//Original name: FILLER-WS-CMT-TM-INT-TM
	private char flr1 = Types.SPACE_CHAR;
	//Original name: WS-CMT-TM-INT-TM-MM
	private String mm = "00";
	//Original name: FILLER-WS-CMT-TM-INT-TM-1
	private char flr2 = Types.SPACE_CHAR;
	//Original name: WS-CMT-TM-INT-TM-SS
	private String ss = "00";

	//==== METHODS ====
	public void setWsCmtTmIntTmFormatted(String data) {
		byte[] buffer = new byte[Len.WS_CMT_TM_INT_TM];
		MarshalByte.writeString(buffer, 1, data, Len.WS_CMT_TM_INT_TM);
		setWsCmtTmIntTmBytes(buffer, 1);
	}

	public void setWsCmtTmIntTmBytes(byte[] buffer, int offset) {
		int position = offset;
		hh = MarshalByte.readFixedString(buffer, position, Len.HH);
		position += Len.HH;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mm = MarshalByte.readFixedString(buffer, position, Len.MM);
		position += Len.MM;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ss = MarshalByte.readFixedString(buffer, position, Len.SS);
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public String getSsFormatted() {
		return this.ss;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HH = 2;
		public static final int MM = 2;
		public static final int SS = 2;
		public static final int FLR1 = 1;
		public static final int WS_CMT_TM_INT_TM = HH + MM + SS + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
