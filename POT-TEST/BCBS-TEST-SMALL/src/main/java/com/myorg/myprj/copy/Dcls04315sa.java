/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.commons.data.to.IS02813saS04315sa;
import com.myorg.myprj.commons.data.to.IS02813saS04315saS04316sa;

/**Original name: DCLS04315SA<br>
 * Variable: DCLS04315SA from copybook S04315SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls04315sa implements IS02813saS04315sa, IS02813saS04315saS04316sa {

	//==== PROPERTIES ====
	//Original name: CLM-CNTL-ID
	private String clmCntlId = DefaultValues.stringVal(Len.CLM_CNTL_ID);
	//Original name: CLM-CNTL-SFX-ID
	private char clmCntlSfxId = DefaultValues.CHAR_VAL;
	//Original name: CLM-PMT-ID
	private short clmPmtId = DefaultValues.SHORT_VAL;
	//Original name: CRS-RF-CLM-CNTL-ID
	private String crsRfClmCntlId = DefaultValues.stringVal(Len.CRS_RF_CLM_CNTL_ID);
	//Original name: CS-RF-CLM-CL-SX-ID
	private char csRfClmClSxId = DefaultValues.CHAR_VAL;
	//Original name: CRS-REF-CLM-PMT-ID
	private short crsRefClmPmtId = DefaultValues.SHORT_VAL;
	//Original name: CRS-REF-CLI-ID
	private short crsRefCliId = DefaultValues.SHORT_VAL;
	//Original name: CRS-REF-RSN-CD
	private char crsRefRsnCd = DefaultValues.CHAR_VAL;
	//Original name: INIT-CLM-TS
	private String initClmTs = DefaultValues.stringVal(Len.INIT_CLM_TS);
	//Original name: DNL-RMRK-CD
	private String dnlRmrkCd = DefaultValues.stringVal(Len.DNL_RMRK_CD);
	//Original name: ADJD-PROC-CD
	private String adjdProcCd = DefaultValues.stringVal(Len.ADJD_PROC_CD);
	//Original name: ADJD-PROC-MOD1-CD
	private String adjdProcMod1Cd = DefaultValues.stringVal(Len.ADJD_PROC_MOD1_CD);
	//Original name: ADJD-PROC-MOD2-CD
	private String adjdProcMod2Cd = DefaultValues.stringVal(Len.ADJD_PROC_MOD2_CD);
	//Original name: ADJD-PROC-MOD3-CD
	private String adjdProcMod3Cd = DefaultValues.stringVal(Len.ADJD_PROC_MOD3_CD);
	//Original name: ADJD-PROC-MOD4-CD
	private String adjdProcMod4Cd = DefaultValues.stringVal(Len.ADJD_PROC_MOD4_CD);
	//Original name: INIT-CLM-CNTL-ID
	private String initClmCntlId = DefaultValues.stringVal(Len.INIT_CLM_CNTL_ID);
	//Original name: INT-CLM-CNL-SFX-ID
	private char intClmCnlSfxId = DefaultValues.CHAR_VAL;
	//Original name: INIT-CLM-PMT-ID
	private short initClmPmtId = DefaultValues.SHORT_VAL;
	//Original name: ACT-INACT-CD
	private char actInactCd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setClmCntlId(String clmCntlId) {
		this.clmCntlId = Functions.subString(clmCntlId, Len.CLM_CNTL_ID);
	}

	public String getClmCntlId() {
		return this.clmCntlId;
	}

	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.clmCntlSfxId = clmCntlSfxId;
	}

	public char getClmCntlSfxId() {
		return this.clmCntlSfxId;
	}

	public void setClmPmtId(short clmPmtId) {
		this.clmPmtId = clmPmtId;
	}

	public short getClmPmtId() {
		return this.clmPmtId;
	}

	public void setCrsRfClmCntlId(String crsRfClmCntlId) {
		this.crsRfClmCntlId = Functions.subString(crsRfClmCntlId, Len.CRS_RF_CLM_CNTL_ID);
	}

	public String getCrsRfClmCntlId() {
		return this.crsRfClmCntlId;
	}

	public void setCsRfClmClSxId(char csRfClmClSxId) {
		this.csRfClmClSxId = csRfClmClSxId;
	}

	public char getCsRfClmClSxId() {
		return this.csRfClmClSxId;
	}

	public void setCrsRefClmPmtId(short crsRefClmPmtId) {
		this.crsRefClmPmtId = crsRefClmPmtId;
	}

	public short getCrsRefClmPmtId() {
		return this.crsRefClmPmtId;
	}

	public void setCrsRefCliId(short crsRefCliId) {
		this.crsRefCliId = crsRefCliId;
	}

	public short getCrsRefCliId() {
		return this.crsRefCliId;
	}

	public void setCrsRefRsnCd(char crsRefRsnCd) {
		this.crsRefRsnCd = crsRefRsnCd;
	}

	public char getCrsRefRsnCd() {
		return this.crsRefRsnCd;
	}

	public void setInitClmTs(String initClmTs) {
		this.initClmTs = Functions.subString(initClmTs, Len.INIT_CLM_TS);
	}

	public String getInitClmTs() {
		return this.initClmTs;
	}

	public void setDnlRmrkCd(String dnlRmrkCd) {
		this.dnlRmrkCd = Functions.subString(dnlRmrkCd, Len.DNL_RMRK_CD);
	}

	public String getDnlRmrkCd() {
		return this.dnlRmrkCd;
	}

	public void setAdjdProcCd(String adjdProcCd) {
		this.adjdProcCd = Functions.subString(adjdProcCd, Len.ADJD_PROC_CD);
	}

	public String getAdjdProcCd() {
		return this.adjdProcCd;
	}

	public void setAdjdProcMod1Cd(String adjdProcMod1Cd) {
		this.adjdProcMod1Cd = Functions.subString(adjdProcMod1Cd, Len.ADJD_PROC_MOD1_CD);
	}

	public String getAdjdProcMod1Cd() {
		return this.adjdProcMod1Cd;
	}

	public void setAdjdProcMod2Cd(String adjdProcMod2Cd) {
		this.adjdProcMod2Cd = Functions.subString(adjdProcMod2Cd, Len.ADJD_PROC_MOD2_CD);
	}

	public String getAdjdProcMod2Cd() {
		return this.adjdProcMod2Cd;
	}

	public void setAdjdProcMod3Cd(String adjdProcMod3Cd) {
		this.adjdProcMod3Cd = Functions.subString(adjdProcMod3Cd, Len.ADJD_PROC_MOD3_CD);
	}

	public String getAdjdProcMod3Cd() {
		return this.adjdProcMod3Cd;
	}

	public void setAdjdProcMod4Cd(String adjdProcMod4Cd) {
		this.adjdProcMod4Cd = Functions.subString(adjdProcMod4Cd, Len.ADJD_PROC_MOD4_CD);
	}

	public String getAdjdProcMod4Cd() {
		return this.adjdProcMod4Cd;
	}

	public void setInitClmCntlId(String initClmCntlId) {
		this.initClmCntlId = Functions.subString(initClmCntlId, Len.INIT_CLM_CNTL_ID);
	}

	public String getInitClmCntlId() {
		return this.initClmCntlId;
	}

	public void setIntClmCnlSfxId(char intClmCnlSfxId) {
		this.intClmCnlSfxId = intClmCnlSfxId;
	}

	public char getIntClmCnlSfxId() {
		return this.intClmCnlSfxId;
	}

	public void setInitClmPmtId(short initClmPmtId) {
		this.initClmPmtId = initClmPmtId;
	}

	public short getInitClmPmtId() {
		return this.initClmPmtId;
	}

	public void setActInactCd(char actInactCd) {
		this.actInactCd = actInactCd;
	}

	public char getActInactCd() {
		return this.actInactCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_CNTL_ID = 12;
		public static final int CRS_RF_CLM_CNTL_ID = 12;
		public static final int INIT_CLM_TS = 26;
		public static final int DNL_RMRK_CD = 3;
		public static final int ADJD_PROC_CD = 5;
		public static final int ADJD_PROC_MOD1_CD = 2;
		public static final int ADJD_PROC_MOD2_CD = 2;
		public static final int ADJD_PROC_MOD3_CD = 2;
		public static final int ADJD_PROC_MOD4_CD = 2;
		public static final int INIT_CLM_CNTL_ID = 12;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_TS = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
