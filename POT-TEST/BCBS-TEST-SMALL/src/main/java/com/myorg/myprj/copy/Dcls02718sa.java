/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.commons.data.to.IS02718sa;

/**Original name: DCLS02718SA<br>
 * Variable: DCLS02718SA from copybook S02718SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02718sa implements IS02718sa {

	//==== PROPERTIES ====
	//Original name: SV201-PROD-SRV-ID
	private String sv201ProdSrvId = DefaultValues.stringVal(Len.SV201_PROD_SRV_ID);
	//Original name: PROD-SRV-ID-QLF-CD
	private String prodSrvIdQlfCd = DefaultValues.stringVal(Len.PROD_SRV_ID_QLF_CD);
	//Original name: SV2022-PROD-SRV-ID
	private String sv2022ProdSrvId = DefaultValues.stringVal(Len.SV2022_PROD_SRV_ID);
	//Original name: SV2023-PROC-MOD-CD
	private String sv2023ProcModCd = DefaultValues.stringVal(Len.SV2023_PROC_MOD_CD);
	//Original name: SV2024-PROC-MOD-CD
	private String sv2024ProcModCd = DefaultValues.stringVal(Len.SV2024_PROC_MOD_CD);
	//Original name: SV2025-PROC-MOD-CD
	private String sv2025ProcModCd = DefaultValues.stringVal(Len.SV2025_PROC_MOD_CD);
	//Original name: SV2026-PROC-MOD-CD
	private String sv2026ProcModCd = DefaultValues.stringVal(Len.SV2026_PROC_MOD_CD);
	//Original name: SV203-MNTRY-AM
	private AfDecimal sv203MntryAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: QNTY-CT
	private AfDecimal qntyCt = new AfDecimal(DefaultValues.DEC_VAL, 7, 2);

	//==== METHODS ====
	@Override
	public void setSv201ProdSrvId(String sv201ProdSrvId) {
		this.sv201ProdSrvId = Functions.subString(sv201ProdSrvId, Len.SV201_PROD_SRV_ID);
	}

	@Override
	public String getSv201ProdSrvId() {
		return this.sv201ProdSrvId;
	}

	@Override
	public void setProdSrvIdQlfCd(String prodSrvIdQlfCd) {
		this.prodSrvIdQlfCd = Functions.subString(prodSrvIdQlfCd, Len.PROD_SRV_ID_QLF_CD);
	}

	@Override
	public String getProdSrvIdQlfCd() {
		return this.prodSrvIdQlfCd;
	}

	@Override
	public void setSv2022ProdSrvId(String sv2022ProdSrvId) {
		this.sv2022ProdSrvId = Functions.subString(sv2022ProdSrvId, Len.SV2022_PROD_SRV_ID);
	}

	@Override
	public String getSv2022ProdSrvId() {
		return this.sv2022ProdSrvId;
	}

	@Override
	public void setSv2023ProcModCd(String sv2023ProcModCd) {
		this.sv2023ProcModCd = Functions.subString(sv2023ProcModCd, Len.SV2023_PROC_MOD_CD);
	}

	@Override
	public String getSv2023ProcModCd() {
		return this.sv2023ProcModCd;
	}

	@Override
	public void setSv2024ProcModCd(String sv2024ProcModCd) {
		this.sv2024ProcModCd = Functions.subString(sv2024ProcModCd, Len.SV2024_PROC_MOD_CD);
	}

	@Override
	public String getSv2024ProcModCd() {
		return this.sv2024ProcModCd;
	}

	@Override
	public void setSv2025ProcModCd(String sv2025ProcModCd) {
		this.sv2025ProcModCd = Functions.subString(sv2025ProcModCd, Len.SV2025_PROC_MOD_CD);
	}

	@Override
	public String getSv2025ProcModCd() {
		return this.sv2025ProcModCd;
	}

	@Override
	public void setSv2026ProcModCd(String sv2026ProcModCd) {
		this.sv2026ProcModCd = Functions.subString(sv2026ProcModCd, Len.SV2026_PROC_MOD_CD);
	}

	@Override
	public String getSv2026ProcModCd() {
		return this.sv2026ProcModCd;
	}

	@Override
	public void setSv203MntryAm(AfDecimal sv203MntryAm) {
		this.sv203MntryAm.assign(sv203MntryAm);
	}

	@Override
	public AfDecimal getSv203MntryAm() {
		return this.sv203MntryAm.copy();
	}

	@Override
	public void setQntyCt(AfDecimal qntyCt) {
		this.qntyCt.assign(qntyCt);
	}

	@Override
	public AfDecimal getQntyCt() {
		return this.qntyCt.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SV201_PROD_SRV_ID = 48;
		public static final int PROD_SRV_ID_QLF_CD = 2;
		public static final int SV2022_PROD_SRV_ID = 48;
		public static final int SV2023_PROC_MOD_CD = 2;
		public static final int SV2024_PROC_MOD_CD = 2;
		public static final int SV2025_PROC_MOD_CD = 2;
		public static final int SV2026_PROC_MOD_CD = 2;
		public static final int CLM_CNTL_ID = 12;
		public static final int LOOP_ID = 6;
		public static final int SEG_ID = 3;
		public static final int SV2027_DS_TX = 80;
		public static final int UNIT_BSS_MEAS_CD = 2;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
