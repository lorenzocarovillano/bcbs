/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: DB2-S02815-PROVIDER-INFO<br>
 * Variable: DB2-S02815-PROVIDER-INFO from copybook NF05CURS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Db2S02815ProviderInfo {

	//==== PROPERTIES ====
	//Original name: DB2-BI-PROV-ID
	private String biProvId = DefaultValues.stringVal(Len.BI_PROV_ID);
	//Original name: DB2-BI-PROV-LOB
	private char biProvLob = DefaultValues.CHAR_VAL;
	//Original name: DB2-BI-PROV-SPEC
	private String biProvSpec = DefaultValues.stringVal(Len.BI_PROV_SPEC);
	//Original name: DB2-PE-PROV-ID
	private String peProvId = DefaultValues.stringVal(Len.PE_PROV_ID);
	//Original name: DB2-PE-PROV-LOB
	private char peProvLob = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setS02815ProviderInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		biProvId = MarshalByte.readString(buffer, position, Len.BI_PROV_ID);
		position += Len.BI_PROV_ID;
		biProvLob = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		biProvSpec = MarshalByte.readString(buffer, position, Len.BI_PROV_SPEC);
		position += Len.BI_PROV_SPEC;
		peProvId = MarshalByte.readString(buffer, position, Len.PE_PROV_ID);
		position += Len.PE_PROV_ID;
		peProvLob = MarshalByte.readChar(buffer, position);
	}

	public byte[] getS02815ProviderInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, biProvId, Len.BI_PROV_ID);
		position += Len.BI_PROV_ID;
		MarshalByte.writeChar(buffer, position, biProvLob);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, biProvSpec, Len.BI_PROV_SPEC);
		position += Len.BI_PROV_SPEC;
		MarshalByte.writeString(buffer, position, peProvId, Len.PE_PROV_ID);
		position += Len.PE_PROV_ID;
		MarshalByte.writeChar(buffer, position, peProvLob);
		return buffer;
	}

	public void setBiProvId(String biProvId) {
		this.biProvId = Functions.subString(biProvId, Len.BI_PROV_ID);
	}

	public String getBiProvId() {
		return this.biProvId;
	}

	public void setBiProvLob(char biProvLob) {
		this.biProvLob = biProvLob;
	}

	public char getBiProvLob() {
		return this.biProvLob;
	}

	public void setBiProvSpec(String biProvSpec) {
		this.biProvSpec = Functions.subString(biProvSpec, Len.BI_PROV_SPEC);
	}

	public String getBiProvSpec() {
		return this.biProvSpec;
	}

	public void setPeProvId(String peProvId) {
		this.peProvId = Functions.subString(peProvId, Len.PE_PROV_ID);
	}

	public String getPeProvId() {
		return this.peProvId;
	}

	public void setPeProvLob(char peProvLob) {
		this.peProvLob = peProvLob;
	}

	public char getPeProvLob() {
		return this.peProvLob;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BI_PROV_ID = 10;
		public static final int BI_PROV_SPEC = 4;
		public static final int PE_PROV_ID = 10;
		public static final int BI_PROV_LOB = 1;
		public static final int PE_PROV_LOB = 1;
		public static final int S02815_PROVIDER_INFO = BI_PROV_ID + BI_PROV_LOB + BI_PROV_SPEC + PE_PROV_ID + PE_PROV_LOB;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
