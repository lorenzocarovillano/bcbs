/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: HOLD-KEY<br>
 * Variable: HOLD-KEY from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class HoldKey {

	//==== PROPERTIES ====
	//Original name: HOLD-CLAIM-ID-1
	private String claimId1 = DefaultValues.stringVal(Len.CLAIM_ID1);
	//Original name: FILLER-HOLD-CLAIM-ID-2
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: FILLER-HOLD-CLAIM-ID
	private String flr2 = DefaultValues.stringVal(Len.FLR2);
	//Original name: HOLD-CLAIM-SFX
	private char claimSfx = DefaultValues.CHAR_VAL;
	//Original name: HOLD-PMT-ID
	private String pmtId = DefaultValues.stringVal(Len.PMT_ID);

	//==== METHODS ====
	public String getHoldKeyFormatted() {
		return MarshalByteExt.bufferToStr(getHoldKeyBytes());
	}

	public void setHoldKeyBytes(byte[] buffer) {
		setHoldKeyBytes(buffer, 1);
	}

	public byte[] getHoldKeyBytes() {
		byte[] buffer = new byte[Len.HOLD_KEY];
		return getHoldKeyBytes(buffer, 1);
	}

	public void setHoldKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		setClaimIdBytes(buffer, position);
		position += Len.CLAIM_ID;
		claimSfx = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pmtId = MarshalByte.readFixedString(buffer, position, Len.PMT_ID);
	}

	public byte[] getHoldKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		getClaimIdBytes(buffer, position);
		position += Len.CLAIM_ID;
		MarshalByte.writeChar(buffer, position, claimSfx);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pmtId, Len.PMT_ID);
		return buffer;
	}

	public void setClaimIdFormatted(String data) {
		byte[] buffer = new byte[Len.CLAIM_ID];
		MarshalByte.writeString(buffer, 1, data, Len.CLAIM_ID);
		setClaimIdBytes(buffer, 1);
	}

	public String getClaimIdFormatted() {
		return MarshalByteExt.bufferToStr(getClaimIdBytes());
	}

	/**Original name: HOLD-CLAIM-ID<br>*/
	public byte[] getClaimIdBytes() {
		byte[] buffer = new byte[Len.CLAIM_ID];
		return getClaimIdBytes(buffer, 1);
	}

	public void setClaimIdBytes(byte[] buffer, int offset) {
		int position = offset;
		setClaimId2Bytes(buffer, position);
		position += Len.CLAIM_ID2;
		flr2 = MarshalByte.readFixedString(buffer, position, Len.FLR2);
	}

	public byte[] getClaimIdBytes(byte[] buffer, int offset) {
		int position = offset;
		getClaimId2Bytes(buffer, position);
		position += Len.CLAIM_ID2;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		return buffer;
	}

	public void setClaimId2Bytes(byte[] buffer, int offset) {
		int position = offset;
		claimId1 = MarshalByte.readFixedString(buffer, position, Len.CLAIM_ID1);
		position += Len.CLAIM_ID1;
		flr1 = MarshalByte.readFixedString(buffer, position, Len.FLR1);
	}

	public byte[] getClaimId2Bytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, claimId1, Len.CLAIM_ID1);
		position += Len.CLAIM_ID1;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public short getClaimId1() {
		return NumericDisplay.asShort(this.claimId1);
	}

	public void setClaimSfx(char claimSfx) {
		this.claimSfx = claimSfx;
	}

	public char getClaimSfx() {
		return this.claimSfx;
	}

	public short getPmtId() {
		return NumericDisplay.asShort(this.pmtId);
	}

	public String getPmtIdFormatted() {
		return this.pmtId;
	}

	public String getPmtIdAsString() {
		return getPmtIdFormatted();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLAIM_ID1 = 1;
		public static final int FLR1 = 1;
		public static final int FLR2 = 10;
		public static final int PMT_ID = 2;
		public static final int CLAIM_ID2 = CLAIM_ID1 + FLR1;
		public static final int CLAIM_ID = CLAIM_ID2 + FLR2;
		public static final int CLAIM_SFX = 1;
		public static final int HOLD_KEY = CLAIM_ID + CLAIM_SFX + PMT_ID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
