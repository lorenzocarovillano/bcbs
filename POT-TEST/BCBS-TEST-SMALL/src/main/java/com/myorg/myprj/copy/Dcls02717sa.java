/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.commons.data.to.IS02717sa;

/**Original name: DCLS02717SA<br>
 * Variable: DCLS02717SA from copybook S02717SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02717sa implements IS02717sa {

	//==== PROPERTIES ====
	//Original name: PROD-SRV-ID-QLF-CD
	private String prodSrvIdQlfCd = DefaultValues.stringVal(Len.PROD_SRV_ID_QLF_CD);
	//Original name: PROD-SRV-ID
	private String prodSrvId = DefaultValues.stringVal(Len.PROD_SRV_ID);
	//Original name: SV1013-PROC-MOD-CD
	private String sv1013ProcModCd = DefaultValues.stringVal(Len.SV1013_PROC_MOD_CD);
	//Original name: SV1014-PROC-MOD-CD
	private String sv1014ProcModCd = DefaultValues.stringVal(Len.SV1014_PROC_MOD_CD);
	//Original name: SV1015-PROC-MOD-CD
	private String sv1015ProcModCd = DefaultValues.stringVal(Len.SV1015_PROC_MOD_CD);
	//Original name: SV1016-PROC-MOD-CD
	private String sv1016ProcModCd = DefaultValues.stringVal(Len.SV1016_PROC_MOD_CD);
	//Original name: SV102-MNTRY-AM
	private AfDecimal sv102MntryAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: QNTY-CT
	private AfDecimal qntyCt = new AfDecimal(DefaultValues.DEC_VAL, 7, 2);

	//==== METHODS ====
	@Override
	public void setProdSrvIdQlfCd(String prodSrvIdQlfCd) {
		this.prodSrvIdQlfCd = Functions.subString(prodSrvIdQlfCd, Len.PROD_SRV_ID_QLF_CD);
	}

	@Override
	public String getProdSrvIdQlfCd() {
		return this.prodSrvIdQlfCd;
	}

	@Override
	public void setProdSrvId(String prodSrvId) {
		this.prodSrvId = Functions.subString(prodSrvId, Len.PROD_SRV_ID);
	}

	@Override
	public String getProdSrvId() {
		return this.prodSrvId;
	}

	@Override
	public void setSv1013ProcModCd(String sv1013ProcModCd) {
		this.sv1013ProcModCd = Functions.subString(sv1013ProcModCd, Len.SV1013_PROC_MOD_CD);
	}

	@Override
	public String getSv1013ProcModCd() {
		return this.sv1013ProcModCd;
	}

	@Override
	public void setSv1014ProcModCd(String sv1014ProcModCd) {
		this.sv1014ProcModCd = Functions.subString(sv1014ProcModCd, Len.SV1014_PROC_MOD_CD);
	}

	@Override
	public String getSv1014ProcModCd() {
		return this.sv1014ProcModCd;
	}

	@Override
	public void setSv1015ProcModCd(String sv1015ProcModCd) {
		this.sv1015ProcModCd = Functions.subString(sv1015ProcModCd, Len.SV1015_PROC_MOD_CD);
	}

	@Override
	public String getSv1015ProcModCd() {
		return this.sv1015ProcModCd;
	}

	@Override
	public void setSv1016ProcModCd(String sv1016ProcModCd) {
		this.sv1016ProcModCd = Functions.subString(sv1016ProcModCd, Len.SV1016_PROC_MOD_CD);
	}

	@Override
	public String getSv1016ProcModCd() {
		return this.sv1016ProcModCd;
	}

	@Override
	public void setSv102MntryAm(AfDecimal sv102MntryAm) {
		this.sv102MntryAm.assign(sv102MntryAm);
	}

	@Override
	public AfDecimal getSv102MntryAm() {
		return this.sv102MntryAm.copy();
	}

	@Override
	public void setQntyCt(AfDecimal qntyCt) {
		this.qntyCt.assign(qntyCt);
	}

	@Override
	public AfDecimal getQntyCt() {
		return this.qntyCt.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROD_SRV_ID_QLF_CD = 2;
		public static final int PROD_SRV_ID = 48;
		public static final int SV1013_PROC_MOD_CD = 2;
		public static final int SV1014_PROC_MOD_CD = 2;
		public static final int SV1015_PROC_MOD_CD = 2;
		public static final int SV1016_PROC_MOD_CD = 2;
		public static final int CLM_CNTL_ID = 12;
		public static final int LOOP_ID = 6;
		public static final int SEG_ID = 3;
		public static final int SV1017_DS_TX = 80;
		public static final int UNIT_BSS_MEAS_CD = 2;
		public static final int FAC_VL_CD = 2;
		public static final int SV1071_DIAG_PT_CD = 2;
		public static final int SV1072_DIAG_PT_CD = 2;
		public static final int SV1073_DIAG_PT_CD = 2;
		public static final int SV1074_DIAG_PT_CD = 2;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
