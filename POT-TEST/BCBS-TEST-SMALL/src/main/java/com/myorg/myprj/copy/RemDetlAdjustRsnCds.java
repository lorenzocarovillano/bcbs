/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: REM-DETL-ADJUST-RSN-CDS<br>
 * Variable: REM-DETL-ADJUST-RSN-CDS from copybook NF07RMIT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class RemDetlAdjustRsnCds {

	//==== PROPERTIES ====
	//Original name: REM-DETL-ADJ-RSN-CD-1
	private String cd1 = DefaultValues.stringVal(Len.CD1);
	//Original name: REM-DETL-ADJ-RSN-CD-2
	private String cd2 = DefaultValues.stringVal(Len.CD2);
	//Original name: REM-DETL-ADJ-RSN-CD-3
	private String cd3 = DefaultValues.stringVal(Len.CD3);
	//Original name: REM-DETL-ADJ-RSN-CD-4
	private String cd4 = DefaultValues.stringVal(Len.CD4);

	//==== METHODS ====
	public void setDetlAdjustRsnCdsBytes(byte[] buffer, int offset) {
		int position = offset;
		cd1 = MarshalByte.readString(buffer, position, Len.CD1);
		position += Len.CD1;
		cd2 = MarshalByte.readString(buffer, position, Len.CD2);
		position += Len.CD2;
		cd3 = MarshalByte.readString(buffer, position, Len.CD3);
		position += Len.CD3;
		cd4 = MarshalByte.readString(buffer, position, Len.CD4);
	}

	public byte[] getDetlAdjustRsnCdsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cd1, Len.CD1);
		position += Len.CD1;
		MarshalByte.writeString(buffer, position, cd2, Len.CD2);
		position += Len.CD2;
		MarshalByte.writeString(buffer, position, cd3, Len.CD3);
		position += Len.CD3;
		MarshalByte.writeString(buffer, position, cd4, Len.CD4);
		return buffer;
	}

	public void setCd1(String cd1) {
		this.cd1 = Functions.subString(cd1, Len.CD1);
	}

	public String getCd1() {
		return this.cd1;
	}

	public void setCd2(String cd2) {
		this.cd2 = Functions.subString(cd2, Len.CD2);
	}

	public String getCd2() {
		return this.cd2;
	}

	public void setCd3(String cd3) {
		this.cd3 = Functions.subString(cd3, Len.CD3);
	}

	public String getCd3() {
		return this.cd3;
	}

	public void setCd4(String cd4) {
		this.cd4 = Functions.subString(cd4, Len.CD4);
	}

	public String getCd4() {
		return this.cd4;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CD1 = 3;
		public static final int CD2 = 3;
		public static final int CD3 = 3;
		public static final int CD4 = 3;
		public static final int DETL_ADJUST_RSN_CDS = CD1 + CD2 + CD3 + CD4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
