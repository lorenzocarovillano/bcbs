/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-ALL-COUNTERS<br>
 * Variable: WS-ALL-COUNTERS from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsAllCounters {

	//==== PROPERTIES ====
	//Original name: HEADING-CNTR
	private String headingCntr = DefaultValues.stringVal(Len.HEADING_CNTR);
	//Original name: REC-LINE-CNTR
	private String recLineCntr = DefaultValues.stringVal(Len.REC_LINE_CNTR);
	//Original name: DETAIL-LINE-CNTR
	private String detailLineCntr = DefaultValues.stringVal(Len.DETAIL_LINE_CNTR);
	//Original name: REG-EXPENSE-COUNT
	private String regExpenseCount = DefaultValues.stringVal(Len.REG_EXPENSE_COUNT);
	//Original name: KSS-EXPENSE-COUNT
	private String kssExpenseCount = DefaultValues.stringVal(Len.KSS_EXPENSE_COUNT);
	//Original name: ADJUST-IN-DOLLARS
	private AfDecimal adjustInDollars = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: REG-WRITEOFF-CNTR
	private String regWriteoffCntr = DefaultValues.stringVal(Len.REG_WRITEOFF_CNTR);
	//Original name: OUT-INTEREST-CNTR
	private String outInterestCntr = DefaultValues.stringVal(Len.OUT_INTEREST_CNTR);
	//Original name: OUT-KSS-INT-CNTR
	private String outKssIntCntr = DefaultValues.stringVal(Len.OUT_KSS_INT_CNTR);
	//Original name: TOT-TOTAL-INTEREST
	private AfDecimal totTotalInterest = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: TOT-LATE-PMT-INTEREST
	private AfDecimal totLatePmtInterest = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: TOT-LATE-DENIAL-INTEREST
	private AfDecimal totLateDenialInterest = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: WS-COUNTERS
	private WsCounters wsCounters = new WsCounters();

	//==== METHODS ====
	public void setWsAllCountersFormatted(String data) {
		byte[] buffer = new byte[Len.WS_ALL_COUNTERS];
		MarshalByte.writeString(buffer, 1, data, Len.WS_ALL_COUNTERS);
		setWsAllCountersBytes(buffer, 1);
	}

	public String getWsAllCountersFormatted() {
		return MarshalByteExt.bufferToStr(getWsAllCountersBytes());
	}

	public byte[] getWsAllCountersBytes() {
		byte[] buffer = new byte[Len.WS_ALL_COUNTERS];
		return getWsAllCountersBytes(buffer, 1);
	}

	public void setWsAllCountersBytes(byte[] buffer, int offset) {
		int position = offset;
		headingCntr = MarshalByte.readFixedString(buffer, position, Len.HEADING_CNTR);
		position += Len.HEADING_CNTR;
		recLineCntr = MarshalByte.readFixedString(buffer, position, Len.REC_LINE_CNTR);
		position += Len.REC_LINE_CNTR;
		detailLineCntr = MarshalByte.readFixedString(buffer, position, Len.DETAIL_LINE_CNTR);
		position += Len.DETAIL_LINE_CNTR;
		regExpenseCount = MarshalByte.readFixedString(buffer, position, Len.REG_EXPENSE_COUNT);
		position += Len.REG_EXPENSE_COUNT;
		kssExpenseCount = MarshalByte.readFixedString(buffer, position, Len.KSS_EXPENSE_COUNT);
		position += Len.KSS_EXPENSE_COUNT;
		adjustInDollars.assign(MarshalByte.readDecimal(buffer, position, Len.Int.ADJUST_IN_DOLLARS, Len.Fract.ADJUST_IN_DOLLARS));
		position += Len.ADJUST_IN_DOLLARS;
		regWriteoffCntr = MarshalByte.readFixedString(buffer, position, Len.REG_WRITEOFF_CNTR);
		position += Len.REG_WRITEOFF_CNTR;
		outInterestCntr = MarshalByte.readFixedString(buffer, position, Len.OUT_INTEREST_CNTR);
		position += Len.OUT_INTEREST_CNTR;
		outKssIntCntr = MarshalByte.readFixedString(buffer, position, Len.OUT_KSS_INT_CNTR);
		position += Len.OUT_KSS_INT_CNTR;
		totTotalInterest.assign(MarshalByte.readDecimal(buffer, position, Len.Int.TOT_TOTAL_INTEREST, Len.Fract.TOT_TOTAL_INTEREST));
		position += Len.TOT_TOTAL_INTEREST;
		totLatePmtInterest.assign(MarshalByte.readDecimal(buffer, position, Len.Int.TOT_LATE_PMT_INTEREST, Len.Fract.TOT_LATE_PMT_INTEREST));
		position += Len.TOT_LATE_PMT_INTEREST;
		totLateDenialInterest.assign(MarshalByte.readDecimal(buffer, position, Len.Int.TOT_LATE_DENIAL_INTEREST, Len.Fract.TOT_LATE_DENIAL_INTEREST));
		position += Len.TOT_LATE_DENIAL_INTEREST;
		wsCounters.setWsCountersBytes(buffer, position);
	}

	public byte[] getWsAllCountersBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, headingCntr, Len.HEADING_CNTR);
		position += Len.HEADING_CNTR;
		MarshalByte.writeString(buffer, position, recLineCntr, Len.REC_LINE_CNTR);
		position += Len.REC_LINE_CNTR;
		MarshalByte.writeString(buffer, position, detailLineCntr, Len.DETAIL_LINE_CNTR);
		position += Len.DETAIL_LINE_CNTR;
		MarshalByte.writeString(buffer, position, regExpenseCount, Len.REG_EXPENSE_COUNT);
		position += Len.REG_EXPENSE_COUNT;
		MarshalByte.writeString(buffer, position, kssExpenseCount, Len.KSS_EXPENSE_COUNT);
		position += Len.KSS_EXPENSE_COUNT;
		MarshalByte.writeDecimal(buffer, position, adjustInDollars.copy());
		position += Len.ADJUST_IN_DOLLARS;
		MarshalByte.writeString(buffer, position, regWriteoffCntr, Len.REG_WRITEOFF_CNTR);
		position += Len.REG_WRITEOFF_CNTR;
		MarshalByte.writeString(buffer, position, outInterestCntr, Len.OUT_INTEREST_CNTR);
		position += Len.OUT_INTEREST_CNTR;
		MarshalByte.writeString(buffer, position, outKssIntCntr, Len.OUT_KSS_INT_CNTR);
		position += Len.OUT_KSS_INT_CNTR;
		MarshalByte.writeDecimal(buffer, position, totTotalInterest.copy());
		position += Len.TOT_TOTAL_INTEREST;
		MarshalByte.writeDecimal(buffer, position, totLatePmtInterest.copy());
		position += Len.TOT_LATE_PMT_INTEREST;
		MarshalByte.writeDecimal(buffer, position, totLateDenialInterest.copy());
		position += Len.TOT_LATE_DENIAL_INTEREST;
		wsCounters.getWsCountersBytes(buffer, position);
		return buffer;
	}

	public void setHeadingCntrFormatted(String headingCntr) {
		this.headingCntr = Trunc.toUnsignedNumeric(headingCntr, Len.HEADING_CNTR);
	}

	public int getHeadingCntr() {
		return NumericDisplay.asInt(this.headingCntr);
	}

	public void setRecLineCntrFormatted(String recLineCntr) {
		this.recLineCntr = Trunc.toUnsignedNumeric(recLineCntr, Len.REC_LINE_CNTR);
	}

	public int getRecLineCntr() {
		return NumericDisplay.asInt(this.recLineCntr);
	}

	public void setDetailLineCntrFormatted(String detailLineCntr) {
		this.detailLineCntr = Trunc.toUnsignedNumeric(detailLineCntr, Len.DETAIL_LINE_CNTR);
	}

	public int getDetailLineCntr() {
		return NumericDisplay.asInt(this.detailLineCntr);
	}

	public void setRegExpenseCount(int regExpenseCount) {
		this.regExpenseCount = NumericDisplay.asString(regExpenseCount, Len.REG_EXPENSE_COUNT);
	}

	public void setRegExpenseCountFormatted(String regExpenseCount) {
		this.regExpenseCount = Trunc.toUnsignedNumeric(regExpenseCount, Len.REG_EXPENSE_COUNT);
	}

	public int getRegExpenseCount() {
		return NumericDisplay.asInt(this.regExpenseCount);
	}

	public String getRegExpenseCountFormatted() {
		return this.regExpenseCount;
	}

	public String getRegExpenseCountAsString() {
		return getRegExpenseCountFormatted();
	}

	public void setKssExpenseCountFormatted(String kssExpenseCount) {
		this.kssExpenseCount = Trunc.toUnsignedNumeric(kssExpenseCount, Len.KSS_EXPENSE_COUNT);
	}

	public int getKssExpenseCount() {
		return NumericDisplay.asInt(this.kssExpenseCount);
	}

	public String getKssExpenseCountFormatted() {
		return this.kssExpenseCount;
	}

	public String getKssExpenseCountAsString() {
		return getKssExpenseCountFormatted();
	}

	public void setAdjustInDollars(AfDecimal adjustInDollars) {
		this.adjustInDollars.assign(adjustInDollars);
	}

	public AfDecimal getAdjustInDollars() {
		return this.adjustInDollars.copy();
	}

	public void setRegWriteoffCntr(int regWriteoffCntr) {
		this.regWriteoffCntr = NumericDisplay.asString(regWriteoffCntr, Len.REG_WRITEOFF_CNTR);
	}

	public void setRegWriteoffCntrFormatted(String regWriteoffCntr) {
		this.regWriteoffCntr = Trunc.toUnsignedNumeric(regWriteoffCntr, Len.REG_WRITEOFF_CNTR);
	}

	public int getRegWriteoffCntr() {
		return NumericDisplay.asInt(this.regWriteoffCntr);
	}

	public String getRegWriteoffCntrFormatted() {
		return this.regWriteoffCntr;
	}

	public String getRegWriteoffCntrAsString() {
		return getRegWriteoffCntrFormatted();
	}

	public void setOutInterestCntr(int outInterestCntr) {
		this.outInterestCntr = NumericDisplay.asString(outInterestCntr, Len.OUT_INTEREST_CNTR);
	}

	public void setOutInterestCntrFormatted(String outInterestCntr) {
		this.outInterestCntr = Trunc.toUnsignedNumeric(outInterestCntr, Len.OUT_INTEREST_CNTR);
	}

	public int getOutInterestCntr() {
		return NumericDisplay.asInt(this.outInterestCntr);
	}

	public String getOutInterestCntrFormatted() {
		return this.outInterestCntr;
	}

	public String getOutInterestCntrAsString() {
		return getOutInterestCntrFormatted();
	}

	public void setOutKssIntCntrFormatted(String outKssIntCntr) {
		this.outKssIntCntr = Trunc.toUnsignedNumeric(outKssIntCntr, Len.OUT_KSS_INT_CNTR);
	}

	public int getOutKssIntCntr() {
		return NumericDisplay.asInt(this.outKssIntCntr);
	}

	public String getOutKssIntCntrFormatted() {
		return this.outKssIntCntr;
	}

	public String getOutKssIntCntrAsString() {
		return getOutKssIntCntrFormatted();
	}

	public void setTotTotalInterest(AfDecimal totTotalInterest) {
		this.totTotalInterest.assign(totTotalInterest);
	}

	public AfDecimal getTotTotalInterest() {
		return this.totTotalInterest.copy();
	}

	public void setTotLatePmtInterest(AfDecimal totLatePmtInterest) {
		this.totLatePmtInterest.assign(totLatePmtInterest);
	}

	public AfDecimal getTotLatePmtInterest() {
		return this.totLatePmtInterest.copy();
	}

	public void setTotLateDenialInterest(AfDecimal totLateDenialInterest) {
		this.totLateDenialInterest.assign(totLateDenialInterest);
	}

	public AfDecimal getTotLateDenialInterest() {
		return this.totLateDenialInterest.copy();
	}

	public WsCounters getWsCounters() {
		return wsCounters;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HEADING_CNTR = 7;
		public static final int REC_LINE_CNTR = 7;
		public static final int DETAIL_LINE_CNTR = 7;
		public static final int REG_EXPENSE_COUNT = 7;
		public static final int KSS_EXPENSE_COUNT = 7;
		public static final int REG_WRITEOFF_CNTR = 7;
		public static final int OUT_INTEREST_CNTR = 7;
		public static final int OUT_KSS_INT_CNTR = 7;
		public static final int ADJUST_IN_DOLLARS = 13;
		public static final int TOT_TOTAL_INTEREST = 11;
		public static final int TOT_LATE_PMT_INTEREST = 11;
		public static final int TOT_LATE_DENIAL_INTEREST = 11;
		public static final int WS_ALL_COUNTERS = HEADING_CNTR + REC_LINE_CNTR + DETAIL_LINE_CNTR + REG_EXPENSE_COUNT + KSS_EXPENSE_COUNT
				+ ADJUST_IN_DOLLARS + REG_WRITEOFF_CNTR + OUT_INTEREST_CNTR + OUT_KSS_INT_CNTR + TOT_TOTAL_INTEREST + TOT_LATE_PMT_INTEREST
				+ TOT_LATE_DENIAL_INTEREST + WsCounters.Len.WS_COUNTERS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int ADJUST_IN_DOLLARS = 11;
			public static final int TOT_TOTAL_INTEREST = 9;
			public static final int TOT_LATE_PMT_INTEREST = 9;
			public static final int TOT_LATE_DENIAL_INTEREST = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int ADJUST_IN_DOLLARS = 2;
			public static final int TOT_TOTAL_INTEREST = 2;
			public static final int TOT_LATE_PMT_INTEREST = 2;
			public static final int TOT_LATE_DENIAL_INTEREST = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
