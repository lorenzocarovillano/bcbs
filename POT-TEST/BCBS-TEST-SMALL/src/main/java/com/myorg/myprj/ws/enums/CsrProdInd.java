/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: CSR-PROD-IND<br>
 * Variable: CSR-PROD-IND from copybook NF07AREA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CsrProdInd {

	//==== PROPERTIES ====
	private String value = "";
	public static final String BCBS = "BCS";
	public static final String FEP = "FEP";
	public static final String KSS = "KSS";

	//==== METHODS ====
	public void setProdInd(String prodInd) {
		this.value = Functions.subString(prodInd, Len.VALUE);
	}

	public String getProdInd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
