/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PROGRAM-HOLD-AREAS<br>
 * Variable: PROGRAM-HOLD-AREAS from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ProgramHoldAreas {

	//==== PROPERTIES ====
	//Original name: WS-TRIG-PMT-ID
	private short wsTrigPmtId = DefaultValues.SHORT_VAL;
	//Original name: WS-TRIG-CLM-PD-AM
	private AfDecimal wsTrigClmPdAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: WS-TRIG-LOCAL-BILL-PROV-LOB-CD
	private char wsTrigLocalBillProvLobCd = Types.SPACE_CHAR;
	//Original name: WS-TRIG-LST-FNL-PMT-PT-ID
	private short wsTrigLstFnlPmtPtId = DefaultValues.SHORT_VAL;
	//Original name: WS-TRIG-GL-SOTE-ORIG-CD
	private short wsTrigGlSoteOrigCd = DefaultValues.SHORT_VAL;
	//Original name: WS-ACCRUED-PRMPT-PAY-INT-AM
	private AfDecimal wsAccruedPrmptPayIntAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: WS-TRIG-SCHDL-DRG-ALW-AM
	private AfDecimal wsTrigSchdlDrgAlwAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: WS-TRIG-ALT-DRG-ALW-AM
	private AfDecimal wsTrigAltDrgAlwAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: WS-TRIG-OVER-DRG-ALW-AM
	private AfDecimal wsTrigOverDrgAlwAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: TRIG-NPI-CD
	private char trigNpiCd = Types.SPACE_CHAR;
	//Original name: WS-XR4-CRS-RF-CLM-CNTL-ID
	private String wsXr4CrsRfClmCntlId = "";
	//Original name: WS-XR4-CS-RF-CLM-CL-SX-ID
	private char wsXr4CsRfClmClSxId = Types.SPACE_CHAR;
	//Original name: WS-XR4-CRS-REF-CLM-PMT-N
	private WsXr4CrsRefClmPmtN wsXr4CrsRefClmPmtN = new WsXr4CrsRefClmPmtN();
	//Original name: WS-XR4-CRS-REF-CLI-N
	private WsXr4CrsRefCliN wsXr4CrsRefCliN = new WsXr4CrsRefCliN();
	//Original name: WS-XR4-CRS-REF-RSN-CD
	private char wsXr4CrsRefRsnCd = Types.SPACE_CHAR;
	//Original name: WS-XR4-DNL-RMRK-CD
	private String wsXr4DnlRmrkCd = "";
	//Original name: WS-XR4-ADJD-PROC-CD
	private String wsXr4AdjdProcCd = "";
	//Original name: WS-XR4-ADJD-PROC-MOD1-CD
	private String wsXr4AdjdProcMod1Cd = "";
	//Original name: WS-XR4-ADJD-PROC-MOD2-CD
	private String wsXr4AdjdProcMod2Cd = "";
	//Original name: WS-XR4-ADJD-PROC-MOD3-CD
	private String wsXr4AdjdProcMod3Cd = "";
	//Original name: WS-XR4-ADJD-PROC-MOD4-CD
	private String wsXr4AdjdProcMod4Cd = "";
	//Original name: WS-XR4-INIT-CLM-CNTL-ID
	private String wsXr4InitClmCntlId = "";
	//Original name: WS-XR4-INT-CLM-CNL-SFX-ID
	private char wsXr4IntClmCnlSfxId = Types.SPACE_CHAR;
	//Original name: WS-XR4-INIT-CLM-PMT-ID
	private String wsXr4InitClmPmtId = "";
	//Original name: WS-XR4-XR3-INIT-CLM-TS
	private String wsXr4Xr3InitClmTs = "";
	//Original name: WS-XR4-ACT-INACT-CD
	private char wsXr4ActInactCd = Types.SPACE_CHAR;
	//Original name: WS-VR4-CRS-RF-CLM-CNTL-ID
	private String wsVr4CrsRfClmCntlId = "";
	//Original name: WS-VR4-CS-RF-CLM-CL-SX-ID
	private char wsVr4CsRfClmClSxId = Types.SPACE_CHAR;
	//Original name: WS-VR4-CRS-REF-CLM-PMT-N
	private WsVr4CrsRefClmPmtN wsVr4CrsRefClmPmtN = new WsVr4CrsRefClmPmtN();
	//Original name: WS-VR4-CRS-REF-CLI-N
	private WsVr4CrsRefCliN wsVr4CrsRefCliN = new WsVr4CrsRefCliN();
	//Original name: WS-VR4-CRS-REF-RSN-CD
	private char wsVr4CrsRefRsnCd = Types.SPACE_CHAR;
	//Original name: WS-VR4-DNL-RMRK-CD
	private String wsVr4DnlRmrkCd = "";
	//Original name: WS-VR4-ADJD-PROC-CD
	private String wsVr4AdjdProcCd = "";
	//Original name: WS-VR4-ADJD-PROC-MOD1-CD
	private String wsVr4AdjdProcMod1Cd = "";
	//Original name: WS-VR4-ADJD-PROC-MOD2-CD
	private String wsVr4AdjdProcMod2Cd = "";
	//Original name: WS-VR4-ADJD-PROC-MOD3-CD
	private String wsVr4AdjdProcMod3Cd = "";
	//Original name: WS-VR4-ADJD-PROC-MOD4-CD
	private String wsVr4AdjdProcMod4Cd = "";
	//Original name: WS-VR4-INIT-CLM-CNTL-ID
	private String wsVr4InitClmCntlId = "";
	//Original name: WS-VR4-INT-CLM-CNL-SFX-ID
	private char wsVr4IntClmCnlSfxId = Types.SPACE_CHAR;
	//Original name: WS-VR4-INIT-CLM-PMT-ID
	private String wsVr4InitClmPmtId = "";
	//Original name: WS-VR4-ACT-INACT-CD
	private char wsVr4ActInactCd = Types.SPACE_CHAR;

	//==== METHODS ====
	public void setWsTrigPmtId(short wsTrigPmtId) {
		this.wsTrigPmtId = wsTrigPmtId;
	}

	public short getWsTrigPmtId() {
		return this.wsTrigPmtId;
	}

	public void setWsTrigClmPdAm(AfDecimal wsTrigClmPdAm) {
		this.wsTrigClmPdAm.assign(wsTrigClmPdAm);
	}

	public AfDecimal getWsTrigClmPdAm() {
		return this.wsTrigClmPdAm.copy();
	}

	public void setWsTrigLocalBillProvLobCd(char wsTrigLocalBillProvLobCd) {
		this.wsTrigLocalBillProvLobCd = wsTrigLocalBillProvLobCd;
	}

	public char getWsTrigLocalBillProvLobCd() {
		return this.wsTrigLocalBillProvLobCd;
	}

	public void setWsTrigLstFnlPmtPtId(short wsTrigLstFnlPmtPtId) {
		this.wsTrigLstFnlPmtPtId = wsTrigLstFnlPmtPtId;
	}

	public short getWsTrigLstFnlPmtPtId() {
		return this.wsTrigLstFnlPmtPtId;
	}

	public void setWsTrigGlSoteOrigCd(short wsTrigGlSoteOrigCd) {
		this.wsTrigGlSoteOrigCd = wsTrigGlSoteOrigCd;
	}

	public short getWsTrigGlSoteOrigCd() {
		return this.wsTrigGlSoteOrigCd;
	}

	public void setWsAccruedPrmptPayIntAm(AfDecimal wsAccruedPrmptPayIntAm) {
		this.wsAccruedPrmptPayIntAm.assign(wsAccruedPrmptPayIntAm);
	}

	public AfDecimal getWsAccruedPrmptPayIntAm() {
		return this.wsAccruedPrmptPayIntAm.copy();
	}

	public void setWsTrigSchdlDrgAlwAm(AfDecimal wsTrigSchdlDrgAlwAm) {
		this.wsTrigSchdlDrgAlwAm.assign(wsTrigSchdlDrgAlwAm);
	}

	public AfDecimal getWsTrigSchdlDrgAlwAm() {
		return this.wsTrigSchdlDrgAlwAm.copy();
	}

	public void setWsTrigAltDrgAlwAm(AfDecimal wsTrigAltDrgAlwAm) {
		this.wsTrigAltDrgAlwAm.assign(wsTrigAltDrgAlwAm);
	}

	public AfDecimal getWsTrigAltDrgAlwAm() {
		return this.wsTrigAltDrgAlwAm.copy();
	}

	public void setWsTrigOverDrgAlwAm(AfDecimal wsTrigOverDrgAlwAm) {
		this.wsTrigOverDrgAlwAm.assign(wsTrigOverDrgAlwAm);
	}

	public AfDecimal getWsTrigOverDrgAlwAm() {
		return this.wsTrigOverDrgAlwAm.copy();
	}

	public void setTrigNpiCd(char trigNpiCd) {
		this.trigNpiCd = trigNpiCd;
	}

	public char getTrigNpiCd() {
		return this.trigNpiCd;
	}

	public void setWsXr4CrsRfClmCntlId(String wsXr4CrsRfClmCntlId) {
		this.wsXr4CrsRfClmCntlId = Functions.subString(wsXr4CrsRfClmCntlId, Len.WS_XR4_CRS_RF_CLM_CNTL_ID);
	}

	public String getWsXr4CrsRfClmCntlId() {
		return this.wsXr4CrsRfClmCntlId;
	}

	public void setWsXr4CsRfClmClSxId(char wsXr4CsRfClmClSxId) {
		this.wsXr4CsRfClmClSxId = wsXr4CsRfClmClSxId;
	}

	public char getWsXr4CsRfClmClSxId() {
		return this.wsXr4CsRfClmClSxId;
	}

	public void setWsXr4CrsRefRsnCd(char wsXr4CrsRefRsnCd) {
		this.wsXr4CrsRefRsnCd = wsXr4CrsRefRsnCd;
	}

	public char getWsXr4CrsRefRsnCd() {
		return this.wsXr4CrsRefRsnCd;
	}

	public void setWsXr4DnlRmrkCd(String wsXr4DnlRmrkCd) {
		this.wsXr4DnlRmrkCd = Functions.subString(wsXr4DnlRmrkCd, Len.WS_XR4_DNL_RMRK_CD);
	}

	public String getWsXr4DnlRmrkCd() {
		return this.wsXr4DnlRmrkCd;
	}

	public void setWsXr4AdjdProcCd(String wsXr4AdjdProcCd) {
		this.wsXr4AdjdProcCd = Functions.subString(wsXr4AdjdProcCd, Len.WS_XR4_ADJD_PROC_CD);
	}

	public String getWsXr4AdjdProcCd() {
		return this.wsXr4AdjdProcCd;
	}

	public void setWsXr4AdjdProcMod1Cd(String wsXr4AdjdProcMod1Cd) {
		this.wsXr4AdjdProcMod1Cd = Functions.subString(wsXr4AdjdProcMod1Cd, Len.WS_XR4_ADJD_PROC_MOD1_CD);
	}

	public String getWsXr4AdjdProcMod1Cd() {
		return this.wsXr4AdjdProcMod1Cd;
	}

	public void setWsXr4AdjdProcMod2Cd(String wsXr4AdjdProcMod2Cd) {
		this.wsXr4AdjdProcMod2Cd = Functions.subString(wsXr4AdjdProcMod2Cd, Len.WS_XR4_ADJD_PROC_MOD2_CD);
	}

	public String getWsXr4AdjdProcMod2Cd() {
		return this.wsXr4AdjdProcMod2Cd;
	}

	public void setWsXr4AdjdProcMod3Cd(String wsXr4AdjdProcMod3Cd) {
		this.wsXr4AdjdProcMod3Cd = Functions.subString(wsXr4AdjdProcMod3Cd, Len.WS_XR4_ADJD_PROC_MOD3_CD);
	}

	public String getWsXr4AdjdProcMod3Cd() {
		return this.wsXr4AdjdProcMod3Cd;
	}

	public void setWsXr4AdjdProcMod4Cd(String wsXr4AdjdProcMod4Cd) {
		this.wsXr4AdjdProcMod4Cd = Functions.subString(wsXr4AdjdProcMod4Cd, Len.WS_XR4_ADJD_PROC_MOD4_CD);
	}

	public String getWsXr4AdjdProcMod4Cd() {
		return this.wsXr4AdjdProcMod4Cd;
	}

	public void setWsXr4InitClmCntlId(String wsXr4InitClmCntlId) {
		this.wsXr4InitClmCntlId = Functions.subString(wsXr4InitClmCntlId, Len.WS_XR4_INIT_CLM_CNTL_ID);
	}

	public String getWsXr4InitClmCntlId() {
		return this.wsXr4InitClmCntlId;
	}

	public void setWsXr4IntClmCnlSfxId(char wsXr4IntClmCnlSfxId) {
		this.wsXr4IntClmCnlSfxId = wsXr4IntClmCnlSfxId;
	}

	public char getWsXr4IntClmCnlSfxId() {
		return this.wsXr4IntClmCnlSfxId;
	}

	public void setWsXr4InitClmPmtId(String wsXr4InitClmPmtId) {
		this.wsXr4InitClmPmtId = Functions.subString(wsXr4InitClmPmtId, Len.WS_XR4_INIT_CLM_PMT_ID);
	}

	public String getWsXr4InitClmPmtId() {
		return this.wsXr4InitClmPmtId;
	}

	public void setWsXr4Xr3InitClmTs(String wsXr4Xr3InitClmTs) {
		this.wsXr4Xr3InitClmTs = Functions.subString(wsXr4Xr3InitClmTs, Len.WS_XR4_XR3_INIT_CLM_TS);
	}

	public String getWsXr4Xr3InitClmTs() {
		return this.wsXr4Xr3InitClmTs;
	}

	public void setWsXr4ActInactCd(char wsXr4ActInactCd) {
		this.wsXr4ActInactCd = wsXr4ActInactCd;
	}

	public char getWsXr4ActInactCd() {
		return this.wsXr4ActInactCd;
	}

	public void setWsVr4CrsRfClmCntlId(String wsVr4CrsRfClmCntlId) {
		this.wsVr4CrsRfClmCntlId = Functions.subString(wsVr4CrsRfClmCntlId, Len.WS_VR4_CRS_RF_CLM_CNTL_ID);
	}

	public String getWsVr4CrsRfClmCntlId() {
		return this.wsVr4CrsRfClmCntlId;
	}

	public void setWsVr4CsRfClmClSxId(char wsVr4CsRfClmClSxId) {
		this.wsVr4CsRfClmClSxId = wsVr4CsRfClmClSxId;
	}

	public char getWsVr4CsRfClmClSxId() {
		return this.wsVr4CsRfClmClSxId;
	}

	public void setWsVr4CrsRefRsnCd(char wsVr4CrsRefRsnCd) {
		this.wsVr4CrsRefRsnCd = wsVr4CrsRefRsnCd;
	}

	public char getWsVr4CrsRefRsnCd() {
		return this.wsVr4CrsRefRsnCd;
	}

	public void setWsVr4DnlRmrkCd(String wsVr4DnlRmrkCd) {
		this.wsVr4DnlRmrkCd = Functions.subString(wsVr4DnlRmrkCd, Len.WS_VR4_DNL_RMRK_CD);
	}

	public String getWsVr4DnlRmrkCd() {
		return this.wsVr4DnlRmrkCd;
	}

	public void setWsVr4AdjdProcCd(String wsVr4AdjdProcCd) {
		this.wsVr4AdjdProcCd = Functions.subString(wsVr4AdjdProcCd, Len.WS_VR4_ADJD_PROC_CD);
	}

	public String getWsVr4AdjdProcCd() {
		return this.wsVr4AdjdProcCd;
	}

	public void setWsVr4AdjdProcMod1Cd(String wsVr4AdjdProcMod1Cd) {
		this.wsVr4AdjdProcMod1Cd = Functions.subString(wsVr4AdjdProcMod1Cd, Len.WS_VR4_ADJD_PROC_MOD1_CD);
	}

	public String getWsVr4AdjdProcMod1Cd() {
		return this.wsVr4AdjdProcMod1Cd;
	}

	public void setWsVr4AdjdProcMod2Cd(String wsVr4AdjdProcMod2Cd) {
		this.wsVr4AdjdProcMod2Cd = Functions.subString(wsVr4AdjdProcMod2Cd, Len.WS_VR4_ADJD_PROC_MOD2_CD);
	}

	public String getWsVr4AdjdProcMod2Cd() {
		return this.wsVr4AdjdProcMod2Cd;
	}

	public void setWsVr4AdjdProcMod3Cd(String wsVr4AdjdProcMod3Cd) {
		this.wsVr4AdjdProcMod3Cd = Functions.subString(wsVr4AdjdProcMod3Cd, Len.WS_VR4_ADJD_PROC_MOD3_CD);
	}

	public String getWsVr4AdjdProcMod3Cd() {
		return this.wsVr4AdjdProcMod3Cd;
	}

	public void setWsVr4AdjdProcMod4Cd(String wsVr4AdjdProcMod4Cd) {
		this.wsVr4AdjdProcMod4Cd = Functions.subString(wsVr4AdjdProcMod4Cd, Len.WS_VR4_ADJD_PROC_MOD4_CD);
	}

	public String getWsVr4AdjdProcMod4Cd() {
		return this.wsVr4AdjdProcMod4Cd;
	}

	public void setWsVr4InitClmCntlId(String wsVr4InitClmCntlId) {
		this.wsVr4InitClmCntlId = Functions.subString(wsVr4InitClmCntlId, Len.WS_VR4_INIT_CLM_CNTL_ID);
	}

	public String getWsVr4InitClmCntlId() {
		return this.wsVr4InitClmCntlId;
	}

	public void setWsVr4IntClmCnlSfxId(char wsVr4IntClmCnlSfxId) {
		this.wsVr4IntClmCnlSfxId = wsVr4IntClmCnlSfxId;
	}

	public char getWsVr4IntClmCnlSfxId() {
		return this.wsVr4IntClmCnlSfxId;
	}

	public void setWsVr4InitClmPmtId(String wsVr4InitClmPmtId) {
		this.wsVr4InitClmPmtId = Functions.subString(wsVr4InitClmPmtId, Len.WS_VR4_INIT_CLM_PMT_ID);
	}

	public String getWsVr4InitClmPmtId() {
		return this.wsVr4InitClmPmtId;
	}

	public void setWsVr4ActInactCd(char wsVr4ActInactCd) {
		this.wsVr4ActInactCd = wsVr4ActInactCd;
	}

	public char getWsVr4ActInactCd() {
		return this.wsVr4ActInactCd;
	}

	public WsVr4CrsRefCliN getWsVr4CrsRefCliN() {
		return wsVr4CrsRefCliN;
	}

	public WsVr4CrsRefClmPmtN getWsVr4CrsRefClmPmtN() {
		return wsVr4CrsRefClmPmtN;
	}

	public WsXr4CrsRefCliN getWsXr4CrsRefCliN() {
		return wsXr4CrsRefCliN;
	}

	public WsXr4CrsRefClmPmtN getWsXr4CrsRefClmPmtN() {
		return wsXr4CrsRefClmPmtN;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_XR4_CRS_RF_CLM_CNTL_ID = 12;
		public static final int WS_XR4_DNL_RMRK_CD = 3;
		public static final int WS_XR4_ADJD_PROC_CD = 5;
		public static final int WS_XR4_ADJD_PROC_MOD1_CD = 2;
		public static final int WS_XR4_ADJD_PROC_MOD2_CD = 2;
		public static final int WS_XR4_ADJD_PROC_MOD3_CD = 2;
		public static final int WS_XR4_ADJD_PROC_MOD4_CD = 2;
		public static final int WS_XR4_INIT_CLM_CNTL_ID = 12;
		public static final int WS_XR4_INIT_CLM_PMT_ID = 2;
		public static final int WS_XR4_XR3_INIT_CLM_TS = 26;
		public static final int WS_VR4_CRS_RF_CLM_CNTL_ID = 12;
		public static final int WS_VR4_DNL_RMRK_CD = 3;
		public static final int WS_VR4_ADJD_PROC_CD = 5;
		public static final int WS_VR4_ADJD_PROC_MOD1_CD = 2;
		public static final int WS_VR4_ADJD_PROC_MOD2_CD = 2;
		public static final int WS_VR4_ADJD_PROC_MOD3_CD = 2;
		public static final int WS_VR4_ADJD_PROC_MOD4_CD = 2;
		public static final int WS_VR4_INIT_CLM_CNTL_ID = 12;
		public static final int WS_VR4_INIT_CLM_PMT_ID = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
