/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [S02993SA]
 * 
 */
public interface IS02993sa extends BaseSqlTo {

	/**
	 * Host Variable DNL-RMRK-CD
	 * 
	 */
	String getDnlRmrkCd();

	void setDnlRmrkCd(String dnlRmrkCd);
};
