/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.occurs;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.copy.CsrAdjudicatedCodes;

/**Original name: CSR-XREF-AREA<br>
 * Variables: CSR-XREF-AREA from copybook NF07AREA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class CsrXrefArea {

	//==== PROPERTIES ====
	//Original name: CSR-XREF-CLM-ID
	private String clmId = "";
	//Original name: CSR-XREF-SX-ID
	private char sxId = Types.SPACE_CHAR;
	//Original name: CSR-XREF-PMT-ID
	private String pmtId = "00";
	//Original name: CSR-XREF-CLI-ID
	private String cliId = "000";
	//Original name: CSR-XREF-RSN-CD
	private String rsnCd = "";
	//Original name: CSR-XREF-DNL-RMRK-CD
	private String dnlRmrkCd = "";
	//Original name: CSR-XREF-ADJUDICATED-CODES
	private CsrAdjudicatedCodes adjudicatedCodes = new CsrAdjudicatedCodes();

	//==== METHODS ====
	public byte[] getXrefAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		getClaimNumberSuffixBytes(buffer, position);
		position += Len.CLAIM_NUMBER_SUFFIX;
		MarshalByte.writeString(buffer, position, pmtId, Len.PMT_ID);
		position += Len.PMT_ID;
		MarshalByte.writeString(buffer, position, cliId, Len.CLI_ID);
		position += Len.CLI_ID;
		MarshalByte.writeString(buffer, position, rsnCd, Len.RSN_CD);
		position += Len.RSN_CD;
		MarshalByte.writeString(buffer, position, dnlRmrkCd, Len.DNL_RMRK_CD);
		position += Len.DNL_RMRK_CD;
		adjudicatedCodes.getAdjudicatedCodesBytes(buffer, position);
		return buffer;
	}

	public byte[] getClaimNumberSuffixBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clmId, Len.CLM_ID);
		position += Len.CLM_ID;
		MarshalByte.writeChar(buffer, position, sxId);
		return buffer;
	}

	public void setClmId(String clmId) {
		this.clmId = Functions.subString(clmId, Len.CLM_ID);
	}

	public String getClmId() {
		return this.clmId;
	}

	public void setSxId(char sxId) {
		this.sxId = sxId;
	}

	public char getSxId() {
		return this.sxId;
	}

	public void setPmtId(short pmtId) {
		this.pmtId = NumericDisplay.asString(pmtId, Len.PMT_ID);
	}

	public void setPmtIdFromBuffer(byte[] buffer) {
		pmtId = MarshalByte.readFixedString(buffer, 1, Len.PMT_ID);
	}

	public short getPmtId() {
		return NumericDisplay.asShort(this.pmtId);
	}

	public void setCliId(short cliId) {
		this.cliId = NumericDisplay.asString(cliId, Len.CLI_ID);
	}

	public void setCliIdFromBuffer(byte[] buffer) {
		cliId = MarshalByte.readFixedString(buffer, 1, Len.CLI_ID);
	}

	public short getCliId() {
		return NumericDisplay.asShort(this.cliId);
	}

	public void setRsnCd(String rsnCd) {
		this.rsnCd = Functions.subString(rsnCd, Len.RSN_CD);
	}

	public String getRsnCd() {
		return this.rsnCd;
	}

	public void setDnlRmrkCd(String dnlRmrkCd) {
		this.dnlRmrkCd = Functions.subString(dnlRmrkCd, Len.DNL_RMRK_CD);
	}

	public String getDnlRmrkCd() {
		return this.dnlRmrkCd;
	}

	public CsrAdjudicatedCodes getAdjudicatedCodes() {
		return adjudicatedCodes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PMT_ID = 2;
		public static final int CLI_ID = 3;
		public static final int CLM_ID = 12;
		public static final int SX_ID = 1;
		public static final int CLAIM_NUMBER_SUFFIX = CLM_ID + SX_ID;
		public static final int RSN_CD = 2;
		public static final int DNL_RMRK_CD = 2;
		public static final int XREF_AREA = CLAIM_NUMBER_SUFFIX + PMT_ID + CLI_ID + RSN_CD + DNL_RMRK_CD + CsrAdjudicatedCodes.Len.ADJUDICATED_CODES;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
