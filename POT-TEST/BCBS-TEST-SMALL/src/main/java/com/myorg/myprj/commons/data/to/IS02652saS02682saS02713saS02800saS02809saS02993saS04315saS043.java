/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [S02652SA, S02682SA, S02713SA, S02800SA, S02809SA, S02993SA, S04315SA, S04316SA, TRIGGER2]
 * 
 */
public interface IS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043 extends BaseSqlTo {

	/**
	 * Host Variable TRIG-PROD-IND
	 * 
	 */
	String getTrigProdInd();

	void setTrigProdInd(String trigProdInd);

	/**
	 * Host Variable TRIG-HIST-BILL-PROV-NPI-ID
	 * 
	 */
	String getTrigHistBillProvNpiId();

	void setTrigHistBillProvNpiId(String trigHistBillProvNpiId);

	/**
	 * Host Variable TRIG-LOCAL-BILL-PROV-LOB-CD
	 * 
	 */
	char getTrigLocalBillProvLobCd();

	void setTrigLocalBillProvLobCd(char trigLocalBillProvLobCd);

	/**
	 * Host Variable TRIG-LOCAL-BILL-PROV-ID
	 * 
	 */
	String getTrigLocalBillProvId();

	void setTrigLocalBillProvId(String trigLocalBillProvId);

	/**
	 * Host Variable TRIG-BILL-PROV-ID-QLF-CD
	 * 
	 */
	String getTrigBillProvIdQlfCd();

	void setTrigBillProvIdQlfCd(String trigBillProvIdQlfCd);

	/**
	 * Host Variable TRIG-BILL-PROV-SPEC-CD
	 * 
	 */
	String getTrigBillProvSpecCd();

	void setTrigBillProvSpecCd(String trigBillProvSpecCd);

	/**
	 * Host Variable TRIG-HIST-PERF-PROV-NPI-ID
	 * 
	 */
	String getTrigHistPerfProvNpiId();

	void setTrigHistPerfProvNpiId(String trigHistPerfProvNpiId);

	/**
	 * Host Variable TRIG-LOCAL-PERF-PROV-LOB-CD
	 * 
	 */
	char getTrigLocalPerfProvLobCd();

	void setTrigLocalPerfProvLobCd(char trigLocalPerfProvLobCd);

	/**
	 * Host Variable TRIG-LOCAL-PERF-PROV-ID
	 * 
	 */
	String getTrigLocalPerfProvId();

	void setTrigLocalPerfProvId(String trigLocalPerfProvId);

	/**
	 * Host Variable TRIG-INS-ID
	 * 
	 */
	String getTrigInsId();

	void setTrigInsId(String trigInsId);

	/**
	 * Host Variable TRIG-MEMBER-ID
	 * 
	 */
	String getTrigMemberId();

	void setTrigMemberId(String trigMemberId);

	/**
	 * Host Variable TRIG-CORP-RCV-DT
	 * 
	 */
	String getTrigCorpRcvDt();

	void setTrigCorpRcvDt(String trigCorpRcvDt);

	/**
	 * Host Variable TRIG-PMT-ADJD-DT
	 * 
	 */
	String getTrigPmtAdjdDt();

	void setTrigPmtAdjdDt(String trigPmtAdjdDt);

	/**
	 * Host Variable TRIG-PMT-FRM-SERV-DT
	 * 
	 */
	String getTrigPmtFrmServDt();

	void setTrigPmtFrmServDt(String trigPmtFrmServDt);

	/**
	 * Host Variable TRIG-PMT-THR-SERV-DT
	 * 
	 */
	String getTrigPmtThrServDt();

	void setTrigPmtThrServDt(String trigPmtThrServDt);

	/**
	 * Host Variable TRIG-BILL-FRM-DT
	 * 
	 */
	String getTrigBillFrmDt();

	void setTrigBillFrmDt(String trigBillFrmDt);

	/**
	 * Host Variable TRIG-BILL-THR-DT
	 * 
	 */
	String getTrigBillThrDt();

	void setTrigBillThrDt(String trigBillThrDt);

	/**
	 * Host Variable TRIG-ASG-CD
	 * 
	 */
	String getTrigAsgCd();

	void setTrigAsgCd(String trigAsgCd);

	/**
	 * Host Variable WS-TRIG-CLM-PD-AM
	 * 
	 */
	AfDecimal getWsTrigClmPdAm();

	void setWsTrigClmPdAm(AfDecimal wsTrigClmPdAm);

	/**
	 * Host Variable TRIG-ADJ-TYP-CD
	 * 
	 */
	char getTrigAdjTypCd();

	void setTrigAdjTypCd(char trigAdjTypCd);

	/**
	 * Host Variable TRIG-KCAPS-TEAM-NM
	 * 
	 */
	String getTrigKcapsTeamNm();

	void setTrigKcapsTeamNm(String trigKcapsTeamNm);

	/**
	 * Host Variable TRIG-KCAPS-USE-ID
	 * 
	 */
	String getTrigKcapsUseId();

	void setTrigKcapsUseId(String trigKcapsUseId);

	/**
	 * Host Variable TRIG-HIST-LOAD-CD
	 * 
	 */
	char getTrigHistLoadCd();

	void setTrigHistLoadCd(char trigHistLoadCd);

	/**
	 * Host Variable TRIG-PMT-BK-PROD-CD
	 * 
	 */
	char getTrigPmtBkProdCd();

	void setTrigPmtBkProdCd(char trigPmtBkProdCd);

	/**
	 * Host Variable TRIG-DRG-CD
	 * 
	 */
	String getTrigDrgCd();

	void setTrigDrgCd(String trigDrgCd);

	/**
	 * Host Variable WS-TRIG-SCHDL-DRG-ALW-AM
	 * 
	 */
	AfDecimal getWsTrigSchdlDrgAlwAm();

	void setWsTrigSchdlDrgAlwAm(AfDecimal wsTrigSchdlDrgAlwAm);

	/**
	 * Host Variable WS-TRIG-ALT-DRG-ALW-AM
	 * 
	 */
	AfDecimal getWsTrigAltDrgAlwAm();

	void setWsTrigAltDrgAlwAm(AfDecimal wsTrigAltDrgAlwAm);

	/**
	 * Host Variable WS-TRIG-OVER-DRG-ALW-AM
	 * 
	 */
	AfDecimal getWsTrigOverDrgAlwAm();

	void setWsTrigOverDrgAlwAm(AfDecimal wsTrigOverDrgAlwAm);

	/**
	 * Host Variable TRIG-PMT-OI-IN
	 * 
	 */
	char getTrigPmtOiIn();

	void setTrigPmtOiIn(char trigPmtOiIn);

	/**
	 * Host Variable TRIG-PAT-ACT-MED-REC-ID
	 * 
	 */
	String getTrigPatActMedRecId();

	void setTrigPatActMedRecId(String trigPatActMedRecId);

	/**
	 * Host Variable TRIG-PGM-AREA-CD
	 * 
	 */
	String getTrigPgmAreaCd();

	void setTrigPgmAreaCd(String trigPgmAreaCd);

	/**
	 * Host Variable TRIG-GMIS-INDICATOR
	 * 
	 */
	String getTrigGmisIndicator();

	void setTrigGmisIndicator(String trigGmisIndicator);

	/**
	 * Host Variable TRIG-FIN-CD
	 * 
	 */
	String getTrigFinCd();

	void setTrigFinCd(String trigFinCd);

	/**
	 * Host Variable TRIG-ADJD-PROV-STAT-CD
	 * 
	 */
	char getTrigAdjdProvStatCd();

	void setTrigAdjdProvStatCd(char trigAdjdProvStatCd);

	/**
	 * Host Variable TRIG-ITS-CLM-TYP-CD
	 * 
	 */
	char getTrigItsClmTypCd();

	void setTrigItsClmTypCd(char trigItsClmTypCd);

	/**
	 * Host Variable TRIG-PRMPT-PAY-DAY-CD
	 * 
	 */
	String getTrigPrmptPayDayCd();

	void setTrigPrmptPayDayCd(String trigPrmptPayDayCd);

	/**
	 * Host Variable TRIG-PRMPT-PAY-OVRD-CD
	 * 
	 */
	char getTrigPrmptPayOvrdCd();

	void setTrigPrmptPayOvrdCd(char trigPrmptPayOvrdCd);

	/**
	 * Host Variable WS-ACCRUED-PRMPT-PAY-INT-AM
	 * 
	 */
	AfDecimal getWsAccruedPrmptPayIntAm();

	void setWsAccruedPrmptPayIntAm(AfDecimal wsAccruedPrmptPayIntAm);

	/**
	 * Host Variable TRIG-PROV-UNWRP-DT
	 * 
	 */
	String getTrigProvUnwrpDt();

	void setTrigProvUnwrpDt(String trigProvUnwrpDt);

	/**
	 * Host Variable TRIG-GRP-ID
	 * 
	 */
	String getTrigGrpId();

	void setTrigGrpId(String trigGrpId);

	/**
	 * Host Variable TRIG-TYP-GRP-CD
	 * 
	 */
	String getTrigTypGrpCd();

	void setTrigTypGrpCd(String trigTypGrpCd);

	/**
	 * Host Variable TRIG-NPI-CD
	 * 
	 */
	char getTrigNpiCd();

	void setTrigNpiCd(char trigNpiCd);

	/**
	 * Host Variable TRIG-CLAIM-LOB-CD
	 * 
	 */
	char getTrigClaimLobCd();

	void setTrigClaimLobCd(char trigClaimLobCd);

	/**
	 * Host Variable TRIG-RATE-CD
	 * 
	 */
	String getTrigRateCd();

	void setTrigRateCd(String trigRateCd);

	/**
	 * Host Variable TRIG-NTWRK-CD
	 * 
	 */
	String getTrigNtwrkCd();

	void setTrigNtwrkCd(String trigNtwrkCd);

	/**
	 * Host Variable TRIG-BASE-CN-ARNG-CD
	 * 
	 */
	String getTrigBaseCnArngCd();

	void setTrigBaseCnArngCd(String trigBaseCnArngCd);

	/**
	 * Host Variable TRIG-PRIM-CN-ARNG-CD
	 * 
	 */
	String getTrigPrimCnArngCd();

	void setTrigPrimCnArngCd(String trigPrimCnArngCd);

	/**
	 * Host Variable TRIG-ENR-CL-CD
	 * 
	 */
	String getTrigEnrClCd();

	void setTrigEnrClCd(String trigEnrClCd);

	/**
	 * Host Variable WS-TRIG-LST-FNL-PMT-PT-ID
	 * 
	 */
	short getWsTrigLstFnlPmtPtId();

	void setWsTrigLstFnlPmtPtId(short wsTrigLstFnlPmtPtId);

	/**
	 * Host Variable TRIG-STAT-ADJ-PREV-PMT
	 * 
	 */
	char getTrigStatAdjPrevPmt();

	void setTrigStatAdjPrevPmt(char trigStatAdjPrevPmt);

	/**
	 * Host Variable TRIG-EFT-IND
	 * 
	 */
	char getTrigEftInd();

	void setTrigEftInd(char trigEftInd);

	/**
	 * Host Variable TRIG-EFT-ACCOUNT-TYPE
	 * 
	 */
	char getTrigEftAccountType();

	void setTrigEftAccountType(char trigEftAccountType);

	/**
	 * Host Variable TRIG-EFT-ACCT
	 * 
	 */
	String getTrigEftAcct();

	void setTrigEftAcct(String trigEftAcct);

	/**
	 * Host Variable TRIG-EFT-TRANS
	 * 
	 */
	String getTrigEftTrans();

	void setTrigEftTrans(String trigEftTrans);

	/**
	 * Host Variable TRIG-ALPH-PRFX-CD
	 * 
	 */
	String getTrigAlphPrfxCd();

	void setTrigAlphPrfxCd(String trigAlphPrfxCd);

	/**
	 * Host Variable TRIG-GL-OFST-ORIG-CD
	 * 
	 */
	String getTrigGlOfstOrigCd();

	void setTrigGlOfstOrigCd(String trigGlOfstOrigCd);

	/**
	 * Host Variable WS-TRIG-GL-SOTE-ORIG-CD
	 * 
	 */
	short getWsTrigGlSoteOrigCd();

	void setWsTrigGlSoteOrigCd(short wsTrigGlSoteOrigCd);

	/**
	 * Host Variable TRIG-VOID-CD
	 * 
	 */
	char getTrigVoidCd();

	void setTrigVoidCd(char trigVoidCd);

	/**
	 * Host Variable TRIG-ITS-INS-ID
	 * 
	 */
	String getTrigItsInsId();

	void setTrigItsInsId(String trigItsInsId);

	/**
	 * Host Variable TRIG-ADJ-RESP-CD
	 * 
	 */
	char getTrigAdjRespCd();

	void setTrigAdjRespCd(char trigAdjRespCd);

	/**
	 * Host Variable TRIG-ADJ-TRCK-CD
	 * 
	 */
	String getTrigAdjTrckCd();

	void setTrigAdjTrckCd(String trigAdjTrckCd);

	/**
	 * Host Variable TRIG-CLMCK-ADJ-STAT-CD
	 * 
	 */
	char getTrigClmckAdjStatCd();

	void setTrigClmckAdjStatCd(char trigClmckAdjStatCd);

	/**
	 * Host Variable TRIG-837-BILL-PROV-NPI-ID
	 * 
	 */
	String getTrig837BillProvNpiId();

	void setTrig837BillProvNpiId(String trig837BillProvNpiId);

	/**
	 * Host Variable TRIG-837-PERF-PROV-NPI-ID
	 * 
	 */
	String getTrig837PerfProvNpiId();

	void setTrig837PerfProvNpiId(String trig837PerfProvNpiId);

	/**
	 * Host Variable TRIG-ITS-CK
	 * 
	 */
	char getTrigItsCk();

	void setTrigItsCk(char trigItsCk);

	/**
	 * Host Variable TRIG-DATE-COVERAGE-LAPSED
	 * 
	 */
	String getTrigDateCoverageLapsed();

	void setTrigDateCoverageLapsed(String trigDateCoverageLapsed);

	/**
	 * Host Variable TRIG-OI-PAY-NM
	 * 
	 */
	String getTrigOiPayNm();

	void setTrigOiPayNm(String trigOiPayNm);

	/**
	 * Host Variable TRIG-CORR-PRIORITY-SUB-ID
	 * 
	 */
	String getTrigCorrPrioritySubId();

	void setTrigCorrPrioritySubId(String trigCorrPrioritySubId);

	/**
	 * Host Variable TRIG-HIPAA-VERSION-FORMAT-ID
	 * 
	 */
	String getTrigHipaaVersionFormatId();

	void setTrigHipaaVersionFormatId(String trigHipaaVersionFormatId);

	/**
	 * Host Variable TRIG-VBR-IN
	 * 
	 */
	char getTrigVbrIn();

	void setTrigVbrIn(char trigVbrIn);

	/**
	 * Host Variable TRIG-MRKT-PKG-CD
	 * 
	 */
	String getTrigMrktPkgCd();

	void setTrigMrktPkgCd(String trigMrktPkgCd);

	/**
	 * Host Variable CLM-CNTL-ID
	 * 
	 */
	String getClmCntlId();

	void setClmCntlId(String clmCntlId);

	/**
	 * Host Variable CLM-CNTL-SFX-ID
	 * 
	 */
	char getClmCntlSfxId();

	void setClmCntlSfxId(char clmCntlSfxId);

	/**
	 * Host Variable CLM-PMT-ID
	 * 
	 */
	short getClmPmtId();

	void setClmPmtId(short clmPmtId);

	/**
	 * Host Variable CLI-ID
	 * 
	 */
	short getCliId();

	void setCliId(short cliId);

	/**
	 * Host Variable PROV-SBMT-LN-NO-ID
	 * 
	 */
	short getProvSbmtLnNoId();

	void setProvSbmtLnNoId(short provSbmtLnNoId);

	/**
	 * Host Variable ADDNL-RMT-LI-ID
	 * 
	 */
	short getAddnlRmtLiId();

	void setAddnlRmtLiId(short addnlRmtLiId);

	/**
	 * Host Variable LI-FRM-SERV-DT
	 * 
	 */
	String getLiFrmServDt();

	void setLiFrmServDt(String liFrmServDt);

	/**
	 * Host Variable LI-THR-SERV-DT
	 * 
	 */
	String getLiThrServDt();

	void setLiThrServDt(String liThrServDt);

	/**
	 * Host Variable BEN-TYP-CD
	 * 
	 */
	String getBenTypCd();

	void setBenTypCd(String benTypCd);

	/**
	 * Host Variable EXMN-ACTN-CD
	 * 
	 */
	char getExmnActnCd();

	void setExmnActnCd(char exmnActnCd);

	/**
	 * Host Variable DNL-RMRK-CD
	 * 
	 */
	String getDnlRmrkCd();

	void setDnlRmrkCd(String dnlRmrkCd);

	/**
	 * Host Variable DNL-RMRK-USG-CD
	 * 
	 */
	char getDnlRmrkUsgCd();

	void setDnlRmrkUsgCd(char dnlRmrkUsgCd);

	/**
	 * Host Variable PRMPT-PAY-DNL-IN
	 * 
	 */
	char getPrmptPayDnlIn();

	void setPrmptPayDnlIn(char prmptPayDnlIn);

	/**
	 * Host Variable LI-EXPLN-CD
	 * 
	 */
	char getLiExplnCd();

	void setLiExplnCd(char liExplnCd);

	/**
	 * Host Variable RVW-BY-CD
	 * 
	 */
	String getRvwByCd();

	void setRvwByCd(String rvwByCd);

	/**
	 * Host Variable PROC-CD
	 * 
	 */
	String getProcCd();

	void setProcCd(String procCd);

	/**
	 * Host Variable PROC-MOD1-CD
	 * 
	 */
	String getProcMod1Cd();

	void setProcMod1Cd(String procMod1Cd);

	/**
	 * Host Variable PROC-MOD2-CD
	 * 
	 */
	String getProcMod2Cd();

	void setProcMod2Cd(String procMod2Cd);

	/**
	 * Host Variable PROC-MOD3-CD
	 * 
	 */
	String getProcMod3Cd();

	void setProcMod3Cd(String procMod3Cd);

	/**
	 * Host Variable PROC-MOD4-CD
	 * 
	 */
	String getProcMod4Cd();

	void setProcMod4Cd(String procMod4Cd);

	/**
	 * Host Variable REV-CD
	 * 
	 */
	String getRevCd();

	void setRevCd(String revCd);

	/**
	 * Host Variable UNIT-CT
	 * 
	 */
	AfDecimal getUnitCt();

	void setUnitCt(AfDecimal unitCt);

	/**
	 * Host Variable ORIG-SBMT-UNIT-CT
	 * 
	 */
	AfDecimal getOrigSbmtUnitCt();

	void setOrigSbmtUnitCt(AfDecimal origSbmtUnitCt);

	/**
	 * Host Variable POS-CD
	 * 
	 */
	String getPosCd();

	void setPosCd(String posCd);

	/**
	 * Host Variable TS-CD
	 * 
	 */
	String getTsCd();

	void setTsCd(String tsCd);

	/**
	 * Host Variable ORIG-SBMT-CHG-AM
	 * 
	 */
	AfDecimal getOrigSbmtChgAm();

	void setOrigSbmtChgAm(AfDecimal origSbmtChgAm);

	/**
	 * Host Variable CHG-AM
	 * 
	 */
	AfDecimal getChgAm();

	void setChgAm(AfDecimal chgAm);

	/**
	 * Host Variable LI-ALCT-OI-COV-AM
	 * 
	 */
	AfDecimal getLiAlctOiCovAm();

	void setLiAlctOiCovAm(AfDecimal liAlctOiCovAm);

	/**
	 * Host Variable LI-ALCT-OI-PD-AM
	 * 
	 */
	AfDecimal getLiAlctOiPdAm();

	void setLiAlctOiPdAm(AfDecimal liAlctOiPdAm);

	/**
	 * Host Variable LI-ALCT-OI-SAVE-AM
	 * 
	 */
	AfDecimal getLiAlctOiSaveAm();

	void setLiAlctOiSaveAm(AfDecimal liAlctOiSaveAm);

	/**
	 * Host Variable COV-PMT-LVL-STT-CD
	 * 
	 */
	String getCovPmtLvlSttCd();

	void setCovPmtLvlSttCd(String covPmtLvlSttCd);

	/**
	 * Host Variable LI-ALW-CHG-AM
	 * 
	 */
	AfDecimal getLiAlwChgAm();

	void setLiAlwChgAm(AfDecimal liAlwChgAm);

	/**
	 * Host Variable FEP-EDT-OVRD1-CD
	 * 
	 */
	String getFepEdtOvrd1Cd();

	void setFepEdtOvrd1Cd(String fepEdtOvrd1Cd);

	/**
	 * Host Variable FEP-EDT-OVRD2-CD
	 * 
	 */
	String getFepEdtOvrd2Cd();

	void setFepEdtOvrd2Cd(String fepEdtOvrd2Cd);

	/**
	 * Host Variable FEP-EDT-OVRD3-CD
	 * 
	 */
	String getFepEdtOvrd3Cd();

	void setFepEdtOvrd3Cd(String fepEdtOvrd3Cd);

	/**
	 * Host Variable ADJD-OVRD1-CD
	 * 
	 */
	String getAdjdOvrd1Cd();

	void setAdjdOvrd1Cd(String adjdOvrd1Cd);

	/**
	 * Host Variable ADJD-OVRD2-CD
	 * 
	 */
	String getAdjdOvrd2Cd();

	void setAdjdOvrd2Cd(String adjdOvrd2Cd);

	/**
	 * Host Variable ADJD-OVRD3-CD
	 * 
	 */
	String getAdjdOvrd3Cd();

	void setAdjdOvrd3Cd(String adjdOvrd3Cd);

	/**
	 * Host Variable ADJD-OVRD4-CD
	 * 
	 */
	String getAdjdOvrd4Cd();

	void setAdjdOvrd4Cd(String adjdOvrd4Cd);

	/**
	 * Host Variable ADJD-OVRD5-CD
	 * 
	 */
	String getAdjdOvrd5Cd();

	void setAdjdOvrd5Cd(String adjdOvrd5Cd);

	/**
	 * Host Variable ADJD-OVRD6-CD
	 * 
	 */
	String getAdjdOvrd6Cd();

	void setAdjdOvrd6Cd(String adjdOvrd6Cd);

	/**
	 * Host Variable ADJD-OVRD7-CD
	 * 
	 */
	String getAdjdOvrd7Cd();

	void setAdjdOvrd7Cd(String adjdOvrd7Cd);

	/**
	 * Host Variable ADJD-OVRD8-CD
	 * 
	 */
	String getAdjdOvrd8Cd();

	void setAdjdOvrd8Cd(String adjdOvrd8Cd);

	/**
	 * Host Variable ADJD-OVRD9-CD
	 * 
	 */
	String getAdjdOvrd9Cd();

	void setAdjdOvrd9Cd(String adjdOvrd9Cd);

	/**
	 * Host Variable ADJD-OVRD10-CD
	 * 
	 */
	String getAdjdOvrd10Cd();

	void setAdjdOvrd10Cd(String adjdOvrd10Cd);

	/**
	 * Host Variable CRS-RF-CLM-CNTL-ID
	 * 
	 */
	String getCrsRfClmCntlId();

	void setCrsRfClmCntlId(String crsRfClmCntlId);

	/**
	 * Host Variable CS-RF-CLM-CL-SX-ID
	 * 
	 */
	char getCsRfClmClSxId();

	void setCsRfClmClSxId(char csRfClmClSxId);

	/**
	 * Host Variable CRS-REF-CLM-PMT-ID
	 * 
	 */
	short getCrsRefClmPmtId();

	void setCrsRefClmPmtId(short crsRefClmPmtId);

	/**
	 * Host Variable CRS-REF-CLI-ID
	 * 
	 */
	short getCrsRefCliId();

	void setCrsRefCliId(short crsRefCliId);

	/**
	 * Host Variable CRS-REF-RSN-CD
	 * 
	 */
	char getCrsRefRsnCd();

	void setCrsRefRsnCd(char crsRefRsnCd);

	/**
	 * Host Variable ADJD-PROC-CD
	 * 
	 */
	String getAdjdProcCd();

	void setAdjdProcCd(String adjdProcCd);

	/**
	 * Host Variable ADJD-PROC-MOD1-CD
	 * 
	 */
	String getAdjdProcMod1Cd();

	void setAdjdProcMod1Cd(String adjdProcMod1Cd);

	/**
	 * Host Variable ADJD-PROC-MOD2-CD
	 * 
	 */
	String getAdjdProcMod2Cd();

	void setAdjdProcMod2Cd(String adjdProcMod2Cd);

	/**
	 * Host Variable ADJD-PROC-MOD3-CD
	 * 
	 */
	String getAdjdProcMod3Cd();

	void setAdjdProcMod3Cd(String adjdProcMod3Cd);

	/**
	 * Host Variable ADJD-PROC-MOD4-CD
	 * 
	 */
	String getAdjdProcMod4Cd();

	void setAdjdProcMod4Cd(String adjdProcMod4Cd);

	/**
	 * Host Variable INIT-CLM-CNTL-ID
	 * 
	 */
	String getInitClmCntlId();

	void setInitClmCntlId(String initClmCntlId);

	/**
	 * Host Variable INT-CLM-CNL-SFX-ID
	 * 
	 */
	char getIntClmCnlSfxId();

	void setIntClmCnlSfxId(char intClmCnlSfxId);

	/**
	 * Host Variable INIT-CLM-PMT-ID
	 * 
	 */
	short getInitClmPmtId();

	void setInitClmPmtId(short initClmPmtId);

	/**
	 * Host Variable INIT-CLM-TS
	 * 
	 */
	String getInitClmTs();

	void setInitClmTs(String initClmTs);

	/**
	 * Host Variable ACT-INACT-CD
	 * 
	 */
	char getActInactCd();

	void setActInactCd(char actInactCd);

	/**
	 * Host Variable WS-XR4-CRS-RF-CLM-CNTL-ID
	 * 
	 */
	String getWsXr4CrsRfClmCntlId();

	void setWsXr4CrsRfClmCntlId(String wsXr4CrsRfClmCntlId);

	/**
	 * Host Variable WS-XR4-CS-RF-CLM-CL-SX-ID
	 * 
	 */
	char getWsXr4CsRfClmClSxId();

	void setWsXr4CsRfClmClSxId(char wsXr4CsRfClmClSxId);

	/**
	 * Host Variable WS-XR4-CRS-REF-CLM-PMT-ID
	 * 
	 */
	String getWsXr4CrsRefClmPmtId();

	void setWsXr4CrsRefClmPmtId(String wsXr4CrsRefClmPmtId);

	/**
	 * Host Variable WS-XR4-CRS-REF-CLI-ID
	 * 
	 */
	String getWsXr4CrsRefCliId();

	void setWsXr4CrsRefCliId(String wsXr4CrsRefCliId);

	/**
	 * Host Variable WS-XR4-CRS-REF-RSN-CD
	 * 
	 */
	char getWsXr4CrsRefRsnCd();

	void setWsXr4CrsRefRsnCd(char wsXr4CrsRefRsnCd);

	/**
	 * Host Variable WS-XR4-DNL-RMRK-CD
	 * 
	 */
	String getWsXr4DnlRmrkCd();

	void setWsXr4DnlRmrkCd(String wsXr4DnlRmrkCd);

	/**
	 * Host Variable WS-XR4-ADJD-PROC-CD
	 * 
	 */
	String getWsXr4AdjdProcCd();

	void setWsXr4AdjdProcCd(String wsXr4AdjdProcCd);

	/**
	 * Host Variable WS-XR4-ADJD-PROC-MOD1-CD
	 * 
	 */
	String getWsXr4AdjdProcMod1Cd();

	void setWsXr4AdjdProcMod1Cd(String wsXr4AdjdProcMod1Cd);

	/**
	 * Host Variable WS-XR4-ADJD-PROC-MOD2-CD
	 * 
	 */
	String getWsXr4AdjdProcMod2Cd();

	void setWsXr4AdjdProcMod2Cd(String wsXr4AdjdProcMod2Cd);

	/**
	 * Host Variable WS-XR4-ADJD-PROC-MOD3-CD
	 * 
	 */
	String getWsXr4AdjdProcMod3Cd();

	void setWsXr4AdjdProcMod3Cd(String wsXr4AdjdProcMod3Cd);

	/**
	 * Host Variable WS-XR4-ADJD-PROC-MOD4-CD
	 * 
	 */
	String getWsXr4AdjdProcMod4Cd();

	void setWsXr4AdjdProcMod4Cd(String wsXr4AdjdProcMod4Cd);

	/**
	 * Host Variable WS-XR4-INIT-CLM-CNTL-ID
	 * 
	 */
	String getWsXr4InitClmCntlId();

	void setWsXr4InitClmCntlId(String wsXr4InitClmCntlId);

	/**
	 * Host Variable WS-XR4-INT-CLM-CNL-SFX-ID
	 * 
	 */
	char getWsXr4IntClmCnlSfxId();

	void setWsXr4IntClmCnlSfxId(char wsXr4IntClmCnlSfxId);

	/**
	 * Host Variable WS-XR4-INIT-CLM-PMT-ID
	 * 
	 */
	String getWsXr4InitClmPmtId();

	void setWsXr4InitClmPmtId(String wsXr4InitClmPmtId);

	/**
	 * Host Variable WS-XR4-XR3-INIT-CLM-TS
	 * 
	 */
	String getWsXr4Xr3InitClmTs();

	void setWsXr4Xr3InitClmTs(String wsXr4Xr3InitClmTs);

	/**
	 * Host Variable WS-XR4-ACT-INACT-CD
	 * 
	 */
	char getWsXr4ActInactCd();

	void setWsXr4ActInactCd(char wsXr4ActInactCd);

	/**
	 * Host Variable REF-ID
	 * 
	 */
	String getRefId();

	void setRefId(String refId);

	/**
	 * Host Variable ID-CD
	 * 
	 */
	String getIdCd();

	void setIdCd(String idCd);

	/**
	 * Host Variable LST-ORG-NM
	 * 
	 */
	String getLstOrgNm();

	void setLstOrgNm(String lstOrgNm);

	/**
	 * Host Variable FRST-NM
	 * 
	 */
	String getFrstNm();

	void setFrstNm(String frstNm);

	/**
	 * Host Variable MID-NM
	 * 
	 */
	String getMidNm();

	void setMidNm(String midNm);

	/**
	 * Host Variable SFX-NM
	 * 
	 */
	String getSfxNm();

	void setSfxNm(String sfxNm);

	/**
	 * Host Variable CLM-SBMT-ID
	 * 
	 */
	String getClmSbmtId();

	void setClmSbmtId(String clmSbmtId);

	/**
	 * Host Variable MNTRY-AM
	 * 
	 */
	AfDecimal getMntryAm();

	void setMntryAm(AfDecimal mntryAm);

	/**
	 * Host Variable FAC-VL-CD
	 * 
	 */
	String getFacVlCd();

	void setFacVlCd(String facVlCd);

	/**
	 * Host Variable FAC-QLF-CD
	 * 
	 */
	String getFacQlfCd();

	void setFacQlfCd(String facQlfCd);

	/**
	 * Host Variable CLM-FREQ-TYP-CD
	 * 
	 */
	char getClmFreqTypCd();

	void setClmFreqTypCd(char clmFreqTypCd);

	/**
	 * Host Variable CLM-ADJ-GRP-CD
	 * 
	 */
	String getClmAdjGrpCd();

	void setClmAdjGrpCd(String clmAdjGrpCd);

	/**
	 * Host Variable CLM-ADJ-RSN-CD
	 * 
	 */
	String getClmAdjRsnCd();

	void setClmAdjRsnCd(String clmAdjRsnCd);

	/**
	 * Host Variable QNTY-CT
	 * 
	 */
	AfDecimal getQntyCt();

	void setQntyCt(AfDecimal qntyCt);

	/**
	 * Host Variable WS-EOB-AM
	 * 
	 */
	AfDecimal getWsEobAm();

	void setWsEobAm(AfDecimal wsEobAm);

	/**
	 * Host Variable WS-HIST-BUNDLE-IND
	 * 
	 */
	char getWsHistBundleInd();

	void setWsHistBundleInd(char wsHistBundleInd);
};
