/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: HOLD-RA-ADDRESS<br>
 * Variable: HOLD-RA-ADDRESS from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class HoldRaAddress {

	//==== PROPERTIES ====
	//Original name: HOLD-RA-ADDR1
	private String addr1 = DefaultValues.stringVal(Len.ADDR1);
	//Original name: HOLD-RA-ADDR2
	private String addr2 = DefaultValues.stringVal(Len.ADDR2);
	//Original name: HOLD-RA-ADDR3-CITY
	private String addr3City = DefaultValues.stringVal(Len.ADDR3_CITY);
	//Original name: HOLD-RA-ADDR3-ST
	private String addr3St = DefaultValues.stringVal(Len.ADDR3_ST);
	//Original name: HOLD-RA-ADDR3-ZIP
	private String addr3Zip = DefaultValues.stringVal(Len.ADDR3_ZIP);

	//==== METHODS ====
	public String getHoldRaAddressFormatted() {
		return MarshalByteExt.bufferToStr(getHoldRaAddressBytes());
	}

	public byte[] getHoldRaAddressBytes() {
		byte[] buffer = new byte[Len.HOLD_RA_ADDRESS];
		return getHoldRaAddressBytes(buffer, 1);
	}

	public byte[] getHoldRaAddressBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, addr1, Len.ADDR1);
		position += Len.ADDR1;
		MarshalByte.writeString(buffer, position, addr2, Len.ADDR2);
		position += Len.ADDR2;
		getAddr3Bytes(buffer, position);
		return buffer;
	}

	public void setAddr1(String addr1) {
		this.addr1 = Functions.subString(addr1, Len.ADDR1);
	}

	public String getAddr1() {
		return this.addr1;
	}

	public void setAddr2(String addr2) {
		this.addr2 = Functions.subString(addr2, Len.ADDR2);
	}

	public String getAddr2() {
		return this.addr2;
	}

	/**Original name: HOLD-RA-ADDR3<br>*/
	public byte[] getAddr3Bytes() {
		byte[] buffer = new byte[Len.ADDR3];
		return getAddr3Bytes(buffer, 1);
	}

	public byte[] getAddr3Bytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, addr3City, Len.ADDR3_CITY);
		position += Len.ADDR3_CITY;
		MarshalByte.writeString(buffer, position, addr3St, Len.ADDR3_ST);
		position += Len.ADDR3_ST;
		MarshalByte.writeString(buffer, position, addr3Zip, Len.ADDR3_ZIP);
		return buffer;
	}

	public void initAddr3Spaces() {
		addr3City = "";
		addr3St = "";
		addr3Zip = "";
	}

	public void setAddr3City(String addr3City) {
		this.addr3City = Functions.subString(addr3City, Len.ADDR3_CITY);
	}

	public String getAddr3City() {
		return this.addr3City;
	}

	public void setAddr3St(String addr3St) {
		this.addr3St = Functions.subString(addr3St, Len.ADDR3_ST);
	}

	public String getAddr3St() {
		return this.addr3St;
	}

	public void setAddr3Zip(String addr3Zip) {
		this.addr3Zip = Functions.subString(addr3Zip, Len.ADDR3_ZIP);
	}

	public String getAddr3Zip() {
		return this.addr3Zip;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ADDR1 = 25;
		public static final int ADDR2 = 25;
		public static final int ADDR3_CITY = 25;
		public static final int ADDR3_ST = 2;
		public static final int ADDR3_ZIP = 10;
		public static final int ADDR3 = ADDR3_CITY + ADDR3_ST + ADDR3_ZIP;
		public static final int HOLD_RA_ADDRESS = ADDR1 + ADDR2 + ADDR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
