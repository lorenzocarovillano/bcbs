/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.myorg.myprj.commons.data.to.IS02800caS02809saS02811saS02813sa;

/**Original name: VOID-INFORMATION<br>
 * Variable: VOID-INFORMATION from copybook NF05VOID<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class VoidInformation implements IS02800caS02809saS02811saS02813sa {

	//==== PROPERTIES ====
	//Original name: VOID-S02813-INFO
	private VoidS02813Info voidS02813Info = new VoidS02813Info();
	//Original name: VOID-S02809-LINE-INFO
	private VoidS02809LineInfo voidS02809LineInfo = new VoidS02809LineInfo();
	//Original name: VOID-S02811-CALC1-INFO
	private VoidS02811Calc1Info voidS02811Calc1Info = new VoidS02811Calc1Info();
	//Original name: VOID-S02811-CALC2-INFO
	private VoidS02811Calc1Info voidS02811Calc2Info = new VoidS02811Calc1Info();
	//Original name: VOID-S02811-CALC3-INFO
	private VoidS02811Calc1Info voidS02811Calc3Info = new VoidS02811Calc1Info();
	//Original name: VOID-S02811-CALC4-INFO
	private VoidS02811Calc1Info voidS02811Calc4Info = new VoidS02811Calc1Info();
	//Original name: VOID-S02811-CALC5-INFO
	private VoidS02811Calc1Info voidS02811Calc5Info = new VoidS02811Calc1Info();
	//Original name: VOID-S02811-CALC6-INFO
	private VoidS02811Calc1Info voidS02811Calc6Info = new VoidS02811Calc1Info();
	//Original name: VOID-S02811-CALC7-INFO
	private VoidS02811Calc1Info voidS02811Calc7Info = new VoidS02811Calc1Info();
	//Original name: VOID-S02811-CALC8-INFO
	private VoidS02811Calc1Info voidS02811Calc8Info = new VoidS02811Calc1Info();
	//Original name: VOID-S02811-CALC9-INFO
	private VoidS02811Calc1Info voidS02811Calc9Info = new VoidS02811Calc1Info();

	//==== METHODS ====
	@Override
	public char getC1CalcExplnCd() {
		return voidS02811Calc1Info.getC1CalcExplnCd();
	}

	@Override
	public void setC1CalcExplnCd(char c1CalcExplnCd) {
		this.voidS02811Calc1Info.setC1CalcExplnCd(c1CalcExplnCd);
	}

	@Override
	public char getC1CorpCd() {
		return voidS02811Calc1Info.getC1CorpCd();
	}

	@Override
	public void setC1CorpCd(char c1CorpCd) {
		this.voidS02811Calc1Info.setC1CorpCd(c1CorpCd);
	}

	@Override
	public String getC1GlAcctId() {
		return voidS02811Calc1Info.getC1GlAcctId();
	}

	@Override
	public void setC1GlAcctId(String c1GlAcctId) {
		this.voidS02811Calc1Info.setC1GlAcctId(c1GlAcctId);
	}

	@Override
	public AfDecimal getC1PdAm() {
		return voidS02811Calc1Info.getC1PdAm();
	}

	@Override
	public void setC1PdAm(AfDecimal c1PdAm) {
		this.voidS02811Calc1Info.setC1PdAm(c1PdAm.copy());
	}

	@Override
	public String getC1ProdCd() {
		return voidS02811Calc1Info.getC1ProdCd();
	}

	@Override
	public void setC1ProdCd(String c1ProdCd) {
		this.voidS02811Calc1Info.setC1ProdCd(c1ProdCd);
	}

	@Override
	public char getC2CalcExplnCd() {
		return voidS02811Calc2Info.getC1CalcExplnCd();
	}

	@Override
	public void setC2CalcExplnCd(char c2CalcExplnCd) {
		this.voidS02811Calc2Info.setC1CalcExplnCd(c2CalcExplnCd);
	}

	@Override
	public char getC2CorpCd() {
		return voidS02811Calc2Info.getC1CorpCd();
	}

	@Override
	public void setC2CorpCd(char c2CorpCd) {
		this.voidS02811Calc2Info.setC1CorpCd(c2CorpCd);
	}

	@Override
	public String getC2GlAcctId() {
		return voidS02811Calc2Info.getC1GlAcctId();
	}

	@Override
	public void setC2GlAcctId(String c2GlAcctId) {
		this.voidS02811Calc2Info.setC1GlAcctId(c2GlAcctId);
	}

	@Override
	public AfDecimal getC2PdAm() {
		return voidS02811Calc2Info.getC1PdAm();
	}

	@Override
	public void setC2PdAm(AfDecimal c2PdAm) {
		this.voidS02811Calc2Info.setC1PdAm(c2PdAm.copy());
	}

	@Override
	public String getC2ProdCd() {
		return voidS02811Calc2Info.getC1ProdCd();
	}

	@Override
	public void setC2ProdCd(String c2ProdCd) {
		this.voidS02811Calc2Info.setC1ProdCd(c2ProdCd);
	}

	@Override
	public char getC3CalcExplnCd() {
		return voidS02811Calc3Info.getC1CalcExplnCd();
	}

	@Override
	public void setC3CalcExplnCd(char c3CalcExplnCd) {
		this.voidS02811Calc3Info.setC1CalcExplnCd(c3CalcExplnCd);
	}

	@Override
	public char getC3CorpCd() {
		return voidS02811Calc3Info.getC1CorpCd();
	}

	@Override
	public void setC3CorpCd(char c3CorpCd) {
		this.voidS02811Calc3Info.setC1CorpCd(c3CorpCd);
	}

	@Override
	public String getC3GlAcctId() {
		return voidS02811Calc3Info.getC1GlAcctId();
	}

	@Override
	public void setC3GlAcctId(String c3GlAcctId) {
		this.voidS02811Calc3Info.setC1GlAcctId(c3GlAcctId);
	}

	@Override
	public AfDecimal getC3PdAm() {
		return voidS02811Calc3Info.getC1PdAm();
	}

	@Override
	public void setC3PdAm(AfDecimal c3PdAm) {
		this.voidS02811Calc3Info.setC1PdAm(c3PdAm.copy());
	}

	@Override
	public String getC3ProdCd() {
		return voidS02811Calc3Info.getC1ProdCd();
	}

	@Override
	public void setC3ProdCd(String c3ProdCd) {
		this.voidS02811Calc3Info.setC1ProdCd(c3ProdCd);
	}

	@Override
	public char getC4CalcExplnCd() {
		return voidS02811Calc4Info.getC1CalcExplnCd();
	}

	@Override
	public void setC4CalcExplnCd(char c4CalcExplnCd) {
		this.voidS02811Calc4Info.setC1CalcExplnCd(c4CalcExplnCd);
	}

	@Override
	public char getC4CorpCd() {
		return voidS02811Calc4Info.getC1CorpCd();
	}

	@Override
	public void setC4CorpCd(char c4CorpCd) {
		this.voidS02811Calc4Info.setC1CorpCd(c4CorpCd);
	}

	@Override
	public String getC4GlAcctId() {
		return voidS02811Calc4Info.getC1GlAcctId();
	}

	@Override
	public void setC4GlAcctId(String c4GlAcctId) {
		this.voidS02811Calc4Info.setC1GlAcctId(c4GlAcctId);
	}

	@Override
	public AfDecimal getC4PdAm() {
		return voidS02811Calc4Info.getC1PdAm();
	}

	@Override
	public void setC4PdAm(AfDecimal c4PdAm) {
		this.voidS02811Calc4Info.setC1PdAm(c4PdAm.copy());
	}

	@Override
	public String getC4ProdCd() {
		return voidS02811Calc4Info.getC1ProdCd();
	}

	@Override
	public void setC4ProdCd(String c4ProdCd) {
		this.voidS02811Calc4Info.setC1ProdCd(c4ProdCd);
	}

	@Override
	public char getC5CalcExplnCd() {
		return voidS02811Calc5Info.getC1CalcExplnCd();
	}

	@Override
	public void setC5CalcExplnCd(char c5CalcExplnCd) {
		this.voidS02811Calc5Info.setC1CalcExplnCd(c5CalcExplnCd);
	}

	@Override
	public char getC5CorpCd() {
		return voidS02811Calc5Info.getC1CorpCd();
	}

	@Override
	public void setC5CorpCd(char c5CorpCd) {
		this.voidS02811Calc5Info.setC1CorpCd(c5CorpCd);
	}

	@Override
	public String getC5GlAcctId() {
		return voidS02811Calc5Info.getC1GlAcctId();
	}

	@Override
	public void setC5GlAcctId(String c5GlAcctId) {
		this.voidS02811Calc5Info.setC1GlAcctId(c5GlAcctId);
	}

	@Override
	public AfDecimal getC5PdAm() {
		return voidS02811Calc5Info.getC1PdAm();
	}

	@Override
	public void setC5PdAm(AfDecimal c5PdAm) {
		this.voidS02811Calc5Info.setC1PdAm(c5PdAm.copy());
	}

	@Override
	public String getC5ProdCd() {
		return voidS02811Calc5Info.getC1ProdCd();
	}

	@Override
	public void setC5ProdCd(String c5ProdCd) {
		this.voidS02811Calc5Info.setC1ProdCd(c5ProdCd);
	}

	@Override
	public char getC6CalcExplnCd() {
		return voidS02811Calc6Info.getC1CalcExplnCd();
	}

	@Override
	public void setC6CalcExplnCd(char c6CalcExplnCd) {
		this.voidS02811Calc6Info.setC1CalcExplnCd(c6CalcExplnCd);
	}

	@Override
	public char getC6CorpCd() {
		return voidS02811Calc6Info.getC1CorpCd();
	}

	@Override
	public void setC6CorpCd(char c6CorpCd) {
		this.voidS02811Calc6Info.setC1CorpCd(c6CorpCd);
	}

	@Override
	public String getC6GlAcctId() {
		return voidS02811Calc6Info.getC1GlAcctId();
	}

	@Override
	public void setC6GlAcctId(String c6GlAcctId) {
		this.voidS02811Calc6Info.setC1GlAcctId(c6GlAcctId);
	}

	@Override
	public AfDecimal getC6PdAm() {
		return voidS02811Calc6Info.getC1PdAm();
	}

	@Override
	public void setC6PdAm(AfDecimal c6PdAm) {
		this.voidS02811Calc6Info.setC1PdAm(c6PdAm.copy());
	}

	@Override
	public String getC6ProdCd() {
		return voidS02811Calc6Info.getC1ProdCd();
	}

	@Override
	public void setC6ProdCd(String c6ProdCd) {
		this.voidS02811Calc6Info.setC1ProdCd(c6ProdCd);
	}

	@Override
	public char getC7CalcExplnCd() {
		return voidS02811Calc7Info.getC1CalcExplnCd();
	}

	@Override
	public void setC7CalcExplnCd(char c7CalcExplnCd) {
		this.voidS02811Calc7Info.setC1CalcExplnCd(c7CalcExplnCd);
	}

	@Override
	public char getC7CorpCd() {
		return voidS02811Calc7Info.getC1CorpCd();
	}

	@Override
	public void setC7CorpCd(char c7CorpCd) {
		this.voidS02811Calc7Info.setC1CorpCd(c7CorpCd);
	}

	@Override
	public String getC7GlAcctId() {
		return voidS02811Calc7Info.getC1GlAcctId();
	}

	@Override
	public void setC7GlAcctId(String c7GlAcctId) {
		this.voidS02811Calc7Info.setC1GlAcctId(c7GlAcctId);
	}

	@Override
	public AfDecimal getC7PdAm() {
		return voidS02811Calc7Info.getC1PdAm();
	}

	@Override
	public void setC7PdAm(AfDecimal c7PdAm) {
		this.voidS02811Calc7Info.setC1PdAm(c7PdAm.copy());
	}

	@Override
	public String getC7ProdCd() {
		return voidS02811Calc7Info.getC1ProdCd();
	}

	@Override
	public void setC7ProdCd(String c7ProdCd) {
		this.voidS02811Calc7Info.setC1ProdCd(c7ProdCd);
	}

	@Override
	public char getC8CalcExplnCd() {
		return voidS02811Calc8Info.getC1CalcExplnCd();
	}

	@Override
	public void setC8CalcExplnCd(char c8CalcExplnCd) {
		this.voidS02811Calc8Info.setC1CalcExplnCd(c8CalcExplnCd);
	}

	@Override
	public char getC8CorpCd() {
		return voidS02811Calc8Info.getC1CorpCd();
	}

	@Override
	public void setC8CorpCd(char c8CorpCd) {
		this.voidS02811Calc8Info.setC1CorpCd(c8CorpCd);
	}

	@Override
	public String getC8GlAcctId() {
		return voidS02811Calc8Info.getC1GlAcctId();
	}

	@Override
	public void setC8GlAcctId(String c8GlAcctId) {
		this.voidS02811Calc8Info.setC1GlAcctId(c8GlAcctId);
	}

	@Override
	public AfDecimal getC8PdAm() {
		return voidS02811Calc8Info.getC1PdAm();
	}

	@Override
	public void setC8PdAm(AfDecimal c8PdAm) {
		this.voidS02811Calc8Info.setC1PdAm(c8PdAm.copy());
	}

	@Override
	public String getC8ProdCd() {
		return voidS02811Calc8Info.getC1ProdCd();
	}

	@Override
	public void setC8ProdCd(String c8ProdCd) {
		this.voidS02811Calc8Info.setC1ProdCd(c8ProdCd);
	}

	@Override
	public char getC9CalcExplnCd() {
		return voidS02811Calc9Info.getC1CalcExplnCd();
	}

	@Override
	public void setC9CalcExplnCd(char c9CalcExplnCd) {
		this.voidS02811Calc9Info.setC1CalcExplnCd(c9CalcExplnCd);
	}

	@Override
	public char getC9CorpCd() {
		return voidS02811Calc9Info.getC1CorpCd();
	}

	@Override
	public void setC9CorpCd(char c9CorpCd) {
		this.voidS02811Calc9Info.setC1CorpCd(c9CorpCd);
	}

	@Override
	public String getC9GlAcctId() {
		return voidS02811Calc9Info.getC1GlAcctId();
	}

	@Override
	public void setC9GlAcctId(String c9GlAcctId) {
		this.voidS02811Calc9Info.setC1GlAcctId(c9GlAcctId);
	}

	@Override
	public AfDecimal getC9PdAm() {
		return voidS02811Calc9Info.getC1PdAm();
	}

	@Override
	public void setC9PdAm(AfDecimal c9PdAm) {
		this.voidS02811Calc9Info.setC1PdAm(c9PdAm.copy());
	}

	@Override
	public String getC9ProdCd() {
		return voidS02811Calc9Info.getC1ProdCd();
	}

	@Override
	public void setC9ProdCd(String c9ProdCd) {
		this.voidS02811Calc9Info.setC1ProdCd(c9ProdCd);
	}

	@Override
	public AfDecimal getCasEobCoinsAm() {
		return voidS02811Calc1Info.getAsEobCoinsAm();
	}

	@Override
	public void setCasEobCoinsAm(AfDecimal casEobCoinsAm) {
		this.voidS02811Calc1Info.setAsEobCoinsAm(casEobCoinsAm.copy());
	}

	@Override
	public AfDecimal getCasEobCopayAm() {
		return voidS02811Calc1Info.getAsEobCopayAm();
	}

	@Override
	public void setCasEobCopayAm(AfDecimal casEobCopayAm) {
		this.voidS02811Calc1Info.setAsEobCopayAm(casEobCopayAm.copy());
	}

	@Override
	public AfDecimal getCasEobDedAm() {
		return voidS02811Calc1Info.getAsEobDedAm();
	}

	@Override
	public void setCasEobDedAm(AfDecimal casEobDedAm) {
		this.voidS02811Calc1Info.setAsEobDedAm(casEobDedAm.copy());
	}

	@Override
	public short getCliId() {
		return voidS02809LineInfo.getCliId();
	}

	@Override
	public void setCliId(short cliId) {
		this.voidS02809LineInfo.setCliId(cliId);
	}

	@Override
	public String getClmCntlId() {
		return voidS02813Info.getClmCntlId();
	}

	@Override
	public void setClmCntlId(String clmCntlId) {
		this.voidS02813Info.setClmCntlId(clmCntlId);
	}

	@Override
	public char getClmCntlSfxId() {
		return voidS02813Info.getClmCntlSfxId();
	}

	@Override
	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.voidS02813Info.setClmCntlSfxId(clmCntlSfxId);
	}

	@Override
	public AfDecimal getClmPdAm() {
		return voidS02813Info.getClmPdAm();
	}

	@Override
	public void setClmPdAm(AfDecimal clmPdAm) {
		this.voidS02813Info.setClmPdAm(clmPdAm.copy());
	}

	@Override
	public short getClmPmtId() {
		return voidS02813Info.getClmPmtId();
	}

	@Override
	public void setClmPmtId(short clmPmtId) {
		this.voidS02813Info.setClmPmtId(clmPmtId);
	}

	@Override
	public AfDecimal getLiAlctOiPdAm() {
		return voidS02809LineInfo.getLiAlctOiPdAm();
	}

	@Override
	public void setLiAlctOiPdAm(AfDecimal liAlctOiPdAm) {
		this.voidS02809LineInfo.setLiAlctOiPdAm(liAlctOiPdAm.copy());
	}

	@Override
	public AfDecimal getLiChgAm() {
		return voidS02809LineInfo.getLiChgAm();
	}

	@Override
	public void setLiChgAm(AfDecimal liChgAm) {
		this.voidS02809LineInfo.setLiChgAm(liChgAm.copy());
	}

	@Override
	public String getLiDnlRmrkCd() {
		return voidS02809LineInfo.getLiDnlRmrkCd();
	}

	@Override
	public void setLiDnlRmrkCd(String liDnlRmrkCd) {
		this.voidS02809LineInfo.setLiDnlRmrkCd(liDnlRmrkCd);
	}

	@Override
	public char getLiExplnCd() {
		return voidS02809LineInfo.getLiExplnCd();
	}

	@Override
	public void setLiExplnCd(char liExplnCd) {
		this.voidS02809LineInfo.setLiExplnCd(liExplnCd);
	}

	@Override
	public AfDecimal getLiNcovAm() {
		return voidS02809LineInfo.getLiNcovAm();
	}

	@Override
	public void setLiNcovAm(AfDecimal liNcovAm) {
		this.voidS02809LineInfo.setLiNcovAm(liNcovAm.copy());
	}

	@Override
	public AfDecimal getLiPatRespAm() {
		return voidS02809LineInfo.getLiPatRespAm();
	}

	@Override
	public void setLiPatRespAm(AfDecimal liPatRespAm) {
		this.voidS02809LineInfo.setLiPatRespAm(liPatRespAm.copy());
	}

	@Override
	public AfDecimal getLiPwoAm() {
		return voidS02809LineInfo.getLiPwoAm();
	}

	@Override
	public void setLiPwoAm(AfDecimal liPwoAm) {
		this.voidS02809LineInfo.setLiPwoAm(liPwoAm.copy());
	}

	@Override
	public char getPmtAdjTypCd() {
		return voidS02813Info.getPmtAdjTypCd();
	}

	@Override
	public void setPmtAdjTypCd(char pmtAdjTypCd) {
		this.voidS02813Info.setPmtAdjTypCd(pmtAdjTypCd);
	}

	@Override
	public String getPmtEnrClCd() {
		return voidS02813Info.getPmtEnrClCd();
	}

	@Override
	public void setPmtEnrClCd(String pmtEnrClCd) {
		this.voidS02813Info.setPmtEnrClCd(pmtEnrClCd);
	}

	@Override
	public String getPmtFinCd() {
		return voidS02813Info.getPmtFinCd();
	}

	@Override
	public void setPmtFinCd(String pmtFinCd) {
		this.voidS02813Info.setPmtFinCd(pmtFinCd);
	}

	@Override
	public char getPmtHistLoadCd() {
		return voidS02813Info.getPmtHistLoadCd();
	}

	@Override
	public void setPmtHistLoadCd(char pmtHistLoadCd) {
		this.voidS02813Info.setPmtHistLoadCd(pmtHistLoadCd);
	}

	@Override
	public String getPmtKcapsTeamNm() {
		return voidS02813Info.getPmtKcapsTeamNm();
	}

	@Override
	public void setPmtKcapsTeamNm(String pmtKcapsTeamNm) {
		this.voidS02813Info.setPmtKcapsTeamNm(pmtKcapsTeamNm);
	}

	@Override
	public String getPmtKcapsUseId() {
		return voidS02813Info.getPmtKcapsUseId();
	}

	@Override
	public void setPmtKcapsUseId(String pmtKcapsUseId) {
		this.voidS02813Info.setPmtKcapsUseId(pmtKcapsUseId);
	}

	@Override
	public String getPmtNtwrkCd() {
		return voidS02813Info.getPmtNtwrkCd();
	}

	@Override
	public void setPmtNtwrkCd(String pmtNtwrkCd) {
		this.voidS02813Info.setPmtNtwrkCd(pmtNtwrkCd);
	}

	@Override
	public String getPmtPgmAreaCd() {
		return voidS02813Info.getPmtPgmAreaCd();
	}

	@Override
	public void setPmtPgmAreaCd(String pmtPgmAreaCd) {
		this.voidS02813Info.setPmtPgmAreaCd(pmtPgmAreaCd);
	}

	public VoidS02809LineInfo getVoidS02809LineInfo() {
		return voidS02809LineInfo;
	}

	public VoidS02811Calc1Info getVoidS02811Calc1Info() {
		return voidS02811Calc1Info;
	}

	public VoidS02811Calc1Info getVoidS02811Calc2Info() {
		return voidS02811Calc2Info;
	}

	public VoidS02811Calc1Info getVoidS02811Calc3Info() {
		return voidS02811Calc3Info;
	}

	public VoidS02811Calc1Info getVoidS02811Calc4Info() {
		return voidS02811Calc4Info;
	}

	public VoidS02811Calc1Info getVoidS02811Calc5Info() {
		return voidS02811Calc5Info;
	}

	public VoidS02811Calc1Info getVoidS02811Calc6Info() {
		return voidS02811Calc6Info;
	}

	public VoidS02811Calc1Info getVoidS02811Calc7Info() {
		return voidS02811Calc7Info;
	}

	public VoidS02811Calc1Info getVoidS02811Calc8Info() {
		return voidS02811Calc8Info;
	}

	public VoidS02811Calc1Info getVoidS02811Calc9Info() {
		return voidS02811Calc9Info;
	}

	public VoidS02813Info getVoidS02813Info() {
		return voidS02813Info;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 25;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
