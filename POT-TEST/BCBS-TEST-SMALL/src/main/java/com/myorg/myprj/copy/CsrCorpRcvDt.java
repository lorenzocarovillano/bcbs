/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CSR-CORP-RCV-DT<br>
 * Variable: CSR-CORP-RCV-DT from copybook NF07AREA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CsrCorpRcvDt {

	//==== PROPERTIES ====
	//Original name: CSR-CORP-RECEIVED-YEAR
	private String year = "";
	//Original name: FILLER-CSR-CORP-RCV-DT
	private char flr1 = Types.SPACE_CHAR;
	//Original name: CSR-CORP-RECEIVED-MONTH
	private String month = "";
	//Original name: FILLER-CSR-CORP-RCV-DT-1
	private char flr2 = Types.SPACE_CHAR;
	//Original name: CSR-CORP-RECEIVED-DAY
	private String day = "";

	//==== METHODS ====
	public void setCorpRcvDtFormatted(String data) {
		byte[] buffer = new byte[Len.CORP_RCV_DT];
		MarshalByte.writeString(buffer, 1, data, Len.CORP_RCV_DT);
		setCorpRcvDtBytes(buffer, 1);
	}

	public void setCorpRcvDtBytes(byte[] buffer, int offset) {
		int position = offset;
		year = MarshalByte.readString(buffer, position, Len.YEAR);
		position += Len.YEAR;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		month = MarshalByte.readString(buffer, position, Len.MONTH);
		position += Len.MONTH;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		day = MarshalByte.readString(buffer, position, Len.DAY);
	}

	public byte[] getCorpRcvDtBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, year, Len.YEAR);
		position += Len.YEAR;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, month, Len.MONTH);
		position += Len.MONTH;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, day, Len.DAY);
		return buffer;
	}

	public void initCorpRcvDtSpaces() {
		year = "";
		flr1 = Types.SPACE_CHAR;
		month = "";
		flr2 = Types.SPACE_CHAR;
		day = "";
	}

	public void setYear(String year) {
		this.year = Functions.subString(year, Len.YEAR);
	}

	public String getYear() {
		return this.year;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setMonth(String month) {
		this.month = Functions.subString(month, Len.MONTH);
	}

	public String getMonth() {
		return this.month;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setDay(String day) {
		this.day = Functions.subString(day, Len.DAY);
	}

	public String getDay() {
		return this.day;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int YEAR = 4;
		public static final int MONTH = 2;
		public static final int DAY = 2;
		public static final int FLR1 = 1;
		public static final int CORP_RCV_DT = YEAR + MONTH + DAY + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
