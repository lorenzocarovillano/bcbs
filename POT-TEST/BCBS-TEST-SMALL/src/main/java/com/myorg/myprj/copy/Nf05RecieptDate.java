/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: NF05-RECIEPT-DATE<br>
 * Variable: NF05-RECIEPT-DATE from copybook NF05EOB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Nf05RecieptDate {

	//==== PROPERTIES ====
	//Original name: NF05-RECIEPT-DATE-CC
	private String cc = DefaultValues.stringVal(Len.CC);
	//Original name: NF05-RECIEPT-DATE-YY
	private String yy = DefaultValues.stringVal(Len.YY);
	//Original name: FILLER-NF05-RECIEPT-DATE
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: NF05-RECIEPT-DATE-MM
	private String mm = DefaultValues.stringVal(Len.MM);
	//Original name: FILLER-NF05-RECIEPT-DATE-1
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: NF05-RECIEPT-DATE-DD
	private String dd = DefaultValues.stringVal(Len.DD);

	//==== METHODS ====
	public void setNf05RecieptDateFormatted(String data) {
		byte[] buffer = new byte[Len.NF05_RECIEPT_DATE];
		MarshalByte.writeString(buffer, 1, data, Len.NF05_RECIEPT_DATE);
		setNf05RecieptDateBytes(buffer, 1);
	}

	public void setNf05RecieptDateBytes(byte[] buffer, int offset) {
		int position = offset;
		cc = MarshalByte.readString(buffer, position, Len.CC);
		position += Len.CC;
		yy = MarshalByte.readString(buffer, position, Len.YY);
		position += Len.YY;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mm = MarshalByte.readString(buffer, position, Len.MM);
		position += Len.MM;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dd = MarshalByte.readString(buffer, position, Len.DD);
	}

	public byte[] getExpWrapDateCcyyMmDdBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cc, Len.CC);
		position += Len.CC;
		MarshalByte.writeString(buffer, position, yy, Len.YY);
		position += Len.YY;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position += Len.MM;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dd, Len.DD);
		return buffer;
	}

	public void setCc(String cc) {
		this.cc = Functions.subString(cc, Len.CC);
	}

	public String getCc() {
		return this.cc;
	}

	public void setYy(String yy) {
		this.yy = Functions.subString(yy, Len.YY);
	}

	public String getYy() {
		return this.yy;
	}

	public void setSep11(char sep11) {
		this.flr1 = sep11;
	}

	public char getSep11() {
		return this.flr1;
	}

	public void setMm(String mm) {
		this.mm = Functions.subString(mm, Len.MM);
	}

	public String getMm() {
		return this.mm;
	}

	public void setSep12(char sep12) {
		this.flr2 = sep12;
	}

	public char getSep12() {
		return this.flr2;
	}

	public void setDd(String dd) {
		this.dd = Functions.subString(dd, Len.DD);
	}

	public String getDd() {
		return this.dd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CC = 2;
		public static final int YY = 2;
		public static final int MM = 2;
		public static final int DD = 2;
		public static final int FLR1 = 1;
		public static final int NF05_RECIEPT_DATE = CC + YY + MM + DD + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
