/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

/**Original name: WS-ROLL-LOGIC-WORK<br>
 * Variable: WS-ROLL-LOGIC-WORK from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsRollLogicWork {

	//==== PROPERTIES ====
	//Original name: WS-LINE-ACCUMULATORS
	private WsLineAccumulators lineAccumulators = new WsLineAccumulators();
	//Original name: WS-CLAIM-ACCUMULATORS
	private WsClaimAccumulators claimAccumulators = new WsClaimAccumulators();

	//==== METHODS ====
	public WsClaimAccumulators getClaimAccumulators() {
		return claimAccumulators;
	}

	public WsLineAccumulators getLineAccumulators() {
		return lineAccumulators;
	}
}
