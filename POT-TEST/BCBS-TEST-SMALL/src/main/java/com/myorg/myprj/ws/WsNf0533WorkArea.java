/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-NF0533-WORK-AREA<br>
 * Variable: WS-NF0533-WORK-AREA from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsNf0533WorkArea {

	//==== PROPERTIES ====
	//Original name: WS-TRG-PMT-ID
	private short wsTrgPmtId = DefaultValues.SHORT_VAL;
	//Original name: NEW-KEY
	private NewKey newKey = new NewKey();
	//Original name: HOLD-KEY
	private HoldKey holdKey = new HoldKey();
	//Original name: HOLD-ASG-CD
	private String holdAsgCd = DefaultValues.stringVal(Len.HOLD_ASG_CD);
	//Original name: HOLD-ITS-CLM-TYP-CD
	private char holdItsClmTypCd = DefaultValues.CHAR_VAL;
	//Original name: HOLD-KCAPS-TEAM-NM
	private String holdKcapsTeamNm = DefaultValues.stringVal(Len.HOLD_KCAPS_TEAM_NM);
	//Original name: HOLD-KCAPS-USE-ID
	private String holdKcapsUseId = DefaultValues.stringVal(Len.HOLD_KCAPS_USE_ID);
	//Original name: HOLD-HIST-LOAD-CD
	private char holdHistLoadCd = DefaultValues.CHAR_VAL;
	//Original name: WS-MT-QT-INDICATOR
	private char wsMtQtIndicator = DefaultValues.CHAR_VAL;
	//Original name: HOLD-PGM-AREA-CD
	private String holdPgmAreaCd = DefaultValues.stringVal(Len.HOLD_PGM_AREA_CD);
	//Original name: HOLD-ENR-CL-CD
	private String holdEnrClCd = DefaultValues.stringVal(Len.HOLD_ENR_CL_CD);
	//Original name: HOLD-FIN-CD
	private String holdFinCd = DefaultValues.stringVal(Len.HOLD_FIN_CD);
	//Original name: HOLD-NTWRK-CD
	private String holdNtwrkCd = DefaultValues.stringVal(Len.HOLD_NTWRK_CD);
	//Original name: HOLD-INS-ID
	private String holdInsId = DefaultValues.stringVal(Len.HOLD_INS_ID);
	//Original name: HOLD-ALT-INS-ID
	private String holdAltInsId = DefaultValues.stringVal(Len.HOLD_ALT_INS_ID);
	//Original name: HOLD-PATIENT-NAME
	private String holdPatientName = DefaultValues.stringVal(Len.HOLD_PATIENT_NAME);
	//Original name: HOLD-BILLING-NAME
	private String holdBillingName = DefaultValues.stringVal(Len.HOLD_BILLING_NAME);
	//Original name: HOLD-PERFORMING-NAME
	private String holdPerformingName = DefaultValues.stringVal(Len.HOLD_PERFORMING_NAME);
	//Original name: HOLD-BI-PROV-ID
	private String holdBiProvId = DefaultValues.stringVal(Len.HOLD_BI_PROV_ID);
	//Original name: HOLD-BI-PROV-LOB
	private char holdBiProvLob = DefaultValues.CHAR_VAL;
	//Original name: HOLD-BI-PROV-NPI-NUM
	private String holdBiProvNpiNum = DefaultValues.stringVal(Len.HOLD_BI_PROV_NPI_NUM);
	//Original name: HOLD-BK-PROD-CD
	private char holdBkProdCd = DefaultValues.CHAR_VAL;
	//Original name: HOLD-TYP-GRP-CD
	private String holdTypGrpCd = DefaultValues.stringVal(Len.HOLD_TYP_GRP_CD);
	//Original name: HOLD-NPI-CD
	private String holdNpiCd = DefaultValues.stringVal(Len.HOLD_NPI_CD);
	//Original name: HOLD-MEMBER-ID
	private String holdMemberId = DefaultValues.stringVal(Len.HOLD_MEMBER_ID);
	//Original name: HOLD-CLM-SYST-VER-ID
	private String holdClmSystVerId = DefaultValues.stringVal(Len.HOLD_CLM_SYST_VER_ID);
	//Original name: HOLD-PA-ID-TO-ACUM-ID
	private String holdPaIdToAcumId = DefaultValues.stringVal(Len.HOLD_PA_ID_TO_ACUM_ID);
	//Original name: HOLD-MEM-ID-TO-ACUM-ID
	private String holdMemIdToAcumId = DefaultValues.stringVal(Len.HOLD_MEM_ID_TO_ACUM_ID);
	//Original name: HOLD-PROD-IND
	private String holdProdInd = DefaultValues.stringVal(Len.HOLD_PROD_IND);
	//Original name: HOLD-PROVIDER-NUM
	private String holdProviderNum = DefaultValues.stringVal(Len.HOLD_PROVIDER_NUM);
	//Original name: HOLD-PROVIDER-NAME
	private String holdProviderName = DefaultValues.stringVal(Len.HOLD_PROVIDER_NAME);
	//Original name: HOLD-PROVIDER-LOB
	private String holdProviderLob = DefaultValues.stringVal(Len.HOLD_PROVIDER_LOB);
	//Original name: HOLD-BI-PROVIDER-NUM
	private String holdBiProviderNum = DefaultValues.stringVal(Len.HOLD_BI_PROVIDER_NUM);
	//Original name: HOLD-BI-PROVIDER-NAME
	private String holdBiProviderName = DefaultValues.stringVal(Len.HOLD_BI_PROVIDER_NAME);
	//Original name: HOLD-BI-PROVIDER-LOB
	private String holdBiProviderLob = DefaultValues.stringVal(Len.HOLD_BI_PROVIDER_LOB);
	//Original name: HOLD-PE-PROVIDER-NAME
	private String holdPeProviderName = DefaultValues.stringVal(Len.HOLD_PE_PROVIDER_NAME);
	//Original name: HOLD-CLM-CORP-CODE
	private char holdClmCorpCode = DefaultValues.CHAR_VAL;
	//Original name: HOLD-RA-ADDRESS
	private HoldRaAddress holdRaAddress = new HoldRaAddress();
	//Original name: HOLD-PROVIDER-ADRESS-STUFF
	private HoldProviderAdressStuff holdProviderAdressStuff = new HoldProviderAdressStuff();
	//Original name: HOLD-CLM-INS-LN-CD
	private String holdClmInsLnCd = DefaultValues.stringVal(Len.HOLD_CLM_INS_LN_CD);
	//Original name: HOLD-VBR-IN
	private char holdVbrIn = 'N';
	//Original name: HOLD-ACR-PRMT-PA-INT-AM
	private AfDecimal holdAcrPrmtPaIntAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: HOLD-VOID-KEY
	private HoldKey holdVoidKey = new HoldKey();
	//Original name: WS-PRV-NAME
	private WsPrvName wsPrvName = new WsPrvName();
	//Original name: WS-INDUS-CD
	private String wsIndusCd = DefaultValues.stringVal(Len.WS_INDUS_CD);
	//Original name: WS-DB2-NCOV-AMT
	private AfDecimal wsDb2NcovAmt = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: WS-DB2-PAT-RESP
	private AfDecimal wsDb2PatResp = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: WS-DB2-PWO-AMT
	private AfDecimal wsDb2PwoAmt = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: PATIENT-NAME
	private String patientName = "";
	//Original name: PERFORMING-NAME
	private String performingName = "";
	//Original name: BILLING-NAME
	private String billingName = "";
	/**Original name: WS-DB2-BI-NPI-PROV-ID<br>
	 * <pre>*  THIS IS TO OBTAIN THE PROVIDER NPI IF ONE EXISTS</pre>*/
	private String wsDb2BiNpiProvId = DefaultValues.stringVal(Len.WS_DB2_BI_NPI_PROV_ID);

	//==== METHODS ====
	public void setWsTrgPmtId(short wsTrgPmtId) {
		this.wsTrgPmtId = wsTrgPmtId;
	}

	public short getWsTrgPmtId() {
		return this.wsTrgPmtId;
	}

	public void setHoldAsgCd(String holdAsgCd) {
		this.holdAsgCd = Functions.subString(holdAsgCd, Len.HOLD_ASG_CD);
	}

	public String getHoldAsgCd() {
		return this.holdAsgCd;
	}

	public void setHoldItsClmTypCd(char holdItsClmTypCd) {
		this.holdItsClmTypCd = holdItsClmTypCd;
	}

	public char getHoldItsClmTypCd() {
		return this.holdItsClmTypCd;
	}

	public void setHoldKcapsTeamNm(String holdKcapsTeamNm) {
		this.holdKcapsTeamNm = Functions.subString(holdKcapsTeamNm, Len.HOLD_KCAPS_TEAM_NM);
	}

	public String getHoldKcapsTeamNm() {
		return this.holdKcapsTeamNm;
	}

	public void setHoldKcapsUseId(String holdKcapsUseId) {
		this.holdKcapsUseId = Functions.subString(holdKcapsUseId, Len.HOLD_KCAPS_USE_ID);
	}

	public String getHoldKcapsUseId() {
		return this.holdKcapsUseId;
	}

	public void setHoldHistLoadCd(char holdHistLoadCd) {
		this.holdHistLoadCd = holdHistLoadCd;
	}

	public char getHoldHistLoadCd() {
		return this.holdHistLoadCd;
	}

	public void setWsMtQtIndicator(char wsMtQtIndicator) {
		this.wsMtQtIndicator = wsMtQtIndicator;
	}

	public void setWsMtQtIndicatorFormatted(String wsMtQtIndicator) {
		setWsMtQtIndicator(Functions.charAt(wsMtQtIndicator, Types.CHAR_SIZE));
	}

	public char getWsMtQtIndicator() {
		return this.wsMtQtIndicator;
	}

	public void setHoldPgmAreaCd(String holdPgmAreaCd) {
		this.holdPgmAreaCd = Functions.subString(holdPgmAreaCd, Len.HOLD_PGM_AREA_CD);
	}

	public String getHoldPgmAreaCd() {
		return this.holdPgmAreaCd;
	}

	public void setHoldEnrClCd(String holdEnrClCd) {
		this.holdEnrClCd = Functions.subString(holdEnrClCd, Len.HOLD_ENR_CL_CD);
	}

	public String getHoldEnrClCd() {
		return this.holdEnrClCd;
	}

	public void setHoldFinCd(String holdFinCd) {
		this.holdFinCd = Functions.subString(holdFinCd, Len.HOLD_FIN_CD);
	}

	public String getHoldFinCd() {
		return this.holdFinCd;
	}

	public String getHoldNtwrkCd() {
		return this.holdNtwrkCd;
	}

	public void setHoldInsId(String holdInsId) {
		this.holdInsId = Functions.subString(holdInsId, Len.HOLD_INS_ID);
	}

	public String getHoldInsId() {
		return this.holdInsId;
	}

	public void setHoldAltInsId(String holdAltInsId) {
		this.holdAltInsId = Functions.subString(holdAltInsId, Len.HOLD_ALT_INS_ID);
	}

	public String getHoldAltInsId() {
		return this.holdAltInsId;
	}

	public void setHoldPatientName(String holdPatientName) {
		this.holdPatientName = Functions.subString(holdPatientName, Len.HOLD_PATIENT_NAME);
	}

	public String getHoldPatientName() {
		return this.holdPatientName;
	}

	public String getHoldPatientNameFormatted() {
		return Functions.padBlanks(getHoldPatientName(), Len.HOLD_PATIENT_NAME);
	}

	public void setHoldBillingName(String holdBillingName) {
		this.holdBillingName = Functions.subString(holdBillingName, Len.HOLD_BILLING_NAME);
	}

	public String getHoldBillingName() {
		return this.holdBillingName;
	}

	public void setHoldPerformingName(String holdPerformingName) {
		this.holdPerformingName = Functions.subString(holdPerformingName, Len.HOLD_PERFORMING_NAME);
	}

	public String getHoldPerformingName() {
		return this.holdPerformingName;
	}

	public void setHoldBiProvId(String holdBiProvId) {
		this.holdBiProvId = Functions.subString(holdBiProvId, Len.HOLD_BI_PROV_ID);
	}

	public String getHoldBiProvId() {
		return this.holdBiProvId;
	}

	public void setHoldBiProvLob(char holdBiProvLob) {
		this.holdBiProvLob = holdBiProvLob;
	}

	public char getHoldBiProvLob() {
		return this.holdBiProvLob;
	}

	public void setHoldBiProvNpiNum(String holdBiProvNpiNum) {
		this.holdBiProvNpiNum = Functions.subString(holdBiProvNpiNum, Len.HOLD_BI_PROV_NPI_NUM);
	}

	public String getHoldBiProvNpiNum() {
		return this.holdBiProvNpiNum;
	}

	public void setHoldBkProdCd(char holdBkProdCd) {
		this.holdBkProdCd = holdBkProdCd;
	}

	public char getHoldBkProdCd() {
		return this.holdBkProdCd;
	}

	public void setHoldTypGrpCd(String holdTypGrpCd) {
		this.holdTypGrpCd = Functions.subString(holdTypGrpCd, Len.HOLD_TYP_GRP_CD);
	}

	public String getHoldTypGrpCd() {
		return this.holdTypGrpCd;
	}

	public void setHoldNpiCd(String holdNpiCd) {
		this.holdNpiCd = Functions.subString(holdNpiCd, Len.HOLD_NPI_CD);
	}

	public String getHoldNpiCd() {
		return this.holdNpiCd;
	}

	public void setHoldMemberId(String holdMemberId) {
		this.holdMemberId = Functions.subString(holdMemberId, Len.HOLD_MEMBER_ID);
	}

	public String getHoldMemberId() {
		return this.holdMemberId;
	}

	public void setHoldClmSystVerId(String holdClmSystVerId) {
		this.holdClmSystVerId = Functions.subString(holdClmSystVerId, Len.HOLD_CLM_SYST_VER_ID);
	}

	public String getHoldClmSystVerId() {
		return this.holdClmSystVerId;
	}

	public void setHoldPaIdToAcumId(String holdPaIdToAcumId) {
		this.holdPaIdToAcumId = Functions.subString(holdPaIdToAcumId, Len.HOLD_PA_ID_TO_ACUM_ID);
	}

	public String getHoldPaIdToAcumId() {
		return this.holdPaIdToAcumId;
	}

	public void setHoldMemIdToAcumId(String holdMemIdToAcumId) {
		this.holdMemIdToAcumId = Functions.subString(holdMemIdToAcumId, Len.HOLD_MEM_ID_TO_ACUM_ID);
	}

	public String getHoldMemIdToAcumId() {
		return this.holdMemIdToAcumId;
	}

	public void setHoldProdInd(String holdProdInd) {
		this.holdProdInd = Functions.subString(holdProdInd, Len.HOLD_PROD_IND);
	}

	public String getHoldProdInd() {
		return this.holdProdInd;
	}

	public void setHoldProviderNum(String holdProviderNum) {
		this.holdProviderNum = Functions.subString(holdProviderNum, Len.HOLD_PROVIDER_NUM);
	}

	public String getHoldProviderNum() {
		return this.holdProviderNum;
	}

	public void setHoldProviderName(String holdProviderName) {
		this.holdProviderName = Functions.subString(holdProviderName, Len.HOLD_PROVIDER_NAME);
	}

	public String getHoldProviderName() {
		return this.holdProviderName;
	}

	public void setHoldProviderLobFormatted(String holdProviderLob) {
		this.holdProviderLob = Trunc.toUnsignedNumeric(holdProviderLob, Len.HOLD_PROVIDER_LOB);
	}

	public short getHoldProviderLob() {
		return NumericDisplay.asShort(this.holdProviderLob);
	}

	public String getHoldProviderLobFormatted() {
		return this.holdProviderLob;
	}

	public void setHoldBiProviderNum(String holdBiProviderNum) {
		this.holdBiProviderNum = Functions.subString(holdBiProviderNum, Len.HOLD_BI_PROVIDER_NUM);
	}

	public String getHoldBiProviderNum() {
		return this.holdBiProviderNum;
	}

	public void setHoldBiProviderName(String holdBiProviderName) {
		this.holdBiProviderName = Functions.subString(holdBiProviderName, Len.HOLD_BI_PROVIDER_NAME);
	}

	public String getHoldBiProviderName() {
		return this.holdBiProviderName;
	}

	public String getHoldBiProviderNameFormatted() {
		return Functions.padBlanks(getHoldBiProviderName(), Len.HOLD_BI_PROVIDER_NAME);
	}

	public void setHoldBiProviderLobFormatted(String holdBiProviderLob) {
		this.holdBiProviderLob = Trunc.toUnsignedNumeric(holdBiProviderLob, Len.HOLD_BI_PROVIDER_LOB);
	}

	public short getHoldBiProviderLob() {
		return NumericDisplay.asShort(this.holdBiProviderLob);
	}

	public void setHoldPeProviderName(String holdPeProviderName) {
		this.holdPeProviderName = Functions.subString(holdPeProviderName, Len.HOLD_PE_PROVIDER_NAME);
	}

	public String getHoldPeProviderName() {
		return this.holdPeProviderName;
	}

	public String getHoldPeProviderNameFormatted() {
		return Functions.padBlanks(getHoldPeProviderName(), Len.HOLD_PE_PROVIDER_NAME);
	}

	public void setHoldClmCorpCode(char holdClmCorpCode) {
		this.holdClmCorpCode = holdClmCorpCode;
	}

	public char getHoldClmCorpCode() {
		return this.holdClmCorpCode;
	}

	public void setHoldClmInsLnCd(String holdClmInsLnCd) {
		this.holdClmInsLnCd = Functions.subString(holdClmInsLnCd, Len.HOLD_CLM_INS_LN_CD);
	}

	public String getHoldClmInsLnCd() {
		return this.holdClmInsLnCd;
	}

	public void setHoldVbrIn(char holdVbrIn) {
		this.holdVbrIn = holdVbrIn;
	}

	public void setHoldVbrInFormatted(String holdVbrIn) {
		setHoldVbrIn(Functions.charAt(holdVbrIn, Types.CHAR_SIZE));
	}

	public char getHoldVbrIn() {
		return this.holdVbrIn;
	}

	public void setHoldAcrPrmtPaIntAm(AfDecimal holdAcrPrmtPaIntAm) {
		this.holdAcrPrmtPaIntAm.assign(holdAcrPrmtPaIntAm);
	}

	public AfDecimal getHoldAcrPrmtPaIntAm() {
		return this.holdAcrPrmtPaIntAm.copy();
	}

	public void setWsIndusCd(String wsIndusCd) {
		this.wsIndusCd = Functions.subString(wsIndusCd, Len.WS_INDUS_CD);
	}

	public String getWsIndusCd() {
		return this.wsIndusCd;
	}

	public void setWsDb2NcovAmt(AfDecimal wsDb2NcovAmt) {
		this.wsDb2NcovAmt.assign(wsDb2NcovAmt);
	}

	public AfDecimal getWsDb2NcovAmt() {
		return this.wsDb2NcovAmt.copy();
	}

	public void setWsDb2PatResp(AfDecimal wsDb2PatResp) {
		this.wsDb2PatResp.assign(wsDb2PatResp);
	}

	public AfDecimal getWsDb2PatResp() {
		return this.wsDb2PatResp.copy();
	}

	public void setWsDb2PwoAmt(AfDecimal wsDb2PwoAmt) {
		this.wsDb2PwoAmt.assign(wsDb2PwoAmt);
	}

	public AfDecimal getWsDb2PwoAmt() {
		return this.wsDb2PwoAmt.copy();
	}

	public void setPatientName(String patientName) {
		this.patientName = Functions.subString(patientName, Len.PATIENT_NAME);
	}

	public String getPatientName() {
		return this.patientName;
	}

	public void setPerformingName(String performingName) {
		this.performingName = Functions.subString(performingName, Len.PERFORMING_NAME);
	}

	public String getPerformingName() {
		return this.performingName;
	}

	public void setBillingName(String billingName) {
		this.billingName = Functions.subString(billingName, Len.BILLING_NAME);
	}

	public String getBillingName() {
		return this.billingName;
	}

	public void setWsDb2BiNpiProvId(String wsDb2BiNpiProvId) {
		this.wsDb2BiNpiProvId = Functions.subString(wsDb2BiNpiProvId, Len.WS_DB2_BI_NPI_PROV_ID);
	}

	public String getWsDb2BiNpiProvId() {
		return this.wsDb2BiNpiProvId;
	}

	public HoldKey getHoldKey() {
		return holdKey;
	}

	public HoldProviderAdressStuff getHoldProviderAdressStuff() {
		return holdProviderAdressStuff;
	}

	public HoldRaAddress getHoldRaAddress() {
		return holdRaAddress;
	}

	public HoldKey getHoldVoidKey() {
		return holdVoidKey;
	}

	public NewKey getNewKey() {
		return newKey;
	}

	public WsPrvName getWsPrvName() {
		return wsPrvName;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_INDUS_CD = 30;
		public static final int PATIENT_NAME = 39;
		public static final int PERFORMING_NAME = 25;
		public static final int BILLING_NAME = 25;
		public static final int HOLD_ASG_CD = 2;
		public static final int HOLD_KCAPS_TEAM_NM = 5;
		public static final int HOLD_KCAPS_USE_ID = 3;
		public static final int HOLD_FNL_BUS_DT = 10;
		public static final int HOLD_PGM_AREA_CD = 3;
		public static final int HOLD_ENR_CL_CD = 3;
		public static final int HOLD_FIN_CD = 3;
		public static final int HOLD_NTWRK_CD = 3;
		public static final int HOLD_INS_ID = 12;
		public static final int HOLD_ALT_INS_ID = 12;
		public static final int HOLD_PATIENT_NAME = 39;
		public static final int HOLD_BILLING_NAME = 25;
		public static final int HOLD_PERFORMING_NAME = 25;
		public static final int HOLD_BI_PROV_ID = 10;
		public static final int HOLD_BI_PROV_NPI_NUM = 10;
		public static final int HOLD_TYP_GRP_CD = 3;
		public static final int HOLD_NPI_CD = 3;
		public static final int HOLD_MEMBER_ID = 14;
		public static final int HOLD_CLM_SYST_VER_ID = 6;
		public static final int HOLD_PA_ID_TO_ACUM_ID = 12;
		public static final int HOLD_MEM_ID_TO_ACUM_ID = 14;
		public static final int HOLD_PROD_IND = 3;
		public static final int HOLD_PROVIDER_NUM = 10;
		public static final int HOLD_PROVIDER_NAME = 40;
		public static final int HOLD_PERF_PROVIDER_NAME = 25;
		public static final int HOLD_PROVIDER_LOB = 1;
		public static final int HOLD_BI_PROVIDER_NUM = 10;
		public static final int HOLD_BI_PROVIDER_NAME = 40;
		public static final int HOLD_BI_PROVIDER_LOB = 1;
		public static final int HOLD_PE_PROVIDER_NUM = 10;
		public static final int HOLD_PE_PROVIDER_NAME = 25;
		public static final int HOLD_PE_PROVIDER_LOB = 1;
		public static final int HOLD_CN_ARNG_CD = 5;
		public static final int HOLD_CLM_INS_LN_CD = 3;
		public static final int WS_DB2_BI_NPI_PROV_ID = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
