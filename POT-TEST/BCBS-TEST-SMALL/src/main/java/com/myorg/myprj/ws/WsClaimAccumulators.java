/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WS-CLAIM-ACCUMULATORS<br>
 * Variable: WS-CLAIM-ACCUMULATORS from program NF0533ML<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class WsClaimAccumulators {

	//==== PROPERTIES ====
	//Original name: WS-CLAIM-ACCUM-OI-PAID
	private AfDecimal oiPaid = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: WS-CLAIM-ACCUM-PAID
	private AfDecimal paid = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: WS-CLAIM-ACCUM-ALLOWD
	private AfDecimal allowd = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: WS-CLAIM-ACCUM-CHARGE
	private AfDecimal charge = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: WS-CLAIM-ACCUM-WRITEOFF
	private AfDecimal writeoff = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: WS-CLAIM-ACCUM-NOT-COVD
	private AfDecimal notCovd = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: WS-CLAIM-ACCUM-PAT-OWES
	private AfDecimal patOwes = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: WS-CLAIM-ACCUM-DEDUCT
	private AfDecimal deduct = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: WS-CLAIM-ACCUM-COINS
	private AfDecimal coins = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);
	//Original name: WS-CLAIM-ACCUM-COPAY
	private AfDecimal copay = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);

	//==== METHODS ====
	public void setOiPaid(AfDecimal oiPaid) {
		this.oiPaid.assign(oiPaid);
	}

	public AfDecimal getOiPaid() {
		return this.oiPaid.copy();
	}

	public void setPaid(AfDecimal paid) {
		this.paid.assign(paid);
	}

	public AfDecimal getPaid() {
		return this.paid.copy();
	}

	public void setAllowd(AfDecimal allowd) {
		this.allowd.assign(allowd);
	}

	public AfDecimal getAllowd() {
		return this.allowd.copy();
	}

	public void setCharge(AfDecimal charge) {
		this.charge.assign(charge);
	}

	public AfDecimal getCharge() {
		return this.charge.copy();
	}

	public void setWriteoff(AfDecimal writeoff) {
		this.writeoff.assign(writeoff);
	}

	public AfDecimal getWriteoff() {
		return this.writeoff.copy();
	}

	public void setNotCovd(AfDecimal notCovd) {
		this.notCovd.assign(notCovd);
	}

	public AfDecimal getNotCovd() {
		return this.notCovd.copy();
	}

	public void setPatOwes(AfDecimal patOwes) {
		this.patOwes.assign(patOwes);
	}

	public AfDecimal getPatOwes() {
		return this.patOwes.copy();
	}

	public void setDeduct(AfDecimal deduct) {
		this.deduct.assign(deduct);
	}

	public AfDecimal getDeduct() {
		return this.deduct.copy();
	}

	public void setCoins(AfDecimal coins) {
		this.coins.assign(coins);
	}

	public AfDecimal getCoins() {
		return this.coins.copy();
	}

	public void setCopay(AfDecimal copay) {
		this.copay.assign(copay);
	}

	public AfDecimal getCopay() {
		return this.copay.copy();
	}
}
