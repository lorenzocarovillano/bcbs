/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS02815sa;

/**
 * Data Access Object(DAO) for table [S02815SA]
 * 
 */
public class S02815saDao extends BaseSqlDao<IS02815sa> {

	public S02815saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02815sa> getToClass() {
		return IS02815sa.class;
	}

	public String selectRec(String cntlId, char cntlSfxId, short pmtId, String dft) {
		return buildQuery("selectRec").bind("cntlId", cntlId).bind("cntlSfxId", String.valueOf(cntlSfxId)).bind("pmtId", pmtId)
				.scalarResultString(dft);
	}
}
