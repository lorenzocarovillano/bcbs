/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: FILLER-DATE-RECORD<br>
 * Variable: FILLER-DATE-RECORD from program NF0533ML<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class FillerDateRecord {

	//==== PROPERTIES ====
	//Original name: CURRENT-CC
	private String cc = DefaultValues.stringVal(Len.CC);
	//Original name: CURRENT-YY
	private String yy = DefaultValues.stringVal(Len.YY);
	//Original name: CURRENT-DASH1
	private char dash1 = DefaultValues.CHAR_VAL;
	//Original name: CURRENT-MM
	private String mm = DefaultValues.stringVal(Len.MM);
	//Original name: CURRENT-DASH2
	private char dash2 = DefaultValues.CHAR_VAL;
	//Original name: CURRENT-DD
	private String dd = DefaultValues.stringVal(Len.DD);

	//==== METHODS ====
	public String getCurrentDateFormatted() {
		int position = 1;
		return MarshalByte.readFixedString(getFillerDateRecordBytes(), position, Len.CURRENT_DATE);
	}

	/**Original name: FILLER-DATE-RECORD<br>*/
	public byte[] getFillerDateRecordBytes() {
		byte[] buffer = new byte[Len.FILLER_DATE_RECORD];
		return getFillerDateRecordBytes(buffer, 1);
	}

	public void setFillerDateRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		setCcyyBytes(buffer, position);
		position += Len.CCYY;
		dash1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mm = MarshalByte.readString(buffer, position, Len.MM);
		position += Len.MM;
		dash2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dd = MarshalByte.readString(buffer, position, Len.DD);
	}

	public byte[] getFillerDateRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		getCcyyBytes(buffer, position);
		position += Len.CCYY;
		MarshalByte.writeChar(buffer, position, dash1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position += Len.MM;
		MarshalByte.writeChar(buffer, position, dash2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dd, Len.DD);
		return buffer;
	}

	public String getCcyyFormatted() {
		return MarshalByteExt.bufferToStr(getCcyyBytes());
	}

	/**Original name: CURRENT-CCYY<br>*/
	public byte[] getCcyyBytes() {
		byte[] buffer = new byte[Len.CCYY];
		return getCcyyBytes(buffer, 1);
	}

	public void setCcyyBytes(byte[] buffer, int offset) {
		int position = offset;
		cc = MarshalByte.readString(buffer, position, Len.CC);
		position += Len.CC;
		yy = MarshalByte.readString(buffer, position, Len.YY);
	}

	public byte[] getCcyyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cc, Len.CC);
		position += Len.CC;
		MarshalByte.writeString(buffer, position, yy, Len.YY);
		return buffer;
	}

	public void setCc(String cc) {
		this.cc = Functions.subString(cc, Len.CC);
	}

	public String getCc() {
		return this.cc;
	}

	public void setYy(String yy) {
		this.yy = Functions.subString(yy, Len.YY);
	}

	public String getYy() {
		return this.yy;
	}

	public void setDash1(char dash1) {
		this.dash1 = dash1;
	}

	public char getDash1() {
		return this.dash1;
	}

	public void setMm(String mm) {
		this.mm = Functions.subString(mm, Len.MM);
	}

	public String getMm() {
		return this.mm;
	}

	public String getMmFormatted() {
		return Functions.padBlanks(getMm(), Len.MM);
	}

	public void setDash2(char dash2) {
		this.dash2 = dash2;
	}

	public char getDash2() {
		return this.dash2;
	}

	public void setDd(String dd) {
		this.dd = Functions.subString(dd, Len.DD);
	}

	public String getDd() {
		return this.dd;
	}

	public String getDdFormatted() {
		return Functions.padBlanks(getDd(), Len.DD);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CC = 2;
		public static final int YY = 2;
		public static final int CCYY = CC + YY;
		public static final int DASH1 = 1;
		public static final int MM = 2;
		public static final int DASH2 = 1;
		public static final int DD = 2;
		public static final int FILLER_DATE_RECORD = CCYY + DASH1 + MM + DASH2 + DD;
		public static final int CURRENT_DATE = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int CURRENT_DATE = 10;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
