/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLS02713SA<br>
 * Variable: DCLS02713SA from copybook S02713SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02713sa {

	//==== PROPERTIES ====
	//Original name: REF-ID
	private String refId = DefaultValues.stringVal(Len.REF_ID);

	//==== METHODS ====
	public void setRefId(String refId) {
		this.refId = Functions.subString(refId, Len.REF_ID);
	}

	public String getRefId() {
		return this.refId;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REF_ID = 50;
		public static final int CLM_CNTL_ID = 12;
		public static final int LOOP_ID = 6;
		public static final int SEG_ID = 3;
		public static final int REF_ID_QLF_CD = 3;
		public static final int REF041_RF_ID_QF_CD = 3;
		public static final int REF042_REF_ID = 50;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
