/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: ERR-LOCAL-BILL-PROV-INFO<br>
 * Variable: ERR-LOCAL-BILL-PROV-INFO from copybook NF07ERRS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrLocalBillProvInfo {

	//==== PROPERTIES ====
	//Original name: ERR-LOCAL-BILL-PROV-LOB-CD
	private char localBillProvLobCd = DefaultValues.CHAR_VAL;
	//Original name: FILLER-ERR-LOCAL-BILL-PROV-INFO
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: ERR-LOCAL-BILL-PROV-ID
	private String localBillProvId = DefaultValues.stringVal(Len.LOCAL_BILL_PROV_ID);
	//Original name: FILLER-ERR-LOCAL-BILL-PROV-INFO-1
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: ERR-BILL-PROV-ID-QLF-CD
	private String billProvIdQlfCd = DefaultValues.stringVal(Len.BILL_PROV_ID_QLF_CD);
	//Original name: FILLER-ERR-LOCAL-BILL-PROV-INFO-2
	private char flr3 = DefaultValues.CHAR_VAL;
	//Original name: ERR-BILL-PROV-SPEC-CD
	private String billProvSpecCd = DefaultValues.stringVal(Len.BILL_PROV_SPEC_CD);

	//==== METHODS ====
	public void setErrLocalBillProvInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		localBillProvLobCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		localBillProvId = MarshalByte.readString(buffer, position, Len.LOCAL_BILL_PROV_ID);
		position += Len.LOCAL_BILL_PROV_ID;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		billProvIdQlfCd = MarshalByte.readString(buffer, position, Len.BILL_PROV_ID_QLF_CD);
		position += Len.BILL_PROV_ID_QLF_CD;
		flr3 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		billProvSpecCd = MarshalByte.readString(buffer, position, Len.BILL_PROV_SPEC_CD);
	}

	public byte[] getErrLocalBillProvInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, localBillProvLobCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, localBillProvId, Len.LOCAL_BILL_PROV_ID);
		position += Len.LOCAL_BILL_PROV_ID;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, billProvIdQlfCd, Len.BILL_PROV_ID_QLF_CD);
		position += Len.BILL_PROV_ID_QLF_CD;
		MarshalByte.writeChar(buffer, position, flr3);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, billProvSpecCd, Len.BILL_PROV_SPEC_CD);
		return buffer;
	}

	public void initErrLocalBillProvInfoSpaces() {
		localBillProvLobCd = Types.SPACE_CHAR;
		flr1 = Types.SPACE_CHAR;
		localBillProvId = "";
		flr2 = Types.SPACE_CHAR;
		billProvIdQlfCd = "";
		flr3 = Types.SPACE_CHAR;
		billProvSpecCd = "";
	}

	public void setLocalBillProvLobCd(char localBillProvLobCd) {
		this.localBillProvLobCd = localBillProvLobCd;
	}

	public char getLocalBillProvLobCd() {
		return this.localBillProvLobCd;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setLocalBillProvId(String localBillProvId) {
		this.localBillProvId = Functions.subString(localBillProvId, Len.LOCAL_BILL_PROV_ID);
	}

	public String getLocalBillProvId() {
		return this.localBillProvId;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setBillProvIdQlfCd(String billProvIdQlfCd) {
		this.billProvIdQlfCd = Functions.subString(billProvIdQlfCd, Len.BILL_PROV_ID_QLF_CD);
	}

	public String getBillProvIdQlfCd() {
		return this.billProvIdQlfCd;
	}

	public void setFlr3(char flr3) {
		this.flr3 = flr3;
	}

	public char getFlr3() {
		return this.flr3;
	}

	public void setBillProvSpecCd(String billProvSpecCd) {
		this.billProvSpecCd = Functions.subString(billProvSpecCd, Len.BILL_PROV_SPEC_CD);
	}

	public String getBillProvSpecCd() {
		return this.billProvSpecCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LOCAL_BILL_PROV_LOB_CD = 1;
		public static final int FLR1 = 1;
		public static final int LOCAL_BILL_PROV_ID = 10;
		public static final int BILL_PROV_ID_QLF_CD = 3;
		public static final int BILL_PROV_SPEC_CD = 4;
		public static final int ERR_LOCAL_BILL_PROV_INFO = LOCAL_BILL_PROV_LOB_CD + LOCAL_BILL_PROV_ID + BILL_PROV_ID_QLF_CD + BILL_PROV_SPEC_CD
				+ 3 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
