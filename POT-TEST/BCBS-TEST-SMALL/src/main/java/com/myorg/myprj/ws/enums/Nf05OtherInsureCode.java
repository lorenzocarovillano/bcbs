/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: NF05-OTHER-INSURE-CODE<br>
 * Variable: NF05-OTHER-INSURE-CODE from copybook NF05EOB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Nf05OtherInsureCode {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char OTHER_INS_NA = ' ';
	public static final char OTHER_INS_AUTO = 'A';
	public static final char OTHER_INS_BLUE_BLUE = 'B';
	public static final char OTHR_INS_BL_BL_W_NTCVD = 'B';
	public static final char OTHER_INS_OTHER_CARR = 'C';
	public static final char OTHER_INS_COMM_WO = 'F';
	public static final char OTHER_INS_MER_ASSIGN = 'L';
	public static final char OTHER_INS_MER = 'M';
	public static final char OTHER_INS_MER_UNASSIGN = 'M';
	public static final char OTHER_INS_NONE = 'N';
	public static final char OTHER_INS_PLAN65 = 'P';
	public static final char OTHR_INS_P65_ASGN_PAYSUB = 'Q';
	public static final char OTHR_INS_POS_MSG5_MER = 'R';
	public static final char OTHER_INS_SUBROGATION = 'S';
	public static final char OTHER_INS_WORKMANS_COMP = 'W';
	public static final char OTHR_INS_POS_MSG5_OTHR = 'X';
	private static final char[] OTHER_INS_PAY_NO = new char[] { 'B', 'C', 'F' };

	//==== METHODS ====
	public void setNf05OtherInsureCode(char nf05OtherInsureCode) {
		this.value = nf05OtherInsureCode;
	}

	public char getNf05OtherInsureCode() {
		return this.value;
	}

	public boolean isOtherInsSubrogation() {
		return value == OTHER_INS_SUBROGATION;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
