/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-PRV-STOP-PAY-TABLE<br>
 * Variables: WS-PRV-STOP-PAY-TABLE from program NF0533ML<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WsPrvStopPayTable {

	//==== PROPERTIES ====
	//Original name: SRS-PROV-ID
	private String id = DefaultValues.stringVal(Len.ID);
	//Original name: SRS-PROV-LOB-CD
	private char lobCd = DefaultValues.CHAR_VAL;
	//Original name: SRS-PROV-CN-ARNG-EF-DT
	private String cnArngEfDt = DefaultValues.stringVal(Len.CN_ARNG_EF_DT);
	//Original name: SRS-PROV-CN-ARNG-TRM-DT
	private String cnArngTrmDt = DefaultValues.stringVal(Len.CN_ARNG_TRM_DT);

	//==== METHODS ====
	public void setId(String id) {
		this.id = Functions.subString(id, Len.ID);
	}

	public String getId() {
		return this.id;
	}

	public void setLobCd(char lobCd) {
		this.lobCd = lobCd;
	}

	public char getLobCd() {
		return this.lobCd;
	}

	public void setCnArngEfDt(String cnArngEfDt) {
		this.cnArngEfDt = Functions.subString(cnArngEfDt, Len.CN_ARNG_EF_DT);
	}

	public String getCnArngEfDt() {
		return this.cnArngEfDt;
	}

	public void setCnArngTrmDt(String cnArngTrmDt) {
		this.cnArngTrmDt = Functions.subString(cnArngTrmDt, Len.CN_ARNG_TRM_DT);
	}

	public String getCnArngTrmDt() {
		return this.cnArngTrmDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ID = 10;
		public static final int CN_ARNG_EF_DT = 10;
		public static final int CN_ARNG_TRM_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
