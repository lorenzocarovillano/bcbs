/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: EXP-EXAMINER-ACTION<br>
 * Variable: EXP-EXAMINER-ACTION from copybook NF06EXP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class ExpExaminerAction {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char VOID_FLD = 'V';
	public static final char CANCEL = 'C';
	public static final char DENIED = 'D';
	public static final char PAID = ' ';
	public static final char SUSPEND = 'S';
	public static final char MANUAL_PAY = 'M';
	public static final char SEMI_AUTO = 'X';

	//==== METHODS ====
	public void setExpExaminerAction(char expExaminerAction) {
		this.value = expExaminerAction;
	}

	public void setExpExaminerActionFormatted(String expExaminerAction) {
		setExpExaminerAction(Functions.charAt(expExaminerAction, Types.CHAR_SIZE));
	}

	public char getExpExaminerAction() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
