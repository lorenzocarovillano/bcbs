/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.data.fao;

import com.bphx.ctu.af.io.file.AbstractInputOutputDAO;
import com.bphx.ctu.af.io.file.FileAccessStatus;
import com.myorg.myprj.data.fto.BusinessDateTO;

/**Original name: BUSINESS-DATE/CYCLDATE/CYCLDATE[NF0533ML]<br>*/
public class BusinessDateDAO extends AbstractInputOutputDAO<BusinessDateTO> {

	//==== PROPERTIES ====
	public static final int RECORD_SIZE = 80;

	//==== CONSTRUCTORS ====
	public BusinessDateDAO(FileAccessStatus fileStatus) {
		super(fileStatus);
	}

	//==== METHODS ====
	@Override
	public String getName() {
		return "CYCLDATE";
	}

	@Override
	public int getRecordSize() {
		return RECORD_SIZE;
	}

	@Override
	public BusinessDateTO createTo() {
		return new BusinessDateTO();
	}

	@Override
	public BusinessDateTO read() {
		BusinessDateTO businessDateTO = new BusinessDateTO();
		return read(businessDateTO);
	}
}
