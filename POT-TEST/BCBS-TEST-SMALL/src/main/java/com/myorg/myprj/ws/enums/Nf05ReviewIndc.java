/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: NF05-REVIEW-INDC<br>
 * Variable: NF05-REVIEW-INDC from copybook NF05EOB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Nf05ReviewIndc {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char REVIEW_NA = ' ';
	public static final char REVIEW_COMMITTEE = 'A';
	public static final char SPCL_REVIEW_BY_UTIL = 'E';
	public static final char SPCL_REVIEW_BY_BS_UNIT = 'F';
	public static final char REVIEW_MGT_DECISION = 'G';

	//==== METHODS ====
	public void setNf05ReviewIndc(char nf05ReviewIndc) {
		this.value = nf05ReviewIndc;
	}

	public char getNf05ReviewIndc() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
