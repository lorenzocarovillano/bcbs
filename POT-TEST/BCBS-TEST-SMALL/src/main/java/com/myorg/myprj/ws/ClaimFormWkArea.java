/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: CLAIM-FORM-WK-AREA<br>
 * Variable: CLAIM-FORM-WK-AREA from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ClaimFormWkArea {

	//==== PROPERTIES ====
	//Original name: HOLD-CLM-FORM
	private String holdClmForm = DefaultValues.stringVal(Len.HOLD_CLM_FORM);

	//==== METHODS ====
	public String getHoldClmForm() {
		return this.holdClmForm;
	}

	public String getHoldClmFormFormatted() {
		return Functions.padBlanks(getHoldClmForm(), Len.HOLD_CLM_FORM);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int HOLD_CLM_FORM = 3;
		public static final int HOLD_E210_GRP_NO = 6;
		public static final int HOLD_E210_ACCT_NO = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
