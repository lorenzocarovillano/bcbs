/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WS-LINE-ACCUMULATORS<br>
 * Variable: WS-LINE-ACCUMULATORS from program NF0533ML<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class WsLineAccumulators {

	//==== PROPERTIES ====
	//Original name: WS-LINE-ACCUM-PAID
	private AfDecimal paid = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: WS-LINE-ACCUM-NOTCOVD
	private AfDecimal notcovd = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: WS-LINE-ACCUM-PAT-OWES
	private AfDecimal patOwes = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: WS-LINE-ACCUM-DEDUCTIBLE
	private AfDecimal deductible = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: WS-LINE-ACCUM-COINSURANCE
	private AfDecimal coinsurance = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: WS-LINE-ACCUM-COPAY
	private AfDecimal copay = new AfDecimal(DefaultValues.DEC_VAL, 7, 2);
	//Original name: WS-LINE-ACCUM-WRITEOFF
	private AfDecimal writeoff = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);

	//==== METHODS ====
	public void setPaid(AfDecimal paid) {
		this.paid.assign(paid);
	}

	public AfDecimal getPaid() {
		return this.paid.copy();
	}

	public void setNotcovd(AfDecimal notcovd) {
		this.notcovd.assign(notcovd);
	}

	public AfDecimal getNotcovd() {
		return this.notcovd.copy();
	}

	public void setPatOwes(AfDecimal patOwes) {
		this.patOwes.assign(patOwes);
	}

	public AfDecimal getPatOwes() {
		return this.patOwes.copy();
	}

	public void setDeductible(AfDecimal deductible) {
		this.deductible.assign(deductible);
	}

	public AfDecimal getDeductible() {
		return this.deductible.copy();
	}

	public void setCoinsurance(AfDecimal coinsurance) {
		this.coinsurance.assign(coinsurance);
	}

	public AfDecimal getCoinsurance() {
		return this.coinsurance.copy();
	}

	public void setCopay(AfDecimal copay) {
		this.copay.assign(copay);
	}

	public AfDecimal getCopay() {
		return this.copay.copy();
	}

	public void setWriteoff(AfDecimal writeoff) {
		this.writeoff.assign(writeoff);
	}

	public AfDecimal getWriteoff() {
		return this.writeoff.copy();
	}
}
