/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: FILLER-WS-DATE-FIELDS-1<br>
 * Variable: FILLER-WS-DATE-FIELDS-1 from program NF0533ML<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class FillerWsDateFields1 {

	//==== PROPERTIES ====
	//Original name: WS-BUSINESS-JULIAN-YEAR
	private String year = DefaultValues.stringVal(Len.YEAR);
	//Original name: WS-BUSINESS-JULIAN-DAY
	private String day = DefaultValues.stringVal(Len.DAY);

	//==== METHODS ====
	public String getWsBusinessJulianDateFormatted() {
		int position = 1;
		return MarshalByte.readFixedString(getFillerWsDateFields1Bytes(), position, Len.WS_BUSINESS_JULIAN_DATE);
	}

	public String getWsBusinessJulianDateAsString() {
		return getWsBusinessJulianDateFormatted();
	}

	/**Original name: FILLER-WS-DATE-FIELDS-1<br>*/
	public byte[] getFillerWsDateFields1Bytes() {
		byte[] buffer = new byte[Len.FILLER_WS_DATE_FIELDS1];
		return getFillerWsDateFields1Bytes(buffer, 1);
	}

	public byte[] getFillerWsDateFields1Bytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, year, Len.YEAR);
		position += Len.YEAR;
		MarshalByte.writeString(buffer, position, day, Len.DAY);
		return buffer;
	}

	public void setYear(String year) {
		this.year = Functions.subString(year, Len.YEAR);
	}

	public String getYear() {
		return this.year;
	}

	public void setDayFormatted(String day) {
		this.day = Trunc.toUnsignedNumeric(day, Len.DAY);
	}

	public short getDay() {
		return NumericDisplay.asShort(this.day);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int YEAR = 4;
		public static final int DAY = 3;
		public static final int FILLER_WS_DATE_FIELDS1 = YEAR + DAY;
		public static final int WS_BUSINESS_JULIAN_DATE = 7;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_BUSINESS_JULIAN_DATE = 7;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
