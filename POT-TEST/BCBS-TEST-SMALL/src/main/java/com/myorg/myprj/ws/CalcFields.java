/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.commons.data.to.IS02811saS02813sa;

/**Original name: CALC-FIELDS<br>
 * Variable: CALC-FIELDS from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CalcFields implements IS02811saS02813sa {

	//==== PROPERTIES ====
	//Original name: HOLD-CALC-TYP-CD
	private char holdCalcTypCd = Types.SPACE_CHAR;
	//Original name: HOLD-COV-LOB-CD
	private char holdCovLobCd = Types.SPACE_CHAR;
	//Original name: HOLD-CALC-EXPLN-CD
	private char holdCalcExplnCd = Types.SPACE_CHAR;
	//Original name: HOLD-PAY-CD
	private char holdPayCd = Types.SPACE_CHAR;
	//Original name: HOLD-ALW-CHG-AM
	private AfDecimal holdAlwChgAm = new AfDecimal(0, 11, 2);
	//Original name: HOLD-PD-AM
	private AfDecimal holdPdAm = new AfDecimal(0, 11, 2);
	//Original name: HOLD-GRP-ID
	private String holdGrpId = "";
	//Original name: HOLD-CORP-CD
	private char holdCorpCd = Types.SPACE_CHAR;
	//Original name: HOLD-PROD-CD
	private String holdProdCd = "";
	//Original name: HOLD-TYP-BEN-CD
	private String holdTypBenCd = "";
	//Original name: HOLD-SPECI-BEN-CD
	private String holdSpeciBenCd = "";
	//Original name: HOLD-GL-ACCT-ID
	private String holdGlAcctId = "";
	//Original name: HOLD-INS-TC-CD
	private String holdInsTcCd = "";
	//Original name: CALC-RECORD-KEY
	private CalcRecordKey calcRecordKey = new CalcRecordKey();
	//Original name: CALC-TYP-COUNTERS
	private CalcTypCounters calcTypCounters = new CalcTypCounters();

	//==== METHODS ====
	public void setHoldCalcTypCd(char holdCalcTypCd) {
		this.holdCalcTypCd = holdCalcTypCd;
	}

	public char getHoldCalcTypCd() {
		return this.holdCalcTypCd;
	}

	public void setHoldCovLobCd(char holdCovLobCd) {
		this.holdCovLobCd = holdCovLobCd;
	}

	public char getHoldCovLobCd() {
		return this.holdCovLobCd;
	}

	public void setHoldCalcExplnCd(char holdCalcExplnCd) {
		this.holdCalcExplnCd = holdCalcExplnCd;
	}

	public char getHoldCalcExplnCd() {
		return this.holdCalcExplnCd;
	}

	public void setHoldPayCd(char holdPayCd) {
		this.holdPayCd = holdPayCd;
	}

	public char getHoldPayCd() {
		return this.holdPayCd;
	}

	public void setHoldAlwChgAm(AfDecimal holdAlwChgAm) {
		this.holdAlwChgAm.assign(holdAlwChgAm);
	}

	public AfDecimal getHoldAlwChgAm() {
		return this.holdAlwChgAm.copy();
	}

	public void setHoldPdAm(AfDecimal holdPdAm) {
		this.holdPdAm.assign(holdPdAm);
	}

	public AfDecimal getHoldPdAm() {
		return this.holdPdAm.copy();
	}

	public void setHoldGrpId(String holdGrpId) {
		this.holdGrpId = Functions.subString(holdGrpId, Len.HOLD_GRP_ID);
	}

	public String getHoldGrpId() {
		return this.holdGrpId;
	}

	public void setHoldCorpCd(char holdCorpCd) {
		this.holdCorpCd = holdCorpCd;
	}

	public char getHoldCorpCd() {
		return this.holdCorpCd;
	}

	public void setHoldProdCd(String holdProdCd) {
		this.holdProdCd = Functions.subString(holdProdCd, Len.HOLD_PROD_CD);
	}

	public String getHoldProdCd() {
		return this.holdProdCd;
	}

	public void setHoldTypBenCd(String holdTypBenCd) {
		this.holdTypBenCd = Functions.subString(holdTypBenCd, Len.HOLD_TYP_BEN_CD);
	}

	public String getHoldTypBenCd() {
		return this.holdTypBenCd;
	}

	public void setHoldSpeciBenCd(String holdSpeciBenCd) {
		this.holdSpeciBenCd = Functions.subString(holdSpeciBenCd, Len.HOLD_SPECI_BEN_CD);
	}

	public String getHoldSpeciBenCd() {
		return this.holdSpeciBenCd;
	}

	public String getHoldSpeciBenCdFormatted() {
		return Functions.padBlanks(getHoldSpeciBenCd(), Len.HOLD_SPECI_BEN_CD);
	}

	public void setHoldGlAcctId(String holdGlAcctId) {
		this.holdGlAcctId = Functions.subString(holdGlAcctId, Len.HOLD_GL_ACCT_ID);
	}

	public String getHoldGlAcctId() {
		return this.holdGlAcctId;
	}

	public String getHoldGlAcctIdFormatted() {
		return Functions.padBlanks(getHoldGlAcctId(), Len.HOLD_GL_ACCT_ID);
	}

	public void setHoldInsTcCd(String holdInsTcCd) {
		this.holdInsTcCd = Functions.subString(holdInsTcCd, Len.HOLD_INS_TC_CD);
	}

	public String getHoldInsTcCd() {
		return this.holdInsTcCd;
	}

	@Override
	public AfDecimal getAlwChgAm() {
		return getHoldAlwChgAm();
	}

	@Override
	public void setAlwChgAm(AfDecimal alwChgAm) {
		this.setHoldAlwChgAm(alwChgAm.copy());
	}

	@Override
	public char getCalcExplnCd() {
		return getHoldCalcExplnCd();
	}

	@Override
	public void setCalcExplnCd(char calcExplnCd) {
		this.setHoldCalcExplnCd(calcExplnCd);
	}

	public CalcRecordKey getCalcRecordKey() {
		return calcRecordKey;
	}

	@Override
	public char getCalcTypCd() {
		return getHoldCalcTypCd();
	}

	@Override
	public void setCalcTypCd(char calcTypCd) {
		this.setHoldCalcTypCd(calcTypCd);
	}

	public CalcTypCounters getCalcTypCounters() {
		return calcTypCounters;
	}

	@Override
	public char getCorpCd() {
		return getHoldCorpCd();
	}

	@Override
	public void setCorpCd(char corpCd) {
		this.setHoldCorpCd(corpCd);
	}

	@Override
	public char getCovLobCd() {
		return getHoldCovLobCd();
	}

	@Override
	public void setCovLobCd(char covLobCd) {
		this.setHoldCovLobCd(covLobCd);
	}

	@Override
	public String getGlAcctId() {
		return getHoldGlAcctId();
	}

	@Override
	public void setGlAcctId(String glAcctId) {
		this.setHoldGlAcctId(glAcctId);
	}

	@Override
	public String getGrpId() {
		return getHoldGrpId();
	}

	@Override
	public void setGrpId(String grpId) {
		this.setHoldGrpId(grpId);
	}

	@Override
	public String getInsTcCd() {
		return getHoldInsTcCd();
	}

	@Override
	public void setInsTcCd(String insTcCd) {
		this.setHoldInsTcCd(insTcCd);
	}

	@Override
	public char getPayCd() {
		return getHoldPayCd();
	}

	@Override
	public void setPayCd(char payCd) {
		this.setHoldPayCd(payCd);
	}

	@Override
	public AfDecimal getPdAm() {
		return getHoldPdAm();
	}

	@Override
	public void setPdAm(AfDecimal pdAm) {
		this.setHoldPdAm(pdAm.copy());
	}

	@Override
	public String getProdCd() {
		return getHoldProdCd();
	}

	@Override
	public void setProdCd(String prodCd) {
		this.setHoldProdCd(prodCd);
	}

	@Override
	public String getSpeciBenCd() {
		return getHoldSpeciBenCd();
	}

	@Override
	public void setSpeciBenCd(String speciBenCd) {
		this.setHoldSpeciBenCd(speciBenCd);
	}

	@Override
	public String getTypBenCd() {
		return getHoldTypBenCd();
	}

	@Override
	public void setTypBenCd(String typBenCd) {
		this.setHoldTypBenCd(typBenCd);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HOLD_GRP_ID = 9;
		public static final int HOLD_PROD_CD = 2;
		public static final int HOLD_TYP_BEN_CD = 3;
		public static final int HOLD_SPECI_BEN_CD = 4;
		public static final int HOLD_GL_ACCT_ID = 8;
		public static final int HOLD_INS_TC_CD = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
