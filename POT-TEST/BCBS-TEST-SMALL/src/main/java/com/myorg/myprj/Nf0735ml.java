/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj;

import static java.lang.Math.abs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.DbService;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.io.file.FileAccessStatus;
import com.bphx.ctu.af.io.file.FileBufferedDAO;
import com.bphx.ctu.af.io.file.FileRecordBuffer;
import com.bphx.ctu.af.io.file.OpenMode;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.bphx.ctu.af.util.date.DateFunctions;
import com.bphx.ctu.af.util.display.DisplayUtil;
import com.modernsystems.batch.DdCard;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import com.myorg.myprj.commons.data.dao.S02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Dao;
import com.myorg.myprj.commons.data.dao.S02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Dao;
import com.myorg.myprj.commons.data.dao.S02717saDao;
import com.myorg.myprj.commons.data.dao.S02718saDao;
import com.myorg.myprj.commons.data.dao.S02719saDao;
import com.myorg.myprj.commons.data.dao.S02811saS02813saDao;
import com.myorg.myprj.commons.data.dao.S02952saDao;
import com.myorg.myprj.commons.data.dao.S03086saDao;
import com.myorg.myprj.commons.data.dao.Trigger2Dao;
import com.myorg.myprj.copy.CsrCalcInformation;
import com.myorg.myprj.copy.Sqlca;
import com.myorg.myprj.data.fao.JobParmsFileDAO;
import com.myorg.myprj.data.fto.JobParmsFileTO;
import com.myorg.myprj.ws.Nf0735mlData;

/**Original name: NF0735ML<br>
 * <pre>----------------------------------------------------------------
 *  07/29/2010
 *  THIS IS THE NEW PROGRAM THAT ADDS THE NEW 5010 DATA AND CREATES
 *  THE 3300 BYTE OUTPUT FILE WITH THE DEAD FIELDS REMOVED
 * ----------------------------------------------------------------
 * DATE-WRITTEN.   DECEMBER 2003.
 * AUTHOR.         CLAIMS TEAM.
 * *****************************************************************
 *  THIS VERSION HAS THE LOGIC TO FORCE THE SWITCH TO BE SET
 *  WITH THE EXISTENCE CHECK AND THE BLOCK FOR THE 5 TABLES.
 *   7/14/05.
 * *****************************************************************
 *   1100'S -  3086 INFO LOGIC
 *   1300'S -  READS
 *   2000'S -  MAIN PROCESSING
 *       2100'S - LINE PROCESSING
 *   6000'S -  DATABASE FETCHES
 *       6600'S - PROVIDER SUBROUTINE PROCESSING
 *   7000'S -  COMMIT     LOGIC
 *   8000'S -  DATE ROUTINE FUNCTIONS
 *   9000'S -  ERROR ROUTINES
 * *****************************************************************
 *                 M A I N T E N A N C E    L O G                  *
 *                                                                 *
 *  9/22/04   PROJ 29089 AUTO DEDUCT MGMT RPT        C298          *
 *    TWO NEW FIELDS WERE ADDED TO TA02813 THAT NEED TO BE PASSED  *
 *    DOWNLINE FOR REPORTING. (ADJUSTMENT RESP & ADJUSTMENT TRACK) *
 *                                                                 *
 *  10/2004   PROJ 28178 WEB REMITS - PHASE 2        C126          *
 *    RECOMPILED FOR NEW NU02PROV COPYBOOK.  REMOVED UNUSED        *
 *    COPYBOOKS NF07ERRS AND NF07MSGS.                             *
 *                                                                 *
 *  10/28/04  PAR #880. PUT A CASE STATEMENT IN THE VOID CURSOR    *
 *  SO THAT IF ANY LINE ON THE CLAIM HAD AN ADDNL-RMT-LN > 0 IT    *
 *  WOULD RETURN A 'Y' INTO     VOID-ADDNL-LN.       C298          *
 *  ALSO ADDED FIELD TO THE VOID RECORD FOR FUTURE USE.            *
 *                                                                 *
 *  1/11/05  CRIT MAINT CORRECT THE VOID LINE CURSOR. IT WAS       *
 *  BRINGING BACK A CARTESIAN PRODUCT FOR CERTAIN BUNDLE CLAIMS    *
 *                                                                 *
 *  4/XX/05   PROJ 28448 NPI PROJECT (PHASE 1)       C126          *
 *    GET NPI PROVIDER NUMBER FROM TABLE 2682 & PASS TO NF0736ML.  *
 *    GET THE NPI NUMBER FOR BOTH BILLING AND PERFORMING PROVIDER. *
 *    PUT THE 3 JOINS TO TABLE 2682 TOGETHER IN ONE BLOCK, DUE     *
 *    TO A LIMIT ON THE NUMBER OF JOINS YOU CAN HAVE IN A CURSOR.  *
 *    CLEANED UP SOME UNUSED WORKING STORAGE FIELDS & DISPLAYS.    *
 *                                                                 *
 *  04/01/06  PROJ 29371 GMIS HISTORY                C298          *
 *                                                                 *
 *  05/27/06  PROJ 29320 MULTI-CALC                  C126          *
 *    1. ADD LOGIC FOR A CALC-CURSOR TO GET INFO FROM TABLE 2811.  *
 *    2. ADD LOGIC TO ALLOW FOR THE FOLLOWING NUMBER OF ARC'S:
 *   (ADJUSTABLE RATE CODES)
 *           18 ARC'S FOR GROUP CODE CO, PER LINE
 *           24 ARC'S FOR GROUP CODE PR, PER LINE
 *           18 ARC'S FOR GROUP CODE OA, PER LINE
 *            6 ARC'S FOR GROUP CODE PI, PER LINE
 *           42 ARC'S FOR GROUP CODE CR, PER LINE
 *                                                                 *
 *  06/19/06 GMIS PROB LOGS: #6960, #6746, #6747. 'U' & 'I'        *
 *    COMBINATIONS, 'I' & 'M' COMBINATIONS, HISTORY REPLACEMENTS.  *
 *  10/14/06    PROJ 28448   NPI - STAGE 2.   C126
 *    1) ADD NPI BILLING & PERFORMING PROVIDER NUMBERS TO THE
 *       ORDER BY CLAUSE FOR CLAIM-LINE AND VOID-LINE CURSORS.
 *       THIS MUST BE KEPT IN SYNC WITH SORT CARD NF078034.
 *       NOTE (8/07):  THERE IS NO LONGER A NEED TO KEEP THE
 *       ORDER BY CLAUSE IN SYNC WITH SORT CARD NF078034.  THAT
 *       WAS NECESSARY BACK WHEN NF0735ML & NF0736ML WERE ONE
 *       PROGRAM.
 *    2) REMOVE LOGIC THAT PULLS NPI NUMBERS FROM TRANSACTION
 *       TABLES (TS02682).  LOGIC WAS MOVED TO NF0732ML.
 *    3) ADD LOGIC TO MOVE NPI NUMBERS (FROM HIST & TRANSACTIONS)
 *       TO THE OUTPUT FILE.
 *  4/15/07   PROJ 34684  ITS 9.4.   C298
 *     ADDED A SWITCH THAT NEEDS TO BE PASSED DOWN TO NF0737ML.
 *     IF ANOTHER PLAN PAID THE '57' CLAIM THEN WE WILL NOT UPDATE
 *     CHECK NUMBER/ CHECK DATE ON TA02813.
 *  8/11/07   PROJ 28448  NPI        C126
 *     DISPLAY NUMBER OF TRIGGER RECORDS LOADED, BEFORE OPENING
 *     THE CURSOR.  HELPFUL INFO WHEN CALLED FOR CRIT MAINT.
 *  4/10/08   PROJ 37087             C126
 *     ADDED DISPLAYS WHEN CLOSING CURSORS AND FILES.
 *  11/6/08                          C126
 *     MODIFIED THE DISPLAY OF THE NUMBER OF TRIGGER RECORDS
 *     LOADED TO CLARIFY THAT IT INCLUDES A HEADER RECORD.
 *  11/17/08    C126
 *              REMOVE SOME OF THE DISPLAYS FOR COMMITS, ETC.
 *  11/24/08    C126
 *              ADD SOME "PROGRESS" DISPLAYS AFTER THE CURSOR
 *              IS OPENED.
 *  04/17/10    B821
 *  THIS PROGRAM HAVE BEEN MODIFIED TO ACCOMODATE THE NEW FILE NAME
 *  CHANGES IN COPY MEMBER NF07835.  COPY MEMBERS NF07CLC AND
 *  NF07VLC HAVE BEEN REMOVED AND THE HOST VARIABLES ARE NOW THE
 *  ACTUAL FIELDS CREATED IN THE DCLGEN OF THE TABLES.
 *  THIS PROGRAM HAS ALSO BEEN MODIFIED AND JOINS REMOVED FROM BOTH
 *  CURSORS AND REPLACED WITH INDIVIDUAL DB2 SELECTS.
 *  THIS PROGRAM WILL PRODUCE THE NEW 5010 FORMAT FILE.
 *  06/27/13    C141 INSERT LOGIC FOR SPECI-BEN-CD AND TYPE-BEN-CD
 *  29121
 *  2013         S BUTLER #44110            INCREASE DIAG CODE
 *                                          LOOK FOR ICD10
 *  11/19/14   PRJ65296  ERICA BRINKMEYER (C140)
 *     ADDED CODE TO POPULATE THE VBR-IN FOR PCMH CLAIMS TO THE
 *     PAYMENTS AND VOIDS FILE.
 *  08/19/16  PR75182 LAURA WRIGHT (C002)
 *     ADDED CODE TO FILL NEW FIELD MEDICARE-XOVER ON THE
 *     NF07AREA COPYBOOK.
 *  06/02/17  PRJ76290 LAURA WRIGHT (C002)
 *     ADDED A LINE TO THE PAYMENT CURSOR WHERE CLAUSE WHICH
 *     RETRIEVES THE XREF INFORMATION FROM THE TA04315.  THE
 *     NEW LINE WILL BE 'AND XR2.ACT_INACT_CD = 'A''.
 *  05/21/18  PRJ76290 LAURA WRIGHT (C002)
 *     THE NF07AREA XREF AREA OCCURS CHANGED FROM 3 TO 6. THE FILE
 *     SIZE HAS INCREASED.
 *     CHANGES WERE ADDED TO THE CLAIM AND VOID CURSORS TO HANDLE
 *     THE NEW XREF PROCESSING.
 *     CHANGED PARA NAME 2158-MOVE-PTRS-ONLY TO 2158A-MOVE-PTRS-ONLY
 *     AND 2158B-MOVE-PTRS-ONLY TO HANDLE THE XREF RECORDS.
 *     CHANGE 2626-MOVE-XREF-ONLY TO 2626A-MOVE-XREF-ONLY
 *     AND 2626B-MOVE-XREF-ONLY TO HANDLE THE VOIDS.
 *     IN EACH PARA THE REASON CODES NOW INCLUDE I, M, AND L.
 *  01/01/19  PRJ81792 DDS
 *            FOR FEP BLUE FOCUS GL ACCOUNT NUMBERS NEED TO BE
 *            ASSIGNED PER GROUP NUMBER. LOGIC ADDED TO THE
 *            CALC_CURSOR AND APPLIED TO VSR-CALC-GL-ACCT-ID
 *            AND CSR-CALC-GL-ACCT-ID
 * *****************************************************************
 *  C140      BEACON PREREQ 26  - FEP CHANGES              -06/2020
 *            CHANGE TO USE NEW MARKETING PACKAGE CODE.
 *  A079      BEACON PREREQ 26  - FEP CHANGES               12/2020
 *            CORRECTIONS
 * *****************************************************************
 * LCAROVILLANO SOURCE-COMPUTER. IBM-3081.
 * *OURCE-COMPUTER. IBM-3081 WITH DEBUGGING MODE.
 * LCAROVILLANO SPECIAL-NAMES.
 * LCAROVILLANO     C01 IS TOP-OF-PAGE.</pre>*/
public class Nf0735ml extends BatchProgram {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private S02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Dao s02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Dao = new S02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Dao(
			dbAccessStatus);
	private S02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Dao s02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Dao = new S02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Dao(
			dbAccessStatus);
	private Trigger2Dao trigger2Dao = new Trigger2Dao(dbAccessStatus);
	private S03086saDao s03086saDao = new S03086saDao(dbAccessStatus);
	private S02811saS02813saDao s02811saS02813saDao = new S02811saS02813saDao(dbAccessStatus);
	private S02719saDao s02719saDao = new S02719saDao(dbAccessStatus);
	private S02718saDao s02718saDao = new S02718saDao(dbAccessStatus);
	private S02717saDao s02717saDao = new S02717saDao(dbAccessStatus);
	private S02952saDao s02952saDao = new S02952saDao(dbAccessStatus);
	public JobParmsFileTO jobParmsFileTO = new JobParmsFileTO();
	public JobParmsFileDAO jobParmsFileDAO = new JobParmsFileDAO(new FileAccessStatus());
	public FileRecordBuffer inputTriggerTo = new FileRecordBuffer(Len.INPUT_REC);
	public FileBufferedDAO inputTriggerDAO = new FileBufferedDAO(new FileAccessStatus(), "IN835TRG", Len.INPUT_REC);
	public FileRecordBuffer outputOrigTo = new FileRecordBuffer(Len.OUT_ORIG_REC);
	public FileBufferedDAO outputOrigDAO = new FileBufferedDAO(new FileAccessStatus(), "PAYMENTS", Len.OUT_ORIG_REC);
	public FileRecordBuffer outputVoidTo = new FileRecordBuffer(Len.OUT_VOID_REC);
	public FileBufferedDAO outputVoidDAO = new FileBufferedDAO(new FileAccessStatus(), "VOIDS", Len.OUT_VOID_REC);
	public FileRecordBuffer outputOrigGmisTo = new FileRecordBuffer(Len.OUT_ORIG_REC_GMIS);
	public FileBufferedDAO outputOrigGmisDAO = new FileBufferedDAO(new FileAccessStatus(), "GMISPAYS", Len.OUT_ORIG_REC_GMIS);
	public FileRecordBuffer outputVoidGmisTo = new FileRecordBuffer(Len.OUT_VOID_REC_GMIS);
	public FileBufferedDAO outputVoidGmisDAO = new FileBufferedDAO(new FileAccessStatus(), "GMISVOID", Len.OUT_VOID_REC_GMIS);
	//Original name: WORKING-STORAGE
	private Nf0735mlData ws = new Nf0735mlData();

	//==== METHODS ====
	/**Original name: 1000-START-PROGRAM<br>
	 * <pre>    DISPLAY '1000-START-PROGRAM'.</pre>*/
	public long execute() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN
		//                                               SAVE-DATE-TIME.
		ws.getWsDateFields().getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
		ws.getWsDateFields().getSaveDateTime().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
		// COB_CODE: MOVE GREGORN-HH   TO FORMAT-HH.
		ws.getFormattedTime().setFormatHhFormatted(ws.getWsDateFields().getGregorn().getHhFormatted());
		// COB_CODE: MOVE GREGORN-MI   TO FORMAT-MM.
		ws.getFormattedTime().setFormatMmFormatted(ws.getWsDateFields().getGregorn().getMiFormatted());
		// COB_CODE: MOVE GREGORN-SS   TO FORMAT-SS.
		ws.getFormattedTime().setFormatSsFormatted(ws.getWsDateFields().getGregorn().getSsFormatted());
		// COB_CODE: OPEN INPUT  JOB-PARMS-FILE
		//                       INPUT-TRIGGER.
		jobParmsFileDAO.open(OpenMode.READ, "Nf0735ml");
		inputTriggerDAO.open(OpenMode.READ, "Nf0735ml");
		// COB_CODE: OPEN OUTPUT OUTPUT-ORIG
		//                       OUTPUT-VOID
		//                       OUTPUT-ORIG-GMIS
		//                       OUTPUT-VOID-GMIS.
		outputOrigDAO.open(OpenMode.WRITE, "Nf0735ml");
		outputVoidDAO.open(OpenMode.WRITE, "Nf0735ml");
		outputOrigGmisDAO.open(OpenMode.WRITE, "Nf0735ml");
		outputVoidGmisDAO.open(OpenMode.WRITE, "Nf0735ml");
		// COB_CODE: PERFORM 1300-READ-JOB-PARMS.
		readJobParms();
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY 'JOB NAME: ' WS-JOB-NAME
		//                 '  JOB STEP: ' WS-JOB-STEP
		//                 '  JOB SEQ: '  WS-JOB-SEQ.
		DisplayUtil.sysout.write(new String[] { "JOB NAME: ", jobParmsFileTO.getNameFormatted(), "  JOB STEP: ", jobParmsFileTO.getStepFormatted(),
				"  JOB SEQ: ", jobParmsFileTO.getSeqAsString() });
		// COB_CODE: IF WS-JOB-NAME (1:2) = 'NF'
		//               MOVE 9999  TO DISPLAY-TIME-INTERVAL
		//           ELSE
		//               MOVE 99    TO DISPLAY-TIME-INTERVAL
		//           END-IF.
		if (Conditions.eq(jobParmsFileTO.getNameFormatted().substring((1) - 1, 2), "NF")) {
			// COB_CODE: MOVE 9999  TO DISPLAY-TIME-INTERVAL
			ws.getProgramCounters().setDisplayTimeInterval(9999);
		} else {
			// COB_CODE: MOVE 99    TO DISPLAY-TIME-INTERVAL
			ws.getProgramCounters().setDisplayTimeInterval(99);
		}
		// COB_CODE: MOVE WS-JOB-SEQ TO WS-JOB-SEQ-PACKED.
		ws.setWsJobSeqPacked(jobParmsFileTO.getSeq());
		// COB_CODE: PERFORM 1100-CHECK-FOR-RESTART.
		checkForRestart();
		// COB_CODE: PERFORM 1010-READ-TRIGGER-HEADER.
		readTriggerHeader();
		// COB_CODE: PERFORM 1011-DECLARE-TRIGGER-TBL.
		declareTriggerTbl();
		// COB_CODE: PERFORM 1012-HOUSEKEEPING.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=NF0735ML.COB:line=1783, because the code is unreachable.
		// COB_CODE: IF TRIGGER-EMPTY
		//               GOBACK
		//           ELSE
		//               DISPLAY ' '
		//           END-IF.
		if (ws.getProgramSwitches().isTriggerEmpty()) {
			// COB_CODE: PERFORM 1019-CLOSE-ROUTINE
			closeRoutine();
			// COB_CODE: DISPLAY '----------------------------------------'
			DisplayUtil.sysout.write("----------------------------------------");
			// COB_CODE: DISPLAY '            ' WS-PGM-NAME ' ENDING'
			DisplayUtil.sysout.write("            ", ws.getWsPgmNameFormatted(), " ENDING");
			// COB_CODE: DISPLAY '   TRIGGER FILE IS EMPTY. '
			DisplayUtil.sysout.write("   TRIGGER FILE IS EMPTY. ");
			// COB_CODE: DISPLAY '----------------------------------------'
			DisplayUtil.sysout.write("----------------------------------------");
			// COB_CODE: GOBACK
			throw new ReturnException();
		} else {
			// COB_CODE: PERFORM 1005-OPEN-CLAIM-CURSOR
			openClaimCursor();
			// COB_CODE: MOVE 0 TO FETCH-DONE-SW
			ws.getProgramSwitches().setFetchDoneSw(((short) 0));
			// COB_CODE: PERFORM 2155-FETCH-LOOP UNTIL FETCH-DONE
			while (!ws.getProgramSwitches().isFetchDone()) {
				fetchLoop();
			}
			// COB_CODE: MOVE ZEROES TO STATS-CNT
			ws.getProgramCounters().setStatsCnt(0);
			// COB_CODE: MOVE 0 TO CLAIM-CURSOR-EOF-SW
			ws.getProgramSwitches().setClaimCursorEofSw(((short) 0));
			// COB_CODE: PERFORM 2150-MAP-PAYMENT-RECORD
			//                   UNTIL CLAIM-CURSOR-EOF
			while (!ws.getProgramSwitches().isClaimCursorEof()) {
				mapPaymentRecord();
			}
			// COB_CODE: PERFORM 7100-SQL-COMMIT
			sqlCommit();
			// COB_CODE: MOVE ZEROES TO CHKP-UOW-CNTR
			ws.getProgramCounters().setChkpUowCntr(0);
			// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN
			ws.getWsDateFields().getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
			// COB_CODE: MOVE GREGORN-HH   TO FORMAT-HH
			ws.getFormattedTime().setFormatHhFormatted(ws.getWsDateFields().getGregorn().getHhFormatted());
			// COB_CODE: MOVE GREGORN-MI   TO FORMAT-MM
			ws.getFormattedTime().setFormatMmFormatted(ws.getWsDateFields().getGregorn().getMiFormatted());
			// COB_CODE: MOVE GREGORN-SS   TO FORMAT-SS
			ws.getFormattedTime().setFormatSsFormatted(ws.getWsDateFields().getGregorn().getSsFormatted());
			// COB_CODE: DISPLAY '   ORIGINAL FILE FINISHED AT     '
			//                                                   FORMATTED-TIME
			DisplayUtil.sysout.write("   ORIGINAL FILE FINISHED AT     ", ws.getFormattedTime().getFormattedTimeFormatted());
			// COB_CODE: EXEC SQL
			//               CLOSE CLAIM_LINE_CURSOR
			//           END-EXEC
			s02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Dao.closeClaimLineCursor();
			// COB_CODE: EVALUATE SQLCODE
			//             WHEN 0
			//                 DISPLAY '   CLAIM_LINE_CURSOR IS CLOSED'
			//             WHEN OTHER
			//               CALL 'ABEND'
			//           END-EVALUATE
			switch (sqlca.getSqlcode()) {

			case 0:// COB_CODE: DISPLAY '   CLAIM_LINE_CURSOR IS CLOSED'
				DisplayUtil.sysout.write("   CLAIM_LINE_CURSOR IS CLOSED");
				break;

			default:// COB_CODE: STRING WS-JOB-NAME, ' '
				//                , WS-JOB-STEP, ' '
				//                , WS-JOB-SEQ
				//                DELIMITED BY '-'    INTO DB2-ERR-KEY
				concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"),
						" ", Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
						Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
				ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
				// COB_CODE: MOVE 'S02800SA'            TO DB2-ERR-TABLE
				ws.setDb2ErrTable("S02800SA");
				// COB_CODE: MOVE 'CLOSE CLAIM CURSOR'  TO DB2-ERR-LAST-CALL
				ws.setDb2ErrLastCall("CLOSE CLAIM CURSOR");
				// COB_CODE: MOVE '1000-START-PROGRAM'  TO DB2-ERR-PARA
				ws.setDb2ErrPara("1000-START-PROGRAM");
				// COB_CODE: PERFORM 9950-ERROR-PARA
				errorPara();
				// COB_CODE: CALL 'ABEND'
				DynamicCall.invoke("ABEND");
				break;
			}
			//*
			// COB_CODE: PERFORM 1006-OPEN-VOID-CURSOR
			openVoidCursor();
			//*
			// COB_CODE: MOVE 'N' TO VOID-EOF-SW
			ws.getProgramSwitches().setVoidEofSwFormatted("N");
			// COB_CODE: MOVE 0   TO FETCH-VOID-DONE-SW
			ws.getProgramSwitches().setFetchVoidDoneSw(((short) 0));
			// COB_CODE: PERFORM 2156-FETCH-VOID-LOOP UNTIL FETCH-VOID-DONE
			while (!ws.getProgramSwitches().isFetchVoidDone()) {
				fetchVoidLoop();
			}
			// COB_CODE: MOVE 0 TO FETCH-VOID-DONE-SW
			ws.getProgramSwitches().setFetchVoidDoneSw(((short) 0));
			// COB_CODE: MOVE ZEROES TO STATS-CNT
			ws.getProgramCounters().setStatsCnt(0);
			// COB_CODE: PERFORM 2600-PROCESS-VOID-PMT
			//                   UNTIL VOID-EOF
			while (!ws.getProgramSwitches().isVoidEof()) {
				processVoidPmt();
			}
			// COB_CODE: PERFORM 7100-SQL-COMMIT
			sqlCommit();
			// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN
			ws.getWsDateFields().getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
			// COB_CODE: MOVE GREGORN-HH   TO FORMAT-HH
			ws.getFormattedTime().setFormatHhFormatted(ws.getWsDateFields().getGregorn().getHhFormatted());
			// COB_CODE: MOVE GREGORN-MI   TO FORMAT-MM
			ws.getFormattedTime().setFormatMmFormatted(ws.getWsDateFields().getGregorn().getMiFormatted());
			// COB_CODE: MOVE GREGORN-SS   TO FORMAT-SS
			ws.getFormattedTime().setFormatSsFormatted(ws.getWsDateFields().getGregorn().getSsFormatted());
			// COB_CODE: DISPLAY '   VOID FILE FINISHED AT         '
			//                                                   FORMATTED-TIME
			DisplayUtil.sysout.write("   VOID FILE FINISHED AT         ", ws.getFormattedTime().getFormattedTimeFormatted());
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
		}
		// COB_CODE: EXEC SQL
		//               CLOSE VOID_LINE_CURSOR
		//           END-EXEC.
		s02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Dao.closeVoidLineCursor();
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 DISPLAY ' '
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: DISPLAY '   VOID_LINE_CURSOR IS CLOSED'
			DisplayUtil.sysout.write("   VOID_LINE_CURSOR IS CLOSED");
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			break;

		default:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02800SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02800SA");
			// COB_CODE: MOVE 'CLOSE VOID CURSOR'   TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("CLOSE VOID CURSOR");
			// COB_CODE: MOVE '1000-START-PROGRAM'  TO DB2-ERR-PARA
			ws.setDb2ErrPara("1000-START-PROGRAM");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
		// COB_CODE: PERFORM 1019-CLOSE-ROUTINE.
		closeRoutine();
		// COB_CODE: GOBACK.
		//last return statement was skipped
		return 0;
	}

	public static Nf0735ml getInstance() {
		return ((Nf0735ml) Programs.getInstance(Nf0735ml.class));
	}

	/**Original name: 1005-OPEN-CLAIM-CURSOR<br>
	 * <pre>    DISPLAY '1005-OPEN-CLAIM-CURSOR'.</pre>*/
	private void openClaimCursor() {
		ConcatUtil concatUtil = null;
		GenericParam abendCode = null;
		// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN.
		ws.getWsDateFields().getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
		// COB_CODE: MOVE GREGORN-HH   TO FORMAT-HH.
		ws.getFormattedTime().setFormatHhFormatted(ws.getWsDateFields().getGregorn().getHhFormatted());
		// COB_CODE: MOVE GREGORN-MI   TO FORMAT-MM.
		ws.getFormattedTime().setFormatMmFormatted(ws.getWsDateFields().getGregorn().getMiFormatted());
		// COB_CODE: MOVE GREGORN-SS   TO FORMAT-SS.
		ws.getFormattedTime().setFormatSsFormatted(ws.getWsDateFields().getGregorn().getSsFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '   OPENING THE CLAIM CURSOR AT ' FORMATTED-TIME
		//                   '     PLEASE WAIT . . . .'.
		DisplayUtil.sysout.write("   OPENING THE CLAIM CURSOR AT ", ws.getFormattedTime().getFormattedTimeFormatted(), "     PLEASE WAIT . . . .");
		// COB_CODE: EXEC SQL
		//              OPEN CLAIM_LINE_CURSOR
		//           END-EXEC.
		s02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Dao.openClaimLineCursor();
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 CONTINUE
		//              WHEN -911
		//                  CALL 'ABEND' USING ABEND-CODE
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		case -911:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02801SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02801SA");
			// COB_CODE: MOVE 'OPENING ORIG CURSOR ' TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("OPENING ORIG CURSOR ");
			// COB_CODE: MOVE '1005-OPEN-CLAIM-CURSOR' TO DB2-ERR-PARA
			ws.setDb2ErrPara("1005-OPEN-CLAIM-CURSOR");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: PERFORM 1019-CLOSE-ROUTINE
			closeRoutine();
			// COB_CODE: MOVE +911 TO RETURN-CODE
			Session.setReturnCode(911);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;

		default:// COB_CODE: PERFORM 9940-DISPLAY-SQLCODE
			displaySqlcode();
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'       INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02800SA'               TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02800SA");
			// COB_CODE: MOVE 'OPEN CLAIM CURSOR'      TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("OPEN CLAIM CURSOR");
			// COB_CODE: MOVE '1005-OPEN-CLAIM-CURSOR' TO DB2-ERR-PARA
			ws.setDb2ErrPara("1005-OPEN-CLAIM-CURSOR");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
		// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN.
		ws.getWsDateFields().getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
		// COB_CODE: MOVE GREGORN-HH   TO FORMAT-HH.
		ws.getFormattedTime().setFormatHhFormatted(ws.getWsDateFields().getGregorn().getHhFormatted());
		// COB_CODE: MOVE GREGORN-MI   TO FORMAT-MM.
		ws.getFormattedTime().setFormatMmFormatted(ws.getWsDateFields().getGregorn().getMiFormatted());
		// COB_CODE: MOVE GREGORN-SS   TO FORMAT-SS.
		ws.getFormattedTime().setFormatSsFormatted(ws.getWsDateFields().getGregorn().getSsFormatted());
		// COB_CODE: DISPLAY '   CLAIM CURSOR IS OPEN        ' FORMATTED-TIME.
		DisplayUtil.sysout.write("   CLAIM CURSOR IS OPEN        ", ws.getFormattedTime().getFormattedTimeFormatted());
		// COB_CODE: PERFORM 7100-SQL-COMMIT.
		sqlCommit();
		// COB_CODE: MOVE ZEROES TO CHKP-UOW-CNTR.
		ws.getProgramCounters().setChkpUowCntr(0);
	}

	/**Original name: 1006-OPEN-VOID-CURSOR<br>
	 * <pre>---------------------------------------------------------------*
	 *     DISPLAY '1006-OPEN-VOID-CURSOR'.</pre>*/
	private void openVoidCursor() {
		ConcatUtil concatUtil = null;
		GenericParam abendCode = null;
		// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN.
		ws.getWsDateFields().getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
		// COB_CODE: MOVE GREGORN-HH   TO FORMAT-HH.
		ws.getFormattedTime().setFormatHhFormatted(ws.getWsDateFields().getGregorn().getHhFormatted());
		// COB_CODE: MOVE GREGORN-MI   TO FORMAT-MM.
		ws.getFormattedTime().setFormatMmFormatted(ws.getWsDateFields().getGregorn().getMiFormatted());
		// COB_CODE: MOVE GREGORN-SS   TO FORMAT-SS.
		ws.getFormattedTime().setFormatSsFormatted(ws.getWsDateFields().getGregorn().getSsFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '   OPENING THE VOID CURSOR AT    ' FORMATTED-TIME
		//                   '     PLEASE WAIT . . . .'.
		DisplayUtil.sysout.write("   OPENING THE VOID CURSOR AT    ", ws.getFormattedTime().getFormattedTimeFormatted(), "     PLEASE WAIT . . . .");
		// COB_CODE: EXEC SQL
		//              OPEN VOID_LINE_CURSOR
		//           END-EXEC.
		s02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Dao.openVoidLineCursor();
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 CONTINUE
		//             WHEN -911
		//                  CALL 'ABEND' USING ABEND-CODE
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		case -911:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02801SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02801SA");
			// COB_CODE: MOVE 'OPENING VOID CURSOR ' TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("OPENING VOID CURSOR ");
			// COB_CODE: MOVE '1006-OPEN-VOID-CURSOR ' TO DB2-ERR-PARA
			ws.setDb2ErrPara("1006-OPEN-VOID-CURSOR ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: PERFORM 1019-CLOSE-ROUTINE
			closeRoutine();
			// COB_CODE: MOVE +911 TO RETURN-CODE
			Session.setReturnCode(911);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;

		default:// COB_CODE: PERFORM 9940-DISPLAY-SQLCODE
			displaySqlcode();
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'   INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02800SA'           TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02800SA");
			// COB_CODE: MOVE 'OPEN VOID CURSOR'   TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("OPEN VOID CURSOR");
			// COB_CODE: MOVE '1000-START-PROGRAM' TO DB2-ERR-PARA
			ws.setDb2ErrPara("1000-START-PROGRAM");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
		// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN.
		ws.getWsDateFields().getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
		// COB_CODE: MOVE GREGORN-HH   TO FORMAT-HH.
		ws.getFormattedTime().setFormatHhFormatted(ws.getWsDateFields().getGregorn().getHhFormatted());
		// COB_CODE: MOVE GREGORN-MI   TO FORMAT-MM.
		ws.getFormattedTime().setFormatMmFormatted(ws.getWsDateFields().getGregorn().getMiFormatted());
		// COB_CODE: MOVE GREGORN-SS   TO FORMAT-SS.
		ws.getFormattedTime().setFormatSsFormatted(ws.getWsDateFields().getGregorn().getSsFormatted());
		// COB_CODE: DISPLAY '   VOID CURSOR IS OPEN           ' FORMATTED-TIME.
		DisplayUtil.sysout.write("   VOID CURSOR IS OPEN           ", ws.getFormattedTime().getFormattedTimeFormatted());
		// COB_CODE: PERFORM 7100-SQL-COMMIT.
		sqlCommit();
		// COB_CODE: MOVE ZEROES TO CHKP-UOW-CNTR.
		ws.getProgramCounters().setChkpUowCntr(0);
	}

	/**Original name: 1010-READ-TRIGGER-HEADER<br>
	 * <pre>---------------------------------------------------------------*
	 *     DISPLAY ' *1010-READ-TRIGGER-HEADER* '.
	 * ******************************************************
	 *   THIS PERFORMANCE SHOULD RETRIEVE THE SINGLE HEADER *
	 *   RECORD ON THE TRIGGER FILE.                        *
	 * ******************************************************</pre>*/
	private void readTriggerHeader() {
		GenericParam abendCode = null;
		// COB_CODE: PERFORM 1305-READ-INSERT-TRIGGER.
		readInsertTrigger();
		// COB_CODE: IF INTG-EOF
		//              CALL 'ABEND' USING ABEND-CODE
		//           END-IF.
		if (ws.getProgramSwitches().isIntgEof()) {
			// COB_CODE: DISPLAY '*********************************'
			DisplayUtil.sysout.write("*********************************");
			// COB_CODE: DISPLAY '      ' WS-PGM-NAME ' ABENDING'
			DisplayUtil.sysout.write("      ", ws.getWsPgmNameFormatted(), " ABENDING");
			// COB_CODE: DISPLAY '    TRIGGER FILE IS EMPTY     '
			DisplayUtil.sysout.write("    TRIGGER FILE IS EMPTY     ");
			// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: MOVE +102   TO ABEND-CODE
			ws.setAbendCode(102);
			// COB_CODE: MOVE +102   TO RETURN-CODE
			Session.setReturnCode(102);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
		}
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '***** PIP PARMS DATA ************'.
		DisplayUtil.sysout.write("***** PIP PARMS DATA ************");
		// COB_CODE: DISPLAY '** TRIG-BUS-DATE = ' TRIG-BUS-DATE.
		DisplayUtil.sysout.write("** TRIG-BUS-DATE = ", ws.getTrigRecordLayout().getTrigBusDateFormatted());
		// COB_CODE: DISPLAY '** TRIG-PIP-FLAG = ' TRIG-PIP-FLAG.
		DisplayUtil.sysout.write("** TRIG-PIP-FLAG = ", String.valueOf(ws.getTrigRecordLayout().getTrigPipFlag()));
		// COB_CODE: DISPLAY '** TRIG-EOM-FLAG = ' TRIG-EOM-FLAG.
		DisplayUtil.sysout.write("** TRIG-EOM-FLAG = ", String.valueOf(ws.getTrigRecordLayout().getTrigEomFlag()));
		// COB_CODE: DISPLAY '*********************************'.
		DisplayUtil.sysout.write("*********************************");
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: IF TRIG-EOM-FLAG = ' '
		//           OR TRIG-PIP-FLAG = ' '
		//              CALL 'ABEND' USING ABEND-CODE
		//           END-IF.
		if (ws.getTrigRecordLayout().getTrigEomFlag() == ' ' || ws.getTrigRecordLayout().getTrigPipFlag() == ' ') {
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: DISPLAY '* ONE OF THE FLAGS WAS BLANK  *'
			DisplayUtil.sysout.write("* ONE OF THE FLAGS WAS BLANK  *");
			// COB_CODE: DISPLAY '* FIX FILE AND RESTART JOB    *'
			DisplayUtil.sysout.write("* FIX FILE AND RESTART JOB    *");
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: MOVE +103   TO ABEND-CODE
			ws.setAbendCode(103);
			// COB_CODE: MOVE +103   TO RETURN-CODE
			Session.setReturnCode(103);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
		}
		// COB_CODE: MOVE TRIG-HEADER-REC TO WS-HOLD-TRIG-HEADER-AREA.
		ws.getWsHoldTrigHeaderArea().setWsHoldTrigHeaderAreaBytes(ws.getTrigRecordLayout().getTrigHeaderRecBytes());
		// COB_CODE: code not available
		outputOrigTo.setVariable(ws.getWsHoldTrigHeaderArea().getWsHoldTrigHeaderAreaFormatted());
		outputOrigDAO.write(outputOrigTo);
	}

	/**Original name: 1011-DECLARE-TRIGGER-TBL<br>
	 * <pre>--------------------------------------------------------------*
	 *     DISPLAY ' *1011-DECLARE-TRIGGER-TBL* '.
	 * ***************************************************************
	 *    THIS PARAGRAPH DECLARES A GLOBAL TEMPORARY DB2 TABLE. THEN *
	 *    IT READS THE TRIGGER FILE AND INSERTS EACH RECORD FROM     *
	 *    THAT FILE INTO THE TEMP TABLE.                             *
	 *    PROCESSING OF CLAIMS DOES NOT BEGIN UNTIL AFTER TEMP TABLE *
	 *    IS COMPLETELY LOADED.                                      *
	 *                                                               *
	 *    THIS SHOULD SAVE THOUSANDS OF 'OPEN' & 'CLOSE' CURSORS     *
	 * ***************************************************************
	 * ***  25
	 * ***  37</pre>*/
	private void declareTriggerTbl() {
		ConcatUtil concatUtil = null;
		GenericParam abendCode = null;
		// COB_CODE:      EXEC SQL
		//                DECLARE GLOBAL TEMPORARY TABLE TRIGGER2
		//                   (T_PROD_IND           CHAR(3)  NOT NULL WITH DEFAULT ' '
		//                  , T_CLM_CNTL_ID        CHAR(12) NOT NULL WITH DEFAULT ' '
		//                  , T_CLM_CNTL_SFX_ID    CHAR(1)  NOT NULL WITH DEFAULT ' '
		//                  , T_PMT_ID             DECIMAL(2,0) NOT NULL
		//                                                      WITH DEFAULT 0
		//                  , T_HIST_BILLNG_NPI CHAR(10) NOT NULL WITH DEFAULT ' '
		//                  , T_LCL_BILL_LOB_CD CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_LCL_BILL_PROV_ID CHAR(10) NOT NULL WITH DEFAULT ' '
		//                  , T_BILL_PROV_ID_QLF   CHAR(3)  NOT NULL WITH DEFAULT ' '
		//                  , T_BILL_PROV_SPEC     CHAR(4)  NOT NULL WITH DEFAULT ' '
		//                  , T_HIST_PERF_NPI CHAR(10) NOT NULL WITH DEFAULT ' '
		//                  , T_LCL_PERF_LOB_CD CHAR(01) NOT NULL WITH DEFAULT ' '
		//                  , T_LCL_PERF_ID CHAR(10) NOT NULL WITH DEFAULT ' '
		//                  , T_INS_ID             CHAR(12) NOT NULL WITH DEFAULT ' '
		//                  , T_MEMBER_ID          CHAR(14) NOT NULL WITH DEFAULT ' '
		//                  , T_CORP_RCV_DT        DATE NOT NULL
		//                                              WITH DEFAULT '9999-12-31'
		//                  , T_PMT_ADJD_DT        DATE NOT NULL
		//                                              WITH DEFAULT '9999-12-31'
		//                  , T_PMT_FRM_SERV_DT    DATE NOT NULL
		//                                              WITH DEFAULT '9999-12-31'
		//                  , T_PMT_THR_SERV_DT    DATE NOT NULL
		//                                              WITH DEFAULT '9999-12-31'
		//                  , T_BILL_FRM_DT        DATE NOT NULL
		//                                              WITH DEFAULT '9999-12-31'
		//                  , T_BILL_THR_DT        DATE NOT NULL
		//                                              WITH DEFAULT '9999-12-31'
		//                  , T_ASG_CD             CHAR(2) NOT NULL WITH DEFAULT ' '
		//                  , T_CLM_PD_AM          DECIMAL(11,2) NOT NULL
		//                                              WITH DEFAULT 0
		//                  , T_ADJ_TYP_CD         CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_KCAPS_TEAM_NM      CHAR(5) NOT NULL WITH DEFAULT ' '
		//                  , T_KCAPS_USE_ID       CHAR(3) NOT NULL WITH DEFAULT ' '
		//                  , T_HIST_LOAD_CD       CHAR(1) NOT NULL WITH DEFAULT ' '
		//           ****  25
		//                  , T_PMT_BK_PROD_CD     CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_DRG_CD             CHAR(4) NOT NULL WITH DEFAULT ' '
		//                  , T_SCHDL_DRG_ALW_AM   DECIMAL(11, 2) NOT NULL WITH
		//                                                         DEFAULT 0
		//                  , T_ALT_DRG_ALW_AM     DECIMAL(11, 2) NOT NULL WITH
		//                                                         DEFAULT 0
		//                  , T_OVER_DRG_ALW_AM    DECIMAL(11, 2) NOT NULL WITH
		//                                                         DEFAULT 0
		//                  , T_GMIS_INDICATOR     CHAR(7) NOT NULL WITH DEFAULT ' '
		//                  , T_PMT_OI_IN          CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_PAT_ACT_MED_REC    CHAR(25) NOT NULL WITH DEFAULT ' '
		//                  , T_PGM_AREA_CD        CHAR(3) NOT NULL WITH DEFAULT ' '
		//                  , T_FIN_CD             CHAR(3) NOT NULL WITH DEFAULT ' '
		//                  , T_ADJD_PROV_STAT     CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_ITS_CLM_TYP_CD     CHAR(1) NOT NULL WITH DEFAULT ' '
		//           ****  37
		//                  , T_PRMPT_PAY_DAY      CHAR(2) NOT NULL WITH DEFAULT ' '
		//                  , T_PRMPT_PAY_OVRD     CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_ACCRUED_PRMPT_PAY_INT  DECIMAL(11,2) NOT NULL
		//                                                     WITH DEFAULT 0
		//                  , T_PROV_UNWRP_DT      DATE NOT NULL
		//                                              WITH DEFAULT '9999-12-31'
		//                  , T_GRP_ID             CHAR(12) NOT NULL WITH DEFAULT ' '
		//                  , T_TYP_GRP_CD         CHAR(3) NOT NULL WITH DEFAULT ' '
		//                  , T_NPI_CD         CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_CLAIM_LOB_CD       CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_RATE_CD            CHAR(2) NOT NULL WITH DEFAULT ' '
		//                  , T_NTWRK_CD           CHAR(3) NOT NULL WITH DEFAULT ' '
		//                  , T_BASE_CN_ARNG_CD    CHAR(5) NOT NULL WITH DEFAULT ' '
		//                  , T_PRIM_CN_ARNG_CD    CHAR(5) NOT NULL WITH DEFAULT ' '
		//                  , T_ENR_CL_CD          CHAR(3) NOT NULL WITH DEFAULT ' '
		//                  , T_LST_FNL_PMT_ID     DECIMAL(2,0) NOT NULL
		//                                                      WITH DEFAULT 0
		//                  , T_STAT_ADJ_PREV      CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_EFT_IND            CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_EFT_ACCOUNT_TYPE   CHAR(01) NOT NULL WITH DEFAULT ' '
		//                  , T_EFT_ACCT           CHAR(17) NOT NULL WITH DEFAULT ' '
		//                  , T_EFT_TRANS          CHAR(9) NOT NULL WITH DEFAULT ' '
		//                  , T_ALPH_PRFX_CD       CHAR(3) NOT NULL WITH DEFAULT ' '
		//                  , T_GL_OFST_ORIG_CD    CHAR(2) NOT NULL WITH DEFAULT ' '
		//                  , T_GL_SOTE_ORIG_CD    DECIMAL(2,0) NOT NULL
		//                                                      WITH DEFAULT 0
		//                  , T_VOID_CD            CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_ITS_INS_ID         CHAR(17) NOT NULL WITH DEFAULT ' '
		//                  , T_ADJ_RESP_CD        CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_ADJ_TRCK_CD        CHAR(2) NOT NULL WITH DEFAULT ' '
		//                  , T_CLMCK_ADJ_ST_CD    CHAR(1) NOT NULL WITH DEFAULT ' '
		//                  , T_837_BILL_PRV_NPI   CHAR(10) NOT NULL WITH DEFAULT ' '
		//                  , T_837_PERF_PRV_NPI   CHAR(10) NOT NULL WITH DEFAULT ' '
		//                  , T_ITS_CK             CHAR(01) NOT NULL WITH DEFAULT ' '
		//                  , T_DATE_COVERAGE_LAPSED DATE NOT NULL
		//                                              WITH DEFAULT '9999-12-31'
		//                  , T_OI_PAY_NM          CHAR(35) NOT NULL WITH DEFAULT ' '
		//                  , T_CORR_PRIORITY_SUB_ID
		//                                         CHAR(30) NOT NULL WITH DEFAULT ' '
		//                  , T_HIPAA_VERSION_FORMAT_ID
		//                                         CHAR(10) NOT NULL WITH DEFAULT ' '
		//                  , T_VBR_IN             CHAR(01) NOT NULL WITH DEFAULT ' '
		//                  , T_MRKT_PKG_CD        CHAR(30) NOT NULL WITH DEFAULT ' ')
		//                    ON COMMIT PRESERVE ROWS
		//                END-EXEC.
		//SQL statement db2sql:DeclareGlobalTempTable doesn't need a translation
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                   CONTINUE
		//               WHEN OTHER
		//                   CALL 'ABEND' USING ABEND-CODE
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: DISPLAY '     ' WS-PGM-NAME ' ABENDING  '
			DisplayUtil.sysout.write("     ", ws.getWsPgmNameFormatted(), " ABENDING  ");
			// COB_CODE: DISPLAY 'PARA: 1011-DECLARE-TRIGGER-TBL  '
			DisplayUtil.sysout.write("PARA: 1011-DECLARE-TRIGGER-TBL  ");
			// COB_CODE: DISPLAY 'ERROR DECLARING TRIGGER TABLE  '
			DisplayUtil.sysout.write("ERROR DECLARING TRIGGER TABLE  ");
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: PERFORM 9940-DISPLAY-SQLCODE
			displaySqlcode();
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'INTERNAL'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("INTERNAL");
			// COB_CODE: MOVE 'DECLARE TEMP TABLE' TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("DECLARE TEMP TABLE");
			// COB_CODE: MOVE '1011-DECLARE-TRIGGER-TBL' TO DB2-ERR-PARA
			ws.setDb2ErrPara("1011-DECLARE-TRIGGER-TBL");
			//            PERFORM DB2-STAT-ERR
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: MOVE +110   TO ABEND-CODE
			ws.setAbendCode(110);
			// COB_CODE: MOVE +110   TO RETURN-CODE
			Session.setReturnCode(110);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;
		}
		// COB_CODE: PERFORM 1305-READ-INSERT-TRIGGER.
		readInsertTrigger();
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: IF INTG-EOF
		//              DISPLAY '   '
		//           ELSE
		//                  'INCLUDING THE HEADER RECORD.)'
		//           END-IF.
		if (ws.getProgramSwitches().isIntgEof()) {
			// COB_CODE: MOVE   1   TO   TRIGGER-EMPTY-SW
			ws.getProgramSwitches().setTriggerEmptySw(((short) 1));
			// COB_CODE: DISPLAY '  TRIGGER FILE HAS NO F0 RECORDS ON IT. '
			DisplayUtil.sysout.write("  TRIGGER FILE HAS NO F0 RECORDS ON IT. ");
			// COB_CODE: DISPLAY '   '
			DisplayUtil.sysout.write("   ");
		} else {
			// COB_CODE: PERFORM 1016-LOAD-TRIGGER
			//               UNTIL INTG-EOF
			while (!ws.getProgramSwitches().isIntgEof()) {
				loadTrigger();
			}
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY ' TRIGGER FILE IS LOADED! ('
			//               TRIGGER-RECORDS-IN ' RECORDS LOADED, '
			//               'INCLUDING THE HEADER RECORD.)'
			DisplayUtil.sysout.write(" TRIGGER FILE IS LOADED! (", ws.getProgramCounters().getTriggerRecordsInAsString(), " RECORDS LOADED, ",
					"INCLUDING THE HEADER RECORD.)");
		}
	}

	/**Original name: 1016-LOAD-TRIGGER<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '1016-LOAD-TRIGGER'.
	 * ***************************************************************
	 *  THIS READS THE TRIGGER FILE AND INSERTS EACH RECORD TO A TEMP*
	 *  TABLE.
	 *  MOVING FIELDS TO WORKING STORAGE IS NECESSARY BECAUSE THE    *
	 *  NUMERIC FIELDS ON THE TRIGGER ARE NOT SIGNED AND PACKED BUT  *
	 *  THE ONES IN DB2 ARE.                                         *
	 * ***************************************************************</pre>*/
	private void loadTrigger() {
		ConcatUtil concatUtil = null;
		GenericParam abendCode = null;
		// COB_CODE: MOVE TRIG-CLM-PMT-ID       TO WS-TRIG-PMT-ID.
		ws.getProgramHoldAreas().setWsTrigPmtId(ws.getTrigRecordLayout().getTrigClmPmtId());
		//    DISPLAY 'TRIG-CLM-PMT-ID = ' TRIG-CLM-PMT-ID.
		// COB_CODE: MOVE TRIG-CLM-PD-AM        TO WS-TRIG-CLM-PD-AM.
		ws.getProgramHoldAreas().setWsTrigClmPdAm(Trunc.toDecimal(ws.getTrigRecordLayout().getTrigClmPdAm(), 11, 2));
		// COB_CODE: MOVE TRIG-LOCAL-BILL-PROV-LOB-CD
		//                                      TO WS-TRIG-LOCAL-BILL-PROV-LOB-CD.
		ws.getProgramHoldAreas().setWsTrigLocalBillProvLobCd(ws.getTrigRecordLayout().getTrigLocalBillProvLobCd());
		// COB_CODE: MOVE TRIG-ACCRUED-PRMPT-PAY-INT-AM TO
		//                WS-ACCRUED-PRMPT-PAY-INT-AM.
		ws.getProgramHoldAreas().setWsAccruedPrmptPayIntAm(Trunc.toDecimal(ws.getTrigRecordLayout().getTrigAccruedPrmptPayIntAm(), 11, 2));
		// COB_CODE: MOVE TRIG-LST-FNL-PMT-PT-ID TO WS-TRIG-LST-FNL-PMT-PT-ID.
		ws.getProgramHoldAreas().setWsTrigLstFnlPmtPtId(ws.getTrigRecordLayout().getTrigLstFnlPmtPtId());
		// COB_CODE: MOVE TRIG-SCHDL-DRG-ALW-AM TO WS-TRIG-SCHDL-DRG-ALW-AM.
		ws.getProgramHoldAreas().setWsTrigSchdlDrgAlwAm(Trunc.toDecimal(ws.getTrigRecordLayout().getTrigSchdlDrgAlwAm(), 11, 2));
		// COB_CODE: MOVE TRIG-ALT-DRG-ALW-AM   TO WS-TRIG-ALT-DRG-ALW-AM.
		ws.getProgramHoldAreas().setWsTrigAltDrgAlwAm(Trunc.toDecimal(ws.getTrigRecordLayout().getTrigAltDrgAlwAm(), 11, 2));
		// COB_CODE: MOVE TRIG-OVER-DRG-ALW-AM  TO WS-TRIG-OVER-DRG-ALW-AM.
		ws.getProgramHoldAreas().setWsTrigOverDrgAlwAm(Trunc.toDecimal(ws.getTrigRecordLayout().getTrigOverDrgAlwAm(), 11, 2));
		// COB_CODE: MOVE TRIG-GL-SOTE-ORIG-CD  TO WS-TRIG-GL-SOTE-ORIG-CD.
		ws.getProgramHoldAreas().setWsTrigGlSoteOrigCd(ws.getTrigRecordLayout().getTrigGlSoteOrigCd());
		// COB_CODE:      EXEC SQL
		//                     INSERT INTO SESSION.TRIGGER2 (
		//                                 T_PROD_IND
		//                               , T_CLM_CNTL_ID
		//                               , T_CLM_CNTL_SFX_ID
		//                               , T_PMT_ID
		//                               , T_HIST_BILLNG_NPI
		//                               , T_LCL_BILL_LOB_CD
		//                               , T_LCL_BILL_PROV_ID
		//                               , T_BILL_PROV_ID_QLF
		//                               , T_BILL_PROV_SPEC
		//                               , T_HIST_PERF_NPI
		//                               , T_LCL_PERF_LOB_CD
		//                               , T_LCL_PERF_ID
		//                               , T_INS_ID
		//                               , T_MEMBER_ID
		//                               , T_CORP_RCV_DT
		//                               , T_PMT_ADJD_DT
		//                               , T_PMT_FRM_SERV_DT
		//                               , T_PMT_THR_SERV_DT
		//                               , T_BILL_FRM_DT
		//                               , T_BILL_THR_DT
		//                               , T_ASG_CD
		//                               , T_CLM_PD_AM
		//                               , T_ADJ_TYP_CD
		//                               , T_KCAPS_TEAM_NM
		//                               , T_KCAPS_USE_ID
		//                               , T_HIST_LOAD_CD
		//                               , T_PMT_BK_PROD_CD
		//                               , T_DRG_CD
		//                               , T_SCHDL_DRG_ALW_AM
		//                               , T_ALT_DRG_ALW_AM
		//                               , T_OVER_DRG_ALW_AM
		//                               , T_GMIS_INDICATOR
		//                               , T_PMT_OI_IN
		//                               , T_PAT_ACT_MED_REC
		//                               , T_PGM_AREA_CD
		//                               , T_FIN_CD
		//                               , T_ADJD_PROV_STAT
		//                               , T_ITS_CLM_TYP_CD
		//                               , T_PRMPT_PAY_DAY
		//                               , T_PRMPT_PAY_OVRD
		//                               , T_ACCRUED_PRMPT_PAY_INT
		//                               , T_PROV_UNWRP_DT
		//                               , T_GRP_ID
		//                               , T_TYP_GRP_CD
		//                               , T_NPI_CD
		//                               , T_CLAIM_LOB_CD
		//                               , T_RATE_CD
		//                               , T_NTWRK_CD
		//                               , T_BASE_CN_ARNG_CD
		//                               , T_PRIM_CN_ARNG_CD
		//                               , T_ENR_CL_CD
		//                               , T_LST_FNL_PMT_ID
		//                               , T_STAT_ADJ_PREV
		//                               , T_EFT_IND
		//                               , T_EFT_ACCOUNT_TYPE
		//                               , T_EFT_ACCT
		//                               , T_EFT_TRANS
		//                               , T_ALPH_PRFX_CD
		//                               , T_GL_OFST_ORIG_CD
		//                               , T_GL_SOTE_ORIG_CD
		//                               , T_VOID_CD
		//                               , T_ITS_INS_ID
		//                               , T_ADJ_RESP_CD
		//                               , T_ADJ_TRCK_CD
		//                               , T_CLMCK_ADJ_ST_CD
		//                               , T_837_BILL_PRV_NPI
		//                               , T_837_PERF_PRV_NPI
		//                               , T_ITS_CK
		//                               , T_DATE_COVERAGE_LAPSED
		//                               , T_OI_PAY_NM
		//                               , T_CORR_PRIORITY_SUB_ID
		//                               , T_HIPAA_VERSION_FORMAT_ID
		//                               , T_VBR_IN
		//                               , T_MRKT_PKG_CD)
		//                     VALUES (:TRIG-PROD-IND
		//                           , :TRIG-CLM-CNTL-ID
		//                           , :TRIG-CLM-CNTL-SFX
		//                           , :WS-TRIG-PMT-ID
		//                           , :TRIG-HIST-BILL-PROV-NPI-ID
		//                           , :TRIG-LOCAL-BILL-PROV-LOB-CD
		//                           , :TRIG-LOCAL-BILL-PROV-ID
		//                           , :TRIG-BILL-PROV-ID-QLF-CD
		//                           , :TRIG-BILL-PROV-SPEC-CD
		//                           , :TRIG-HIST-PERF-PROV-NPI-ID
		//                           , :TRIG-LOCAL-PERF-PROV-LOB-CD
		//                           , :TRIG-LOCAL-PERF-PROV-ID
		//                           , :TRIG-INS-ID
		//                           , :TRIG-MEMBER-ID
		//                           , :TRIG-CORP-RCV-DT
		//                           , :TRIG-PMT-ADJD-DT
		//                           , :TRIG-PMT-FRM-SERV-DT
		//                           , :TRIG-PMT-THR-SERV-DT
		//                           , :TRIG-BILL-FRM-DT
		//                           , :TRIG-BILL-THR-DT
		//           ********    20
		//                           , :TRIG-ASG-CD
		//                           , :WS-TRIG-CLM-PD-AM
		//                           , :TRIG-ADJ-TYP-CD
		//                           , :TRIG-KCAPS-TEAM-NM
		//                           , :TRIG-KCAPS-USE-ID
		//                           , :TRIG-HIST-LOAD-CD
		//                           , :TRIG-PMT-BK-PROD-CD
		//                           , :TRIG-DRG-CD
		//                           , :WS-TRIG-SCHDL-DRG-ALW-AM
		//                           , :WS-TRIG-ALT-DRG-ALW-AM
		//                           , :WS-TRIG-OVER-DRG-ALW-AM
		//                           , :TRIG-GMIS-INDICATOR
		//                           , :TRIG-PMT-OI-IN
		//                           , :TRIG-PAT-ACT-MED-REC-ID
		//           ************  35
		//                           , :TRIG-PGM-AREA-CD
		//                           , :TRIG-FIN-CD
		//                           , :TRIG-ADJD-PROV-STAT-CD
		//                           , :TRIG-ITS-CLM-TYP-CD
		//                           , :TRIG-PRMPT-PAY-DAY-CD
		//                           , :TRIG-PRMPT-PAY-OVRD-CD
		//                           , :WS-ACCRUED-PRMPT-PAY-INT-AM
		//                           , :TRIG-PROV-UNWRP-DT
		//                           , :TRIG-GRP-ID
		//                           , :TRIG-TYP-GRP-CD
		//                           , :TRIG-NPI-CD
		//                           , :TRIG-CLAIM-LOB-CD
		//                           , :TRIG-RATE-CD
		//                           , :TRIG-NTWRK-CD
		//                           , :TRIG-BASE-CN-ARNG-CD
		//                           , :TRIG-PRIM-CN-ARNG-CD
		//                           , :TRIG-ENR-CL-CD
		//                           , :WS-TRIG-LST-FNL-PMT-PT-ID
		//                           , :TRIG-STAT-ADJ-PREV-PMT
		//                           , :TRIG-EFT-IND
		//                           , :TRIG-EFT-ACCOUNT-TYPE
		//                           , :TRIG-EFT-ACCT
		//                           , :TRIG-EFT-TRANS
		//                           , :TRIG-ALPH-PRFX-CD
		//                           , :TRIG-GL-OFST-ORIG-CD
		//                           , :WS-TRIG-GL-SOTE-ORIG-CD
		//                           , :TRIG-VOID-CD
		//                           , :TRIG-ITS-INS-ID
		//                           , :TRIG-ADJ-RESP-CD
		//                           , :TRIG-ADJ-TRCK-CD
		//                           , :TRIG-CLMCK-ADJ-STAT-CD
		//                           , :TRIG-837-BILL-PROV-NPI-ID
		//                           , :TRIG-837-PERF-PROV-NPI-ID
		//                           , :TRIG-ITS-CK
		//                           , :TRIG-DATE-COVERAGE-LAPSED
		//                           , :TRIG-OI-PAY-NM
		//                           , :TRIG-CORR-PRIORITY-SUB-ID
		//                           , :TRIG-HIPAA-VERSION-FORMAT-ID
		//                           , :TRIG-VBR-IN
		//                           , :TRIG-MRKT-PKG-CD)
		//                END-EXEC.
		trigger2Dao.insertRec(ws);
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                   CONTINUE
		//               WHEN OTHER
		//                   CALL 'ABEND' USING ABEND-CODE
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: DISPLAY '     ' WS-PGM-NAME ' ABENDING  '
			DisplayUtil.sysout.write("     ", ws.getWsPgmNameFormatted(), " ABENDING  ");
			// COB_CODE: DISPLAY 'PARA: 1016-LOAD-TRIGGER        '
			DisplayUtil.sysout.write("PARA: 1016-LOAD-TRIGGER        ");
			// COB_CODE: DISPLAY 'ERROR INSERTING TRIGGER TABLE  '
			DisplayUtil.sysout.write("ERROR INSERTING TRIGGER TABLE  ");
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: PERFORM 9940-DISPLAY-SQLCODE
			displaySqlcode();
			// COB_CODE: MOVE '1016-LOAD-TRIGGER  ' TO DB2-ERR-PARA
			ws.setDb2ErrPara("1016-LOAD-TRIGGER  ");
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'INTERNAL'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("INTERNAL");
			// COB_CODE: MOVE 'LOADING TEMP TABLE' TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("LOADING TEMP TABLE");
			//***         PERFORM DB2-STAT-ERR
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: MOVE +160   TO ABEND-CODE
			ws.setAbendCode(160);
			// COB_CODE: MOVE +160   TO RETURN-CODE
			Session.setReturnCode(160);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;
		}
		// COB_CODE: PERFORM 1305-READ-INSERT-TRIGGER.
		readInsertTrigger();
	}

	/**Original name: 1019-CLOSE-ROUTINE<br>*/
	private void closeRoutine() {
		// COB_CODE: CLOSE INPUT-TRIGGER
		//                 JOB-PARMS-FILE
		//                 OUTPUT-ORIG
		//                 OUTPUT-VOID
		//                 OUTPUT-ORIG-GMIS
		//                 OUTPUT-VOID-GMIS.
		inputTriggerDAO.close();
		jobParmsFileDAO.close();
		outputOrigDAO.close();
		outputVoidDAO.close();
		outputOrigGmisDAO.close();
		outputVoidGmisDAO.close();
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY ' TOTAL TRIGGER RECORDS  = ' TRIGGER-RECORDS-IN
		//                   '    (INCLUDING THE HEADER RECORD)'.
		DisplayUtil.sysout.write(" TOTAL TRIGGER RECORDS  = ", ws.getProgramCounters().getTriggerRecordsInAsString(),
				"    (INCLUDING THE HEADER RECORD)");
		// COB_CODE: COMPUTE CURR-TRIG-PROCESSED-CNT =
		//               CURR-TRIG-PROCESSED-CNT - 1.
		ws.getProgramCounters().setCurrTrigProcessedCnt(Trunc.toInt(abs(ws.getProgramCounters().getCurrTrigProcessedCnt() - 1), 6));
		// COB_CODE: DISPLAY '   CURR TRG PROCESSED  = ' CURR-TRIG-PROCESSED-CNT.
		DisplayUtil.sysout.write("   CURR TRG PROCESSED  = ", ws.getProgramCounters().getCurrTrigProcessedCntAsString());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY ' TOTAL ORIGINAL REPAY RECORDS WRITTEN = '
		//                   WS-ORIG-REPAY-CNT
		//                   '(HEADER RECORD NOT IN INCLUDED)'.
		DisplayUtil.sysout.write(" TOTAL ORIGINAL REPAY RECORDS WRITTEN = ", ws.getProgramCounters().getWsOrigRepayCntAsString(),
				"(HEADER RECORD NOT IN INCLUDED)");
		// COB_CODE: DISPLAY ' TOTAL VOID RECORDS WRITTEN = '
		//                    WS-VOID-RECORDS-CNT.
		DisplayUtil.sysout.write(" TOTAL VOID RECORDS WRITTEN = ", ws.getProgramCounters().getWsVoidRecordsCntAsString());
	}

	/**Original name: 1100-CHECK-FOR-RESTART<br>
	 * <pre>    DISPLAY '1100-CHECK-FOR-RESTART'.
	 * * ------------------------------------------------------------ **
	 * *  WITHIN THIS MODULE IN ADDITION TO CHECKING FOR NUMBER       **
	 * *  OF RECORDS BEFORE ISSUING A CHECKPOINT, ALSO QUERY THE      **
	 * *  TIME IN THE TABLE TO SEE WHEN THE LAST ONE WAS. IF IT WAS   **
	 * *  MORE THAN SAY 10 SECONDS AGO THEN GO AHEAD AND CHECKPOINT   **
	 * *  AT LOGICAL UOW EVEN IF SPECIFIC NUMBER OF RECORDS HAS NOT   **
	 * *  BEEN REACHED.  -- NEED TO ADD THIS! 10-15-01                **
	 * * ------------------------------------------------------------ **</pre>*/
	private void checkForRestart() {
		ConcatUtil concatUtil = null;
		GenericParam abendCode = null;
		// COB_CODE: PERFORM 1102-SELECT-RESTART.
		selectRestart();
		// COB_CODE: PERFORM 1103-DISPLAY-RESTART-INFO.
		displayRestartInfo();
		// COB_CODE: EXEC SQL
		//              UPDATE S03086SA
		//                  SET CMT_CT         = :DCLS03086SA.CMT-CT
		//                    , RSTRT1_TX      = :DCLS03086SA.RSTRT1-TX
		//                    , RSTRT2_TX      = :DCLS03086SA.RSTRT2-TX
		//                    , CMT_TS         = CURRENT_TIMESTAMP
		//                  WHERE JOB_NM       = :DCLS03086SA.JOB-NM
		//                    AND JBSTP_NM     = :DCLS03086SA.JBSTP-NM
		//                    AND JBSTP_SEQ_ID = :DCLS03086SA.JBSTP-SEQ-ID
		//           END-EXEC.
		s03086saDao.updateRec1(ws.getDcls03086sa());
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                   CONTINUE
		//               WHEN OTHER
		//                   CALL 'ABEND' USING ABEND-CODE
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: DISPLAY '     ' WS-PGM-NAME ' ABENDING  '
			DisplayUtil.sysout.write("     ", ws.getWsPgmNameFormatted(), " ABENDING  ");
			// COB_CODE: DISPLAY 'PARA: 9000-CHECK-FOR-JOB-RESTART'
			DisplayUtil.sysout.write("PARA: 9000-CHECK-FOR-JOB-RESTART");
			// COB_CODE: DISPLAY 'ERROR UPDATING S03086SA TABLE  '
			DisplayUtil.sysout.write("ERROR UPDATING S03086SA TABLE  ");
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: PERFORM 9940-DISPLAY-SQLCODE
			displaySqlcode();
			// COB_CODE: PERFORM 1103-DISPLAY-RESTART-INFO
			displayRestartInfo();
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S03086SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S03086SA");
			// COB_CODE: MOVE 'UPDATE S03086SA'     TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("UPDATE S03086SA");
			// COB_CODE: MOVE '9000-CHECK-FOR-JOB-RESTART' TO DB2-ERR-PARA
			ws.setDb2ErrPara("9000-CHECK-FOR-JOB-RESTART");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: MOVE +3086  TO ABEND-CODE
			ws.setAbendCode(3086);
			// COB_CODE: MOVE +3086  TO RETURN-CODE
			Session.setReturnCode(3086);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;
		}
		// COB_CODE: PERFORM 7100-SQL-COMMIT.
		sqlCommit();
	}

	/**Original name: 1102-SELECT-RESTART<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '1102-SELECT-RESTART'.
	 * *****************************************************************
	 *  THIS SELECT RETRIEVES THE COMMIT COUNT FREQUENCY. THERE ARE TWO*
	 *  DIFFERENT VALUES; FIRST IS BY # OF CLAIMS - ROW-FREQ-CMT-CT,   *
	 *  SECOND IS BY ELAPSED TIME - CMT-TM-INT-TM.                     *
	 * *****************************************************************</pre>*/
	private void selectRestart() {
		ConcatUtil concatUtil = null;
		GenericParam abendCode = null;
		// COB_CODE: EXEC SQL
		//              SELECT JOB_NM
		//                  ,  JBSTP_NM
		//                  ,  JBSTP_SEQ_ID
		//                  ,  INFO_CHG_TS
		//                  ,  APPL_TRM_CD
		//                  ,  RSTRT_HIST_IN
		//                  ,  ROW_FREQ_CMT_CT
		//                  ,  CMT_TM_INT_TM
		//                  ,  CMT_CT
		//                  ,  CMT_TS
		//                  ,  RSTRT1_TX
		//                  ,  RSTRT2_TX
		//                  ,  INFO_CHG_ID
		//              INTO  :DCLS03086SA.JOB-NM
		//                  , :DCLS03086SA.JBSTP-NM
		//                  , :DCLS03086SA.JBSTP-SEQ-ID
		//                  , :DCLS03086SA.INFO-CHG-TS
		//                  , :DCLS03086SA.APPL-TRM-CD
		//                  , :DCLS03086SA.RSTRT-HIST-IN
		//                  , :DCLS03086SA.ROW-FREQ-CMT-CT
		//                  , :DCLS03086SA.CMT-TM-INT-TM
		//                  , :DCLS03086SA.CMT-CT
		//                  , :DCLS03086SA.CMT-TS
		//                  , :DCLS03086SA.RSTRT1-TX
		//                  , :DCLS03086SA.RSTRT2-TX
		//                  , :DCLS03086SA.INFO-CHG-ID
		//              FROM S03086SA
		//                  WHERE JOB_NM       = :WS-JOB-NAME
		//                    AND JBSTP_NM     = :WS-JOB-STEP
		//                    AND JBSTP_SEQ_ID = :WS-JOB-SEQ-PACKED
		//           END-EXEC.
		s03086saDao.selectRec1(jobParmsFileTO.getName(), jobParmsFileTO.getStep(), ws.getWsJobSeqPacked(), ws.getDcls03086sa());
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                   MOVE WS-CMT-TM-INT-TM-SS TO EXPANDED-CMT-TM-SS
		//               WHEN OTHER
		//                   CALL 'ABEND' USING ABEND-CODE
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE APPL-TRM-CD     OF DCLS03086SA TO
				//                                           WS-APPL-TRM-CD
			ws.getDataSavedFromTable3086().setWsApplTrmCd(ws.getDcls03086sa().getApplTrmCd());
			// COB_CODE: MOVE RSTRT-HIST-IN   OF DCLS03086SA TO
			//                                           WS-RSTRT-HIST-IN
			ws.getDataSavedFromTable3086().setWsRstrtHistIn(ws.getDcls03086sa().getRstrtHistIn());
			// COB_CODE: MOVE ROW-FREQ-CMT-CT OF DCLS03086SA TO
			//                                           WS-ROW-FREQ-CMT-CT
			ws.getDataSavedFromTable3086().setWsRowFreqCmtCt(TruncAbs.toInt(ws.getDcls03086sa().getRowFreqCmtCt(), 9));
			// COB_CODE: MOVE CMT-TM-INT-TM   OF DCLS03086SA TO
			//                                           WS-CMT-TM-INT-TM
			ws.getDataSavedFromTable3086().getWsCmtTmIntTm().setWsCmtTmIntTmFormatted(ws.getDcls03086sa().getCmtTmIntTmFormatted());
			// COB_CODE: MOVE WS-CMT-TM-INT-TM-SS TO EXPANDED-CMT-TM-SS
			ws.getDataSavedFromTable3086().setExpandedCmtTmSsFormatted(ws.getDataSavedFromTable3086().getWsCmtTmIntTm().getSsFormatted());
			break;

		default:// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: DISPLAY '     ' WS-PGM-NAME ' ABENDING  '
			DisplayUtil.sysout.write("     ", ws.getWsPgmNameFormatted(), " ABENDING  ");
			// COB_CODE: DISPLAY 'ERROR SELECTING S03086SA TABLE '
			DisplayUtil.sysout.write("ERROR SELECTING S03086SA TABLE ");
			// COB_CODE: DISPLAY 'FOR RESTART INFORMATION.       '
			DisplayUtil.sysout.write("FOR RESTART INFORMATION.       ");
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: PERFORM 9940-DISPLAY-SQLCODE
			displaySqlcode();
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S03086SA'                TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S03086SA");
			// COB_CODE: MOVE 'SELECT S03086SA'         TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("SELECT S03086SA");
			// COB_CODE: MOVE '1102-SELECT-RESTART   '  TO DB2-ERR-PARA
			ws.setDb2ErrPara("1102-SELECT-RESTART   ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: MOVE +3086  TO ABEND-CODE
			ws.setAbendCode(3086);
			// COB_CODE: MOVE +3086  TO RETURN-CODE
			Session.setReturnCode(3086);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;
		}
	}

	/**Original name: 1103-DISPLAY-RESTART-INFO<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '1103-DISPLAY-RESTART-INFO'.</pre>*/
	private void displayRestartInfo() {
		// COB_CODE: DISPLAY  ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY  'RESTART INFORMATION FROM TA03086: '.
		DisplayUtil.sysout.write("RESTART INFORMATION FROM TA03086: ");
		// COB_CODE: DISPLAY  '  JOB-NM          ' JOB-NM          OF DCLS03086SA.
		DisplayUtil.sysout.write("  JOB-NM          ", ws.getDcls03086sa().getJobNmFormatted());
		// COB_CODE: DISPLAY  '  JOBSTP-NM       ' JBSTP-NM        OF DCLS03086SA.
		DisplayUtil.sysout.write("  JOBSTP-NM       ", ws.getDcls03086sa().getJbstpNmFormatted());
		// COB_CODE: DISPLAY  '  JBSTP-SEQ-ID    ' JBSTP-SEQ-ID    OF DCLS03086SA.
		DisplayUtil.sysout.write("  JBSTP-SEQ-ID    ", ws.getDcls03086sa().getJbstpSeqIdAsString());
		// COB_CODE: DISPLAY  '  INFO-CHG-TS     ' INFO-CHG-TS     OF DCLS03086SA.
		DisplayUtil.sysout.write("  INFO-CHG-TS     ", ws.getDcls03086sa().getInfoChgTsFormatted());
		// COB_CODE: IF APPL-TRM-CD OF DCLS03086SA = SPACES
		//                  '  APPL-TRM-CD     ' APPL-TRM-CD     OF DCLS03086SA
		//           ELSE
		//                  '                            <<<<< NOTE <<<<<<<<<<<'.
		if (Conditions.eq(ws.getDcls03086sa().getApplTrmCd(), Types.SPACE_CHAR)) {
			// COB_CODE: DISPLAY
			//              '  APPL-TRM-CD     ' APPL-TRM-CD     OF DCLS03086SA
			DisplayUtil.sysout.write("  APPL-TRM-CD     ", String.valueOf(ws.getDcls03086sa().getApplTrmCd()));
		} else {
			// COB_CODE: DISPLAY
			//              '  APPL-TRM-CD     ' APPL-TRM-CD     OF DCLS03086SA
			//              '                            <<<<< NOTE <<<<<<<<<<<'.
			DisplayUtil.sysout.write("  APPL-TRM-CD     ", String.valueOf(ws.getDcls03086sa().getApplTrmCd()),
					"                            <<<<< NOTE <<<<<<<<<<<");
		}
		// COB_CODE: DISPLAY  '  RSTRT-HIST-IN   ' RSTRT-HIST-IN   OF DCLS03086SA.
		DisplayUtil.sysout.write("  RSTRT-HIST-IN   ", String.valueOf(ws.getDcls03086sa().getRstrtHistIn()));
		// COB_CODE: DISPLAY  '  ROW-FREQ-CMT-CT ' ROW-FREQ-CMT-CT OF DCLS03086SA.
		DisplayUtil.sysout.write("  ROW-FREQ-CMT-CT ", ws.getDcls03086sa().getRowFreqCmtCtAsString());
		// COB_CODE: DISPLAY  '  CMT-TM-INT-TM   ' CMT-TM-INT-TM   OF DCLS03086SA.
		DisplayUtil.sysout.write("  CMT-TM-INT-TM   ", ws.getDcls03086sa().getCmtTmIntTmFormatted());
		// COB_CODE: DISPLAY  '  CMT-CT          ' CMT-CT          OF DCLS03086SA.
		DisplayUtil.sysout.write("  CMT-CT          ", ws.getDcls03086sa().getCmtCtAsString());
		// COB_CODE: DISPLAY  '  CMT-TS          ' CMT-TS          OF DCLS03086SA.
		DisplayUtil.sysout.write("  CMT-TS          ", ws.getDcls03086sa().getCmtTsFormatted());
		// COB_CODE: DISPLAY  '  INFO-CHG-ID     ' INFO-CHG-ID     OF DCLS03086SA.
		DisplayUtil.sysout.write("  INFO-CHG-ID     ", ws.getDcls03086sa().getInfoChgIdFormatted());
		// COB_CODE: DISPLAY  '  RSTRT1-TX       ' RSTRT1-TX       OF DCLS03086SA.
		DisplayUtil.sysout.write("  RSTRT1-TX       ", ws.getDcls03086sa().getRstrt1TxFormatted());
		// COB_CODE: DISPLAY  '  RSTRT2-TX       ' RSTRT2-TX       OF DCLS03086SA.
		DisplayUtil.sysout.write("  RSTRT2-TX       ", ws.getDcls03086sa().getRstrt2TxFormatted());
	}

	/**Original name: 1300-READ-JOB-PARMS<br>
	 * <pre>----------------------------------------------------------------*
	 * ------------- DB2 READ -----------------------------------------*
	 * ------  THE CKPT SELECT SHOULD ONLY RETRIEVE ONE ROW     -------*
	 * ------  AT ANY TIME SO THERE IS NO NEED FOR 'FETCH'      -------*
	 * ------  OR BUILDING A CURSOR IN WORKING STORAGE.         -------*
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     DISPLAY '1300-READ-JOB-PARMS'.
	 * ********************************************************
	 *   NEED TO INVESTIGATE THE ROLE OF "JOB-SEQ   " IN HERE.
	 *   OTHER TWO FIELDS READ IN FROM THE JOB-PARMS-FILE WILL
	 *   CHANGE, BUT IN THE EVENT OF A RESTART THIS ONE COULD.
	 * ********************************************************</pre>*/
	private void readJobParms() {
		GenericParam abendCode = null;
		// COB_CODE: READ JOB-PARMS-FILE INTO WS-JOB-PARMS
		//               AT END
		//                  CALL 'ABEND' USING ABEND-CODE.
		jobParmsFileTO = jobParmsFileDAO.read(jobParmsFileTO);
		if (jobParmsFileDAO.getFileStatus().isEOF()) {
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
			// COB_CODE: DISPLAY '      ' WS-PGM-NAME ' ABENDING'
			DisplayUtil.sysout.write("      ", ws.getWsPgmNameFormatted(), " ABENDING");
			// COB_CODE: DISPLAY '  JOB PARMS FILE IS EMPTY     '
			DisplayUtil.sysout.write("  JOB PARMS FILE IS EMPTY     ");
			// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE.
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
		}
	}

	/**Original name: 1305-READ-INSERT-TRIGGER<br>*/
	private void readInsertTrigger() {
		// COB_CODE: READ INPUT-TRIGGER INTO TRIG-RECORD-LAYOUT
		//             AT END
		//               MOVE 1 TO INTG-EOF-SW.
		inputTriggerTo = inputTriggerDAO.read(inputTriggerTo);
		if (inputTriggerDAO.getFileStatus().isSuccess()) {
			ws.getTrigRecordLayout().setTrigRecordLayoutFromBuffer(inputTriggerTo.getData());
		}
		if (inputTriggerDAO.getFileStatus().isEOF()) {
			// COB_CODE: MOVE 1 TO INTG-EOF-SW.
			ws.getProgramSwitches().setIntgEofSw(((short) 1));
		}
		// COB_CODE: IF INTG-EOF-SW = 1
		//               NEXT SENTENCE
		//           ELSE
		//               ADD 1 TO STATS-CNT
		//           END-IF.
		if (!(ws.getProgramSwitches().getIntgEofSw() == 1)) {
			// COB_CODE: ADD 1 TO TRIGGER-RECORDS-IN
			ws.getProgramCounters().setTriggerRecordsIn(Trunc.toInt(1 + ws.getProgramCounters().getTriggerRecordsIn(), 6));
			// COB_CODE: ADD 1 TO CURR-TRIG-PROCESSED-CNT
			ws.getProgramCounters().setCurrTrigProcessedCnt(Trunc.toInt(1 + ws.getProgramCounters().getCurrTrigProcessedCnt(), 6));
			// COB_CODE: ADD 1 TO STATS-CNT
			ws.getProgramCounters().setStatsCnt(Trunc.toInt(1 + ws.getProgramCounters().getStatsCnt(), 6));
		}
	}

	/**Original name: 2150-MAP-PAYMENT-RECORD<br>
	 * <pre>------------- MAIN PROCESSING   --------------------------------*
	 * ----------------------------------------------------------------*
	 *     DISPLAY '   *2150-MAP-PAYMENT-RECORD*'.</pre>*/
	private void mapPaymentRecord() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE SPACES TO CSR-PMT-COMMON-INFO.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().initPmtCommonInfoSpaces();
		// COB_CODE: INITIALIZE CSR-PMT-COMMON-INFO.
		initPmtCommonInfo();
		// COB_CODE: MOVE SPACES TO CSR-CALC-INFORMATION.
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().initCalcInformationSpaces();
		// COB_CODE: INITIALIZE CSR-CALC-INFORMATION.
		initCalcInformation();
		// COB_CODE: MOVE SPACES TO CSR-ARC-IND.
		ws.getNf07areaofCsrProgramLineArea().setArcInd(Types.SPACE_CHAR);
		// COB_CODE: MOVE TRIG-PROD-IND             TO CSR-PROD-IND
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getProdInd().setProdInd(ws.getTrigRecordLayout().getTrigProdInd());
		// COB_CODE: MOVE TRIG-HIST-BILL-PROV-NPI-ID TO CSR-HIST-BILL-PROV-NPI-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillingProvider()
				.setHistBillProvNpiId(ws.getTrigRecordLayout().getTrigHistBillProvNpiId());
		// COB_CODE: MOVE TRIG-LOCAL-BILL-PROV-LOB-CD TO
		//                CSR-LOCAL-BILL-PROV-LOB-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillingProvider().getLocalBillProvLobCd()
				.setLocalBillProvLobCd(ws.getTrigRecordLayout().getTrigLocalBillProvLobCd());
		// COB_CODE: MOVE TRIG-LOCAL-BILL-PROV-ID   TO CSR-LOCAL-BILL-PROV-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillingProvider()
				.setLocalBillProvId(ws.getTrigRecordLayout().getTrigLocalBillProvId());
		// COB_CODE: MOVE TRIG-BILL-PROV-ID-QLF-CD  TO CSR-BILL-PROV-ID-QLF-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillProvIdQlfCd()
				.setBillProvIdQlfCd(ws.getTrigRecordLayout().getTrigBillProvIdQlfCd());
		// COB_CODE: MOVE TRIG-BILL-PROV-SPEC-CD    TO CSR-BILL-PROV-SPEC
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillProvSpec(ws.getTrigRecordLayout().getTrigBillProvSpecCd());
		// COB_CODE: MOVE TRIG-HIST-PERF-PROV-NPI-ID TO CSR-HIST-PERF-PROV-NPI-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPerformingProvider()
				.setHistPerfProvNpiId(ws.getTrigRecordLayout().getTrigHistPerfProvNpiId());
		//****MOVE TRIG-LOCAL-PERF-PROV-LOB-CD TO ????????
		//*   MOVE WS-TRIG-LOCAL-PERF-PROV-ID TO
		//*        CSR-LOCAL-PERF-PROV-LOB-CD
		// COB_CODE: MOVE TRIG-LOCAL-PERF-PROV-ID       TO CSR-LOCAL-PERF-PROV-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPerformingProvider()
				.setLocalPerfProvId(ws.getTrigRecordLayout().getTrigLocalPerfProvId());
		// COB_CODE: MOVE CLM-CNTL-ID OF DCLS02809SA     TO CSR-CLM-CNTL-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setClmCntlIdFormatted(ws.getDcls02809sa().getClmCntlIdFormatted());
		// COB_CODE: MOVE CLM-CNTL-SFX-ID OF DCLS02809SA TO CSR-CLM-CNTL-SFX-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setClmCntlSfxId(ws.getDcls02809sa().getClmCntlSfxId());
		// COB_CODE: IF (CSR-CLM-LOT-NUMBER = '14' OR '71' OR '72' OR '24'
		//                                         OR '81')
		//              END-IF
		//           ELSE
		//                TO CSR-MEDICARE-XOVER
		//           END-IF.
		if (Conditions.eq(ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmLotNumber(), "14")
				|| Conditions.eq(ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmLotNumber(), "71")
				|| Conditions.eq(ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmLotNumber(), "72")
				|| Conditions.eq(ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmLotNumber(), "24")
				|| Conditions.eq(ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmLotNumber(), "81")) {
			// COB_CODE: IF (CSR-CLM-CNTL-SFX-ID = 'G' OR 'H' OR 'P')
			//                TO CSR-MEDICARE-XOVER
			//           ELSE
			//                TO CSR-MEDICARE-XOVER
			//           END-IF
			if (ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId() == 'G'
					|| ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId() == 'H'
					|| ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId() == 'P') {
				// COB_CODE: MOVE 'X'
				//             TO CSR-MEDICARE-XOVER
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillingProvider().setMedicareXoverFormatted("X");
			} else {
				// COB_CODE: MOVE SPACES
				//             TO CSR-MEDICARE-XOVER
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillingProvider().setMedicareXover(Types.SPACE_CHAR);
			}
		} else {
			// COB_CODE: MOVE SPACES
			//             TO CSR-MEDICARE-XOVER
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillingProvider().setMedicareXover(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CLM-PMT-ID OF DCLS02809SA TO CSR-PMT-ID
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPmtId(TruncAbs.toShort(ws.getDcls02809sa().getClmPmtId(), 2));
		// COB_CODE: MOVE WS-TRIG-LST-FNL-PMT-PT-ID TO
		//                                             CSR-CLM-FNLZD-PMT.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo()
				.setClmFnlzdPmt(TruncAbs.toShort(ws.getProgramHoldAreas().getWsTrigLstFnlPmtPtId(), 2));
		// COB_CODE: MOVE CLI-ID OF DCLS02809SA     TO CSR-LI-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setLiId(TruncAbs.toShort(ws.getDcls02809sa().getCliId(), 3));
		// COB_CODE: MOVE TRIG-GMIS-INDICATOR       TO CSR-GMIS-INDICATOR.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getGmisIndicator().setGmisIndicator(ws.getTrigRecordLayout().getTrigGmisIndicator());
		// COB_CODE: MOVE PROV-SBMT-LN-NO-ID OF DCLS02809SA
		//                                          TO CSR-PROV-SBMT-LN-NO-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setProvSbmtLnNoId(TruncAbs.toShort(ws.getDcls02809sa().getProvSbmtLnNoId(), 3));
		// COB_CODE: MOVE ADDNL-RMT-LI-ID OF DCLS02809SA
		//                                          TO CSR-ADDNL-RMT-LI-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAddnlRmtLiId(TruncAbs.toShort(ws.getDcls02809sa().getAddnlRmtLiId(), 3));
		// COB_CODE: MOVE TRIG-ALPH-PRFX-CD         TO CSR-INSURED-PREFIX
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setInsuredPrefix(ws.getTrigRecordLayout().getTrigAlphPrfxCd());
		// COB_CODE: MOVE TRIG-INS-ID               TO CSR-INSURED-NUMBER
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setInsuredNumber(ws.getTrigRecordLayout().getTrigInsId());
		// COB_CODE: MOVE TRIG-MEMBER-ID            TO CSR-MEMBER-ID
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMemberId(ws.getTrigRecordLayout().getTrigMemberId());
		// COB_CODE: MOVE TRIG-CORP-RCV-DT          TO CSR-CORP-RCV-DT
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getCorpRcvDt()
				.setCorpRcvDtFormatted(ws.getTrigRecordLayout().getTrigCorpRcvDtFormatted());
		// COB_CODE: MOVE TRIG-PMT-ADJD-DT          TO CSR-PMT-ADJD-DT
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtAdjdDt()
				.setCorpRcvDtFormatted(ws.getTrigRecordLayout().getTrigPmtAdjdDtFormatted());
		// COB_CODE: MOVE TRIG-PMT-FRM-SERV-DT      TO CSR-PMT-FRM-SERV-DT
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtFrmServDt()
				.setCorpRcvDtFormatted(ws.getTrigRecordLayout().getTrigPmtFrmServDtFormatted());
		// COB_CODE: MOVE TRIG-PMT-THR-SERV-DT      TO CSR-PMT-THR-SERV-DT
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtThrServDt()
				.setCorpRcvDtFormatted(ws.getTrigRecordLayout().getTrigPmtThrServDtFormatted());
		// COB_CODE: MOVE TRIG-BILL-FRM-DT          TO CSR-BILL-FRM-DT
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillFrmDt()
				.setCorpRcvDtFormatted(ws.getTrigRecordLayout().getTrigBillFrmDtFormatted());
		// COB_CODE: MOVE TRIG-BILL-THR-DT          TO CSR-BILL-THR-DT
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillThrDt()
				.setCorpRcvDtFormatted(ws.getTrigRecordLayout().getTrigBillThrDtFormatted());
		// COB_CODE: MOVE TRIG-ASG-CD               TO CSR-ASG-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAsgCd().setAsgCd(ws.getTrigRecordLayout().getTrigAsgCd());
		// COB_CODE: MOVE WS-TRIG-CLM-PD-AM         TO CSR-CLM-PD-AM
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setClmPdAm(Trunc.toDecimal(ws.getProgramHoldAreas().getWsTrigClmPdAm(), 11, 2));
		// COB_CODE: MOVE TRIG-ADJ-TYP-CD           TO CSR-ADJ-TYP-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjTypCd(ws.getTrigRecordLayout().getTrigAdjTypCd());
		// COB_CODE: MOVE TRIG-KCAPS-TEAM-NM        TO CSR-KCAPS-TEAM-NM.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setKcapsTeamNm(ws.getTrigRecordLayout().getTrigKcapsTeamNm());
		// COB_CODE: MOVE TRIG-KCAPS-USE-ID         TO CSR-KCAPS-USE-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setKcapsUseId(ws.getTrigRecordLayout().getTrigKcapsUseId());
		// COB_CODE: MOVE TRIG-HIST-LOAD-CD         TO CSR-HIST-LOAD-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setHistLoadCd(ws.getTrigRecordLayout().getTrigHistLoadCd());
		// COB_CODE: MOVE TRIG-PMT-BK-PROD-CD       TO CSR-PMT-BK-PROD-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtBkProdCd().setPmtBkProdCd(ws.getTrigRecordLayout().getTrigPmtBkProdCd());
		// COB_CODE: MOVE TRIG-DRG-CD               TO CSR-DRG-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setDrgCd(ws.getTrigRecordLayout().getTrigDrgCd());
		// COB_CODE: MOVE WS-TRIG-SCHDL-DRG-ALW-AM  TO CSR-SCHDL-DRG-ALW-AM.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo()
				.setSchdlDrgAlwAm(Trunc.toDecimal(ws.getProgramHoldAreas().getWsTrigSchdlDrgAlwAm(), 11, 2));
		// COB_CODE: MOVE WS-TRIG-ALT-DRG-ALW-AM    TO CSR-ALT-DRG-ALW-AM
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo()
				.setAltDrgAlwAm(Trunc.toDecimal(ws.getProgramHoldAreas().getWsTrigAltDrgAlwAm(), 11, 2));
		// COB_CODE: MOVE WS-TRIG-OVER-DRG-ALW-AM   TO CSR-OVER-DRG-ALW-AM
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo()
				.setOverDrgAlwAm(Trunc.toDecimal(ws.getProgramHoldAreas().getWsTrigOverDrgAlwAm(), 11, 2));
		// COB_CODE: MOVE TRIG-GMIS-INDICATOR       TO CSR-GMIS-INDICATOR
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getGmisIndicator().setGmisIndicator(ws.getTrigRecordLayout().getTrigGmisIndicator());
		// COB_CODE: MOVE TRIG-PMT-OI-IN            TO CSR-OI-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setOiCd(ws.getTrigRecordLayout().getTrigPmtOiIn());
		// COB_CODE: MOVE TRIG-PAT-ACT-MED-REC-ID   TO CSR-PAT-ACT-MED-REC.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatActMedRec(ws.getTrigRecordLayout().getTrigPatActMedRecId());
		// COB_CODE: MOVE TRIG-PGM-AREA-CD          TO CSR-PGM-AREA-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPgmAreaCd(ws.getTrigRecordLayout().getTrigPgmAreaCd());
		// COB_CODE: MOVE TRIG-FIN-CD               TO CSR-FIN-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setFinCd(ws.getTrigRecordLayout().getTrigFinCd());
		// COB_CODE: MOVE TRIG-ADJD-PROV-STAT-CD    TO CSR-ADJD-PROV-STAT
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdProvStat(ws.getTrigRecordLayout().getTrigAdjdProvStatCd());
		// COB_CODE: MOVE TRIG-ITS-CLM-TYP-CD       TO CSR-ITS-CLM-TYP-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getItsInformation().getClmTypCd()
				.setClmTypCd(ws.getTrigRecordLayout().getTrigItsClmTypCd());
		// COB_CODE: MOVE TRIG-ITS-CK               TO CSR-ITS-CK
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getItsInformation().setCk(ws.getTrigRecordLayout().getTrigItsCk());
		// COB_CODE: MOVE TRIG-DATE-COVERAGE-LAPSED    TO CSR-DATE-COVERAGE-LAPSED
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getDateCoverageLapsed()
				.setCorpRcvDtFormatted(ws.getTrigRecordLayout().getTrigDateCoverageLapsedFormatted());
		// COB_CODE: MOVE TRIG-OI-PAY-NM               TO CSR-OI-PAY-NM
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setOiPayNm(ws.getTrigRecordLayout().getTrigOiPayNm());
		// COB_CODE: MOVE TRIG-CORR-PRIORITY-SUB-ID TO
		//                                         CSR-CORRECTED-PRIORITY-SUBID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCorrectedPrioritySubid(ws.getTrigRecordLayout().getTrigCorrPrioritySubId());
		// COB_CODE: IF CLM-PMT-ID OF DCLS02809SA > 1
		//               MOVE 'REPAY'               TO CSR-RECORD-TYPE
		//           ELSE
		//               MOVE 'ORIG'                TO CSR-RECORD-TYPE
		//           END-IF
		if (ws.getDcls02809sa().getClmPmtId() > 1) {
			// COB_CODE: MOVE 'REPAY'               TO CSR-RECORD-TYPE
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getRecordType().setRecordType("REPAY");
		} else {
			// COB_CODE: MOVE 'ORIG'                TO CSR-RECORD-TYPE
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getRecordType().setRecordType("ORIG");
		}
		// COB_CODE: MOVE TRIG-VBR-IN               TO CSR-VBR-IN.
		ws.getNf07areaofCsrProgramLineArea().setVbrIn(ws.getTrigRecordLayout().getTrigVbrIn());
		// COB_CODE: MOVE TRIG-MRKT-PKG-CD          TO CSR-MRKT-PKG-CD.
		ws.getNf07areaofCsrProgramLineArea().setMrktPkgCd(ws.getTrigRecordLayout().getTrigMrktPkgCd());
		// COB_CODE: MOVE TRIG-HIPAA-VERSION-FORMAT-ID TO CSR-VERSION-FORMAT-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getVersionFormatId()
				.setVersionFormatId(ws.getTrigRecordLayout().getTrigHipaaVersionFormatId());
		// COB_CODE: MOVE TRIG-ITS-INS-ID           TO CSR-ITS-INS-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getItsInformation()
				.setInsIdFormatted(ws.getTrigRecordLayout().getTrigItsInsIdFormatted());
		// COB_CODE: MOVE TRIG-PRMPT-PAY-DAY-CD     TO CSR-PRMPT-PAY-DAY-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPrmptPayDayCd(ws.getTrigRecordLayout().getTrigPrmptPayDayCd());
		// COB_CODE: MOVE TRIG-PRMPT-PAY-OVRD-CD    TO CSR-PRMPT-PAY-OVRD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPrmptPayOvrd().setPrmptPayOvrd(ws.getTrigRecordLayout().getTrigPrmptPayOvrdCd());
		// COB_CODE: MOVE WS-ACCRUED-PRMPT-PAY-INT-AM TO
		//                CSR-ACCRUED-PROMPT-PAY-INT
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo()
				.setAccruedPromptPayInt(Trunc.toDecimal(ws.getProgramHoldAreas().getWsAccruedPrmptPayIntAm(), 11, 2));
		// COB_CODE: MOVE TRIG-PROV-UNWRP-DT        TO CSR-PROV-UNWRP-DT
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getProvUnwrpDt()
				.setCorpRcvDtFormatted(ws.getTrigRecordLayout().getTrigProvUnwrpDtFormatted());
		// COB_CODE: MOVE TRIG-GRP-ID               TO CSR-GRP-ID
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setGrpId(ws.getTrigRecordLayout().getTrigGrpId());
		// COB_CODE: MOVE TRIG-TYP-GRP-CD           TO CSR-TYP-GRP-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setTypGrpCd(ws.getTrigRecordLayout().getTrigTypGrpCd());
		// COB_CODE: MOVE TRIG-RATE-CD              TO CSR-RATE-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setRateCd(ws.getTrigRecordLayout().getTrigRateCd());
		// COB_CODE: MOVE TRIG-NTWRK-CD             TO CSR-NTWRK-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setNtwrkCd(ws.getTrigRecordLayout().getTrigNtwrkCd());
		// COB_CODE: MOVE TRIG-BASE-CN-ARNG-CD      TO CSR-BASE-CN-ARNG-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBaseCnArngCd(ws.getTrigRecordLayout().getTrigBaseCnArngCd());
		// COB_CODE: MOVE TRIG-PRIM-CN-ARNG-CD      TO CSR-PRIM-CN-ARNG-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPrimCnArngCd(ws.getTrigRecordLayout().getTrigPrimCnArngCd());
		// COB_CODE: MOVE TRIG-ENR-CL-CD            TO CSR-ENR-CL-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setEnrClCd(ws.getTrigRecordLayout().getTrigEnrClCd());
		// COB_CODE: MOVE TRIG-STAT-ADJ-PREV-PMT    TO CSR-STAT-ADJ-PREV.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setStatAdjPrev(ws.getTrigRecordLayout().getTrigStatAdjPrevPmt());
		// COB_CODE: MOVE TRIG-EFT-IND                   TO CSR-EFT-IND.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setEftInd(ws.getTrigRecordLayout().getTrigEftInd());
		// COB_CODE: MOVE TRIG-EFT-ACCOUNT-TYPE          TO CSR-EFT-ACCOUNT-TYPE.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setEftAccountType(ws.getTrigRecordLayout().getTrigEftAccountType());
		// COB_CODE: MOVE TRIG-EFT-ACCT                  TO CSR-EFT-ACCT.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setEftAcct(ws.getTrigRecordLayout().getTrigEftAcct());
		// COB_CODE: MOVE TRIG-EFT-TRANS                 TO CSR-EFT-TRANS.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setEftTrans(ws.getTrigRecordLayout().getTrigEftTrans());
		// COB_CODE: MOVE LI-FRM-SERV-DT OF DCLS02809SA  TO CSR-LI-FRM-SERV-DT.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getLiFrmServDt().setCorpRcvDtFormatted(ws.getDcls02809sa().getLiFrmServDtFormatted());
		// COB_CODE: MOVE LI-THR-SERV-DT OF DCLS02809SA  TO CSR-LI-THR-SERV-DT.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getLiThrServDt().setCorpRcvDtFormatted(ws.getDcls02809sa().getLiThrServDtFormatted());
		// COB_CODE: MOVE BEN-TYP-CD     OF DCLS02809SA  TO CSR-BEN-TYP-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBenTypCd(ws.getDcls02809sa().getBenTypCd());
		// COB_CODE: MOVE EXMN-ACTN-CD   OF DCLS02809SA  TO CSR-EXMN-ACTN-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getExmnActnCd().setExmnActnCd(ws.getDcls02809sa().getExmnActnCd());
		// COB_CODE: MOVE DNL-RMRK-CD    OF DCLS02809SA  TO CSR-DNL-RMRK-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setDnlRmrkCd(ws.getDcls02809sa().getDnlRmrkCd());
		// COB_CODE: MOVE DNL-RMRK-USG-CD OF DCLS02993SA TO CSR-DNL-RMRK-USG-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setDnlRmrkUsgCd(ws.getDcls02993sa().getDnlRmrkUsgCd());
		// COB_CODE: MOVE PRMPT-PAY-DNL-IN OF DCLS02993SA TO CSR-PRMPT-PAY-DNL-IN
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPrmptPayDnlIn(ws.getDcls02993sa().getPrmptPayDnlIn());
		// COB_CODE: MOVE LI-EXPLN-CD    OF DCLS02809SA  TO CSR-LI-EXPLN-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setLiExplnCd(ws.getDcls02809sa().getLiExplnCd());
		// COB_CODE: MOVE RVW-BY-CD      OF DCLS02809SA  TO CSR-RVW-BY-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setRvwByCd(ws.getDcls02809sa().getRvwByCd());
		// COB_CODE: MOVE PROC-CD        OF DCLS02809SA  TO CSR-PROC-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setCd(ws.getDcls02809sa().getProcCd());
		// COB_CODE: MOVE PROC-MOD1-CD   OF DCLS02809SA  TO CSR-PROC-MOD1-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod1Cd(ws.getDcls02809sa().getProcMod1Cd());
		// COB_CODE: MOVE PROC-MOD2-CD   OF DCLS02809SA  TO CSR-PROC-MOD2-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod2Cd(ws.getDcls02809sa().getProcMod2Cd());
		// COB_CODE: MOVE PROC-MOD3-CD   OF DCLS02809SA  TO CSR-PROC-MOD3-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod3Cd(ws.getDcls02809sa().getProcMod3Cd());
		// COB_CODE: MOVE PROC-MOD4-CD   OF DCLS02809SA  TO CSR-PROC-MOD4-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod4Cd(ws.getDcls02809sa().getProcMod4Cd());
		// COB_CODE: MOVE REV-CD         OF DCLS02809SA  TO CSR-REV-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setRevCd(ws.getDcls02809sa().getRevCd());
		// COB_CODE: MOVE UNIT-CT        OF DCLS02809SA  TO CSR-UNIT-CT.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setUnitCt(Trunc.toDecimal(ws.getDcls02809sa().getUnitCt(), 7, 2));
		// COB_CODE: MOVE ORIG-SBMT-UNIT-CT OF DCLS02809SA TO CSR-ORIG-SUB-UNIT-CT
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setOrigSubUnitCt(Trunc.toDecimal(ws.getDcls02809sa().getOrigSbmtUnitCt(), 7, 2));
		// COB_CODE: MOVE POS-CD         OF DCLS02809SA  TO CSR-PLACE-OF-SERVICE
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPlaceOfService(ws.getDcls02809sa().getPosCd());
		// COB_CODE: MOVE TS-CD          OF DCLS02809SA  TO CSR-TYPE-OF-SERVICE
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setTypeOfService(ws.getDcls02809sa().getTsCd());
		// COB_CODE: MOVE ORIG-SBMT-CHG-AM OF DCLS02809SA
		//             TO CSR-ORIG-SBMT-CHG-AM.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setOrigSbmtChgAm(Trunc.toDecimal(ws.getDcls02809sa().getOrigSbmtChgAm(), 11, 2));
		// COB_CODE: MOVE CHG-AM         OF DCLS02809SA  TO CSR-CHG-AM.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setChgAm(Trunc.toDecimal(ws.getDcls02809sa().getChgAm(), 11, 2));
		// COB_CODE: MOVE LI-ALCT-OI-COV-AM OF DCLS02809SA
		//             TO CSR-LI-ALCT-OI-COV-AM
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setLiAlctOiCovAm(Trunc.toDecimal(ws.getDcls02809sa().getLiAlctOiCovAm(), 11, 2));
		// COB_CODE: MOVE LI-ALCT-OI-PD-AM OF DCLS02809SA
		//             TO CSR-LI-ALCT-OI-PD-AM.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setLiAlctOiPdAm(Trunc.toDecimal(ws.getDcls02809sa().getLiAlctOiPdAm(), 11, 2));
		// COB_CODE: MOVE LI-ALCT-OI-SAVE-AM OF DCLS02809SA
		//             TO CSR-LI-ALCT-OI-SAVE-AM
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setLiAlctOiSaveAm(Trunc.toDecimal(ws.getDcls02809sa().getLiAlctOiSaveAm(), 11, 2));
		// COB_CODE: MOVE TRIG-ADJD-PROV-STAT-CD TO CSR-ADJD-PROV-STAT-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdProvStatCd(ws.getTrigRecordLayout().getTrigAdjdProvStatCd());
		// COB_CODE: MOVE LI-ALW-CHG-AM   OF DCLS02809SA  TO CSR-LI-ALW-CHG-AM
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setLiAlwChgAm(Trunc.toDecimal(ws.getDcls02809sa().getLiAlwChgAm(), 11, 2));
		// COB_CODE: MOVE TRIG-GL-OFST-ORIG-CD            TO CSR-GL-OFST-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setGlOfstCd(ws.getTrigRecordLayout().getTrigGlOfstOrigCd());
		// COB_CODE: MOVE WS-TRIG-GL-SOTE-ORIG-CD         TO CSR-GL-SOTE-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setGlSoteCd(TruncAbs.toShort(ws.getProgramHoldAreas().getWsTrigGlSoteOrigCd(), 2));
		// COB_CODE: MOVE FEP-EDT-OVRD1-CD OF DCLS02809SA
		//             TO CSR-FEP-EDT-OVRD1-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setFepEdtOvrd1Cd(ws.getDcls02809sa().getFepEdtOvrd1Cd());
		// COB_CODE: MOVE FEP-EDT-OVRD2-CD OF DCLS02809SA
		//             TO CSR-FEP-EDT-OVRD2-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setFepEdtOvrd2Cd(ws.getDcls02809sa().getFepEdtOvrd2Cd());
		// COB_CODE: MOVE FEP-EDT-OVRD3-CD OF DCLS02809SA
		//             TO CSR-FEP-EDT-OVRD3-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setFepEdtOvrd3Cd(ws.getDcls02809sa().getFepEdtOvrd3Cd());
		// COB_CODE: MOVE ADJD-OVRD1-CD OF DCLS02809SA    TO CSR-ADJD-OVRD1-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd1Cd(ws.getDcls02809sa().getAdjdOvrd1Cd());
		// COB_CODE: MOVE ADJD-OVRD2-CD OF DCLS02809SA    TO CSR-ADJD-OVRD2-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd2Cd(ws.getDcls02809sa().getAdjdOvrd2Cd());
		// COB_CODE: MOVE ADJD-OVRD3-CD OF DCLS02809SA    TO CSR-ADJD-OVRD3-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd3Cd(ws.getDcls02809sa().getAdjdOvrd3Cd());
		// COB_CODE: MOVE ADJD-OVRD4-CD OF DCLS02809SA    TO CSR-ADJD-OVRD4-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd4Cd(ws.getDcls02809sa().getAdjdOvrd4Cd());
		// COB_CODE: MOVE ADJD-OVRD5-CD OF DCLS02809SA    TO CSR-ADJD-OVRD5-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd5Cd(ws.getDcls02809sa().getAdjdOvrd5Cd());
		// COB_CODE: MOVE ADJD-OVRD6-CD OF DCLS02809SA    TO CSR-ADJD-OVRD6-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd6Cd(ws.getDcls02809sa().getAdjdOvrd6Cd());
		// COB_CODE: MOVE ADJD-OVRD7-CD OF DCLS02809SA    TO CSR-ADJD-OVRD7-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd7Cd(ws.getDcls02809sa().getAdjdOvrd7Cd());
		// COB_CODE: MOVE ADJD-OVRD8-CD OF DCLS02809SA    TO CSR-ADJD-OVRD8-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd8Cd(ws.getDcls02809sa().getAdjdOvrd8Cd());
		// COB_CODE: MOVE ADJD-OVRD9-CD OF DCLS02809SA    TO CSR-ADJD-OVRD9-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd9Cd(ws.getDcls02809sa().getAdjdOvrd9Cd());
		// COB_CODE: MOVE ADJD-OVRD10-CD OF DCLS02809SA   TO CSR-ADJD-OVRD10-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd10Cd(ws.getDcls02809sa().getAdjdOvrd10Cd());
		// COB_CODE: MOVE TRIG-VOID-CD                    TO CSR-VOID-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setVoidCd(ws.getTrigRecordLayout().getTrigVoidCd());
		// COB_CODE: MOVE TRIG-ADJ-RESP-CD                TO CSR-ADJ-RESP-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjRespCd(ws.getTrigRecordLayout().getTrigAdjRespCd());
		// COB_CODE: MOVE TRIG-ADJ-TRCK-CD                TO CSR-ADJ-TRCK-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjTrckCd(ws.getTrigRecordLayout().getTrigAdjTrckCd());
		// COB_CODE: MOVE TRIG-CLAIM-LOB-CD            TO CSR-CLAIM-LOB-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClaimLobCd().setClaimLobCd(ws.getTrigRecordLayout().getTrigClaimLobCd());
		//
		// COB_CODE: INITIALIZE CSR-ORIGINAL-SUBMITTED-INFO
		initOriginalSubmittedInfo();
		//
		// COB_CODE: MOVE REF-ID OF DCLS02713SA          TO CSR-REF-ID
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setRefId(ws.getDcls02713sa().getRefId());
		//
		// COB_CODE: MOVE CLM-CNTL-ID OF DCLS02809SA     TO CSR-CLM-CNTL-ID
		//                                                  WS-CLM-CNTL-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setClmCntlIdFormatted(ws.getDcls02809sa().getClmCntlIdFormatted());
		ws.getWsSelectFields().setClmCntlId(ws.getDcls02809sa().getClmCntlId());
		// COB_CODE: MOVE CLM-CNTL-SFX-ID OF DCLS02809SA TO CSR-CLM-CNTL-SFX-ID
		//                                                  WS-CLM-CNTL-SFX-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setClmCntlSfxId(ws.getDcls02809sa().getClmCntlSfxId());
		ws.getWsSelectFields().setClmCntlSfxId(ws.getDcls02809sa().getClmCntlSfxId());
		// COB_CODE: MOVE CLM-PMT-ID OF DCLS02809SA      TO CSR-PMT-ID
		//                                                  WS-CLM-PMT-ID
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPmtId(TruncAbs.toShort(ws.getDcls02809sa().getClmPmtId(), 2));
		ws.getWsSelectFields().setClmPmtId(ws.getDcls02809sa().getClmPmtId());
		// COB_CODE: MOVE WS-TRIG-LST-FNL-PMT-PT-ID      TO CSR-CLM-FNLZD-PMT.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo()
				.setClmFnlzdPmt(TruncAbs.toShort(ws.getProgramHoldAreas().getWsTrigLstFnlPmtPtId(), 2));
		//**------------------------------------------------------------
		// COB_CODE: MOVE PROV-SBMT-LN-NO-ID OF DCLS02809SA TO
		//                                                  CSR-PROV-SBMT-LN-NO-ID
		//                                                  WS-PROV-SBMT-LN-NO-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setProvSbmtLnNoId(TruncAbs.toShort(ws.getDcls02809sa().getProvSbmtLnNoId(), 3));
		ws.getWsSelectFields().setProvSbmtLnNoId(ws.getDcls02809sa().getProvSbmtLnNoId());
		//**-------------------------------------------------
		//**-------------------------------------------------
		// COB_CODE: MOVE 'N' TO WS-PROFESSIONAL-DATA-FOUND-SW
		//                       WS-INSTITUTIONAL-DATA-FOUND-SW
		ws.getProgramSwitches().setWsProfessionalDataFoundSwFormatted("N");
		ws.getProgramSwitches().setWsInstitutionalDataFoundSwFormatted("N");
		// COB_CODE: PERFORM 6022-GET-PROFESSIONAL-DATA
		getProfessionalData();
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                        CSR-ORIG-QUANTITY-COUNT
		//               WHEN 100
		//                   INITIALIZE CSR-ORIGINAL-SUBMITTED-INFO
		//           END-EVALUATE
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE 'Y' TO WS-PROFESSIONAL-DATA-FOUND-SW
			ws.getProgramSwitches().setWsProfessionalDataFoundSwFormatted("Y");
			// COB_CODE: MOVE PROD-SRV-ID        OF DCLS02717SA TO
			//                CSR-ORIG-PROCEDURE-SRV-ID
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureSrvId(ws.getDcls02717sa().getProdSrvId());
			// COB_CODE: MOVE PROD-SRV-ID-QLF-CD OF DCLS02717SA TO
			//                CSR-ORIG-ID-QLF-CD
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setIdQlfCd(ws.getDcls02717sa().getProdSrvIdQlfCd());
			// COB_CODE: MOVE SV1013-PROC-MOD-CD OF DCLS02717SA TO
			//                CSR-ORIG-PROCEDURE-MOD-1
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
					.setProcedureMod1(ws.getDcls02717sa().getSv1013ProcModCd());
			// COB_CODE: MOVE SV1014-PROC-MOD-CD OF DCLS02717SA TO
			//                CSR-ORIG-PROCEDURE-MOD-2
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
					.setProcedureMod2(ws.getDcls02717sa().getSv1014ProcModCd());
			// COB_CODE: MOVE SV1015-PROC-MOD-CD OF DCLS02717SA TO
			//                CSR-ORIG-PROCEDURE-MOD-3
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
					.setProcedureMod3(ws.getDcls02717sa().getSv1015ProcModCd());
			// COB_CODE: MOVE SV1016-PROC-MOD-CD OF DCLS02717SA TO
			//                CSR-ORIG-PROCEDURE-MOD-4
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
					.setProcedureMod4(ws.getDcls02717sa().getSv1016ProcModCd());
			// COB_CODE: MOVE SV102-MNTRY-AM     OF DCLS02717SA TO
			//                CSR-ORIG-MONETARY-AMOUNT
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
					.setMonetaryAmount(Trunc.toDecimal(ws.getDcls02717sa().getSv102MntryAm(), 11, 2));
			// COB_CODE: MOVE QNTY-CT            OF DCLS02717SA TO
			//                CSR-ORIG-QUANTITY-COUNT
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
					.setQuantityCount(Trunc.toDecimal(ws.getDcls02717sa().getQntyCt(), 7, 2));
			break;

		case 100:// COB_CODE: INITIALIZE CSR-ORIGINAL-SUBMITTED-INFO
			initOriginalSubmittedInfo();
			break;

		default:
			break;
		}
		//
		// COB_CODE: IF PROFESSIONAL-DATA-FOUND
		//              CONTINUE
		//           ELSE
		//               END-EVALUATE
		//           END-IF
		if (!ws.getProgramSwitches().isProfessionalDataFound()) {
			// COB_CODE: PERFORM 6021-GET-INSTITUTIONAL-DATA
			getInstitutionalData();
			// COB_CODE:          EVALUATE SQLCODE
			//                    WHEN 0
			//                             CSR-ORIG-QUANTITY-COUNT
			//                    WHEN 100
			//           *
			//                        INITIALIZE CSR-ORIGINAL-SUBMITTED-INFO
			//                    END-EVALUATE
			switch (sqlca.getSqlcode()) {

			case 0:// COB_CODE: MOVE 'Y' TO WS-INSTITUTIONAL-DATA-FOUND-SW
				ws.getProgramSwitches().setWsInstitutionalDataFoundSwFormatted("Y");
				// COB_CODE: MOVE SV201-PROD-SRV-ID  OF DCLS02718SA TO
				//                CSR-ORIG-REVENUE-SRV-ID
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setRevenueSrvId(ws.getDcls02718sa().getSv201ProdSrvId());
				// COB_CODE: MOVE SV2022-PROD-SRV-ID  OF DCLS02718SA TO
				//                CSR-ORIG-PROCEDURE-SRV-ID
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureSrvId(ws.getDcls02718sa().getSv2022ProdSrvId());
				// COB_CODE: MOVE PROD-SRV-ID-QLF-CD OF DCLS02718SA TO
				//                CSR-ORIG-ID-QLF-CD
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setIdQlfCd(ws.getDcls02718sa().getProdSrvIdQlfCd());
				// COB_CODE: MOVE SV2023-PROC-MOD-CD OF DCLS02718SA TO
				//                CSR-ORIG-PROCEDURE-MOD-1
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod1(ws.getDcls02718sa().getSv2023ProcModCd());
				// COB_CODE: MOVE SV2024-PROC-MOD-CD OF DCLS02718SA TO
				//                CSR-ORIG-PROCEDURE-MOD-2
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod2(ws.getDcls02718sa().getSv2024ProcModCd());
				// COB_CODE: MOVE SV2025-PROC-MOD-CD OF DCLS02718SA TO
				//                CSR-ORIG-PROCEDURE-MOD-3
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod3(ws.getDcls02718sa().getSv2025ProcModCd());
				// COB_CODE: MOVE SV2026-PROC-MOD-CD OF DCLS02718SA TO
				//                CSR-ORIG-PROCEDURE-MOD-4
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod4(ws.getDcls02718sa().getSv2026ProcModCd());
				// COB_CODE: MOVE SV203-MNTRY-AM     OF DCLS02718SA TO
				//                CSR-ORIG-MONETARY-AMOUNT
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setMonetaryAmount(Trunc.toDecimal(ws.getDcls02718sa().getSv203MntryAm(), 11, 2));
				// COB_CODE: MOVE QNTY-CT            OF DCLS02718SA TO
				//                CSR-ORIG-QUANTITY-COUNT
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setQuantityCount(Trunc.toDecimal(ws.getDcls02718sa().getQntyCt(), 7, 2));
				break;

			case 100://
				// COB_CODE: INITIALIZE CSR-ORIGINAL-SUBMITTED-INFO
				initOriginalSubmittedInfo();
				break;

			default:
				break;
			}
		}
		//*
		// COB_CODE: IF PROFESSIONAL-DATA-FOUND OR
		//              INSTITUTIONAL-DATA-FOUND
		//              CONTINUE
		//           ELSE
		//               END-IF
		//           END-IF
		if (!(ws.getProgramSwitches().isProfessionalDataFound() || ws.getProgramSwitches().isInstitutionalDataFound())) {
			// COB_CODE: PERFORM 6020-GET-DENTAL-DATA
			getDentalData();
			// COB_CODE: IF SQLCODE = 0
			//                    CSR-ORIG-QUANTITY-COUNT
			//           ELSE
			//               INITIALIZE CSR-ORIGINAL-SUBMITTED-INFO
			//           END-IF
			if (sqlca.getSqlcode() == 0) {
				// COB_CODE: MOVE PROD-SRV-ID        OF DCLS02719SA TO
				//                CSR-ORIG-PROCEDURE-SRV-ID
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureSrvId(ws.getDcls02719sa().getProdSrvId());
				// COB_CODE: MOVE PROD-SRV-ID-QLF-CD OF DCLS02719SA TO
				//                CSR-ORIG-ID-QLF-CD
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setIdQlfCd(ws.getDcls02719sa().getProdSrvIdQlfCd());
				// COB_CODE: MOVE SV3013-PROC-MOD-CD OF DCLS02719SA TO
				//                CSR-ORIG-PROCEDURE-MOD-1
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod1(ws.getDcls02719sa().getSv3013ProcModCd());
				// COB_CODE: MOVE SV3014-PROC-MOD-CD OF DCLS02719SA TO
				//                CSR-ORIG-PROCEDURE-MOD-2
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod2(ws.getDcls02719sa().getSv3014ProcModCd());
				// COB_CODE: MOVE SV3015-PROC-MOD-CD OF DCLS02719SA TO
				//                CSR-ORIG-PROCEDURE-MOD-3
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod3(ws.getDcls02719sa().getSv3015ProcModCd());
				// COB_CODE: MOVE SV3016-PROC-MOD-CD OF DCLS02719SA TO
				//                CSR-ORIG-PROCEDURE-MOD-4
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod4(ws.getDcls02719sa().getSv3016ProcModCd());
				// COB_CODE: MOVE MNTRY-AM           OF DCLS02719SA TO
				//                CSR-ORIG-MONETARY-AMOUNT
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setMonetaryAmount(Trunc.toDecimal(ws.getDcls02719sa().getMntryAm(), 11, 2));
				// COB_CODE: MOVE QNTY-CT            OF DCLS02719SA TO
				//                CSR-ORIG-QUANTITY-COUNT
				ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setQuantityCount(Trunc.toDecimal(ws.getDcls02719sa().getQntyCt(), 7, 2));
			} else {
				// COB_CODE: INITIALIZE CSR-ORIGINAL-SUBMITTED-INFO
				initOriginalSubmittedInfo();
			}
		}
		//
		//
		//
		// COB_CODE: PERFORM 6030-GET-MEMBER-DATA
		getMemberData();
		//
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//               MOVE SSN-ID     OF DCLS02952SA TO CSR-MBR-SSN-ID
		//             WHEN +100
		//                               CSR-MBR-SSN-ID
		//                               CSR-MBR-SSN-ID
		//                               CSR-MBR-SSN-ID
		//                               CSR-MBR-SSN-ID
		//                               CSR-MBR-SSN-ID
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE LST-ORG-NM OF DCLS02952SA TO CSR-MBR-LST-ORG-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrLstOrgNm(ws.getDcls02952sa().getLstOrgNm());
			// COB_CODE: MOVE FRST-NM    OF DCLS02952SA TO CSR-MBR-FRST-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrFrstNm(ws.getDcls02952sa().getFrstNm());
			// COB_CODE: MOVE MID-NM     OF DCLS02952SA TO CSR-MBR-MID-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrMidNm(ws.getDcls02952sa().getMidNm());
			// COB_CODE: MOVE SFX-NM     OF DCLS02952SA TO CSR-MBR-SFX-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrSfxNm(ws.getDcls02952sa().getSfxNm());
			// COB_CODE: MOVE SSN-ID     OF DCLS02952SA TO CSR-MBR-SSN-ID
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrSsnId(ws.getDcls02952sa().getSsnId());
			break;

		case 100:// COB_CODE: MOVE SPACES  TO CSR-MBR-LST-ORG-NM
			//                           CSR-MBR-FRST-NM
			//                           CSR-MBR-MID-NM
			//                           CSR-MBR-SFX-NM
			//                           CSR-MBR-SSN-ID
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrLstOrgNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrFrstNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrMidNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrSfxNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrSsnId("");
			break;

		default:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02952SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02952SA");
			// COB_CODE: MOVE 'READ MBR DATA     '  TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("READ MBR DATA     ");
			// COB_CODE: MOVE '6030-GET-MEMBER   '  TO DB2-ERR-PARA
			ws.setDb2ErrPara("6030-GET-MEMBER   ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
		//
		// COB_CODE: PERFORM 6031-GET-PATIENT-DATA
		getPatientData();
		//
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//               MOVE SSN-ID     OF DCLS02952SA TO CSR-PAT-SSN-ID
		//             WHEN +100
		//                               CSR-PAT-SSN-ID
		//                               CSR-PAT-SSN-ID
		//                               CSR-PAT-SSN-ID
		//                               CSR-PAT-SSN-ID
		//                               CSR-PAT-SSN-ID
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE LST-ORG-NM OF DCLS02952SA TO CSR-PAT-LST-ORG-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatLstOrgNm(ws.getDcls02952sa().getLstOrgNm());
			// COB_CODE: MOVE FRST-NM    OF DCLS02952SA TO CSR-PAT-FRST-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatFrstNm(ws.getDcls02952sa().getFrstNm());
			// COB_CODE: MOVE MID-NM     OF DCLS02952SA TO CSR-PAT-MID-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatMidNm(ws.getDcls02952sa().getMidNm());
			// COB_CODE: MOVE SFX-NM     OF DCLS02952SA TO CSR-PAT-SFX-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatSfxNm(ws.getDcls02952sa().getSfxNm());
			// COB_CODE: MOVE SSN-ID     OF DCLS02952SA TO CSR-PAT-SSN-ID
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatSsnId(ws.getDcls02952sa().getSsnId());
			break;

		case 100:// COB_CODE: MOVE SPACES  TO CSR-PAT-LST-ORG-NM
			//                           CSR-PAT-FRST-NM
			//                           CSR-PAT-MID-NM
			//                           CSR-PAT-SFX-NM
			//                           CSR-PAT-SSN-ID
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatLstOrgNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatFrstNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatMidNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatSfxNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatSsnId("");
			break;

		default:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02952SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02952SA");
			// COB_CODE: MOVE 'READ PATIENT DATA '  TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("READ PATIENT DATA ");
			// COB_CODE: MOVE '6031-GET-PATIENT  '  TO DB2-ERR-PARA
			ws.setDb2ErrPara("6031-GET-PATIENT  ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
		//
		// COB_CODE: PERFORM 6032-GET-PERF-DATA
		getPerfData();
		//
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                                        TO CSR-PERF-ENTY-TYP-QLF-CD
		//             WHEN +100
		//                               CSR-PERF-ENTY-TYP-QLF-CD
		//                               CSR-PERF-ENTY-TYP-QLF-CD
		//                               CSR-PERF-ENTY-TYP-QLF-CD
		//                               CSR-PERF-ENTY-TYP-QLF-CD
		//                               CSR-PERF-ENTY-TYP-QLF-CD
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE LST-ORG-NM OF DCLS02952SA TO CSR-PERF-LST-ORG-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfLstOrgNm(ws.getDcls02952sa().getLstOrgNm());
			// COB_CODE: MOVE FRST-NM    OF DCLS02952SA TO CSR-PERF-FRST-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfFrstNm(ws.getDcls02952sa().getFrstNm());
			// COB_CODE: MOVE MID-NM     OF DCLS02952SA TO CSR-PERF-MID-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfMidNm(ws.getDcls02952sa().getMidNm());
			// COB_CODE: MOVE SFX-NM     OF DCLS02952SA TO CSR-PERF-SFX-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfSfxNm(ws.getDcls02952sa().getSfxNm());
			// COB_CODE: MOVE ENTY-TYP-QLF-CD OF DCLS02952SA
			//                                    TO CSR-PERF-ENTY-TYP-QLF-CD
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfEntyTypQlfCd(ws.getDcls02952sa().getEntyTypQlfCd());
			break;

		case 100:// COB_CODE: MOVE SPACES  TO CSR-PERF-LST-ORG-NM
			//                           CSR-PERF-FRST-NM
			//                           CSR-PERF-MID-NM
			//                           CSR-PERF-SFX-NM
			//                           CSR-PERF-ENTY-TYP-QLF-CD
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfLstOrgNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfFrstNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfMidNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfSfxNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfEntyTypQlfCd(Types.SPACE_CHAR);
			break;

		default:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02952SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02952SA");
			// COB_CODE: MOVE 'READ PERF DATA    '  TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("READ PERF DATA    ");
			// COB_CODE: MOVE '6033-PERF-DATA    '  TO DB2-ERR-PARA
			ws.setDb2ErrPara("6033-PERF-DATA    ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
		//
		// COB_CODE: PERFORM 6033-GET-BILLED-DATA
		getBilledData();
		//
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                                            TO CSR-BILL-ENTY-TYP-QLF-CD
		//             WHEN +100
		//                               CSR-BILL-ENTY-TYP-QLF-CD
		//                               CSR-BILL-ENTY-TYP-QLF-CD
		//                               CSR-BILL-ENTY-TYP-QLF-CD
		//                               CSR-BILL-ENTY-TYP-QLF-CD
		//                               CSR-BILL-ENTY-TYP-QLF-CD
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE LST-ORG-NM OF DCLS02952SA TO CSR-BILL-LST-ORG-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillLstOrgNm(ws.getDcls02952sa().getLstOrgNm());
			// COB_CODE: MOVE FRST-NM    OF DCLS02952SA TO CSR-BILL-FRST-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillFrstNm(ws.getDcls02952sa().getFrstNm());
			// COB_CODE: MOVE MID-NM     OF DCLS02952SA TO CSR-BILL-MID-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillMidNm(ws.getDcls02952sa().getMidNm());
			// COB_CODE: MOVE SFX-NM     OF DCLS02952SA TO CSR-BILL-SFX-NM
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillSfxNm(ws.getDcls02952sa().getSfxNm());
			// COB_CODE: MOVE ENTY-TYP-QLF-CD OF DCLS02952SA
			//                                        TO CSR-BILL-ENTY-TYP-QLF-CD
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillEntyTypQlfCd(ws.getDcls02952sa().getEntyTypQlfCd());
			break;

		case 100:// COB_CODE: MOVE SPACES  TO CSR-BILL-LST-ORG-NM
			//                           CSR-BILL-FRST-NM
			//                           CSR-BILL-MID-NM
			//                           CSR-BILL-SFX-NM
			//                           CSR-BILL-ENTY-TYP-QLF-CD
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillLstOrgNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillFrstNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillMidNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillSfxNm("");
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillEntyTypQlfCd(Types.SPACE_CHAR);
			break;

		default:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02952SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02952SA");
			// COB_CODE: MOVE 'READ BILLING DATA '  TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("READ BILLING DATA ");
			// COB_CODE: MOVE '6032-GET-BILLING  '  TO DB2-ERR-PARA
			ws.setDb2ErrPara("6032-GET-BILLING  ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
		//
		// COB_CODE: MOVE ID-CD OF DCLS02682SA       TO CSR-837-INS-ID
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837InsIdFormatted(ws.getDcls02682sa().getIdCdFormatted());
		// COB_CODE: MOVE LST-ORG-NM OF DCLS02682SA  TO CSR-837-INS-LST-NM
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837InsLstNm(ws.getDcls02682sa().getLstOrgNm());
		// COB_CODE: MOVE FRST-NM OF DCLS02682SA     TO CSR-837-INS-FRST-NM
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837InsFrstNm(ws.getDcls02682sa().getFrstNm());
		// COB_CODE: MOVE MID-NM OF DCLS02682SA      TO CSR-837-INS-MID-NM
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837InsMidNm(ws.getDcls02682sa().getMidNm());
		// COB_CODE: MOVE SFX-NM OF DCLS02682SA      TO CSR-837-INS-SFX-NM
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837InsSfxNm(ws.getDcls02682sa().getSfxNm());
		//
		// COB_CODE: MOVE CLM-SBMT-ID OF DCLS02652SA TO CSR-837-CLM-SBMT-ID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837ClmSbmtId(ws.getDcls02652sa().getClmSbmtId());
		// COB_CODE: MOVE FAC-VL-CD   OF DCLS02652SA TO CSR-837-FAC-VL-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837FacVlCd(ws.getDcls02652sa().getFacVlCd());
		// COB_CODE: MOVE FAC-QLF-CD  OF DCLS02652SA TO CSR-837-FAC-QLF-CD
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837FacQlfCd(ws.getDcls02652sa().getFacQlfCd());
		// COB_CODE: MOVE CLM-FREQ-TYP-CD OF DCLS02652SA
		//             TO CSR-837-CLM-FREQ-TYP-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837ClmFreqTypCd(ws.getDcls02652sa().getClmFreqTypCd());
		// COB_CODE: MOVE TRIG-837-BILL-PROV-NPI-ID TO CSR-837-BILL-PROV-NPI-ID
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837BillProvNpiId(ws.getTrigRecordLayout().getTrig837BillProvNpiId());
		// COB_CODE: MOVE TRIG-837-PERF-PROV-NPI-ID TO CSR-837-PERF-PROV-NPI-ID
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837PerfProvNpiId(ws.getTrigRecordLayout().getTrig837PerfProvNpiId());
		//---------------------------------------------------------
		//  GET CALC INFORMATION DATA
		//---------------------------------------------------------
		// COB_CODE: MOVE CSR-CLM-CNTL-ID           TO WS-CALC-CLM-CNTL-ID.
		ws.getCalcFields().getCalcRecordKey().setCntlId(ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmCntlIdFormatted());
		// COB_CODE: MOVE CSR-CLM-CNTL-SFX-ID       TO WS-CALC-CLM-SFX-ID.
		ws.getCalcFields().getCalcRecordKey().setSfxId(ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId());
		// COB_CODE: MOVE CSR-PMT-ID                TO WS-CALC-CLM-PMT-NUM.
		ws.getCalcFields().getCalcRecordKey().setPmtNum(ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtId());
		// COB_CODE: MOVE CSR-LI-ID                 TO WS-CALC-CLM-LI-ID-PACKED.
		ws.getCalcFields().getCalcRecordKey().setLiIdPacked(ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getLiId());
		//
		// COB_CODE: PERFORM 2200-GET-CALC-INFO.
		getCalcInfo();
		//
		//---------------------------------------------------------
		//  GET XREF INFORMATION DATA
		//---------------------------------------------------------
		// COB_CODE: PERFORM 2159-INITIALIZE-XREF
		//               VARYING X-SUB FROM 1 BY 1
		//               UNTIL X-SUB > 6.
		ws.getProgramSubscripts().setxSub(((short) 1));
		while (!(ws.getProgramSubscripts().getxSub() > 6)) {
			initializeXref();
			ws.getProgramSubscripts().setxSub(Trunc.toShort(ws.getProgramSubscripts().getxSub() + 1, 4));
		}
		// COB_CODE: MOVE SPACES TO CSR-INCD-ADJD-PROC-CD
		//                          CSR-INCD-ADJD-MOD1-CD
		//                          CSR-INCD-ADJD-MOD2-CD
		//                          CSR-INCD-ADJD-MOD3-CD
		//                          CSR-INCD-ADJD-MOD4-CD
		//                          CSR-INCD-RSN-CD.
		ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdProcCd("");
		ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdMod1Cd("");
		ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdMod2Cd("");
		ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdMod3Cd("");
		ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdMod4Cd("");
		ws.getNf07areaofCsrProgramLineArea().getXrefIncd().getRsnCd().setRsnCd("");
		//---------------------------------------------------------
		//  GET PAYMENT CAS INFORMATION
		//---------------------------------------------------------
		// COB_CODE: MOVE SPACES TO CSR-CASCO-CLM-ADJ-GRP-CD
		//                          CSR-CASPR-CLM-ADJ-GRP-CD
		//                          CSR-CASOA-CLM-ADJ-GRP-CD
		//                          CSR-CASPI-CLM-ADJ-GRP-CD.
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().setCascoClmAdjGrpCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().setCasprClmAdjGrpCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().setCasoaClmAdjGrpCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().setCaspiClmAdjGrpCd("");
		//*
		// COB_CODE: PERFORM 2154-INITIALIZE-CSR-CAS.
		initializeCsrCas();
		// COB_CODE: MOVE 1 TO  CASCO-SUB
		//                      CASPR-SUB
		//                      CASOA-SUB
		//                      CASPI-SUB.
		ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().setCascoSub(((short) 1));
		ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().setCasprSub(((short) 1));
		ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().setCasoaSub(((short) 1));
		ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().setCaspiSub(((short) 1));
		// COB_CODE: PERFORM 2158A-MOVE-PTRS-ONLY
		aMovePtrsOnly();
		// COB_CODE: PERFORM 2158B-MOVE-PTRS-ONLY
		bMovePtrsOnly();
		// COB_CODE: MOVE 'Y' TO WS-FIRST-2158-SW
		ws.getProgramSwitches().setWsFirst2158SwFormatted("Y");
		// COB_CODE: PERFORM 2152-EVALUATE-PMT-GROUP-CD
		//               UNTIL LAST-ROW-THIS-LINE.
		while (!ws.getProgramSwitches().isLastRowThisLine()) {
			evaluatePmtGroupCd();
		}
		// COB_CODE: MOVE 'N' TO WS-FIRST-2158-SW
		ws.getProgramSwitches().setWsFirst2158SwFormatted("N");
		// COB_CODE: MOVE 'N'   TO LAST-ROW-THIS-LINE-SW.
		ws.getProgramSwitches().setLastRowThisLineSwFormatted("N");
		//*** DO NOT WRITE OUT CLAIM LINE CANCELLATIONS
		//
		// COB_CODE: INITIALIZE OUT-ORIG-REC
		//                      OUT-ORIG-REC-GMIS.
		outputOrigTo.setVariable("");
		outputOrigGmisTo.setVariable("");
		// COB_CODE: IF CSR-EXMN-ACTN-CD = 'C'
		//               CONTINUE
		//           ELSE
		//               END-IF
		//           END-IF.
		if (!(ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getExmnActnCd().getExmnActnCd() == 'C')) {
			// COB_CODE: IF CSR-GMIS-CLAIM
			//               WRITE OUT-ORIG-REC-GMIS FROM CSR-PROGRAM-LINE-AREA
			//               WRITE OUT-ORIG-REC      FROM CSR-PROGRAM-LINE-AREA
			//           END-IF
			if (ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getGmisIndicator().isGmisClaim()) {
				// COB_CODE: WRITE OUT-ORIG-REC-GMIS FROM CSR-PROGRAM-LINE-AREA
				outputOrigGmisTo.setData(ws.getCsrProgramLineAreaBytes());
				outputOrigGmisDAO.write(outputOrigGmisTo);
			} else {
				// COB_CODE: code not available
				outputOrigTo.setVariable(ws.getCsrProgramLineAreaFormatted());
				outputOrigDAO.write(outputOrigTo);
			}
		}
		// COB_CODE: ADD 1 TO WS-ORIG-REPAY-CNT
		ws.getProgramCounters().setWsOrigRepayCnt(Trunc.toInt(1 + ws.getProgramCounters().getWsOrigRepayCnt(), 6));
		// COB_CODE: PERFORM 7000-CK-FOR-COMMIT.
		ckForCommit();
	}

	/**Original name: 2152-EVALUATE-PMT-GROUP-CD<br>*/
	private void evaluatePmtGroupCd() {
		// COB_CODE: MOVE CLM-ADJ-GRP-CD OF DCLS02800SA TO WS-SAVE-GRP.
		ws.getProgramSwitches().getWsSavedCas().setSaveGrp(ws.getDcls02800sa().getClmAdjGrpCd());
		// COB_CODE: MOVE CLM-ADJ-RSN-CD OF DCLS02800SA TO WS-SAVE-ARC.
		ws.getProgramSwitches().getWsSavedCas().setSaveArc(ws.getDcls02800sa().getClmAdjRsnCd());
		// COB_CODE: MOVE MNTRY-AM       OF DCLS02800SA TO WS-SAVE-AMT.
		ws.getProgramSwitches().getWsSavedCas().setSaveAmt(Trunc.toDecimal(ws.getDcls02800sa().getMntryAm(), 11, 2));
		// COB_CODE: MOVE QNTY-CT        OF DCLS02800SA TO WS-SAVE-QTY.
		ws.getProgramSwitches().getWsSavedCas().setSaveQty(Trunc.toDecimal(ws.getDcls02800sa().getQntyCt(), 7, 2));
		// COB_CODE: IF BYPASS-2153-NO
		//                PERFORM 2153-MOVE-PMT-CAS-INFO
		//           ELSE
		//                MOVE 'N' TO BYPASS-2153-SW
		//           END-IF.
		if (ws.getProgramSwitches().isBypass2153No()) {
			// COB_CODE: PERFORM 2153-MOVE-PMT-CAS-INFO
			movePmtCasInfo();
		} else {
			// COB_CODE: MOVE 'N' TO BYPASS-2153-SW
			ws.getProgramSwitches().setBypass2153SwFormatted("N");
		}
		// COB_CODE: PERFORM 2155-FETCH-LOOP UNTIL FETCH-DONE.
		while (!ws.getProgramSwitches().isFetchDone()) {
			fetchLoop();
		}
		// COB_CODE: MOVE 0 TO FETCH-DONE-SW.
		ws.getProgramSwitches().setFetchDoneSw(((short) 0));
		// COB_CODE: IF CLAIM-CURSOR-EOF
		//               MOVE 'Y' TO LAST-ROW-THIS-LINE-SW
		//           ELSE
		//               END-IF
		//           END-IF.
		if (ws.getProgramSwitches().isClaimCursorEof()) {
			// COB_CODE: MOVE 'Y' TO LAST-ROW-THIS-LINE-SW
			ws.getProgramSwitches().setLastRowThisLineSwFormatted("Y");
		} else if (Conditions.eq(ws.getDcls02809sa().getClmCntlId(), ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmCntlIdBytes())
				&& ws.getDcls02809sa().getClmCntlSfxId() == ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId()
				&& ws.getDcls02809sa().getClmPmtId() == ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtId()
				&& ws.getProgramSwitches().getWsLiIdUnpacked() == ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getLiId()) {
			// COB_CODE: IF  CLM-CNTL-ID OF DCLS02809SA     = CSR-CLM-CNTL-ID
			//           AND CLM-CNTL-SFX-ID OF DCLS02809SA = CSR-CLM-CNTL-SFX-ID
			//           AND CLM-PMT-ID OF DCLS02809SA      = CSR-PMT-ID
			//           AND WS-LI-ID-UNPACKED              = CSR-LI-ID
			//              END-IF
			//           ELSE
			//               MOVE 'Y' TO LAST-ROW-THIS-LINE-SW
			//           END-IF
			// COB_CODE:              IF CLM-ADJ-GRP-CD OF DCLS02800SA =  WS-SAVE-GRP
			//                       AND CLM-ADJ-RSN-CD OF DCLS02800SA =  WS-SAVE-ARC
			//                       AND MNTRY-AM       OF DCLS02800SA =  WS-SAVE-AMT
			//                       AND QNTY-CT        OF DCLS02800SA =  WS-SAVE-QTY
			//           ** ------------------------------------------------------ **
			//           ** SAME CAS/SAME LINE BUT DIFFERENT XREF WILL HIT HERE &  **
			//           ** THE XREF WILL LOAD BUT CAS WON'T.                      **
			//           ** ------------------------------------------------------ **
			//                            MOVE 'Y' TO BYPASS-2153-SW
			//                       ELSE
			//           ** ------------------------------------------------------ **
			//           ** DIFFERENT CAS/SAME LINE & SAME XREF WILL HIT HERE AND  **
			//           ** THE CAS WILL BE LOADED BUT XREF WON'T.                 **
			//           ** ------------------------------------------------------ **
			//                            CONTINUE
			//                       END-IF
			if (Conditions.eq(ws.getDcls02800sa().getClmAdjGrpCd(), ws.getProgramSwitches().getWsSavedCas().getSaveGrp())
					&& Conditions.eq(ws.getDcls02800sa().getClmAdjRsnCd(), ws.getProgramSwitches().getWsSavedCas().getSaveArc())
					&& ws.getDcls02800sa().getMntryAm().compareTo(ws.getProgramSwitches().getWsSavedCas().getSaveAmt()) == 0
					&& ws.getDcls02800sa().getQntyCt().compareTo(ws.getProgramSwitches().getWsSavedCas().getSaveQty()) == 0) {
				//* ------------------------------------------------------ **
				//* SAME CAS/SAME LINE BUT DIFFERENT XREF WILL HIT HERE &  **
				//* THE XREF WILL LOAD BUT CAS WON'T.                      **
				//* ------------------------------------------------------ **
				// COB_CODE: IF CRS-REF-RSN-CD OF DCLS04315SA > SPACES
				//              PERFORM 2158A-MOVE-PTRS-ONLY
				//           ELSE
				//              PERFORM 2158B-MOVE-PTRS-ONLY
				//           END-IF
				if (Conditions.gt(ws.getDcls04315sa().getCrsRefRsnCd(), Types.SPACE_CHAR)) {
					// COB_CODE: PERFORM 2158A-MOVE-PTRS-ONLY
					aMovePtrsOnly();
				} else {
					// COB_CODE: PERFORM 2158B-MOVE-PTRS-ONLY
					bMovePtrsOnly();
				}
				// COB_CODE: MOVE 'Y' TO BYPASS-2153-SW
				ws.getProgramSwitches().setBypass2153SwFormatted("Y");
			}
		} else {
			// COB_CODE: MOVE 'Y' TO LAST-ROW-THIS-LINE-SW
			ws.getProgramSwitches().setLastRowThisLineSwFormatted("Y");
		}
	}

	/**Original name: 2153-MOVE-PMT-CAS-INFO<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '   *2153-MOVE-PMT-CAS-INFO*'.</pre>*/
	private void movePmtCasInfo() {
		// COB_CODE: EVALUATE CLM-ADJ-GRP-CD OF DCLS02800SA
		//              WHEN 'CO'
		//                 ADD 1 TO CASCO-SUB
		//              WHEN 'PR'
		//                 ADD 1 TO CASPR-SUB
		//              WHEN 'OA'
		//                 ADD 1 TO CASOA-SUB
		//              WHEN 'PI'
		//                 ADD 1 TO CASPI-SUB
		//           END-EVALUATE.
		switch (ws.getDcls02800sa().getClmAdjGrpCd()) {

		case "CO":// COB_CODE: IF CASCO-SUB > CAS-MAX-CO-18
			//               CALL 'ABEND'
			//           END-IF
			if (ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCascoSub() > ws.getProgramSubscripts().getCasSubscripts()
					.getCasMaxCo18()) {
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: DISPLAY '*****   NF0735ML ABENDING   *****'
				DisplayUtil.sysout.write("*****   NF0735ML ABENDING   *****");
				// COB_CODE: DISPLAY 'CASCO-SUB EXCEEDS MAX ALLOWED'
				DisplayUtil.sysout.write("CASCO-SUB EXCEEDS MAX ALLOWED");
				// COB_CODE: DISPLAY 'MORE THAN ' CAS-MAX-CO-18
				//                   ' CO CAS SEGS FOUND'
				DisplayUtil.sysout.write("MORE THAN ", ws.getProgramSubscripts().getCasSubscripts().getCasMaxCo18AsString(), " CO CAS SEGS FOUND");
				// COB_CODE: DISPLAY ' TA2809 CLAIM ' CLM-CNTL-ID OF DCLS02809SA
				//                   ' CLM SFX ID = ' CLM-CNTL-SFX-ID OF
				//                                    DCLS02809SA
				//                   '    PMT ID = ' CLM-PMT-ID OF DCLS02809SA
				//                   '    LINE = ' WS-LI-ID-UNPACKED
				DisplayUtil.sysout.write(new String[] { " TA2809 CLAIM ", ws.getDcls02809sa().getClmCntlIdFormatted(), " CLM SFX ID = ",
						String.valueOf(ws.getDcls02809sa().getClmCntlSfxId()), "    PMT ID = ", ws.getDcls02809sa().getClmPmtIdAsString(),
						"    LINE = ", ws.getProgramSwitches().getWsLiIdUnpackedAsString() });
				// COB_CODE: DISPLAY ' CASCO-SUB = ' CASCO-SUB
				DisplayUtil.sysout.write(" CASCO-SUB = ", ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCascoSubAsString());
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: CALL 'ABEND'
				DynamicCall.invoke("ABEND");
			}
			// COB_CODE: MOVE CLM-ADJ-GRP-CD OF DCLS02800SA
			//             TO CSR-CASCO-CLM-ADJ-GRP-CD
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().setCascoClmAdjGrpCd(ws.getDcls02800sa().getClmAdjGrpCd());
			// COB_CODE: MOVE CLM-ADJ-RSN-CD OF DCLS02800SA
			//             TO CSR-CASCO-CLM-ADJ-RSN-CD (CASCO-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCascoSub())
					.setClmAdjRsnCd(ws.getDcls02800sa().getClmAdjRsnCd());
			// COB_CODE: MOVE MNTRY-AM OF DCLS02800SA
			//             TO CSR-CASCO-MNTRY-AM  (CASCO-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCascoSub())
					.setMntryAm(Trunc.toDecimal(ws.getDcls02800sa().getMntryAm(), 11, 2));
			// COB_CODE: MOVE QNTY-CT OF DCLS02800SA
			//             TO CSR-CASCO-QNTY-CT   (CASCO-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCascoSub())
					.setQntyCt(Trunc.toDecimal(ws.getDcls02800sa().getQntyCt(), 7, 2));
			// COB_CODE: MOVE WS-EOB-AM TO CSR-CASCO-EOB-AM   (CASCO-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCascoSub())
					.setEobAm(Trunc.toDecimal(ws.getProgramSwitches().getWsEobAm(), 11, 2));
			// COB_CODE: ADD 1 TO CASCO-SUB
			ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs()
					.setCascoSub(Trunc.toShort(1 + ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCascoSub(), 4));
			break;

		case "PR":// COB_CODE: IF CASPR-SUB > CAS-MAX-PR-24
			//               CALL 'ABEND'
			//           END-IF
			if (ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasprSub() > ws.getProgramSubscripts().getCasSubscripts()
					.getCasMaxPr24()) {
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: DISPLAY '*****   NF0735ML ABENDING   *****'
				DisplayUtil.sysout.write("*****   NF0735ML ABENDING   *****");
				// COB_CODE: DISPLAY 'CASPR-SUB EXCEEDS MAX ALLOWED'
				DisplayUtil.sysout.write("CASPR-SUB EXCEEDS MAX ALLOWED");
				// COB_CODE: DISPLAY 'MORE THAN ' CAS-MAX-PR-24
				//                   ' PR CAS SEGS FOUND'
				DisplayUtil.sysout.write("MORE THAN ", ws.getProgramSubscripts().getCasSubscripts().getCasMaxPr24AsString(), " PR CAS SEGS FOUND");
				// COB_CODE: DISPLAY 'TA2809  CLAIM ' CLM-CNTL-ID OF DCLS02809SA
				//                        CLM-CNTL-SFX-ID OF DCLS02809SA
				//                   '    PMT = ' CLM-PMT-ID OF DCLS02809SA
				//                   '    LINE = ' WS-LI-ID-UNPACKED
				DisplayUtil.sysout.write(new String[] { "TA2809  CLAIM ", ws.getDcls02809sa().getClmCntlIdFormatted(),
						String.valueOf(ws.getDcls02809sa().getClmCntlSfxId()), "    PMT = ", ws.getDcls02809sa().getClmPmtIdAsString(), "    LINE = ",
						ws.getProgramSwitches().getWsLiIdUnpackedAsString() });
				// COB_CODE: DISPLAY '      CASPR SUB = ' CASPR-SUB
				DisplayUtil.sysout.write("      CASPR SUB = ", ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasprSubAsString());
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: CALL 'ABEND'
				DynamicCall.invoke("ABEND");
			}
			// COB_CODE: MOVE CLM-ADJ-GRP-CD OF DCLS02800SA
			//             TO CSR-CASPR-CLM-ADJ-GRP-CD
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().setCasprClmAdjGrpCd(ws.getDcls02800sa().getClmAdjGrpCd());
			// COB_CODE: MOVE CLM-ADJ-RSN-CD OF DCLS02800SA
			//             TO CSR-CASPR-CLM-ADJ-RSN-CD (CASPR-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasprSub())
					.setClmAdjRsnCd(ws.getDcls02800sa().getClmAdjRsnCd());
			// COB_CODE: MOVE MNTRY-AM OF DCLS02800SA
			//             TO CSR-CASPR-MNTRY-AM  (CASPR-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasprSub())
					.setMntryAm(Trunc.toDecimal(ws.getDcls02800sa().getMntryAm(), 11, 2));
			// COB_CODE: MOVE QNTY-CT OF DCLS02800SA
			//             TO CSR-CASPR-QNTY-CT   (CASPR-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasprSub())
					.setQntyCt(Trunc.toDecimal(ws.getDcls02800sa().getQntyCt(), 7, 2));
			// COB_CODE: MOVE WS-EOB-AM TO CSR-CASPR-EOB-AM   (CASPR-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasprSub())
					.setEobAm(Trunc.toDecimal(ws.getProgramSwitches().getWsEobAm(), 11, 2));
			// COB_CODE: ADD 1 TO CASPR-SUB
			ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs()
					.setCasprSub(Trunc.toShort(1 + ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasprSub(), 4));
			break;

		case "OA":// COB_CODE: IF CASOA-SUB > CAS-MAX-OA-18
			//               CALL 'ABEND'
			//           END-IF
			if (ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasoaSub() > ws.getProgramSubscripts().getCasSubscripts()
					.getCasMaxOa18()) {
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: DISPLAY '*****   NF0735ML ABENDING   *****'
				DisplayUtil.sysout.write("*****   NF0735ML ABENDING   *****");
				// COB_CODE: DISPLAY 'CASOA-SUB EXCEEDS MAX ALLOWED'
				DisplayUtil.sysout.write("CASOA-SUB EXCEEDS MAX ALLOWED");
				//              DISPLAY ' TA2800 CLAIM ' CLM-CNTL-ID OF DCLS02800SA
				//                               CLM-CNTL-SFX-ID OF DCLS02800SA
				//              DISPLAY ' TA2800 GROUP '
				//              DISPLAY ' TA2800 RSN CD'
				//              DISPLAY '   CSR AMT = '
				// COB_CODE:                DISPLAY 'MORE THAN ' CAS-MAX-OA-18
				//                                  ' OA CAS SEGS FOUND'
				//           *              DISPLAY ' TA2800 CLAIM ' CLM-CNTL-ID OF DCLS02800SA
				//           *                               CLM-CNTL-SFX-ID OF DCLS02800SA
				//           *              DISPLAY ' TA2800 GROUP '
				//                              CLM-ADJ-GRP-CD OF DCLS02800SA
				//           *              DISPLAY ' TA2800 RSN CD'
				//                              CLM-ADJ-RSN-CD OF DCLS02800SA
				//           *              DISPLAY '   CSR AMT = '
				//                              MNTRY-AM OF DCLS02800SA
				DisplayUtil.sysout.write(new String[] { "MORE THAN ", ws.getProgramSubscripts().getCasSubscripts().getCasMaxOa18AsString(),
						" OA CAS SEGS FOUND", ws.getDcls02800sa().getClmAdjGrpCdFormatted(), ws.getDcls02800sa().getClmAdjRsnCdFormatted(),
						ws.getDcls02800sa().getMntryAmAsString() });
				//              DISPLAY '    EOB-AM = ' WS-EOB-AM
				// COB_CODE: DISPLAY '      CASOA SUB = ' CASOA-SUB
				DisplayUtil.sysout.write("      CASOA SUB = ", ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasoaSubAsString());
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: CALL 'ABEND'
				DynamicCall.invoke("ABEND");
			}
			// COB_CODE: MOVE CLM-ADJ-GRP-CD OF DCLS02800SA
			//             TO CSR-CASOA-CLM-ADJ-GRP-CD
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().setCasoaClmAdjGrpCd(ws.getDcls02800sa().getClmAdjGrpCd());
			// COB_CODE: MOVE CLM-ADJ-RSN-CD OF DCLS02800SA
			//             TO CSR-CASOA-CLM-ADJ-RSN-CD (CASOA-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCasoaReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasoaSub())
					.setClmAdjRsnCd(ws.getDcls02800sa().getClmAdjRsnCd());
			// COB_CODE: MOVE MNTRY-AM OF DCLS02800SA
			//             TO CSR-CASOA-MNTRY-AM  (CASOA-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCasoaReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasoaSub())
					.setMntryAm(Trunc.toDecimal(ws.getDcls02800sa().getMntryAm(), 11, 2));
			// COB_CODE: MOVE QNTY-CT OF DCLS02800SA
			//             TO CSR-CASOA-QNTY-CT   (CASOA-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCasoaReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasoaSub())
					.setQntyCt(Trunc.toDecimal(ws.getDcls02800sa().getQntyCt(), 7, 2));
			// COB_CODE: MOVE WS-EOB-AM TO CSR-CASPR-EOB-AM   (CASOA-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasoaSub())
					.setEobAm(Trunc.toDecimal(ws.getProgramSwitches().getWsEobAm(), 11, 2));
			// COB_CODE: ADD 1 TO CASOA-SUB
			ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs()
					.setCasoaSub(Trunc.toShort(1 + ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCasoaSub(), 4));
			break;

		case "PI":// COB_CODE: IF CASPI-SUB > CAS-MAX-PI-6
			//               CALL 'ABEND'
			//           END-IF
			if (ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCaspiSub() > ws.getProgramSubscripts().getCasSubscripts()
					.getCasMaxPi6()) {
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: DISPLAY '*****   NF0735ML ABENDING   *****'
				DisplayUtil.sysout.write("*****   NF0735ML ABENDING   *****");
				// COB_CODE: DISPLAY 'CASPI-SUB EXCEEDS MAX ALLOWED'
				DisplayUtil.sysout.write("CASPI-SUB EXCEEDS MAX ALLOWED");
				//              DISPLAY ' TA2800 CLAIM ' CLM-CNTL-ID OF DCLS02800SA
				//                               CLM-CNTL-SFX-ID OF DCLS02800SA
				//              DISPLAY ' TA2800 GROUP '
				//              DISPLAY ' TA2800 RSN CD'
				//              DISPLAY '   CSR AMT = '
				// COB_CODE:                DISPLAY 'MORE THAN ' CAS-MAX-PI-6
				//                                  ' PI CAS SEGS FOUND'
				//           *              DISPLAY ' TA2800 CLAIM ' CLM-CNTL-ID OF DCLS02800SA
				//           *                               CLM-CNTL-SFX-ID OF DCLS02800SA
				//           *              DISPLAY ' TA2800 GROUP '
				//                              CLM-ADJ-GRP-CD OF DCLS02800SA
				//           *              DISPLAY ' TA2800 RSN CD'
				//                              CLM-ADJ-RSN-CD OF DCLS02800SA
				//           *              DISPLAY '   CSR AMT = '
				//                              MNTRY-AM OF DCLS02800SA
				DisplayUtil.sysout.write(new String[] { "MORE THAN ", ws.getProgramSubscripts().getCasSubscripts().getCasMaxPi6AsString(),
						" PI CAS SEGS FOUND", ws.getDcls02800sa().getClmAdjGrpCdFormatted(), ws.getDcls02800sa().getClmAdjRsnCdFormatted(),
						ws.getDcls02800sa().getMntryAmAsString() });
				//              DISPLAY '    EOB-AM = ' WS-EOB-AM
				// COB_CODE: DISPLAY '      CASPI SUB = ' CASPI-SUB
				DisplayUtil.sysout.write("      CASPI SUB = ", ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCaspiSubAsString());
				// COB_CODE: CALL 'ABEND'
				DynamicCall.invoke("ABEND");
			}
			// COB_CODE: MOVE CLM-ADJ-GRP-CD OF DCLS02800SA
			//             TO CSR-CASPI-CLM-ADJ-GRP-CD
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().setCaspiClmAdjGrpCd(ws.getDcls02800sa().getClmAdjGrpCd());
			// COB_CODE: MOVE CLM-ADJ-RSN-CD OF DCLS02800SA
			//             TO CSR-CASPI-CLM-ADJ-RSN-CD (CASPI-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCaspiReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCaspiSub())
					.setClmAdjRsnCd(ws.getDcls02800sa().getClmAdjRsnCd());
			// COB_CODE: MOVE MNTRY-AM OF DCLS02800SA
			//             TO CSR-CASPI-MNTRY-AM  (CASPI-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCaspiReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCaspiSub())
					.setMntryAm(Trunc.toDecimal(ws.getDcls02800sa().getMntryAm(), 11, 2));
			// COB_CODE: MOVE QNTY-CT OF DCLS02800SA
			//             TO CSR-CASPI-QNTY-CT   (CASPI-SUB)
			ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo()
					.getCaspiReductions(ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCaspiSub())
					.setQntyCt(Trunc.toDecimal(ws.getDcls02800sa().getQntyCt(), 7, 2));
			// COB_CODE: ADD 1 TO CASPI-SUB
			ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs()
					.setCaspiSub(Trunc.toShort(1 + ws.getProgramSubscripts().getCasSubscripts().getPmtCasSubs().getCaspiSub(), 4));
			break;

		default:
			break;
		}
		// COB_CODE: IF CLM-ADJ-RSN-CD OF DCLS02800SA = '16' OR '17' OR
		//                                              '96' OR '125'
		//                MOVE 'X' TO CSR-ARC-IND
		//           END-IF.
		if (Conditions.eq(ws.getDcls02800sa().getClmAdjRsnCd(), "16") || Conditions.eq(ws.getDcls02800sa().getClmAdjRsnCd(), "17")
				|| Conditions.eq(ws.getDcls02800sa().getClmAdjRsnCd(), "96") || Conditions.eq(ws.getDcls02800sa().getClmAdjRsnCd(), "125")) {
			// COB_CODE: MOVE 'X' TO CSR-ARC-IND
			ws.getNf07areaofCsrProgramLineArea().setArcIndFormatted("X");
		}
	}

	/**Original name: 2154-INITIALIZE-CSR-CAS<br>
	 * <pre>---------------------------------------------------------------*
	 *     DISPLAY ' *2154-INITIALIZE-CSR-CAS*'.</pre>*/
	private void initializeCsrCas() {
		// COB_CODE: PERFORM 2154A-INITIALIZE-CSR-CO-CAS
		//               VARYING CAS-SUB FROM 1 BY 1
		//               UNTIL CAS-SUB > CAS-MAX-CO-18.
		ws.getProgramSubscripts().getCasSubscripts().setCasSub(((short) 1));
		while (!(ws.getProgramSubscripts().getCasSubscripts().getCasSub() > ws.getProgramSubscripts().getCasSubscripts().getCasMaxCo18())) {
			aInitializeCsrCoCas();
			ws.getProgramSubscripts().getCasSubscripts().setCasSub(Trunc.toShort(ws.getProgramSubscripts().getCasSubscripts().getCasSub() + 1, 4));
		}
		// COB_CODE: PERFORM 2154B-INITIALIZE-CSR-PR-CAS
		//               VARYING CAS-SUB FROM 1 BY 1
		//               UNTIL CAS-SUB > CAS-MAX-PR-24.
		ws.getProgramSubscripts().getCasSubscripts().setCasSub(((short) 1));
		while (!(ws.getProgramSubscripts().getCasSubscripts().getCasSub() > ws.getProgramSubscripts().getCasSubscripts().getCasMaxPr24())) {
			bInitializeCsrPrCas();
			ws.getProgramSubscripts().getCasSubscripts().setCasSub(Trunc.toShort(ws.getProgramSubscripts().getCasSubscripts().getCasSub() + 1, 4));
		}
		// COB_CODE: PERFORM 2154C-INITIALIZE-CSR-OA-CAS
		//               VARYING CAS-SUB FROM 1 BY 1
		//               UNTIL CAS-SUB > CAS-MAX-OA-18.
		ws.getProgramSubscripts().getCasSubscripts().setCasSub(((short) 1));
		while (!(ws.getProgramSubscripts().getCasSubscripts().getCasSub() > ws.getProgramSubscripts().getCasSubscripts().getCasMaxOa18())) {
			cInitializeCsrOaCas();
			ws.getProgramSubscripts().getCasSubscripts().setCasSub(Trunc.toShort(ws.getProgramSubscripts().getCasSubscripts().getCasSub() + 1, 4));
		}
		// COB_CODE: PERFORM 2154D-INITIALIZE-CSR-PI-CAS
		//               VARYING CAS-SUB FROM 1 BY 1
		//               UNTIL CAS-SUB > CAS-MAX-PI-6.
		ws.getProgramSubscripts().getCasSubscripts().setCasSub(((short) 1));
		while (!(ws.getProgramSubscripts().getCasSubscripts().getCasSub() > ws.getProgramSubscripts().getCasSubscripts().getCasMaxPi6())) {
			dInitializeCsrPiCas();
			ws.getProgramSubscripts().getCasSubscripts().setCasSub(Trunc.toShort(ws.getProgramSubscripts().getCasSubscripts().getCasSub() + 1, 4));
		}
	}

	/**Original name: 2154A-INITIALIZE-CSR-CO-CAS<br>
	 * <pre>---------------------------------------------------------------*
	 *     DISPLAY ' *2154A-INITIALIZE-CSR-CO-CAS*'.</pre>*/
	private void aInitializeCsrCoCas() {
		// COB_CODE: MOVE SPACES TO  CSR-CASCO-CLM-ADJ-RSN-CD (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setClmAdjRsnCd("");
		// COB_CODE: MOVE ZEROS  TO  CSR-CASCO-MNTRY-AM (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setMntryAm(new AfDecimal(0, 11, 2));
		// COB_CODE: MOVE ZEROS  TO  CSR-CASCO-QNTY-CT  (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setQntyCt(new AfDecimal(0, 7, 2));
		// COB_CODE: MOVE ZEROS  TO  CSR-CASCO-EOB-AM   (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setEobAm(new AfDecimal(0, 11, 2));
	}

	/**Original name: 2154B-INITIALIZE-CSR-PR-CAS<br>
	 * <pre>---------------------------------------------------------------*
	 *     DISPLAY ' *2154B-INITIALIZE-CSR-PR-CAS*'.</pre>*/
	private void bInitializeCsrPrCas() {
		// COB_CODE: MOVE SPACES TO  CSR-CASPR-CLM-ADJ-RSN-CD (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setClmAdjRsnCd("");
		// COB_CODE: MOVE ZEROS  TO  CSR-CASPR-MNTRY-AM (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setMntryAm(new AfDecimal(0, 11, 2));
		// COB_CODE: MOVE ZEROS  TO  CSR-CASPR-QNTY-CT  (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setQntyCt(new AfDecimal(0, 7, 2));
		// COB_CODE: MOVE ZEROS  TO  CSR-CASPR-EOB-AM   (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setEobAm(new AfDecimal(0, 11, 2));
	}

	/**Original name: 2154C-INITIALIZE-CSR-OA-CAS<br>
	 * <pre>---------------------------------------------------------------*
	 *     DISPLAY ' *2154C-INITIALIZE-CSR-OA-CAS*'.</pre>*/
	private void cInitializeCsrOaCas() {
		// COB_CODE: MOVE SPACES TO  CSR-CASOA-CLM-ADJ-RSN-CD (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCasoaReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setClmAdjRsnCd("");
		// COB_CODE: MOVE ZEROS  TO  CSR-CASOA-MNTRY-AM (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCasoaReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setMntryAm(new AfDecimal(0, 11, 2));
		// COB_CODE: MOVE ZEROS  TO  CSR-CASOA-QNTY-CT  (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCasoaReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setQntyCt(new AfDecimal(0, 7, 2));
	}

	/**Original name: 2154D-INITIALIZE-CSR-PI-CAS<br>
	 * <pre>---------------------------------------------------------------*
	 *     DISPLAY ' *2154D-INITIALIZE-CSR-PI-CAS*'.</pre>*/
	private void dInitializeCsrPiCas() {
		// COB_CODE: MOVE SPACES TO  CSR-CASPI-CLM-ADJ-RSN-CD (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCaspiReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setClmAdjRsnCd("");
		// COB_CODE: MOVE ZEROS  TO  CSR-CASPI-MNTRY-AM (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCaspiReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setMntryAm(new AfDecimal(0, 11, 2));
		// COB_CODE: MOVE ZEROS  TO  CSR-CASPI-QNTY-CT  (CAS-SUB).
		ws.getNf07areaofCsrProgramLineArea().getPmtCasInfo().getCaspiReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setQntyCt(new AfDecimal(0, 7, 2));
	}

	/**Original name: 2155-FETCH-LOOP<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     DISPLAY '  *2155-FETCH-LOOP* '.
	 * * ------------------------------------------------------------ **
	 * * THIS LOOP IS PERFORMED IN ORDER TO BYPASS ANY CLAIM LINES    **
	 * * THAT HAVE A DNL RMRK CD = 'FH'. THEY ARE NOT TO BE REPORTED  **
	 * * ON THE REMIT.                                                **
	 * * ------------------------------------------------------------ **</pre>*/
	private void fetchLoop() {
		// COB_CODE: PERFORM 6010-FETCH-TEMP-TRIG-ROW.
		fetchTempTrigRow();
		// COB_CODE: IF CLAIM-CURSOR-EOF
		//               MOVE 1 TO FETCH-DONE-SW
		//           ELSE
		//               END-IF
		//           END-IF.
		if (ws.getProgramSwitches().isClaimCursorEof()) {
			// COB_CODE: MOVE 1 TO FETCH-DONE-SW
			ws.getProgramSwitches().setFetchDoneSw(((short) 1));
		} else if (Conditions.eq(ws.getDcls02809sa().getClmCntlId(), ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmCntlIdBytes())
				&& ws.getDcls02809sa().getClmCntlSfxId() == ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId()
				&& ws.getDcls02809sa().getClmPmtId() == ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtId()) {
			// COB_CODE: IF  CLM-CNTL-ID OF DCLS02809SA     = CSR-CLM-CNTL-ID
			//           AND CLM-CNTL-SFX-ID OF DCLS02809SA = CSR-CLM-CNTL-SFX-ID
			//           AND CLM-PMT-ID OF DCLS02809SA      = CSR-PMT-ID
			//               END-IF
			//           ELSE
			//               END-IF
			//           END-IF
			// COB_CODE: IF (DNL-RMRK-CD    OF DCLS02809SA = 'FH'
			//            OR DNL-RMRK-CD    OF DCLS02809SA = 'LX'
			//            OR EXMN-ACTN-CD   OF DCLS02809SA = 'C')
			//               CONTINUE
			//           ELSE
			//               MOVE 1 TO FETCH-DONE-SW
			//           END-IF
			if (!(Conditions.eq(ws.getDcls02809sa().getDnlRmrkCd(), "FH") || Conditions.eq(ws.getDcls02809sa().getDnlRmrkCd(), "LX")
					|| ws.getDcls02809sa().getExmnActnCd() == 'C')) {
				// COB_CODE: MOVE 1 TO FETCH-DONE-SW
				ws.getProgramSwitches().setFetchDoneSw(((short) 1));
			}
		} else {
			// COB_CODE: IF TRIG-HIST-BILL-PROV-NPI-ID NOT = SPACES
			//               ADD 1 TO WS-NPI-HIST-CNT
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getTrigRecordLayout().getTrigHistBillProvNpiId())) {
				// COB_CODE: ADD 1 TO WS-NPI-HIST-CNT
				ws.getProgramCounters().setWsNpiHistCnt(Trunc.toInt(1 + ws.getProgramCounters().getWsNpiHistCnt(), 6));
			}
			// COB_CODE: IF TRIG-837-BILL-PROV-NPI-ID NOT = SPACES
			//               ADD 1 TO WS-NPI-TXNS-CNT
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getTrigRecordLayout().getTrig837BillProvNpiId())) {
				// COB_CODE: ADD 1 TO WS-NPI-TXNS-CNT
				ws.getProgramCounters().setWsNpiTxnsCnt(Trunc.toInt(1 + ws.getProgramCounters().getWsNpiTxnsCnt(), 6));
			}
			// COB_CODE: IF (DNL-RMRK-CD    OF DCLS02809SA = 'FH' OR
			//               DNL-RMRK-CD    OF DCLS02809SA = 'LX' OR
			//               EXMN-ACTN-CD   OF DCLS02809SA = 'C')
			//               CONTINUE
			//           ELSE
			//               ADD 1 TO ORIG-PMTS-CNTR
			//           END-IF
			if (!(Conditions.eq(ws.getDcls02809sa().getDnlRmrkCd(), "FH") || Conditions.eq(ws.getDcls02809sa().getDnlRmrkCd(), "LX")
					|| ws.getDcls02809sa().getExmnActnCd() == 'C')) {
				// COB_CODE: MOVE 1 TO FETCH-DONE-SW
				ws.getProgramSwitches().setFetchDoneSw(((short) 1));
				// COB_CODE: ADD 1 TO CHKP-UOW-CNTR
				ws.getProgramCounters().setChkpUowCntr(Trunc.toInt(1 + ws.getProgramCounters().getChkpUowCntr(), 6));
				// COB_CODE: ADD 1 TO STATS-CNT
				ws.getProgramCounters().setStatsCnt(Trunc.toInt(1 + ws.getProgramCounters().getStatsCnt(), 6));
				// COB_CODE: ADD 1 TO ORIG-PMTS-CNTR
				ws.getProgramCounters().setOrigPmtsCntr(Trunc.toInt(1 + ws.getProgramCounters().getOrigPmtsCntr(), 6));
			}
		}
	}

	/**Original name: 2156-FETCH-VOID-LOOP<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '  *2156-FETCH-VOID-LOOP* '.
	 * * ------------------------------------------------------------ **
	 * * THIS LOOP IS PERFORMED IN ORDER TO BYPASS ANY CLAIM LINES    **
	 * * THAT HAVE A DNL RMRK CD = 'FH'. THEY ARE NOT TO BE REPORTED  **
	 * * ON THE REMIT.                                                **
	 * * ------------------------------------------------------------ **</pre>*/
	private void fetchVoidLoop() {
		// COB_CODE: PERFORM 6015-FETCH-VOID-ROW.
		fetchVoidRow();
		//    DISPLAY ' '.
		//    DISPLAY '  JUST FETCHED VOID RECORD '.
		//    DISPLAY '   MEMBER INFO RETURNED: '.
		//    DISPLAY ' '.
		// COB_CODE: IF VOID-EOF-SW = 'Y'
		//               MOVE 1 TO FETCH-VOID-DONE-SW
		//           ELSE
		//               END-IF
		//           END-IF.
		if (ws.getProgramSwitches().getVoidEofSw() == 'Y') {
			// COB_CODE: MOVE 1 TO FETCH-VOID-DONE-SW
			ws.getProgramSwitches().setFetchVoidDoneSw(((short) 1));
		} else if (Conditions.eq(ws.getDcls02813sa().getClmCntlId(), ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmCntlIdBytes())
				&& ws.getDcls02813sa().getClmCntlSfxId() == ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId()
				&& ws.getDcls02813sa().getClmPmtId() == ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPmtId()) {
			// COB_CODE: IF  CLM-CNTL-ID OF DCLS02813SA = VSR-CLM-CNTL-ID
			//           AND CLM-CNTL-SFX-ID OF DCLS02813SA = VSR-CLM-CNTL-SFX-ID
			//           AND CLM-PMT-ID OF DCLS02813SA  = VSR-PMT-ID
			//               END-IF
			//           ELSE
			//               END-IF
			//           END-IF
			// COB_CODE: IF (DNL-RMRK-CD OF DCLS02809SA = 'FH'
			//            OR DNL-RMRK-CD OF DCLS02809SA = 'LX'
			//            OR EXMN-ACTN-CD OF DCLS02809SA = 'C')
			//           AND NOT VOID-EOF
			//               CONTINUE
			//           ELSE
			//               MOVE 1 TO FETCH-VOID-DONE-SW
			//           END-IF
			if (!((Conditions.eq(ws.getDcls02809sa().getDnlRmrkCd(), "FH") || Conditions.eq(ws.getDcls02809sa().getDnlRmrkCd(), "LX")
					|| ws.getDcls02809sa().getExmnActnCd() == 'C') && !ws.getProgramSwitches().isVoidEof())) {
				// COB_CODE: MOVE 1 TO FETCH-VOID-DONE-SW
				ws.getProgramSwitches().setFetchVoidDoneSw(((short) 1));
			}
		} else if (!((Conditions.eq(ws.getDcls02809sa().getDnlRmrkCd(), "FH") || Conditions.eq(ws.getDcls02809sa().getDnlRmrkCd(), "LX")
				|| ws.getDcls02809sa().getExmnActnCd() == 'C') && !ws.getProgramSwitches().isVoidEof())) {
			// COB_CODE: IF (DNL-RMRK-CD OF DCLS02809SA = 'FH'
			//            OR DNL-RMRK-CD OF DCLS02809SA = 'LX'
			//            OR EXMN-ACTN-CD OF DCLS02809SA = 'C')
			//           AND NOT VOID-EOF
			//               CONTINUE
			//           ELSE
			//               MOVE 1 TO FETCH-VOID-DONE-SW
			//           END-IF
			// COB_CODE: MOVE 1 TO FETCH-VOID-DONE-SW
			ws.getProgramSwitches().setFetchVoidDoneSw(((short) 1));
		}
	}

	/**Original name: 2158A-MOVE-PTRS-ONLY<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '   *2158A-MOVE-PTRS-ONLY* '.
	 * * ------------------------------------------------------------ **
	 * * WHEN THERE ARE MORE THAN ONE POINTER FOR A LINE THIS MODULE  **
	 * * IS EXECUTED.  IT WILL POPULATE THE 2ND & 3RD OCCURRENCE OF   **
	 * * THE XREF FIELDS.   C298.                                     **
	 * * ------------------------------------------------------------ **</pre>*/
	private void aMovePtrsOnly() {
		// COB_CODE: EVALUATE CRS-REF-RSN-CD OF DCLS04315SA
		//                WHEN 'U'
		//                     MOVE 1 TO X-SUB
		//                WHEN 'B'
		//                     MOVE 2 TO X-SUB
		//                WHEN 'R'
		//                     MOVE 3 TO X-SUB
		//                WHEN 'I'
		//                     MOVE 4 TO X-SUB
		//                WHEN 'L'
		//                     MOVE 5 TO X-SUB
		//                WHEN 'M'
		//                     MOVE 6 TO X-SUB
		//                WHEN 'D'
		//                WHEN 'F'
		//                WHEN 'P'
		//                WHEN 'S'
		//                WHEN 'V'
		//                     MOVE 0 TO X-SUB
		//                WHEN OTHER
		//                     MOVE 0 TO X-SUB
		//           END-EVALUATE.
		switch (ws.getDcls04315sa().getCrsRefRsnCd()) {

		case 'U':// COB_CODE: MOVE 1 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 1));
			break;

		case 'B':// COB_CODE: MOVE 2 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 2));
			break;

		case 'R':// COB_CODE: MOVE 3 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 3));
			break;

		case 'I':// COB_CODE: MOVE 4 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 4));
			break;

		case 'L':// COB_CODE: MOVE 5 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 5));
			break;

		case 'M':// COB_CODE: MOVE 6 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 6));
			break;

		case 'D':
		case 'F':
		case 'P':
		case 'S':
		case 'V':// COB_CODE: MOVE 0 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 0));
			break;

		default:// COB_CODE: MOVE 0 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 0));
			break;
		}
		// COB_CODE: IF ACT-INACT-CD OF DCLS04315SA = 'A'
		//                   CSR-INCD-ADJD-MOD4-CD
		//           END-IF.
		if (ws.getDcls04315sa().getActInactCd() == 'A') {
			// COB_CODE: MOVE CRS-REF-RSN-CD    OF DCLS04315SA TO
			//                CSR-INCD-RSN-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefIncd().getRsnCd().setRsnCd(String.valueOf(ws.getDcls04315sa().getCrsRefRsnCd()));
			// COB_CODE: MOVE ADJD-PROC-CD      OF DCLS04315SA TO
			//                CSR-INCD-ADJD-PROC-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdProcCd(ws.getDcls04315sa().getAdjdProcCd());
			// COB_CODE: MOVE ADJD-PROC-MOD1-CD OF DCLS04315SA TO
			//                CSR-INCD-ADJD-MOD1-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdMod1Cd(ws.getDcls04315sa().getAdjdProcMod1Cd());
			// COB_CODE: MOVE ADJD-PROC-MOD2-CD OF DCLS04315SA TO
			//                CSR-INCD-ADJD-MOD2-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdMod2Cd(ws.getDcls04315sa().getAdjdProcMod2Cd());
			// COB_CODE: MOVE ADJD-PROC-MOD3-CD OF DCLS04315SA TO
			//                CSR-INCD-ADJD-MOD3-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdMod3Cd(ws.getDcls04315sa().getAdjdProcMod3Cd());
			// COB_CODE: MOVE ADJD-PROC-MOD4-CD OF DCLS04315SA TO
			//                CSR-INCD-ADJD-MOD4-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdMod4Cd(ws.getDcls04315sa().getAdjdProcMod4Cd());
		}
		//---------------------------------------------------------------*
		//  THE 'WS-SAVE-XREF-CLI' IS USED ONLY WHEN THERE ARE MULTIPLE  *
		//  CAS JOINING WITH A XREF THAT IS INACTIVE SO THAT THE CURSOR  *
		//  RETURNS MULTIPLE INACTIVES PER LINE. THE PROGRAM WAS FAILING *
		//  THE 'IF' IN 2152 FOR DIFFERENT CAS/SAME LINE & SAME XREF.    *
		//  3/1/06  C298.                                                *
		//---------------------------------------------------------------*
		//**
		// COB_CODE: IF X-SUB > 0
		//           AND ACT-INACT-CD OF DCLS04315SA = 'A'
		//                              TO CSR-ADJD-MOD4-CD (X-SUB)
		//           END-IF.
		if (ws.getProgramSubscripts().getxSub() > 0 && ws.getDcls04315sa().getActInactCd() == 'A') {
			// COB_CODE: MOVE CRS-RF-CLM-CNTL-ID OF DCLS04315SA
			//                                TO CSR-XREF-CLM-ID (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setClmId(ws.getDcls04315sa().getCrsRfClmCntlId());
			// COB_CODE: MOVE CS-RF-CLM-CL-SX-ID OF DCLS04315SA
			//                                TO CSR-XREF-SX-ID (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setSxId(ws.getDcls04315sa().getCsRfClmClSxId());
			// COB_CODE: MOVE CRS-REF-CLM-PMT-ID OF DCLS04315SA
			//                                TO CSR-XREF-PMT-ID (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setPmtId(TruncAbs.toShort(ws.getDcls04315sa().getCrsRefClmPmtId(), 2));
			// COB_CODE: MOVE CRS-REF-CLI-ID  OF DCLS04315SA
			//                                TO CSR-XREF-CLI-ID (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setCliId(TruncAbs.toShort(ws.getDcls04315sa().getCrsRefCliId(), 3));
			// COB_CODE: MOVE CRS-REF-RSN-CD OF DCLS04315SA
			//                                TO CSR-XREF-RSN-CD (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setRsnCd(String.valueOf(ws.getDcls04315sa().getCrsRefRsnCd()));
			// COB_CODE: MOVE DNL-RMRK-CD OF DCLS04315SA
			//                                TO CSR-XREF-DNL-RMRK-CD (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setDnlRmrkCd(ws.getDcls04315sa().getDnlRmrkCd());
			//C002
			// COB_CODE:           MOVE ADJD-PROC-CD OF DCLS04315SA
			//                                   TO CSR-ADJD-PROC-CD (X-SUB)
			//           *C002
			//                                      CSR-PROC-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setCd(ws.getDcls04315sa().getAdjdProcCd());
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setCd(ws.getDcls04315sa().getAdjdProcCd());
			//C002
			// COB_CODE: MOVE ADJD-PROC-MOD1-CD OF DCLS04315SA
			//                         TO CSR-ADJD-MOD1-CD (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod1Cd(ws.getDcls04315sa().getAdjdProcMod1Cd());
			// COB_CODE: MOVE ADJD-PROC-MOD2-CD OF DCLS04315SA
			//                         TO CSR-ADJD-MOD2-CD (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod2Cd(ws.getDcls04315sa().getAdjdProcMod2Cd());
			// COB_CODE: MOVE ADJD-PROC-MOD3-CD OF DCLS04315SA
			//                         TO CSR-ADJD-MOD3-CD (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod3Cd(ws.getDcls04315sa().getAdjdProcMod3Cd());
			// COB_CODE: MOVE ADJD-PROC-MOD4-CD OF DCLS04315SA
			//                         TO CSR-ADJD-MOD4-CD (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod4Cd(ws.getDcls04315sa().getAdjdProcMod4Cd());
		}
	}

	/**Original name: 2158B-MOVE-PTRS-ONLY<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '   *2158B-MOVE-PTRS-ONLY* '.
	 * * ------------------------------------------------------------ **
	 * * WHEN THERE ARE MORE THAN ONE POINTER FOR A LINE THIS MODULE  **
	 * * IS EXECUTED.  IT WILL POPULATE THE 2ND & 3RD OCCURRENCE OF   **
	 * * THE XREF FIELDS.   C298.                                     **
	 * * ------------------------------------------------------------ **</pre>*/
	private void bMovePtrsOnly() {
		// COB_CODE: EVALUATE WS-XR4-CRS-REF-RSN-CD
		//                WHEN 'U'
		//                     MOVE 1 TO X-SUB
		//                WHEN 'B'
		//                     MOVE 2 TO X-SUB
		//                WHEN 'R'
		//                     MOVE 3 TO X-SUB
		//                WHEN 'I'
		//                     MOVE 4 TO X-SUB
		//                WHEN 'L'
		//                     MOVE 5 TO X-SUB
		//                WHEN 'M'
		//                     MOVE 6 TO X-SUB
		//                WHEN 'D'
		//                WHEN 'F'
		//                WHEN 'P'
		//                WHEN 'S'
		//                WHEN 'V'
		//                     MOVE 0 TO X-SUB
		//                WHEN OTHER
		//                     MOVE 0 TO X-SUB
		//           END-EVALUATE.
		switch (ws.getProgramHoldAreas().getWsXr4CrsRefRsnCd()) {

		case 'U':// COB_CODE: MOVE 1 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 1));
			break;

		case 'B':// COB_CODE: MOVE 2 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 2));
			break;

		case 'R':// COB_CODE: MOVE 3 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 3));
			break;

		case 'I':// COB_CODE: MOVE 4 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 4));
			break;

		case 'L':// COB_CODE: MOVE 5 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 5));
			break;

		case 'M':// COB_CODE: MOVE 6 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 6));
			break;

		case 'D':
		case 'F':
		case 'P':
		case 'S':
		case 'V':// COB_CODE: MOVE 0 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 0));
			break;

		default:// COB_CODE: MOVE 0 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 0));
			break;
		}
		// COB_CODE: IF WS-XR4-ACT-INACT-CD = 'A'
		//                TO CSR-INCD-ADJD-MOD4-CD
		//           END-IF.
		if (ws.getProgramHoldAreas().getWsXr4ActInactCd() == 'A') {
			// COB_CODE: MOVE WS-XR4-CRS-REF-RSN-CD
			//             TO CSR-INCD-RSN-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefIncd().getRsnCd().setRsnCd(String.valueOf(ws.getProgramHoldAreas().getWsXr4CrsRefRsnCd()));
			// COB_CODE: MOVE WS-XR4-ADJD-PROC-CD
			//             TO CSR-INCD-ADJD-PROC-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdProcCd(ws.getProgramHoldAreas().getWsXr4AdjdProcCd());
			// COB_CODE: MOVE WS-XR4-ADJD-PROC-MOD1-CD
			//             TO CSR-INCD-ADJD-MOD1-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdMod1Cd(ws.getProgramHoldAreas().getWsXr4AdjdProcMod1Cd());
			// COB_CODE: MOVE WS-XR4-ADJD-PROC-MOD2-CD
			//             TO CSR-INCD-ADJD-MOD2-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdMod2Cd(ws.getProgramHoldAreas().getWsXr4AdjdProcMod2Cd());
			// COB_CODE: MOVE WS-XR4-ADJD-PROC-MOD3-CD
			//             TO CSR-INCD-ADJD-MOD3-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdMod3Cd(ws.getProgramHoldAreas().getWsXr4AdjdProcMod3Cd());
			// COB_CODE: MOVE WS-XR4-ADJD-PROC-MOD4-CD
			//             TO CSR-INCD-ADJD-MOD4-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefIncd().setAdjdMod4Cd(ws.getProgramHoldAreas().getWsXr4AdjdProcMod4Cd());
		}
		//---------------------------------------------------------------*
		//  THE 'WS-SAVE-XREF-CLI' IS USED ONLY WHEN THERE ARE MULTIPLE  *
		//  CAS JOINING WITH A XREF THAT IS INACTIVE SO THAT THE CURSOR  *
		//  RETURNS MULTIPLE INACTIVES PER LINE. THE PROGRAM WAS FAILING *
		//  THE 'IF' IN 2152 FOR DIFFERENT CAS/SAME LINE & SAME XREF.    *
		//  3/1/06  C298.                                                *
		//---------------------------------------------------------------*
		//**
		// COB_CODE: IF X-SUB > 0
		//           AND WS-XR4-ACT-INACT-CD = 'A'
		//                                 CSR-PROC-MOD4-CD
		//           END-IF.
		if (ws.getProgramSubscripts().getxSub() > 0 && ws.getProgramHoldAreas().getWsXr4ActInactCd() == 'A') {
			// COB_CODE: MOVE WS-XR4-CRS-RF-CLM-CNTL-ID
			//                                TO CSR-XREF-CLM-ID (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setClmId(ws.getProgramHoldAreas().getWsXr4CrsRfClmCntlId());
			// COB_CODE: MOVE WS-XR4-CS-RF-CLM-CL-SX-ID
			//                                TO CSR-XREF-SX-ID (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setSxId(ws.getProgramHoldAreas().getWsXr4CsRfClmClSxId());
			// COB_CODE: IF WS-XR4-CRS-REF-CLM-PMT-P1 > 0
			//                TO WS-XR4-CRS-REF-CLM-PMT-2
			//           ELSE
			//                TO WS-XR4-CRS-REF-CLM-PMT-2
			//           END-IF
			if (ws.getProgramHoldAreas().getWsXr4CrsRefClmPmtN().getP1() > 0) {
				// COB_CODE: MOVE WS-XR4-CRS-REF-CLM-PMT-P2
				//             TO WS-XR4-CRS-REF-CLM-PMT-1
				ws.setWsXr4CrsRefClmPmt1Formatted(ws.getProgramHoldAreas().getWsXr4CrsRefClmPmtN().getP2Formatted());
				// COB_CODE: MOVE WS-XR4-CRS-REF-CLM-PMT-P1
				//             TO WS-XR4-CRS-REF-CLM-PMT-2
				ws.setWsXr4CrsRefClmPmt2Formatted(ws.getProgramHoldAreas().getWsXr4CrsRefClmPmtN().getP1Formatted());
			} else {
				// COB_CODE: MOVE ZEROS
				//             TO WS-XR4-CRS-REF-CLM-PMT-1
				ws.setWsXr4CrsRefClmPmt1(((short) 0));
				// COB_CODE: MOVE WS-XR4-CRS-REF-CLM-PMT-P2
				//             TO WS-XR4-CRS-REF-CLM-PMT-2
				ws.setWsXr4CrsRefClmPmt2Formatted(ws.getProgramHoldAreas().getWsXr4CrsRefClmPmtN().getP2Formatted());
			}
			// COB_CODE: MOVE WS-XR4-CRS-REF-CLM-PMT-USE
			//                                TO CSR-XREF-PMT-ID (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setPmtIdFromBuffer(ws.getWsXr4CrsRefClmPmtUseBytes());
			// COB_CODE: IF WS-XR4-CRS-REF-CLI-P1 = SPACES
			//                   TO WS-XR4-CRS-REF-CLI-3
			//           END-IF
			if (Characters.EQ_SPACE.test(ws.getProgramHoldAreas().getWsXr4CrsRefCliN().getP1Formatted())) {
				// COB_CODE: MOVE ZEROS
				//             TO WS-XR4-CRS-REF-CLI-1
				ws.getWsXr4CrsRefCliUse().setCli1(((short) 0));
				// COB_CODE: MOVE WS-XR4-CRS-REF-CLI-P3
				//             TO WS-XR4-CRS-REF-CLI-2
				ws.getWsXr4CrsRefCliUse().setCli2Formatted(ws.getProgramHoldAreas().getWsXr4CrsRefCliN().getP3Formatted());
				// COB_CODE: MOVE WS-XR4-CRS-REF-CLI-P2
				//             TO WS-XR4-CRS-REF-CLI-3
				ws.getWsXr4CrsRefCliUse().setCli3Formatted(ws.getProgramHoldAreas().getWsXr4CrsRefCliN().getP2Formatted());
			}
			// COB_CODE: IF WS-XR4-CRS-REF-CLI-P1 = SPACES
			//           AND WS-XR4-CRS-REF-CLI-P2 = SPACES
			//                   TO WS-XR4-CRS-REF-CLI-3
			//           END-IF
			if (Characters.EQ_SPACE.test(ws.getProgramHoldAreas().getWsXr4CrsRefCliN().getP1Formatted())
					&& Characters.EQ_SPACE.test(ws.getProgramHoldAreas().getWsXr4CrsRefCliN().getP2Formatted())) {
				// COB_CODE: MOVE ZEROS
				//             TO WS-XR4-CRS-REF-CLI-1
				//                WS-XR4-CRS-REF-CLI-2
				ws.getWsXr4CrsRefCliUse().setCli1(((short) 0));
				ws.getWsXr4CrsRefCliUse().setCli2(((short) 0));
				// COB_CODE: MOVE WS-XR4-CRS-REF-CLI-P3
				//             TO WS-XR4-CRS-REF-CLI-3
				ws.getWsXr4CrsRefCliUse().setCli3Formatted(ws.getProgramHoldAreas().getWsXr4CrsRefCliN().getP3Formatted());
			}
			// COB_CODE: MOVE WS-XR4-CRS-REF-CLI-USE
			//                                TO CSR-XREF-CLI-ID (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setCliIdFromBuffer(ws.getWsXr4CrsRefCliUse().getWsXr4CrsRefCliUseBytes());
			// COB_CODE: MOVE WS-XR4-CRS-REF-RSN-CD
			//                                TO CSR-XREF-RSN-CD (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setRsnCd(String.valueOf(ws.getProgramHoldAreas().getWsXr4CrsRefRsnCd()));
			// COB_CODE: MOVE WS-XR4-DNL-RMRK-CD
			//                                TO CSR-XREF-DNL-RMRK-CD (X-SUB)
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setDnlRmrkCd(ws.getProgramHoldAreas().getWsXr4DnlRmrkCd());
			// COB_CODE: MOVE WS-XR4-ADJD-PROC-CD
			//                         TO CSR-ADJD-PROC-CD (X-SUB)
			//                            CSR-PROC-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setCd(ws.getProgramHoldAreas().getWsXr4AdjdProcCd());
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setCd(ws.getProgramHoldAreas().getWsXr4AdjdProcCd());
			// COB_CODE: MOVE WS-XR4-ADJD-PROC-MOD1-CD
			//                         TO CSR-ADJD-MOD1-CD (X-SUB)
			//                            CSR-PROC-MOD1-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod1Cd(ws.getProgramHoldAreas().getWsXr4AdjdProcMod1Cd());
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes()
					.setMod1Cd(ws.getProgramHoldAreas().getWsXr4AdjdProcMod1Cd());
			// COB_CODE: MOVE WS-XR4-ADJD-PROC-MOD2-CD
			//                         TO CSR-ADJD-MOD2-CD (X-SUB)
			//                            CSR-PROC-MOD2-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod2Cd(ws.getProgramHoldAreas().getWsXr4AdjdProcMod2Cd());
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes()
					.setMod2Cd(ws.getProgramHoldAreas().getWsXr4AdjdProcMod2Cd());
			// COB_CODE: MOVE WS-XR4-ADJD-PROC-MOD3-CD
			//                         TO CSR-ADJD-MOD3-CD (X-SUB)
			//                            CSR-PROC-MOD3-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod3Cd(ws.getProgramHoldAreas().getWsXr4AdjdProcMod3Cd());
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes()
					.setMod3Cd(ws.getProgramHoldAreas().getWsXr4AdjdProcMod3Cd());
			// COB_CODE: MOVE WS-XR4-ADJD-PROC-MOD4-CD
			//                         TO CSR-ADJD-MOD4-CD (X-SUB)
			//                            CSR-PROC-MOD4-CD
			ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod4Cd(ws.getProgramHoldAreas().getWsXr4AdjdProcMod4Cd());
			ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes()
					.setMod4Cd(ws.getProgramHoldAreas().getWsXr4AdjdProcMod4Cd());
		}
	}

	/**Original name: 2159-INITIALIZE-XREF<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '  *2159-INITIALIZE-XREF* '.
	 * * ------------------------------------------------------------ **</pre>*/
	private void initializeXref() {
		// COB_CODE: MOVE ZEROS  TO  CSR-XREF-PMT-ID      (X-SUB)
		//                           CSR-XREF-CLI-ID      (X-SUB).
		ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setPmtId(((short) 0));
		ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setCliId(((short) 0));
		// COB_CODE: MOVE SPACES TO  CSR-XREF-CLM-ID      (X-SUB)
		//                           CSR-XREF-SX-ID       (X-SUB)
		//                           CSR-XREF-RSN-CD      (X-SUB)
		//                           CSR-XREF-DNL-RMRK-CD (X-SUB)
		//                           CSR-ADJD-PROC-CD     (X-SUB)
		//                           CSR-ADJD-MOD1-CD     (X-SUB)
		//                           CSR-ADJD-MOD2-CD     (X-SUB)
		//                           CSR-ADJD-MOD3-CD     (X-SUB)
		//                           CSR-ADJD-MOD4-CD     (X-SUB).
		ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setClmId("");
		ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setSxId(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setRsnCd("");
		ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setDnlRmrkCd("");
		ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes().setCd("");
		ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes().setMod1Cd("");
		ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes().setMod2Cd("");
		ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes().setMod3Cd("");
		ws.getNf07areaofCsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes().setMod4Cd("");
	}

	/**Original name: 2200-GET-CALC-INFO<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY ' *2200-GET-CALC-INFO.* '.
	 * ****************************************************************
	 *     OPEN A CURSOR AND GET ALL THE CALC INFORMATION FOR THE     *
	 *     CLAIM LINE AND PUT INTO THE CSR- CALC FIELDS.              *
	 *     THE LIMIT IS 9 CALCS.                                      *
	 * ****************************************************************</pre>*/
	private void getCalcInfo() {
		// COB_CODE: ADD 1 TO CALC-CURSOR-CNT.
		ws.getCalcFields().getCalcTypCounters().setCalcCursorCnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getCalcCursorCnt(), 12));
		// COB_CODE: PERFORM 2210-OPEN-CALC-CURSOR.
		openCalcCursor();
		// COB_CODE: MOVE 0 TO CALC-EOF-SW.
		ws.getProgramSwitches().setCalcEofSw(((short) 0));
		// COB_CODE: PERFORM 2215-FETCH-CALC-CURSOR.
		fetchCalcCursor();
		// COB_CODE: PERFORM 2220-GET-AND-SAVE-CALCS
		//               UNTIL CALC-EOF-SW = 1.
		while (!(ws.getProgramSwitches().getCalcEofSw() == 1)) {
			getAndSaveCalcs();
		}
		// COB_CODE: PERFORM 2225-CLOSE-CALC-CURSOR.
		closeCalcCursor();
	}

	/**Original name: 2205-GET-VOID-CALC-INFO<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY ' *2205-GET-VOID-CALC-INFO.* '.
	 * ****************************************************************
	 *     OPEN A CURSOR AND GET ALL THE CALC INFORMATION FOR THE     *
	 *     VOIDED CLAIM LINE AND PUT INTO THE VSR- CALC FIELDS.       *
	 *     THE LIMIT IS 9 CALCS.                                      *
	 * ****************************************************************</pre>*/
	private void getVoidCalcInfo() {
		// COB_CODE: ADD 1 TO CALC-CURSOR-CNT.
		ws.getCalcFields().getCalcTypCounters().setCalcCursorCnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getCalcCursorCnt(), 12));
		// COB_CODE: PERFORM 2210-OPEN-CALC-CURSOR.
		openCalcCursor();
		// COB_CODE: MOVE 0 TO CALC-EOF-SW.
		ws.getProgramSwitches().setCalcEofSw(((short) 0));
		// COB_CODE: PERFORM 2215-FETCH-CALC-CURSOR.
		fetchCalcCursor();
		// COB_CODE: PERFORM 2222-GET-AND-SAVE-VOID-CALCS
		//               UNTIL CALC-EOF-SW = 1.
		while (!(ws.getProgramSwitches().getCalcEofSw() == 1)) {
			getAndSaveVoidCalcs();
		}
		// COB_CODE: PERFORM 2225-CLOSE-CALC-CURSOR.
		closeCalcCursor();
	}

	/**Original name: 2210-OPEN-CALC-CURSOR<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
	private void openCalcCursor() {
		ConcatUtil concatUtil = null;
		GenericParam abendCode = null;
		// COB_CODE: EXEC SQL
		//              OPEN CALC_CURSOR
		//           END-EXEC.
		s02811saS02813saDao.openCalcCursor(ws.getCalcFields().getCalcRecordKey().getCntlId(), ws.getCalcFields().getCalcRecordKey().getSfxId(),
				ws.getCalcFields().getCalcRecordKey().getPmtNum(), ws.getCalcFields().getCalcRecordKey().getLiIdPacked());
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 CONTINUE
		//             WHEN -911
		//                 CALL 'ABEND' USING ABEND-CODE
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		case -911:// COB_CODE: DISPLAY '-911 OPENING CALC_CURSOR, CLAIM # '
			//             WS-CALC-CLM-CNTL-ID  WS-CALC-CLM-SFX-ID
			//             '  PMT: '  WS-CALC-CLM-PMT-NUM
			//             '  LINE: ' WS-CALC-CLM-LI-ID-PACKED
			DisplayUtil.sysout.write(new String[] { "-911 OPENING CALC_CURSOR, CLAIM # ", ws.getCalcFields().getCalcRecordKey().getCntlIdFormatted(),
					String.valueOf(ws.getCalcFields().getCalcRecordKey().getSfxId()), "  PMT: ",
					ws.getCalcFields().getCalcRecordKey().getPmtNumAsString(), "  LINE: ",
					ws.getCalcFields().getCalcRecordKey().getLiIdPackedAsString() });
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02811SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02811SA");
			// COB_CODE: MOVE 'OPENING CALC CURSOR ' TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("OPENING CALC CURSOR ");
			// COB_CODE: MOVE '2210-OPEN-CALC-CURSOR' TO DB2-ERR-PARA
			ws.setDb2ErrPara("2210-OPEN-CALC-CURSOR");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: PERFORM 1019-CLOSE-ROUTINE
			closeRoutine();
			// COB_CODE: MOVE +911 TO RETURN-CODE
			Session.setReturnCode(911);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;

		default:// COB_CODE: DISPLAY 'ERROR OPENING CALC_CURSOR, CLAIM # '
			//               WS-CALC-CLM-CNTL-ID  WS-CALC-CLM-SFX-ID
			//               '  PMT: '  WS-CALC-CLM-PMT-NUM
			//               '  LINE: ' WS-CALC-CLM-LI-ID-PACKED
			DisplayUtil.sysout.write(new String[] { "ERROR OPENING CALC_CURSOR, CLAIM # ", ws.getCalcFields().getCalcRecordKey().getCntlIdFormatted(),
					String.valueOf(ws.getCalcFields().getCalcRecordKey().getSfxId()), "  PMT: ",
					ws.getCalcFields().getCalcRecordKey().getPmtNumAsString(), "  LINE: ",
					ws.getCalcFields().getCalcRecordKey().getLiIdPackedAsString() });
			// COB_CODE: PERFORM 9940-DISPLAY-SQLCODE
			displaySqlcode();
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'         INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02811SA'                 TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02811SA");
			// COB_CODE: MOVE 'OPEN CALC CURSOR      '   TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("OPEN CALC CURSOR      ");
			// COB_CODE: MOVE '2210-OPEN-CALC-CURSOR   ' TO DB2-ERR-PARA
			ws.setDb2ErrPara("2210-OPEN-CALC-CURSOR   ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
	}

	/**Original name: 2215-FETCH-CALC-CURSOR<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
	private void fetchCalcCursor() {
		ConcatUtil concatUtil = null;
		GenericParam abendCode = null;
		// COB_CODE: EXEC SQL
		//               FETCH CALC_CURSOR
		//                    INTO   :HOLD-CALC-TYP-CD
		//                         , :HOLD-COV-LOB-CD
		//                         , :HOLD-CALC-EXPLN-CD
		//                         , :HOLD-PAY-CD
		//                         , :HOLD-ALW-CHG-AM
		//                         , :HOLD-PD-AM
		//                         , :HOLD-GRP-ID
		//                         , :HOLD-CORP-CD
		//                         , :HOLD-PROD-CD
		//                         , :HOLD-TYP-BEN-CD
		//                         , :HOLD-SPECI-BEN-CD
		//                         , :HOLD-GL-ACCT-ID
		//                         , :HOLD-INS-TC-CD
		//           END-EXEC.
		s02811saS02813saDao.fetchCalcCursor(ws.getCalcFields());
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 CONTINUE
		//             WHEN +100
		//                 MOVE 1 TO CALC-EOF-SW
		//             WHEN -911
		//                 CALL 'ABEND' USING ABEND-CODE
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		case 100:// COB_CODE: MOVE 1 TO CALC-EOF-SW
			ws.getProgramSwitches().setCalcEofSw(((short) 1));
			break;

		case -911:// COB_CODE: DISPLAY '-911 FETCHING CALC_CURSOR, CLAIM # '
			//             WS-CALC-CLM-CNTL-ID  WS-CALC-CLM-SFX-ID
			//             '  PMT: '  WS-CALC-CLM-PMT-NUM
			//             '  LINE: ' WS-CALC-CLM-LI-ID-PACKED
			DisplayUtil.sysout.write(new String[] { "-911 FETCHING CALC_CURSOR, CLAIM # ", ws.getCalcFields().getCalcRecordKey().getCntlIdFormatted(),
					String.valueOf(ws.getCalcFields().getCalcRecordKey().getSfxId()), "  PMT: ",
					ws.getCalcFields().getCalcRecordKey().getPmtNumAsString(), "  LINE: ",
					ws.getCalcFields().getCalcRecordKey().getLiIdPackedAsString() });
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02811SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02811SA");
			// COB_CODE: MOVE 'FETCH CALC CURSOR '  TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("FETCH CALC CURSOR ");
			// COB_CODE: MOVE '2215-FETCH-CALC-CURSOR' TO DB2-ERR-PARA
			ws.setDb2ErrPara("2215-FETCH-CALC-CURSOR");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: PERFORM 1019-CLOSE-ROUTINE
			closeRoutine();
			// COB_CODE: MOVE +911 TO RETURN-CODE
			Session.setReturnCode(911);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;

		default:// COB_CODE: DISPLAY 'ERROR FETCHING CALC_CURSOR, CLAIM # '
			//               WS-CALC-CLM-CNTL-ID  WS-CALC-CLM-SFX-ID
			//               '  PMT: '  WS-CALC-CLM-PMT-NUM
			//               '  LINE: ' WS-CALC-CLM-LI-ID-PACKED
			DisplayUtil.sysout.write(new String[] { "ERROR FETCHING CALC_CURSOR, CLAIM # ",
					ws.getCalcFields().getCalcRecordKey().getCntlIdFormatted(), String.valueOf(ws.getCalcFields().getCalcRecordKey().getSfxId()),
					"  PMT: ", ws.getCalcFields().getCalcRecordKey().getPmtNumAsString(), "  LINE: ",
					ws.getCalcFields().getCalcRecordKey().getLiIdPackedAsString() });
			// COB_CODE: PERFORM 9940-DISPLAY-SQLCODE
			displaySqlcode();
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'         INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02811SA'                 TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02811SA");
			// COB_CODE: MOVE 'FETCH CALC CURSOR       ' TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("FETCH CALC CURSOR       ");
			// COB_CODE: MOVE '2215-FETCH-CALC-CURSOR  ' TO DB2-ERR-PARA
			ws.setDb2ErrPara("2215-FETCH-CALC-CURSOR  ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
	}

	/**Original name: 2220-GET-AND-SAVE-CALCS<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY ' *2220-GET-AND-SAVE-CALCS*'.
	 * ****************************************************************
	 *     FETCH ALL THE CALC INFORMATION FOR THE LINE AND MOVE IT    *
	 *     TO THE CALC FIELDS IN THE CSR- RECORD.  MAX CALCS IS 9.    *
	 * ****************************************************************</pre>*/
	private void getAndSaveCalcs() {
		// COB_CODE: EVALUATE HOLD-CALC-TYP-CD
		//             WHEN '1'
		//               ADD 1 TO TOTAL-CALC1-CNT
		//             WHEN '2'
		//               ADD 1 TO TOTAL-CALC2-CNT
		//             WHEN '3'
		//               ADD 1 TO TOTAL-CALC3-CNT
		//             WHEN '4'
		//               ADD 1 TO TOTAL-CALC4-CNT
		//             WHEN '5'
		//               ADD 1 TO TOTAL-CALC5-CNT
		//             WHEN '6'
		//               ADD 1 TO TOTAL-CALC6-CNT
		//             WHEN '7'
		//               ADD 1 TO TOTAL-CALC7-CNT
		//             WHEN '8'
		//               ADD 1 TO TOTAL-CALC8-CNT
		//             WHEN '9'
		//               ADD 1 TO TOTAL-CALC9-CNT
		//             WHEN OTHER
		//                   '  CALC-TYP-CD = ' HOLD-CALC-TYP-CD
		//           END-EVALUATE
		switch (ws.getCalcFields().getHoldCalcTypCd()) {

		case '1':// COB_CODE: COMPUTE CALC-SUB = 1
			ws.getProgramSubscripts().setCalcSub(((short) 1));
			// COB_CODE: ADD 1 TO TOTAL-CALC1-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc1Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc1Cnt(), 12));
			break;

		case '2':// COB_CODE: COMPUTE CALC-SUB = 2
			ws.getProgramSubscripts().setCalcSub(((short) 2));
			// COB_CODE: ADD 1 TO TOTAL-CALC2-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc2Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc2Cnt(), 12));
			break;

		case '3':// COB_CODE: COMPUTE CALC-SUB = 3
			ws.getProgramSubscripts().setCalcSub(((short) 3));
			// COB_CODE: ADD 1 TO TOTAL-CALC3-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc3Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc3Cnt(), 12));
			break;

		case '4':// COB_CODE: COMPUTE CALC-SUB = 4
			ws.getProgramSubscripts().setCalcSub(((short) 4));
			// COB_CODE: ADD 1 TO TOTAL-CALC4-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc4Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc4Cnt(), 12));
			break;

		case '5':// COB_CODE: COMPUTE CALC-SUB = 5
			ws.getProgramSubscripts().setCalcSub(((short) 5));
			// COB_CODE: ADD 1 TO TOTAL-CALC5-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc5Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc5Cnt(), 12));
			break;

		case '6':// COB_CODE: COMPUTE CALC-SUB = 6
			ws.getProgramSubscripts().setCalcSub(((short) 6));
			// COB_CODE: ADD 1 TO TOTAL-CALC6-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc6Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc6Cnt(), 12));
			break;

		case '7':// COB_CODE: COMPUTE CALC-SUB = 7
			ws.getProgramSubscripts().setCalcSub(((short) 7));
			// COB_CODE: ADD 1 TO TOTAL-CALC7-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc7Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc7Cnt(), 12));
			break;

		case '8':// COB_CODE: COMPUTE CALC-SUB = 8
			ws.getProgramSubscripts().setCalcSub(((short) 8));
			// COB_CODE: ADD 1 TO TOTAL-CALC8-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc8Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc8Cnt(), 12));
			break;

		case '9':// COB_CODE: COMPUTE CALC-SUB = 9
			ws.getProgramSubscripts().setCalcSub(((short) 9));
			// COB_CODE: ADD 1 TO TOTAL-CALC9-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc9Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc9Cnt(), 12));
			break;

		default:// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY 'INVALID CALC TYPE FOUND FOR CLAIM '
			//               WS-CALC-CLM-CNTL-ID  WS-CALC-CLM-SFX-ID
			//               '  PMT: '  WS-CALC-CLM-PMT-NUM
			//               '  LINE: ' WS-CALC-CLM-LI-ID-PACKED
			//               '.  CALC INFO WAS IGNORED.'
			//               '  CALC-TYP-CD = ' HOLD-CALC-TYP-CD
			DisplayUtil.sysout.write(new String[] { "INVALID CALC TYPE FOUND FOR CLAIM ", ws.getCalcFields().getCalcRecordKey().getCntlIdFormatted(),
					String.valueOf(ws.getCalcFields().getCalcRecordKey().getSfxId()), "  PMT: ",
					ws.getCalcFields().getCalcRecordKey().getPmtNumAsString(), "  LINE: ",
					ws.getCalcFields().getCalcRecordKey().getLiIdPackedAsString(), ".  CALC INFO WAS IGNORED.", "  CALC-TYP-CD = ",
					String.valueOf(ws.getCalcFields().getHoldCalcTypCd()) });
			break;
		}
		//
		// COB_CODE: MOVE HOLD-CALC-TYP-CD   TO CSR-CALC-TYP-CD(CALC-SUB)
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
				.setCalcTypCd(ws.getCalcFields().getHoldCalcTypCd());
		// COB_CODE: MOVE HOLD-COV-LOB-CD    TO CSR-CALC-COV-LOB-CD(CALC-SUB)
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
				.setCalcCovLobCd(ws.getCalcFields().getHoldCovLobCd());
		// COB_CODE: MOVE HOLD-CALC-EXPLN-CD TO CSR-CALC-EXPLN-CD(CALC-SUB)
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
				.setCalcExplnCd(ws.getCalcFields().getHoldCalcExplnCd());
		// COB_CODE: MOVE HOLD-PAY-CD        TO CSR-CALC-PAY-CD(CALC-SUB)
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
				.setCalcPayCd(ws.getCalcFields().getHoldPayCd());
		// COB_CODE: MOVE HOLD-ALW-CHG-AM    TO CSR-CALC-ALW-CHG-AM(CALC-SUB)
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
				.setCalcAlwChgAm(Trunc.toDecimal(ws.getCalcFields().getHoldAlwChgAm(), 11, 2));
		// COB_CODE: MOVE HOLD-PD-AM         TO CSR-CALC-PD-AM(CALC-SUB)
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
				.setCalcPdAm(Trunc.toDecimal(ws.getCalcFields().getHoldPdAm(), 11, 2));
		// COB_CODE: MOVE HOLD-CORP-CD       TO CSR-CALC-CORP-CD(CALC-SUB)
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
				.setCalcCorpCd(ws.getCalcFields().getHoldCorpCd());
		// COB_CODE: MOVE HOLD-PROD-CD       TO CSR-CALC-PROD-CD(CALC-SUB)
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
				.setCalcProdCd(ws.getCalcFields().getHoldProdCd());
		// COB_CODE: MOVE HOLD-TYP-BEN-CD    TO CSR-CALC-TYP-BEN-CD(CALC-SUB)
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
				.setCalcTypBenCd(ws.getCalcFields().getHoldTypBenCd());
		// COB_CODE: MOVE HOLD-SPECI-BEN-CD  TO CSR-CALC-SPECI-BEN-CD(CALC-SUB)
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
				.setCalcSpeciBenCdFormatted(ws.getCalcFields().getHoldSpeciBenCdFormatted());
		//*****************************************************************
		// FOR FEP BASIC AND BLUE FOCUS OPTIONS BOGUS GL ACCOUNT NUMBERS
		// ARE BRING ASSIGNED PER GROUP SO THE PAYMENTS CAN BE REPORTED
		// CORRECTLY ON THE SOTE REPORT IN NF0630ML.
		//*****************************************************************
		// COB_CODE: DISPLAY 'HOLD GL ACCT FOR CLAIM ' WS-CALC-CLM-CNTL-ID
		//                   ' IS ' HOLD-GL-ACCT-ID
		DisplayUtil.sysout.write("HOLD GL ACCT FOR CLAIM ", ws.getCalcFields().getCalcRecordKey().getCntlIdFormatted(), " IS ",
				ws.getCalcFields().getHoldGlAcctIdFormatted());
		// COB_CODE: IF HOLD-GL-ACCT-ID = '22730100'
		//              END-IF
		//           ELSE
		//           END-IF.
		if (Conditions.eq(ws.getCalcFields().getHoldGlAcctId(), "22730100")) {
			// COB_CODE: IF TRIG-MRKT-PKG-CD = 'FEP001'
			//           OR HOLD-GRP-ID =  '0999901'
			//               MOVE '22730100' TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
			//           ELSE
			//           END-IF
			//           END-IF
			if (Conditions.eq(ws.getTrigRecordLayout().getTrigMrktPkgCd(), "FEP001") || Conditions.eq(ws.getCalcFields().getHoldGrpId(), "0999901")) {
				// COB_CODE: MOVE '22730100' TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
				ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
						.setCalcGlAcctId("22730100");
			} else if (Conditions.eq(ws.getTrigRecordLayout().getTrigMrktPkgCd(), "FEP002")
					|| Conditions.eq(ws.getCalcFields().getHoldGrpId(), "0999903")) {
				// COB_CODE: IF TRIG-MRKT-PKG-CD = 'FEP002'
				//           OR HOLD-GRP-ID      = '0999903'
				//               MOVE '22730189' TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
				//           ELSE
				//           END-IF
				//           END-IF
				// COB_CODE: MOVE '22730189' TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
				ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
						.setCalcGlAcctId("22730189");
			} else if (Conditions.eq(ws.getTrigRecordLayout().getTrigMrktPkgCd(), "FEP003")
					|| Conditions.eq(ws.getCalcFields().getHoldGrpId(), "0999905")) {
				// COB_CODE: IF TRIG-MRKT-PKG-CD = 'FEP003'
				//           OR HOLD-GRP-ID      = '0999905'
				//               MOVE '22730199' TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
				//           END-IF
				// COB_CODE: MOVE '22730199' TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
				ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
						.setCalcGlAcctId("22730199");
			}
		} else if (Conditions.eq(ws.getCalcFields().getHoldGlAcctId(), "22730200")) {
			// COB_CODE:    IF HOLD-GL-ACCT-ID = '22730200'
			//                 END-IF
			//              ELSE
			//                        TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
			//           END-IF.
			// COB_CODE: IF TRIG-MRKT-PKG-CD = 'FEP001'
			//           OR HOLD-GRP-ID      = '0999901'
			//              MOVE '22730200' TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
			//           ELSE
			//             END-IF
			//           END-IF
			if (Conditions.eq(ws.getTrigRecordLayout().getTrigMrktPkgCd(), "FEP001") || Conditions.eq(ws.getCalcFields().getHoldGrpId(), "0999901")) {
				// COB_CODE: MOVE '22730200' TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
				ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
						.setCalcGlAcctId("22730200");
			} else if (Conditions.eq(ws.getTrigRecordLayout().getTrigMrktPkgCd(), "FEP002")
					|| Conditions.eq(ws.getCalcFields().getHoldGrpId(), "0999903")) {
				// COB_CODE: IF TRIG-MRKT-PKG-CD = 'FEP002'
				//           OR HOLD-GRP-ID      = '0999903'
				//              MOVE '22730289' TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
				//           ELSE
				//                END-IF
				//             END-IF
				// COB_CODE: MOVE '22730289' TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
				ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
						.setCalcGlAcctId("22730289");
			} else if (Conditions.eq(ws.getTrigRecordLayout().getTrigMrktPkgCd(), "FEP003")
					|| Conditions.eq(ws.getCalcFields().getHoldGrpId(), "0999905")) {
				// COB_CODE: IF TRIG-MRKT-PKG-CD = 'FEP003'
				//           OR HOLD-GRP-ID      =  '0999905'
				//              MOVE '22730299' TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
				//                END-IF
				// COB_CODE: MOVE '22730299' TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
				ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
						.setCalcGlAcctId("22730299");
			}
		} else {
			// COB_CODE: MOVE HOLD-GL-ACCT-ID
			//                  TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
					.setCalcGlAcctId(ws.getCalcFields().getHoldGlAcctId());
		}
		//**--HERE BF----------------------------------------
		//**-------------------------------------------------
		// COB_CODE: IF CSR-GRP-ID = '0999905'
		//             DISPLAY '* CSR-CLM-PD-AM        ' CSR-CLM-PD-AM
		//           END-IF.
		if (Conditions.eq(ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getGrpId(), "0999905")) {
			// COB_CODE: DISPLAY '* CSR-CLM-CNTL-ID      ' CSR-CLM-CNTL-ID
			DisplayUtil.sysout.write("* CSR-CLM-CNTL-ID      ", ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmCntlIdFormatted());
			// COB_CODE: DISPLAY '* CSR-CLM-CNTL-SFX-ID  ' CSR-CLM-CNTL-SFX-ID
			DisplayUtil.sysout.write("* CSR-CLM-CNTL-SFX-ID  ",
					String.valueOf(ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId()));
			// COB_CODE: DISPLAY '* CSR-CLM-PD-AM        ' CSR-CLM-PD-AM
			DisplayUtil.sysout.write("* CSR-CLM-PD-AM        ", ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClmPdAmAsString());
		}
		//**-------------------------------------------------
		//**  MOVE HOLD-GL-ACCT-ID    TO CSR-CALC-GL-ACCT-ID(CALC-SUB)
		// COB_CODE: MOVE HOLD-INS-TC-CD     TO CSR-CALC-INS-TC-CD(CALC-SUB)
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getCalcSub())
				.setCalcInsTcCd(ws.getCalcFields().getHoldInsTcCd());
		//    ELSE
		//        DISPLAY ' '
		//        DISPLAY 'INVALID CALC TYPE FOUND FOR CLAIM '
		//            WS-CALC-CLM-CNTL-ID  WS-CALC-CLM-SFX-ID
		//            '  PMT: '  WS-CALC-CLM-PMT-NUM
		//            '  LINE: ' WS-CALC-CLM-LI-ID-PACKED
		//            '.  CALC INFO WAS IGNORED.'
		//            '  CALC-TYP-CD = ' HOLD-CALC-TYP-CD
		//    END-IF.
		//    ---------------------------------------------------------
		//    -- CSR-LINE-CALC-EXPLN-CD WILL HOLD THE FIRST NON-BLANK
		//    -- EXPLANATION CODE FOUND ON THE CALC TABLE.
		//    ---------------------------------------------------------
		// COB_CODE: IF CSR-LINE-CALC-EXPLN-CD = SPACES
		//               MOVE HOLD-CALC-EXPLN-CD TO CSR-LINE-CALC-EXPLN-CD
		//           END-IF.
		if (Conditions.eq(ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getLineCalcExplnCd(), Types.SPACE_CHAR)) {
			// COB_CODE: MOVE HOLD-CALC-EXPLN-CD TO CSR-LINE-CALC-EXPLN-CD
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().setLineCalcExplnCd(ws.getCalcFields().getHoldCalcExplnCd());
		}
		//    ---------------------------------------------------------
		//    -- CSR-LINE-CALC-ALW-CHG-AM WILL HOLD THE FIRST NON-ZERO
		//    -- AMOUNT FOUND ON THE CALC TABLE.
		//    ---------------------------------------------------------
		// COB_CODE: IF CSR-LINE-CALC-ALW-CHG-AM = ZEROES
		//               MOVE HOLD-ALW-CHG-AM TO CSR-LINE-CALC-ALW-CHG-AM
		//           END-IF.
		if (ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getLineCalcAlwChgAm().compareTo(0) == 0) {
			// COB_CODE: MOVE HOLD-ALW-CHG-AM TO CSR-LINE-CALC-ALW-CHG-AM
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation()
					.setLineCalcAlwChgAm(Trunc.toDecimal(ws.getCalcFields().getHoldAlwChgAm(), 11, 2));
		}
		//    ---------------------------------------------------------
		//    -- CSR-LINE-CALC-PD-AM IS THE SUM OF ALL THE CALC PD AMTS.
		//    ---------------------------------------------------------
		// COB_CODE: ADD HOLD-PD-AM TO CSR-LINE-CALC-PD-AM.
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().setLineCalcPdAm(Trunc
				.toDecimal(ws.getCalcFields().getHoldPdAm().add(ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getLineCalcPdAm()), 11, 2));
		// COB_CODE: PERFORM 2215-FETCH-CALC-CURSOR.
		fetchCalcCursor();
	}

	/**Original name: 2222-GET-AND-SAVE-VOID-CALCS<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY ' *2222-GET-AND-SAVE-VOID-CALCS*'.
	 * ****************************************************************
	 *     FETCH ALL THE CALC INFORMATION FOR THE LINE AND MOVE IT    *
	 *     TO THE CALC FIELDS IN THE VSR- RECORD.  MAX CALCS IS 9.    *
	 * ****************************************************************</pre>*/
	private void getAndSaveVoidCalcs() {
		// COB_CODE: EVALUATE HOLD-CALC-TYP-CD
		//             WHEN '1'
		//               ADD 1 TO TOTAL-CALC1-CNT
		//             WHEN '2'
		//               ADD 1 TO TOTAL-CALC2-CNT
		//             WHEN '3'
		//               ADD 1 TO TOTAL-CALC3-CNT
		//             WHEN '4'
		//               ADD 1 TO TOTAL-CALC4-CNT
		//             WHEN '5'
		//               ADD 1 TO TOTAL-CALC5-CNT
		//             WHEN '6'
		//               ADD 1 TO TOTAL-CALC6-CNT
		//             WHEN '7'
		//               ADD 1 TO TOTAL-CALC7-CNT
		//             WHEN '8'
		//               ADD 1 TO TOTAL-CALC8-CNT
		//             WHEN '9'
		//               ADD 1 TO TOTAL-CALC9-CNT
		//             WHEN OTHER
		//                   '  CALC-TYP-CD = ' HOLD-CALC-TYP-CD
		//           END-EVALUATE
		switch (ws.getCalcFields().getHoldCalcTypCd()) {

		case '1':// COB_CODE: COMPUTE VOID-SUB = 1
			ws.getProgramSubscripts().setVoidSub(((short) 1));
			// COB_CODE: ADD 1 TO TOTAL-CALC1-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc1Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc1Cnt(), 12));
			break;

		case '2':// COB_CODE: COMPUTE VOID-SUB = 2
			ws.getProgramSubscripts().setVoidSub(((short) 2));
			// COB_CODE: ADD 1 TO TOTAL-CALC2-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc2Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc2Cnt(), 12));
			break;

		case '3':// COB_CODE: COMPUTE VOID-SUB = 3
			ws.getProgramSubscripts().setVoidSub(((short) 3));
			// COB_CODE: ADD 1 TO TOTAL-CALC3-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc3Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc3Cnt(), 12));
			break;

		case '4':// COB_CODE: COMPUTE VOID-SUB = 4
			ws.getProgramSubscripts().setVoidSub(((short) 4));
			// COB_CODE: ADD 1 TO TOTAL-CALC4-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc4Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc4Cnt(), 12));
			break;

		case '5':// COB_CODE: COMPUTE VOID-SUB = 5
			ws.getProgramSubscripts().setVoidSub(((short) 5));
			// COB_CODE: ADD 1 TO TOTAL-CALC5-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc5Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc5Cnt(), 12));
			break;

		case '6':// COB_CODE: COMPUTE VOID-SUB = 6
			ws.getProgramSubscripts().setVoidSub(((short) 6));
			// COB_CODE: ADD 1 TO TOTAL-CALC6-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc6Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc6Cnt(), 12));
			break;

		case '7':// COB_CODE: COMPUTE VOID-SUB = 7
			ws.getProgramSubscripts().setVoidSub(((short) 7));
			// COB_CODE: ADD 1 TO TOTAL-CALC7-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc7Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc7Cnt(), 12));
			break;

		case '8':// COB_CODE: COMPUTE VOID-SUB = 8
			ws.getProgramSubscripts().setVoidSub(((short) 8));
			// COB_CODE: ADD 1 TO TOTAL-CALC8-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc8Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc8Cnt(), 12));
			break;

		case '9':// COB_CODE: COMPUTE VOID-SUB = 9
			ws.getProgramSubscripts().setVoidSub(((short) 9));
			// COB_CODE: ADD 1 TO TOTAL-CALC9-CNT
			ws.getCalcFields().getCalcTypCounters()
					.setTotalCalc9Cnt(Trunc.toLong(1 + ws.getCalcFields().getCalcTypCounters().getTotalCalc9Cnt(), 12));
			break;

		default:// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY 'INVALID CALC TYPE FOUND FOR VOID CLAIM '
			//               WS-CALC-CLM-CNTL-ID  WS-CALC-CLM-SFX-ID
			//               '  PMT: '  WS-CALC-CLM-PMT-NUM
			//               '  LINE: ' WS-CALC-CLM-LI-ID-PACKED
			//               '.  CALC INFO WAS IGNORED.'
			//               '  CALC-TYP-CD = ' HOLD-CALC-TYP-CD
			DisplayUtil.sysout.write(new String[] { "INVALID CALC TYPE FOUND FOR VOID CLAIM ",
					ws.getCalcFields().getCalcRecordKey().getCntlIdFormatted(), String.valueOf(ws.getCalcFields().getCalcRecordKey().getSfxId()),
					"  PMT: ", ws.getCalcFields().getCalcRecordKey().getPmtNumAsString(), "  LINE: ",
					ws.getCalcFields().getCalcRecordKey().getLiIdPackedAsString(), ".  CALC INFO WAS IGNORED.", "  CALC-TYP-CD = ",
					String.valueOf(ws.getCalcFields().getHoldCalcTypCd()) });
			break;
		}
		//
		// COB_CODE: MOVE HOLD-CALC-TYP-CD   TO VSR-CALC-TYP-CD(VOID-SUB)
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
				.setCalcTypCd(ws.getCalcFields().getHoldCalcTypCd());
		// COB_CODE: MOVE HOLD-COV-LOB-CD    TO VSR-CALC-COV-LOB-CD(VOID-SUB)
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
				.setCalcCovLobCd(ws.getCalcFields().getHoldCovLobCd());
		// COB_CODE: MOVE HOLD-CALC-EXPLN-CD TO VSR-CALC-EXPLN-CD(VOID-SUB)
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
				.setCalcExplnCd(ws.getCalcFields().getHoldCalcExplnCd());
		// COB_CODE: MOVE HOLD-PAY-CD        TO VSR-CALC-PAY-CD(VOID-SUB)
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
				.setCalcPayCd(ws.getCalcFields().getHoldPayCd());
		// COB_CODE: MOVE HOLD-ALW-CHG-AM    TO VSR-CALC-ALW-CHG-AM(VOID-SUB)
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
				.setCalcAlwChgAm(Trunc.toDecimal(ws.getCalcFields().getHoldAlwChgAm(), 11, 2));
		// COB_CODE: MOVE HOLD-PD-AM         TO VSR-CALC-PD-AM(VOID-SUB)
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
				.setCalcPdAm(Trunc.toDecimal(ws.getCalcFields().getHoldPdAm(), 11, 2));
		// COB_CODE: MOVE HOLD-CORP-CD       TO VSR-CALC-CORP-CD(VOID-SUB)
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
				.setCalcCorpCd(ws.getCalcFields().getHoldCorpCd());
		// COB_CODE: MOVE HOLD-PROD-CD       TO VSR-CALC-PROD-CD(VOID-SUB)
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
				.setCalcProdCd(ws.getCalcFields().getHoldProdCd());
		// COB_CODE: MOVE HOLD-TYP-BEN-CD    TO VSR-CALC-TYP-BEN-CD(VOID-SUB)
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
				.setCalcTypBenCd(ws.getCalcFields().getHoldTypBenCd());
		// COB_CODE: MOVE HOLD-SPECI-BEN-CD  TO VSR-CALC-SPECI-BEN-CD(VOID-SUB)
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
				.setCalcSpeciBenCdFormatted(ws.getCalcFields().getHoldSpeciBenCdFormatted());
		//*****************************************************************
		// FOR FEP BASIC AND BLUE FOCUS OPTIONS BOGUS GL ACCOUNT NUMBERS
		// ARE BRING ASSIGNED PER GROUP SO THE PAYMENTS CAN BE REPORTED
		// CORRECTLY ON THE SOTE REPORT IN NF0630ML.
		//*****************************************************************
		// COB_CODE: IF HOLD-GL-ACCT-ID = '22730100'
		//              END-IF
		//           ELSE
		//           END-IF.
		if (Conditions.eq(ws.getCalcFields().getHoldGlAcctId(), "22730100")) {
			// COB_CODE: IF TRIG-MRKT-PKG-CD = 'FEP001'
			//           OR HOLD-GRP-ID      = '0999901'
			//               MOVE '22730100' TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
			//           ELSE
			//           END-IF
			//           END-IF
			if (Conditions.eq(ws.getTrigRecordLayout().getTrigMrktPkgCd(), "FEP001") || Conditions.eq(ws.getCalcFields().getHoldGrpId(), "0999901")) {
				// COB_CODE: MOVE '22730100' TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
				ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
						.setCalcGlAcctId("22730100");
			} else if (Conditions.eq(ws.getTrigRecordLayout().getTrigMrktPkgCd(), "FEP002")
					|| Conditions.eq(ws.getCalcFields().getHoldGrpId(), "0999903")) {
				// COB_CODE: IF TRIG-MRKT-PKG-CD = 'FEP002'
				//           OR HOLD-GRP-ID      = '0999903'
				//               MOVE '22730189' TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
				//           ELSE
				//           END-IF
				//           END-IF
				// COB_CODE: MOVE '22730189' TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
				ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
						.setCalcGlAcctId("22730189");
			} else if (Conditions.eq(ws.getTrigRecordLayout().getTrigMrktPkgCd(), "FEP003")
					|| Conditions.eq(ws.getCalcFields().getHoldGrpId(), "0999905")) {
				// COB_CODE: IF TRIG-MRKT-PKG-CD = 'FEP003'
				//           OR HOLD-GRP-ID      = '0999905'
				//               MOVE '22730199' TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
				//           END-IF
				// COB_CODE: MOVE '22730199' TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
				ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
						.setCalcGlAcctId("22730199");
			}
		} else if (Conditions.eq(ws.getCalcFields().getHoldGlAcctId(), "22730200")) {
			// COB_CODE:    IF HOLD-GL-ACCT-ID = '22730200'
			//                 END-IF
			//              ELSE
			//                        TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
			//           END-IF.
			// COB_CODE: IF TRIG-MRKT-PKG-CD = 'FEP001'
			//           OR HOLD-GRP-ID      = '0999901'
			//               MOVE '22730200' TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
			//           ELSE
			//           END-IF
			//           END-IF
			if (Conditions.eq(ws.getTrigRecordLayout().getTrigMrktPkgCd(), "FEP001") || Conditions.eq(ws.getCalcFields().getHoldGrpId(), "0999901")) {
				// COB_CODE: MOVE '22730200' TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
				ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
						.setCalcGlAcctId("22730200");
			} else if (Conditions.eq(ws.getTrigRecordLayout().getTrigMrktPkgCd(), "FEP002")
					|| Conditions.eq(ws.getCalcFields().getHoldGrpId(), "0999903")) {
				// COB_CODE: IF TRIG-MRKT-PKG-CD = 'FEP002'
				//           OR HOLD-GRP-ID      = '0999903'
				//               MOVE '22730289' TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
				//           ELSE
				//           END-IF
				//           END-IF
				// COB_CODE: MOVE '22730289' TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
				ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
						.setCalcGlAcctId("22730289");
			} else if (Conditions.eq(ws.getTrigRecordLayout().getTrigMrktPkgCd(), "FEP003")
					|| Conditions.eq(ws.getCalcFields().getHoldGrpId(), "0999905")) {
				// COB_CODE: IF TRIG-MRKT-PKG-CD = 'FEP003'
				//           OR HOLD-GRP-ID      = '0999905'
				//               MOVE '22730299' TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
				//           END-IF
				// COB_CODE: MOVE '22730299' TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
				ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
						.setCalcGlAcctId("22730299");
			}
		} else {
			// COB_CODE: MOVE HOLD-GL-ACCT-ID
			//                  TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
					.setCalcGlAcctId(ws.getCalcFields().getHoldGlAcctId());
		}
		//    MOVE HOLD-GL-ACCT-ID    TO VSR-CALC-GL-ACCT-ID(VOID-SUB)
		// COB_CODE: MOVE HOLD-INS-TC-CD     TO VSR-CALC-INS-TC-CD(VOID-SUB)
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(ws.getProgramSubscripts().getVoidSub())
				.setCalcInsTcCd(ws.getCalcFields().getHoldInsTcCd());
		//**--HERE BF----------------------------------------
		//**-------------------------------------------------
		// COB_CODE: IF VSR-GRP-ID = '0999905'
		//             DISPLAY '* VSR-CLM-PD-AM        ' VSR-CLM-PD-AM
		//           END-IF.
		if (Conditions.eq(ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getGrpId(), "0999905")) {
			// COB_CODE: DISPLAY '* VSR-CLM-CNTL-ID      ' VSR-CLM-CNTL-ID
			DisplayUtil.sysout.write("* VSR-CLM-CNTL-ID      ", ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmCntlIdFormatted());
			// COB_CODE: DISPLAY '* VSR-CLM-CNTL-SFX-ID  ' VSR-CLM-CNTL-SFX-ID
			DisplayUtil.sysout.write("* VSR-CLM-CNTL-SFX-ID  ",
					String.valueOf(ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId()));
			// COB_CODE: DISPLAY '* VSR-CLM-PD-AM        ' VSR-CLM-PD-AM
			DisplayUtil.sysout.write("* VSR-CLM-PD-AM        ", ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmPdAmAsString());
		}
		//**-------------------------------------------------
		//    ---------------------------------------------------------
		//    -- VSR-LINE-CALC-EXPLN-CD WILL HOLD THE FIRST NON-BLANK
		//    -- EXPLANATION CODE FOUND ON THE CALC TABLE.
		//    ---------------------------------------------------------
		// COB_CODE: IF VSR-LINE-CALC-EXPLN-CD = SPACES
		//               MOVE HOLD-CALC-EXPLN-CD TO VSR-LINE-CALC-EXPLN-CD
		//           END-IF.
		if (Conditions.eq(ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getLineCalcExplnCd(), Types.SPACE_CHAR)) {
			// COB_CODE: MOVE HOLD-CALC-EXPLN-CD TO VSR-LINE-CALC-EXPLN-CD
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().setLineCalcExplnCd(ws.getCalcFields().getHoldCalcExplnCd());
		}
		//    ---------------------------------------------------------
		//    -- VSR-LINE-CALC-ALW-CHG-AM WILL HOLD THE FIRST NON-ZERO
		//    -- AMOUNT FOUND ON THE CALC TABLE.
		//    ---------------------------------------------------------
		// COB_CODE: IF VSR-LINE-CALC-ALW-CHG-AM = ZEROES
		//               MOVE HOLD-ALW-CHG-AM TO VSR-LINE-CALC-ALW-CHG-AM
		//           END-IF.
		if (ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getLineCalcAlwChgAm().compareTo(0) == 0) {
			// COB_CODE: MOVE HOLD-ALW-CHG-AM TO VSR-LINE-CALC-ALW-CHG-AM
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation()
					.setLineCalcAlwChgAm(Trunc.toDecimal(ws.getCalcFields().getHoldAlwChgAm(), 11, 2));
		}
		//    ---------------------------------------------------------
		//    -- VSR-LINE-CALC-PD-AM IS THE SUM OF ALL THE CALC PD AMTS.
		//    ---------------------------------------------------------
		// COB_CODE: ADD HOLD-PD-AM TO VSR-LINE-CALC-PD-AM.
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().setLineCalcPdAm(Trunc
				.toDecimal(ws.getCalcFields().getHoldPdAm().add(ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getLineCalcPdAm()), 11, 2));
		// COB_CODE: PERFORM 2215-FETCH-CALC-CURSOR.
		fetchCalcCursor();
	}

	/**Original name: 2225-CLOSE-CALC-CURSOR<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
	private void closeCalcCursor() {
		ConcatUtil concatUtil = null;
		GenericParam abendCode = null;
		// COB_CODE: EXEC SQL
		//              CLOSE CALC_CURSOR
		//           END-EXEC.
		s02811saS02813saDao.closeCalcCursor();
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                 CONTINUE
		//             WHEN -911
		//                 CALL 'ABEND' USING ABEND-CODE
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		case -911:// COB_CODE: DISPLAY '-911 CLOSING CALC_CURSOR, CLAIM # '
			//             WS-CALC-CLM-CNTL-ID  WS-CALC-CLM-SFX-ID
			//             '  PMT: '  WS-CALC-CLM-PMT-NUM
			//             '  LINE: ' WS-CALC-CLM-LI-ID-PACKED
			DisplayUtil.sysout.write(new String[] { "-911 CLOSING CALC_CURSOR, CLAIM # ", ws.getCalcFields().getCalcRecordKey().getCntlIdFormatted(),
					String.valueOf(ws.getCalcFields().getCalcRecordKey().getSfxId()), "  PMT: ",
					ws.getCalcFields().getCalcRecordKey().getPmtNumAsString(), "  LINE: ",
					ws.getCalcFields().getCalcRecordKey().getLiIdPackedAsString() });
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02811SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02811SA");
			// COB_CODE: MOVE 'CLOSING CALC CURSOR ' TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("CLOSING CALC CURSOR ");
			// COB_CODE: MOVE '2225-CLOSE-CALC-CURSOR' TO DB2-ERR-PARA
			ws.setDb2ErrPara("2225-CLOSE-CALC-CURSOR");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: PERFORM 1019-CLOSE-ROUTINE
			closeRoutine();
			// COB_CODE: MOVE +911 TO RETURN-CODE
			Session.setReturnCode(911);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;

		default:// COB_CODE: DISPLAY 'ERROR CLOSING CALC_CURSOR, CLAIM # '
			//               WS-CALC-CLM-CNTL-ID  WS-CALC-CLM-SFX-ID
			//               '  PMT: '  WS-CALC-CLM-PMT-NUM
			//               '  LINE: ' WS-CALC-CLM-LI-ID-PACKED
			DisplayUtil.sysout.write(new String[] { "ERROR CLOSING CALC_CURSOR, CLAIM # ", ws.getCalcFields().getCalcRecordKey().getCntlIdFormatted(),
					String.valueOf(ws.getCalcFields().getCalcRecordKey().getSfxId()), "  PMT: ",
					ws.getCalcFields().getCalcRecordKey().getPmtNumAsString(), "  LINE: ",
					ws.getCalcFields().getCalcRecordKey().getLiIdPackedAsString() });
			// COB_CODE: PERFORM 9940-DISPLAY-SQLCODE
			displaySqlcode();
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'         INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02811SA'                 TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02811SA");
			// COB_CODE: MOVE 'CLOSE CALC CURSOR       ' TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("CLOSE CALC CURSOR       ");
			// COB_CODE: MOVE '2225-CLOSE-CALC-CURSOR  ' TO DB2-ERR-PARA
			ws.setDb2ErrPara("2225-CLOSE-CALC-CURSOR  ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
	}

	/**Original name: 2600-PROCESS-VOID-PMT<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '  *2600-PROCESS-VOID-PMT* '.
	 * * ------------------------------------------------------------ **
	 * * THIS BUILDS THE VOID FOR ALL NON-GMIS CLAIMS. THE VOID DOES  **
	 * * NOT NET WITH REPAY ANYMORE. THEY ARE TREATED AS TOTALLY      **
	 * * SEPARATE CLAIMS AS FAR AS DOLLAR AMOUNTS ARE CONCERNED WITH  **
	 * * THE EXCEPTION OF THE ACTUAL AMOUNT OF THE PROVIDER'S PAYMENT.**
	 * * ------------------------------------------------------------ **</pre>*/
	private void processVoidPmt() {
		// COB_CODE: MOVE SPACES TO VSR-PMT-COMMON-INFO.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().initPmtCommonInfoSpaces();
		// COB_CODE: INITIALIZE VSR-PMT-COMMON-INFO.
		initPmtCommonInfo1();
		// COB_CODE: MOVE SPACES TO VSR-CALC-INFORMATION.
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().initCalcInformationSpaces();
		// COB_CODE: INITIALIZE VSR-CALC-INFORMATION.
		initCalcInformation1();
		// COB_CODE: MOVE SPACES TO VSR-ARC-IND.
		ws.getNf07areaofVsrProgramLineArea().setArcInd(Types.SPACE_CHAR);
		// COB_CODE: MOVE CLM-CNTL-ID OF DCLS02813SA TO WS-CALC-CLM-CNTL-ID.
		ws.getCalcFields().getCalcRecordKey().setCntlId(ws.getDcls02813sa().getClmCntlId());
		// COB_CODE: MOVE CLM-CNTL-SFX-ID OF DCLS02813SA TO WS-CALC-CLM-SFX-ID.
		ws.getCalcFields().getCalcRecordKey().setSfxId(ws.getDcls02813sa().getClmCntlSfxId());
		// COB_CODE: MOVE CLM-PMT-ID OF DCLS02813SA  TO WS-CALC-CLM-PMT-NUM.
		ws.getCalcFields().getCalcRecordKey().setPmtNum(ws.getDcls02813sa().getClmPmtId());
		// COB_CODE: MOVE CLI-ID OF DCLS02809SA      TO WS-CALC-CLM-LI-ID-PACKED.
		ws.getCalcFields().getCalcRecordKey().setLiIdPacked(ws.getDcls02809sa().getCliId());
		// COB_CODE: PERFORM 2205-GET-VOID-CALC-INFO.
		getVoidCalcInfo();
		// COB_CODE: MOVE SPACES TO VSR-CASCO-CLM-ADJ-GRP-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().setCascoClmAdjGrpCd("");
		// COB_CODE: MOVE SPACES TO VSR-CASPR-CLM-ADJ-GRP-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().setCasprClmAdjGrpCd("");
		// COB_CODE: MOVE SPACES TO VSR-CASOA-CLM-ADJ-GRP-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().setCasoaClmAdjGrpCd("");
		// COB_CODE: MOVE SPACES TO VSR-CASPI-CLM-ADJ-GRP-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().setCaspiClmAdjGrpCd("");
		// COB_CODE: PERFORM 2613-INITIALIZE-VSR-CAS.
		initializeVsrCas();
		// COB_CODE: PERFORM 2615-INITIALIZE-XREF
		//               VARYING X-SUB FROM 1 BY 1
		//               UNTIL X-SUB > 6.
		ws.getProgramSubscripts().setxSub(((short) 1));
		while (!(ws.getProgramSubscripts().getxSub() > 6)) {
			initializeXref1();
			ws.getProgramSubscripts().setxSub(Trunc.toShort(ws.getProgramSubscripts().getxSub() + 1, 4));
		}
		// COB_CODE: MOVE SPACES TO VSR-INCD-ADJD-PROC-CD
		//                          VSR-INCD-ADJD-MOD1-CD
		//                          VSR-INCD-ADJD-MOD2-CD
		//                          VSR-INCD-ADJD-MOD3-CD
		//                          VSR-INCD-ADJD-MOD4-CD
		//                          VSR-INCD-RSN-CD.
		ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdProcCd("");
		ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdMod1Cd("");
		ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdMod2Cd("");
		ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdMod3Cd("");
		ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdMod4Cd("");
		ws.getNf07areaofVsrProgramLineArea().getXrefIncd().getRsnCd().setRsnCd("");
		// COB_CODE: PERFORM 2605-MAP-VOID-RECORD.
		mapVoidRecord();
	}

	/**Original name: 2605-MAP-VOID-RECORD<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '      *2605-MAP-VOID-RECORD*'.
	 *     --  THESE FIELDS SHOULD BE SAME FOR VOID & REPAY --</pre>*/
	private void mapVoidRecord() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE TRIG-PROD-IND                 TO VSR-PROD-IND
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getProdInd().setProdInd(ws.getTrigRecordLayout().getTrigProdInd());
		// COB_CODE: MOVE TRIG-LOCAL-BILL-PROV-LOB-CD   TO
		//                VSR-LOCAL-BILL-PROV-LOB-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillingProvider().getLocalBillProvLobCd()
				.setLocalBillProvLobCd(ws.getTrigRecordLayout().getTrigLocalBillProvLobCd());
		// COB_CODE: MOVE TRIG-HIST-BILL-PROV-NPI-ID    TO
		//                VSR-HIST-BILL-PROV-NPI-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillingProvider()
				.setHistBillProvNpiId(ws.getTrigRecordLayout().getTrigHistBillProvNpiId());
		// COB_CODE: MOVE TRIG-LOCAL-BILL-PROV-ID       TO VSR-LOCAL-BILL-PROV-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillingProvider()
				.setLocalBillProvId(ws.getTrigRecordLayout().getTrigLocalBillProvId());
		// COB_CODE: MOVE TRIG-HIST-PERF-PROV-NPI-ID
		//                                           TO VSR-HIST-PERF-PROV-NPI-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPerformingProvider()
				.setHistPerfProvNpiId(ws.getTrigRecordLayout().getTrigHistPerfProvNpiId());
		// COB_CODE: MOVE TRIG-LOCAL-PERF-PROV-LOB-CD TO
		//                VSR-LOCAL-PERF-PROV-LOB-CD
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPerformingProvider().getLocalPerfProvLobCd()
				.setLocalPerfProvLobCd(ws.getTrigRecordLayout().getTrigLocalPerfProvLobCd());
		// COB_CODE: MOVE TRIG-BILL-PROV-ID-QLF-CD   TO VSR-BILL-PROV-ID-QLF-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillProvIdQlfCd()
				.setBillProvIdQlfCd(ws.getTrigRecordLayout().getTrigBillProvIdQlfCd());
		// COB_CODE: MOVE TRIG-LOCAL-PERF-PROV-ID    TO VSR-LOCAL-PERF-PROV-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPerformingProvider()
				.setLocalPerfProvId(ws.getTrigRecordLayout().getTrigLocalPerfProvId());
		// COB_CODE: MOVE CLM-CNTL-ID OF DCLS02813SA TO VSR-CLM-CNTL-ID
		//                                              WS-CLM-CNTL-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setClmCntlIdFormatted(ws.getDcls02813sa().getClmCntlIdFormatted());
		ws.getWsSelectFields().setClmCntlId(ws.getDcls02813sa().getClmCntlId());
		// COB_CODE: MOVE CLM-CNTL-SFX-ID OF DCLS02813SA TO VSR-CLM-CNTL-SFX-ID
		//                                                  WS-CLM-CNTL-SFX-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setClmCntlSfxId(ws.getDcls02813sa().getClmCntlSfxId());
		ws.getWsSelectFields().setClmCntlSfxId(ws.getDcls02813sa().getClmCntlSfxId());
		// COB_CODE: IF (VSR-CLM-LOT-NUMBER = '14' OR '71' OR '72' OR '24'
		//                                  OR '81')
		//              END-IF
		//           ELSE
		//                TO VSR-MEDICARE-XOVER
		//           END-IF.
		if (Conditions.eq(ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmLotNumber(), "14")
				|| Conditions.eq(ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmLotNumber(), "71")
				|| Conditions.eq(ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmLotNumber(), "72")
				|| Conditions.eq(ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmLotNumber(), "24")
				|| Conditions.eq(ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmLotNumber(), "81")) {
			// COB_CODE: IF (VSR-CLM-CNTL-SFX-ID = 'G' OR 'H' OR 'P')
			//                TO VSR-MEDICARE-XOVER
			//           ELSE
			//                TO VSR-MEDICARE-XOVER
			//           END-IF
			if (ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId() == 'G'
					|| ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId() == 'H'
					|| ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId() == 'P') {
				// COB_CODE: MOVE 'X'
				//             TO VSR-MEDICARE-XOVER
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillingProvider().setMedicareXoverFormatted("X");
			} else {
				// COB_CODE: MOVE SPACES
				//             TO VSR-MEDICARE-XOVER
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillingProvider().setMedicareXover(Types.SPACE_CHAR);
			}
		} else {
			// COB_CODE: MOVE SPACES
			//             TO VSR-MEDICARE-XOVER
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillingProvider().setMedicareXover(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CLM-PMT-ID OF DCLS02813SA  TO VSR-PMT-ID
		//                                              WS-CLM-PMT-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPmtId(TruncAbs.toShort(ws.getDcls02813sa().getClmPmtId(), 2));
		ws.getWsSelectFields().setClmPmtId(ws.getDcls02813sa().getClmPmtId());
		// COB_CODE: MOVE CLI-ID OF DCLS02809SA      TO VSR-LI-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setLiId(TruncAbs.toShort(ws.getDcls02809sa().getCliId(), 3));
		// COB_CODE: MOVE TRIG-GMIS-INDICATOR        TO VSR-GMIS-INDICATOR.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getGmisIndicator().setGmisIndicator(ws.getTrigRecordLayout().getTrigGmisIndicator());
		// COB_CODE: MOVE TRIG-CORR-PRIORITY-SUB-ID    TO
		//                                         CSR-CORRECTED-PRIORITY-SUBID.
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCorrectedPrioritySubid(ws.getTrigRecordLayout().getTrigCorrPrioritySubId());
		// COB_CODE: MOVE 'VOID '                    TO VSR-RECORD-TYPE
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getRecordType().setRecordType("VOID ");
		// COB_CODE: MOVE TRIG-HIPAA-VERSION-FORMAT-ID TO VSR-VERSION-FORMAT-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getVersionFormatId()
				.setVersionFormatId(ws.getTrigRecordLayout().getTrigHipaaVersionFormatId());
		// COB_CODE: MOVE VBR-IN OF DCLS02813SA        TO VSR-VBR-IN.
		ws.getNf07areaofVsrProgramLineArea().setVbrIn(ws.getDcls02813sa().getVbrIn());
		// COB_CODE: MOVE MRKT-PKG-CD OF DCLS02813SA   TO VSR-MRKT-PKG-CD.
		ws.getNf07areaofVsrProgramLineArea().setMrktPkgCd(ws.getDcls02813sa().getMrktPkgCd());
		// COB_CODE: MOVE PROV-SBMT-LN-NO-ID OF DCLS02809SA
		//                                              TO VSR-PROV-SBMT-LN-NO-ID
		//                                                 WS-PROV-SBMT-LN-NO-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setProvSbmtLnNoId(TruncAbs.toShort(ws.getDcls02809sa().getProvSbmtLnNoId(), 3));
		ws.getWsSelectFields().setProvSbmtLnNoId(ws.getDcls02809sa().getProvSbmtLnNoId());
		// COB_CODE: MOVE ADDNL-RMT-LI-ID OF DCLS02809SA
		//                                               TO VSR-ADDNL-RMT-LI-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAddnlRmtLiId(TruncAbs.toShort(ws.getDcls02809sa().getAddnlRmtLiId(), 3));
		// COB_CODE: MOVE TRIG-ALPH-PRFX-CD              TO VSR-INSURED-PREFIX
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setInsuredPrefix(ws.getTrigRecordLayout().getTrigAlphPrfxCd());
		// COB_CODE: MOVE TRIG-INS-ID                    TO VSR-INSURED-NUMBER
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setInsuredNumber(ws.getTrigRecordLayout().getTrigInsId());
		// COB_CODE: MOVE MEM-ID     OF DCLS02813SA      TO VSR-MEMBER-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMemberId(ws.getDcls02813sa().getMemId());
		// COB_CODE: MOVE CLM-PD-AM  OF DCLS02813SA      TO VSR-CLM-PD-AM.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setClmPdAm(Trunc.toDecimal(ws.getDcls02813sa().getClmPdAm(), 11, 2));
		// COB_CODE: MOVE TRIG-DATE-COVERAGE-LAPSED  TO VSR-DATE-COVERAGE-LAPSED.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getDateCoverageLapsed()
				.setCorpRcvDtFormatted(ws.getTrigRecordLayout().getTrigDateCoverageLapsedFormatted());
		// COB_CODE: MOVE TRIG-ASG-CD                    TO VSR-ASG-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAsgCd().setAsgCd(ws.getTrigRecordLayout().getTrigAsgCd());
		// COB_CODE: MOVE ADJ-TYP-CD OF DCLS02813SA      TO VSR-ADJ-TYP-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjTypCd(ws.getDcls02813sa().getAdjTypCd());
		// COB_CODE: MOVE KCAPS-TEAM-NM OF DCLS02813SA   TO VSR-KCAPS-TEAM-NM.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setKcapsTeamNm(ws.getDcls02813sa().getKcapsTeamNm());
		// COB_CODE: MOVE KCAPS-USE-ID OF DCLS02813SA    TO VSR-KCAPS-USE-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setKcapsUseId(ws.getDcls02813sa().getKcapsUseId());
		// COB_CODE: MOVE HIST-LOAD-CD OF DCLS02813SA    TO VSR-HIST-LOAD-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setHistLoadCd(ws.getDcls02813sa().getHistLoadCd());
		// COB_CODE: MOVE PGM-AREA-CD  OF DCLS02813SA    TO VSR-PGM-AREA-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPgmAreaCd(ws.getDcls02813sa().getPgmAreaCd());
		// COB_CODE: MOVE DRG-CD OF DCLS02813SA          TO VSR-DRG-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setDrgCd(ws.getDcls02813sa().getDrgCd());
		// COB_CODE: MOVE SCHDL-DRG-ALW-AM OF DCLS02813SA TO VSR-SCHDL-DRG-ALW-AM.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setSchdlDrgAlwAm(Trunc.toDecimal(ws.getDcls02813sa().getSchdlDrgAlwAm(), 11, 2));
		// COB_CODE: MOVE TRIG-OI-PAY-NM                  TO VSR-OI-PAY-NM.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setOiPayNm(ws.getTrigRecordLayout().getTrigOiPayNm());
		// COB_CODE: MOVE TRIG-PAT-ACT-MED-REC-ID        TO VSR-PAT-ACT-MED-REC.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatActMedRec(ws.getTrigRecordLayout().getTrigPatActMedRecId());
		// COB_CODE: MOVE FIN-CD OF DCLS02813SA          TO VSR-FIN-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setFinCd(ws.getDcls02813sa().getFinCd());
		// COB_CODE: MOVE ITS-CLM-TYP-CD OF DCLS02813SA  TO VSR-ITS-CLM-TYP-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getItsInformation().getClmTypCd().setClmTypCd(ws.getDcls02813sa().getItsClmTypCd());
		// COB_CODE: MOVE TRIG-ITS-INS-ID                TO VSR-ITS-INS-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getItsInformation()
				.setInsIdFormatted(ws.getTrigRecordLayout().getTrigItsInsIdFormatted());
		// COB_CODE: MOVE TRIG-PRMPT-PAY-DAY-CD          TO VSR-PRMPT-PAY-DAY-CD
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPrmptPayDayCd(ws.getTrigRecordLayout().getTrigPrmptPayDayCd());
		// COB_CODE: MOVE TRIG-PRMPT-PAY-OVRD-CD         TO VSR-PRMPT-PAY-OVRD
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPrmptPayOvrd().setPrmptPayOvrd(ws.getTrigRecordLayout().getTrigPrmptPayOvrdCd());
		// COB_CODE: MOVE WS-ACCRUED-PRMPT-PAY-INT-AM    TO
		//                VSR-ACCRUED-PROMPT-PAY-INT
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo()
				.setAccruedPromptPayInt(Trunc.toDecimal(ws.getProgramHoldAreas().getWsAccruedPrmptPayIntAm(), 11, 2));
		// COB_CODE: MOVE TRIG-PROV-UNWRP-DT             TO VSR-PROV-UNWRP-DT
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getProvUnwrpDt()
				.setCorpRcvDtFormatted(ws.getTrigRecordLayout().getTrigProvUnwrpDtFormatted());
		// COB_CODE: MOVE TYP-GRP-CD OF DCLS02813SA      TO VSR-TYP-GRP-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setTypGrpCd(ws.getDcls02813sa().getTypGrpCd());
		// COB_CODE: MOVE NTWRK-CD OF DCLS02813SA        TO VSR-NTWRK-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setNtwrkCd(ws.getDcls02813sa().getNtwrkCd());
		// COB_CODE: MOVE ENR-CL-CD OF DCLS02813SA       TO VSR-ENR-CL-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setEnrClCd(ws.getDcls02813sa().getEnrClCd());
		// COB_CODE: MOVE LI-FRM-SERV-DT OF DCLS02809SA
		//                                               TO VSR-LI-FRM-SERV-DT.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getLiFrmServDt().setCorpRcvDtFormatted(ws.getDcls02809sa().getLiFrmServDtFormatted());
		// COB_CODE: MOVE LI-THR-SERV-DT OF DCLS02809SA
		//                                               TO VSR-LI-THR-SERV-DT.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getLiThrServDt().setCorpRcvDtFormatted(ws.getDcls02809sa().getLiThrServDtFormatted());
		// COB_CODE: MOVE EXMN-ACTN-CD OF DCLS02809SA    TO VSR-EXMN-ACTN-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getExmnActnCd().setExmnActnCd(ws.getDcls02809sa().getExmnActnCd());
		// COB_CODE: MOVE DNL-RMRK-CD OF DCLS02809SA     TO VSR-DNL-RMRK-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setDnlRmrkCd(ws.getDcls02809sa().getDnlRmrkCd());
		// COB_CODE: MOVE DNL-RMRK-USG-CD OF DCLS02993SA TO VSR-DNL-RMRK-USG-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setDnlRmrkUsgCd(ws.getDcls02993sa().getDnlRmrkUsgCd());
		// COB_CODE: MOVE PROC-CD OF DCLS02809SA         TO VSR-PROC-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setCd(ws.getDcls02809sa().getProcCd());
		// COB_CODE: MOVE PROC-MOD1-CD OF DCLS02809SA    TO VSR-PROC-MOD1-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod1Cd(ws.getDcls02809sa().getProcMod1Cd());
		// COB_CODE: MOVE PROC-MOD2-CD OF DCLS02809SA    TO VSR-PROC-MOD2-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod2Cd(ws.getDcls02809sa().getProcMod2Cd());
		// COB_CODE: MOVE PROC-MOD3-CD OF DCLS02809SA    TO VSR-PROC-MOD3-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod3Cd(ws.getDcls02809sa().getProcMod3Cd());
		// COB_CODE: MOVE PROC-MOD4-CD OF DCLS02809SA    TO VSR-PROC-MOD4-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod4Cd(ws.getDcls02809sa().getProcMod4Cd());
		// COB_CODE: MOVE REV-CD OF DCLS02809SA          TO VSR-REV-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setRevCd(ws.getDcls02809sa().getRevCd());
		// COB_CODE: MOVE UNIT-CT OF DCLS02809SA         TO VSR-UNIT-CT.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setUnitCt(Trunc.toDecimal(ws.getDcls02809sa().getUnitCt(), 7, 2));
		// COB_CODE: MOVE ORIG-SBMT-UNIT-CT OF DCLS02809SA TO
		//                VSR-ORIG-SUB-UNIT-CT.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setOrigSubUnitCt(Trunc.toDecimal(ws.getDcls02809sa().getOrigSbmtUnitCt(), 7, 2));
		// COB_CODE: MOVE POS-CD OF DCLS02809SA          TO VSR-PLACE-OF-SERVICE
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPlaceOfService(ws.getDcls02809sa().getPosCd());
		// COB_CODE: MOVE TS-CD OF DCLS02809SA           TO VSR-TYPE-OF-SERVICE
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setTypeOfService(ws.getDcls02809sa().getTsCd());
		// COB_CODE: MOVE ORIG-SBMT-CHG-AM OF DCLS02809SA TO VSR-ORIG-SBMT-CHG-AM.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setOrigSbmtChgAm(Trunc.toDecimal(ws.getDcls02809sa().getOrigSbmtChgAm(), 11, 2));
		// COB_CODE: MOVE CHG-AM OF DCLS02809SA          TO VSR-CHG-AM.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setChgAm(Trunc.toDecimal(ws.getDcls02809sa().getChgAm(), 11, 2));
		// COB_CODE: MOVE LI-ALCT-OI-PD-AM OF DCLS02809SA
		//                                               TO VSR-LI-ALCT-OI-PD-AM.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setLiAlctOiPdAm(Trunc.toDecimal(ws.getDcls02809sa().getLiAlctOiPdAm(), 11, 2));
		// COB_CODE: MOVE GL-OFST-VOID-CD OF DCLS02813SA TO VSR-GL-OFST-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setGlOfstCd(ws.getDcls02813sa().getGlOfstVoidCd());
		// COB_CODE: MOVE GL-SOTE-VOID-CD OF DCLS02813SA TO VSR-GL-SOTE-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setGlSoteCd(TruncAbs.toShort(ws.getDcls02813sa().getGlSoteVoidCd(), 2));
		// COB_CODE: MOVE ADJD-OVRD1-CD OF DCLS02809SA   TO VSR-ADJD-OVRD1-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd1Cd(ws.getDcls02809sa().getAdjdOvrd1Cd());
		// COB_CODE: MOVE ADJD-OVRD2-CD OF DCLS02809SA   TO VSR-ADJD-OVRD2-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd2Cd(ws.getDcls02809sa().getAdjdOvrd2Cd());
		// COB_CODE: MOVE ADJD-OVRD3-CD OF DCLS02809SA   TO VSR-ADJD-OVRD3-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd3Cd(ws.getDcls02809sa().getAdjdOvrd3Cd());
		// COB_CODE: MOVE ADJD-OVRD4-CD OF DCLS02809SA   TO VSR-ADJD-OVRD4-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd4Cd(ws.getDcls02809sa().getAdjdOvrd4Cd());
		// COB_CODE: MOVE ADJD-OVRD5-CD OF DCLS02809SA   TO VSR-ADJD-OVRD5-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd5Cd(ws.getDcls02809sa().getAdjdOvrd5Cd());
		// COB_CODE: MOVE ADJD-OVRD6-CD OF DCLS02809SA   TO VSR-ADJD-OVRD6-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd6Cd(ws.getDcls02809sa().getAdjdOvrd6Cd());
		// COB_CODE: MOVE ADJD-OVRD7-CD OF DCLS02809SA   TO VSR-ADJD-OVRD7-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd7Cd(ws.getDcls02809sa().getAdjdOvrd7Cd());
		// COB_CODE: MOVE ADJD-OVRD8-CD OF DCLS02809SA   TO VSR-ADJD-OVRD8-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd8Cd(ws.getDcls02809sa().getAdjdOvrd8Cd());
		// COB_CODE: MOVE ADJD-OVRD9-CD OF DCLS02809SA   TO VSR-ADJD-OVRD9-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd9Cd(ws.getDcls02809sa().getAdjdOvrd9Cd());
		// COB_CODE: MOVE ADJD-OVRD10-CD OF DCLS02809SA  TO VSR-ADJD-OVRD10-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd10Cd(ws.getDcls02809sa().getAdjdOvrd10Cd());
		// COB_CODE: MOVE VOID-CD OF DCLS02813SA         TO VSR-VOID-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setVoidCd(ws.getDcls02813sa().getVoidCd());
		// COB_CODE: MOVE ADJ-RESP-CD OF DCLS02813SA     TO VSR-ADJ-RESP-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjRespCd(ws.getDcls02813sa().getAdjRespCd());
		// COB_CODE: MOVE ADJ-TRCK-CD OF DCLS02813SA     TO VSR-ADJ-TRCK-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjTrckCd(ws.getDcls02813sa().getAdjTrckCd());
		// COB_CODE: MOVE LOB-CD OF DCLS02813SA          TO VSR-CLAIM-LOB-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClaimLobCd().setClaimLobCd(ws.getDcls02813sa().getLobCd());
		// COB_CODE: MOVE REF-ID OF DCLS02713SA          TO VSR-REF-ID
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setRefId(ws.getDcls02713sa().getRefId());
		//
		// COB_CODE: MOVE 'N' TO WS-PROFESSIONAL-DATA-FOUND-SW
		//                       WS-INSTITUTIONAL-DATA-FOUND-SW
		ws.getProgramSwitches().setWsProfessionalDataFoundSwFormatted("N");
		ws.getProgramSwitches().setWsInstitutionalDataFoundSwFormatted("N");
		//**
		// COB_CODE: PERFORM 6022-GET-PROFESSIONAL-DATA
		getProfessionalData();
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                        VSR-ORIG-QUANTITY-COUNT
		//               WHEN 100
		//                   INITIALIZE VSR-ORIGINAL-SUBMITTED-INFO
		//           END-EVALUATE
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE 'Y' TO WS-PROFESSIONAL-DATA-FOUND-SW
			ws.getProgramSwitches().setWsProfessionalDataFoundSwFormatted("Y");
			// COB_CODE: MOVE PROD-SRV-ID        OF DCLS02717SA TO
			//                VSR-ORIG-PROCEDURE-SRV-ID
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureSrvId(ws.getDcls02717sa().getProdSrvId());
			// COB_CODE: MOVE PROD-SRV-ID-QLF-CD OF DCLS02717SA TO
			//                VSR-ORIG-ID-QLF-CD
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setIdQlfCd(ws.getDcls02717sa().getProdSrvIdQlfCd());
			// COB_CODE: MOVE SV1013-PROC-MOD-CD OF DCLS02717SA TO
			//                VSR-ORIG-PROCEDURE-MOD-1
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
					.setProcedureMod1(ws.getDcls02717sa().getSv1013ProcModCd());
			// COB_CODE: MOVE SV1014-PROC-MOD-CD OF DCLS02717SA TO
			//                VSR-ORIG-PROCEDURE-MOD-2
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
					.setProcedureMod2(ws.getDcls02717sa().getSv1014ProcModCd());
			// COB_CODE: MOVE SV1015-PROC-MOD-CD OF DCLS02717SA TO
			//                VSR-ORIG-PROCEDURE-MOD-3
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
					.setProcedureMod3(ws.getDcls02717sa().getSv1015ProcModCd());
			// COB_CODE: MOVE SV1016-PROC-MOD-CD OF DCLS02717SA TO
			//                VSR-ORIG-PROCEDURE-MOD-4
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
					.setProcedureMod4(ws.getDcls02717sa().getSv1016ProcModCd());
			// COB_CODE: MOVE SV102-MNTRY-AM     OF DCLS02717SA TO
			//                VSR-ORIG-MONETARY-AMOUNT
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
					.setMonetaryAmount(Trunc.toDecimal(ws.getDcls02717sa().getSv102MntryAm(), 11, 2));
			// COB_CODE: MOVE QNTY-CT            OF DCLS02717SA TO
			//                VSR-ORIG-QUANTITY-COUNT
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
					.setQuantityCount(Trunc.toDecimal(ws.getDcls02717sa().getQntyCt(), 7, 2));
			break;

		case 100:// COB_CODE: INITIALIZE VSR-ORIGINAL-SUBMITTED-INFO
			initOriginalSubmittedInfo1();
			break;

		default:
			break;
		}
		//
		// COB_CODE: IF PROFESSIONAL-DATA-FOUND
		//              CONTINUE
		//           ELSE
		//               END-EVALUATE
		//           END-IF
		if (!ws.getProgramSwitches().isProfessionalDataFound()) {
			// COB_CODE: PERFORM 6021-GET-INSTITUTIONAL-DATA
			getInstitutionalData();
			// COB_CODE: EVALUATE SQLCODE
			//           WHEN 0
			//                    VSR-ORIG-QUANTITY-COUNT
			//           WHEN 100
			//               INITIALIZE VSR-ORIGINAL-SUBMITTED-INFO
			//           END-EVALUATE
			switch (sqlca.getSqlcode()) {

			case 0:// COB_CODE: MOVE 'Y' TO WS-INSTITUTIONAL-DATA-FOUND-SW
				ws.getProgramSwitches().setWsInstitutionalDataFoundSwFormatted("Y");
				// COB_CODE: MOVE SV201-PROD-SRV-ID  OF DCLS02718SA TO
				//                VSR-ORIG-REVENUE-SRV-ID
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setRevenueSrvId(ws.getDcls02718sa().getSv201ProdSrvId());
				// COB_CODE: MOVE SV2022-PROD-SRV-ID  OF DCLS02718SA TO
				//                VSR-ORIG-PROCEDURE-SRV-ID
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureSrvId(ws.getDcls02718sa().getSv2022ProdSrvId());
				// COB_CODE: MOVE PROD-SRV-ID-QLF-CD OF DCLS02718SA TO
				//                VSR-ORIG-ID-QLF-CD
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setIdQlfCd(ws.getDcls02718sa().getProdSrvIdQlfCd());
				// COB_CODE: MOVE SV2023-PROC-MOD-CD OF DCLS02718SA TO
				//                VSR-ORIG-PROCEDURE-MOD-1
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod1(ws.getDcls02718sa().getSv2023ProcModCd());
				// COB_CODE: MOVE SV2024-PROC-MOD-CD OF DCLS02718SA TO
				//                VSR-ORIG-PROCEDURE-MOD-2
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod2(ws.getDcls02718sa().getSv2024ProcModCd());
				// COB_CODE: MOVE SV2025-PROC-MOD-CD OF DCLS02718SA TO
				//                VSR-ORIG-PROCEDURE-MOD-3
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod3(ws.getDcls02718sa().getSv2025ProcModCd());
				// COB_CODE: MOVE SV2026-PROC-MOD-CD OF DCLS02718SA TO
				//                VSR-ORIG-PROCEDURE-MOD-4
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod4(ws.getDcls02718sa().getSv2026ProcModCd());
				// COB_CODE: MOVE SV203-MNTRY-AM     OF DCLS02718SA TO
				//                VSR-ORIG-MONETARY-AMOUNT
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setMonetaryAmount(Trunc.toDecimal(ws.getDcls02718sa().getSv203MntryAm(), 11, 2));
				// COB_CODE: MOVE QNTY-CT            OF DCLS02718SA TO
				//                VSR-ORIG-QUANTITY-COUNT
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setQuantityCount(Trunc.toDecimal(ws.getDcls02718sa().getQntyCt(), 7, 2));
				break;

			case 100:// COB_CODE: INITIALIZE VSR-ORIGINAL-SUBMITTED-INFO
				initOriginalSubmittedInfo1();
				break;

			default:
				break;
			}
		}
		//
		// COB_CODE: IF PROFESSIONAL-DATA-FOUND OR
		//              INSTITUTIONAL-DATA-FOUND
		//              CONTINUE
		//           ELSE
		//               END-IF
		//           END-IF
		if (!(ws.getProgramSwitches().isProfessionalDataFound() || ws.getProgramSwitches().isInstitutionalDataFound())) {
			// COB_CODE: PERFORM 6020-GET-DENTAL-DATA
			getDentalData();
			// COB_CODE: IF SQLCODE = 0
			//                    VSR-ORIG-QUANTITY-COUNT
			//           ELSE
			//               INITIALIZE VSR-ORIGINAL-SUBMITTED-INFO
			//           END-IF
			if (sqlca.getSqlcode() == 0) {
				// COB_CODE: MOVE PROD-SRV-ID        OF DCLS02719SA TO
				//                VSR-ORIG-PROCEDURE-SRV-ID
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureSrvId(ws.getDcls02719sa().getProdSrvId());
				// COB_CODE: MOVE PROD-SRV-ID-QLF-CD OF DCLS02719SA TO
				//                VSR-ORIG-ID-QLF-CD
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setIdQlfCd(ws.getDcls02719sa().getProdSrvIdQlfCd());
				// COB_CODE: MOVE SV3013-PROC-MOD-CD OF DCLS02719SA TO
				//                VSR-ORIG-PROCEDURE-MOD-1
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod1(ws.getDcls02719sa().getSv3013ProcModCd());
				// COB_CODE: MOVE SV3014-PROC-MOD-CD OF DCLS02719SA TO
				//                VSR-ORIG-PROCEDURE-MOD-2
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod2(ws.getDcls02719sa().getSv3014ProcModCd());
				// COB_CODE: MOVE SV3015-PROC-MOD-CD OF DCLS02719SA TO
				//                VSR-ORIG-PROCEDURE-MOD-3
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod3(ws.getDcls02719sa().getSv3015ProcModCd());
				// COB_CODE: MOVE SV3016-PROC-MOD-CD OF DCLS02719SA TO
				//                VSR-ORIG-PROCEDURE-MOD-4
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setProcedureMod4(ws.getDcls02719sa().getSv3016ProcModCd());
				// COB_CODE: MOVE MNTRY-AM           OF DCLS02719SA TO
				//                VSR-ORIG-MONETARY-AMOUNT
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setMonetaryAmount(Trunc.toDecimal(ws.getDcls02719sa().getMntryAm(), 11, 2));
				// COB_CODE: MOVE QNTY-CT            OF DCLS02719SA TO
				//                VSR-ORIG-QUANTITY-COUNT
				ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo()
						.setQuantityCount(Trunc.toDecimal(ws.getDcls02719sa().getQntyCt(), 7, 2));
			} else {
				// COB_CODE: INITIALIZE VSR-ORIGINAL-SUBMITTED-INFO
				initOriginalSubmittedInfo1();
			}
		}
		//
		// COB_CODE: PERFORM 6030-GET-MEMBER-DATA
		getMemberData();
		//
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//               MOVE SSN-ID     OF DCLS02952SA TO VSR-MBR-SSN-ID
		//             WHEN +100
		//                               VSR-MBR-SSN-ID
		//                               VSR-MBR-SSN-ID
		//                               VSR-MBR-SSN-ID
		//                               VSR-MBR-SSN-ID
		//                               VSR-MBR-SSN-ID
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE LST-ORG-NM OF DCLS02952SA TO VSR-MBR-LST-ORG-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrLstOrgNm(ws.getDcls02952sa().getLstOrgNm());
			// COB_CODE: MOVE FRST-NM    OF DCLS02952SA TO VSR-MBR-FRST-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrFrstNm(ws.getDcls02952sa().getFrstNm());
			// COB_CODE: MOVE MID-NM     OF DCLS02952SA TO VSR-MBR-MID-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrMidNm(ws.getDcls02952sa().getMidNm());
			// COB_CODE: MOVE SFX-NM     OF DCLS02952SA TO VSR-MBR-SFX-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrSfxNm(ws.getDcls02952sa().getSfxNm());
			// COB_CODE: MOVE SSN-ID     OF DCLS02952SA TO VSR-MBR-SSN-ID
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrSsnId(ws.getDcls02952sa().getSsnId());
			break;

		case 100:// COB_CODE: MOVE SPACES  TO VSR-MBR-LST-ORG-NM
			//                           VSR-MBR-FRST-NM
			//                           VSR-MBR-MID-NM
			//                           VSR-MBR-SFX-NM
			//                           VSR-MBR-SSN-ID
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrLstOrgNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrFrstNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrMidNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrSfxNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrSsnId("");
			break;

		default:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02952SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02952SA");
			// COB_CODE: MOVE 'READ MBR DATA     '  TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("READ MBR DATA     ");
			// COB_CODE: MOVE '6030-GET-MEMBER   '  TO DB2-ERR-PARA
			ws.setDb2ErrPara("6030-GET-MEMBER   ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
		//
		// COB_CODE: PERFORM 6031-GET-PATIENT-DATA
		getPatientData();
		//
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//               MOVE SSN-ID     OF DCLS02952SA TO VSR-PAT-SSN-ID
		//             WHEN +100
		//                               VSR-PAT-SSN-ID
		//                               VSR-PAT-SSN-ID
		//                               VSR-PAT-SSN-ID
		//                               VSR-PAT-SSN-ID
		//                               VSR-PAT-SSN-ID
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE LST-ORG-NM OF DCLS02952SA TO VSR-PAT-LST-ORG-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatLstOrgNm(ws.getDcls02952sa().getLstOrgNm());
			// COB_CODE: MOVE FRST-NM    OF DCLS02952SA TO VSR-PAT-FRST-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatFrstNm(ws.getDcls02952sa().getFrstNm());
			// COB_CODE: MOVE MID-NM     OF DCLS02952SA TO VSR-PAT-MID-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatMidNm(ws.getDcls02952sa().getMidNm());
			// COB_CODE: MOVE SFX-NM     OF DCLS02952SA TO VSR-PAT-SFX-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatSfxNm(ws.getDcls02952sa().getSfxNm());
			// COB_CODE: MOVE SSN-ID     OF DCLS02952SA TO VSR-PAT-SSN-ID
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatSsnId(ws.getDcls02952sa().getSsnId());
			break;

		case 100:// COB_CODE: MOVE SPACES  TO VSR-PAT-LST-ORG-NM
			//                           VSR-PAT-FRST-NM
			//                           VSR-PAT-MID-NM
			//                           VSR-PAT-SFX-NM
			//                           VSR-PAT-SSN-ID
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatLstOrgNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatFrstNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatMidNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatSfxNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatSsnId("");
			break;

		default:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02952SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02952SA");
			// COB_CODE: MOVE 'READ PATIENT DATA '  TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("READ PATIENT DATA ");
			// COB_CODE: MOVE '6031-GET-PATIENT  '  TO DB2-ERR-PARA
			ws.setDb2ErrPara("6031-GET-PATIENT  ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
		//
		// COB_CODE: PERFORM 6032-GET-PERF-DATA
		getPerfData();
		//
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                                        TO VSR-PERF-ENTY-TYP-QLF-CD
		//             WHEN +100
		//                               VSR-PERF-ENTY-TYP-QLF-CD
		//                               VSR-PERF-ENTY-TYP-QLF-CD
		//                               VSR-PERF-ENTY-TYP-QLF-CD
		//                               VSR-PERF-ENTY-TYP-QLF-CD
		//                               VSR-PERF-ENTY-TYP-QLF-CD
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE LST-ORG-NM OF DCLS02952SA TO VSR-PERF-LST-ORG-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfLstOrgNm(ws.getDcls02952sa().getLstOrgNm());
			// COB_CODE: MOVE FRST-NM    OF DCLS02952SA TO VSR-PERF-FRST-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfFrstNm(ws.getDcls02952sa().getFrstNm());
			// COB_CODE: MOVE MID-NM     OF DCLS02952SA TO VSR-PERF-MID-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfMidNm(ws.getDcls02952sa().getMidNm());
			// COB_CODE: MOVE SFX-NM     OF DCLS02952SA TO VSR-PERF-SFX-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfSfxNm(ws.getDcls02952sa().getSfxNm());
			// COB_CODE: MOVE ENTY-TYP-QLF-CD OF DCLS02952SA
			//                                    TO VSR-PERF-ENTY-TYP-QLF-CD
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfEntyTypQlfCd(ws.getDcls02952sa().getEntyTypQlfCd());
			break;

		case 100:// COB_CODE: MOVE SPACES  TO VSR-PERF-LST-ORG-NM
			//                           VSR-PERF-FRST-NM
			//                           VSR-PERF-MID-NM
			//                           VSR-PERF-SFX-NM
			//                           VSR-PERF-ENTY-TYP-QLF-CD
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfLstOrgNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfFrstNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfMidNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfSfxNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfEntyTypQlfCd(Types.SPACE_CHAR);
			break;

		default:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02952SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02952SA");
			// COB_CODE: MOVE 'READ PERF DATA    '  TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("READ PERF DATA    ");
			// COB_CODE: MOVE '6033-PERF-DATA    '  TO DB2-ERR-PARA
			ws.setDb2ErrPara("6033-PERF-DATA    ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
		//
		// COB_CODE: PERFORM 6033-GET-BILLED-DATA
		getBilledData();
		//
		// COB_CODE: EVALUATE SQLCODE
		//             WHEN 0
		//                                            TO VSR-BILL-ENTY-TYP-QLF-CD
		//             WHEN +100
		//                               VSR-BILL-ENTY-TYP-QLF-CD
		//                               VSR-BILL-ENTY-TYP-QLF-CD
		//                               VSR-BILL-ENTY-TYP-QLF-CD
		//                               VSR-BILL-ENTY-TYP-QLF-CD
		//                               VSR-BILL-ENTY-TYP-QLF-CD
		//             WHEN OTHER
		//               CALL 'ABEND'
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE LST-ORG-NM OF DCLS02952SA TO VSR-BILL-LST-ORG-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillLstOrgNm(ws.getDcls02952sa().getLstOrgNm());
			// COB_CODE: MOVE FRST-NM    OF DCLS02952SA TO VSR-BILL-FRST-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillFrstNm(ws.getDcls02952sa().getFrstNm());
			// COB_CODE: MOVE MID-NM     OF DCLS02952SA TO VSR-BILL-MID-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillMidNm(ws.getDcls02952sa().getMidNm());
			// COB_CODE: MOVE SFX-NM     OF DCLS02952SA TO VSR-BILL-SFX-NM
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillSfxNm(ws.getDcls02952sa().getSfxNm());
			// COB_CODE: MOVE ENTY-TYP-QLF-CD OF DCLS02952SA
			//                                        TO VSR-BILL-ENTY-TYP-QLF-CD
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillEntyTypQlfCd(ws.getDcls02952sa().getEntyTypQlfCd());
			break;

		case 100:// COB_CODE: MOVE SPACES  TO VSR-BILL-LST-ORG-NM
			//                           VSR-BILL-FRST-NM
			//                           VSR-BILL-MID-NM
			//                           VSR-BILL-SFX-NM
			//                           VSR-BILL-ENTY-TYP-QLF-CD
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillLstOrgNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillFrstNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillMidNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillSfxNm("");
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillEntyTypQlfCd(Types.SPACE_CHAR);
			break;

		default:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02952SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02952SA");
			// COB_CODE: MOVE 'READ BILLING DATA '  TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("READ BILLING DATA ");
			// COB_CODE: MOVE '6032-GET-BILLING  '  TO DB2-ERR-PARA
			ws.setDb2ErrPara("6032-GET-BILLING  ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: CALL 'ABEND'
			DynamicCall.invoke("ABEND");
			break;
		}
		// COB_CODE: MOVE  ID-CD      OF DCLS02682SA  TO  VSR-837-INS-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837InsIdFormatted(ws.getDcls02682sa().getIdCdFormatted());
		// COB_CODE: MOVE  LST-ORG-NM OF DCLS02682SA  TO  VSR-837-INS-LST-NM.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837InsLstNm(ws.getDcls02682sa().getLstOrgNm());
		// COB_CODE: MOVE  FRST-NM    OF DCLS02682SA  TO  VSR-837-INS-FRST-NM.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837InsFrstNm(ws.getDcls02682sa().getFrstNm());
		// COB_CODE: MOVE  MID-NM     OF DCLS02682SA  TO  VSR-837-INS-MID-NM.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837InsMidNm(ws.getDcls02682sa().getMidNm());
		// COB_CODE: MOVE  SFX-NM     OF DCLS02682SA  TO  VSR-837-INS-SFX-NM.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837InsSfxNm(ws.getDcls02682sa().getSfxNm());
		// COB_CODE: MOVE FAC-VL-CD OF DCLS02652SA  TO VSR-837-FAC-VL-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837FacVlCd(ws.getDcls02652sa().getFacVlCd());
		// COB_CODE: MOVE CLM-FREQ-TYP-CD OF DCLS02652SA
		//                                          TO VSR-837-CLM-FREQ-TYP-CD.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837ClmFreqTypCd(ws.getDcls02652sa().getClmFreqTypCd());
		// COB_CODE: MOVE CLM-SBMT-ID OF DCLS02652SA
		//                                          TO VSR-837-CLM-SBMT-ID.
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837ClmSbmtId(ws.getDcls02652sa().getClmSbmtId());
		//
		// COB_CODE: PERFORM 2626A-MOVE-XREF-ONLY.
		aMoveXrefOnly();
		// COB_CODE: PERFORM 2626B-MOVE-XREF-ONLY.
		bMoveXrefOnly();
		// COB_CODE: MOVE 1 TO  CASCO-VSUB
		//                      CASPR-VSUB
		//                      CASOA-VSUB
		//                      CASPI-VSUB.
		ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().setCascoVsub(((short) 1));
		ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().setCasprVsub(((short) 1));
		ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().setCasoaVsub(((short) 1));
		ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().setCaspiVsub(((short) 1));
		// COB_CODE: MOVE 'Y' TO WS-FIRST-2626-SW.
		ws.getProgramSwitches().setWsFirst2626SwFormatted("Y");
		// COB_CODE: PERFORM 2610-EVALUATE-VOID-GROUP-CD
		//               UNTIL LAST-ROW-THIS-LINE.
		while (!ws.getProgramSwitches().isLastRowThisLine()) {
			evaluateVoidGroupCd();
		}
		// COB_CODE: MOVE 'N' TO WS-FIRST-2626-SW
		//                       LAST-ROW-THIS-LINE-SW.
		ws.getProgramSwitches().setWsFirst2626SwFormatted("N");
		ws.getProgramSwitches().setLastRowThisLineSwFormatted("N");
		//*** DO NOT WRITE OUT CLAIM LINE CANCELLATIONS
		//
		// COB_CODE: INITIALIZE OUT-VOID-REC
		//                      OUT-VOID-REC-GMIS.
		outputVoidTo.setVariable("");
		outputVoidGmisTo.setVariable("");
		// COB_CODE: IF VSR-EXMN-ACTN-CD = 'C'
		//               CONTINUE
		//           ELSE
		//               END-IF
		//           END-IF.
		if (!(ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getExmnActnCd().getExmnActnCd() == 'C')) {
			// COB_CODE: IF VSR-GMIS-CLAIM
			//               WRITE OUT-VOID-REC-GMIS FROM VSR-PROGRAM-LINE-AREA
			//           ELSE
			//               WRITE OUT-VOID-REC      FROM VSR-PROGRAM-LINE-AREA
			//           END-IF
			if (ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getGmisIndicator().isGmisClaim()) {
				// COB_CODE: code not available
				outputVoidGmisTo.setVariable(ws.getVsrProgramLineAreaFormatted());
				outputVoidGmisDAO.write(outputVoidGmisTo);
			} else {
				// COB_CODE: WRITE OUT-VOID-REC      FROM VSR-PROGRAM-LINE-AREA
				outputVoidTo.setData(ws.getVsrProgramLineAreaBytes());
				outputVoidDAO.write(outputVoidTo);
			}
		}
		// COB_CODE: ADD 1 TO WS-VOID-RECORDS-CNT.
		ws.getProgramCounters().setWsVoidRecordsCnt(Trunc.toInt(1 + ws.getProgramCounters().getWsVoidRecordsCnt(), 6));
		// COB_CODE: IF CHKP-UOW-CNTR > WS-ROW-FREQ-CMT-CT
		//              MOVE 1 TO CHKP-UOW-CNTR
		//           END-IF.
		if (ws.getProgramCounters().getChkpUowCntr() > ws.getDataSavedFromTable3086().getWsRowFreqCmtCt()) {
			// COB_CODE: PERFORM 7000-CK-FOR-COMMIT
			ckForCommit();
			// COB_CODE: MOVE 1 TO CHKP-UOW-CNTR
			ws.getProgramCounters().setChkpUowCntr(1);
		}
		// COB_CODE: IF STATS-CNT > DISPLAY-TIME-INTERVAL
		//                   ', ' COMMIT-CNT ' TOTAL COMMITS TAKEN'
		//           END-IF.
		if (ws.getProgramCounters().getStatsCnt() > ws.getProgramCounters().getDisplayTimeInterval()) {
			// COB_CODE: MOVE ZEROES TO STATS-CNT
			ws.getProgramCounters().setStatsCnt(0);
			// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN
			ws.getWsDateFields().getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
			// COB_CODE: MOVE GREGORN-HH   TO FORMAT-HH
			ws.getFormattedTime().setFormatHhFormatted(ws.getWsDateFields().getGregorn().getHhFormatted());
			// COB_CODE: MOVE GREGORN-MI   TO FORMAT-MM
			ws.getFormattedTime().setFormatMmFormatted(ws.getWsDateFields().getGregorn().getMiFormatted());
			// COB_CODE: MOVE GREGORN-SS   TO FORMAT-SS
			ws.getFormattedTime().setFormatSsFormatted(ws.getWsDateFields().getGregorn().getSsFormatted());
			// COB_CODE: DISPLAY VOID-PMTS-CNTR
			//               ' CLAIMS PROCESSED AS OF ' FORMATTED-TIME
			//               ', ' COMMIT-CNT ' TOTAL COMMITS TAKEN'
			DisplayUtil.sysout.write(new String[] { ws.getProgramCounters().getVoidPmtsCntrAsString(), " CLAIMS PROCESSED AS OF ",
					ws.getFormattedTime().getFormattedTimeFormatted(), ", ", ws.getProgramCounters().getCommitCntAsString(),
					" TOTAL COMMITS TAKEN" });
		}
	}

	/**Original name: 2610-EVALUATE-VOID-GROUP-CD<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '   *2610-EVALUATE-VOID-GROUP-CD*'.</pre>*/
	private void evaluateVoidGroupCd() {
		// COB_CODE: MOVE CLM-ADJ-GRP-CD OF DCLS02800SA TO WS-SAVE-GRP.
		ws.getProgramSwitches().getWsSavedCas().setSaveGrp(ws.getDcls02800sa().getClmAdjGrpCd());
		// COB_CODE: MOVE CLM-ADJ-RSN-CD OF DCLS02800SA TO WS-SAVE-ARC.
		ws.getProgramSwitches().getWsSavedCas().setSaveArc(ws.getDcls02800sa().getClmAdjRsnCd());
		// COB_CODE: MOVE MNTRY-AM       OF DCLS02800SA TO WS-SAVE-AMT.
		ws.getProgramSwitches().getWsSavedCas().setSaveAmt(Trunc.toDecimal(ws.getDcls02800sa().getMntryAm(), 11, 2));
		// COB_CODE: MOVE QNTY-CT        OF DCLS02800SA TO WS-SAVE-QTY.
		ws.getProgramSwitches().getWsSavedCas().setSaveQty(Trunc.toDecimal(ws.getDcls02800sa().getQntyCt(), 7, 2));
		// COB_CODE: IF BYPASS-2612-NO
		//                PERFORM 2612-MOVE-VOID-CAS-INFO
		//           ELSE
		//                MOVE 'N' TO BYPASS-2612-SW
		//           END-IF.
		if (ws.getProgramSwitches().isBypass2612No()) {
			// COB_CODE: PERFORM 2612-MOVE-VOID-CAS-INFO
			moveVoidCasInfo();
		} else {
			// COB_CODE: MOVE 'N' TO BYPASS-2612-SW
			ws.getProgramSwitches().setBypass2612SwFormatted("N");
		}
		// COB_CODE: PERFORM 2156-FETCH-VOID-LOOP UNTIL FETCH-VOID-DONE.
		while (!ws.getProgramSwitches().isFetchVoidDone()) {
			fetchVoidLoop();
		}
		// COB_CODE: MOVE 0 TO FETCH-VOID-DONE-SW.
		ws.getProgramSwitches().setFetchVoidDoneSw(((short) 0));
		// COB_CODE: IF VOID-EOF
		//               ADD 1 TO VOID-PMTS-CNTR
		//           ELSE
		//               END-IF
		//           END-IF.
		if (ws.getProgramSwitches().isVoidEof()) {
			// COB_CODE: MOVE 'Y' TO LAST-ROW-THIS-LINE-SW
			ws.getProgramSwitches().setLastRowThisLineSwFormatted("Y");
			// COB_CODE: ADD 1 TO CHKP-UOW-CNTR
			ws.getProgramCounters().setChkpUowCntr(Trunc.toInt(1 + ws.getProgramCounters().getChkpUowCntr(), 6));
			// COB_CODE: ADD 1 TO STATS-CNT
			ws.getProgramCounters().setStatsCnt(Trunc.toInt(1 + ws.getProgramCounters().getStatsCnt(), 6));
			// COB_CODE: ADD 1 TO VOID-PMTS-CNTR
			ws.getProgramCounters().setVoidPmtsCntr(Trunc.toInt(1 + ws.getProgramCounters().getVoidPmtsCntr(), 6));
		} else if (Conditions.eq(ws.getDcls02813sa().getClmCntlId(), ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmCntlIdBytes())
				&& ws.getDcls02813sa().getClmCntlSfxId() == ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClmCntlSfxId()
				&& ws.getDcls02813sa().getClmPmtId() == ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPmtId()
				&& ws.getDcls02809sa().getCliId() == ws.getCalcFields().getCalcRecordKey().getLiIdPacked()) {
			// COB_CODE: IF  CLM-CNTL-ID OF DCLS02813SA     = VSR-CLM-CNTL-ID
			//           AND CLM-CNTL-SFX-ID OF DCLS02813SA = VSR-CLM-CNTL-SFX-ID
			//           AND CLM-PMT-ID OF DCLS02813SA      = VSR-PMT-ID
			//           AND CLI-ID OF DCLS02809SA =  WS-CALC-CLM-LI-ID-PACKED
			//              END-IF
			//           ELSE
			//               ADD 1 TO VOID-PMTS-CNTR
			//           END-IF
			// COB_CODE:  IF CLM-ADJ-GRP-CD OF DCLS02800SA = WS-SAVE-GRP
			//           AND CLM-ADJ-RSN-CD OF DCLS02800SA = WS-SAVE-ARC
			//           AND MNTRY-AM       OF DCLS02800SA = WS-SAVE-AMT
			//           AND QNTY-CT        OF DCLS02800SA = WS-SAVE-QTY
			//                MOVE 'Y' TO BYPASS-2612-SW
			//           ELSE
			//                CONTINUE
			//           END-IF
			if (Conditions.eq(ws.getDcls02800sa().getClmAdjGrpCd(), ws.getProgramSwitches().getWsSavedCas().getSaveGrp())
					&& Conditions.eq(ws.getDcls02800sa().getClmAdjRsnCd(), ws.getProgramSwitches().getWsSavedCas().getSaveArc())
					&& ws.getDcls02800sa().getMntryAm().compareTo(ws.getProgramSwitches().getWsSavedCas().getSaveAmt()) == 0
					&& ws.getDcls02800sa().getQntyCt().compareTo(ws.getProgramSwitches().getWsSavedCas().getSaveQty()) == 0) {
				// COB_CODE: IF CRS-REF-RSN-CD OF DCLS04315SA > SPACES
				//            PERFORM 2626A-MOVE-XREF-ONLY
				//           ELSE
				//            PERFORM 2626B-MOVE-XREF-ONLY
				//           END-IF
				if (Conditions.gt(ws.getDcls04315sa().getCrsRefRsnCd(), Types.SPACE_CHAR)) {
					// COB_CODE: PERFORM 2626A-MOVE-XREF-ONLY
					aMoveXrefOnly();
				} else {
					// COB_CODE: PERFORM 2626B-MOVE-XREF-ONLY
					bMoveXrefOnly();
				}
				// COB_CODE: MOVE 'Y' TO BYPASS-2612-SW
				ws.getProgramSwitches().setBypass2612SwFormatted("Y");
			}
		} else {
			// COB_CODE: MOVE 'Y' TO LAST-ROW-THIS-LINE-SW
			ws.getProgramSwitches().setLastRowThisLineSwFormatted("Y");
			// COB_CODE: ADD 1 TO CHKP-UOW-CNTR
			ws.getProgramCounters().setChkpUowCntr(Trunc.toInt(1 + ws.getProgramCounters().getChkpUowCntr(), 6));
			// COB_CODE: ADD 1 TO STATS-CNT
			ws.getProgramCounters().setStatsCnt(Trunc.toInt(1 + ws.getProgramCounters().getStatsCnt(), 6));
			// COB_CODE: ADD 1 TO VOID-PMTS-CNTR
			ws.getProgramCounters().setVoidPmtsCntr(Trunc.toInt(1 + ws.getProgramCounters().getVoidPmtsCntr(), 6));
		}
	}

	/**Original name: 2612-MOVE-VOID-CAS-INFO<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '   *2612-MOVE-VOID-CAS-INFO*'.</pre>*/
	private void moveVoidCasInfo() {
		// COB_CODE: EVALUATE CLM-ADJ-GRP-CD OF DCLS02800SA
		//              WHEN 'CO'
		//                 ADD 1 TO CASCO-VSUB
		//              WHEN 'PR'
		//                 ADD 1 TO CASPR-VSUB
		//              WHEN 'OA'
		//                 ADD 1 TO CASOA-VSUB
		//              WHEN 'PI'
		//                 ADD 1 TO CASPI-VSUB
		//           END-EVALUATE.
		switch (ws.getDcls02800sa().getClmAdjGrpCd()) {

		case "CO":// COB_CODE: IF CASCO-VSUB > CAS-MAX-CO-18
			//               CALL 'ABEND'
			//           END-IF
			if (ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCascoVsub() > ws.getProgramSubscripts().getCasSubscripts()
					.getCasMaxCo18()) {
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: DISPLAY '*****   NF0735ML ABENDING   *****'
				DisplayUtil.sysout.write("*****   NF0735ML ABENDING   *****");
				// COB_CODE: DISPLAY 'CASCO-VSUB EXCEEDS MAX ALLOWED'
				DisplayUtil.sysout.write("CASCO-VSUB EXCEEDS MAX ALLOWED");
				// COB_CODE: DISPLAY 'MORE THAN ' CAS-MAX-CO-18
				//                   ' CO CAS SEGS FOUND'
				DisplayUtil.sysout.write("MORE THAN ", ws.getProgramSubscripts().getCasSubscripts().getCasMaxCo18AsString(), " CO CAS SEGS FOUND");
				// COB_CODE: CALL 'ABEND'
				DynamicCall.invoke("ABEND");
			}
			// COB_CODE: MOVE CLM-ADJ-GRP-CD OF DCLS02800SA TO
			//                         VSR-CASCO-CLM-ADJ-GRP-CD
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().setCascoClmAdjGrpCd(ws.getDcls02800sa().getClmAdjGrpCd());
			// COB_CODE: MOVE CLM-ADJ-RSN-CD OF DCLS02800SA TO
			//                         VSR-CASCO-CLM-ADJ-RSN-CD (CASCO-VSUB)
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo()
					.getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCascoVsub())
					.setClmAdjRsnCd(ws.getDcls02800sa().getClmAdjRsnCd());
			// COB_CODE: MOVE MNTRY-AM OF DCLS02800SA TO
			//                         VSR-CASCO-MNTRY-AM (CASCO-VSUB)
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo()
					.getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCascoVsub())
					.setMntryAm(Trunc.toDecimal(ws.getDcls02800sa().getMntryAm(), 11, 2));
			// COB_CODE: MOVE QNTY-CT  OF DCLS02800SA TO
			//                         VSR-CASCO-QNTY-CT  (CASCO-VSUB)
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo()
					.getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCascoVsub())
					.setQntyCt(Trunc.toDecimal(ws.getDcls02800sa().getQntyCt(), 7, 2));
			// COB_CODE: ADD 1 TO CASCO-VSUB
			ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs()
					.setCascoVsub(Trunc.toShort(1 + ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCascoVsub(), 4));
			break;

		case "PR":// COB_CODE: IF CASPR-VSUB > CAS-MAX-PR-24
			//               CALL 'ABEND'
			//           END-IF
			if (ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCasprVsub() > ws.getProgramSubscripts().getCasSubscripts()
					.getCasMaxPr24()) {
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: DISPLAY '*****   NF0735ML ABENDING   *****'
				DisplayUtil.sysout.write("*****   NF0735ML ABENDING   *****");
				// COB_CODE: DISPLAY 'CASPR-VSUB EXCEEDS MAX ALLOWED'
				DisplayUtil.sysout.write("CASPR-VSUB EXCEEDS MAX ALLOWED");
				// COB_CODE: DISPLAY 'MORE THAN ' CAS-MAX-PR-24
				//                   ' PR CAS SEGS FOUND'
				DisplayUtil.sysout.write("MORE THAN ", ws.getProgramSubscripts().getCasSubscripts().getCasMaxPr24AsString(), " PR CAS SEGS FOUND");
				// COB_CODE: DISPLAY 'CLAIM CNTL ID ' CLM-CNTL-ID OF DCLS02813SA
				//                               CLM-CNTL-SFX-ID OF DCLS02813SA
				//                   '    PMT = ' CLM-PMT-ID OF DCLS02813SA
				//                   '    LINE = ' CLI-ID OF DCLS02809SA
				DisplayUtil.sysout.write(new String[] { "CLAIM CNTL ID ", ws.getDcls02813sa().getClmCntlIdFormatted(),
						String.valueOf(ws.getDcls02813sa().getClmCntlSfxId()), "    PMT = ", ws.getDcls02813sa().getClmPmtIdAsString(), "    LINE = ",
						ws.getDcls02809sa().getCliIdAsString() });
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: CALL 'ABEND'
				DynamicCall.invoke("ABEND");
			}
			// COB_CODE: MOVE CLM-ADJ-GRP-CD OF DCLS02800SA TO
			//                         VSR-CASPR-CLM-ADJ-GRP-CD
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().setCasprClmAdjGrpCd(ws.getDcls02800sa().getClmAdjGrpCd());
			// COB_CODE: MOVE CLM-ADJ-RSN-CD OF DCLS02800SA TO
			//                         VSR-CASPR-CLM-ADJ-RSN-CD (CASPR-VSUB)
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo()
					.getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCasprVsub())
					.setClmAdjRsnCd(ws.getDcls02800sa().getClmAdjRsnCd());
			// COB_CODE: MOVE MNTRY-AM OF DCLS02800SA TO
			//                         VSR-CASPR-MNTRY-AM (CASPR-VSUB)
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo()
					.getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCasprVsub())
					.setMntryAm(Trunc.toDecimal(ws.getDcls02800sa().getMntryAm(), 11, 2));
			// COB_CODE: MOVE QNTY-CT  OF DCLS02800SA TO
			//                         VSR-CASPR-QNTY-CT  (CASPR-VSUB)
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo()
					.getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCasprVsub())
					.setQntyCt(Trunc.toDecimal(ws.getDcls02800sa().getQntyCt(), 7, 2));
			// COB_CODE: ADD 1 TO CASPR-VSUB
			ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs()
					.setCasprVsub(Trunc.toShort(1 + ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCasprVsub(), 4));
			break;

		case "OA":// COB_CODE: IF CASOA-VSUB > CAS-MAX-OA-18
			//               CALL 'ABEND'
			//           END-IF
			if (ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCasoaVsub() > ws.getProgramSubscripts().getCasSubscripts()
					.getCasMaxOa18()) {
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: DISPLAY '*****   NF0735ML ABENDING   *****'
				DisplayUtil.sysout.write("*****   NF0735ML ABENDING   *****");
				// COB_CODE: DISPLAY 'CASOA-VSUB EXCEEDS MAX ALLOWED'
				DisplayUtil.sysout.write("CASOA-VSUB EXCEEDS MAX ALLOWED");
				// COB_CODE: DISPLAY 'MORE THAN ' CAS-MAX-OA-18
				//                   ' OA CAS SEGS FOUND'
				DisplayUtil.sysout.write("MORE THAN ", ws.getProgramSubscripts().getCasSubscripts().getCasMaxOa18AsString(), " OA CAS SEGS FOUND");
				// COB_CODE: DISPLAY 'CLAIM CNTL ID ' CLM-CNTL-ID OF DCLS02813SA
				//                                CLM-CNTL-SFX-ID OF DCLS02813SA
				//                   '    PMT = ' CLM-PMT-ID OF DCLS02813SA
				//                   '   LINE = ' CLI-ID OF DCLS02809SA
				DisplayUtil.sysout.write(new String[] { "CLAIM CNTL ID ", ws.getDcls02813sa().getClmCntlIdFormatted(),
						String.valueOf(ws.getDcls02813sa().getClmCntlSfxId()), "    PMT = ", ws.getDcls02813sa().getClmPmtIdAsString(), "   LINE = ",
						ws.getDcls02809sa().getCliIdAsString() });
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: CALL 'ABEND'
				DynamicCall.invoke("ABEND");
			}
			// COB_CODE: MOVE CLM-ADJ-GRP-CD OF DCLS02800SA TO
			//                         VSR-CASOA-CLM-ADJ-GRP-CD
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().setCasoaClmAdjGrpCd(ws.getDcls02800sa().getClmAdjGrpCd());
			// COB_CODE: MOVE CLM-ADJ-RSN-CD OF DCLS02800SA TO
			//                         VSR-CASOA-CLM-ADJ-RSN-CD (CASOA-VSUB)
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo()
					.getCasoaReductions(ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCasoaVsub())
					.setClmAdjRsnCd(ws.getDcls02800sa().getClmAdjRsnCd());
			// COB_CODE: MOVE MNTRY-AM OF DCLS02800SA TO
			//                         VSR-CASOA-MNTRY-AM (CASOA-VSUB)
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo()
					.getCasoaReductions(ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCasoaVsub())
					.setMntryAm(Trunc.toDecimal(ws.getDcls02800sa().getMntryAm(), 11, 2));
			// COB_CODE: MOVE QNTY-CT  OF DCLS02800SA TO
			//                         VSR-CASOA-QNTY-CT  (CASOA-VSUB)
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo()
					.getCasoaReductions(ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCasoaVsub())
					.setQntyCt(Trunc.toDecimal(ws.getDcls02800sa().getQntyCt(), 7, 2));
			// COB_CODE: ADD 1 TO CASOA-VSUB
			ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs()
					.setCasoaVsub(Trunc.toShort(1 + ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCasoaVsub(), 4));
			break;

		case "PI":// COB_CODE: IF CASPI-VSUB > CAS-MAX-PI-6
			//               CALL 'ABEND'
			//           END-IF
			if (ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCaspiVsub() > ws.getProgramSubscripts().getCasSubscripts()
					.getCasMaxPi6()) {
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: DISPLAY '*****   NF0735ML ABENDING   *****'
				DisplayUtil.sysout.write("*****   NF0735ML ABENDING   *****");
				// COB_CODE: DISPLAY 'CASPI-VSUB EXCEEDS MAX ALLOWED'
				DisplayUtil.sysout.write("CASPI-VSUB EXCEEDS MAX ALLOWED");
				// COB_CODE: DISPLAY 'MORE THAN ' CAS-MAX-PI-6
				//                   ' PI CAS SEGS FOUND'
				DisplayUtil.sysout.write("MORE THAN ", ws.getProgramSubscripts().getCasSubscripts().getCasMaxPi6AsString(), " PI CAS SEGS FOUND");
				// COB_CODE: DISPLAY 'CLAIM CNTL ID ' CLM-CNTL-ID OF DCLS02813SA
				//                                CLM-CNTL-SFX-ID OF DCLS02813SA
				//                   '    PMT = ' CLM-PMT-ID OF DCLS02813SA
				//                   '   LINE = ' CLI-ID OF DCLS02809SA
				DisplayUtil.sysout.write(new String[] { "CLAIM CNTL ID ", ws.getDcls02813sa().getClmCntlIdFormatted(),
						String.valueOf(ws.getDcls02813sa().getClmCntlSfxId()), "    PMT = ", ws.getDcls02813sa().getClmPmtIdAsString(), "   LINE = ",
						ws.getDcls02809sa().getCliIdAsString() });
				// COB_CODE: DISPLAY ' '
				DisplayUtil.sysout.write(" ");
				// COB_CODE: CALL 'ABEND'
				DynamicCall.invoke("ABEND");
			}
			// COB_CODE: MOVE CLM-ADJ-GRP-CD OF DCLS02800SA TO
			//                         VSR-CASPI-CLM-ADJ-GRP-CD
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().setCaspiClmAdjGrpCd(ws.getDcls02800sa().getClmAdjGrpCd());
			// COB_CODE: MOVE CLM-ADJ-RSN-CD OF DCLS02800SA TO
			//                         VSR-CASPI-CLM-ADJ-RSN-CD (CASPI-VSUB)
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo()
					.getCaspiReductions(ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCaspiVsub())
					.setClmAdjRsnCd(ws.getDcls02800sa().getClmAdjRsnCd());
			// COB_CODE: MOVE MNTRY-AM OF DCLS02800SA TO
			//                         VSR-CASPI-MNTRY-AM (CASPI-VSUB)
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo()
					.getCaspiReductions(ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCaspiVsub())
					.setMntryAm(Trunc.toDecimal(ws.getDcls02800sa().getMntryAm(), 11, 2));
			// COB_CODE: MOVE QNTY-CT  OF DCLS02800SA TO
			//                         VSR-CASPI-QNTY-CT  (CASPI-VSUB)
			ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo()
					.getCaspiReductions(ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCaspiVsub())
					.setQntyCt(Trunc.toDecimal(ws.getDcls02800sa().getQntyCt(), 7, 2));
			// COB_CODE: ADD 1 TO CASPI-VSUB
			ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs()
					.setCaspiVsub(Trunc.toShort(1 + ws.getProgramSubscripts().getCasSubscripts().getVoidCasSubs().getCaspiVsub(), 4));
			break;

		default:
			break;
		}
		// COB_CODE: IF CLM-ADJ-RSN-CD OF DCLS02800SA = '16' OR '17' OR '96'
		//                                            OR '125'
		//                MOVE 'X' TO VSR-ARC-IND
		//           END-IF.
		if (Conditions.eq(ws.getDcls02800sa().getClmAdjRsnCd(), "16") || Conditions.eq(ws.getDcls02800sa().getClmAdjRsnCd(), "17")
				|| Conditions.eq(ws.getDcls02800sa().getClmAdjRsnCd(), "96") || Conditions.eq(ws.getDcls02800sa().getClmAdjRsnCd(), "125")) {
			// COB_CODE: MOVE 'X' TO VSR-ARC-IND
			ws.getNf07areaofVsrProgramLineArea().setArcIndFormatted("X");
		}
	}

	/**Original name: 2613-INITIALIZE-VSR-CAS<br>
	 * <pre>---------------------------------------------------------------*
	 *     DISPLAY ' *2613-INITIALIZE-VSR-CAS*'.</pre>*/
	private void initializeVsrCas() {
		// COB_CODE: PERFORM 2613A-INITIALIZE-VSR-CO-CAS
		//               VARYING CAS-SUB FROM 1 BY 1
		//               UNTIL CAS-SUB > CAS-MAX-CO-18.
		ws.getProgramSubscripts().getCasSubscripts().setCasSub(((short) 1));
		while (!(ws.getProgramSubscripts().getCasSubscripts().getCasSub() > ws.getProgramSubscripts().getCasSubscripts().getCasMaxCo18())) {
			aInitializeVsrCoCas();
			ws.getProgramSubscripts().getCasSubscripts().setCasSub(Trunc.toShort(ws.getProgramSubscripts().getCasSubscripts().getCasSub() + 1, 4));
		}
		// COB_CODE: PERFORM 2613B-INITIALIZE-VSR-PR-CAS
		//               VARYING CAS-SUB FROM 1 BY 1
		//               UNTIL CAS-SUB > CAS-MAX-PR-24.
		ws.getProgramSubscripts().getCasSubscripts().setCasSub(((short) 1));
		while (!(ws.getProgramSubscripts().getCasSubscripts().getCasSub() > ws.getProgramSubscripts().getCasSubscripts().getCasMaxPr24())) {
			bInitializeVsrPrCas();
			ws.getProgramSubscripts().getCasSubscripts().setCasSub(Trunc.toShort(ws.getProgramSubscripts().getCasSubscripts().getCasSub() + 1, 4));
		}
		// COB_CODE: PERFORM 2613C-INITIALIZE-VSR-OA-CAS
		//               VARYING CAS-SUB FROM 1 BY 1
		//               UNTIL CAS-SUB > CAS-MAX-OA-18.
		ws.getProgramSubscripts().getCasSubscripts().setCasSub(((short) 1));
		while (!(ws.getProgramSubscripts().getCasSubscripts().getCasSub() > ws.getProgramSubscripts().getCasSubscripts().getCasMaxOa18())) {
			cInitializeVsrOaCas();
			ws.getProgramSubscripts().getCasSubscripts().setCasSub(Trunc.toShort(ws.getProgramSubscripts().getCasSubscripts().getCasSub() + 1, 4));
		}
		// COB_CODE: PERFORM 2613D-INITIALIZE-VSR-PI-CAS
		//               VARYING CAS-SUB FROM 1 BY 1
		//               UNTIL CAS-SUB > CAS-MAX-PI-6.
		ws.getProgramSubscripts().getCasSubscripts().setCasSub(((short) 1));
		while (!(ws.getProgramSubscripts().getCasSubscripts().getCasSub() > ws.getProgramSubscripts().getCasSubscripts().getCasMaxPi6())) {
			dInitializeVsrPiCas();
			ws.getProgramSubscripts().getCasSubscripts().setCasSub(Trunc.toShort(ws.getProgramSubscripts().getCasSubscripts().getCasSub() + 1, 4));
		}
	}

	/**Original name: 2613A-INITIALIZE-VSR-CO-CAS<br>
	 * <pre>---------------------------------------------------------------*
	 *     DISPLAY ' *2613A-INITIALIZE-VSR-CO-CAS'.</pre>*/
	private void aInitializeVsrCoCas() {
		// COB_CODE: MOVE SPACES TO  VSR-CASCO-CLM-ADJ-RSN-CD (CAS-SUB).
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setClmAdjRsnCd("");
		// COB_CODE: MOVE ZEROS  TO  VSR-CASCO-MNTRY-AM (CAS-SUB).
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setMntryAm(new AfDecimal(0, 11, 2));
		// COB_CODE: MOVE ZEROS  TO  VSR-CASCO-QNTY-CT  (CAS-SUB).
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().getCascoReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setQntyCt(new AfDecimal(0, 7, 2));
	}

	/**Original name: 2613B-INITIALIZE-VSR-PR-CAS<br>
	 * <pre>---------------------------------------------------------------*
	 *     DISPLAY ' *2613B-INITIALIZE-VSR-PR-CAS'.</pre>*/
	private void bInitializeVsrPrCas() {
		// COB_CODE: MOVE SPACES TO  VSR-CASPR-CLM-ADJ-RSN-CD (CAS-SUB).
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setClmAdjRsnCd("");
		// COB_CODE: MOVE ZEROS  TO  VSR-CASPR-MNTRY-AM (CAS-SUB).
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setMntryAm(new AfDecimal(0, 11, 2));
		// COB_CODE: MOVE ZEROS  TO  VSR-CASPR-QNTY-CT  (CAS-SUB).
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().getCasprReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setQntyCt(new AfDecimal(0, 7, 2));
	}

	/**Original name: 2613C-INITIALIZE-VSR-OA-CAS<br>
	 * <pre>---------------------------------------------------------------*
	 *     DISPLAY ' *2613C-INITIALIZE-VSR-OA-CAS'.</pre>*/
	private void cInitializeVsrOaCas() {
		// COB_CODE: MOVE SPACES TO  VSR-CASOA-CLM-ADJ-RSN-CD (CAS-SUB).
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().getCasoaReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setClmAdjRsnCd("");
		// COB_CODE: MOVE ZEROS  TO  VSR-CASOA-MNTRY-AM (CAS-SUB).
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().getCasoaReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setMntryAm(new AfDecimal(0, 11, 2));
		// COB_CODE: MOVE ZEROS  TO  VSR-CASOA-QNTY-CT  (CAS-SUB).
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().getCasoaReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setQntyCt(new AfDecimal(0, 7, 2));
	}

	/**Original name: 2613D-INITIALIZE-VSR-PI-CAS<br>
	 * <pre>---------------------------------------------------------------*
	 *     DISPLAY ' *2613D-INITIALIZE-VSR-PI-CAS'.</pre>*/
	private void dInitializeVsrPiCas() {
		// COB_CODE: MOVE SPACES TO  VSR-CASPI-CLM-ADJ-RSN-CD (CAS-SUB).
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().getCaspiReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setClmAdjRsnCd("");
		// COB_CODE: MOVE ZEROS  TO  VSR-CASPI-MNTRY-AM (CAS-SUB).
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().getCaspiReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setMntryAm(new AfDecimal(0, 11, 2));
		// COB_CODE: MOVE ZEROS  TO  VSR-CASPI-QNTY-CT  (CAS-SUB).
		ws.getNf07areaofVsrProgramLineArea().getPmtCasInfo().getCaspiReductions(ws.getProgramSubscripts().getCasSubscripts().getCasSub())
				.setQntyCt(new AfDecimal(0, 7, 2));
	}

	/**Original name: 2615-INITIALIZE-XREF<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '  *2615-INITIALIZE-XREF* '.
	 * * ------------------------------------------------------------ **</pre>*/
	private void initializeXref1() {
		// COB_CODE: MOVE ZEROS  TO  VSR-XREF-PMT-ID      (X-SUB)
		//                           VSR-XREF-CLI-ID      (X-SUB).
		ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setPmtId(((short) 0));
		ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setCliId(((short) 0));
		// COB_CODE: MOVE SPACES TO  VSR-XREF-CLM-ID      (X-SUB)
		//                           VSR-XREF-SX-ID       (X-SUB)
		//                           VSR-XREF-RSN-CD      (X-SUB)
		//                           VSR-XREF-DNL-RMRK-CD (X-SUB)
		//                           VSR-ADJD-PROC-CD     (X-SUB)
		//                           VSR-ADJD-MOD1-CD     (X-SUB)
		//                           VSR-ADJD-MOD2-CD     (X-SUB)
		//                           VSR-ADJD-MOD3-CD     (X-SUB)
		//                           VSR-ADJD-MOD4-CD     (X-SUB).
		ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setClmId("");
		ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setSxId(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setRsnCd("");
		ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setDnlRmrkCd("");
		ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes().setCd("");
		ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes().setMod1Cd("");
		ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes().setMod2Cd("");
		ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes().setMod3Cd("");
		ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes().setMod4Cd("");
	}

	/**Original name: 2626A-MOVE-XREF-ONLY<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY '   *2626A-MOVE-XREF-ONLY* '.
	 * * ------------------------------------------------------------ **
	 * * THIS WILL BE EXECUTED FOR EACH ROW RETRIEVED FROM THE CURSOR **
	 * * IF THE XREF RSN IS POPULATED THEN IT WILL BE MOVED TO THE    **
	 * * CORRESPONDING OCCURENCE IN THE FILE.  C298.                  **
	 * * ------------------------------------------------------------ **</pre>*/
	private void aMoveXrefOnly() {
		// COB_CODE: EVALUATE CRS-REF-RSN-CD OF DCLS04315SA
		//                WHEN 'U'
		//                     MOVE 1 TO X-SUB
		//                WHEN 'B'
		//                     MOVE 2 TO X-SUB
		//                WHEN 'R'
		//                     MOVE 3 TO X-SUB
		//                WHEN 'I'
		//                     MOVE 4 TO X-SUB
		//                WHEN 'L'
		//                     MOVE 5 TO X-SUB
		//                WHEN 'M'
		//                     MOVE 6 TO X-SUB
		//                WHEN 'D'
		//                WHEN 'F'
		//                WHEN 'P'
		//                WHEN 'S'
		//                WHEN 'V'
		//                     MOVE 0 TO X-SUB
		//                WHEN OTHER
		//                     MOVE 0 TO X-SUB
		//           END-EVALUATE.
		switch (ws.getDcls04315sa().getCrsRefRsnCd()) {

		case 'U':// COB_CODE: MOVE 1 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 1));
			break;

		case 'B':// COB_CODE: MOVE 2 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 2));
			break;

		case 'R':// COB_CODE: MOVE 3 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 3));
			break;

		case 'I':// COB_CODE: MOVE 4 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 4));
			break;

		case 'L':// COB_CODE: MOVE 5 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 5));
			break;

		case 'M':// COB_CODE: MOVE 6 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 6));
			break;

		case 'D':
		case 'F':
		case 'P':
		case 'S':
		case 'V':// COB_CODE: MOVE 0 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 0));
			break;

		default:// COB_CODE: MOVE 0 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 0));
			break;
		}
		// COB_CODE: IF ACT-INACT-CD OF DCLS04315SA = 'A'
		//                TO VSR-INCD-ADJD-MOD4-CD
		//              END-IF.
		if (ws.getDcls04315sa().getActInactCd() == 'A') {
			// COB_CODE: MOVE CRS-REF-RSN-CD OF DCLS04315SA
			//             TO VSR-INCD-RSN-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefIncd().getRsnCd().setRsnCd(String.valueOf(ws.getDcls04315sa().getCrsRefRsnCd()));
			// COB_CODE: MOVE ADJD-PROC-CD OF DCLS04315SA
			//             TO VSR-INCD-ADJD-PROC-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdProcCd(ws.getDcls04315sa().getAdjdProcCd());
			// COB_CODE: MOVE ADJD-PROC-MOD1-CD OF DCLS04315SA
			//             TO VSR-INCD-ADJD-MOD1-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdMod1Cd(ws.getDcls04315sa().getAdjdProcMod1Cd());
			// COB_CODE: MOVE ADJD-PROC-MOD2-CD OF DCLS04315SA
			//             TO VSR-INCD-ADJD-MOD2-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdMod2Cd(ws.getDcls04315sa().getAdjdProcMod2Cd());
			// COB_CODE: MOVE ADJD-PROC-MOD3-CD OF DCLS04315SA
			//             TO VSR-INCD-ADJD-MOD3-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdMod3Cd(ws.getDcls04315sa().getAdjdProcMod3Cd());
			// COB_CODE: MOVE ADJD-PROC-MOD4-CD OF DCLS04315SA
			//             TO VSR-INCD-ADJD-MOD4-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdMod4Cd(ws.getDcls04315sa().getAdjdProcMod4Cd());
		}
		// COB_CODE: IF X-SUB > 0
		//           AND ACT-INACT-CD OF DCLS04315SA = 'A'
		//                              TO VSR-ADJD-MOD4-CD (X-SUB)
		//           END-IF.
		if (ws.getProgramSubscripts().getxSub() > 0 && ws.getDcls04315sa().getActInactCd() == 'A') {
			// COB_CODE: MOVE CRS-RF-CLM-CNTL-ID OF DCLS04315SA
			//                                     TO VSR-XREF-CLM-ID (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setClmId(ws.getDcls04315sa().getCrsRfClmCntlId());
			// COB_CODE: MOVE CS-RF-CLM-CL-SX-ID OF DCLS04315SA
			//                                TO VSR-XREF-SX-ID (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setSxId(ws.getDcls04315sa().getCsRfClmClSxId());
			// COB_CODE: MOVE CRS-REF-CLM-PMT-ID OF DCLS04315SA
			//                                TO VSR-XREF-PMT-ID (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setPmtId(TruncAbs.toShort(ws.getDcls04315sa().getCrsRefClmPmtId(), 2));
			// COB_CODE: MOVE CRS-REF-CLI-ID OF DCLS04315SA
			//                                TO VSR-XREF-CLI-ID (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setCliId(TruncAbs.toShort(ws.getDcls04315sa().getCrsRefCliId(), 3));
			// COB_CODE: MOVE CRS-REF-RSN-CD OF DCLS04315SA
			//                                TO VSR-XREF-RSN-CD (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setRsnCd(String.valueOf(ws.getDcls04315sa().getCrsRefRsnCd()));
			// COB_CODE: MOVE DNL-RMRK-CD OF DCLS04315SA
			//                                TO VSR-XREF-DNL-RMRK-CD (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).setDnlRmrkCd(ws.getDcls04315sa().getDnlRmrkCd());
			// COB_CODE: MOVE ADJD-PROC-CD OF DCLS04315SA
			//                         TO VSR-ADJD-PROC-CD (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setCd(ws.getDcls04315sa().getAdjdProcCd());
			//                          VSR-PROC-CD
			// COB_CODE: MOVE ADJD-PROC-MOD1-CD OF DCLS04315SA
			//                         TO VSR-ADJD-MOD1-CD (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod1Cd(ws.getDcls04315sa().getAdjdProcMod1Cd());
			// COB_CODE: MOVE ADJD-PROC-MOD2-CD OF DCLS04315SA
			//                         TO VSR-ADJD-MOD2-CD (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod2Cd(ws.getDcls04315sa().getAdjdProcMod2Cd());
			// COB_CODE: MOVE ADJD-PROC-MOD3-CD OF DCLS04315SA
			//                         TO VSR-ADJD-MOD3-CD (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod3Cd(ws.getDcls04315sa().getAdjdProcMod3Cd());
			// COB_CODE: MOVE ADJD-PROC-MOD4-CD OF DCLS04315SA
			//                         TO VSR-ADJD-MOD4-CD (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod4Cd(ws.getDcls04315sa().getAdjdProcMod4Cd());
		}
	}

	/**Original name: 2626B-MOVE-XREF-ONLY<br>
	 * <pre>    DISPLAY '   *2626B-MOVE-PTRS-ONLY* '.
	 * * ------------------------------------------------------------ **
	 * * WHEN THERE ARE MORE THAN ONE POINTER FOR A LINE THIS MODULE  **
	 * * IS EXECUTED.  IT WILL POPULATE THE 2ND & 3RD OCCURRENCE OF   **
	 * * THE XREF FIELDS.   C298.                                     **
	 * * ------------------------------------------------------------ **</pre>*/
	private void bMoveXrefOnly() {
		// COB_CODE: EVALUATE WS-VR4-CRS-REF-RSN-CD
		//                WHEN 'U'
		//                     MOVE 1 TO X-SUB
		//                WHEN 'B'
		//                     MOVE 2 TO X-SUB
		//                WHEN 'R'
		//                     MOVE 3 TO X-SUB
		//                WHEN 'I'
		//                     MOVE 4 TO X-SUB
		//                WHEN 'L'
		//                     MOVE 5 TO X-SUB
		//                WHEN 'M'
		//                     MOVE 6 TO X-SUB
		//                WHEN 'D'
		//                WHEN 'F'
		//                WHEN 'P'
		//                WHEN 'S'
		//                WHEN 'V'
		//                     MOVE 0 TO X-SUB
		//                WHEN OTHER
		//                     MOVE 0 TO X-SUB
		//           END-EVALUATE.
		switch (ws.getProgramHoldAreas().getWsVr4CrsRefRsnCd()) {

		case 'U':// COB_CODE: MOVE 1 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 1));
			break;

		case 'B':// COB_CODE: MOVE 2 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 2));
			break;

		case 'R':// COB_CODE: MOVE 3 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 3));
			break;

		case 'I':// COB_CODE: MOVE 4 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 4));
			break;

		case 'L':// COB_CODE: MOVE 5 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 5));
			break;

		case 'M':// COB_CODE: MOVE 6 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 6));
			break;

		case 'D':
		case 'F':
		case 'P':
		case 'S':
		case 'V':// COB_CODE: MOVE 0 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 0));
			break;

		default:// COB_CODE: MOVE 0 TO X-SUB
			ws.getProgramSubscripts().setxSub(((short) 0));
			break;
		}
		// COB_CODE: IF WS-VR4-ACT-INACT-CD = 'A'
		//                TO VSR-INCD-ADJD-MOD4-CD
		//           END-IF.
		if (ws.getProgramHoldAreas().getWsVr4ActInactCd() == 'A') {
			// COB_CODE: MOVE WS-VR4-CRS-REF-RSN-CD
			//             TO VSR-INCD-RSN-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefIncd().getRsnCd().setRsnCd(String.valueOf(ws.getProgramHoldAreas().getWsVr4CrsRefRsnCd()));
			// COB_CODE: MOVE WS-VR4-ADJD-PROC-CD
			//             TO VSR-INCD-ADJD-PROC-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdProcCd(ws.getProgramHoldAreas().getWsVr4AdjdProcCd());
			// COB_CODE: MOVE WS-VR4-ADJD-PROC-MOD1-CD
			//             TO VSR-INCD-ADJD-MOD1-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdMod1Cd(ws.getProgramHoldAreas().getWsVr4AdjdProcMod1Cd());
			// COB_CODE: MOVE WS-VR4-ADJD-PROC-MOD2-CD
			//             TO VSR-INCD-ADJD-MOD2-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdMod2Cd(ws.getProgramHoldAreas().getWsVr4AdjdProcMod2Cd());
			// COB_CODE: MOVE WS-VR4-ADJD-PROC-MOD3-CD
			//             TO VSR-INCD-ADJD-MOD3-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdMod3Cd(ws.getProgramHoldAreas().getWsVr4AdjdProcMod3Cd());
			// COB_CODE: MOVE WS-VR4-ADJD-PROC-MOD4-CD
			//             TO VSR-INCD-ADJD-MOD4-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefIncd().setAdjdMod4Cd(ws.getProgramHoldAreas().getWsVr4AdjdProcMod4Cd());
		}
		//---------------------------------------------------------------*
		//  THE 'WS-SAVE-XREF-CLI' IS USED ONLY WHEN THERE ARE MULTIPLE  *
		//  CAS JOINING WITH A XREF THAT IS INACTIVE SO THAT THE CURSOR  *
		//  RETURNS MULTIPLE INACTIVES PER LINE. THE PROGRAM WAS FAILING *
		//  THE 'IF' IN 2152 FOR DIFFERENT CAS/SAME LINE & SAME XREF.    *
		//  3/1/06  C298.                                                *
		//---------------------------------------------------------------*
		//
		//**
		// COB_CODE: IF X-SUB > 0
		//           AND WS-VR4-ACT-INACT-CD = 'A'
		//                                 VSR-PROC-MOD4-CD
		//           END-IF.
		if (ws.getProgramSubscripts().getxSub() > 0 && ws.getProgramHoldAreas().getWsVr4ActInactCd() == 'A') {
			// COB_CODE: MOVE WS-VR4-CRS-RF-CLM-CNTL-ID
			//                                TO VSR-XREF-CLM-ID (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setClmId(ws.getProgramHoldAreas().getWsVr4CrsRfClmCntlId());
			// COB_CODE: MOVE WS-VR4-CS-RF-CLM-CL-SX-ID
			//                                TO VSR-XREF-SX-ID (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setSxId(ws.getProgramHoldAreas().getWsVr4CsRfClmClSxId());
			// COB_CODE: IF WS-VR4-CRS-REF-CLM-PMT-P1 > 0
			//                TO WS-VR4-CRS-REF-CLM-PMT-2
			//           ELSE
			//                TO WS-VR4-CRS-REF-CLM-PMT-2
			//           END-IF
			if (ws.getProgramHoldAreas().getWsVr4CrsRefClmPmtN().getP1() > 0) {
				// COB_CODE: MOVE WS-VR4-CRS-REF-CLM-PMT-P2
				//             TO WS-VR4-CRS-REF-CLM-PMT-1
				ws.setWsVr4CrsRefClmPmt1Formatted(ws.getProgramHoldAreas().getWsVr4CrsRefClmPmtN().getP2Formatted());
				// COB_CODE: MOVE WS-VR4-CRS-REF-CLM-PMT-P1
				//             TO WS-VR4-CRS-REF-CLM-PMT-2
				ws.setWsVr4CrsRefClmPmt2Formatted(ws.getProgramHoldAreas().getWsVr4CrsRefClmPmtN().getP1Formatted());
			} else {
				// COB_CODE: MOVE ZEROS
				//             TO WS-VR4-CRS-REF-CLM-PMT-1
				ws.setWsVr4CrsRefClmPmt1(((short) 0));
				// COB_CODE: MOVE WS-VR4-CRS-REF-CLM-PMT-P2
				//             TO WS-VR4-CRS-REF-CLM-PMT-2
				ws.setWsVr4CrsRefClmPmt2Formatted(ws.getProgramHoldAreas().getWsVr4CrsRefClmPmtN().getP2Formatted());
			}
			// COB_CODE: MOVE WS-VR4-CRS-REF-CLM-PMT-USE
			//                                TO VSR-XREF-PMT-ID (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setPmtIdFromBuffer(ws.getWsVr4CrsRefClmPmtUseBytes());
			// COB_CODE: IF WS-VR4-CRS-REF-CLI-P1 = SPACES
			//                   TO WS-VR4-CRS-REF-CLI-3
			//           END-IF
			if (Characters.EQ_SPACE.test(ws.getProgramHoldAreas().getWsVr4CrsRefCliN().getP1Formatted())) {
				// COB_CODE: MOVE ZEROS
				//             TO WS-VR4-CRS-REF-CLI-1
				ws.getWsVr4CrsRefCliUse().setCli1(((short) 0));
				// COB_CODE: MOVE WS-VR4-CRS-REF-CLI-P3
				//             TO WS-VR4-CRS-REF-CLI-2
				ws.getWsVr4CrsRefCliUse().setCli2Formatted(ws.getProgramHoldAreas().getWsVr4CrsRefCliN().getP3Formatted());
				// COB_CODE: MOVE WS-VR4-CRS-REF-CLI-P2
				//             TO WS-VR4-CRS-REF-CLI-3
				ws.getWsVr4CrsRefCliUse().setCli3Formatted(ws.getProgramHoldAreas().getWsVr4CrsRefCliN().getP2Formatted());
			}
			// COB_CODE: IF WS-VR4-CRS-REF-CLI-P1 = SPACES
			//           AND WS-VR4-CRS-REF-CLI-P2 = SPACES
			//                   TO WS-VR4-CRS-REF-CLI-3
			//           END-IF
			if (Characters.EQ_SPACE.test(ws.getProgramHoldAreas().getWsVr4CrsRefCliN().getP1Formatted())
					&& Characters.EQ_SPACE.test(ws.getProgramHoldAreas().getWsVr4CrsRefCliN().getP2Formatted())) {
				// COB_CODE: MOVE ZEROS
				//             TO WS-VR4-CRS-REF-CLI-3
				//                WS-VR4-CRS-REF-CLI-2
				ws.getWsVr4CrsRefCliUse().setCli3(((short) 0));
				ws.getWsVr4CrsRefCliUse().setCli2(((short) 0));
				// COB_CODE: MOVE WS-VR4-CRS-REF-CLI-P3
				//             TO WS-VR4-CRS-REF-CLI-3
				ws.getWsVr4CrsRefCliUse().setCli3Formatted(ws.getProgramHoldAreas().getWsVr4CrsRefCliN().getP3Formatted());
			}
			// COB_CODE: MOVE WS-VR4-CRS-REF-CLI-USE
			//                                TO VSR-XREF-CLI-ID (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setCliIdFromBuffer(ws.getWsVr4CrsRefCliUse().getWsVr4CrsRefCliUseBytes());
			// COB_CODE: MOVE WS-VR4-CRS-REF-RSN-CD
			//                                TO VSR-XREF-RSN-CD (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setRsnCd(String.valueOf(ws.getProgramHoldAreas().getWsVr4CrsRefRsnCd()));
			// COB_CODE: MOVE WS-VR4-DNL-RMRK-CD
			//                                TO VSR-XREF-DNL-RMRK-CD (X-SUB)
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub())
					.setDnlRmrkCd(ws.getProgramHoldAreas().getWsVr4DnlRmrkCd());
			// COB_CODE: MOVE WS-VR4-ADJD-PROC-CD
			//                         TO VSR-ADJD-PROC-CD (X-SUB)
			//                            VSR-PROC-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setCd(ws.getProgramHoldAreas().getWsVr4AdjdProcCd());
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setCd(ws.getProgramHoldAreas().getWsVr4AdjdProcCd());
			// COB_CODE: MOVE WS-VR4-ADJD-PROC-MOD1-CD
			//                         TO VSR-ADJD-MOD1-CD (X-SUB)
			//                            VSR-PROC-MOD1-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod1Cd(ws.getProgramHoldAreas().getWsVr4AdjdProcMod1Cd());
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes()
					.setMod1Cd(ws.getProgramHoldAreas().getWsVr4AdjdProcMod1Cd());
			// COB_CODE: MOVE WS-VR4-ADJD-PROC-MOD2-CD
			//                         TO VSR-ADJD-MOD2-CD (X-SUB)
			//                            VSR-PROC-MOD2-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod2Cd(ws.getProgramHoldAreas().getWsVr4AdjdProcMod2Cd());
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes()
					.setMod2Cd(ws.getProgramHoldAreas().getWsVr4AdjdProcMod2Cd());
			// COB_CODE: MOVE WS-VR4-ADJD-PROC-MOD3-CD
			//                         TO VSR-ADJD-MOD3-CD (X-SUB)
			//                            VSR-PROC-MOD3-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod3Cd(ws.getProgramHoldAreas().getWsVr4AdjdProcMod3Cd());
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes()
					.setMod3Cd(ws.getProgramHoldAreas().getWsVr4AdjdProcMod3Cd());
			// COB_CODE: MOVE WS-VR4-ADJD-PROC-MOD4-CD
			//                         TO VSR-ADJD-MOD4-CD (X-SUB)
			//                            VSR-PROC-MOD4-CD
			ws.getNf07areaofVsrProgramLineArea().getXrefArea(ws.getProgramSubscripts().getxSub()).getAdjudicatedCodes()
					.setMod4Cd(ws.getProgramHoldAreas().getWsVr4AdjdProcMod4Cd());
			ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes()
					.setMod4Cd(ws.getProgramHoldAreas().getWsVr4AdjdProcMod4Cd());
		}
	}

	/**Original name: 6010-FETCH-TEMP-TRIG-ROW<br>
	 * <pre>----------------- 6000'S ARE FOR DB2 SQL------------------------*
	 *     DISPLAY '    *6010-FETCH-TEMP-TRIG-ROW* '.
	 * *    -----------------------------------------------------------*
	 * *    THIS WILL FETCH A CLM/SFX/PMT/LINE ROW FROM THE TEMP
	 * *    TRIGGER TABLE, WHICH CONTAINS PAYMENT INFORMATION.
	 * *    -----------------------------------------------------------*
	 * *    WILL NEED TO COME BACK TO THIS BECAUSE IT SHOULD BE ABLE --*
	 * *    TO HANDLE > 6 TRIOS PER ADJUST GRP CD PER CALC PER LINE. --*
	 * *    IT IS NOT IN THE ONLINE YET BUT ONCE THEY DEEM IT NECESSARY*
	 * *    IT WILL GO IN THE ONLINE FAST BUT WILL TAKE SOME TIME HERE.*
	 * *    PLACES TO CHANGE INCLUDE: CURSOR, FETCH, NF07AREA, LOGIC TO*
	 * *    COMBINE LIKE CAS SEGMENTS, LOGIC TO WRITE CAS SEGMENTS. UGH!
	 * *    -----------------------------------------------------------*</pre>*/
	private void fetchTempTrigRow() {
		ConcatUtil concatUtil = null;
		GenericParam abendCode = null;
		// COB_CODE:      EXEC SQL
		//                     FETCH CLAIM_LINE_CURSOR
		//                      INTO   :TRIG-PROD-IND
		//                           , :TRIG-HIST-BILL-PROV-NPI-ID
		//                           , :TRIG-LOCAL-BILL-PROV-LOB-CD
		//                           , :TRIG-LOCAL-BILL-PROV-ID
		//                           , :TRIG-BILL-PROV-ID-QLF-CD
		//                           , :TRIG-BILL-PROV-SPEC-CD
		//                           , :TRIG-HIST-PERF-PROV-NPI-ID
		//                           , :TRIG-LOCAL-PERF-PROV-LOB-CD
		//                           , :TRIG-LOCAL-PERF-PROV-ID
		//                           , :TRIG-INS-ID
		//                           , :TRIG-MEMBER-ID
		//                           , :TRIG-CORP-RCV-DT
		//                           , :TRIG-PMT-ADJD-DT
		//                           , :TRIG-PMT-FRM-SERV-DT
		//                           , :TRIG-PMT-THR-SERV-DT
		//                           , :TRIG-BILL-FRM-DT
		//                           , :TRIG-BILL-THR-DT
		//                           , :TRIG-ASG-CD
		//                           , :WS-TRIG-CLM-PD-AM
		//                           , :TRIG-ADJ-TYP-CD
		//                           , :TRIG-KCAPS-TEAM-NM
		//                           , :TRIG-KCAPS-USE-ID
		//                           , :TRIG-HIST-LOAD-CD
		//                           , :TRIG-PMT-BK-PROD-CD
		//                           , :TRIG-DRG-CD
		//           **************  25
		//                           , :WS-TRIG-SCHDL-DRG-ALW-AM
		//                           , :WS-TRIG-ALT-DRG-ALW-AM
		//                           , :WS-TRIG-OVER-DRG-ALW-AM
		//                           , :TRIG-PMT-OI-IN
		//                           , :TRIG-PAT-ACT-MED-REC-ID
		//                           , :TRIG-PGM-AREA-CD
		//                           , :TRIG-GMIS-INDICATOR
		//                           , :TRIG-FIN-CD
		//                           , :TRIG-ADJD-PROV-STAT-CD
		//                           , :TRIG-ITS-CLM-TYP-CD
		//                           , :TRIG-PRMPT-PAY-DAY-CD
		//                           , :TRIG-PRMPT-PAY-OVRD-CD
		//                           , :WS-ACCRUED-PRMPT-PAY-INT-AM
		//           **************  38
		//                           , :TRIG-PROV-UNWRP-DT
		//                           , :TRIG-GRP-ID
		//                           , :TRIG-TYP-GRP-CD
		//                           , :TRIG-NPI-CD
		//                           , :TRIG-CLAIM-LOB-CD
		//                           , :TRIG-RATE-CD
		//                           , :TRIG-NTWRK-CD
		//                           , :TRIG-BASE-CN-ARNG-CD
		//                           , :TRIG-PRIM-CN-ARNG-CD
		//           ************    47
		//                           , :TRIG-ENR-CL-CD
		//                           , :WS-TRIG-LST-FNL-PMT-PT-ID
		//                           , :TRIG-STAT-ADJ-PREV-PMT
		//                           , :TRIG-EFT-IND
		//                           , :TRIG-EFT-ACCOUNT-TYPE
		//                           , :TRIG-EFT-ACCT
		//                           , :TRIG-EFT-TRANS
		//                           , :TRIG-ALPH-PRFX-CD
		//                           , :TRIG-GL-OFST-ORIG-CD
		//                           , :WS-TRIG-GL-SOTE-ORIG-CD
		//                           , :TRIG-VOID-CD
		//                           , :TRIG-ITS-INS-ID
		//                           , :TRIG-ADJ-RESP-CD
		//                           , :TRIG-ADJ-TRCK-CD
		//                           , :TRIG-CLMCK-ADJ-STAT-CD
		//                           , :TRIG-837-BILL-PROV-NPI-ID
		//                           , :TRIG-837-PERF-PROV-NPI-ID
		//                           , :TRIG-ITS-CK
		//                           , :TRIG-DATE-COVERAGE-LAPSED
		//                           , :TRIG-OI-PAY-NM
		//                           , :TRIG-CORR-PRIORITY-SUB-ID
		//                           , :TRIG-HIPAA-VERSION-FORMAT-ID
		//                           , :TRIG-VBR-IN
		//                           , :TRIG-MRKT-PKG-CD
		//                           , :DCLS02809SA.CLM-CNTL-ID
		//                           , :DCLS02809SA.CLM-CNTL-SFX-ID
		//                           , :DCLS02809SA.CLM-PMT-ID
		//                           , :DCLS02809SA.CLI-ID
		//                           , :DCLS02809SA.PROV-SBMT-LN-NO-ID
		//                           , :DCLS02809SA.ADDNL-RMT-LI-ID
		//                           , :DCLS02809SA.LI-FRM-SERV-DT
		//           **************  75
		//                           , :DCLS02809SA.LI-THR-SERV-DT
		//                           , :DCLS02809SA.BEN-TYP-CD
		//                           , :DCLS02809SA.EXMN-ACTN-CD
		//                           , :DCLS02809SA.DNL-RMRK-CD
		//                           , :DCLS02993SA.DNL-RMRK-USG-CD
		//                           , :DCLS02993SA.PRMPT-PAY-DNL-IN
		//                           , :DCLS02809SA.LI-EXPLN-CD
		//                           , :DCLS02809SA.RVW-BY-CD
		//                           , :DCLS02809SA.PROC-CD
		//                           , :DCLS02809SA.PROC-MOD1-CD
		//                           , :DCLS02809SA.PROC-MOD2-CD
		//                           , :DCLS02809SA.PROC-MOD3-CD
		//                           , :DCLS02809SA.PROC-MOD4-CD
		//                           , :DCLS02809SA.REV-CD
		//                           , :DCLS02809SA.UNIT-CT
		//                           , :DCLS02809SA.ORIG-SBMT-UNIT-CT
		//                           , :DCLS02809SA.POS-CD
		//                           , :DCLS02809SA.TS-CD
		//                           , :DCLS02809SA.ORIG-SBMT-CHG-AM
		//                           , :DCLS02809SA.CHG-AM
		//                           , :DCLS02809SA.LI-ALCT-OI-COV-AM
		//                           , :DCLS02809SA.LI-ALCT-OI-PD-AM
		//           ************    97
		//                           , :DCLS02809SA.LI-ALCT-OI-SAVE-AM
		//                           , :DCLS02809SA.COV-PMT-LVL-STT-CD
		//                           , :DCLS02809SA.LI-ALW-CHG-AM
		//                           , :DCLS02809SA.FEP-EDT-OVRD1-CD
		//                           , :DCLS02809SA.FEP-EDT-OVRD2-CD
		//                           , :DCLS02809SA.FEP-EDT-OVRD3-CD
		//                           , :DCLS02809SA.ADJD-OVRD1-CD
		//                           , :DCLS02809SA.ADJD-OVRD2-CD
		//                           , :DCLS02809SA.ADJD-OVRD3-CD
		//                           , :DCLS02809SA.ADJD-OVRD4-CD
		//                           , :DCLS02809SA.ADJD-OVRD5-CD
		//                           , :DCLS02809SA.ADJD-OVRD6-CD
		//                           , :DCLS02809SA.ADJD-OVRD7-CD
		//                           , :DCLS02809SA.ADJD-OVRD8-CD
		//                           , :DCLS02809SA.ADJD-OVRD9-CD
		//           ************    112
		//                           , :DCLS02809SA.ADJD-OVRD10-CD
		//                           , :DCLS04315SA.CRS-RF-CLM-CNTL-ID
		//                           , :DCLS04315SA.CS-RF-CLM-CL-SX-ID
		//                           , :DCLS04315SA.CRS-REF-CLM-PMT-ID
		//                           , :DCLS04315SA.CRS-REF-CLI-ID
		//                           , :DCLS04315SA.CRS-REF-RSN-CD
		//                           , :DCLS04315SA.DNL-RMRK-CD
		//                           , :DCLS04315SA.ADJD-PROC-CD
		//                           , :DCLS04315SA.ADJD-PROC-MOD1-CD
		//                           , :DCLS04315SA.ADJD-PROC-MOD2-CD
		//                           , :DCLS04315SA.ADJD-PROC-MOD3-CD
		//                           , :DCLS04315SA.ADJD-PROC-MOD4-CD
		//                           , :DCLS04315SA.INIT-CLM-CNTL-ID
		//                           , :DCLS04315SA.INT-CLM-CNL-SFX-ID
		//                           , :DCLS04315SA.INIT-CLM-PMT-ID
		//                           , :DCLS04315SA.INIT-CLM-TS
		//                           , :DCLS04315SA.ACT-INACT-CD
		//                           , :WS-XR4-CRS-RF-CLM-CNTL-ID
		//                           , :WS-XR4-CS-RF-CLM-CL-SX-ID
		//                           , :WS-XR4-CRS-REF-CLM-PMT-ID
		//                           , :WS-XR4-CRS-REF-CLI-ID
		//                           , :WS-XR4-CRS-REF-RSN-CD
		//                           , :WS-XR4-DNL-RMRK-CD
		//                           , :WS-XR4-ADJD-PROC-CD
		//                           , :WS-XR4-ADJD-PROC-MOD1-CD
		//                           , :WS-XR4-ADJD-PROC-MOD2-CD
		//                           , :WS-XR4-ADJD-PROC-MOD3-CD
		//                           , :WS-XR4-ADJD-PROC-MOD4-CD
		//                           , :WS-XR4-INIT-CLM-CNTL-ID
		//                           , :WS-XR4-INT-CLM-CNL-SFX-ID
		//                           , :WS-XR4-INIT-CLM-PMT-ID
		//                           , :WS-XR4-XR3-INIT-CLM-TS
		//                           , :WS-XR4-ACT-INACT-CD
		//           *                 -------------------------
		//           *                 ORIGINAL SUBMITTED INFO
		//           *                   PROFESSIONAL CLAIMS
		//           *                 -------------------------
		//                           , :DCLS02713SA.REF-ID
		//           *                 -------------------------
		//                           , :DCLS02682SA.ID-CD
		//                           , :DCLS02682SA.LST-ORG-NM
		//                           , :DCLS02682SA.FRST-NM
		//                           , :DCLS02682SA.MID-NM
		//                           , :DCLS02682SA.SFX-NM
		//                           , :DCLS02652SA.CLM-SBMT-ID
		//                           , :DCLS02652SA.MNTRY-AM
		//                           , :DCLS02652SA.FAC-VL-CD
		//           ********        138
		//                           , :DCLS02652SA.FAC-QLF-CD
		//                           , :DCLS02652SA.CLM-FREQ-TYP-CD
		//           *                 -------------------------
		//           *                  PAYMENT CAS INFORMATION
		//           *                 -------------------------
		//                           , :DCLS02800SA.CLM-ADJ-GRP-CD
		//                           , :DCLS02800SA.CLM-ADJ-RSN-CD
		//                           , :DCLS02800SA.MNTRY-AM
		//                           , :DCLS02800SA.QNTY-CT
		//                           , :WS-EOB-AM
		//                           , :WS-HIST-BUNDLE-IND
		//                END-EXEC.
		s02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Dao
				.fetchClaimLineCursor(ws.getS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Nf0735ml());
		// COB_CODE: EVALUATE SQLCODE
		//              WHEN 0
		//                  END-IF
		//              WHEN +100
		//                  MOVE 1 TO CLAIM-CURSOR-EOF-SW
		//              WHEN -911
		//                  CALL 'ABEND' USING ABEND-CODE
		//              WHEN OTHER
		//                  CALL 'ABEND' USING ABEND-CODE
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: MOVE CLI-ID OF DCLS02809SA TO WS-LI-ID-UNPACKED
			ws.getProgramSwitches().setWsLiIdUnpacked(TruncAbs.toShort(ws.getDcls02809sa().getCliId(), 3));
			// COB_CODE: IF WS-HIST-BUNDLE-IND = 'Y'
			//              MOVE 'GMIS' TO TRIG-GMIS-INDICATOR
			//           END-IF
			if (ws.getProgramSwitches().getWsHistBundleInd() == 'Y') {
				// COB_CODE: MOVE 'GMIS' TO TRIG-GMIS-INDICATOR
				ws.getTrigRecordLayout().setTrigGmisIndicator("GMIS");
			}
			break;

		case 100:// COB_CODE: MOVE 1 TO CLAIM-CURSOR-EOF-SW
			ws.getProgramSwitches().setClaimCursorEofSw(((short) 1));
			break;

		case -911:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02801SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02801SA");
			// COB_CODE: MOVE 'FETCH  S02801 S02809' TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("FETCH  S02801 S02809");
			// COB_CODE: MOVE '6010-FETCH-TEMP-TRIG-ROW' TO DB2-ERR-PARA
			ws.setDb2ErrPara("6010-FETCH-TEMP-TRIG-ROW");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: PERFORM 1019-CLOSE-ROUTINE
			closeRoutine();
			// COB_CODE: MOVE +911 TO RETURN-CODE
			Session.setReturnCode(911);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;

		default:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02801SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02801SA");
			// COB_CODE: MOVE 'FETCH  S02801 S02809' TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("FETCH  S02801 S02809");
			// COB_CODE: MOVE '6010-FETCH-TEMP-TRIG-ROW' TO DB2-ERR-PARA
			ws.setDb2ErrPara("6010-FETCH-TEMP-TRIG-ROW");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: MOVE +2801  TO ABEND-CODE
			ws.setAbendCode(2801);
			// COB_CODE: MOVE +2801  TO RETURN-CODE
			Session.setReturnCode(2801);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;
		}
	}

	/**Original name: 6015-FETCH-VOID-ROW<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
	private void fetchVoidRow() {
		ConcatUtil concatUtil = null;
		GenericParam abendCode = null;
		// COB_CODE: EXEC SQL
		//                FETCH VOID_LINE_CURSOR
		//                 INTO   :TRIG-GMIS-INDICATOR
		//                      , :TRIG-PROD-IND
		//                      , :TRIG-HIST-BILL-PROV-NPI-ID
		//                      , :TRIG-LOCAL-BILL-PROV-LOB-CD
		//                      , :TRIG-LOCAL-BILL-PROV-ID
		//                      , :TRIG-BILL-PROV-ID-QLF-CD
		//                      , :TRIG-BILL-PROV-SPEC-CD
		//                      , :TRIG-HIST-PERF-PROV-NPI-ID
		//                      , :TRIG-LOCAL-PERF-PROV-LOB-CD
		//                      , :TRIG-LOCAL-PERF-PROV-ID
		//                      , :TRIG-INS-ID
		//                      , :TRIG-MEMBER-ID
		//                      , :TRIG-CORP-RCV-DT
		//                      , :TRIG-PMT-ADJD-DT
		//                      , :TRIG-PMT-FRM-SERV-DT
		//                      , :TRIG-PMT-THR-SERV-DT
		//                      , :TRIG-BILL-FRM-DT
		//                      , :TRIG-BILL-THR-DT
		//                      , :TRIG-ASG-CD
		//                      , :WS-TRIG-CLM-PD-AM
		//                      , :TRIG-ADJ-TYP-CD
		//                      , :TRIG-KCAPS-TEAM-NM
		//                      , :TRIG-KCAPS-USE-ID
		//                      , :TRIG-HIST-LOAD-CD
		//                      , :TRIG-PMT-BK-PROD-CD
		//                      , :TRIG-DRG-CD
		//                      , :WS-TRIG-SCHDL-DRG-ALW-AM
		//                      , :WS-TRIG-ALT-DRG-ALW-AM
		//                      , :WS-TRIG-OVER-DRG-ALW-AM
		//                      , :TRIG-PMT-OI-IN
		//                      , :TRIG-PAT-ACT-MED-REC-ID
		//                      , :TRIG-PGM-AREA-CD
		//                      , :TRIG-FIN-CD
		//                      , :TRIG-ADJD-PROV-STAT-CD
		//                      , :TRIG-ITS-CLM-TYP-CD
		//                      , :TRIG-PRMPT-PAY-DAY-CD
		//                      , :TRIG-PRMPT-PAY-OVRD-CD
		//                      , :WS-ACCRUED-PRMPT-PAY-INT-AM
		//                      , :TRIG-PROV-UNWRP-DT
		//                      , :TRIG-GRP-ID
		//                      , :TRIG-TYP-GRP-CD
		//                      , :TRIG-NPI-CD
		//                      , :TRIG-CLAIM-LOB-CD
		//                      , :TRIG-RATE-CD
		//                      , :TRIG-NTWRK-CD
		//                      , :TRIG-BASE-CN-ARNG-CD
		//                      , :TRIG-PRIM-CN-ARNG-CD
		//                      , :TRIG-ENR-CL-CD
		//                      , :WS-TRIG-LST-FNL-PMT-PT-ID
		//                      , :TRIG-STAT-ADJ-PREV-PMT
		//                      , :TRIG-EFT-IND
		//                      , :TRIG-EFT-ACCOUNT-TYPE
		//                      , :TRIG-EFT-ACCT
		//                      , :TRIG-EFT-TRANS
		//                      , :TRIG-ALPH-PRFX-CD
		//                      , :TRIG-GL-OFST-ORIG-CD
		//                      , :WS-TRIG-GL-SOTE-ORIG-CD
		//                      , :TRIG-VOID-CD
		//                      , :TRIG-ITS-INS-ID
		//                      , :TRIG-ADJ-RESP-CD
		//                      , :TRIG-ADJ-TRCK-CD
		//                      , :TRIG-CLMCK-ADJ-STAT-CD
		//                      , :TRIG-837-BILL-PROV-NPI-ID
		//                      , :TRIG-837-PERF-PROV-NPI-ID
		//                      , :TRIG-ITS-CK
		//                      , :TRIG-DATE-COVERAGE-LAPSED
		//                      , :TRIG-OI-PAY-NM
		//                      , :TRIG-CORR-PRIORITY-SUB-ID
		//                      , :TRIG-HIPAA-VERSION-FORMAT-ID
		//                      , :DCLS02813SA.VBR-IN
		//                      , :DCLS02813SA.MRKT-PKG-CD
		//                      , :DCLS02813SA.CLM-CNTL-ID
		//                      , :DCLS02813SA.CLM-CNTL-SFX-ID
		//                      , :DCLS02813SA.CLM-PMT-ID
		//                      , :DCLS02813SA.MEM-ID
		//                      , :DCLS02813SA.CLM-PD-AM
		//                      , :DCLS02813SA.ADJ-TYP-CD
		//                      , :DCLS02813SA.KCAPS-TEAM-NM
		//                      , :DCLS02813SA.KCAPS-USE-ID
		//                      , :DCLS02813SA.HIST-LOAD-CD
		//                      , :DCLS02813SA.PGM-AREA-CD
		//                      , :DCLS02813SA.FIN-CD
		//                      , :DCLS02813SA.ITS-CLM-TYP-CD
		//                      , :DCLS02813SA.TYP-GRP-CD
		//                      , :DCLS02813SA.NPI-CD
		//                      , :DCLS02813SA.LOB-CD
		//                      , :DCLS02813SA.NTWRK-CD
		//                      , :DCLS02813SA.ENR-CL-CD
		//                      , :DCLS02813SA.GL-OFST-VOID-CD
		//                      , :DCLS02813SA.GL-SOTE-VOID-CD
		//                      , :DCLS02813SA.VOID-CD
		//                      , :DCLS02813SA.DRG-CD
		//                      , :DCLS02813SA.SCHDL-DRG-ALW-AM
		//                      , :DCLS02813SA.ADJ-RESP-CD
		//                      , :DCLS02813SA.ADJ-TRCK-CD
		//                      , :DCLS02813SA.PRMPT-PAY-INT-AM
		//                      , :DCLS02809SA.CLI-ID
		//                      , :DCLS02809SA.PROV-SBMT-LN-NO-ID
		//                      , :DCLS02809SA.ADDNL-RMT-LI-ID
		//                      , :DCLS02809SA.LI-FRM-SERV-DT
		//                      , :DCLS02809SA.LI-THR-SERV-DT
		//                      , :DCLS02809SA.EXMN-ACTN-CD
		//                      , :DCLS02809SA.DNL-RMRK-CD
		//                      , :DCLS02993SA.DNL-RMRK-USG-CD
		//                      , :DCLS02809SA.PROC-CD
		//                      , :DCLS02809SA.PROC-MOD1-CD
		//                      , :DCLS02809SA.PROC-MOD2-CD
		//                      , :DCLS02809SA.PROC-MOD3-CD
		//                      , :DCLS02809SA.PROC-MOD4-CD
		//                      , :DCLS02809SA.REV-CD
		//                      , :DCLS02809SA.UNIT-CT
		//                      , :DCLS02809SA.ORIG-SBMT-UNIT-CT
		//                      , :DCLS02809SA.ORIG-SBMT-CHG-AM
		//                      , :DCLS02809SA.CHG-AM
		//                      , :DCLS02809SA.LI-ALCT-OI-PD-AM
		//                      , :DCLS02809SA.TS-CD
		//                      , :DCLS02809SA.ADJD-OVRD1-CD
		//                      , :DCLS02809SA.ADJD-OVRD2-CD
		//                      , :DCLS02809SA.ADJD-OVRD3-CD
		//                      , :DCLS02809SA.ADJD-OVRD4-CD
		//                      , :DCLS02809SA.ADJD-OVRD5-CD
		//                      , :DCLS02809SA.ADJD-OVRD6-CD
		//                      , :DCLS02809SA.ADJD-OVRD7-CD
		//                      , :DCLS02809SA.ADJD-OVRD8-CD
		//                      , :DCLS02809SA.ADJD-OVRD9-CD
		//                      , :DCLS02809SA.ADJD-OVRD10-CD
		//                      , :DCLS04315SA.CRS-RF-CLM-CNTL-ID
		//                      , :DCLS04315SA.CS-RF-CLM-CL-SX-ID
		//                      , :DCLS04315SA.CRS-REF-CLM-PMT-ID
		//                      , :DCLS04315SA.CRS-REF-CLI-ID
		//                      , :DCLS04315SA.CRS-REF-RSN-CD
		//                      , :DCLS04315SA.DNL-RMRK-CD
		//                      , :DCLS04315SA.ADJD-PROC-CD
		//                      , :DCLS04315SA.ADJD-PROC-MOD1-CD
		//                      , :DCLS04315SA.ADJD-PROC-MOD2-CD
		//                      , :DCLS04315SA.ADJD-PROC-MOD3-CD
		//                      , :DCLS04315SA.ADJD-PROC-MOD4-CD
		//                      , :DCLS04315SA.ACT-INACT-CD
		//                      , :WS-VR4-CRS-RF-CLM-CNTL-ID
		//                      , :WS-VR4-CS-RF-CLM-CL-SX-ID
		//                      , :WS-VR4-CRS-REF-CLM-PMT-ID
		//                      , :WS-VR4-CRS-REF-CLI-ID
		//                      , :WS-VR4-CRS-REF-RSN-CD
		//                      , :WS-VR4-DNL-RMRK-CD
		//                      , :WS-VR4-ADJD-PROC-CD
		//                      , :WS-VR4-ADJD-PROC-MOD1-CD
		//                      , :WS-VR4-ADJD-PROC-MOD2-CD
		//                      , :WS-VR4-ADJD-PROC-MOD3-CD
		//                      , :WS-VR4-ADJD-PROC-MOD4-CD
		//                      , :WS-VR4-INIT-CLM-CNTL-ID
		//                      , :WS-VR4-INT-CLM-CNL-SFX-ID
		//                      , :WS-VR4-INIT-CLM-PMT-ID
		//                      , :WS-VR4-ACT-INACT-CD
		//                      , :DCLS02713SA.REF-ID
		//                      , :DCLS02682SA.ID-CD
		//                      , :DCLS02682SA.LST-ORG-NM
		//                      , :DCLS02682SA.FRST-NM
		//                      , :DCLS02682SA.MID-NM
		//                      , :DCLS02682SA.SFX-NM
		//                      , :DCLS02652SA.CLM-SBMT-ID
		//                      , :DCLS02652SA.FAC-VL-CD
		//                      , :DCLS02652SA.CLM-FREQ-TYP-CD
		//                      , :DCLS02800SA.CLM-ADJ-GRP-CD
		//                      , :DCLS02800SA.CLM-ADJ-RSN-CD
		//                      , :DCLS02800SA.MNTRY-AM
		//                      , :DCLS02800SA.QNTY-CT
		//           END-EXEC.
		s02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Dao
				.fetchVoidLineCursor(ws.getS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Nf0735ml());
		// COB_CODE: EVALUATE SQLCODE
		//              WHEN 0
		//                  CONTINUE
		//              WHEN +100
		//                  MOVE 'Y' TO VOID-EOF-SW
		//              WHEN OTHER
		//                  CALL 'ABEND' USING ABEND-CODE
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		case 100:// COB_CODE: MOVE 'Y' TO VOID-EOF-SW
			ws.getProgramSwitches().setVoidEofSwFormatted("Y");
			break;

		default:// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '-'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Nf0735mlData.Len.DB2_ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "-"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "-"));
			ws.setDb2ErrKey(concatUtil.replaceInString(ws.getDb2ErrKeyFormatted()));
			// COB_CODE: MOVE 'S02801SA'            TO DB2-ERR-TABLE
			ws.setDb2ErrTable("S02801SA");
			// COB_CODE: MOVE 'FETCH  S02801 S02809' TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("FETCH  S02801 S02809");
			// COB_CODE: MOVE '6015-FETCH-VOID-ROW ' TO DB2-ERR-PARA
			ws.setDb2ErrPara("6015-FETCH-VOID-ROW ");
			// COB_CODE: PERFORM 9950-ERROR-PARA
			errorPara();
			// COB_CODE: MOVE +2801  TO ABEND-CODE
			ws.setAbendCode(2801);
			// COB_CODE: MOVE +2801  TO RETURN-CODE
			Session.setReturnCode(2801);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;
		}
	}

	/**Original name: 6020-GET-DENTAL-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     DISPLAY '6021-GET-DENTAL-DATA'.
	 * *****************************************************************
	 *  THIS SELECT RETRIEVES THE DATA PER CLAIM LINE FOR THE          *
	 *  DENTAL SERVICE PROVIDER                                        *
	 * *****************************************************************</pre>*/
	private void getDentalData() {
		// COB_CODE: EXEC SQL
		//              SELECT PROD_SRV_ID
		//                    ,PROD_SRV_ID_QLF_CD
		//                    ,SV3013_PROC_MOD_CD
		//                    ,SV3014_PROC_MOD_CD
		//                    ,SV3015_PROC_MOD_CD
		//                    ,SV3016_PROC_MOD_CD
		//                    ,MNTRY_AM
		//                    ,QNTY_CT
		//              INTO  :DCLS02719SA.PROD-SRV-ID
		//                  , :DCLS02719SA.PROD-SRV-ID-QLF-CD
		//                  , :DCLS02719SA.SV3013-PROC-MOD-CD
		//                  , :DCLS02719SA.SV3014-PROC-MOD-CD
		//                  , :DCLS02719SA.SV3015-PROC-MOD-CD
		//                  , :DCLS02719SA.SV3016-PROC-MOD-CD
		//                  , :DCLS02719SA.MNTRY-AM
		//                  , :DCLS02719SA.QNTY-CT
		//              FROM S02719SA
		//                  WHERE CLM_CNTL_ID = :WS-CLM-CNTL-ID
		//                    AND CLM_CNTL_SFX_ID = :WS-CLM-CNTL-SFX-ID
		//                    AND PROV_SBMT_LN_NO_ID = :WS-PROV-SBMT-LN-NO-ID
		//           END-EXEC.
		s02719saDao.selectRec(ws.getWsSelectFields().getClmCntlId(), ws.getWsSelectFields().getClmCntlSfxId(),
				ws.getWsSelectFields().getProvSbmtLnNoId(), ws.getDcls02719sa());
	}

	/**Original name: 6021-GET-INSTITUTIONAL-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     DISPLAY '6021-GET-INSTITUTIONAL-DATA'.
	 * *****************************************************************
	 *  THIS SELECT RETRIEVES THE DATA PER CLAIM LINE FOR THE          *
	 *  INSTITUTION SERVICE PROVIDER                                  *
	 * *****************************************************************</pre>*/
	private void getInstitutionalData() {
		// COB_CODE: EXEC SQL
		//              SELECT SV201_PROD_SRV_ID
		//                    ,PROD_SRV_ID_QLF_CD
		//                    ,SV2022_PROD_SRV_ID
		//                    ,SV2023_PROC_MOD_CD
		//                    ,SV2024_PROC_MOD_CD
		//                    ,SV2025_PROC_MOD_CD
		//                    ,SV2026_PROC_MOD_CD
		//                    ,SV203_MNTRY_AM
		//                    ,QNTY_CT
		//              INTO  :DCLS02718SA.SV201-PROD-SRV-ID
		//                  , :DCLS02718SA.PROD-SRV-ID-QLF-CD
		//                  , :DCLS02718SA.SV2022-PROD-SRV-ID
		//                  , :DCLS02718SA.SV2023-PROC-MOD-CD
		//                  , :DCLS02718SA.SV2024-PROC-MOD-CD
		//                  , :DCLS02718SA.SV2025-PROC-MOD-CD
		//                  , :DCLS02718SA.SV2026-PROC-MOD-CD
		//                  , :DCLS02718SA.SV203-MNTRY-AM
		//                  , :DCLS02718SA.QNTY-CT
		//              FROM S02718SA
		//                  WHERE CLM_CNTL_ID = :WS-CLM-CNTL-ID
		//                    AND CLM_CNTL_SFX_ID = :WS-CLM-CNTL-SFX-ID
		//                    AND PROV_SBMT_LN_NO_ID = :WS-PROV-SBMT-LN-NO-ID
		//           END-EXEC.
		s02718saDao.selectRec(ws.getWsSelectFields().getClmCntlId(), ws.getWsSelectFields().getClmCntlSfxId(),
				ws.getWsSelectFields().getProvSbmtLnNoId(), ws.getDcls02718sa());
	}

	/**Original name: 6022-GET-PROFESSIONAL-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     DISPLAY '6022-GET-PROFESSIONAL-DATA'.
	 * *****************************************************************
	 *  THIS SELECT RETRIEVES THE DATA PER CLAIM LINE FOR THE          *
	 *  PROFESSIONAL SERVICE PROVIDER                                  *
	 * *****************************************************************</pre>*/
	private void getProfessionalData() {
		// COB_CODE: EXEC SQL
		//              SELECT PROD_SRV_ID
		//                    ,PROD_SRV_ID_QLF_CD
		//                    ,SV1013_PROC_MOD_CD
		//                    ,SV1014_PROC_MOD_CD
		//                    ,SV1015_PROC_MOD_CD
		//                    ,SV1016_PROC_MOD_CD
		//                    ,SV102_MNTRY_AM
		//                    ,QNTY_CT
		//              INTO  :DCLS02717SA.PROD-SRV-ID
		//                  , :DCLS02717SA.PROD-SRV-ID-QLF-CD
		//                  , :DCLS02717SA.SV1013-PROC-MOD-CD
		//                  , :DCLS02717SA.SV1014-PROC-MOD-CD
		//                  , :DCLS02717SA.SV1015-PROC-MOD-CD
		//                  , :DCLS02717SA.SV1016-PROC-MOD-CD
		//                  , :DCLS02717SA.SV102-MNTRY-AM
		//                  , :DCLS02717SA.QNTY-CT
		//              FROM S02717SA
		//                  WHERE CLM_CNTL_ID = :WS-CLM-CNTL-ID
		//                    AND CLM_CNTL_SFX_ID = :WS-CLM-CNTL-SFX-ID
		//                    AND PROV_SBMT_LN_NO_ID = :WS-PROV-SBMT-LN-NO-ID
		//           END-EXEC.
		s02717saDao.selectRec(ws.getWsSelectFields().getClmCntlId(), ws.getWsSelectFields().getClmCntlSfxId(),
				ws.getWsSelectFields().getProvSbmtLnNoId(), ws.getDcls02717sa());
	}

	/**Original name: 6030-GET-MEMBER-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     DISPLAY '6030-GET-MEMBER-DATA'.
	 * *****************************************************************
	 *  THIS SELECT RETRIEVES THE DATA PER CLAIM FOR THE PATIENT       *
	 *  INFORMATION REQUIRED ON THE 837                                *
	 * *****************************************************************</pre>*/
	private void getMemberData() {
		// COB_CODE: EXEC SQL
		//              SELECT LST_ORG_NM
		//                    ,FRST_NM
		//                    ,MID_NM
		//                    ,SFX_NM
		//                    ,SSN_ID
		//              INTO  :DCLS02952SA.LST-ORG-NM
		//                  , :DCLS02952SA.FRST-NM
		//                  , :DCLS02952SA.MID-NM
		//                  , :DCLS02952SA.SFX-NM
		//                  , :DCLS02952SA.SSN-ID
		//              FROM S02952SA
		//                  WHERE CLM_CNTL_ID = :WS-CLM-CNTL-ID
		//                    AND CLM_CNTL_SFX_ID = :WS-CLM-CNTL-SFX-ID
		//                    AND CLM_PMT_ID      = :WS-CLM-PMT-ID
		//                    AND ENTY_ID_CD      = 'IL'
		//                    AND ENTY_TYP_QLF_CD = '1'
		//           END-EXEC.
		s02952saDao.selectRec(ws.getWsSelectFields().getClmCntlId(), ws.getWsSelectFields().getClmCntlSfxId(), ws.getWsSelectFields().getClmPmtId(),
				ws.getDcls02952sa());
	}

	/**Original name: 6031-GET-PATIENT-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     DISPLAY '6031-GET-PATIENT-DATA'.
	 * *****************************************************************
	 *  THIS SELECT RETRIEVES THE DATA PER CLAIM FOR THE PATIENT       *
	 *  INFORMATION REQUIRED ON THE 837                                *
	 * *****************************************************************</pre>*/
	private void getPatientData() {
		// COB_CODE: EXEC SQL
		//              SELECT LST_ORG_NM
		//                    ,FRST_NM
		//                    ,MID_NM
		//                    ,SFX_NM
		//                    ,SSN_ID
		//                    ,ENTY_ID_CD
		//                    ,ENTY_TYP_QLF_CD
		//              INTO  :DCLS02952SA.LST-ORG-NM
		//                  , :DCLS02952SA.FRST-NM
		//                  , :DCLS02952SA.MID-NM
		//                  , :DCLS02952SA.SFX-NM
		//                  , :DCLS02952SA.SSN-ID
		//                  , :DCLS02952SA.ENTY-ID-CD
		//                  , :DCLS02952SA.ENTY-TYP-QLF-CD
		//              FROM S02952SA
		//                  WHERE CLM_CNTL_ID     = :WS-CLM-CNTL-ID
		//                    AND CLM_CNTL_SFX_ID = :WS-CLM-CNTL-SFX-ID
		//                    AND CLM_PMT_ID      = :WS-CLM-PMT-ID
		//                    AND ENTY_ID_CD      = 'QC'
		//                    AND ENTY_TYP_QLF_CD = '1'
		//           END-EXEC.
		s02952saDao.selectRec1(ws.getWsSelectFields().getClmCntlId(), ws.getWsSelectFields().getClmCntlSfxId(), ws.getWsSelectFields().getClmPmtId(),
				ws.getDcls02952sa());
	}

	/**Original name: 6032-GET-PERF-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     DISPLAY '6032-GET-PERF-DATA'.
	 * *****************************************************************
	 *  THIS SELECT RETRIEVES THE DATA PER CLAIM FOR THE PATIENT       *
	 *  INFORMATION REQUIRED ON THE 837                                *
	 * *****************************************************************</pre>*/
	private void getPerfData() {
		// COB_CODE: EXEC SQL
		//              SELECT LST_ORG_NM
		//                    ,FRST_NM
		//                    ,MID_NM
		//                    ,SFX_NM
		//                    ,ENTY_ID_CD
		//                    ,ENTY_TYP_QLF_CD
		//              INTO  :DCLS02952SA.LST-ORG-NM
		//                  , :DCLS02952SA.FRST-NM
		//                  , :DCLS02952SA.MID-NM
		//                  , :DCLS02952SA.SFX-NM
		//                  , :DCLS02952SA.ENTY-ID-CD
		//                  , :DCLS02952SA.ENTY-TYP-QLF-CD
		//              FROM S02952SA
		//                  WHERE CLM_CNTL_ID     = :WS-CLM-CNTL-ID
		//                    AND CLM_CNTL_SFX_ID = :WS-CLM-CNTL-SFX-ID
		//                    AND CLM_PMT_ID      = :WS-CLM-PMT-ID
		//                    AND ENTY_ID_CD      = '87'
		//                    AND ENTY_TYP_QLF_CD IN ('1', '2')
		//           END-EXEC.
		s02952saDao.selectRec2(ws.getWsSelectFields().getClmCntlId(), ws.getWsSelectFields().getClmCntlSfxId(), ws.getWsSelectFields().getClmPmtId(),
				ws.getDcls02952sa());
	}

	/**Original name: 6033-GET-BILLED-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     DISPLAY '6033-GET-BILLED-DATA'.
	 * *****************************************************************
	 *  THIS SELECT RETRIEVES THE DATA PER CLAIM FOR THE PATIENT       *
	 *  INFORMATION REQUIRED ON THE 837                                *
	 * *****************************************************************</pre>*/
	private void getBilledData() {
		// COB_CODE: EXEC SQL
		//              SELECT LST_ORG_NM
		//                    ,FRST_NM
		//                    ,MID_NM
		//                    ,SFX_NM
		//                    ,ENTY_ID_CD
		//                    ,ENTY_TYP_QLF_CD
		//              INTO  :DCLS02952SA.LST-ORG-NM
		//                  , :DCLS02952SA.FRST-NM
		//                  , :DCLS02952SA.MID-NM
		//                  , :DCLS02952SA.SFX-NM
		//                  , :DCLS02952SA.ENTY-ID-CD
		//                  , :DCLS02952SA.ENTY-TYP-QLF-CD
		//              FROM S02952SA
		//                  WHERE CLM_CNTL_ID     = :WS-CLM-CNTL-ID
		//                    AND CLM_CNTL_SFX_ID = :WS-CLM-CNTL-SFX-ID
		//                    AND CLM_PMT_ID      = :WS-CLM-PMT-ID
		//                    AND ENTY_ID_CD      = '85'
		//                    AND ENTY_TYP_QLF_CD IN ('1', '2')
		//           END-EXEC.
		s02952saDao.selectRec3(ws.getWsSelectFields().getClmCntlId(), ws.getWsSelectFields().getClmCntlSfxId(), ws.getWsSelectFields().getClmPmtId(),
				ws.getDcls02952sa());
	}

	/**Original name: 7000-CK-FOR-COMMIT<br>
	 * <pre>----------------------------------------------------------------*
	 *     DISPLAY ' *7000-CK-FOR-COMMIT* '.</pre>*/
	private void ckForCommit() {
		GenericParam second1 = null;
		StringParam fc = null;
		GenericParam second2 = null;
		// COB_CODE: MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN.
		ws.getWsDateFields().getGregorn().setGregornFormatted(Functions.subString(DateFunctions.getCurrentDate(), 1, 14));
		// COB_CODE: MOVE SAVE-DATE-TIME TO TIMESTP-STRING.
		ws.getTimestp().setStringFld(ws.getWsDateFields().getSaveDateTime().getSaveDateTimeFormatted());
		// COB_CODE: MOVE 50             TO TIMESTP-LENGTH.
		ws.getTimestp().setLength2(((short) 50));
		// COB_CODE: MOVE 'YYYYMMDDHHMISS' TO PICSTR-STRING.
		ws.getPicstr().setStringFld("YYYYMMDDHHMISS");
		// COB_CODE: MOVE 50             TO PICSTR-LENGTH.
		ws.getPicstr().setLength2(((short) 50));
		// COB_CODE: CALL 'CEESECS' USING TIMESTP, PICSTR, SECOND1, FC.
		second1 = new GenericParam(MarshalByteExt.doubleToBuffer(ws.getSecond1()));
		fc = new StringParam(ws.getFc(), Nf0735mlData.Len.FC);
		DynamicCall.invoke("CEESECS", ws.getTimestp(), ws.getPicstr(), second1, fc);
		ws.setSecond1FromBuffer(second1.getByteData());
		ws.setFc(fc.getString());
		// COB_CODE: IF FC = LOW-VALUE
		//               NEXT SENTENCE
		//           ELSE
		//               DISPLAY '       FC      IS  ' FC
		//           END-IF.
		if (!Characters.EQ_LOW.test(ws.getFcFormatted())) {
			// COB_CODE: DISPLAY ' >->-> FAIL CE1DT020 1  FC IS ' FC
			DisplayUtil.sysout.write(" >->-> FAIL CE1DT020 1  FC IS ", ws.getFcFormatted());
			// COB_CODE: DISPLAY '       SAVE DATE TIME = ' SAVE-DATE-TIME
			DisplayUtil.sysout.write("       SAVE DATE TIME = ", ws.getWsDateFields().getSaveDateTime().getSaveDateTimeFormatted());
			// COB_CODE: DISPLAY '       TIMESTP  IS  ' TIMESTP
			DisplayUtil.sysout.write("       TIMESTP  IS  ", ws.getTimestp().getTimestpFormatted());
			// COB_CODE: DISPLAY '       TIMELENG IS  ' TIMESTP-LENGTH
			DisplayUtil.sysout.write("       TIMELENG IS  ", ws.getTimestp().getLength2AsString());
			// COB_CODE: DISPLAY '       PICSTR   IS  ' PICSTR-STRING
			DisplayUtil.sysout.write("       PICSTR   IS  ", ws.getPicstr().getStringFldFormatted());
			// COB_CODE: DISPLAY '       PIC LENG IS  ' PICSTR-LENGTH
			DisplayUtil.sysout.write("       PIC LENG IS  ", ws.getPicstr().getLength2AsString());
			// COB_CODE: DISPLAY '       SECOND1  IS  ' SECOND1
			DisplayUtil.sysout.write("       SECOND1  IS  ", ws.getSecond1AsString());
			// COB_CODE: DISPLAY '       FC      IS  ' FC
			DisplayUtil.sysout.write("       FC      IS  ", ws.getFcFormatted());
		}
		// COB_CODE: MOVE GREGORN         TO TIMESTP-STRING.
		ws.getTimestp().setStringFld(ws.getWsDateFields().getGregorn().getSaveDateTimeFormatted());
		// COB_CODE: MOVE 50              TO TIMESTP-LENGTH.
		ws.getTimestp().setLength2(((short) 50));
		// COB_CODE: MOVE 'YYYYMMDDHHMISS' TO PICSTR-STRING.
		ws.getPicstr().setStringFld("YYYYMMDDHHMISS");
		// COB_CODE: MOVE 50              TO PICSTR-LENGTH.
		ws.getPicstr().setLength2(((short) 50));
		// COB_CODE: CALL 'CEESECS' USING TIMESTP, PICSTR, SECOND2, FC.
		second2 = new GenericParam(MarshalByteExt.doubleToBuffer(ws.getSecond2()));
		fc = new StringParam(ws.getFc(), Nf0735mlData.Len.FC);
		DynamicCall.invoke("CEESECS", ws.getTimestp(), ws.getPicstr(), second2, fc);
		ws.setSecond2FromBuffer(second2.getByteData());
		ws.setFc(fc.getString());
		// COB_CODE: IF FC = LOW-VALUE
		//               NEXT SENTENCE
		//           ELSE
		//               DISPLAY ' '
		//           END-IF.
		if (!Characters.EQ_LOW.test(ws.getFcFormatted())) {
			// COB_CODE: DISPLAY ' >->->  FAIL CE1DT020 2  FC IS ' FC
			DisplayUtil.sysout.write(" >->->  FAIL CE1DT020 2  FC IS ", ws.getFcFormatted());
			// COB_CODE: DISPLAY '        GREGORN    = ' GREGORN
			DisplayUtil.sysout.write("        GREGORN    = ", ws.getWsDateFields().getGregorn().getSaveDateTimeFormatted());
			// COB_CODE: DISPLAY '        TIMESTP  IS  ' TIMESTP
			DisplayUtil.sysout.write("        TIMESTP  IS  ", ws.getTimestp().getTimestpFormatted());
			// COB_CODE: DISPLAY '        PICSTR   IS  ' PICSTR-STRING
			DisplayUtil.sysout.write("        PICSTR   IS  ", ws.getPicstr().getStringFldFormatted());
			// COB_CODE: DISPLAY '        SECOND2  IS  ' SECOND2
			DisplayUtil.sysout.write("        SECOND2  IS  ", ws.getSecond2AsString());
			// COB_CODE: DISPLAY '        FC      IS  ' FC
			DisplayUtil.sysout.write("        FC      IS  ", ws.getFcFormatted());
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
		}
		// COB_CODE: MOVE  ZEROS      TO  DIFFERENCE.
		ws.getWsDateFields().setDifference(0);
		// COB_CODE: COMPUTE DIFFERENCE = ( SECOND2 - SECOND1 ).
		ws.getWsDateFields().setDifference(((int) (ws.getSecond2() - ws.getSecond1())));
		// COB_CODE: IF DIFFERENCE < 0
		//               MOVE '-' TO DIFF-SIGN
		//           ELSE
		//               MOVE '+' TO DIFF-SIGN.
		if (ws.getWsDateFields().getDifference() < 0) {
			// COB_CODE: MOVE '-' TO DIFF-SIGN
			ws.getWsDateFields().setDiffSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+' TO DIFF-SIGN.
			ws.getWsDateFields().setDiffSignFormatted("+");
		}
		// COB_CODE: MOVE DIFFERENCE TO DIFFERENCE-DISP.
		ws.getWsDateFields().setDifferenceDisp(TruncAbs.toInt(ws.getWsDateFields().getDifference(), 6));
		// COB_CODE: MOVE GREGORN-HH   TO GREG-HH.
		ws.getWsDateFields().getGregHhMiSs().setHhFormatted(ws.getWsDateFields().getGregorn().getHhFormatted());
		// COB_CODE: MOVE GREGORN-MI   TO GREG-MI.
		ws.getWsDateFields().getGregHhMiSs().setMiFormatted(ws.getWsDateFields().getGregorn().getMiFormatted());
		// COB_CODE: MOVE GREGORN-SS   TO GREG-SS.
		ws.getWsDateFields().getGregHhMiSs().setSsFormatted(ws.getWsDateFields().getGregorn().getSsFormatted());
		// COB_CODE: MOVE SAVE-DATE-HH TO SAVE-HH.
		ws.getWsDateFields().getSaveHhMiSs().setHhFormatted(ws.getWsDateFields().getSaveDateTime().getHhFormatted());
		// COB_CODE: MOVE SAVE-DATE-MI TO SAVE-MI.
		ws.getWsDateFields().getSaveHhMiSs().setMiFormatted(ws.getWsDateFields().getSaveDateTime().getMiFormatted());
		// COB_CODE: MOVE SAVE-DATE-SS TO SAVE-SS.
		ws.getWsDateFields().getSaveHhMiSs().setSsFormatted(ws.getWsDateFields().getSaveDateTime().getSsFormatted());
		// COB_CODE: IF DIFFERENCE >= EXPANDED-CMT-TM-SS
		//               OR CHKP-UOW-CNTR > WS-ROW-FREQ-CMT-CT
		//               MOVE GREGORN      TO SAVE-DATE-TIME
		//           END-IF.
		if (ws.getWsDateFields().getDifference() >= ws.getDataSavedFromTable3086().getExpandedCmtTmSs()
				|| ws.getProgramCounters().getChkpUowCntr() > ws.getDataSavedFromTable3086().getWsRowFreqCmtCt()) {
			// COB_CODE: PERFORM 7100-SQL-COMMIT
			sqlCommit();
			// COB_CODE: MOVE 1 TO CHKP-UOW-CNTR
			ws.getProgramCounters().setChkpUowCntr(1);
			// COB_CODE: MOVE GREGORN      TO SAVE-DATE-TIME
			ws.getWsDateFields().getSaveDateTime().setSaveDateTimeBytes(ws.getWsDateFields().getGregorn().getSaveDateTimeBytes());
		}
	}

	/**Original name: 7100-SQL-COMMIT<br>
	 * <pre>------------- COMMIT POINT  ------------------------------------*
	 *     DISPLAY '7100-SQL-COMMIT'.</pre>*/
	private void sqlCommit() {
		GenericParam abendCode = null;
		// COB_CODE: EXEC SQL
		//              COMMIT
		//           END-EXEC.
		DbService.getCurrent().commit(dbAccessStatus);
		// COB_CODE:      EVALUATE SQLCODE
		//                    WHEN 0
		//                        ADD 1 TO COMMIT-CNT
		//           *>>>         MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN
		//           *>>>         MOVE GREGORN      TO SAVE-DATE-TIME
		//           *>>>         MOVE GREGORN-HH   TO FORMAT-HH
		//           *>>>         MOVE GREGORN-MI   TO FORMAT-MM
		//           *>>>         MOVE GREGORN-SS   TO FORMAT-SS
		//           *            DISPLAY ' '
		//           *>>>         PERFORM 9930-DISPLAY-CLAIM-NUM
		//           *            DISPLAY ' '
		//                    WHEN OTHER
		//                        CALL 'ABEND' USING ABEND-CODE
		//                END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: ADD 1 TO COMMIT-CNT
			ws.getProgramCounters().setCommitCnt(Trunc.toInt(1 + ws.getProgramCounters().getCommitCnt(), 6));
			//>>>         MOVE FUNCTION CURRENT-DATE(1:14) TO GREGORN
			//>>>         MOVE GREGORN      TO SAVE-DATE-TIME
			//>>>         MOVE GREGORN-HH   TO FORMAT-HH
			//>>>         MOVE GREGORN-MI   TO FORMAT-MM
			//>>>         MOVE GREGORN-SS   TO FORMAT-SS
			//            DISPLAY ' '
			//>>>         PERFORM 9930-DISPLAY-CLAIM-NUM
			//            DISPLAY ' '
			break;

		default:// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
			// COB_CODE: DISPLAY '     ' WS-PGM-NAME ' ABENDING '
			DisplayUtil.sysout.write("     ", ws.getWsPgmNameFormatted(), " ABENDING ");
			// COB_CODE: DISPLAY '    ERROR DOING A COMMIT      '
			DisplayUtil.sysout.write("    ERROR DOING A COMMIT      ");
			// COB_CODE: DISPLAY '    7100-SQL-COMMIT           '
			DisplayUtil.sysout.write("    7100-SQL-COMMIT           ");
			// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
			// COB_CODE: PERFORM 9940-DISPLAY-SQLCODE
			displaySqlcode();
			// COB_CODE: MOVE SPACES                    TO DB2-ERR-TABLE
			ws.setDb2ErrTable("");
			// COB_CODE: MOVE 'SQL COMMIT         '     TO DB2-ERR-LAST-CALL
			ws.setDb2ErrLastCall("SQL COMMIT         ");
			// COB_CODE: MOVE '7100-SQL-COMMIT       '  TO DB2-ERR-PARA
			ws.setDb2ErrPara("7100-SQL-COMMIT       ");
			//            PERFORM DB2-STAT-ERR
			// COB_CODE: EXEC SQL
			//               ROLLBACK
			//           END-EXEC
			DbService.getCurrent().rollback(dbAccessStatus);
			// COB_CODE: MOVE +200   TO ABEND-CODE
			ws.setAbendCode(200);
			// COB_CODE: MOVE +200   TO RETURN-CODE
			Session.setReturnCode(200);
			// COB_CODE: CALL 'ABEND' USING ABEND-CODE
			abendCode = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getAbendCode()));
			DynamicCall.invoke("ABEND", abendCode);
			ws.setAbendCodeFromBuffer(abendCode.getByteData());
			break;
		}
	}

	/**Original name: 9940-DISPLAY-SQLCODE<br>*/
	private void displaySqlcode() {
		// COB_CODE: IF SQLCODE < 0
		//               MOVE '-' TO WS-SQLCODE-SIGN
		//           ELSE
		//               MOVE '+' TO WS-SQLCODE-SIGN.
		if (sqlca.getSqlcode() < 0) {
			// COB_CODE: MOVE '-' TO WS-SQLCODE-SIGN
			ws.setWsSqlcodeSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+' TO WS-SQLCODE-SIGN.
			ws.setWsSqlcodeSignFormatted("+");
		}
		// COB_CODE: MOVE SQLCODE TO WS-SQLCODE.
		ws.setWsSqlcode(TruncAbs.toInt(sqlca.getSqlcode(), 9));
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '***>>> SQLCODE = ' WS-SQLCODE-UNPACKED
		//                   '    SQLSTATE = ' SQLSTATE.
		DisplayUtil.sysout.write("***>>> SQLCODE = ", ws.getWsSqlcodeUnpackedFormatted(), "    SQLSTATE = ", sqlca.getSqlstateFormatted());
		// COB_CODE: DISPLAY '***>>> SQLERRMC = ' SQLERRMC.
		DisplayUtil.sysout.write("***>>> SQLERRMC = ", sqlca.getSqlerrmcFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
	}

	/**Original name: 9950-ERROR-PARA<br>*/
	private void errorPara() {
		// COB_CODE: MOVE SQLCODE TO SQL-CODE.
		ws.setSqlCode(sqlca.getSqlcode());
		// COB_CODE: DISPLAY ' *** A B E N D ***  SQL-CODE : ' SQL-CODE.
		DisplayUtil.sysout.write(" *** A B E N D ***  SQL-CODE : ", ws.getSqlCodeAsString());
		// COB_CODE: DISPLAY '                    SQLCODE  : ' SQLCODE.
		DisplayUtil.sysout.write("                    SQLCODE  : ", sqlca.getSqlcodeAsString());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY ' DB2-ERR-KEY IS ' DB2-ERR-KEY.
		DisplayUtil.sysout.write(" DB2-ERR-KEY IS ", ws.getDb2ErrKeyFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY ' DB2-ERR-TABLE IS ' DB2-ERR-TABLE.
		DisplayUtil.sysout.write(" DB2-ERR-TABLE IS ", ws.getDb2ErrTableFormatted());
		// COB_CODE: DISPLAY ' DB2-ERR-LAST-CALL  IS ' DB2-ERR-LAST-CALL.
		DisplayUtil.sysout.write(" DB2-ERR-LAST-CALL  IS ", ws.getDb2ErrLastCallFormatted());
		// COB_CODE: DISPLAY ' DB2-ERR-PARA  IS ' DB2-ERR-PARA.
		DisplayUtil.sysout.write(" DB2-ERR-PARA  IS ", ws.getDb2ErrParaFormatted());
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: PERFORM 9999-DISPLAY-MORE-DB2-INFORM.
		displayMoreDb2Inform();
		// COB_CODE: EXEC SQL
		//               ROLLBACK
		//           END-EXEC.
		DbService.getCurrent().rollback(dbAccessStatus);
	}

	/**Original name: 9999-DISPLAY-MORE-DB2-INFORM<br>
	 * <pre>----------------------------------------------------------------*
	 * **************************************************
	 *      CALL THE DSNTIAR SUBROUTINE FOR THE
	 *      FORMATED DB2 MESSAGE AND DISPLAY THE MESSAGE.
	 * **************************************************</pre>*/
	private void displayMoreDb2Inform() {
		GenericParam errorTextLen = null;
		// COB_CODE: CALL 'DSNTIAR' USING SQLCA
		//                                ERROR-MESSAGE
		//                                ERROR-TEXT-LEN.
		errorTextLen = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getErrorTextLen()));
		DynamicCall.invoke("DSNTIAR", sqlca, ws.getErrorMessage(), errorTextLen);
		ws.setErrorTextLenFromBuffer(errorTextLen.getByteData());
		//**************************************************
		//    DISPLAY THE ERROR ON SYSOUT
		//**************************************************
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '***************************************************'
		DisplayUtil.sysout.write("***************************************************");
		// COB_CODE: DISPLAY 'CLAIMS SYSTEM BATCH DB2 ERROR EXPLANATION'.
		DisplayUtil.sysout.write("CLAIMS SYSTEM BATCH DB2 ERROR EXPLANATION");
		// COB_CODE: DISPLAY ERROR-TEXT(1).
		DisplayUtil.sysout.write(ws.getErrorMessage().getTextFormatted(1));
		// COB_CODE: DISPLAY ERROR-TEXT(2).
		DisplayUtil.sysout.write(ws.getErrorMessage().getTextFormatted(2));
		// COB_CODE: DISPLAY ERROR-TEXT(3).
		DisplayUtil.sysout.write(ws.getErrorMessage().getTextFormatted(3));
		// COB_CODE: DISPLAY ERROR-TEXT(4).
		DisplayUtil.sysout.write(ws.getErrorMessage().getTextFormatted(4));
		// COB_CODE: DISPLAY ERROR-TEXT(5).
		DisplayUtil.sysout.write(ws.getErrorMessage().getTextFormatted(5));
		// COB_CODE: DISPLAY ERROR-TEXT(6).
		DisplayUtil.sysout.write(ws.getErrorMessage().getTextFormatted(6));
		// COB_CODE: DISPLAY ERROR-TEXT(7).
		DisplayUtil.sysout.write(ws.getErrorMessage().getTextFormatted(7));
		// COB_CODE: DISPLAY ERROR-TEXT(8).
		DisplayUtil.sysout.write(ws.getErrorMessage().getTextFormatted(8));
		// COB_CODE: DISPLAY '***************************************************'
		DisplayUtil.sysout.write("***************************************************");
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
	}

	@Override
	public DdCard[] getDefaultDdCards() {
		return new DdCard[] { new DdCard("PAYMENTS", 3405), new DdCard("JOBPARMS", 80), new DdCard("IN835TRG", 600), new DdCard("GMISPAYS", 3405),
				new DdCard("GMISVOID", 3405), new DdCard("VOIDS", 3405) };
	}

	public void initPmtCommonInfo() {
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getProdInd().setProdInd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillingProvider().getLocalBillProvLobCd().setLocalBillProvLobCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillingProvider().setHistBillProvNpiId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillingProvider().setLocalBillProvId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillingProvider().setMedicareXover(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setClmLotNumber("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setClmCntlSfxId(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPmtIdFormatted("00");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setLiIdFormatted("000");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setProvSbmtLnNoIdFormatted("000");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAddnlRmtLiIdFormatted("000");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setClmFnlzdPmtFormatted("00");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getGmisIndicator().setGmisIndicator("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getRecordType().setRecordType("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getVersionFormatId().setVersionFormatId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getClaimLobCd().setClaimLobCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getExmnActnCd().setExmnActnCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setDnlRmrkCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setDnlRmrkUsgCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPerformingProvider().getLocalPerfProvLobCd()
				.setLocalPerfProvLobCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPerformingProvider().setHistPerfProvNpiId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPerformingProvider().setLocalPerfProvId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillProvIdQlfCd().setBillProvIdQlfCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillProvSpec("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setInsuredPrefix("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setInsuredNumber("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMemberId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getCorpRcvDt().setYear("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getCorpRcvDt().setMonth("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getCorpRcvDt().setDay("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtAdjdDt().setYear("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtAdjdDt().setMonth("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtAdjdDt().setDay("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtFrmServDt().setYear("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtFrmServDt().setMonth("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtFrmServDt().setDay("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtThrServDt().setYear("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtThrServDt().setMonth("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtThrServDt().setDay("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillFrmDt().setYear("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillFrmDt().setMonth("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillFrmDt().setDay("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillThrDt().setYear("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillThrDt().setMonth("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getBillThrDt().setDay("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getDateCoverageLapsed().setYear("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getDateCoverageLapsed().setMonth("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getDateCoverageLapsed().setDay("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAsgCd().setAsgCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setClmPdAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjTypCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setKcapsTeamNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setKcapsUseId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setHistLoadCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPmtBkProdCd().setPmtBkProdCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setDrgCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setSchdlDrgAlwAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAltDrgAlwAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setOverDrgAlwAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setOiCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setOiPayNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatActMedRec("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPgmAreaCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setFinCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdProvStat(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getItsInformation().getClmTypCd().setClmTypCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getItsInformation().setCk(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getItsInformation().setInsPrefix("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getItsInformation().setInsNumber("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPrmptPayDayCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getPrmptPayOvrd().setPrmptPayOvrd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAccruedPromptPayInt(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getProvUnwrpDt().setYear("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getProvUnwrpDt().setMonth("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getProvUnwrpDt().setDay("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setGrpId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setNpiCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setRateCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setNtwrkCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setFiller("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBaseCnArngCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPrimCnArngCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPrcInstReimb1(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPrcProfReimb1(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setEligInstReimb("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setEligProfReimb("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setEnrClCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setStatAdjPrev(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setEftInd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setEftAccountType(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setEftAcct("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setEftTrans("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setTypGrpCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getLiFrmServDt().setYear("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getLiFrmServDt().setMonth("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getLiFrmServDt().setDay("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getLiThrServDt().setYear("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getLiThrServDt().setMonth("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getLiThrServDt().setDay("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBenTypCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPrmptPayDnlIn(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setLiExplnCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setRvwByCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod1Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod2Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod3Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod4Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setRevCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setUnitCt(new AfDecimal(0, 7, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setOrigSubUnitCt(new AfDecimal(0, 7, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPlaceOfService("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setTypeOfService("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setOrigSbmtChgAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setChgAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setLiAlctOiCovAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setLiAlctOiPdAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setLiAlctOiSaveAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdProvStatCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setLiAlwChgAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setGlOfstCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setGlSoteCdFormatted("00");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setFepEdtOvrd1Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setFepEdtOvrd2Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setFepEdtOvrd3Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd1Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd2Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd3Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd4Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd5Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd6Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd7Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd8Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd9Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd10Cd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setVoidCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjRespCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setAdjTrckCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCorrectedPrioritySubid("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setRefId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setRevenueSrvId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureSrvId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setIdQlfCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod1("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod2("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod3("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod4("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setMonetaryAmount(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setQuantityCount(new AfDecimal(0, 7, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrLstOrgNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrFrstNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrMidNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrSfxNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setMbrSsnId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatLstOrgNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatFrstNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatMidNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatSfxNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPatSsnId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfLstOrgNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfFrstNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfMidNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfSfxNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setPerfEntyTypQlfCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillLstOrgNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillFrstNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillMidNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillSfxNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setBillEntyTypQlfCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837InsuredPrefix("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837InsuredNumber("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837InsLstNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837InsFrstNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837InsMidNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837InsSfxNm("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837ClmSbmtId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837FacVlCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837FacQlfCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837ClmFreqTypCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837BillProvNpiId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().setCsr837PerfProvNpiId("");
	}

	public void initCalcInformation() {
		for (int idx0 = 1; idx0 <= CsrCalcInformation.CALC_ENTRY_MAXOCCURS; idx0++) {
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcTypCd(Types.SPACE_CHAR);
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcCovLobCd(Types.SPACE_CHAR);
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcExplnCd(Types.SPACE_CHAR);
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcPayCd(Types.SPACE_CHAR);
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcAlwChgAm(new AfDecimal(0, 11, 2));
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcPdAm(new AfDecimal(0, 11, 2));
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcCorpCd(Types.SPACE_CHAR);
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcProdCd("");
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcTypBenCd("");
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setFiller("");
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcSpeciBenCd4(Types.SPACE_CHAR);
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcGlAcctId("");
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcInsTcCd("");
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcOriginalLineFormatted("000");
			ws.getNf07areaofCsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcOriginalTypeFormatted("0");
		}
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().setLineCalcExplnCd(Types.SPACE_CHAR);
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().setLineCalcAlwChgAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getCalcInformation().setLineCalcPdAm(new AfDecimal(0, 11, 2));
	}

	public void initOriginalSubmittedInfo() {
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setRevenueSrvId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureSrvId("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setIdQlfCd("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod1("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod2("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod3("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod4("");
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setMonetaryAmount(new AfDecimal(0, 11, 2));
		ws.getNf07areaofCsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setQuantityCount(new AfDecimal(0, 7, 2));
	}

	public void initPmtCommonInfo1() {
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getProdInd().setProdInd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillingProvider().getLocalBillProvLobCd().setLocalBillProvLobCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillingProvider().setHistBillProvNpiId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillingProvider().setLocalBillProvId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillingProvider().setMedicareXover(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setClmLotNumber("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setClmCntlSfxId(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPmtIdFormatted("00");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setLiIdFormatted("000");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setProvSbmtLnNoIdFormatted("000");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAddnlRmtLiIdFormatted("000");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setClmFnlzdPmtFormatted("00");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getGmisIndicator().setGmisIndicator("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getRecordType().setRecordType("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getVersionFormatId().setVersionFormatId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getClaimLobCd().setClaimLobCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getExmnActnCd().setExmnActnCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setDnlRmrkCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setDnlRmrkUsgCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPerformingProvider().getLocalPerfProvLobCd()
				.setLocalPerfProvLobCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPerformingProvider().setHistPerfProvNpiId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPerformingProvider().setLocalPerfProvId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillProvIdQlfCd().setBillProvIdQlfCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillProvSpec("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setInsuredPrefix("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setInsuredNumber("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMemberId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getCorpRcvDt().setYear("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getCorpRcvDt().setMonth("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getCorpRcvDt().setDay("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPmtAdjdDt().setYear("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPmtAdjdDt().setMonth("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPmtAdjdDt().setDay("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPmtFrmServDt().setYear("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPmtFrmServDt().setMonth("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPmtFrmServDt().setDay("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPmtThrServDt().setYear("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPmtThrServDt().setMonth("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPmtThrServDt().setDay("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillFrmDt().setYear("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillFrmDt().setMonth("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillFrmDt().setDay("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillThrDt().setYear("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillThrDt().setMonth("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getBillThrDt().setDay("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getDateCoverageLapsed().setYear("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getDateCoverageLapsed().setMonth("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getDateCoverageLapsed().setDay("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAsgCd().setAsgCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setClmPdAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjTypCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setKcapsTeamNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setKcapsUseId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setHistLoadCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPmtBkProdCd().setPmtBkProdCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setDrgCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setSchdlDrgAlwAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAltDrgAlwAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setOverDrgAlwAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setOiCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setOiPayNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatActMedRec("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPgmAreaCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setFinCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdProvStat(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getItsInformation().getClmTypCd().setClmTypCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getItsInformation().setCk(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getItsInformation().setInsPrefix("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getItsInformation().setInsNumber("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPrmptPayDayCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getPrmptPayOvrd().setPrmptPayOvrd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAccruedPromptPayInt(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getProvUnwrpDt().setYear("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getProvUnwrpDt().setMonth("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getProvUnwrpDt().setDay("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setGrpId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setNpiCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setRateCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setNtwrkCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setFiller("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBaseCnArngCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPrimCnArngCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPrcInstReimb1(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPrcProfReimb1(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setEligInstReimb("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setEligProfReimb("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setEnrClCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setStatAdjPrev(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setEftInd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setEftAccountType(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setEftAcct("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setEftTrans("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setTypGrpCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getLiFrmServDt().setYear("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getLiFrmServDt().setMonth("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getLiFrmServDt().setDay("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getLiThrServDt().setYear("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getLiThrServDt().setMonth("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getLiThrServDt().setDay("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBenTypCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPrmptPayDnlIn(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setLiExplnCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setRvwByCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod1Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod2Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod3Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getAdjudicatedCodes().setMod4Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setRevCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setUnitCt(new AfDecimal(0, 7, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setOrigSubUnitCt(new AfDecimal(0, 7, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPlaceOfService("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setTypeOfService("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setOrigSbmtChgAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setChgAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setLiAlctOiCovAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setLiAlctOiPdAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setLiAlctOiSaveAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdProvStatCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setLiAlwChgAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setGlOfstCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setGlSoteCdFormatted("00");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setFepEdtOvrd1Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setFepEdtOvrd2Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setFepEdtOvrd3Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd1Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd2Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd3Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd4Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd5Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd6Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd7Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd8Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd9Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjdOvrd10Cd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setVoidCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjRespCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setAdjTrckCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCorrectedPrioritySubid("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setRefId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setRevenueSrvId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureSrvId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setIdQlfCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod1("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod2("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod3("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod4("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setMonetaryAmount(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setQuantityCount(new AfDecimal(0, 7, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrLstOrgNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrFrstNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrMidNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrSfxNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setMbrSsnId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatLstOrgNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatFrstNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatMidNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatSfxNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPatSsnId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfLstOrgNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfFrstNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfMidNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfSfxNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setPerfEntyTypQlfCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillLstOrgNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillFrstNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillMidNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillSfxNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setBillEntyTypQlfCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837InsuredPrefix("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837InsuredNumber("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837InsLstNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837InsFrstNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837InsMidNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837InsSfxNm("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837ClmSbmtId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837FacVlCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837FacQlfCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837ClmFreqTypCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837BillProvNpiId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().setCsr837PerfProvNpiId("");
	}

	public void initCalcInformation1() {
		for (int idx0 = 1; idx0 <= CsrCalcInformation.CALC_ENTRY_MAXOCCURS; idx0++) {
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcTypCd(Types.SPACE_CHAR);
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcCovLobCd(Types.SPACE_CHAR);
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcExplnCd(Types.SPACE_CHAR);
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcPayCd(Types.SPACE_CHAR);
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcAlwChgAm(new AfDecimal(0, 11, 2));
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcPdAm(new AfDecimal(0, 11, 2));
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcCorpCd(Types.SPACE_CHAR);
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcProdCd("");
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcTypBenCd("");
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setFiller("");
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcSpeciBenCd4(Types.SPACE_CHAR);
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcGlAcctId("");
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcInsTcCd("");
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcOriginalLineFormatted("000");
			ws.getNf07areaofVsrProgramLineArea().getCalcInformation().getCalcEntry(idx0).setCalcOriginalTypeFormatted("0");
		}
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().setLineCalcExplnCd(Types.SPACE_CHAR);
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().setLineCalcAlwChgAm(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getCalcInformation().setLineCalcPdAm(new AfDecimal(0, 11, 2));
	}

	public void initOriginalSubmittedInfo1() {
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setRevenueSrvId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureSrvId("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setIdQlfCd("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod1("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod2("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod3("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setProcedureMod4("");
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setMonetaryAmount(new AfDecimal(0, 11, 2));
		ws.getNf07areaofVsrProgramLineArea().getPmtCommonInfo().getOriginalSubmittedInfo().setQuantityCount(new AfDecimal(0, 7, 2));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int INPUT_REC = 600;
		public static final int OUT_ORIG_REC = 3405;
		public static final int OUT_VOID_REC = 3405;
		public static final int OUT_ORIG_REC_GMIS = 3405;
		public static final int OUT_VOID_REC_GMIS = 3405;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
