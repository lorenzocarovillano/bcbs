/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: DB2-S02952-NAME-DEMO-INFO<br>
 * Variable: DB2-S02952-NAME-DEMO-INFO from copybook NF05CURS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Db2S02952NameDemoInfo {

	//==== PROPERTIES ====
	//Original name: DB2-PROVIDER-NAME
	private String providerName = DefaultValues.stringVal(Len.PROVIDER_NAME);
	//Original name: DB2-PAT-LST-NM
	private String patLstNm = DefaultValues.stringVal(Len.PAT_LST_NM);
	//Original name: DB2-PAT-FRST-NM
	private String patFrstNm = DefaultValues.stringVal(Len.PAT_FRST_NM);
	//Original name: DB2-PAT-MID-NM
	private String patMidNm = DefaultValues.stringVal(Len.PAT_MID_NM);

	//==== METHODS ====
	public void setS02952NameDemoInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		providerName = MarshalByte.readString(buffer, position, Len.PROVIDER_NAME);
		position += Len.PROVIDER_NAME;
		patLstNm = MarshalByte.readString(buffer, position, Len.PAT_LST_NM);
		position += Len.PAT_LST_NM;
		patFrstNm = MarshalByte.readString(buffer, position, Len.PAT_FRST_NM);
		position += Len.PAT_FRST_NM;
		patMidNm = MarshalByte.readString(buffer, position, Len.PAT_MID_NM);
	}

	public byte[] getS02952NameDemoInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, providerName, Len.PROVIDER_NAME);
		position += Len.PROVIDER_NAME;
		MarshalByte.writeString(buffer, position, patLstNm, Len.PAT_LST_NM);
		position += Len.PAT_LST_NM;
		MarshalByte.writeString(buffer, position, patFrstNm, Len.PAT_FRST_NM);
		position += Len.PAT_FRST_NM;
		MarshalByte.writeString(buffer, position, patMidNm, Len.PAT_MID_NM);
		return buffer;
	}

	public void setProviderName(String providerName) {
		this.providerName = Functions.subString(providerName, Len.PROVIDER_NAME);
	}

	public String getProviderName() {
		return this.providerName;
	}

	public void setPatLstNm(String patLstNm) {
		this.patLstNm = Functions.subString(patLstNm, Len.PAT_LST_NM);
	}

	public String getPatLstNm() {
		return this.patLstNm;
	}

	public void setPatFrstNm(String patFrstNm) {
		this.patFrstNm = Functions.subString(patFrstNm, Len.PAT_FRST_NM);
	}

	public String getPatFrstNm() {
		return this.patFrstNm;
	}

	public void setPatMidNm(String patMidNm) {
		this.patMidNm = Functions.subString(patMidNm, Len.PAT_MID_NM);
	}

	public String getPatMidNm() {
		return this.patMidNm;
	}

	public String getPatMidNmFormatted() {
		return Functions.padBlanks(getPatMidNm(), Len.PAT_MID_NM);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROVIDER_NAME = 25;
		public static final int PAT_LST_NM = 35;
		public static final int PAT_FRST_NM = 25;
		public static final int PAT_MID_NM = 25;
		public static final int S02952_NAME_DEMO_INFO = PROVIDER_NAME + PAT_LST_NM + PAT_FRST_NM + PAT_MID_NM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
