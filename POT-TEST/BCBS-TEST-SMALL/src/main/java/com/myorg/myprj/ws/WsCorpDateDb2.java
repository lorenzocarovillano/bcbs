/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WS-CORP-DATE-DB2<br>
 * Variable: WS-CORP-DATE-DB2 from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsCorpDateDb2 {

	//==== PROPERTIES ====
	//Original name: WS-CORP-CCYY
	private String ccyy = DefaultValues.stringVal(Len.CCYY);
	//Original name: FILLER-WS-CORP-DATE-DB2
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: WS-CORP-MM
	private String mm = DefaultValues.stringVal(Len.MM);
	//Original name: FILLER-WS-CORP-DATE-DB2-1
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: WS-CORP-DD
	private String dd = DefaultValues.stringVal(Len.DD);

	//==== METHODS ====
	public void setWsCorpDateDb2Formatted(String data) {
		byte[] buffer = new byte[Len.WS_CORP_DATE_DB2];
		MarshalByte.writeString(buffer, 1, data, Len.WS_CORP_DATE_DB2);
		setWsCorpDateDb2Bytes(buffer, 1);
	}

	public void setWsCorpDateDb2Bytes(byte[] buffer, int offset) {
		int position = offset;
		ccyy = MarshalByte.readFixedString(buffer, position, Len.CCYY);
		position += Len.CCYY;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mm = MarshalByte.readFixedString(buffer, position, Len.MM);
		position += Len.MM;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dd = MarshalByte.readFixedString(buffer, position, Len.DD);
	}

	public String getCcyyFormatted() {
		return this.ccyy;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getMmFormatted() {
		return this.mm;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public String getDdFormatted() {
		return this.dd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CCYY = 4;
		public static final int MM = 2;
		public static final int DD = 2;
		public static final int FLR1 = 1;
		public static final int WS_CORP_DATE_DB2 = CCYY + MM + DD + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
