/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.ISqlCa;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: SQLCA<br>
 * Variable: SQLCA from copybook SQLCA<br>
 * Generated as a class for rule SQLCA.<br>*/
public class Sqlca extends SerializableParameter implements ISqlCa {

	//==== PROPERTIES ====
	public static final int SQLERRD_MAXOCCURS = 6;
	//Original name: SQLCAID
	private String sqlcaid = DefaultValues.stringVal(Len.SQLCAID);
	//Original name: SQLCABC
	private int sqlcabc = DefaultValues.BIN_INT_VAL;
	//Original name: SQLCODE
	private int sqlcode = DefaultValues.BIN_INT_VAL;
	//Original name: SQLERRML
	private short sqlerrml = DefaultValues.BIN_SHORT_VAL;
	//Original name: SQLERRMC
	private String sqlerrmc = DefaultValues.stringVal(Len.SQLERRMC);
	//Original name: SQLERRP
	private String sqlerrp = DefaultValues.stringVal(Len.SQLERRP);
	//Original name: SQLERRD
	private int[] sqlerrd = new int[SQLERRD_MAXOCCURS];
	//Original name: SQLWARN0
	private char sqlwarn0 = DefaultValues.CHAR_VAL;
	//Original name: SQLWARN1
	private char sqlwarn1 = DefaultValues.CHAR_VAL;
	//Original name: SQLWARN2
	private char sqlwarn2 = DefaultValues.CHAR_VAL;
	//Original name: SQLWARN3
	private char sqlwarn3 = DefaultValues.CHAR_VAL;
	//Original name: SQLWARN4
	private char sqlwarn4 = DefaultValues.CHAR_VAL;
	//Original name: SQLWARN5
	private char sqlwarn5 = DefaultValues.CHAR_VAL;
	//Original name: SQLWARN6
	private char sqlwarn6 = DefaultValues.CHAR_VAL;
	//Original name: SQLWARN7
	private char sqlwarn7 = DefaultValues.CHAR_VAL;
	//Original name: SQLWARN8
	private char sqlwarn8 = DefaultValues.CHAR_VAL;
	//Original name: SQLWARN9
	private char sqlwarn9 = DefaultValues.CHAR_VAL;
	//Original name: SQLWARNA
	private char sqlwarna = DefaultValues.CHAR_VAL;
	//Original name: SQLSTATE
	private String sqlstate = DefaultValues.stringVal(Len.SQLSTATE);

	//==== CONSTRUCTORS ====
	public Sqlca() {
		init();
	}

	//==== METHODS ====
	public void updateSqlcaid(String sqlCaid) {
		sqlcaid = sqlCaid;
	}

	public void updateSqlcode(int sqlCode) {
		sqlcode = sqlCode;
	}

	public void updateSqlerrml(short sqlErrml) {
		sqlerrml = sqlErrml;
	}

	public void updateSqlerrmc(String sqlErrmc) {
		sqlerrmc = sqlErrmc;
	}

	public void updateSqlerrp(String sqlErrp) {
		sqlerrp = sqlErrp;
	}

	public void updateSqlerrd(int index, int value) {
		setSqlerrd(index, value);
	}

	public void updateSqlwarn(int index, char value) {
		switch (index) {

		case 1:
			sqlwarn0 = value;
			break;

		case 2:
			sqlwarn1 = value;
			break;

		case 3:
			sqlwarn2 = value;
			break;

		case 4:
			sqlwarn3 = value;
			break;

		case 5:
			sqlwarn4 = value;
			break;

		case 6:
			sqlwarn5 = value;
			break;

		case 7:
			sqlwarn6 = value;
			break;

		case 8:
			sqlwarn7 = value;
			break;

		case 9:
			sqlwarn8 = value;
			break;

		case 10:
			sqlwarn9 = value;
			break;

		case 11:
			sqlwarna = value;
			break;

		default:
			break;
		}
	}

	public void updateSqlstate(String sqlState) {
		sqlstate = sqlState;
	}

	@Override
	public int getLength() {
		return Len.SQLCA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setSqlcaBytes(buf);
	}

	private void init() {
		for (int sqlerrdIdx = 1; sqlerrdIdx <= SQLERRD_MAXOCCURS; sqlerrdIdx++) {
			setSqlerrd(sqlerrdIdx, DefaultValues.BIN_INT_VAL);
		}
	}

	public String getSqlcaFormatted() {
		return MarshalByteExt.bufferToStr(getSqlcaBytes());
	}

	public void setSqlcaBytes(byte[] buffer) {
		setSqlcaBytes(buffer, 1);
	}

	public byte[] getSqlcaBytes() {
		byte[] buffer = new byte[Len.SQLCA];
		return getSqlcaBytes(buffer, 1);
	}

	public void setSqlcaBytes(byte[] buffer, int offset) {
		int position = offset;
		sqlcaid = MarshalByte.readString(buffer, position, Len.SQLCAID);
		position += Len.SQLCAID;
		sqlcabc = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		sqlcode = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		setSqlerrmBytes(buffer, position);
		position += Len.SQLERRM;
		sqlerrp = MarshalByte.readString(buffer, position, Len.SQLERRP);
		position += Len.SQLERRP;
		for (int idx = 1; idx <= SQLERRD_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setSqlerrd(idx, MarshalByte.readBinaryInt(buffer, position));
				position += Types.INT_SIZE;
			} else {
				setSqlerrd(idx, Types.INVALID_BINARY_INT_VAL);
				position += Types.INT_SIZE;
			}
		}
		setSqlwarnBytes(buffer, position);
		position += Len.SQLWARN;
		setSqlextBytes(buffer, position);
	}

	public byte[] getSqlcaBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, sqlcaid, Len.SQLCAID);
		position += Len.SQLCAID;
		MarshalByte.writeBinaryInt(buffer, position, sqlcabc);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, sqlcode);
		position += Types.INT_SIZE;
		getSqlerrmBytes(buffer, position);
		position += Len.SQLERRM;
		MarshalByte.writeString(buffer, position, sqlerrp, Len.SQLERRP);
		position += Len.SQLERRP;
		for (int idx = 1; idx <= SQLERRD_MAXOCCURS; idx++) {
			MarshalByte.writeBinaryInt(buffer, position, getSqlerrd(idx));
			position += Types.INT_SIZE;
		}
		getSqlwarnBytes(buffer, position);
		position += Len.SQLWARN;
		getSqlextBytes(buffer, position);
		return buffer;
	}

	public void setSqlcaid(String sqlcaid) {
		this.sqlcaid = Functions.subString(sqlcaid, Len.SQLCAID);
	}

	public String getSqlcaid() {
		return this.sqlcaid;
	}

	public void setSqlcabc(int sqlcabc) {
		this.sqlcabc = sqlcabc;
	}

	public int getSqlcabc() {
		return this.sqlcabc;
	}

	public void setSqlcode(int sqlcode) {
		this.sqlcode = sqlcode;
	}

	public int getSqlcode() {
		return this.sqlcode;
	}

	public String getSqlcodeFormatted() {
		return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.BINARY)).format(getSqlcode()).toString();
	}

	public String getSqlcodeAsString() {
		return getSqlcodeFormatted();
	}

	public void setSqlerrmBytes(byte[] buffer, int offset) {
		int position = offset;
		sqlerrml = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		sqlerrmc = MarshalByte.readString(buffer, position, Len.SQLERRMC);
	}

	public byte[] getSqlerrmBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, sqlerrml);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, sqlerrmc, Len.SQLERRMC);
		return buffer;
	}

	public void setSqlerrml(short sqlerrml) {
		this.sqlerrml = sqlerrml;
	}

	public short getSqlerrml() {
		return this.sqlerrml;
	}

	public void setSqlerrmc(String sqlerrmc) {
		this.sqlerrmc = Functions.subString(sqlerrmc, Len.SQLERRMC);
	}

	public String getSqlerrmc() {
		return this.sqlerrmc;
	}

	public String getSqlerrmcFormatted() {
		return Functions.padBlanks(getSqlerrmc(), Len.SQLERRMC);
	}

	public void setSqlerrp(String sqlerrp) {
		this.sqlerrp = Functions.subString(sqlerrp, Len.SQLERRP);
	}

	public String getSqlerrp() {
		return this.sqlerrp;
	}

	public void setSqlerrd(int sqlerrdIdx, int sqlerrd) {
		this.sqlerrd[sqlerrdIdx - 1] = sqlerrd;
	}

	public int getSqlerrd(int sqlerrdIdx) {
		return this.sqlerrd[sqlerrdIdx - 1];
	}

	public void setSqlwarnBytes(byte[] buffer, int offset) {
		int position = offset;
		sqlwarn0 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		sqlwarn1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		sqlwarn2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		sqlwarn3 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		sqlwarn4 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		sqlwarn5 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		sqlwarn6 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		sqlwarn7 = MarshalByte.readChar(buffer, position);
	}

	public byte[] getSqlwarnBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, sqlwarn0);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, sqlwarn1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, sqlwarn2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, sqlwarn3);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, sqlwarn4);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, sqlwarn5);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, sqlwarn6);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, sqlwarn7);
		return buffer;
	}

	public void setSqlwarn0(char sqlwarn0) {
		this.sqlwarn0 = sqlwarn0;
	}

	public char getSqlwarn0() {
		return this.sqlwarn0;
	}

	public void setSqlwarn1(char sqlwarn1) {
		this.sqlwarn1 = sqlwarn1;
	}

	public char getSqlwarn1() {
		return this.sqlwarn1;
	}

	public void setSqlwarn2(char sqlwarn2) {
		this.sqlwarn2 = sqlwarn2;
	}

	public char getSqlwarn2() {
		return this.sqlwarn2;
	}

	public void setSqlwarn3(char sqlwarn3) {
		this.sqlwarn3 = sqlwarn3;
	}

	public char getSqlwarn3() {
		return this.sqlwarn3;
	}

	public void setSqlwarn4(char sqlwarn4) {
		this.sqlwarn4 = sqlwarn4;
	}

	public char getSqlwarn4() {
		return this.sqlwarn4;
	}

	public void setSqlwarn5(char sqlwarn5) {
		this.sqlwarn5 = sqlwarn5;
	}

	public char getSqlwarn5() {
		return this.sqlwarn5;
	}

	public void setSqlwarn6(char sqlwarn6) {
		this.sqlwarn6 = sqlwarn6;
	}

	public char getSqlwarn6() {
		return this.sqlwarn6;
	}

	public void setSqlwarn7(char sqlwarn7) {
		this.sqlwarn7 = sqlwarn7;
	}

	public char getSqlwarn7() {
		return this.sqlwarn7;
	}

	public void setSqlextBytes(byte[] buffer, int offset) {
		int position = offset;
		sqlwarn8 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		sqlwarn9 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		sqlwarna = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		sqlstate = MarshalByte.readString(buffer, position, Len.SQLSTATE);
	}

	public byte[] getSqlextBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, sqlwarn8);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, sqlwarn9);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, sqlwarna);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, sqlstate, Len.SQLSTATE);
		return buffer;
	}

	public void setSqlwarn8(char sqlwarn8) {
		this.sqlwarn8 = sqlwarn8;
	}

	public char getSqlwarn8() {
		return this.sqlwarn8;
	}

	public void setSqlwarn9(char sqlwarn9) {
		this.sqlwarn9 = sqlwarn9;
	}

	public char getSqlwarn9() {
		return this.sqlwarn9;
	}

	public void setSqlwarna(char sqlwarna) {
		this.sqlwarna = sqlwarna;
	}

	public char getSqlwarna() {
		return this.sqlwarna;
	}

	public void setSqlstate(String sqlstate) {
		this.sqlstate = Functions.subString(sqlstate, Len.SQLSTATE);
	}

	public String getSqlstate() {
		return this.sqlstate;
	}

	public String getSqlstateFormatted() {
		return Functions.padBlanks(getSqlstate(), Len.SQLSTATE);
	}

	@Override
	public byte[] serialize() {
		return getSqlcaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SQLCAID = 8;
		public static final int SQLERRMC = 70;
		public static final int SQLERRP = 8;
		public static final int SQLSTATE = 5;
		public static final int SQLCABC = 4;
		public static final int SQLCODE = 4;
		public static final int SQLERRML = 2;
		public static final int SQLERRM = SQLERRML + SQLERRMC;
		public static final int SQLERRD = 4;
		public static final int SQLWARN0 = 1;
		public static final int SQLWARN1 = 1;
		public static final int SQLWARN2 = 1;
		public static final int SQLWARN3 = 1;
		public static final int SQLWARN4 = 1;
		public static final int SQLWARN5 = 1;
		public static final int SQLWARN6 = 1;
		public static final int SQLWARN7 = 1;
		public static final int SQLWARN = SQLWARN0 + SQLWARN1 + SQLWARN2 + SQLWARN3 + SQLWARN4 + SQLWARN5 + SQLWARN6 + SQLWARN7;
		public static final int SQLWARN8 = 1;
		public static final int SQLWARN9 = 1;
		public static final int SQLWARNA = 1;
		public static final int SQLEXT = SQLWARN8 + SQLWARN9 + SQLWARNA + SQLSTATE;
		public static final int SQLCA = SQLCAID + SQLCABC + SQLCODE + SQLERRM + SQLERRP + Sqlca.SQLERRD_MAXOCCURS * SQLERRD + SQLWARN + SQLEXT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
