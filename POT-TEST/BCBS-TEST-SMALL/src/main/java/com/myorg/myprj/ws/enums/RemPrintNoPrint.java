/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: REM-PRINT-NO-PRINT<br>
 * Variable: REM-PRINT-NO-PRINT from copybook NF07RMIT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class RemPrintNoPrint {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.VALUE);
	public static final String PAPER_REMIT = "PAPER";
	public static final String NO_PAPER_REMIT = "NOPRT";

	//==== METHODS ====
	public void setRemPrintNoPrint(String remPrintNoPrint) {
		this.value = Functions.subString(remPrintNoPrint, Len.VALUE);
	}

	public String getRemPrintNoPrint() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
