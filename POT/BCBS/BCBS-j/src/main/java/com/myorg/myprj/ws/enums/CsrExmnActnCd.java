/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: CSR-EXMN-ACTN-CD<br>
 * Variable: CSR-EXMN-ACTN-CD from copybook NF07AREA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CsrExmnActnCd {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char WAS_PAID = ' ';
	public static final char WAS_MANUALLY_PAID = 'M';
	public static final char WAS_DENIED = 'D';
	public static final char LINE_CANCELLED = 'C';
	private static final char[] HAS_BEEN_PAID = new char[] { ' ', 'M' };

	//==== METHODS ====
	public void setExmnActnCd(char exmnActnCd) {
		this.value = exmnActnCd;
	}

	public char getExmnActnCd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
