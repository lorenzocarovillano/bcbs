/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WS-CORP-DATE-N<br>
 * Variable: WS-CORP-DATE-N from program NF0533ML<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsCorpDateN extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WsCorpDateN() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_CORP_DATE_N;
	}

	public void setCcyyNFormatted(String ccyyN) {
		writeString(Pos.CCYY_N, Trunc.toUnsignedNumeric(ccyyN, Len.CCYY_N), Len.CCYY_N);
	}

	/**Original name: WS-CORP-CCYY-N<br>*/
	public short getCcyyN() {
		return readNumDispUnsignedShort(Pos.CCYY_N, Len.CCYY_N);
	}

	public void setMmNFormatted(String mmN) {
		writeString(Pos.MM_N, Trunc.toUnsignedNumeric(mmN, Len.MM_N), Len.MM_N);
	}

	/**Original name: WS-CORP-MM-N<br>*/
	public short getMmN() {
		return readNumDispUnsignedShort(Pos.MM_N, Len.MM_N);
	}

	public void setDdNFormatted(String ddN) {
		writeString(Pos.DD_N, Trunc.toUnsignedNumeric(ddN, Len.DD_N), Len.DD_N);
	}

	/**Original name: WS-CORP-DD-N<br>*/
	public short getDdN() {
		return readNumDispUnsignedShort(Pos.DD_N, Len.DD_N);
	}

	/**Original name: WS-CORP-DATE-9<br>*/
	public int getWsCorpDate9() {
		return readNumDispUnsignedInt(Pos.WS_CORP_DATE9, Len.WS_CORP_DATE9);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_CORP_DATE_N = 1;
		public static final int CCYY_N = WS_CORP_DATE_N;
		public static final int MM_N = CCYY_N + Len.CCYY_N;
		public static final int DD_N = MM_N + Len.MM_N;
		public static final int WS_CORP_DATE9 = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int CCYY_N = 4;
		public static final int MM_N = 2;
		public static final int DD_N = 2;
		public static final int WS_CORP_DATE_N = CCYY_N + MM_N + DD_N;
		public static final int WS_CORP_DATE9 = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
