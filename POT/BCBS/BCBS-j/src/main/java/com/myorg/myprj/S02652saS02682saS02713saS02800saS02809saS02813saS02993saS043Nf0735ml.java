/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.myorg.myprj.commons.data.to.IS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043;
import com.myorg.myprj.ws.Nf0735mlData;

/**Original name: S02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Nf0735ml<br>*/
public class S02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Nf0735ml
		implements IS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043 {

	//==== PROPERTIES ====
	private Nf0735mlData ws;

	//==== CONSTRUCTORS ====
	public S02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Nf0735ml(Nf0735mlData ws) {
		this.ws = ws;
	}

	//==== METHODS ====
	@Override
	public char getActInactCd() {
		return ws.getDcls04315sa().getActInactCd();
	}

	@Override
	public void setActInactCd(char actInactCd) {
		this.ws.getDcls04315sa().setActInactCd(actInactCd);
	}

	@Override
	public short getAddnlRmtLiId() {
		return ws.getDcls02809sa().getAddnlRmtLiId();
	}

	@Override
	public void setAddnlRmtLiId(short addnlRmtLiId) {
		this.ws.getDcls02809sa().setAddnlRmtLiId(addnlRmtLiId);
	}

	@Override
	public char getAdjRespCd() {
		return ws.getDcls02813sa().getAdjRespCd();
	}

	@Override
	public void setAdjRespCd(char adjRespCd) {
		this.ws.getDcls02813sa().setAdjRespCd(adjRespCd);
	}

	@Override
	public String getAdjTrckCd() {
		return ws.getDcls02813sa().getAdjTrckCd();
	}

	@Override
	public void setAdjTrckCd(String adjTrckCd) {
		this.ws.getDcls02813sa().setAdjTrckCd(adjTrckCd);
	}

	@Override
	public char getAdjTypCd() {
		return ws.getDcls02813sa().getAdjTypCd();
	}

	@Override
	public void setAdjTypCd(char adjTypCd) {
		this.ws.getDcls02813sa().setAdjTypCd(adjTypCd);
	}

	@Override
	public String getAdjdOvrd10Cd() {
		return ws.getDcls02809sa().getAdjdOvrd10Cd();
	}

	@Override
	public void setAdjdOvrd10Cd(String adjdOvrd10Cd) {
		this.ws.getDcls02809sa().setAdjdOvrd10Cd(adjdOvrd10Cd);
	}

	@Override
	public String getAdjdOvrd1Cd() {
		return ws.getDcls02809sa().getAdjdOvrd1Cd();
	}

	@Override
	public void setAdjdOvrd1Cd(String adjdOvrd1Cd) {
		this.ws.getDcls02809sa().setAdjdOvrd1Cd(adjdOvrd1Cd);
	}

	@Override
	public String getAdjdOvrd2Cd() {
		return ws.getDcls02809sa().getAdjdOvrd2Cd();
	}

	@Override
	public void setAdjdOvrd2Cd(String adjdOvrd2Cd) {
		this.ws.getDcls02809sa().setAdjdOvrd2Cd(adjdOvrd2Cd);
	}

	@Override
	public String getAdjdOvrd3Cd() {
		return ws.getDcls02809sa().getAdjdOvrd3Cd();
	}

	@Override
	public void setAdjdOvrd3Cd(String adjdOvrd3Cd) {
		this.ws.getDcls02809sa().setAdjdOvrd3Cd(adjdOvrd3Cd);
	}

	@Override
	public String getAdjdOvrd4Cd() {
		return ws.getDcls02809sa().getAdjdOvrd4Cd();
	}

	@Override
	public void setAdjdOvrd4Cd(String adjdOvrd4Cd) {
		this.ws.getDcls02809sa().setAdjdOvrd4Cd(adjdOvrd4Cd);
	}

	@Override
	public String getAdjdOvrd5Cd() {
		return ws.getDcls02809sa().getAdjdOvrd5Cd();
	}

	@Override
	public void setAdjdOvrd5Cd(String adjdOvrd5Cd) {
		this.ws.getDcls02809sa().setAdjdOvrd5Cd(adjdOvrd5Cd);
	}

	@Override
	public String getAdjdOvrd6Cd() {
		return ws.getDcls02809sa().getAdjdOvrd6Cd();
	}

	@Override
	public void setAdjdOvrd6Cd(String adjdOvrd6Cd) {
		this.ws.getDcls02809sa().setAdjdOvrd6Cd(adjdOvrd6Cd);
	}

	@Override
	public String getAdjdOvrd7Cd() {
		return ws.getDcls02809sa().getAdjdOvrd7Cd();
	}

	@Override
	public void setAdjdOvrd7Cd(String adjdOvrd7Cd) {
		this.ws.getDcls02809sa().setAdjdOvrd7Cd(adjdOvrd7Cd);
	}

	@Override
	public String getAdjdOvrd8Cd() {
		return ws.getDcls02809sa().getAdjdOvrd8Cd();
	}

	@Override
	public void setAdjdOvrd8Cd(String adjdOvrd8Cd) {
		this.ws.getDcls02809sa().setAdjdOvrd8Cd(adjdOvrd8Cd);
	}

	@Override
	public String getAdjdOvrd9Cd() {
		return ws.getDcls02809sa().getAdjdOvrd9Cd();
	}

	@Override
	public void setAdjdOvrd9Cd(String adjdOvrd9Cd) {
		this.ws.getDcls02809sa().setAdjdOvrd9Cd(adjdOvrd9Cd);
	}

	@Override
	public String getAdjdProcCd() {
		return ws.getDcls04315sa().getAdjdProcCd();
	}

	@Override
	public void setAdjdProcCd(String adjdProcCd) {
		this.ws.getDcls04315sa().setAdjdProcCd(adjdProcCd);
	}

	@Override
	public String getAdjdProcMod1Cd() {
		return ws.getDcls04315sa().getAdjdProcMod1Cd();
	}

	@Override
	public void setAdjdProcMod1Cd(String adjdProcMod1Cd) {
		this.ws.getDcls04315sa().setAdjdProcMod1Cd(adjdProcMod1Cd);
	}

	@Override
	public String getAdjdProcMod2Cd() {
		return ws.getDcls04315sa().getAdjdProcMod2Cd();
	}

	@Override
	public void setAdjdProcMod2Cd(String adjdProcMod2Cd) {
		this.ws.getDcls04315sa().setAdjdProcMod2Cd(adjdProcMod2Cd);
	}

	@Override
	public String getAdjdProcMod3Cd() {
		return ws.getDcls04315sa().getAdjdProcMod3Cd();
	}

	@Override
	public void setAdjdProcMod3Cd(String adjdProcMod3Cd) {
		this.ws.getDcls04315sa().setAdjdProcMod3Cd(adjdProcMod3Cd);
	}

	@Override
	public String getAdjdProcMod4Cd() {
		return ws.getDcls04315sa().getAdjdProcMod4Cd();
	}

	@Override
	public void setAdjdProcMod4Cd(String adjdProcMod4Cd) {
		this.ws.getDcls04315sa().setAdjdProcMod4Cd(adjdProcMod4Cd);
	}

	@Override
	public AfDecimal getChgAm() {
		return ws.getDcls02809sa().getChgAm();
	}

	@Override
	public void setChgAm(AfDecimal chgAm) {
		this.ws.getDcls02809sa().setChgAm(chgAm.copy());
	}

	@Override
	public short getCliId() {
		return ws.getDcls02809sa().getCliId();
	}

	@Override
	public void setCliId(short cliId) {
		this.ws.getDcls02809sa().setCliId(cliId);
	}

	@Override
	public String getClmAdjGrpCd() {
		return ws.getDcls02800sa().getClmAdjGrpCd();
	}

	@Override
	public void setClmAdjGrpCd(String clmAdjGrpCd) {
		this.ws.getDcls02800sa().setClmAdjGrpCd(clmAdjGrpCd);
	}

	@Override
	public String getClmAdjRsnCd() {
		return ws.getDcls02800sa().getClmAdjRsnCd();
	}

	@Override
	public void setClmAdjRsnCd(String clmAdjRsnCd) {
		this.ws.getDcls02800sa().setClmAdjRsnCd(clmAdjRsnCd);
	}

	@Override
	public String getClmCntlId() {
		return ws.getDcls02813sa().getClmCntlId();
	}

	@Override
	public void setClmCntlId(String clmCntlId) {
		this.ws.getDcls02813sa().setClmCntlId(clmCntlId);
	}

	@Override
	public char getClmCntlSfxId() {
		return ws.getDcls02813sa().getClmCntlSfxId();
	}

	@Override
	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.ws.getDcls02813sa().setClmCntlSfxId(clmCntlSfxId);
	}

	@Override
	public char getClmFreqTypCd() {
		return ws.getDcls02652sa().getClmFreqTypCd();
	}

	@Override
	public void setClmFreqTypCd(char clmFreqTypCd) {
		this.ws.getDcls02652sa().setClmFreqTypCd(clmFreqTypCd);
	}

	@Override
	public AfDecimal getClmPdAm() {
		return ws.getDcls02813sa().getClmPdAm();
	}

	@Override
	public void setClmPdAm(AfDecimal clmPdAm) {
		this.ws.getDcls02813sa().setClmPdAm(clmPdAm.copy());
	}

	@Override
	public short getClmPmtId() {
		return ws.getDcls02813sa().getClmPmtId();
	}

	@Override
	public void setClmPmtId(short clmPmtId) {
		this.ws.getDcls02813sa().setClmPmtId(clmPmtId);
	}

	@Override
	public String getClmSbmtId() {
		return ws.getDcls02652sa().getClmSbmtId();
	}

	@Override
	public void setClmSbmtId(String clmSbmtId) {
		this.ws.getDcls02652sa().setClmSbmtId(clmSbmtId);
	}

	@Override
	public short getCrsRefCliId() {
		return ws.getDcls04315sa().getCrsRefCliId();
	}

	@Override
	public void setCrsRefCliId(short crsRefCliId) {
		this.ws.getDcls04315sa().setCrsRefCliId(crsRefCliId);
	}

	@Override
	public short getCrsRefClmPmtId() {
		return ws.getDcls04315sa().getCrsRefClmPmtId();
	}

	@Override
	public void setCrsRefClmPmtId(short crsRefClmPmtId) {
		this.ws.getDcls04315sa().setCrsRefClmPmtId(crsRefClmPmtId);
	}

	@Override
	public char getCrsRefRsnCd() {
		return ws.getDcls04315sa().getCrsRefRsnCd();
	}

	@Override
	public void setCrsRefRsnCd(char crsRefRsnCd) {
		this.ws.getDcls04315sa().setCrsRefRsnCd(crsRefRsnCd);
	}

	@Override
	public String getCrsRfClmCntlId() {
		return ws.getDcls04315sa().getCrsRfClmCntlId();
	}

	@Override
	public void setCrsRfClmCntlId(String crsRfClmCntlId) {
		this.ws.getDcls04315sa().setCrsRfClmCntlId(crsRfClmCntlId);
	}

	@Override
	public char getCsRfClmClSxId() {
		return ws.getDcls04315sa().getCsRfClmClSxId();
	}

	@Override
	public void setCsRfClmClSxId(char csRfClmClSxId) {
		this.ws.getDcls04315sa().setCsRfClmClSxId(csRfClmClSxId);
	}

	@Override
	public String getDnlRmrkCd() {
		return ws.getDcls02809sa().getDnlRmrkCd();
	}

	@Override
	public void setDnlRmrkCd(String dnlRmrkCd) {
		this.ws.getDcls02809sa().setDnlRmrkCd(dnlRmrkCd);
	}

	@Override
	public char getDnlRmrkUsgCd() {
		return ws.getDcls02993sa().getDnlRmrkUsgCd();
	}

	@Override
	public void setDnlRmrkUsgCd(char dnlRmrkUsgCd) {
		this.ws.getDcls02993sa().setDnlRmrkUsgCd(dnlRmrkUsgCd);
	}

	@Override
	public String getDrgCd() {
		return ws.getDcls02813sa().getDrgCd();
	}

	@Override
	public void setDrgCd(String drgCd) {
		this.ws.getDcls02813sa().setDrgCd(drgCd);
	}

	@Override
	public String getEnrClCd() {
		return ws.getDcls02813sa().getEnrClCd();
	}

	@Override
	public void setEnrClCd(String enrClCd) {
		this.ws.getDcls02813sa().setEnrClCd(enrClCd);
	}

	@Override
	public char getExmnActnCd() {
		return ws.getDcls02809sa().getExmnActnCd();
	}

	@Override
	public void setExmnActnCd(char exmnActnCd) {
		this.ws.getDcls02809sa().setExmnActnCd(exmnActnCd);
	}

	@Override
	public String getFacVlCd() {
		return ws.getDcls02652sa().getFacVlCd();
	}

	@Override
	public void setFacVlCd(String facVlCd) {
		this.ws.getDcls02652sa().setFacVlCd(facVlCd);
	}

	@Override
	public String getFinCd() {
		return ws.getDcls02813sa().getFinCd();
	}

	@Override
	public void setFinCd(String finCd) {
		this.ws.getDcls02813sa().setFinCd(finCd);
	}

	@Override
	public String getFrstNm() {
		return ws.getDcls02682sa().getFrstNm();
	}

	@Override
	public void setFrstNm(String frstNm) {
		this.ws.getDcls02682sa().setFrstNm(frstNm);
	}

	@Override
	public String getGlOfstVoidCd() {
		return ws.getDcls02813sa().getGlOfstVoidCd();
	}

	@Override
	public void setGlOfstVoidCd(String glOfstVoidCd) {
		this.ws.getDcls02813sa().setGlOfstVoidCd(glOfstVoidCd);
	}

	@Override
	public short getGlSoteVoidCd() {
		return ws.getDcls02813sa().getGlSoteVoidCd();
	}

	@Override
	public void setGlSoteVoidCd(short glSoteVoidCd) {
		this.ws.getDcls02813sa().setGlSoteVoidCd(glSoteVoidCd);
	}

	@Override
	public char getHistLoadCd() {
		return ws.getDcls02813sa().getHistLoadCd();
	}

	@Override
	public void setHistLoadCd(char histLoadCd) {
		this.ws.getDcls02813sa().setHistLoadCd(histLoadCd);
	}

	@Override
	public String getIdCd() {
		return ws.getDcls02682sa().getIdCd();
	}

	@Override
	public void setIdCd(String idCd) {
		this.ws.getDcls02682sa().setIdCd(idCd);
	}

	@Override
	public char getItsClmTypCd() {
		return ws.getDcls02813sa().getItsClmTypCd();
	}

	@Override
	public void setItsClmTypCd(char itsClmTypCd) {
		this.ws.getDcls02813sa().setItsClmTypCd(itsClmTypCd);
	}

	@Override
	public String getKcapsTeamNm() {
		return ws.getDcls02813sa().getKcapsTeamNm();
	}

	@Override
	public void setKcapsTeamNm(String kcapsTeamNm) {
		this.ws.getDcls02813sa().setKcapsTeamNm(kcapsTeamNm);
	}

	@Override
	public String getKcapsUseId() {
		return ws.getDcls02813sa().getKcapsUseId();
	}

	@Override
	public void setKcapsUseId(String kcapsUseId) {
		this.ws.getDcls02813sa().setKcapsUseId(kcapsUseId);
	}

	@Override
	public AfDecimal getLiAlctOiPdAm() {
		return ws.getDcls02809sa().getLiAlctOiPdAm();
	}

	@Override
	public void setLiAlctOiPdAm(AfDecimal liAlctOiPdAm) {
		this.ws.getDcls02809sa().setLiAlctOiPdAm(liAlctOiPdAm.copy());
	}

	@Override
	public String getLiFrmServDt() {
		return ws.getDcls02809sa().getLiFrmServDt();
	}

	@Override
	public void setLiFrmServDt(String liFrmServDt) {
		this.ws.getDcls02809sa().setLiFrmServDt(liFrmServDt);
	}

	@Override
	public String getLiThrServDt() {
		return ws.getDcls02809sa().getLiThrServDt();
	}

	@Override
	public void setLiThrServDt(String liThrServDt) {
		this.ws.getDcls02809sa().setLiThrServDt(liThrServDt);
	}

	@Override
	public char getLobCd() {
		return ws.getDcls02813sa().getLobCd();
	}

	@Override
	public void setLobCd(char lobCd) {
		this.ws.getDcls02813sa().setLobCd(lobCd);
	}

	@Override
	public String getLstOrgNm() {
		return ws.getDcls02682sa().getLstOrgNm();
	}

	@Override
	public void setLstOrgNm(String lstOrgNm) {
		this.ws.getDcls02682sa().setLstOrgNm(lstOrgNm);
	}

	@Override
	public String getMemId() {
		return ws.getDcls02813sa().getMemId();
	}

	@Override
	public void setMemId(String memId) {
		this.ws.getDcls02813sa().setMemId(memId);
	}

	@Override
	public String getMidNm() {
		return ws.getDcls02682sa().getMidNm();
	}

	@Override
	public void setMidNm(String midNm) {
		this.ws.getDcls02682sa().setMidNm(midNm);
	}

	@Override
	public AfDecimal getMntryAm() {
		return ws.getDcls02800sa().getMntryAm();
	}

	@Override
	public void setMntryAm(AfDecimal mntryAm) {
		this.ws.getDcls02800sa().setMntryAm(mntryAm.copy());
	}

	@Override
	public String getMrktPkgCd() {
		return ws.getDcls02813sa().getMrktPkgCd();
	}

	@Override
	public void setMrktPkgCd(String mrktPkgCd) {
		this.ws.getDcls02813sa().setMrktPkgCd(mrktPkgCd);
	}

	@Override
	public char getNpiCd() {
		return ws.getDcls02813sa().getNpiCd();
	}

	@Override
	public void setNpiCd(char npiCd) {
		this.ws.getDcls02813sa().setNpiCd(npiCd);
	}

	@Override
	public String getNtwrkCd() {
		return ws.getDcls02813sa().getNtwrkCd();
	}

	@Override
	public void setNtwrkCd(String ntwrkCd) {
		this.ws.getDcls02813sa().setNtwrkCd(ntwrkCd);
	}

	@Override
	public AfDecimal getOrigSbmtChgAm() {
		return ws.getDcls02809sa().getOrigSbmtChgAm();
	}

	@Override
	public void setOrigSbmtChgAm(AfDecimal origSbmtChgAm) {
		this.ws.getDcls02809sa().setOrigSbmtChgAm(origSbmtChgAm.copy());
	}

	@Override
	public AfDecimal getOrigSbmtUnitCt() {
		return ws.getDcls02809sa().getOrigSbmtUnitCt();
	}

	@Override
	public void setOrigSbmtUnitCt(AfDecimal origSbmtUnitCt) {
		this.ws.getDcls02809sa().setOrigSbmtUnitCt(origSbmtUnitCt.copy());
	}

	@Override
	public String getPgmAreaCd() {
		return ws.getDcls02813sa().getPgmAreaCd();
	}

	@Override
	public void setPgmAreaCd(String pgmAreaCd) {
		this.ws.getDcls02813sa().setPgmAreaCd(pgmAreaCd);
	}

	@Override
	public AfDecimal getPrmptPayIntAm() {
		return ws.getDcls02813sa().getPrmptPayIntAm();
	}

	@Override
	public void setPrmptPayIntAm(AfDecimal prmptPayIntAm) {
		this.ws.getDcls02813sa().setPrmptPayIntAm(prmptPayIntAm.copy());
	}

	@Override
	public String getProcCd() {
		return ws.getDcls02809sa().getProcCd();
	}

	@Override
	public void setProcCd(String procCd) {
		this.ws.getDcls02809sa().setProcCd(procCd);
	}

	@Override
	public String getProcMod1Cd() {
		return ws.getDcls02809sa().getProcMod1Cd();
	}

	@Override
	public void setProcMod1Cd(String procMod1Cd) {
		this.ws.getDcls02809sa().setProcMod1Cd(procMod1Cd);
	}

	@Override
	public String getProcMod2Cd() {
		return ws.getDcls02809sa().getProcMod2Cd();
	}

	@Override
	public void setProcMod2Cd(String procMod2Cd) {
		this.ws.getDcls02809sa().setProcMod2Cd(procMod2Cd);
	}

	@Override
	public String getProcMod3Cd() {
		return ws.getDcls02809sa().getProcMod3Cd();
	}

	@Override
	public void setProcMod3Cd(String procMod3Cd) {
		this.ws.getDcls02809sa().setProcMod3Cd(procMod3Cd);
	}

	@Override
	public String getProcMod4Cd() {
		return ws.getDcls02809sa().getProcMod4Cd();
	}

	@Override
	public void setProcMod4Cd(String procMod4Cd) {
		this.ws.getDcls02809sa().setProcMod4Cd(procMod4Cd);
	}

	@Override
	public short getProvSbmtLnNoId() {
		return ws.getDcls02809sa().getProvSbmtLnNoId();
	}

	@Override
	public void setProvSbmtLnNoId(short provSbmtLnNoId) {
		this.ws.getDcls02809sa().setProvSbmtLnNoId(provSbmtLnNoId);
	}

	@Override
	public AfDecimal getQntyCt() {
		return ws.getDcls02800sa().getQntyCt();
	}

	@Override
	public void setQntyCt(AfDecimal qntyCt) {
		this.ws.getDcls02800sa().setQntyCt(qntyCt.copy());
	}

	@Override
	public String getRefId() {
		return ws.getDcls02713sa().getRefId();
	}

	@Override
	public void setRefId(String refId) {
		this.ws.getDcls02713sa().setRefId(refId);
	}

	@Override
	public String getRevCd() {
		return ws.getDcls02809sa().getRevCd();
	}

	@Override
	public void setRevCd(String revCd) {
		this.ws.getDcls02809sa().setRevCd(revCd);
	}

	@Override
	public AfDecimal getSchdlDrgAlwAm() {
		return ws.getDcls02813sa().getSchdlDrgAlwAm();
	}

	@Override
	public void setSchdlDrgAlwAm(AfDecimal schdlDrgAlwAm) {
		this.ws.getDcls02813sa().setSchdlDrgAlwAm(schdlDrgAlwAm.copy());
	}

	@Override
	public String getSfxNm() {
		return ws.getDcls02682sa().getSfxNm();
	}

	@Override
	public void setSfxNm(String sfxNm) {
		this.ws.getDcls02682sa().setSfxNm(sfxNm);
	}

	@Override
	public String getTrig837BillProvNpiId() {
		return ws.getTrigRecordLayout().getTrig837BillProvNpiId();
	}

	@Override
	public void setTrig837BillProvNpiId(String trig837BillProvNpiId) {
		this.ws.getTrigRecordLayout().setTrig837BillProvNpiId(trig837BillProvNpiId);
	}

	@Override
	public String getTrig837PerfProvNpiId() {
		return ws.getTrigRecordLayout().getTrig837PerfProvNpiId();
	}

	@Override
	public void setTrig837PerfProvNpiId(String trig837PerfProvNpiId) {
		this.ws.getTrigRecordLayout().setTrig837PerfProvNpiId(trig837PerfProvNpiId);
	}

	@Override
	public char getTrigAdjRespCd() {
		return ws.getTrigRecordLayout().getTrigAdjRespCd();
	}

	@Override
	public void setTrigAdjRespCd(char trigAdjRespCd) {
		this.ws.getTrigRecordLayout().setTrigAdjRespCd(trigAdjRespCd);
	}

	@Override
	public String getTrigAdjTrckCd() {
		return ws.getTrigRecordLayout().getTrigAdjTrckCd();
	}

	@Override
	public void setTrigAdjTrckCd(String trigAdjTrckCd) {
		this.ws.getTrigRecordLayout().setTrigAdjTrckCd(trigAdjTrckCd);
	}

	@Override
	public char getTrigAdjTypCd() {
		return ws.getTrigRecordLayout().getTrigAdjTypCd();
	}

	@Override
	public void setTrigAdjTypCd(char trigAdjTypCd) {
		this.ws.getTrigRecordLayout().setTrigAdjTypCd(trigAdjTypCd);
	}

	@Override
	public char getTrigAdjdProvStatCd() {
		return ws.getTrigRecordLayout().getTrigAdjdProvStatCd();
	}

	@Override
	public void setTrigAdjdProvStatCd(char trigAdjdProvStatCd) {
		this.ws.getTrigRecordLayout().setTrigAdjdProvStatCd(trigAdjdProvStatCd);
	}

	@Override
	public String getTrigAlphPrfxCd() {
		return ws.getTrigRecordLayout().getTrigAlphPrfxCd();
	}

	@Override
	public void setTrigAlphPrfxCd(String trigAlphPrfxCd) {
		this.ws.getTrigRecordLayout().setTrigAlphPrfxCd(trigAlphPrfxCd);
	}

	@Override
	public String getTrigAsgCd() {
		return ws.getTrigRecordLayout().getTrigAsgCd();
	}

	@Override
	public void setTrigAsgCd(String trigAsgCd) {
		this.ws.getTrigRecordLayout().setTrigAsgCd(trigAsgCd);
	}

	@Override
	public String getTrigBaseCnArngCd() {
		return ws.getTrigRecordLayout().getTrigBaseCnArngCd();
	}

	@Override
	public void setTrigBaseCnArngCd(String trigBaseCnArngCd) {
		this.ws.getTrigRecordLayout().setTrigBaseCnArngCd(trigBaseCnArngCd);
	}

	@Override
	public String getTrigBillFrmDt() {
		return ws.getTrigRecordLayout().getTrigBillFrmDt();
	}

	@Override
	public void setTrigBillFrmDt(String trigBillFrmDt) {
		this.ws.getTrigRecordLayout().setTrigBillFrmDt(trigBillFrmDt);
	}

	@Override
	public String getTrigBillProvIdQlfCd() {
		return ws.getTrigRecordLayout().getTrigBillProvIdQlfCd();
	}

	@Override
	public void setTrigBillProvIdQlfCd(String trigBillProvIdQlfCd) {
		this.ws.getTrigRecordLayout().setTrigBillProvIdQlfCd(trigBillProvIdQlfCd);
	}

	@Override
	public String getTrigBillProvSpecCd() {
		return ws.getTrigRecordLayout().getTrigBillProvSpecCd();
	}

	@Override
	public void setTrigBillProvSpecCd(String trigBillProvSpecCd) {
		this.ws.getTrigRecordLayout().setTrigBillProvSpecCd(trigBillProvSpecCd);
	}

	@Override
	public String getTrigBillThrDt() {
		return ws.getTrigRecordLayout().getTrigBillThrDt();
	}

	@Override
	public void setTrigBillThrDt(String trigBillThrDt) {
		this.ws.getTrigRecordLayout().setTrigBillThrDt(trigBillThrDt);
	}

	@Override
	public char getTrigClaimLobCd() {
		return ws.getTrigRecordLayout().getTrigClaimLobCd();
	}

	@Override
	public void setTrigClaimLobCd(char trigClaimLobCd) {
		this.ws.getTrigRecordLayout().setTrigClaimLobCd(trigClaimLobCd);
	}

	@Override
	public char getTrigClmckAdjStatCd() {
		return ws.getTrigRecordLayout().getTrigClmckAdjStatCd();
	}

	@Override
	public void setTrigClmckAdjStatCd(char trigClmckAdjStatCd) {
		this.ws.getTrigRecordLayout().setTrigClmckAdjStatCd(trigClmckAdjStatCd);
	}

	@Override
	public String getTrigCorpRcvDt() {
		return ws.getTrigRecordLayout().getTrigCorpRcvDt();
	}

	@Override
	public void setTrigCorpRcvDt(String trigCorpRcvDt) {
		this.ws.getTrigRecordLayout().setTrigCorpRcvDt(trigCorpRcvDt);
	}

	@Override
	public String getTrigCorrPrioritySubId() {
		return ws.getTrigRecordLayout().getTrigCorrPrioritySubId();
	}

	@Override
	public void setTrigCorrPrioritySubId(String trigCorrPrioritySubId) {
		this.ws.getTrigRecordLayout().setTrigCorrPrioritySubId(trigCorrPrioritySubId);
	}

	@Override
	public String getTrigDateCoverageLapsed() {
		return ws.getTrigRecordLayout().getTrigDateCoverageLapsed();
	}

	@Override
	public void setTrigDateCoverageLapsed(String trigDateCoverageLapsed) {
		this.ws.getTrigRecordLayout().setTrigDateCoverageLapsed(trigDateCoverageLapsed);
	}

	@Override
	public String getTrigDrgCd() {
		return ws.getTrigRecordLayout().getTrigDrgCd();
	}

	@Override
	public void setTrigDrgCd(String trigDrgCd) {
		this.ws.getTrigRecordLayout().setTrigDrgCd(trigDrgCd);
	}

	@Override
	public char getTrigEftAccountType() {
		return ws.getTrigRecordLayout().getTrigEftAccountType();
	}

	@Override
	public void setTrigEftAccountType(char trigEftAccountType) {
		this.ws.getTrigRecordLayout().setTrigEftAccountType(trigEftAccountType);
	}

	@Override
	public String getTrigEftAcct() {
		return ws.getTrigRecordLayout().getTrigEftAcct();
	}

	@Override
	public void setTrigEftAcct(String trigEftAcct) {
		this.ws.getTrigRecordLayout().setTrigEftAcct(trigEftAcct);
	}

	@Override
	public char getTrigEftInd() {
		return ws.getTrigRecordLayout().getTrigEftInd();
	}

	@Override
	public void setTrigEftInd(char trigEftInd) {
		this.ws.getTrigRecordLayout().setTrigEftInd(trigEftInd);
	}

	@Override
	public String getTrigEftTrans() {
		return ws.getTrigRecordLayout().getTrigEftTrans();
	}

	@Override
	public void setTrigEftTrans(String trigEftTrans) {
		this.ws.getTrigRecordLayout().setTrigEftTrans(trigEftTrans);
	}

	@Override
	public String getTrigEnrClCd() {
		return ws.getTrigRecordLayout().getTrigEnrClCd();
	}

	@Override
	public void setTrigEnrClCd(String trigEnrClCd) {
		this.ws.getTrigRecordLayout().setTrigEnrClCd(trigEnrClCd);
	}

	@Override
	public String getTrigFinCd() {
		return ws.getTrigRecordLayout().getTrigFinCd();
	}

	@Override
	public void setTrigFinCd(String trigFinCd) {
		this.ws.getTrigRecordLayout().setTrigFinCd(trigFinCd);
	}

	@Override
	public String getTrigGlOfstOrigCd() {
		return ws.getTrigRecordLayout().getTrigGlOfstOrigCd();
	}

	@Override
	public void setTrigGlOfstOrigCd(String trigGlOfstOrigCd) {
		this.ws.getTrigRecordLayout().setTrigGlOfstOrigCd(trigGlOfstOrigCd);
	}

	@Override
	public String getTrigGmisIndicator() {
		return ws.getTrigRecordLayout().getTrigGmisIndicator();
	}

	@Override
	public void setTrigGmisIndicator(String trigGmisIndicator) {
		this.ws.getTrigRecordLayout().setTrigGmisIndicator(trigGmisIndicator);
	}

	@Override
	public String getTrigGrpId() {
		return ws.getTrigRecordLayout().getTrigGrpId();
	}

	@Override
	public void setTrigGrpId(String trigGrpId) {
		this.ws.getTrigRecordLayout().setTrigGrpId(trigGrpId);
	}

	@Override
	public String getTrigHipaaVersionFormatId() {
		return ws.getTrigRecordLayout().getTrigHipaaVersionFormatId();
	}

	@Override
	public void setTrigHipaaVersionFormatId(String trigHipaaVersionFormatId) {
		this.ws.getTrigRecordLayout().setTrigHipaaVersionFormatId(trigHipaaVersionFormatId);
	}

	@Override
	public String getTrigHistBillProvNpiId() {
		return ws.getTrigRecordLayout().getTrigHistBillProvNpiId();
	}

	@Override
	public void setTrigHistBillProvNpiId(String trigHistBillProvNpiId) {
		this.ws.getTrigRecordLayout().setTrigHistBillProvNpiId(trigHistBillProvNpiId);
	}

	@Override
	public char getTrigHistLoadCd() {
		return ws.getTrigRecordLayout().getTrigHistLoadCd();
	}

	@Override
	public void setTrigHistLoadCd(char trigHistLoadCd) {
		this.ws.getTrigRecordLayout().setTrigHistLoadCd(trigHistLoadCd);
	}

	@Override
	public String getTrigHistPerfProvNpiId() {
		return ws.getTrigRecordLayout().getTrigHistPerfProvNpiId();
	}

	@Override
	public void setTrigHistPerfProvNpiId(String trigHistPerfProvNpiId) {
		this.ws.getTrigRecordLayout().setTrigHistPerfProvNpiId(trigHistPerfProvNpiId);
	}

	@Override
	public String getTrigInsId() {
		return ws.getTrigRecordLayout().getTrigInsId();
	}

	@Override
	public void setTrigInsId(String trigInsId) {
		this.ws.getTrigRecordLayout().setTrigInsId(trigInsId);
	}

	@Override
	public char getTrigItsCk() {
		return ws.getTrigRecordLayout().getTrigItsCk();
	}

	@Override
	public void setTrigItsCk(char trigItsCk) {
		this.ws.getTrigRecordLayout().setTrigItsCk(trigItsCk);
	}

	@Override
	public char getTrigItsClmTypCd() {
		return ws.getTrigRecordLayout().getTrigItsClmTypCd();
	}

	@Override
	public void setTrigItsClmTypCd(char trigItsClmTypCd) {
		this.ws.getTrigRecordLayout().setTrigItsClmTypCd(trigItsClmTypCd);
	}

	@Override
	public String getTrigItsInsId() {
		return ws.getTrigRecordLayout().getTrigItsInsId();
	}

	@Override
	public void setTrigItsInsId(String trigItsInsId) {
		this.ws.getTrigRecordLayout().setTrigItsInsId(trigItsInsId);
	}

	@Override
	public String getTrigKcapsTeamNm() {
		return ws.getTrigRecordLayout().getTrigKcapsTeamNm();
	}

	@Override
	public void setTrigKcapsTeamNm(String trigKcapsTeamNm) {
		this.ws.getTrigRecordLayout().setTrigKcapsTeamNm(trigKcapsTeamNm);
	}

	@Override
	public String getTrigKcapsUseId() {
		return ws.getTrigRecordLayout().getTrigKcapsUseId();
	}

	@Override
	public void setTrigKcapsUseId(String trigKcapsUseId) {
		this.ws.getTrigRecordLayout().setTrigKcapsUseId(trigKcapsUseId);
	}

	@Override
	public String getTrigLocalBillProvId() {
		return ws.getTrigRecordLayout().getTrigLocalBillProvId();
	}

	@Override
	public void setTrigLocalBillProvId(String trigLocalBillProvId) {
		this.ws.getTrigRecordLayout().setTrigLocalBillProvId(trigLocalBillProvId);
	}

	@Override
	public char getTrigLocalBillProvLobCd() {
		return ws.getTrigRecordLayout().getTrigLocalBillProvLobCd();
	}

	@Override
	public void setTrigLocalBillProvLobCd(char trigLocalBillProvLobCd) {
		this.ws.getTrigRecordLayout().setTrigLocalBillProvLobCd(trigLocalBillProvLobCd);
	}

	@Override
	public String getTrigLocalPerfProvId() {
		return ws.getTrigRecordLayout().getTrigLocalPerfProvId();
	}

	@Override
	public void setTrigLocalPerfProvId(String trigLocalPerfProvId) {
		this.ws.getTrigRecordLayout().setTrigLocalPerfProvId(trigLocalPerfProvId);
	}

	@Override
	public char getTrigLocalPerfProvLobCd() {
		return ws.getTrigRecordLayout().getTrigLocalPerfProvLobCd();
	}

	@Override
	public void setTrigLocalPerfProvLobCd(char trigLocalPerfProvLobCd) {
		this.ws.getTrigRecordLayout().setTrigLocalPerfProvLobCd(trigLocalPerfProvLobCd);
	}

	@Override
	public String getTrigMemberId() {
		return ws.getTrigRecordLayout().getTrigMemberId();
	}

	@Override
	public void setTrigMemberId(String trigMemberId) {
		this.ws.getTrigRecordLayout().setTrigMemberId(trigMemberId);
	}

	@Override
	public char getTrigNpiCd() {
		return ws.getProgramHoldAreas().getTrigNpiCd();
	}

	@Override
	public void setTrigNpiCd(char trigNpiCd) {
		this.ws.getProgramHoldAreas().setTrigNpiCd(trigNpiCd);
	}

	@Override
	public String getTrigNtwrkCd() {
		return ws.getTrigRecordLayout().getTrigNtwrkCd();
	}

	@Override
	public void setTrigNtwrkCd(String trigNtwrkCd) {
		this.ws.getTrigRecordLayout().setTrigNtwrkCd(trigNtwrkCd);
	}

	@Override
	public String getTrigOiPayNm() {
		return ws.getTrigRecordLayout().getTrigOiPayNm();
	}

	@Override
	public void setTrigOiPayNm(String trigOiPayNm) {
		this.ws.getTrigRecordLayout().setTrigOiPayNm(trigOiPayNm);
	}

	@Override
	public String getTrigPatActMedRecId() {
		return ws.getTrigRecordLayout().getTrigPatActMedRecId();
	}

	@Override
	public void setTrigPatActMedRecId(String trigPatActMedRecId) {
		this.ws.getTrigRecordLayout().setTrigPatActMedRecId(trigPatActMedRecId);
	}

	@Override
	public String getTrigPgmAreaCd() {
		return ws.getTrigRecordLayout().getTrigPgmAreaCd();
	}

	@Override
	public void setTrigPgmAreaCd(String trigPgmAreaCd) {
		this.ws.getTrigRecordLayout().setTrigPgmAreaCd(trigPgmAreaCd);
	}

	@Override
	public String getTrigPmtAdjdDt() {
		return ws.getTrigRecordLayout().getTrigPmtAdjdDt();
	}

	@Override
	public void setTrigPmtAdjdDt(String trigPmtAdjdDt) {
		this.ws.getTrigRecordLayout().setTrigPmtAdjdDt(trigPmtAdjdDt);
	}

	@Override
	public char getTrigPmtBkProdCd() {
		return ws.getTrigRecordLayout().getTrigPmtBkProdCd();
	}

	@Override
	public void setTrigPmtBkProdCd(char trigPmtBkProdCd) {
		this.ws.getTrigRecordLayout().setTrigPmtBkProdCd(trigPmtBkProdCd);
	}

	@Override
	public String getTrigPmtFrmServDt() {
		return ws.getTrigRecordLayout().getTrigPmtFrmServDt();
	}

	@Override
	public void setTrigPmtFrmServDt(String trigPmtFrmServDt) {
		this.ws.getTrigRecordLayout().setTrigPmtFrmServDt(trigPmtFrmServDt);
	}

	@Override
	public char getTrigPmtOiIn() {
		return ws.getTrigRecordLayout().getTrigPmtOiIn();
	}

	@Override
	public void setTrigPmtOiIn(char trigPmtOiIn) {
		this.ws.getTrigRecordLayout().setTrigPmtOiIn(trigPmtOiIn);
	}

	@Override
	public String getTrigPmtThrServDt() {
		return ws.getTrigRecordLayout().getTrigPmtThrServDt();
	}

	@Override
	public void setTrigPmtThrServDt(String trigPmtThrServDt) {
		this.ws.getTrigRecordLayout().setTrigPmtThrServDt(trigPmtThrServDt);
	}

	@Override
	public String getTrigPrimCnArngCd() {
		return ws.getTrigRecordLayout().getTrigPrimCnArngCd();
	}

	@Override
	public void setTrigPrimCnArngCd(String trigPrimCnArngCd) {
		this.ws.getTrigRecordLayout().setTrigPrimCnArngCd(trigPrimCnArngCd);
	}

	@Override
	public String getTrigPrmptPayDayCd() {
		return ws.getTrigRecordLayout().getTrigPrmptPayDayCd();
	}

	@Override
	public void setTrigPrmptPayDayCd(String trigPrmptPayDayCd) {
		this.ws.getTrigRecordLayout().setTrigPrmptPayDayCd(trigPrmptPayDayCd);
	}

	@Override
	public char getTrigPrmptPayOvrdCd() {
		return ws.getTrigRecordLayout().getTrigPrmptPayOvrdCd();
	}

	@Override
	public void setTrigPrmptPayOvrdCd(char trigPrmptPayOvrdCd) {
		this.ws.getTrigRecordLayout().setTrigPrmptPayOvrdCd(trigPrmptPayOvrdCd);
	}

	@Override
	public String getTrigProdInd() {
		return ws.getTrigRecordLayout().getTrigProdInd();
	}

	@Override
	public void setTrigProdInd(String trigProdInd) {
		this.ws.getTrigRecordLayout().setTrigProdInd(trigProdInd);
	}

	@Override
	public String getTrigProvUnwrpDt() {
		return ws.getTrigRecordLayout().getTrigProvUnwrpDt();
	}

	@Override
	public void setTrigProvUnwrpDt(String trigProvUnwrpDt) {
		this.ws.getTrigRecordLayout().setTrigProvUnwrpDt(trigProvUnwrpDt);
	}

	@Override
	public String getTrigRateCd() {
		return ws.getTrigRecordLayout().getTrigRateCd();
	}

	@Override
	public void setTrigRateCd(String trigRateCd) {
		this.ws.getTrigRecordLayout().setTrigRateCd(trigRateCd);
	}

	@Override
	public char getTrigStatAdjPrevPmt() {
		return ws.getTrigRecordLayout().getTrigStatAdjPrevPmt();
	}

	@Override
	public void setTrigStatAdjPrevPmt(char trigStatAdjPrevPmt) {
		this.ws.getTrigRecordLayout().setTrigStatAdjPrevPmt(trigStatAdjPrevPmt);
	}

	@Override
	public String getTrigTypGrpCd() {
		return ws.getTrigRecordLayout().getTrigTypGrpCd();
	}

	@Override
	public void setTrigTypGrpCd(String trigTypGrpCd) {
		this.ws.getTrigRecordLayout().setTrigTypGrpCd(trigTypGrpCd);
	}

	@Override
	public char getTrigVoidCd() {
		return ws.getTrigRecordLayout().getTrigVoidCd();
	}

	@Override
	public void setTrigVoidCd(char trigVoidCd) {
		this.ws.getTrigRecordLayout().setTrigVoidCd(trigVoidCd);
	}

	@Override
	public String getTsCd() {
		return ws.getDcls02809sa().getTsCd();
	}

	@Override
	public void setTsCd(String tsCd) {
		this.ws.getDcls02809sa().setTsCd(tsCd);
	}

	@Override
	public String getTypGrpCd() {
		return ws.getDcls02813sa().getTypGrpCd();
	}

	@Override
	public void setTypGrpCd(String typGrpCd) {
		this.ws.getDcls02813sa().setTypGrpCd(typGrpCd);
	}

	@Override
	public AfDecimal getUnitCt() {
		return ws.getDcls02809sa().getUnitCt();
	}

	@Override
	public void setUnitCt(AfDecimal unitCt) {
		this.ws.getDcls02809sa().setUnitCt(unitCt.copy());
	}

	@Override
	public char getVbrIn() {
		return ws.getDcls02813sa().getVbrIn();
	}

	@Override
	public void setVbrIn(char vbrIn) {
		this.ws.getDcls02813sa().setVbrIn(vbrIn);
	}

	@Override
	public char getVoidCd() {
		return ws.getDcls02813sa().getVoidCd();
	}

	@Override
	public void setVoidCd(char voidCd) {
		this.ws.getDcls02813sa().setVoidCd(voidCd);
	}

	@Override
	public AfDecimal getWsAccruedPrmptPayIntAm() {
		return ws.getProgramHoldAreas().getWsAccruedPrmptPayIntAm();
	}

	@Override
	public void setWsAccruedPrmptPayIntAm(AfDecimal wsAccruedPrmptPayIntAm) {
		this.ws.getProgramHoldAreas().setWsAccruedPrmptPayIntAm(wsAccruedPrmptPayIntAm.copy());
	}

	@Override
	public AfDecimal getWsTrigAltDrgAlwAm() {
		return ws.getProgramHoldAreas().getWsTrigAltDrgAlwAm();
	}

	@Override
	public void setWsTrigAltDrgAlwAm(AfDecimal wsTrigAltDrgAlwAm) {
		this.ws.getProgramHoldAreas().setWsTrigAltDrgAlwAm(wsTrigAltDrgAlwAm.copy());
	}

	@Override
	public AfDecimal getWsTrigClmPdAm() {
		return ws.getProgramHoldAreas().getWsTrigClmPdAm();
	}

	@Override
	public void setWsTrigClmPdAm(AfDecimal wsTrigClmPdAm) {
		this.ws.getProgramHoldAreas().setWsTrigClmPdAm(wsTrigClmPdAm.copy());
	}

	@Override
	public short getWsTrigGlSoteOrigCd() {
		return ws.getProgramHoldAreas().getWsTrigGlSoteOrigCd();
	}

	@Override
	public void setWsTrigGlSoteOrigCd(short wsTrigGlSoteOrigCd) {
		this.ws.getProgramHoldAreas().setWsTrigGlSoteOrigCd(wsTrigGlSoteOrigCd);
	}

	@Override
	public short getWsTrigLstFnlPmtPtId() {
		return ws.getProgramHoldAreas().getWsTrigLstFnlPmtPtId();
	}

	@Override
	public void setWsTrigLstFnlPmtPtId(short wsTrigLstFnlPmtPtId) {
		this.ws.getProgramHoldAreas().setWsTrigLstFnlPmtPtId(wsTrigLstFnlPmtPtId);
	}

	@Override
	public AfDecimal getWsTrigOverDrgAlwAm() {
		return ws.getProgramHoldAreas().getWsTrigOverDrgAlwAm();
	}

	@Override
	public void setWsTrigOverDrgAlwAm(AfDecimal wsTrigOverDrgAlwAm) {
		this.ws.getProgramHoldAreas().setWsTrigOverDrgAlwAm(wsTrigOverDrgAlwAm.copy());
	}

	@Override
	public AfDecimal getWsTrigSchdlDrgAlwAm() {
		return ws.getProgramHoldAreas().getWsTrigSchdlDrgAlwAm();
	}

	@Override
	public void setWsTrigSchdlDrgAlwAm(AfDecimal wsTrigSchdlDrgAlwAm) {
		this.ws.getProgramHoldAreas().setWsTrigSchdlDrgAlwAm(wsTrigSchdlDrgAlwAm.copy());
	}

	@Override
	public char getWsVr4ActInactCd() {
		return ws.getProgramHoldAreas().getWsVr4ActInactCd();
	}

	@Override
	public void setWsVr4ActInactCd(char wsVr4ActInactCd) {
		this.ws.getProgramHoldAreas().setWsVr4ActInactCd(wsVr4ActInactCd);
	}

	@Override
	public String getWsVr4AdjdProcCd() {
		return ws.getProgramHoldAreas().getWsVr4AdjdProcCd();
	}

	@Override
	public void setWsVr4AdjdProcCd(String wsVr4AdjdProcCd) {
		this.ws.getProgramHoldAreas().setWsVr4AdjdProcCd(wsVr4AdjdProcCd);
	}

	@Override
	public String getWsVr4AdjdProcMod1Cd() {
		return ws.getProgramHoldAreas().getWsVr4AdjdProcMod1Cd();
	}

	@Override
	public void setWsVr4AdjdProcMod1Cd(String wsVr4AdjdProcMod1Cd) {
		this.ws.getProgramHoldAreas().setWsVr4AdjdProcMod1Cd(wsVr4AdjdProcMod1Cd);
	}

	@Override
	public String getWsVr4AdjdProcMod2Cd() {
		return ws.getProgramHoldAreas().getWsVr4AdjdProcMod2Cd();
	}

	@Override
	public void setWsVr4AdjdProcMod2Cd(String wsVr4AdjdProcMod2Cd) {
		this.ws.getProgramHoldAreas().setWsVr4AdjdProcMod2Cd(wsVr4AdjdProcMod2Cd);
	}

	@Override
	public String getWsVr4AdjdProcMod3Cd() {
		return ws.getProgramHoldAreas().getWsVr4AdjdProcMod3Cd();
	}

	@Override
	public void setWsVr4AdjdProcMod3Cd(String wsVr4AdjdProcMod3Cd) {
		this.ws.getProgramHoldAreas().setWsVr4AdjdProcMod3Cd(wsVr4AdjdProcMod3Cd);
	}

	@Override
	public String getWsVr4AdjdProcMod4Cd() {
		return ws.getProgramHoldAreas().getWsVr4AdjdProcMod4Cd();
	}

	@Override
	public void setWsVr4AdjdProcMod4Cd(String wsVr4AdjdProcMod4Cd) {
		this.ws.getProgramHoldAreas().setWsVr4AdjdProcMod4Cd(wsVr4AdjdProcMod4Cd);
	}

	@Override
	public String getWsVr4CrsRefCliId() {
		return ws.getProgramHoldAreas().getWsVr4CrsRefCliN().getWsVr4CrsRefCliId();
	}

	@Override
	public void setWsVr4CrsRefCliId(String wsVr4CrsRefCliId) {
		this.ws.getProgramHoldAreas().getWsVr4CrsRefCliN().setWsVr4CrsRefCliId(wsVr4CrsRefCliId);
	}

	@Override
	public String getWsVr4CrsRefClmPmtId() {
		return ws.getProgramHoldAreas().getWsVr4CrsRefClmPmtN().getWsVr4CrsRefClmPmtId();
	}

	@Override
	public void setWsVr4CrsRefClmPmtId(String wsVr4CrsRefClmPmtId) {
		this.ws.getProgramHoldAreas().getWsVr4CrsRefClmPmtN().setWsVr4CrsRefClmPmtId(wsVr4CrsRefClmPmtId);
	}

	@Override
	public char getWsVr4CrsRefRsnCd() {
		return ws.getProgramHoldAreas().getWsVr4CrsRefRsnCd();
	}

	@Override
	public void setWsVr4CrsRefRsnCd(char wsVr4CrsRefRsnCd) {
		this.ws.getProgramHoldAreas().setWsVr4CrsRefRsnCd(wsVr4CrsRefRsnCd);
	}

	@Override
	public String getWsVr4CrsRfClmCntlId() {
		return ws.getProgramHoldAreas().getWsVr4CrsRfClmCntlId();
	}

	@Override
	public void setWsVr4CrsRfClmCntlId(String wsVr4CrsRfClmCntlId) {
		this.ws.getProgramHoldAreas().setWsVr4CrsRfClmCntlId(wsVr4CrsRfClmCntlId);
	}

	@Override
	public char getWsVr4CsRfClmClSxId() {
		return ws.getProgramHoldAreas().getWsVr4CsRfClmClSxId();
	}

	@Override
	public void setWsVr4CsRfClmClSxId(char wsVr4CsRfClmClSxId) {
		this.ws.getProgramHoldAreas().setWsVr4CsRfClmClSxId(wsVr4CsRfClmClSxId);
	}

	@Override
	public String getWsVr4DnlRmrkCd() {
		return ws.getProgramHoldAreas().getWsVr4DnlRmrkCd();
	}

	@Override
	public void setWsVr4DnlRmrkCd(String wsVr4DnlRmrkCd) {
		this.ws.getProgramHoldAreas().setWsVr4DnlRmrkCd(wsVr4DnlRmrkCd);
	}

	@Override
	public String getWsVr4InitClmCntlId() {
		return ws.getProgramHoldAreas().getWsVr4InitClmCntlId();
	}

	@Override
	public void setWsVr4InitClmCntlId(String wsVr4InitClmCntlId) {
		this.ws.getProgramHoldAreas().setWsVr4InitClmCntlId(wsVr4InitClmCntlId);
	}

	@Override
	public String getWsVr4InitClmPmtId() {
		return ws.getProgramHoldAreas().getWsVr4InitClmPmtId();
	}

	@Override
	public void setWsVr4InitClmPmtId(String wsVr4InitClmPmtId) {
		this.ws.getProgramHoldAreas().setWsVr4InitClmPmtId(wsVr4InitClmPmtId);
	}

	@Override
	public char getWsVr4IntClmCnlSfxId() {
		return ws.getProgramHoldAreas().getWsVr4IntClmCnlSfxId();
	}

	@Override
	public void setWsVr4IntClmCnlSfxId(char wsVr4IntClmCnlSfxId) {
		this.ws.getProgramHoldAreas().setWsVr4IntClmCnlSfxId(wsVr4IntClmCnlSfxId);
	}
}
