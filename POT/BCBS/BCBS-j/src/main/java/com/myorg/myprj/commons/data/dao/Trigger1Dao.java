/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.ITrigger1;

/**
 * Data Access Object(DAO) for table [TRIGGER1]
 * 
 */
public class Trigger1Dao extends BaseSqlDao<ITrigger1> {

	public Trigger1Dao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<ITrigger1> getToClass() {
		return ITrigger1.class;
	}

	public DbAccessStatus insertRec(ITrigger1 iTrigger1) {
		return buildQuery("insertRec").bind(iTrigger1).executeInsert();
	}
}
