/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: NEW-KEY<br>
 * Variable: NEW-KEY from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class NewKey {

	//==== PROPERTIES ====
	//Original name: NEW-CLAIM-ID
	private String claimId = DefaultValues.stringVal(Len.CLAIM_ID);
	//Original name: NEW-CLAIM-SFX
	private char claimSfx = DefaultValues.CHAR_VAL;
	//Original name: NEW-PMT-ID
	private String pmtId = DefaultValues.stringVal(Len.PMT_ID);

	//==== METHODS ====
	public String getNewKeyFormatted() {
		return MarshalByteExt.bufferToStr(getNewKeyBytes());
	}

	public byte[] getNewKeyBytes() {
		byte[] buffer = new byte[Len.NEW_KEY];
		return getNewKeyBytes(buffer, 1);
	}

	public byte[] getNewKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, claimId, Len.CLAIM_ID);
		position += Len.CLAIM_ID;
		MarshalByte.writeChar(buffer, position, claimSfx);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pmtId, Len.PMT_ID);
		return buffer;
	}

	public void setClaimId(String claimId) {
		this.claimId = Functions.subString(claimId, Len.CLAIM_ID);
	}

	public String getClaimId() {
		return this.claimId;
	}

	public void setClaimSfx(char claimSfx) {
		this.claimSfx = claimSfx;
	}

	public char getClaimSfx() {
		return this.claimSfx;
	}

	public void setPmtId(short pmtId) {
		this.pmtId = NumericDisplay.asString(pmtId, Len.PMT_ID);
	}

	public short getPmtId() {
		return NumericDisplay.asShort(this.pmtId);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLAIM_ID = 12;
		public static final int PMT_ID = 2;
		public static final int CLAIM_SFX = 1;
		public static final int NEW_KEY = CLAIM_ID + CLAIM_SFX + PMT_ID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
