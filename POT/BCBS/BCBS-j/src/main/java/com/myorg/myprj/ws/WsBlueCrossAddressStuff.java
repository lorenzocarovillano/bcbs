/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-BLUE-CROSS-ADDRESS-STUFF<br>
 * Variable: WS-BLUE-CROSS-ADDRESS-STUFF from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsBlueCrossAddressStuff {

	//==== PROPERTIES ====
	//Original name: BLUE-CROSS-NAME
	private String name = "BLUE CROSS & BLUE SHIELD";
	//Original name: BLUE-CROSS-ADDRESS-1
	private String address1 = "";
	//Original name: BLUE-CROSS-ADDRESS-2
	private String address2 = "1133 SW TOPEKA BLVD";
	//Original name: BLUE-CROSS-ADDR3-CITY
	private String addr3City = "TOPEKA";
	//Original name: BLUE-CROSS-ADDR3-ST
	private String addr3St = "KS";
	//Original name: BLUE-CROSS-ADDR3-ZIP
	private String addr3Zip = "666290001";

	//==== METHODS ====
	public String getName() {
		return this.name;
	}

	public String getNameFormatted() {
		return Functions.padBlanks(getName(), Len.NAME);
	}

	public String getAddress1() {
		return this.address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public String getAddr3City() {
		return this.addr3City;
	}

	public String getAddr3St() {
		return this.addr3St;
	}

	public String getAddr3Zip() {
		return this.addr3Zip;
	}

	public String getAddr3ZipFormatted() {
		return Functions.padBlanks(getAddr3Zip(), Len.ADDR3_ZIP);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NAME = 25;
		public static final int ADDR3_ZIP = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
