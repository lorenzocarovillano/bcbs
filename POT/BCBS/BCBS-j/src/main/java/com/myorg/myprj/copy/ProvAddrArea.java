/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: PROV-ADDR-AREA<br>
 * Variable: PROV-ADDR-AREA from copybook NU02PROV<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ProvAddrArea {

	//==== PROPERTIES ====
	//Original name: PROV-ADDR-ATTN
	private String addrAttn = DefaultValues.stringVal(Len.ADDR_ATTN);
	//Original name: PROV-ADDR-STREET
	private String addrStreet = DefaultValues.stringVal(Len.ADDR_STREET);
	//Original name: PROV-ADDR-CITY
	private String addrCity = DefaultValues.stringVal(Len.ADDR_CITY);
	//Original name: FILLER-PROV-ADDR-CITY-ST
	private char flr1 = ' ';
	//Original name: PROV-ADDR-ST
	private String addrSt = DefaultValues.stringVal(Len.ADDR_ST);
	//Original name: PROV-ADDR-ZIP-5
	private String addrZip5 = DefaultValues.stringVal(Len.ADDR_ZIP5);
	//Original name: FILLER-PROV-ADDR-ZIP
	private char flr2 = '-';
	//Original name: PROV-ADDR-ZIP-4
	private String addrZip4 = DefaultValues.stringVal(Len.ADDR_ZIP4);
	//Original name: PROV-PHN-AREA-CD
	private String phnAreaCd = DefaultValues.stringVal(Len.PHN_AREA_CD);
	//Original name: PROV-PHN-NUMBER
	private String phnNumber = DefaultValues.stringVal(Len.PHN_NUMBER);

	//==== METHODS ====
	public void setProvAddrAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		addrAttn = MarshalByte.readString(buffer, position, Len.ADDR_ATTN);
		position += Len.ADDR_ATTN;
		addrStreet = MarshalByte.readString(buffer, position, Len.ADDR_STREET);
		position += Len.ADDR_STREET;
		setAddrCityStBytes(buffer, position);
		position += Len.ADDR_CITY_ST;
		setAddrZipBytes(buffer, position);
		position += Len.ADDR_ZIP;
		setPhoneBytes(buffer, position);
	}

	public byte[] getProvAddrAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, addrAttn, Len.ADDR_ATTN);
		position += Len.ADDR_ATTN;
		MarshalByte.writeString(buffer, position, addrStreet, Len.ADDR_STREET);
		position += Len.ADDR_STREET;
		getAddrCityStBytes(buffer, position);
		position += Len.ADDR_CITY_ST;
		getAddrZipBytes(buffer, position);
		position += Len.ADDR_ZIP;
		getPhoneBytes(buffer, position);
		return buffer;
	}

	public void initProvAddrAreaSpaces() {
		addrAttn = "";
		addrStreet = "";
		initAddrCityStSpaces();
		initAddrZipSpaces();
		initPhoneSpaces();
	}

	public void setAddrAttn(String addrAttn) {
		this.addrAttn = Functions.subString(addrAttn, Len.ADDR_ATTN);
	}

	public String getAddrAttn() {
		return this.addrAttn;
	}

	public void setAddrStreet(String addrStreet) {
		this.addrStreet = Functions.subString(addrStreet, Len.ADDR_STREET);
	}

	public String getAddrStreet() {
		return this.addrStreet;
	}

	public void setAddrCityStBytes(byte[] buffer, int offset) {
		int position = offset;
		addrCity = MarshalByte.readString(buffer, position, Len.ADDR_CITY);
		position += Len.ADDR_CITY;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		addrSt = MarshalByte.readString(buffer, position, Len.ADDR_ST);
	}

	public byte[] getAddrCityStBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, addrCity, Len.ADDR_CITY);
		position += Len.ADDR_CITY;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, addrSt, Len.ADDR_ST);
		return buffer;
	}

	public void initAddrCityStSpaces() {
		addrCity = "";
		flr1 = Types.SPACE_CHAR;
		addrSt = "";
	}

	public void setAddrCity(String addrCity) {
		this.addrCity = Functions.subString(addrCity, Len.ADDR_CITY);
	}

	public String getAddrCity() {
		return this.addrCity;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setAddrSt(String addrSt) {
		this.addrSt = Functions.subString(addrSt, Len.ADDR_ST);
	}

	public String getAddrSt() {
		return this.addrSt;
	}

	public String getAddrZipFormatted() {
		return MarshalByteExt.bufferToStr(getAddrZipBytes());
	}

	/**Original name: PROV-ADDR-ZIP<br>*/
	public byte[] getAddrZipBytes() {
		byte[] buffer = new byte[Len.ADDR_ZIP];
		return getAddrZipBytes(buffer, 1);
	}

	public void setAddrZipBytes(byte[] buffer, int offset) {
		int position = offset;
		addrZip5 = MarshalByte.readString(buffer, position, Len.ADDR_ZIP5);
		position += Len.ADDR_ZIP5;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		addrZip4 = MarshalByte.readString(buffer, position, Len.ADDR_ZIP4);
	}

	public byte[] getAddrZipBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, addrZip5, Len.ADDR_ZIP5);
		position += Len.ADDR_ZIP5;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, addrZip4, Len.ADDR_ZIP4);
		return buffer;
	}

	public void initAddrZipSpaces() {
		addrZip5 = "";
		flr2 = Types.SPACE_CHAR;
		addrZip4 = "";
	}

	public void setAddrZip5(String addrZip5) {
		this.addrZip5 = Functions.subString(addrZip5, Len.ADDR_ZIP5);
	}

	public String getAddrZip5() {
		return this.addrZip5;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setAddrZip4(String addrZip4) {
		this.addrZip4 = Functions.subString(addrZip4, Len.ADDR_ZIP4);
	}

	public String getAddrZip4() {
		return this.addrZip4;
	}

	public void setPhoneBytes(byte[] buffer, int offset) {
		int position = offset;
		phnAreaCd = MarshalByte.readString(buffer, position, Len.PHN_AREA_CD);
		position += Len.PHN_AREA_CD;
		phnNumber = MarshalByte.readString(buffer, position, Len.PHN_NUMBER);
	}

	public byte[] getPhoneBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, phnAreaCd, Len.PHN_AREA_CD);
		position += Len.PHN_AREA_CD;
		MarshalByte.writeString(buffer, position, phnNumber, Len.PHN_NUMBER);
		return buffer;
	}

	public void initPhoneSpaces() {
		phnAreaCd = "";
		phnNumber = "";
	}

	public void setPhnAreaCd(String phnAreaCd) {
		this.phnAreaCd = Functions.subString(phnAreaCd, Len.PHN_AREA_CD);
	}

	public String getPhnAreaCd() {
		return this.phnAreaCd;
	}

	public void setPhnNumber(String phnNumber) {
		this.phnNumber = Functions.subString(phnNumber, Len.PHN_NUMBER);
	}

	public String getPhnNumber() {
		return this.phnNumber;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ADDR_ATTN = 19;
		public static final int ADDR_STREET = 25;
		public static final int ADDR_CITY = 25;
		public static final int FLR1 = 1;
		public static final int ADDR_ST = 2;
		public static final int ADDR_CITY_ST = ADDR_CITY + ADDR_ST + FLR1;
		public static final int ADDR_ZIP5 = 5;
		public static final int ADDR_ZIP4 = 4;
		public static final int ADDR_ZIP = ADDR_ZIP5 + ADDR_ZIP4 + FLR1;
		public static final int PHN_AREA_CD = 3;
		public static final int PHN_NUMBER = 7;
		public static final int PHONE = PHN_AREA_CD + PHN_NUMBER;
		public static final int PROV_ADDR_AREA = ADDR_ATTN + ADDR_STREET + ADDR_CITY_ST + ADDR_ZIP + PHONE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
