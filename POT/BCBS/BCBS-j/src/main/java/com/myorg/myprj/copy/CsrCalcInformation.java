/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.myorg.myprj.ws.occurs.CsrCalcEntry;

/**Original name: CSR-CALC-INFORMATION<br>
 * Variable: CSR-CALC-INFORMATION from copybook NF07AREA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CsrCalcInformation {

	//==== PROPERTIES ====
	public static final int CALC_ENTRY_MAXOCCURS = 9;
	//Original name: CSR-CALC-ENTRY
	private CsrCalcEntry[] calcEntry = new CsrCalcEntry[CALC_ENTRY_MAXOCCURS];
	/**Original name: CSR-LINE-CALC-EXPLN-CD<br>
	 * <pre>      ----------------------------------------------------
	 *       --         CALC SUMMARIZED FIELDS
	 *       -- LINE-CALC-EXPLN-CD = FIRST NON-BLANK EXPLN-CD FOUND
	 *       -- LINE-CALC-ALW-CHG-AM = FIRST NON-ZERO CALC ALW AMT
	 *       -- LINE-CALC-PD-AM = SUM OF ALL CALC PD-AM
	 *       ----------------------------------------------------</pre>*/
	private char lineCalcExplnCd = Types.SPACE_CHAR;
	//Original name: CSR-LINE-CALC-ALW-CHG-AM
	private AfDecimal lineCalcAlwChgAm = new AfDecimal("0", 11, 2);
	//Original name: CSR-LINE-CALC-PD-AM
	private AfDecimal lineCalcPdAm = new AfDecimal("0", 11, 2);

	//==== CONSTRUCTORS ====
	public CsrCalcInformation() {
		init();
	}

	//==== METHODS ====
	private void init() {
		for (int calcEntryIdx = 1; calcEntryIdx <= CALC_ENTRY_MAXOCCURS; calcEntryIdx++) {
			calcEntry[calcEntryIdx - 1] = new CsrCalcEntry();
		}
	}

	public byte[] getCalcInformationBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= CALC_ENTRY_MAXOCCURS; idx++) {
			calcEntry[idx - 1].getCalcEntryBytes(buffer, position);
			position += CsrCalcEntry.Len.CALC_ENTRY;
		}
		MarshalByte.writeChar(buffer, position, lineCalcExplnCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeDecimalAsPacked(buffer, position, lineCalcAlwChgAm.copy());
		position += Len.LINE_CALC_ALW_CHG_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, lineCalcPdAm.copy());
		return buffer;
	}

	public void initCalcInformationSpaces() {
		for (int idx = 1; idx <= CALC_ENTRY_MAXOCCURS; idx++) {
			calcEntry[idx - 1].initCalcEntrySpaces();
		}
		lineCalcExplnCd = Types.SPACE_CHAR;
		lineCalcAlwChgAm.setNaN();
		lineCalcPdAm.setNaN();
	}

	public void setLineCalcExplnCd(char lineCalcExplnCd) {
		this.lineCalcExplnCd = lineCalcExplnCd;
	}

	public char getLineCalcExplnCd() {
		return this.lineCalcExplnCd;
	}

	public void setLineCalcAlwChgAm(AfDecimal lineCalcAlwChgAm) {
		this.lineCalcAlwChgAm.assign(lineCalcAlwChgAm);
	}

	public AfDecimal getLineCalcAlwChgAm() {
		return this.lineCalcAlwChgAm.copy();
	}

	public void setLineCalcPdAm(AfDecimal lineCalcPdAm) {
		this.lineCalcPdAm.assign(lineCalcPdAm);
	}

	public AfDecimal getLineCalcPdAm() {
		return this.lineCalcPdAm.copy();
	}

	public CsrCalcEntry getCalcEntry(int idx) {
		return calcEntry[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LINE_CALC_EXPLN_CD = 1;
		public static final int LINE_CALC_ALW_CHG_AM = 6;
		public static final int LINE_CALC_PD_AM = 6;
		public static final int CALC_INFORMATION = CsrCalcInformation.CALC_ENTRY_MAXOCCURS * CsrCalcEntry.Len.CALC_ENTRY + LINE_CALC_EXPLN_CD
				+ LINE_CALC_ALW_CHG_AM + LINE_CALC_PD_AM;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int LINE_CALC_ALW_CHG_AM = 9;
			public static final int LINE_CALC_PD_AM = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int LINE_CALC_ALW_CHG_AM = 2;
			public static final int LINE_CALC_PD_AM = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
