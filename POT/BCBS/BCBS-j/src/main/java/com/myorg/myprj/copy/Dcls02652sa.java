/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLS02652SA<br>
 * Variable: DCLS02652SA from copybook S02652SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02652sa {

	//==== PROPERTIES ====
	//Original name: CLM-SBMT-ID
	private String clmSbmtId = DefaultValues.stringVal(Len.CLM_SBMT_ID);
	//Original name: MNTRY-AM
	private AfDecimal mntryAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: FAC-VL-CD
	private String facVlCd = DefaultValues.stringVal(Len.FAC_VL_CD);
	//Original name: FAC-QLF-CD
	private String facQlfCd = DefaultValues.stringVal(Len.FAC_QLF_CD);
	//Original name: CLM-FREQ-TYP-CD
	private char clmFreqTypCd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setClmSbmtId(String clmSbmtId) {
		this.clmSbmtId = Functions.subString(clmSbmtId, Len.CLM_SBMT_ID);
	}

	public String getClmSbmtId() {
		return this.clmSbmtId;
	}

	public void setMntryAm(AfDecimal mntryAm) {
		this.mntryAm.assign(mntryAm);
	}

	public AfDecimal getMntryAm() {
		return this.mntryAm.copy();
	}

	public void setFacVlCd(String facVlCd) {
		this.facVlCd = Functions.subString(facVlCd, Len.FAC_VL_CD);
	}

	public String getFacVlCd() {
		return this.facVlCd;
	}

	public void setFacQlfCd(String facQlfCd) {
		this.facQlfCd = Functions.subString(facQlfCd, Len.FAC_QLF_CD);
	}

	public String getFacQlfCd() {
		return this.facQlfCd;
	}

	public void setClmFreqTypCd(char clmFreqTypCd) {
		this.clmFreqTypCd = clmFreqTypCd;
	}

	public char getClmFreqTypCd() {
		return this.clmFreqTypCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_SBMT_ID = 38;
		public static final int FAC_VL_CD = 2;
		public static final int FAC_QLF_CD = 2;
		public static final int CLM_CNTL_ID = 12;
		public static final int LOOP_ID = 6;
		public static final int SEG_ID = 3;
		public static final int CLM111_RLT_CS_CD = 3;
		public static final int CLM112_RLT_CS_CD = 3;
		public static final int CLM113_RLT_CS_CD = 3;
		public static final int ST_PROV_CD = 2;
		public static final int CNTRY_CD = 3;
		public static final int SPEC_PGM_CD = 3;
		public static final int CLM_SBMSN_RSN_CD = 2;
		public static final int DLY_RSN_CD = 2;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
