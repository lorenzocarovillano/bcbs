/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [S03086SA]
 * 
 */
public interface IS03086sa extends BaseSqlTo {

	/**
	 * Host Variable APPL-TRM-CD
	 * 
	 */
	char getApplTrmCd();

	void setApplTrmCd(char applTrmCd);

	/**
	 * Host Variable RSTRT-HIST-IN
	 * 
	 */
	char getRstrtHistIn();

	void setRstrtHistIn(char rstrtHistIn);

	/**
	 * Host Variable ROW-FREQ-CMT-CT
	 * 
	 */
	int getRowFreqCmtCt();

	void setRowFreqCmtCt(int rowFreqCmtCt);

	/**
	 * Host Variable CMT-TM-INT-TM
	 * 
	 */
	String getCmtTmIntTm();

	void setCmtTmIntTm(String cmtTmIntTm);

	/**
	 * Host Variable CMT-CT
	 * 
	 */
	int getCmtCt();

	void setCmtCt(int cmtCt);

	/**
	 * Host Variable CMT-TS
	 * 
	 */
	String getCmtTs();

	void setCmtTs(String cmtTs);

	/**
	 * Host Variable RSTRT1-TX
	 * 
	 */
	String getRstrt1Tx();

	void setRstrt1Tx(String rstrt1Tx);

	/**
	 * Host Variable RSTRT2-TX
	 * 
	 */
	String getRstrt2Tx();

	void setRstrt2Tx(String rstrt2Tx);

	/**
	 * Host Variable JOB-NM
	 * 
	 */
	String getJobNm();

	void setJobNm(String jobNm);

	/**
	 * Host Variable JBSTP-NM
	 * 
	 */
	String getJbstpNm();

	void setJbstpNm(String jbstpNm);

	/**
	 * Host Variable JBSTP-SEQ-ID
	 * 
	 */
	short getJbstpSeqId();

	void setJbstpSeqId(short jbstpSeqId);

	/**
	 * Host Variable INFO-CHG-TS
	 * 
	 */
	String getInfoChgTs();

	void setInfoChgTs(String infoChgTs);

	/**
	 * Host Variable INFO-CHG-ID
	 * 
	 */
	String getInfoChgId();

	void setInfoChgId(String infoChgId);
};
