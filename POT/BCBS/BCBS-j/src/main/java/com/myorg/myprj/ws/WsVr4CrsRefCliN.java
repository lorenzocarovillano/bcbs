/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WS-VR4-CRS-REF-CLI-N<br>
 * Variable: WS-VR4-CRS-REF-CLI-N from program NF0735ML<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsVr4CrsRefCliN {

	//==== PROPERTIES ====
	//Original name: WS-VR4-CRS-REF-CLI-P3
	private String p3 = DefaultValues.stringVal(Len.P3);
	//Original name: WS-VR4-CRS-REF-CLI-P2
	private String p2 = DefaultValues.stringVal(Len.P2);
	//Original name: WS-VR4-CRS-REF-CLI-P1
	private String p1 = DefaultValues.stringVal(Len.P1);

	//==== METHODS ====
	public void setWsVr4CrsRefCliId(String wsVr4CrsRefCliId) {
		int position = 1;
		byte[] buffer = getWsVr4CrsRefCliNBytes();
		MarshalByte.writeString(buffer, position, wsVr4CrsRefCliId, Len.WS_VR4_CRS_REF_CLI_ID);
		setWsVr4CrsRefCliNBytes(buffer);
	}

	/**Original name: WS-VR4-CRS-REF-CLI-ID<br>*/
	public String getWsVr4CrsRefCliId() {
		int position = 1;
		return MarshalByte.readString(getWsVr4CrsRefCliNBytes(), position, Len.WS_VR4_CRS_REF_CLI_ID);
	}

	public void setWsVr4CrsRefCliNBytes(byte[] buffer) {
		setWsVr4CrsRefCliNBytes(buffer, 1);
	}

	/**Original name: WS-VR4-CRS-REF-CLI-N<br>*/
	public byte[] getWsVr4CrsRefCliNBytes() {
		byte[] buffer = new byte[Len.WS_VR4_CRS_REF_CLI_N];
		return getWsVr4CrsRefCliNBytes(buffer, 1);
	}

	public void setWsVr4CrsRefCliNBytes(byte[] buffer, int offset) {
		int position = offset;
		p3 = MarshalByte.readFixedString(buffer, position, Len.P3);
		position += Len.P3;
		p2 = MarshalByte.readFixedString(buffer, position, Len.P2);
		position += Len.P2;
		p1 = MarshalByte.readFixedString(buffer, position, Len.P1);
	}

	public byte[] getWsVr4CrsRefCliNBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, p3, Len.P3);
		position += Len.P3;
		MarshalByte.writeString(buffer, position, p2, Len.P2);
		position += Len.P2;
		MarshalByte.writeString(buffer, position, p1, Len.P1);
		return buffer;
	}

	public String getP3Formatted() {
		return this.p3;
	}

	public String getP2Formatted() {
		return this.p2;
	}

	public String getP1Formatted() {
		return this.p1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int P3 = 1;
		public static final int P2 = 1;
		public static final int P1 = 1;
		public static final int WS_VR4_CRS_REF_CLI_N = P3 + P2 + P1;
		public static final int WS_VR4_CRS_REF_CLI_ID = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_VR4_CRS_REF_CLI_ID = 3;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
