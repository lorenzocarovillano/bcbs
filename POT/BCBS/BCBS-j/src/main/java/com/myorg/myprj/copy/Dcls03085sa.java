/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.myorg.myprj.commons.data.to.IS03085sa;

/**Original name: DCLS03085SA<br>
 * Variable: DCLS03085SA from copybook S03085SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls03085sa implements IS03085sa {

	//==== PROPERTIES ====
	//Original name: JOB-NM
	private String jobNm = DefaultValues.stringVal(Len.JOB_NM);
	//Original name: JBSTP-NM
	private String jbstpNm = DefaultValues.stringVal(Len.JBSTP_NM);
	//Original name: JBSTP-SEQ-ID
	private short jbstpSeqId = DefaultValues.SHORT_VAL;
	//Original name: RSTRT-IN
	private char rstrtIn = DefaultValues.CHAR_VAL;
	//Original name: JBSTP-STRT-TS
	private String jbstpStrtTs = DefaultValues.stringVal(Len.JBSTP_STRT_TS);
	//Original name: JBSTP-END-TS
	private String jbstpEndTs = DefaultValues.stringVal(Len.JBSTP_END_TS);
	//Original name: JBSTP-RUN-CT
	private short jbstpRunCt = DefaultValues.SHORT_VAL;
	//Original name: INFO-CHG-ID
	private String infoChgId = DefaultValues.stringVal(Len.INFO_CHG_ID);
	//Original name: INFO-CHG-TS
	private String infoChgTs = DefaultValues.stringVal(Len.INFO_CHG_TS);

	//==== METHODS ====
	@Override
	public void setJobNm(String jobNm) {
		this.jobNm = Functions.subString(jobNm, Len.JOB_NM);
	}

	@Override
	public String getJobNm() {
		return this.jobNm;
	}

	public String getJobNmFormatted() {
		return Functions.padBlanks(getJobNm(), Len.JOB_NM);
	}

	@Override
	public void setJbstpNm(String jbstpNm) {
		this.jbstpNm = Functions.subString(jbstpNm, Len.JBSTP_NM);
	}

	@Override
	public String getJbstpNm() {
		return this.jbstpNm;
	}

	public String getJbstpNmFormatted() {
		return Functions.padBlanks(getJbstpNm(), Len.JBSTP_NM);
	}

	@Override
	public void setJbstpSeqId(short jbstpSeqId) {
		this.jbstpSeqId = jbstpSeqId;
	}

	@Override
	public short getJbstpSeqId() {
		return this.jbstpSeqId;
	}

	public String getJbstpSeqIdFormatted() {
		return PicFormatter.display(new PicParams("S9(3)V").setUsage(PicUsage.PACKED)).format(getJbstpSeqId()).toString();
	}

	public String getJbstpSeqIdAsString() {
		return getJbstpSeqIdFormatted();
	}

	@Override
	public void setRstrtIn(char rstrtIn) {
		this.rstrtIn = rstrtIn;
	}

	@Override
	public char getRstrtIn() {
		return this.rstrtIn;
	}

	@Override
	public void setJbstpStrtTs(String jbstpStrtTs) {
		this.jbstpStrtTs = Functions.subString(jbstpStrtTs, Len.JBSTP_STRT_TS);
	}

	@Override
	public String getJbstpStrtTs() {
		return this.jbstpStrtTs;
	}

	public String getJbstpStrtTsFormatted() {
		return Functions.padBlanks(getJbstpStrtTs(), Len.JBSTP_STRT_TS);
	}

	@Override
	public void setJbstpEndTs(String jbstpEndTs) {
		this.jbstpEndTs = Functions.subString(jbstpEndTs, Len.JBSTP_END_TS);
	}

	@Override
	public String getJbstpEndTs() {
		return this.jbstpEndTs;
	}

	public String getJbstpEndTsFormatted() {
		return Functions.padBlanks(getJbstpEndTs(), Len.JBSTP_END_TS);
	}

	@Override
	public void setJbstpRunCt(short jbstpRunCt) {
		this.jbstpRunCt = jbstpRunCt;
	}

	@Override
	public short getJbstpRunCt() {
		return this.jbstpRunCt;
	}

	public String getJbstpRunCtFormatted() {
		return PicFormatter.display(new PicParams("S9(3)V").setUsage(PicUsage.PACKED)).format(getJbstpRunCt()).toString();
	}

	public String getJbstpRunCtAsString() {
		return getJbstpRunCtFormatted();
	}

	@Override
	public void setInfoChgId(String infoChgId) {
		this.infoChgId = Functions.subString(infoChgId, Len.INFO_CHG_ID);
	}

	@Override
	public String getInfoChgId() {
		return this.infoChgId;
	}

	public String getInfoChgIdFormatted() {
		return Functions.padBlanks(getInfoChgId(), Len.INFO_CHG_ID);
	}

	@Override
	public void setInfoChgTs(String infoChgTs) {
		this.infoChgTs = Functions.subString(infoChgTs, Len.INFO_CHG_TS);
	}

	@Override
	public String getInfoChgTs() {
		return this.infoChgTs;
	}

	public String getInfoChgTsFormatted() {
		return Functions.padBlanks(getInfoChgTs(), Len.INFO_CHG_TS);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int JOB_NM = 8;
		public static final int JBSTP_NM = 8;
		public static final int JBSTP_STRT_TS = 26;
		public static final int JBSTP_END_TS = 26;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_TS = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
