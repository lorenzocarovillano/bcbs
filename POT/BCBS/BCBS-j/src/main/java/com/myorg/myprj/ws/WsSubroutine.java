/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.copy.SbrHeaderData;
import com.myorg.myprj.ws.occurs.SbrVoidRepayData;

/**Original name: WS-SUBROUTINE<br>
 * Variable: WS-SUBROUTINE from program NF0533ML<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsSubroutine extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int SBR_VOID_REPAY_DATA_MAXOCCURS = 2;
	public static final int SBR_GEN_LDGR_OFFSET_INDC_MAXOCCURS = 2;
	public static final int SBR_REPORT_INDC_MAXOCCURS = 2;
	//Original name: SBR-HEADER-DATA
	private SbrHeaderData sbrHeaderData = new SbrHeaderData();
	//Original name: SBR-VOID-REPAY-DATA
	private SbrVoidRepayData[] sbrVoidRepayData = new SbrVoidRepayData[SBR_VOID_REPAY_DATA_MAXOCCURS];
	//Original name: SBR-GEN-LDGR-OFFSET-INDC
	private String[] sbrGenLdgrOffsetIndc = new String[SBR_GEN_LDGR_OFFSET_INDC_MAXOCCURS];
	/**Original name: SBR-REPORT-INDC<br>
	 * <pre>LCAROVILLANO              88 SBR-CASH-BANK-IV          VALUE 'B '.
	 * LCAROVILLANO              88 SBR-CASH-PLAN-65          VALUE 'C '.
	 * LCAROVILLANO              88 SBR-ACCT-RECEIVABLE       VALUE 'D '.
	 * LCAROVILLANO              88 SBR-ACCT-PAY-TV-RECON     VALUE 'I '.
	 * LCAROVILLANO              88 SBR-ACCT-PAY-PIP          VALUE 'J '.
	 * LCAROVILLANO              88 SBR-ACCT-PAY-FEP-CROSS    VALUE 'M '.
	 * LCAROVILLANO              88 SBR-ACCT-PAY-VEN-DRUGS    VALUE 'T '.
	 * LCAROVILLANO              88 SBR-ITS-ACCTS-PAY-PROV    VALUE 'U '.
	 * LCAROVILLANO              88 SBR-ITS-ACCTS-PAY-SUBS    VALUE 'V '.
	 * LCAROVILLANO              88 SBR-FEP-CASH              VALUE 'W '.
	 * LCAROVILLANO              88 SBR-DESIRES-EFT           VALUE 'X '.
	 * LCAROVILLANO              88 SBR-NO-ENTRY              VALUE 'Z '.</pre>*/
	private String[] sbrReportIndc = new String[SBR_REPORT_INDC_MAXOCCURS];
	/**Original name: FILLER-SBR-RETURN-DATA<br>
	 * <pre>LCAROVILLANO              88 SBR-NO-REPORT             VALUE 00.
	 * LCAROVILLANO              88 SBR-SUM-OF-CLM            VALUE 01.
	 * LCAROVILLANO              88 SBR-ADJ-TYP-BH            VALUE 04.
	 * LCAROVILLANO              88 SBR-HST-LD-ADJ-A          VALUE 05.
	 * LCAROVILLANO              88 SBR-HST-LD-ADJ-L          VALUE 08.
	 * LCAROVILLANO              88 SBR-HST-LD-L              VALUE 09.
	 * LCAROVILLANO              88 SBR-HST-LD-ADJ-M          VALUE 10.
	 * LCAROVILLANO              88 SBR-HST-LD-M              VALUE 11.
	 * LCAROVILLANO              88 SBR-HST-LD-ADJ-D          VALUE 14.
	 * LCAROVILLANO              88 SBR-HST-LD-D              VALUE 15.
	 * LCAROVILLANO              88 SBR-ITS-HOME-EXPNS        VALUE 16.</pre>*/
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== CONSTRUCTORS ====
	public WsSubroutine() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_SUBROUTINE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsSubroutineBytes(buf);
	}

	private void init() {
		for (int sbrVoidRepayDataIdx = 1; sbrVoidRepayDataIdx <= SBR_VOID_REPAY_DATA_MAXOCCURS; sbrVoidRepayDataIdx++) {
			sbrVoidRepayData[sbrVoidRepayDataIdx - 1] = new SbrVoidRepayData();
		}
		for (int sbrGenLdgrOffsetIndcIdx = 1; sbrGenLdgrOffsetIndcIdx <= SBR_GEN_LDGR_OFFSET_INDC_MAXOCCURS; sbrGenLdgrOffsetIndcIdx++) {
			setSbrGenLdgrOffsetIndc(sbrGenLdgrOffsetIndcIdx, DefaultValues.stringVal(Len.SBR_GEN_LDGR_OFFSET_INDC));
		}
		for (int sbrReportIndcIdx = 1; sbrReportIndcIdx <= SBR_REPORT_INDC_MAXOCCURS; sbrReportIndcIdx++) {
			setSbrReportIndcFormatted(sbrReportIndcIdx, DefaultValues.stringVal(Len.SBR_REPORT_INDC));
		}
	}

	public String getWsSubroutineFormatted() {
		return getSubroutineRecordFormatted();
	}

	public void setWsSubroutineBytes(byte[] buffer) {
		setWsSubroutineBytes(buffer, 1);
	}

	public byte[] getWsSubroutineBytes() {
		byte[] buffer = new byte[Len.WS_SUBROUTINE];
		return getWsSubroutineBytes(buffer, 1);
	}

	public void setWsSubroutineBytes(byte[] buffer, int offset) {
		int position = offset;
		setSubroutineRecordBytes(buffer, position);
	}

	public byte[] getWsSubroutineBytes(byte[] buffer, int offset) {
		int position = offset;
		getSubroutineRecordBytes(buffer, position);
		return buffer;
	}

	public String getSubroutineRecordFormatted() {
		return MarshalByteExt.bufferToStr(getSubroutineRecordBytes());
	}

	/**Original name: SUBROUTINE-RECORD<br>
	 * <pre> ---------------------------------------------------
	 *  COPYBOOK MEMBER NF05SUBR
	 *  LENGTH = 126
	 *  LAST UPDATED:  9/9/02   HIPAA
	 *     CHANGED TO ALLOW SUBROUTINE TO BE CALLED ONCE
	 *     PER PAYMENT INSTEAD OF ONCE PER LINE.
	 *  PRJ83126 04/19 BUTLER  REMOVE ADJ-REFUND TYPES C,D,E,F,G,H
	 *  ---------------------------------------------------</pre>*/
	public byte[] getSubroutineRecordBytes() {
		byte[] buffer = new byte[Len.SUBROUTINE_RECORD];
		return getSubroutineRecordBytes(buffer, 1);
	}

	public void setSubroutineRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		sbrHeaderData.setSbrHeaderDataBytes(buffer, position);
		position += SbrHeaderData.Len.SBR_HEADER_DATA;
		for (int idx = 1; idx <= SBR_VOID_REPAY_DATA_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				sbrVoidRepayData[idx - 1].setSbrVoidRepayDataBytes(buffer, position);
				position += SbrVoidRepayData.Len.SBR_VOID_REPAY_DATA;
			} else {
				sbrVoidRepayData[idx - 1].initSbrVoidRepayDataSpaces();
				position += SbrVoidRepayData.Len.SBR_VOID_REPAY_DATA;
			}
		}
		setSbrReturnDataBytes(buffer, position);
	}

	public byte[] getSubroutineRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		sbrHeaderData.getSbrHeaderDataBytes(buffer, position);
		position += SbrHeaderData.Len.SBR_HEADER_DATA;
		for (int idx = 1; idx <= SBR_VOID_REPAY_DATA_MAXOCCURS; idx++) {
			sbrVoidRepayData[idx - 1].getSbrVoidRepayDataBytes(buffer, position);
			position += SbrVoidRepayData.Len.SBR_VOID_REPAY_DATA;
		}
		getSbrReturnDataBytes(buffer, position);
		return buffer;
	}

	public void setSbrReturnDataBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= SBR_GEN_LDGR_OFFSET_INDC_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setSbrGenLdgrOffsetIndc(idx, MarshalByte.readString(buffer, position, Len.SBR_GEN_LDGR_OFFSET_INDC));
				position += Len.SBR_GEN_LDGR_OFFSET_INDC;
			} else {
				setSbrGenLdgrOffsetIndc(idx, "");
				position += Len.SBR_GEN_LDGR_OFFSET_INDC;
			}
		}
		for (int idx = 1; idx <= SBR_REPORT_INDC_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				sbrReportIndc[idx - 1] = MarshalByte.readFixedString(buffer, position, Len.SBR_REPORT_INDC);
				position += Len.SBR_REPORT_INDC;
			} else {
				setSbrReportIndc(idx, Types.INVALID_SHORT_VAL);
				position += Len.SBR_REPORT_INDC;
			}
		}
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getSbrReturnDataBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= SBR_GEN_LDGR_OFFSET_INDC_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getSbrGenLdgrOffsetIndc(idx), Len.SBR_GEN_LDGR_OFFSET_INDC);
			position += Len.SBR_GEN_LDGR_OFFSET_INDC;
		}
		for (int idx = 1; idx <= SBR_REPORT_INDC_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, sbrReportIndc[idx - 1], Len.SBR_REPORT_INDC);
			position += Len.SBR_REPORT_INDC;
		}
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setSbrGenLdgrOffsetIndc(int sbrGenLdgrOffsetIndcIdx, String sbrGenLdgrOffsetIndc) {
		this.sbrGenLdgrOffsetIndc[sbrGenLdgrOffsetIndcIdx - 1] = Functions.subString(sbrGenLdgrOffsetIndc, Len.SBR_GEN_LDGR_OFFSET_INDC);
	}

	public String getSbrGenLdgrOffsetIndc(int sbrGenLdgrOffsetIndcIdx) {
		return this.sbrGenLdgrOffsetIndc[sbrGenLdgrOffsetIndcIdx - 1];
	}

	public void setSbrReportIndc(int sbrReportIndcIdx, short sbrReportIndc) {
		this.sbrReportIndc[sbrReportIndcIdx - 1] = NumericDisplay.asString(sbrReportIndc, Len.SBR_REPORT_INDC);
	}

	public void setSbrReportIndcFormatted(int sbrReportIndcIdx, String sbrReportIndc) {
		this.sbrReportIndc[sbrReportIndcIdx - 1] = Trunc.toUnsignedNumeric(sbrReportIndc, Len.SBR_REPORT_INDC);
	}

	public short getSbrReportIndc(int sbrReportIndcIdx) {
		return NumericDisplay.asShort(this.sbrReportIndc[sbrReportIndcIdx - 1]);
	}

	public String getSbrReportIndcFormatted(int sbrReportIndcIdx) {
		return this.sbrReportIndc[sbrReportIndcIdx - 1];
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public SbrHeaderData getSbrHeaderData() {
		return sbrHeaderData;
	}

	public SbrVoidRepayData getSbrVoidRepayData(int idx) {
		return sbrVoidRepayData[idx - 1];
	}

	@Override
	public byte[] serialize() {
		return getWsSubroutineBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SBR_GEN_LDGR_OFFSET_INDC = 2;
		public static final int SBR_REPORT_INDC = 2;
		public static final int FLR1 = 20;
		public static final int SBR_RETURN_DATA = WsSubroutine.SBR_GEN_LDGR_OFFSET_INDC_MAXOCCURS * SBR_GEN_LDGR_OFFSET_INDC
				+ WsSubroutine.SBR_REPORT_INDC_MAXOCCURS * SBR_REPORT_INDC + FLR1;
		public static final int SUBROUTINE_RECORD = SbrHeaderData.Len.SBR_HEADER_DATA
				+ WsSubroutine.SBR_VOID_REPAY_DATA_MAXOCCURS * SbrVoidRepayData.Len.SBR_VOID_REPAY_DATA + SBR_RETURN_DATA;
		public static final int WS_SUBROUTINE = SUBROUTINE_RECORD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
