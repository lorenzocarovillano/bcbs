/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: NF05-DIAGNOSIS-CODES-DATA<br>
 * Variable: NF05-DIAGNOSIS-CODES-DATA from copybook NF05EOB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Nf05DiagnosisCodesData {

	//==== PROPERTIES ====
	//Original name: NF05-DIAGNOSIS-CODE1
	private String code1 = DefaultValues.stringVal(Len.CODE1);
	//Original name: NF05-DIAGNOSIS-CODE2
	private String code2 = DefaultValues.stringVal(Len.CODE2);
	//Original name: NF05-DIAGNOSIS-CODE3
	private String code3 = DefaultValues.stringVal(Len.CODE3);
	//Original name: NF05-DIAGNOSIS-CODE4
	private String code4 = DefaultValues.stringVal(Len.CODE4);
	//Original name: NF05-DIAGNOSIS-CODE5
	private String code5 = DefaultValues.stringVal(Len.CODE5);
	//Original name: NF05-DIAGNOSIS-CODE6
	private String code6 = DefaultValues.stringVal(Len.CODE6);
	//Original name: NF05-DIAGNOSIS-CODE7
	private String code7 = DefaultValues.stringVal(Len.CODE7);
	//Original name: NF05-DIAGNOSIS-CODE8
	private String code8 = DefaultValues.stringVal(Len.CODE8);
	//Original name: NF05-DIAGNOSIS-CODE9
	private String code9 = DefaultValues.stringVal(Len.CODE9);
	//Original name: NF05-DIAGNOSIS-CODE10
	private String code10 = DefaultValues.stringVal(Len.CODE10);

	//==== METHODS ====
	public byte[] getNf05DiagnosisCodesDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, code1, Len.CODE1);
		position += Len.CODE1;
		MarshalByte.writeString(buffer, position, code2, Len.CODE2);
		position += Len.CODE2;
		MarshalByte.writeString(buffer, position, code3, Len.CODE3);
		position += Len.CODE3;
		MarshalByte.writeString(buffer, position, code4, Len.CODE4);
		position += Len.CODE4;
		MarshalByte.writeString(buffer, position, code5, Len.CODE5);
		position += Len.CODE5;
		MarshalByte.writeString(buffer, position, code6, Len.CODE6);
		position += Len.CODE6;
		MarshalByte.writeString(buffer, position, code7, Len.CODE7);
		position += Len.CODE7;
		MarshalByte.writeString(buffer, position, code8, Len.CODE8);
		position += Len.CODE8;
		MarshalByte.writeString(buffer, position, code9, Len.CODE9);
		position += Len.CODE9;
		MarshalByte.writeString(buffer, position, code10, Len.CODE10);
		return buffer;
	}

	public void setCode1(String code1) {
		this.code1 = Functions.subString(code1, Len.CODE1);
	}

	public String getCode1() {
		return this.code1;
	}

	public void setCode2(String code2) {
		this.code2 = Functions.subString(code2, Len.CODE2);
	}

	public String getCode2() {
		return this.code2;
	}

	public void setCode3(String code3) {
		this.code3 = Functions.subString(code3, Len.CODE3);
	}

	public String getCode3() {
		return this.code3;
	}

	public void setCode4(String code4) {
		this.code4 = Functions.subString(code4, Len.CODE4);
	}

	public String getCode4() {
		return this.code4;
	}

	public void setCode5(String code5) {
		this.code5 = Functions.subString(code5, Len.CODE5);
	}

	public String getCode5() {
		return this.code5;
	}

	public void setCode6(String code6) {
		this.code6 = Functions.subString(code6, Len.CODE6);
	}

	public String getCode6() {
		return this.code6;
	}

	public void setCode7(String code7) {
		this.code7 = Functions.subString(code7, Len.CODE7);
	}

	public String getCode7() {
		return this.code7;
	}

	public void setCode8(String code8) {
		this.code8 = Functions.subString(code8, Len.CODE8);
	}

	public String getCode8() {
		return this.code8;
	}

	public void setCode9(String code9) {
		this.code9 = Functions.subString(code9, Len.CODE9);
	}

	public String getCode9() {
		return this.code9;
	}

	public void setCode10(String code10) {
		this.code10 = Functions.subString(code10, Len.CODE10);
	}

	public String getCode10() {
		return this.code10;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CODE1 = 7;
		public static final int CODE2 = 7;
		public static final int CODE3 = 7;
		public static final int CODE4 = 7;
		public static final int CODE5 = 7;
		public static final int CODE6 = 7;
		public static final int CODE7 = 7;
		public static final int CODE8 = 7;
		public static final int CODE9 = 7;
		public static final int CODE10 = 7;
		public static final int NF05_DIAGNOSIS_CODES_DATA = CODE1 + CODE2 + CODE3 + CODE4 + CODE5 + CODE6 + CODE7 + CODE8 + CODE9 + CODE10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
