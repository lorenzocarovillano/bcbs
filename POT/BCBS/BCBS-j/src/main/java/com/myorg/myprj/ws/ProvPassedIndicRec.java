/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.copy.ProvAddrArea;
import com.myorg.myprj.copy.ProvPassedIndicInput;
import com.myorg.myprj.copy.ProvXrefArea;
import com.myorg.myprj.ws.redefines.ProvName1;

/**Original name: PROV-PASSED-INDIC-REC<br>
 * Variable: PROV-PASSED-INDIC-REC from copybook NU02PROV<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ProvPassedIndicRec extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int PROV_ERR_MSG_MAXOCCURS = 10;
	//Original name: PROV-PASSED-INDIC-INPUT
	private ProvPassedIndicInput provPassedIndicInput = new ProvPassedIndicInput();
	/**Original name: PROV-NAME-1<br>
	 * <pre>-- OUTPUT AREA                                            ------*</pre>*/
	private ProvName1 provName1 = new ProvName1();
	//Original name: PROV-TYPE
	private String provType = DefaultValues.stringVal(Len.PROV_TYPE);
	//Original name: PROV-ADDR-AREA
	private ProvAddrArea provAddrArea = new ProvAddrArea();
	//Original name: PROV-COMM-PAY-IND
	private char provCommPayInd = DefaultValues.CHAR_VAL;
	//Original name: PROV-REP-CD
	private char provRepCd = DefaultValues.CHAR_VAL;
	//Original name: PROV-XREF-AREA
	private ProvXrefArea provXrefArea = new ProvXrefArea();
	//Original name: PROGRAM-STAT
	private String programStat = DefaultValues.stringVal(Len.PROGRAM_STAT);
	//Original name: DATABASE-STAT
	private int databaseStat = DefaultValues.INT_VAL;
	//Original name: PROV-ERR-MSG-LG
	private short provErrMsgLg = ((short) 790);
	//Original name: PROV-ERR-MSG
	private String[] provErrMsg = new String[PROV_ERR_MSG_MAXOCCURS];
	//Original name: PROV-MSG-LG
	private int provMsgLg = 79;
	//Original name: FILLER-PROV-PASSED-INDIC-REC
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: FILLER-PROV-PASSED-INDIC-REC-1
	private String flr2 = DefaultValues.stringVal(Len.FLR2);

	//==== CONSTRUCTORS ====
	public ProvPassedIndicRec() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.PROV_PASSED_INDIC_REC;
	}

	@Override
	public void deserialize(byte[] buf) {
		setProvPassedIndicRecBytes(buf);
	}

	private void init() {
		for (int provErrMsgIdx = 1; provErrMsgIdx <= PROV_ERR_MSG_MAXOCCURS; provErrMsgIdx++) {
			setProvErrMsg(provErrMsgIdx, DefaultValues.stringVal(Len.PROV_ERR_MSG));
		}
	}

	public String getProvPassedIndicRecFormatted() {
		return MarshalByteExt.bufferToStr(getProvPassedIndicRecBytes());
	}

	public void setProvPassedIndicRecBytes(byte[] buffer) {
		setProvPassedIndicRecBytes(buffer, 1);
	}

	public byte[] getProvPassedIndicRecBytes() {
		byte[] buffer = new byte[Len.PROV_PASSED_INDIC_REC];
		return getProvPassedIndicRecBytes(buffer, 1);
	}

	public void setProvPassedIndicRecBytes(byte[] buffer, int offset) {
		int position = offset;
		provPassedIndicInput.setProvPassedIndicInputBytes(buffer, position);
		position += ProvPassedIndicInput.Len.PROV_PASSED_INDIC_INPUT;
		provName1.setProvName1Bytes(buffer, position);
		position += ProvName1.Len.PROV_NAME1;
		provType = MarshalByte.readString(buffer, position, Len.PROV_TYPE);
		position += Len.PROV_TYPE;
		provAddrArea.setProvAddrAreaBytes(buffer, position);
		position += ProvAddrArea.Len.PROV_ADDR_AREA;
		provCommPayInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		provRepCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		provXrefArea.setProvXrefAreaBytes(buffer, position);
		position += ProvXrefArea.Len.PROV_XREF_AREA;
		setIndicStatusCodesBytes(buffer, position);
		position += Len.INDIC_STATUS_CODES;
		setProvErrMsgDataBytes(buffer, position);
		position += Len.PROV_ERR_MSG_DATA;
		provMsgLg = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		flr2 = MarshalByte.readString(buffer, position, Len.FLR2);
	}

	public byte[] getProvPassedIndicRecBytes(byte[] buffer, int offset) {
		int position = offset;
		provPassedIndicInput.getProvPassedIndicInputBytes(buffer, position);
		position += ProvPassedIndicInput.Len.PROV_PASSED_INDIC_INPUT;
		provName1.getProvName1Bytes(buffer, position);
		position += ProvName1.Len.PROV_NAME1;
		MarshalByte.writeString(buffer, position, provType, Len.PROV_TYPE);
		position += Len.PROV_TYPE;
		provAddrArea.getProvAddrAreaBytes(buffer, position);
		position += ProvAddrArea.Len.PROV_ADDR_AREA;
		MarshalByte.writeChar(buffer, position, provCommPayInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, provRepCd);
		position += Types.CHAR_SIZE;
		provXrefArea.getProvXrefAreaBytes(buffer, position);
		position += ProvXrefArea.Len.PROV_XREF_AREA;
		getIndicStatusCodesBytes(buffer, position);
		position += Len.INDIC_STATUS_CODES;
		getProvErrMsgDataBytes(buffer, position);
		position += Len.PROV_ERR_MSG_DATA;
		MarshalByte.writeBinaryInt(buffer, position, provMsgLg);
		position += Types.INT_SIZE;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		return buffer;
	}

	public void initProvPassedIndicRecSpaces() {
		provPassedIndicInput.initProvPassedIndicInputSpaces();
		provName1.initProvName1Spaces();
		provType = "";
		provAddrArea.initProvAddrAreaSpaces();
		provCommPayInd = Types.SPACE_CHAR;
		provRepCd = Types.SPACE_CHAR;
		provXrefArea.initProvXrefAreaSpaces();
		initIndicStatusCodesSpaces();
		initProvErrMsgDataSpaces();
		provMsgLg = Types.INVALID_BINARY_INT_VAL;
		flr1 = Types.SPACE_CHAR;
		flr2 = "";
	}

	public void setProvType(String provType) {
		this.provType = Functions.subString(provType, Len.PROV_TYPE);
	}

	public String getProvType() {
		return this.provType;
	}

	public void setProvCommPayInd(char provCommPayInd) {
		this.provCommPayInd = provCommPayInd;
	}

	public char getProvCommPayInd() {
		return this.provCommPayInd;
	}

	public void setProvRepCd(char provRepCd) {
		this.provRepCd = provRepCd;
	}

	public char getProvRepCd() {
		return this.provRepCd;
	}

	public void setIndicStatusCodesBytes(byte[] buffer, int offset) {
		int position = offset;
		programStat = MarshalByte.readString(buffer, position, Len.PROGRAM_STAT);
		position += Len.PROGRAM_STAT;
		databaseStat = MarshalByte.readInt(buffer, position, Len.DATABASE_STAT);
	}

	public byte[] getIndicStatusCodesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, programStat, Len.PROGRAM_STAT);
		position += Len.PROGRAM_STAT;
		MarshalByte.writeInt(buffer, position, databaseStat, Len.DATABASE_STAT);
		return buffer;
	}

	public void initIndicStatusCodesSpaces() {
		programStat = "";
		databaseStat = Types.INVALID_INT_VAL;
	}

	public void setProgramStat(String programStat) {
		this.programStat = Functions.subString(programStat, Len.PROGRAM_STAT);
	}

	public String getProgramStat() {
		return this.programStat;
	}

	public void setDatabaseStat(int databaseStat) {
		this.databaseStat = databaseStat;
	}

	public int getDatabaseStat() {
		return this.databaseStat;
	}

	public void setProvErrMsgDataBytes(byte[] buffer, int offset) {
		int position = offset;
		provErrMsgLg = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		for (int idx = 1; idx <= PROV_ERR_MSG_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setProvErrMsg(idx, MarshalByte.readString(buffer, position, Len.PROV_ERR_MSG));
				position += Len.PROV_ERR_MSG;
			} else {
				setProvErrMsg(idx, "");
				position += Len.PROV_ERR_MSG;
			}
		}
	}

	public byte[] getProvErrMsgDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, provErrMsgLg);
		position += Types.SHORT_SIZE;
		for (int idx = 1; idx <= PROV_ERR_MSG_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getProvErrMsg(idx), Len.PROV_ERR_MSG);
			position += Len.PROV_ERR_MSG;
		}
		return buffer;
	}

	public void initProvErrMsgDataSpaces() {
		provErrMsgLg = Types.INVALID_BINARY_SHORT_VAL;
		for (int idx = 1; idx <= PROV_ERR_MSG_MAXOCCURS; idx++) {
			provErrMsg[idx - 1] = "";
		}
	}

	public void setProvErrMsgLg(short provErrMsgLg) {
		this.provErrMsgLg = provErrMsgLg;
	}

	public short getProvErrMsgLg() {
		return this.provErrMsgLg;
	}

	public void setProvErrMsg(int provErrMsgIdx, String provErrMsg) {
		this.provErrMsg[provErrMsgIdx - 1] = Functions.subString(provErrMsg, Len.PROV_ERR_MSG);
	}

	public String getProvErrMsg(int provErrMsgIdx) {
		return this.provErrMsg[provErrMsgIdx - 1];
	}

	public String getProvErrMsgFormatted(int provErrMsgIdx) {
		return Functions.padBlanks(getProvErrMsg(provErrMsgIdx), Len.PROV_ERR_MSG);
	}

	public void setProvMsgLg(int provMsgLg) {
		this.provMsgLg = provMsgLg;
	}

	public int getProvMsgLg() {
		return this.provMsgLg;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setFlr2(String flr2) {
		this.flr2 = Functions.subString(flr2, Len.FLR2);
	}

	public String getFlr2() {
		return this.flr2;
	}

	public ProvAddrArea getProvAddrArea() {
		return provAddrArea;
	}

	public ProvName1 getProvName1() {
		return provName1;
	}

	public ProvPassedIndicInput getProvPassedIndicInput() {
		return provPassedIndicInput;
	}

	@Override
	public byte[] serialize() {
		return getProvPassedIndicRecBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROV_TYPE = 4;
		public static final int PROV_COMM_PAY_IND = 1;
		public static final int PROV_REP_CD = 1;
		public static final int PROGRAM_STAT = 3;
		public static final int DATABASE_STAT = 9;
		public static final int INDIC_STATUS_CODES = PROGRAM_STAT + DATABASE_STAT;
		public static final int PROV_ERR_MSG_LG = 2;
		public static final int PROV_ERR_MSG = 79;
		public static final int PROV_ERR_MSG_DATA = PROV_ERR_MSG_LG + ProvPassedIndicRec.PROV_ERR_MSG_MAXOCCURS * PROV_ERR_MSG;
		public static final int PROV_MSG_LG = 4;
		public static final int FLR1 = 1;
		public static final int FLR2 = 41;
		public static final int PROV_PASSED_INDIC_REC = ProvPassedIndicInput.Len.PROV_PASSED_INDIC_INPUT + ProvName1.Len.PROV_NAME1 + PROV_TYPE
				+ ProvAddrArea.Len.PROV_ADDR_AREA + PROV_COMM_PAY_IND + PROV_REP_CD + ProvXrefArea.Len.PROV_XREF_AREA + INDIC_STATUS_CODES
				+ PROV_ERR_MSG_DATA + PROV_MSG_LG + FLR1 + FLR2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
