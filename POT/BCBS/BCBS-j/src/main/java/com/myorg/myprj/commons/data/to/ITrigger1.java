/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [TRIGGER1]
 * 
 */
public interface ITrigger1 extends BaseSqlTo {

	/**
	 * Host Variable TRG-PROD-IND
	 * 
	 */
	String getTrgProdInd();

	void setTrgProdInd(String trgProdInd);

	/**
	 * Host Variable TRG-CLM-CNTL-ID
	 * 
	 */
	String getTrgClmCntlId();

	void setTrgClmCntlId(String trgClmCntlId);

	/**
	 * Host Variable TRG-CLM-CNTL-SFX-ID
	 * 
	 */
	char getTrgClmCntlSfxId();

	void setTrgClmCntlSfxId(char trgClmCntlSfxId);

	/**
	 * Host Variable WS-TRG-PMT-ID
	 * 
	 */
	short getWsTrgPmtId();

	void setWsTrgPmtId(short wsTrgPmtId);

	/**
	 * Host Variable TRG-FNL-BUS-DT
	 * 
	 */
	String getTrgFnlBusDt();

	void setTrgFnlBusDt(String trgFnlBusDt);

	/**
	 * Host Variable TRG-STAT-CATG-CD
	 * 
	 */
	String getTrgStatCatgCd();

	void setTrgStatCatgCd(String trgStatCatgCd);

	/**
	 * Host Variable TRG-BI-PROV-ID
	 * 
	 */
	String getTrgBiProvId();

	void setTrgBiProvId(String trgBiProvId);

	/**
	 * Host Variable TRG-BI-PROV-LOB
	 * 
	 */
	char getTrgBiProvLob();

	void setTrgBiProvLob(char trgBiProvLob);

	/**
	 * Host Variable TRG-BI-SPEC-CD
	 * 
	 */
	String getTrgBiSpecCd();

	void setTrgBiSpecCd(String trgBiSpecCd);

	/**
	 * Host Variable TRG-PE-PROV-ID
	 * 
	 */
	String getTrgPeProvId();

	void setTrgPeProvId(String trgPeProvId);

	/**
	 * Host Variable TRG-PE-PROV-LOB
	 * 
	 */
	char getTrgPeProvLob();

	void setTrgPeProvLob(char trgPeProvLob);

	/**
	 * Host Variable TRG-ALPH-PRFX-CD
	 * 
	 */
	String getTrgAlphPrfxCd();

	void setTrgAlphPrfxCd(String trgAlphPrfxCd);

	/**
	 * Host Variable TRG-INS-ID
	 * 
	 */
	String getTrgInsId();

	void setTrgInsId(String trgInsId);

	/**
	 * Host Variable TRG-MEMBER-ID
	 * 
	 */
	String getTrgMemberId();

	void setTrgMemberId(String trgMemberId);

	/**
	 * Host Variable TRG-CLM-SYST-VER-ID
	 * 
	 */
	String getTrgClmSystVerId();

	void setTrgClmSystVerId(String trgClmSystVerId);

	/**
	 * Host Variable TRG-CORP-RCV-DT
	 * 
	 */
	String getTrgCorpRcvDt();

	void setTrgCorpRcvDt(String trgCorpRcvDt);

	/**
	 * Host Variable TRG-ADJD-DT
	 * 
	 */
	String getTrgAdjdDt();

	void setTrgAdjdDt(String trgAdjdDt);

	/**
	 * Host Variable TRG-BILL-FRM-DT
	 * 
	 */
	String getTrgBillFrmDt();

	void setTrgBillFrmDt(String trgBillFrmDt);

	/**
	 * Host Variable TRG-BILL-THR-DT
	 * 
	 */
	String getTrgBillThrDt();

	void setTrgBillThrDt(String trgBillThrDt);

	/**
	 * Host Variable TRG-ASG-CD
	 * 
	 */
	String getTrgAsgCd();

	void setTrgAsgCd(String trgAsgCd);

	/**
	 * Host Variable TRG-CLM-PD-AM
	 * 
	 */
	AfDecimal getTrgClmPdAm();

	void setTrgClmPdAm(AfDecimal trgClmPdAm);

	/**
	 * Host Variable TRG-KCAPS-TEAM-NM
	 * 
	 */
	String getTrgKcapsTeamNm();

	void setTrgKcapsTeamNm(String trgKcapsTeamNm);

	/**
	 * Host Variable TRG-KCAPS-USE-ID
	 * 
	 */
	String getTrgKcapsUseId();

	void setTrgKcapsUseId(String trgKcapsUseId);

	/**
	 * Host Variable TRG-HIST-LOAD-CD
	 * 
	 */
	char getTrgHistLoadCd();

	void setTrgHistLoadCd(char trgHistLoadCd);

	/**
	 * Host Variable TRG-PMT-BK-PROD-CD
	 * 
	 */
	char getTrgPmtBkProdCd();

	void setTrgPmtBkProdCd(char trgPmtBkProdCd);

	/**
	 * Host Variable TRG-DRG
	 * 
	 */
	String getTrgDrg();

	void setTrgDrg(String trgDrg);

	/**
	 * Host Variable TRG-PAT-ACT-ID
	 * 
	 */
	String getTrgPatActId();

	void setTrgPatActId(String trgPatActId);

	/**
	 * Host Variable TRG-PGM-AREA-CD
	 * 
	 */
	String getTrgPgmAreaCd();

	void setTrgPgmAreaCd(String trgPgmAreaCd);

	/**
	 * Host Variable TRG-FIN-CD
	 * 
	 */
	String getTrgFinCd();

	void setTrgFinCd(String trgFinCd);

	/**
	 * Host Variable TRG-ADJD-PROV-STAT
	 * 
	 */
	char getTrgAdjdProvStat();

	void setTrgAdjdProvStat(char trgAdjdProvStat);

	/**
	 * Host Variable TRG-PRMPT-PAY-DAY
	 * 
	 */
	String getTrgPrmptPayDay();

	void setTrgPrmptPayDay(String trgPrmptPayDay);

	/**
	 * Host Variable TRG-PRMPT-PAY-OVRD
	 * 
	 */
	char getTrgPrmptPayOvrd();

	void setTrgPrmptPayOvrd(char trgPrmptPayOvrd);

	/**
	 * Host Variable TRG-ITS-CLM-TYP-CD
	 * 
	 */
	char getTrgItsClmTypCd();

	void setTrgItsClmTypCd(char trgItsClmTypCd);

	/**
	 * Host Variable TRG-GRP-ID
	 * 
	 */
	String getTrgGrpId();

	void setTrgGrpId(String trgGrpId);

	/**
	 * Host Variable TRG-LOB-CD
	 * 
	 */
	char getTrgLobCd();

	void setTrgLobCd(char trgLobCd);

	/**
	 * Host Variable TRG-TYP-GRP-CD
	 * 
	 */
	String getTrgTypGrpCd();

	void setTrgTypGrpCd(String trgTypGrpCd);

	/**
	 * Host Variable TRG-NPI-CD
	 * 
	 */
	char getTrgNpiCd();

	void setTrgNpiCd(char trgNpiCd);

	/**
	 * Host Variable TRG-BEN-PLN-ID
	 * 
	 */
	String getTrgBenPlnId();

	void setTrgBenPlnId(String trgBenPlnId);

	/**
	 * Host Variable TRG-IRC-CD
	 * 
	 */
	String getTrgIrcCd();

	void setTrgIrcCd(String trgIrcCd);

	/**
	 * Host Variable TRG-NTWRK-CD
	 * 
	 */
	String getTrgNtwrkCd();

	void setTrgNtwrkCd(String trgNtwrkCd);

	/**
	 * Host Variable TRG-PRIM-CN-ARNG-CD
	 * 
	 */
	String getTrgPrimCnArngCd();

	void setTrgPrimCnArngCd(String trgPrimCnArngCd);

	/**
	 * Host Variable TRG-ENR-CL-CD
	 * 
	 */
	String getTrgEnrClCd();

	void setTrgEnrClCd(String trgEnrClCd);

	/**
	 * Host Variable TRG-OI-CD
	 * 
	 */
	char getTrgOiCd();

	void setTrgOiCd(char trgOiCd);

	/**
	 * Host Variable TRG-PAT-RESP
	 * 
	 */
	AfDecimal getTrgPatResp();

	void setTrgPatResp(AfDecimal trgPatResp);

	/**
	 * Host Variable TRG-PWO-AMT
	 * 
	 */
	AfDecimal getTrgPwoAmt();

	void setTrgPwoAmt(AfDecimal trgPwoAmt);

	/**
	 * Host Variable TRG-NCOV-AMT
	 * 
	 */
	AfDecimal getTrgNcovAmt();

	void setTrgNcovAmt(AfDecimal trgNcovAmt);

	/**
	 * Host Variable TRG-PATIENT-NAME
	 * 
	 */
	String getTrgPatientName();

	void setTrgPatientName(String trgPatientName);

	/**
	 * Host Variable TRG-PERFORMING-PROVIDER-NAME
	 * 
	 */
	String getTrgPerformingProviderName();

	void setTrgPerformingProviderName(String trgPerformingProviderName);

	/**
	 * Host Variable TRG-BILLING-PROVIDER-NAME
	 * 
	 */
	String getTrgBillingProviderName();

	void setTrgBillingProviderName(String trgBillingProviderName);

	/**
	 * Host Variable TRG-CLM-INS-LN-CD
	 * 
	 */
	String getTrgClmInsLnCd();

	void setTrgClmInsLnCd(String trgClmInsLnCd);

	/**
	 * Host Variable TRG-ALT-INS-ID
	 * 
	 */
	String getTrgAltInsId();

	void setTrgAltInsId(String trgAltInsId);
};
