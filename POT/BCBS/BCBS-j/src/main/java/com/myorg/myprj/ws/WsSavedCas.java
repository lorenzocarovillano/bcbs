/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-SAVED-CAS<br>
 * Variable: WS-SAVED-CAS from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSavedCas {

	//==== PROPERTIES ====
	//Original name: WS-SAVE-GRP
	private String saveGrp = "";
	//Original name: WS-SAVE-ARC
	private String saveArc = "";
	//Original name: WS-SAVE-AMT
	private AfDecimal saveAmt = new AfDecimal("0", 11, 2);
	//Original name: WS-SAVE-QTY
	private AfDecimal saveQty = new AfDecimal("0", 7, 2);

	//==== METHODS ====
	public void setSaveGrp(String saveGrp) {
		this.saveGrp = Functions.subString(saveGrp, Len.SAVE_GRP);
	}

	public String getSaveGrp() {
		return this.saveGrp;
	}

	public void setSaveArc(String saveArc) {
		this.saveArc = Functions.subString(saveArc, Len.SAVE_ARC);
	}

	public String getSaveArc() {
		return this.saveArc;
	}

	public void setSaveAmt(AfDecimal saveAmt) {
		this.saveAmt.assign(saveAmt);
	}

	public AfDecimal getSaveAmt() {
		return this.saveAmt.copy();
	}

	public void setSaveQty(AfDecimal saveQty) {
		this.saveQty.assign(saveQty);
	}

	public AfDecimal getSaveQty() {
		return this.saveQty.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SAVE_GRP = 2;
		public static final int SAVE_ARC = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
