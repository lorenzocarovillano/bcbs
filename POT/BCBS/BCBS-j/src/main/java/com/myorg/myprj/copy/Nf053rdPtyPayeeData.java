/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: NF05-3RD-PTY-PAYEE-DATA<br>
 * Variable: NF05-3RD-PTY-PAYEE-DATA from copybook NF05EOB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Nf053rdPtyPayeeData {

	//==== PROPERTIES ====
	//Original name: NF05-3RD-PTY-INDC
	private char indc = DefaultValues.CHAR_VAL;
	//Original name: NF05-3RD-PTY-PAYEE-NAME
	private String payeeName = DefaultValues.stringVal(Len.PAYEE_NAME);
	//Original name: NF05-3RD-PTY-PAYEE-NAME2
	private String payeeName2 = DefaultValues.stringVal(Len.PAYEE_NAME2);
	//Original name: NF05-3RD-PTY-PAYEE-STREET
	private String payeeStreet = DefaultValues.stringVal(Len.PAYEE_STREET);
	//Original name: NF05-3RD-PTY-PAYEE-CITY
	private String payeeCity = DefaultValues.stringVal(Len.PAYEE_CITY);
	//Original name: NF05-3RD-PTY-PAYEE-STATE
	private String payeeState = DefaultValues.stringVal(Len.PAYEE_STATE);
	//Original name: NF05-3RD-PTY-PAYEE-ZIP
	private String payeeZip = DefaultValues.stringVal(Len.PAYEE_ZIP);
	//Original name: NF05-3RD-PTY-PAYEE-ZIP-4
	private String payeeZip4 = DefaultValues.stringVal(Len.PAYEE_ZIP4);

	//==== METHODS ====
	public byte[] getNf053rdPtyPayeeDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, indc);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, payeeName, Len.PAYEE_NAME);
		position += Len.PAYEE_NAME;
		MarshalByte.writeString(buffer, position, payeeName2, Len.PAYEE_NAME2);
		position += Len.PAYEE_NAME2;
		MarshalByte.writeString(buffer, position, payeeStreet, Len.PAYEE_STREET);
		position += Len.PAYEE_STREET;
		getPayeeCtyStZipBytes(buffer, position);
		return buffer;
	}

	public void setIndc(char indc) {
		this.indc = indc;
	}

	public char getIndc() {
		return this.indc;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = Functions.subString(payeeName, Len.PAYEE_NAME);
	}

	public String getPayeeName() {
		return this.payeeName;
	}

	public void setPayeeName2(String payeeName2) {
		this.payeeName2 = Functions.subString(payeeName2, Len.PAYEE_NAME2);
	}

	public String getPayeeName2() {
		return this.payeeName2;
	}

	public void setPayeeStreet(String payeeStreet) {
		this.payeeStreet = Functions.subString(payeeStreet, Len.PAYEE_STREET);
	}

	public String getPayeeStreet() {
		return this.payeeStreet;
	}

	public byte[] getPayeeCtyStZipBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, payeeCity, Len.PAYEE_CITY);
		position += Len.PAYEE_CITY;
		MarshalByte.writeString(buffer, position, payeeState, Len.PAYEE_STATE);
		position += Len.PAYEE_STATE;
		MarshalByte.writeString(buffer, position, payeeZip, Len.PAYEE_ZIP);
		position += Len.PAYEE_ZIP;
		MarshalByte.writeString(buffer, position, payeeZip4, Len.PAYEE_ZIP4);
		return buffer;
	}

	public void setPayeeCity(String payeeCity) {
		this.payeeCity = Functions.subString(payeeCity, Len.PAYEE_CITY);
	}

	public String getPayeeCity() {
		return this.payeeCity;
	}

	public void setPayeeState(String payeeState) {
		this.payeeState = Functions.subString(payeeState, Len.PAYEE_STATE);
	}

	public String getPayeeState() {
		return this.payeeState;
	}

	public void setPayeeZip(String payeeZip) {
		this.payeeZip = Functions.subString(payeeZip, Len.PAYEE_ZIP);
	}

	public String getPayeeZip() {
		return this.payeeZip;
	}

	public void setPayeeZip4(String payeeZip4) {
		this.payeeZip4 = Functions.subString(payeeZip4, Len.PAYEE_ZIP4);
	}

	public String getPayeeZip4() {
		return this.payeeZip4;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PAYEE_NAME = 32;
		public static final int PAYEE_NAME2 = 10;
		public static final int PAYEE_STREET = 20;
		public static final int PAYEE_CITY = 18;
		public static final int PAYEE_STATE = 2;
		public static final int PAYEE_ZIP = 5;
		public static final int PAYEE_ZIP4 = 4;
		public static final int INDC = 1;
		public static final int PAYEE_CTY_ST_ZIP = PAYEE_CITY + PAYEE_STATE + PAYEE_ZIP + PAYEE_ZIP4;
		public static final int NF053RD_PTY_PAYEE_DATA = INDC + PAYEE_NAME + PAYEE_NAME2 + PAYEE_STREET + PAYEE_CTY_ST_ZIP;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
