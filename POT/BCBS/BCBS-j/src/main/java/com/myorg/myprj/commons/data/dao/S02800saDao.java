/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS02800sa;

/**
 * Data Access Object(DAO) for table [S02800SA]
 * 
 */
public class S02800saDao extends BaseSqlDao<IS02800sa> {

	public S02800saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02800sa> getToClass() {
		return IS02800sa.class;
	}

	public IS02800sa selectRec(String mCntlId, char mCntlSfxId, short mPmtId, short iId, IS02800sa iS02800sa) {
		return buildQuery("selectRec").bind("mCntlId", mCntlId).bind("mCntlSfxId", String.valueOf(mCntlSfxId)).bind("mPmtId", mPmtId).bind("iId", iId)
				.singleResult(iS02800sa);
	}
}
