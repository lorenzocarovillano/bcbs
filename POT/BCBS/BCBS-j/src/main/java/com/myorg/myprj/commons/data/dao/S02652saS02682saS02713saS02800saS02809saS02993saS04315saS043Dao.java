/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;


import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import com.myorg.myprj.commons.data.to.IS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043;

/**
 * Data Access Object(DAO) for tables [S02652SA, S02682SA, S02713SA, S02800SA, S02809SA, S02993SA, S04315SA, S04316SA, TRIGGER2]
 * 
 */
public class S02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Dao
		extends BaseSqlDao<IS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043> {

	private Cursor claimLineCursor;
	private final IRowMapper<IS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043> fetchClaimLineCursorRm = buildNamedRowMapper(
			IS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043.class, "trigProdInd", "trigHistBillProvNpiId", "trigLocalBillProvLobCd",
			"trigLocalBillProvId", "trigBillProvIdQlfCd", "trigBillProvSpecCd", "trigHistPerfProvNpiId", "trigLocalPerfProvLobCd",
			"trigLocalPerfProvId", "trigInsId", "trigMemberId", "trigCorpRcvDt", "trigPmtAdjdDt", "trigPmtFrmServDt", "trigPmtThrServDt",
			"trigBillFrmDt", "trigBillThrDt", "trigAsgCd", "wsTrigClmPdAm", "trigAdjTypCd", "trigKcapsTeamNm", "trigKcapsUseId", "trigHistLoadCd",
			"trigPmtBkProdCd", "trigDrgCd", "wsTrigSchdlDrgAlwAm", "wsTrigAltDrgAlwAm", "wsTrigOverDrgAlwAm", "trigPmtOiIn", "trigPatActMedRecId",
			"trigPgmAreaCd", "trigGmisIndicator", "trigFinCd", "trigAdjdProvStatCd", "trigItsClmTypCd", "trigPrmptPayDayCd", "trigPrmptPayOvrdCd",
			"wsAccruedPrmptPayIntAm", "trigProvUnwrpDt", "trigGrpId", "trigTypGrpCd", "trigNpiCd", "trigClaimLobCd", "trigRateCd", "trigNtwrkCd",
			"trigBaseCnArngCd", "trigPrimCnArngCd", "trigEnrClCd", "wsTrigLstFnlPmtPtId", "trigStatAdjPrevPmt", "trigEftInd", "trigEftAccountType",
			"trigEftAcct", "trigEftTrans", "trigAlphPrfxCd", "trigGlOfstOrigCd", "wsTrigGlSoteOrigCd", "trigVoidCd", "trigItsInsId", "trigAdjRespCd",
			"trigAdjTrckCd", "trigClmckAdjStatCd", "trig837BillProvNpiId", "trig837PerfProvNpiId", "trigItsCk", "trigDateCoverageLapsed",
			"trigOiPayNm", "trigCorrPrioritySubId", "trigHipaaVersionFormatId", "trigVbrIn", "trigMrktPkgCd", "clmCntlId", "clmCntlSfxId", "clmPmtId",
			"cliId", "provSbmtLnNoId", "addnlRmtLiId", "liFrmServDt", "liThrServDt", "benTypCd", "exmnActnCd", "dnlRmrkCd", "dnlRmrkUsgCd",
			"prmptPayDnlIn", "liExplnCd", "rvwByCd", "procCd", "procMod1Cd", "procMod2Cd", "procMod3Cd", "procMod4Cd", "revCd", "unitCt",
			"origSbmtUnitCt", "posCd", "tsCd", "origSbmtChgAm", "chgAm", "liAlctOiCovAm", "liAlctOiPdAm", "liAlctOiSaveAm", "covPmtLvlSttCd",
			"liAlwChgAm", "fepEdtOvrd1Cd", "fepEdtOvrd2Cd", "fepEdtOvrd3Cd", "adjdOvrd1Cd", "adjdOvrd2Cd", "adjdOvrd3Cd", "adjdOvrd4Cd",
			"adjdOvrd5Cd", "adjdOvrd6Cd", "adjdOvrd7Cd", "adjdOvrd8Cd", "adjdOvrd9Cd", "adjdOvrd10Cd", "crsRfClmCntlId", "csRfClmClSxId",
			"crsRefClmPmtId", "crsRefCliId", "crsRefRsnCd", "dnlRmrkCd", "adjdProcCd", "adjdProcMod1Cd", "adjdProcMod2Cd", "adjdProcMod3Cd",
			"adjdProcMod4Cd", "initClmCntlId", "intClmCnlSfxId", "initClmPmtId", "initClmTs", "actInactCd", "wsXr4CrsRfClmCntlId",
			"wsXr4CsRfClmClSxId", "wsXr4CrsRefClmPmtId", "wsXr4CrsRefCliId", "wsXr4CrsRefRsnCd", "wsXr4DnlRmrkCd", "wsXr4AdjdProcCd",
			"wsXr4AdjdProcMod1Cd", "wsXr4AdjdProcMod2Cd", "wsXr4AdjdProcMod3Cd", "wsXr4AdjdProcMod4Cd", "wsXr4InitClmCntlId", "wsXr4IntClmCnlSfxId",
			"wsXr4InitClmPmtId", "wsXr4Xr3InitClmTs", "wsXr4ActInactCd", "refId", "idCd", "lstOrgNm", "frstNm", "midNm", "sfxNm", "clmSbmtId",
			"mntryAm", "facVlCd", "facQlfCd", "clmFreqTypCd", "clmAdjGrpCd", "clmAdjRsnCd", "mntryAm", "qntyCt", "wsEobAm", "wsHistBundleInd");

	public S02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Dao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043> getToClass() {
		return IS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043.class;
	}

	public DbAccessStatus closeClaimLineCursor() {
		return closeCursor(claimLineCursor);
	}

	public DbAccessStatus openClaimLineCursor() {
		claimLineCursor = buildQuery("openClaimLineCursor").withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public IS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043 fetchClaimLineCursor(
			IS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043 iS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043) {
		return fetch(claimLineCursor, iS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043, fetchClaimLineCursorRm);
	}
}
