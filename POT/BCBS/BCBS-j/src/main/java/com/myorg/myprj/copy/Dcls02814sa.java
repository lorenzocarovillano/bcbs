/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.myorg.myprj.commons.data.to.IS02814sa;

/**Original name: DCLS02814SA<br>
 * Variable: DCLS02814SA from copybook S02814SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02814sa implements IS02814sa {

	//==== PROPERTIES ====
	//Original name: CLM-CNTL-ID
	private String clmCntlId = DefaultValues.stringVal(Len.CLM_CNTL_ID);
	//Original name: CLM-CNTL-SFX-ID
	private char clmCntlSfxId = DefaultValues.CHAR_VAL;
	//Original name: CLM-PMT-ID
	private short clmPmtId = DefaultValues.SHORT_VAL;
	//Original name: EVNT-LOG-STAT-CD
	private String evntLogStatCd = DefaultValues.stringVal(Len.EVNT_LOG_STAT_CD);
	//Original name: EVNT-LOG-BUS-DT
	private String evntLogBusDt = DefaultValues.stringVal(Len.EVNT_LOG_BUS_DT);
	//Original name: EVNT-LOG-SYST-DT
	private String evntLogSystDt = DefaultValues.stringVal(Len.EVNT_LOG_SYST_DT);
	//Original name: KCAPS-TEAM-NM
	private String kcapsTeamNm = DefaultValues.stringVal(Len.KCAPS_TEAM_NM);
	//Original name: KCAPS-USE-ID
	private String kcapsUseId = DefaultValues.stringVal(Len.KCAPS_USE_ID);

	//==== METHODS ====
	@Override
	public void setClmCntlId(String clmCntlId) {
		this.clmCntlId = Functions.subString(clmCntlId, Len.CLM_CNTL_ID);
	}

	@Override
	public String getClmCntlId() {
		return this.clmCntlId;
	}

	@Override
	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.clmCntlSfxId = clmCntlSfxId;
	}

	@Override
	public char getClmCntlSfxId() {
		return this.clmCntlSfxId;
	}

	@Override
	public void setClmPmtId(short clmPmtId) {
		this.clmPmtId = clmPmtId;
	}

	@Override
	public short getClmPmtId() {
		return this.clmPmtId;
	}

	@Override
	public void setEvntLogStatCd(String evntLogStatCd) {
		this.evntLogStatCd = Functions.subString(evntLogStatCd, Len.EVNT_LOG_STAT_CD);
	}

	@Override
	public String getEvntLogStatCd() {
		return this.evntLogStatCd;
	}

	public String getEvntLogStatCdFormatted() {
		return Functions.padBlanks(getEvntLogStatCd(), Len.EVNT_LOG_STAT_CD);
	}

	@Override
	public void setEvntLogBusDt(String evntLogBusDt) {
		this.evntLogBusDt = Functions.subString(evntLogBusDt, Len.EVNT_LOG_BUS_DT);
	}

	@Override
	public String getEvntLogBusDt() {
		return this.evntLogBusDt;
	}

	@Override
	public void setEvntLogSystDt(String evntLogSystDt) {
		this.evntLogSystDt = Functions.subString(evntLogSystDt, Len.EVNT_LOG_SYST_DT);
	}

	@Override
	public String getEvntLogSystDt() {
		return this.evntLogSystDt;
	}

	@Override
	public void setKcapsTeamNm(String kcapsTeamNm) {
		this.kcapsTeamNm = Functions.subString(kcapsTeamNm, Len.KCAPS_TEAM_NM);
	}

	@Override
	public String getKcapsTeamNm() {
		return this.kcapsTeamNm;
	}

	@Override
	public void setKcapsUseId(String kcapsUseId) {
		this.kcapsUseId = Functions.subString(kcapsUseId, Len.KCAPS_USE_ID);
	}

	@Override
	public String getKcapsUseId() {
		return this.kcapsUseId;
	}

	@Override
	public String getDb2PmtKcapsTeamNm() {
		throw new FieldNotMappedException("db2PmtKcapsTeamNm");
	}

	@Override
	public void setDb2PmtKcapsTeamNm(String db2PmtKcapsTeamNm) {
		throw new FieldNotMappedException("db2PmtKcapsTeamNm");
	}

	@Override
	public String getDb2PmtKcapsUseId() {
		throw new FieldNotMappedException("db2PmtKcapsUseId");
	}

	@Override
	public void setDb2PmtKcapsUseId(String db2PmtKcapsUseId) {
		throw new FieldNotMappedException("db2PmtKcapsUseId");
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_CNTL_ID = 12;
		public static final int EVNT_LOG_STAT_CD = 15;
		public static final int EVNT_LOG_BUS_DT = 10;
		public static final int EVNT_LOG_SYST_DT = 10;
		public static final int KCAPS_TEAM_NM = 5;
		public static final int KCAPS_USE_ID = 3;
		public static final int INFO_CHG_TS = 26;
		public static final int INFO_SSYST_ID = 4;
		public static final int INFO_INSRT_PGM_NM = 8;
		public static final int INFO_CHG_ID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
