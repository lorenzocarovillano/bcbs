/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SBR-ITS-CLAIM-TYPE<br>
 * Variable: SBR-ITS-CLAIM-TYPE from copybook NF05SUBR<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SbrItsClaimType {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NON_ITS_CLAIM = 'K';
	public static final char ITS_HOME_CLAIM = 'M';
	public static final char ITS_HOST_CLAIM = 'S';

	//==== METHODS ====
	public void setItsClaimType(char itsClaimType) {
		this.value = itsClaimType;
	}

	public char getItsClaimType() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
