/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import com.myorg.myprj.commons.data.to.IS02800ca;

/**
 * Data Access Object(DAO) for table [S02800CA]
 * 
 */
public class S02800caDao extends BaseSqlDao<IS02800ca> {

	private final IRowMapper<IS02800ca> selectRecRm = buildNamedRowMapper(IS02800ca.class, "ncovAm", "eobDedAm", "eobCoinsAm", "eobCopayAm",
			"eobPenaltyAm");

	public S02800caDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02800ca> getToClass() {
		return IS02800ca.class;
	}

	public IS02800ca selectRec(String mCntlId, char mCntlSfxId, short mPmtId, short iId, IS02800ca iS02800ca) {
		return buildQuery("selectRec").bind("mCntlId", mCntlId).bind("mCntlSfxId", String.valueOf(mCntlSfxId)).bind("mPmtId", mPmtId).bind("iId", iId)
				.rowMapper(selectRecRm).singleResult(iS02800ca);
	}
}
