/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: PICSTR<br>
 * Variable: PICSTR from program NF0735ML<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Picstr extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: PICSTR-LENGTH
	private short length2 = DefaultValues.BIN_SHORT_VAL;
	//Original name: PICSTR-STRING
	private String stringFld = "";

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.PICSTR;
	}

	@Override
	public void deserialize(byte[] buf) {
		setPicstrBytes(buf);
	}

	public String getPicstrFormatted() {
		byte[] buffer = new byte[Len.PICSTR];
		return MarshalByteExt.bufferToStr(getPicstrBytes(buffer, 1));
	}

	public void setPicstrBytes(byte[] buffer) {
		setPicstrBytes(buffer, 1);
	}

	public byte[] getPicstrBytes() {
		byte[] buffer = new byte[Len.PICSTR];
		return getPicstrBytes(buffer, 1);
	}

	public void setPicstrBytes(byte[] buffer, int offset) {
		int position = offset;
		length2 = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		stringFld = MarshalByte.readString(buffer, position, Len.STRING_FLD);
	}

	public byte[] getPicstrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, length2);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, stringFld, Len.STRING_FLD);
		return buffer;
	}

	public void setLength2(short length2) {
		this.length2 = length2;
	}

	public short getLength2() {
		return this.length2;
	}

	public String getLength2Formatted() {
		return PicFormatter.display(new PicParams("S9(4)").setUsage(PicUsage.BINARY)).format(getLength2()).toString();
	}

	public String getLength2AsString() {
		return getLength2Formatted();
	}

	public void setStringFld(String stringFld) {
		this.stringFld = Functions.subString(stringFld, Len.STRING_FLD);
	}

	public String getStringFld() {
		return this.stringFld;
	}

	public String getStringFldFormatted() {
		return Functions.padBlanks(getStringFld(), Len.STRING_FLD);
	}

	@Override
	public byte[] serialize() {
		return getPicstrBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LENGTH2 = 2;
		public static final int STRING_FLD = 300;
		public static final int PICSTR = LENGTH2 + STRING_FLD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
