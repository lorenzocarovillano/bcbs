/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [S02801SA]
 * 
 */
public interface IS02801sa extends BaseSqlTo {

	/**
	 * Host Variable CLM-CNTL-ID
	 * 
	 */
	String getClmCntlId();

	void setClmCntlId(String clmCntlId);

	/**
	 * Host Variable CLM-CNTL-SFX-ID
	 * 
	 */
	char getClmCntlSfxId();

	void setClmCntlSfxId(char clmCntlSfxId);

	/**
	 * Host Variable CLM-PMT-ID
	 * 
	 */
	short getClmPmtId();

	void setClmPmtId(short clmPmtId);

	/**
	 * Host Variable HCC-STAT-CATG-CD
	 * 
	 */
	String getHccStatCatgCd();

	void setHccStatCatgCd(String hccStatCatgCd);

	/**
	 * Host Variable FNL-BUS-DT
	 * 
	 */
	String getFnlBusDt();

	void setFnlBusDt(String fnlBusDt);
};
