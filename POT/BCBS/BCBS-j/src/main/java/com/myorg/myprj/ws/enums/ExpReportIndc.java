/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EXP-REPORT-INDC<br>
 * Variable: EXP-REPORT-INDC from copybook NF06EXP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class ExpReportIndc {

	//==== PROPERTIES ====
	public String value = DefaultValues.stringVal(Len.VALUE);
	public static final String NO_REPORT = "00";
	public static final String SUM_OF_CLM = "01";
	public static final String ADJ_TYP_R = "02";
	public static final String ADJ_TYP_S = "03";
	public static final String ADJ_TYP_BH = "04";
	public static final String HST_LD_ADJ_A = "05";
	public static final String SALINA_PHO = "06";
	public static final String HST_LD_I = "07";
	public static final String HST_LD_ADJ_L = "08";
	public static final String HST_LD_L = "09";
	public static final String HST_LD_ADJ_M = "10";
	public static final String HST_LD_M = "11";
	public static final String HST_LD_ADJ_R = "12";
	public static final String HST_LD_R = "13";
	public static final String HST_LD_ADJ_D = "14";
	public static final String HST_LD_D = "15";
	public static final String ITS_EXPENSE = "16";

	//==== METHODS ====
	public void setExpReportIndc(short expReportIndc) {
		this.value = NumericDisplay.asString(expReportIndc, Len.VALUE);
	}

	public void setExpReportIndcFormatted(String expReportIndc) {
		this.value = Trunc.toUnsignedNumeric(expReportIndc, Len.VALUE);
	}

	public short getExpReportIndc() {
		return NumericDisplay.asShort(this.value);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
