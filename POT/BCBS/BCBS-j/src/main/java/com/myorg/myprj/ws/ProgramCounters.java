/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: PROGRAM-COUNTERS<br>
 * Variable: PROGRAM-COUNTERS from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ProgramCounters {

	//==== PROPERTIES ====
	//Original name: WS-ORIG-REPAY-CNT
	private String wsOrigRepayCnt = "000000";
	//Original name: WS-VOID-RECORDS-CNT
	private String wsVoidRecordsCnt = "000000";
	//Original name: WS-NPI-HIST-CNT
	private int wsNpiHistCnt = 0;
	//Original name: WS-NPI-TXNS-CNT
	private int wsNpiTxnsCnt = 0;
	//Original name: CHKP-UOW-CNTR
	private int chkpUowCntr = 0;
	//Original name: CURR-TRIG-PROCESSED-CNT
	private String currTrigProcessedCnt = "000000";
	//Original name: TRIGGER-RECORDS-IN
	private String triggerRecordsIn = "000000";
	//Original name: COMMIT-CNT
	private String commitCnt = "000000";
	//Original name: DISPLAY-TIME-INTERVAL
	private int displayTimeInterval = 0;
	//Original name: STATS-CNT
	private int statsCnt = 0;
	//Original name: ORIG-PMTS-CNTR
	private int origPmtsCntr = 0;
	//Original name: VOID-PMTS-CNTR
	private String voidPmtsCntr = "000000";

	//==== METHODS ====
	public void setWsOrigRepayCnt(int wsOrigRepayCnt) {
		this.wsOrigRepayCnt = NumericDisplay.asString(wsOrigRepayCnt, Len.WS_ORIG_REPAY_CNT);
	}

	public int getWsOrigRepayCnt() {
		return NumericDisplay.asInt(this.wsOrigRepayCnt);
	}

	public String getWsOrigRepayCntFormatted() {
		return this.wsOrigRepayCnt;
	}

	public String getWsOrigRepayCntAsString() {
		return getWsOrigRepayCntFormatted();
	}

	public void setWsVoidRecordsCnt(int wsVoidRecordsCnt) {
		this.wsVoidRecordsCnt = NumericDisplay.asString(wsVoidRecordsCnt, Len.WS_VOID_RECORDS_CNT);
	}

	public int getWsVoidRecordsCnt() {
		return NumericDisplay.asInt(this.wsVoidRecordsCnt);
	}

	public String getWsVoidRecordsCntFormatted() {
		return this.wsVoidRecordsCnt;
	}

	public String getWsVoidRecordsCntAsString() {
		return getWsVoidRecordsCntFormatted();
	}

	public void setWsNpiHistCnt(int wsNpiHistCnt) {
		this.wsNpiHistCnt = wsNpiHistCnt;
	}

	public int getWsNpiHistCnt() {
		return this.wsNpiHistCnt;
	}

	public void setWsNpiTxnsCnt(int wsNpiTxnsCnt) {
		this.wsNpiTxnsCnt = wsNpiTxnsCnt;
	}

	public int getWsNpiTxnsCnt() {
		return this.wsNpiTxnsCnt;
	}

	public void setChkpUowCntr(int chkpUowCntr) {
		this.chkpUowCntr = chkpUowCntr;
	}

	public int getChkpUowCntr() {
		return this.chkpUowCntr;
	}

	public void setCurrTrigProcessedCnt(int currTrigProcessedCnt) {
		this.currTrigProcessedCnt = NumericDisplay.asString(currTrigProcessedCnt, Len.CURR_TRIG_PROCESSED_CNT);
	}

	public int getCurrTrigProcessedCnt() {
		return NumericDisplay.asInt(this.currTrigProcessedCnt);
	}

	public String getCurrTrigProcessedCntFormatted() {
		return this.currTrigProcessedCnt;
	}

	public String getCurrTrigProcessedCntAsString() {
		return getCurrTrigProcessedCntFormatted();
	}

	public void setTriggerRecordsIn(int triggerRecordsIn) {
		this.triggerRecordsIn = NumericDisplay.asString(triggerRecordsIn, Len.TRIGGER_RECORDS_IN);
	}

	public int getTriggerRecordsIn() {
		return NumericDisplay.asInt(this.triggerRecordsIn);
	}

	public String getTriggerRecordsInFormatted() {
		return this.triggerRecordsIn;
	}

	public String getTriggerRecordsInAsString() {
		return getTriggerRecordsInFormatted();
	}

	public void setCommitCnt(int commitCnt) {
		this.commitCnt = NumericDisplay.asString(commitCnt, Len.COMMIT_CNT);
	}

	public int getCommitCnt() {
		return NumericDisplay.asInt(this.commitCnt);
	}

	public String getCommitCntFormatted() {
		return this.commitCnt;
	}

	public String getCommitCntAsString() {
		return getCommitCntFormatted();
	}

	public void setDisplayTimeInterval(int displayTimeInterval) {
		this.displayTimeInterval = displayTimeInterval;
	}

	public int getDisplayTimeInterval() {
		return this.displayTimeInterval;
	}

	public void setStatsCnt(int statsCnt) {
		this.statsCnt = statsCnt;
	}

	public int getStatsCnt() {
		return this.statsCnt;
	}

	public void setOrigPmtsCntr(int origPmtsCntr) {
		this.origPmtsCntr = origPmtsCntr;
	}

	public int getOrigPmtsCntr() {
		return this.origPmtsCntr;
	}

	public void setVoidPmtsCntr(int voidPmtsCntr) {
		this.voidPmtsCntr = NumericDisplay.asString(voidPmtsCntr, Len.VOID_PMTS_CNTR);
	}

	public int getVoidPmtsCntr() {
		return NumericDisplay.asInt(this.voidPmtsCntr);
	}

	public String getVoidPmtsCntrFormatted() {
		return this.voidPmtsCntr;
	}

	public String getVoidPmtsCntrAsString() {
		return getVoidPmtsCntrFormatted();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_ORIG_REPAY_CNT = 6;
		public static final int WS_VOID_RECORDS_CNT = 6;
		public static final int WS_NPI_HIST_CNT = 6;
		public static final int WS_NPI_TXNS_CNT = 6;
		public static final int CHKP_UOW_CNTR = 6;
		public static final int CURR_TRIG_PROCESSED_CNT = 6;
		public static final int TRIGGER_RECORDS_IN = 6;
		public static final int COMMIT_CNT = 6;
		public static final int DISPLAY_TIME_INTERVAL = 6;
		public static final int STATS_CNT = 6;
		public static final int ORIG_PMTS_CNTR = 6;
		public static final int VOID_PMTS_CNTR = 6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
