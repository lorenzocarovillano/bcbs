/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: CALC-RECORD-KEY<br>
 * Variable: CALC-RECORD-KEY from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CalcRecordKey {

	//==== PROPERTIES ====
	//Original name: WS-CALC-CLM-CNTL-ID
	private String cntlId = "";
	//Original name: WS-CALC-CLM-SFX-ID
	private char sfxId = Types.SPACE_CHAR;
	//Original name: WS-CALC-CLM-PMT-NUM
	private short pmtNum = ((short) 0);
	//Original name: WS-CALC-CLM-LI-ID-PACKED
	private short liIdPacked = ((short) 0);

	//==== METHODS ====
	public void setCntlId(String cntlId) {
		this.cntlId = Functions.subString(cntlId, Len.CNTL_ID);
	}

	public String getCntlId() {
		return this.cntlId;
	}

	public String getCntlIdFormatted() {
		return Functions.padBlanks(getCntlId(), Len.CNTL_ID);
	}

	public void setSfxId(char sfxId) {
		this.sfxId = sfxId;
	}

	public char getSfxId() {
		return this.sfxId;
	}

	public void setPmtNum(short pmtNum) {
		this.pmtNum = pmtNum;
	}

	public short getPmtNum() {
		return this.pmtNum;
	}

	public String getPmtNumFormatted() {
		return PicFormatter.display(new PicParams("S9(2)V").setUsage(PicUsage.PACKED)).format(getPmtNum()).toString();
	}

	public String getPmtNumAsString() {
		return getPmtNumFormatted();
	}

	public void setLiIdPacked(short liIdPacked) {
		this.liIdPacked = liIdPacked;
	}

	public short getLiIdPacked() {
		return this.liIdPacked;
	}

	public String getLiIdPackedFormatted() {
		return PicFormatter.display(new PicParams("S9(3)").setUsage(PicUsage.PACKED)).format(getLiIdPacked()).toString();
	}

	public String getLiIdPackedAsString() {
		return getLiIdPackedFormatted();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CNTL_ID = 12;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
