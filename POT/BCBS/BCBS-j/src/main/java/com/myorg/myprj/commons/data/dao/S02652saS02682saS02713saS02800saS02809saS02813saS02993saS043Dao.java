/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;


import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import com.myorg.myprj.commons.data.to.IS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043;

/**
 * Data Access Object(DAO) for tables [S02652SA, S02682SA, S02713SA, S02800SA, S02809SA, S02813SA, S02993SA, S04315SA, TRIGGER2]
 * 
 */
public class S02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Dao
		extends BaseSqlDao<IS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043> {

	private Cursor voidLineCursor;
	private final IRowMapper<IS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043> fetchVoidLineCursorRm = buildNamedRowMapper(
			IS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043.class, "trigGmisIndicator", "trigProdInd", "trigHistBillProvNpiId",
			"trigLocalBillProvLobCd", "trigLocalBillProvId", "trigBillProvIdQlfCd", "trigBillProvSpecCd", "trigHistPerfProvNpiId",
			"trigLocalPerfProvLobCd", "trigLocalPerfProvId", "trigInsId", "trigMemberId", "trigCorpRcvDt", "trigPmtAdjdDt", "trigPmtFrmServDt",
			"trigPmtThrServDt", "trigBillFrmDt", "trigBillThrDt", "trigAsgCd", "wsTrigClmPdAm", "trigAdjTypCd", "trigKcapsTeamNm", "trigKcapsUseId",
			"trigHistLoadCd", "trigPmtBkProdCd", "trigDrgCd", "wsTrigSchdlDrgAlwAm", "wsTrigAltDrgAlwAm", "wsTrigOverDrgAlwAm", "trigPmtOiIn",
			"trigPatActMedRecId", "trigPgmAreaCd", "trigFinCd", "trigAdjdProvStatCd", "trigItsClmTypCd", "trigPrmptPayDayCd", "trigPrmptPayOvrdCd",
			"wsAccruedPrmptPayIntAm", "trigProvUnwrpDt", "trigGrpId", "trigTypGrpCd", "trigNpiCd", "trigClaimLobCd", "trigRateCd", "trigNtwrkCd",
			"trigBaseCnArngCd", "trigPrimCnArngCd", "trigEnrClCd", "wsTrigLstFnlPmtPtId", "trigStatAdjPrevPmt", "trigEftInd", "trigEftAccountType",
			"trigEftAcct", "trigEftTrans", "trigAlphPrfxCd", "trigGlOfstOrigCd", "wsTrigGlSoteOrigCd", "trigVoidCd", "trigItsInsId", "trigAdjRespCd",
			"trigAdjTrckCd", "trigClmckAdjStatCd", "trig837BillProvNpiId", "trig837PerfProvNpiId", "trigItsCk", "trigDateCoverageLapsed",
			"trigOiPayNm", "trigCorrPrioritySubId", "trigHipaaVersionFormatId", "vbrIn", "mrktPkgCd", "clmCntlId", "clmCntlSfxId", "clmPmtId",
			"memId", "clmPdAm", "adjTypCd", "kcapsTeamNm", "kcapsUseId", "histLoadCd", "pgmAreaCd", "finCd", "itsClmTypCd", "typGrpCd", "npiCd",
			"lobCd", "ntwrkCd", "enrClCd", "glOfstVoidCd", "glSoteVoidCd", "voidCd", "drgCd", "schdlDrgAlwAm", "adjRespCd", "adjTrckCd",
			"prmptPayIntAm", "cliId", "provSbmtLnNoId", "addnlRmtLiId", "liFrmServDt", "liThrServDt", "exmnActnCd", "dnlRmrkCd", "dnlRmrkUsgCd",
			"procCd", "procMod1Cd", "procMod2Cd", "procMod3Cd", "procMod4Cd", "revCd", "unitCt", "origSbmtUnitCt", "origSbmtChgAm", "chgAm",
			"liAlctOiPdAm", "tsCd", "adjdOvrd1Cd", "adjdOvrd2Cd", "adjdOvrd3Cd", "adjdOvrd4Cd", "adjdOvrd5Cd", "adjdOvrd6Cd", "adjdOvrd7Cd",
			"adjdOvrd8Cd", "adjdOvrd9Cd", "adjdOvrd10Cd", "crsRfClmCntlId", "csRfClmClSxId", "crsRefClmPmtId", "crsRefCliId", "crsRefRsnCd",
			"dnlRmrkCd", "adjdProcCd", "adjdProcMod1Cd", "adjdProcMod2Cd", "adjdProcMod3Cd", "adjdProcMod4Cd", "actInactCd", "wsVr4CrsRfClmCntlId",
			"wsVr4CsRfClmClSxId", "wsVr4CrsRefClmPmtId", "wsVr4CrsRefCliId", "wsVr4CrsRefRsnCd", "wsVr4DnlRmrkCd", "wsVr4AdjdProcCd",
			"wsVr4AdjdProcMod1Cd", "wsVr4AdjdProcMod2Cd", "wsVr4AdjdProcMod3Cd", "wsVr4AdjdProcMod4Cd", "wsVr4InitClmCntlId", "wsVr4IntClmCnlSfxId",
			"wsVr4InitClmPmtId", "wsVr4ActInactCd", "refId", "idCd", "lstOrgNm", "frstNm", "midNm", "sfxNm", "clmSbmtId", "facVlCd", "clmFreqTypCd",
			"clmAdjGrpCd", "clmAdjRsnCd", "mntryAm", "qntyCt");

	public S02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Dao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043> getToClass() {
		return IS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043.class;
	}

	public DbAccessStatus closeVoidLineCursor() {
		return closeCursor(voidLineCursor);
	}

	public DbAccessStatus openVoidLineCursor() {
		voidLineCursor = buildQuery("openVoidLineCursor").withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public IS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043 fetchVoidLineCursor(
			IS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043 iS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043) {
		return fetch(voidLineCursor, iS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043, fetchVoidLineCursorRm);
	}
}
