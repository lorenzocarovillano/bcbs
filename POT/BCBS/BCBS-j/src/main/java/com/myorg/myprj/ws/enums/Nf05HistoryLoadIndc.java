/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: NF05-HISTORY-LOAD-INDC<br>
 * Variable: NF05-HISTORY-LOAD-INDC from copybook NF05EOB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Nf05HistoryLoadIndc {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	private static final char[] HIST_LOAD_VALUES = new char[] { ' ', 'A', 'D', 'I', 'K', 'L', 'L', 'R' };
	public static final char NOT_HIST_LOAD = ' ';
	public static final char HST_LOAD_NEAR_SITE = 'K';
	public static final char HST_LOAD_MEDICARE = 'M';
	public static final char HST_LOAD_WITHOUT_CHECK = 'A';
	public static final char HST_LOAD_WITH_TV_CHECK = 'L';
	public static final char HST_LOAD_INTRPLAN_BANK = 'I';
	public static final char HST_LOAD_RECIPROCITY = 'R';
	public static final char HST_LOAD_DRUGS = 'D';

	//==== METHODS ====
	public void setNf05HistoryLoadIndc(char nf05HistoryLoadIndc) {
		this.value = nf05HistoryLoadIndc;
	}

	public char getNf05HistoryLoadIndc() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
