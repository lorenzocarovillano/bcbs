/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.occurs.WsNonPrtRmksTable;

/**Original name: WS-NON-PRT-RMKS-VARIABLES<br>
 * Variable: WS-NON-PRT-RMKS-VARIABLES from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsNonPrtRmksVariables {

	//==== PROPERTIES ====
	public static final int NON_PRT_RMKS_TABLE_MAXOCCURS = 500;
	//Original name: WS-NON-PRT-RMKS-TABLE
	private LazyArrayCopy<WsNonPrtRmksTable> nonPrtRmksTable = new LazyArrayCopy<WsNonPrtRmksTable>(new WsNonPrtRmksTable(), 1,
			NON_PRT_RMKS_TABLE_MAXOCCURS);
	//Original name: WS-NON-PRT-RMKS-TBL-ENTRIES
	private short nonPrtRmksTblEntries = DefaultValues.SHORT_VAL;
	//Original name: WS-SUB-NON-PRT-RMKS
	private short subNonPrtRmks = DefaultValues.SHORT_VAL;
	//Original name: WS-NON-PRT-RMKS-MAX
	private short nonPrtRmksMax = ((short) 500);
	//Original name: WS-NON-PRT-RMKS-FINISHED
	private char nonPrtRmksFinished = DefaultValues.CHAR_VAL;
	//Original name: WS-NON-PRT-RMKS-FOUND
	private char nonPrtRmksFound = DefaultValues.CHAR_VAL;
	//Original name: WS-HOLD-REMARK-CODE
	private String holdRemarkCode = DefaultValues.stringVal(Len.HOLD_REMARK_CODE);
	//Original name: WS-HOLD-ASSIGN-CODE
	private String holdAssignCode = DefaultValues.stringVal(Len.HOLD_ASSIGN_CODE);

	//==== METHODS ====
	public void setNonPrtRmksTblEntries(short nonPrtRmksTblEntries) {
		this.nonPrtRmksTblEntries = nonPrtRmksTblEntries;
	}

	public short getNonPrtRmksTblEntries() {
		return this.nonPrtRmksTblEntries;
	}

	public void setSubNonPrtRmks(short subNonPrtRmks) {
		this.subNonPrtRmks = subNonPrtRmks;
	}

	public short getSubNonPrtRmks() {
		return this.subNonPrtRmks;
	}

	public short getNonPrtRmksMax() {
		return this.nonPrtRmksMax;
	}

	public void setNonPrtRmksFinished(char nonPrtRmksFinished) {
		this.nonPrtRmksFinished = nonPrtRmksFinished;
	}

	public void setNonPrtRmksFinishedFormatted(String nonPrtRmksFinished) {
		setNonPrtRmksFinished(Functions.charAt(nonPrtRmksFinished, Types.CHAR_SIZE));
	}

	public char getNonPrtRmksFinished() {
		return this.nonPrtRmksFinished;
	}

	public void setNonPrtRmksFound(char nonPrtRmksFound) {
		this.nonPrtRmksFound = nonPrtRmksFound;
	}

	public void setNonPrtRmksFoundFormatted(String nonPrtRmksFound) {
		setNonPrtRmksFound(Functions.charAt(nonPrtRmksFound, Types.CHAR_SIZE));
	}

	public char getNonPrtRmksFound() {
		return this.nonPrtRmksFound;
	}

	public void setHoldRemarkCode(String holdRemarkCode) {
		this.holdRemarkCode = Functions.subString(holdRemarkCode, Len.HOLD_REMARK_CODE);
	}

	public String getHoldRemarkCode() {
		return this.holdRemarkCode;
	}

	public void setHoldAssignCode(String holdAssignCode) {
		this.holdAssignCode = Functions.subString(holdAssignCode, Len.HOLD_ASSIGN_CODE);
	}

	public String getHoldAssignCode() {
		return this.holdAssignCode;
	}

	public WsNonPrtRmksTable getNonPrtRmksTable(int idx) {
		return nonPrtRmksTable.get(idx - 1);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NON_PRT_RMKS_TBL_ENTRIES = 4;
		public static final int CNTR_NON_PRT_RMKS = 4;
		public static final int HOLD_REMARK_CODE = 2;
		public static final int HOLD_ASSIGN_CODE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
