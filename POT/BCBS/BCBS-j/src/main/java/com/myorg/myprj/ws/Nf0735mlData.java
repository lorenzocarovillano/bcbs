/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.AfSystem;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.S02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Nf0735ml;
import com.myorg.myprj.S02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Nf0735ml;
import com.myorg.myprj.commons.data.to.ITrigger2;
import com.myorg.myprj.copy.CsrCalcInformation;
import com.myorg.myprj.copy.CsrPmtCasInfo;
import com.myorg.myprj.copy.CsrPmtCommonInfo;
import com.myorg.myprj.copy.CsrXrefIncd;
import com.myorg.myprj.copy.Dcls02652sa;
import com.myorg.myprj.copy.Dcls02682sa;
import com.myorg.myprj.copy.Dcls02713sa;
import com.myorg.myprj.copy.Dcls02717sa;
import com.myorg.myprj.copy.Dcls02718sa;
import com.myorg.myprj.copy.Dcls02719sa;
import com.myorg.myprj.copy.Dcls02800sa;
import com.myorg.myprj.copy.Dcls02809sa;
import com.myorg.myprj.copy.Dcls02813sa;
import com.myorg.myprj.copy.Dcls02952sa;
import com.myorg.myprj.copy.Dcls02993sa;
import com.myorg.myprj.copy.Dcls03086sa;
import com.myorg.myprj.copy.Dcls04315sa;
import com.myorg.myprj.copy.Nf07areaofCsrProgramLineArea;
import com.myorg.myprj.copy.Nf07areaofVsrProgramLineArea;
import com.myorg.myprj.ws.occurs.CsrXrefArea;
import com.myorg.myprj.ws.redefines.TrigRecordLayout;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program NF0735ML<br>
 * Generated as a class for rule WS.<br>*/
public class Nf0735mlData implements ITrigger2 {

	//==== PROPERTIES ====
	private S02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Nf0735ml s02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Nf0735ml = new S02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Nf0735ml(
			this);
	private S02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Nf0735ml s02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Nf0735ml = new S02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Nf0735ml(
			this);
	//Original name: WS-PGM-NAME
	private String wsPgmName = "NF0735ML";
	//Original name: ABEND-CODE
	private int abendCode = 0;
	//Original name: WS-HOLD-TRIG-HEADER-AREA
	private WsHoldTrigHeaderArea wsHoldTrigHeaderArea = new WsHoldTrigHeaderArea();
	//Original name: PROGRAM-COUNTERS
	private ProgramCounters programCounters = new ProgramCounters();
	//Original name: PROGRAM-HOLD-AREAS
	private ProgramHoldAreas programHoldAreas = new ProgramHoldAreas();
	//Original name: WS-XR4-CRS-REF-CLM-PMT-1
	private String wsXr4CrsRefClmPmt1 = DefaultValues.stringVal(Len.WS_XR4_CRS_REF_CLM_PMT1);
	//Original name: WS-XR4-CRS-REF-CLM-PMT-2
	private String wsXr4CrsRefClmPmt2 = DefaultValues.stringVal(Len.WS_XR4_CRS_REF_CLM_PMT2);
	//Original name: WS-XR4-CRS-REF-CLI-USE
	private WsXr4CrsRefCliUse wsXr4CrsRefCliUse = new WsXr4CrsRefCliUse();
	//Original name: WS-VR4-CRS-REF-CLM-PMT-1
	private String wsVr4CrsRefClmPmt1 = DefaultValues.stringVal(Len.WS_VR4_CRS_REF_CLM_PMT1);
	//Original name: WS-VR4-CRS-REF-CLM-PMT-2
	private String wsVr4CrsRefClmPmt2 = DefaultValues.stringVal(Len.WS_VR4_CRS_REF_CLM_PMT2);
	//Original name: WS-VR4-CRS-REF-CLI-USE
	private WsVr4CrsRefCliUse wsVr4CrsRefCliUse = new WsVr4CrsRefCliUse();
	//Original name: WS-SELECT-FIELDS
	private WsSelectFields wsSelectFields = new WsSelectFields();
	//Original name: CALC-FIELDS
	private CalcFields calcFields = new CalcFields();
	//Original name: PROGRAM-SUBSCRIPTS
	private ProgramSubscripts programSubscripts = new ProgramSubscripts();
	//Original name: PROGRAM-SWITCHES
	private ProgramSwitches programSwitches = new ProgramSwitches();
	//Original name: NF07AREA
	private Nf07areaofCsrProgramLineArea nf07areaofCsrProgramLineArea = new Nf07areaofCsrProgramLineArea();
	//Original name: NF07AREA
	private Nf07areaofVsrProgramLineArea nf07areaofVsrProgramLineArea = new Nf07areaofVsrProgramLineArea();
	//Original name: WS-DATE-FIELDS
	private WsDateFieldsNf0735ml wsDateFields = new WsDateFieldsNf0735ml();
	//Original name: FORMATTED-TIME
	private FormattedTime formattedTime = new FormattedTime();
	//Original name: TIMESTP
	private Timestp timestp = new Timestp();
	//Original name: PICSTR
	private Picstr picstr = new Picstr();
	//Original name: SECOND1
	private double second1 = DefaultValues.DOUBLE_VAL;
	//Original name: SECOND2
	private double second2 = DefaultValues.DOUBLE_VAL;
	//Original name: FC
	private String fc = "";
	//Original name: WS-SQLCODE-SIGN
	private char wsSqlcodeSign = Types.SPACE_CHAR;
	//Original name: WS-SQLCODE
	private String wsSqlcode = "000000000";
	//Original name: WS-JOB-SEQ-PACKED
	private short wsJobSeqPacked = DefaultValues.SHORT_VAL;
	//Original name: DATA-SAVED-FROM-TABLE-3086
	private DataSavedFromTable3086 dataSavedFromTable3086 = new DataSavedFromTable3086();
	//Original name: ERROR-MESSAGE
	private ErrorMessage errorMessage = new ErrorMessage();
	//Original name: ERROR-TEXT-LEN
	private int errorTextLen = 78;
	//Original name: SQL-CODE
	private String sqlCode = DefaultValues.stringVal(Len.SQL_CODE);
	//Original name: DB2-ERR-KEY
	private String db2ErrKey = DefaultValues.stringVal(Len.DB2_ERR_KEY);
	//Original name: DB2-ERR-TABLE
	private String db2ErrTable = DefaultValues.stringVal(Len.DB2_ERR_TABLE);
	//Original name: DB2-ERR-LAST-CALL
	private String db2ErrLastCall = DefaultValues.stringVal(Len.DB2_ERR_LAST_CALL);
	//Original name: DB2-ERR-PARA
	private String db2ErrPara = DefaultValues.stringVal(Len.DB2_ERR_PARA);
	/**Original name: TRIG-RECORD-LAYOUT<br>
	 * <pre>    ----------------------------------------------------------
	 *     COPYBOOK NF0736TR MUST BE KEPT IN SYNC WITH THE FOLLOWING:
	 *         NF07F0S   AND   NF738PRV
	 *     ----------------------------------------------------------
	 *      *--------------------------------------------------
	 *      *
	 *      *            I  M  P  O  R  T  A  N  T
	 *      *
	 *      *  THE FOLLOWING COPYBOOKS MUST BE KEPT IN SYNC.
	 *      *  ANY CHANGE TO ONE MUST ALSO BE MADE TO THE
	 *      *  OTHER.
	 *      *
	 *      *  NF07F0S   NF0736TR
	 *      * NOTE: THE FILLER LEFT AT THE END OF NF0736TR IS 5 BYTES
	 *      * LESS THAN THE NF07F0S. THE TRIG-RECORD-LAYOUT IN THIS
	 *      * COPYBOOK IS DEFINED AT 600 BYTES BUT THE NF07F0S IS 605.
	 *     ---------------------------------------------------
	 *     -
	 *     - NOTE:  ALL DATE FIELDS ARE IN CCYY-MM-DD FORMAT
	 *     -
	 *     ---------------------------------------------------</pre>*/
	private TrigRecordLayout trigRecordLayout = new TrigRecordLayout();
	//Original name: DCLS02652SA
	private Dcls02652sa dcls02652sa = new Dcls02652sa();
	//Original name: DCLS02682SA
	private Dcls02682sa dcls02682sa = new Dcls02682sa();
	//Original name: DCLS02713SA
	private Dcls02713sa dcls02713sa = new Dcls02713sa();
	//Original name: DCLS02717SA
	private Dcls02717sa dcls02717sa = new Dcls02717sa();
	//Original name: DCLS02800SA
	private Dcls02800sa dcls02800sa = new Dcls02800sa();
	//Original name: DCLS02809SA
	private Dcls02809sa dcls02809sa = new Dcls02809sa();
	//Original name: DCLS02813SA
	private Dcls02813sa dcls02813sa = new Dcls02813sa();
	//Original name: DCLS04315SA
	private Dcls04315sa dcls04315sa = new Dcls04315sa();
	//Original name: DCLS02718SA
	private Dcls02718sa dcls02718sa = new Dcls02718sa();
	//Original name: DCLS02719SA
	private Dcls02719sa dcls02719sa = new Dcls02719sa();
	//Original name: DCLS02952SA
	private Dcls02952sa dcls02952sa = new Dcls02952sa();
	//Original name: DCLS02993SA
	private Dcls02993sa dcls02993sa = new Dcls02993sa();
	//Original name: DCLS03086SA
	private Dcls03086sa dcls03086sa = new Dcls03086sa();

	//==== METHODS ====
	public String getWsPgmName() {
		return this.wsPgmName;
	}

	public String getWsPgmNameFormatted() {
		return Functions.padBlanks(getWsPgmName(), Len.WS_PGM_NAME);
	}

	public void setAbendCode(int abendCode) {
		this.abendCode = abendCode;
	}

	public void setAbendCodeFromBuffer(byte[] buffer) {
		abendCode = MarshalByte.readBinaryInt(buffer, 1);
	}

	public int getAbendCode() {
		return this.abendCode;
	}

	public String getAbendCodeFormatted() {
		return PicFormatter.display(new PicParams("S9(5)").setUsage(PicUsage.BINARY)).format(getAbendCode()).toString();
	}

	/**Original name: WS-XR4-CRS-REF-CLM-PMT-USE<br>*/
	public byte[] getWsXr4CrsRefClmPmtUseBytes() {
		byte[] buffer = new byte[Len.WS_XR4_CRS_REF_CLM_PMT_USE];
		return getWsXr4CrsRefClmPmtUseBytes(buffer, 1);
	}

	public byte[] getWsXr4CrsRefClmPmtUseBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, wsXr4CrsRefClmPmt1, Len.WS_XR4_CRS_REF_CLM_PMT1);
		position += Len.WS_XR4_CRS_REF_CLM_PMT1;
		MarshalByte.writeString(buffer, position, wsXr4CrsRefClmPmt2, Len.WS_XR4_CRS_REF_CLM_PMT2);
		return buffer;
	}

	public void setWsXr4CrsRefClmPmt1(short wsXr4CrsRefClmPmt1) {
		this.wsXr4CrsRefClmPmt1 = NumericDisplay.asString(wsXr4CrsRefClmPmt1, Len.WS_XR4_CRS_REF_CLM_PMT1);
	}

	public void setWsXr4CrsRefClmPmt1Formatted(String wsXr4CrsRefClmPmt1) {
		this.wsXr4CrsRefClmPmt1 = Trunc.toUnsignedNumeric(wsXr4CrsRefClmPmt1, Len.WS_XR4_CRS_REF_CLM_PMT1);
	}

	public short getWsXr4CrsRefClmPmt1() {
		return NumericDisplay.asShort(this.wsXr4CrsRefClmPmt1);
	}

	public void setWsXr4CrsRefClmPmt2Formatted(String wsXr4CrsRefClmPmt2) {
		this.wsXr4CrsRefClmPmt2 = Trunc.toUnsignedNumeric(wsXr4CrsRefClmPmt2, Len.WS_XR4_CRS_REF_CLM_PMT2);
	}

	public short getWsXr4CrsRefClmPmt2() {
		return NumericDisplay.asShort(this.wsXr4CrsRefClmPmt2);
	}

	/**Original name: WS-VR4-CRS-REF-CLM-PMT-USE<br>*/
	public byte[] getWsVr4CrsRefClmPmtUseBytes() {
		byte[] buffer = new byte[Len.WS_VR4_CRS_REF_CLM_PMT_USE];
		return getWsVr4CrsRefClmPmtUseBytes(buffer, 1);
	}

	public byte[] getWsVr4CrsRefClmPmtUseBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, wsVr4CrsRefClmPmt1, Len.WS_VR4_CRS_REF_CLM_PMT1);
		position += Len.WS_VR4_CRS_REF_CLM_PMT1;
		MarshalByte.writeString(buffer, position, wsVr4CrsRefClmPmt2, Len.WS_VR4_CRS_REF_CLM_PMT2);
		return buffer;
	}

	public void setWsVr4CrsRefClmPmt1(short wsVr4CrsRefClmPmt1) {
		this.wsVr4CrsRefClmPmt1 = NumericDisplay.asString(wsVr4CrsRefClmPmt1, Len.WS_VR4_CRS_REF_CLM_PMT1);
	}

	public void setWsVr4CrsRefClmPmt1Formatted(String wsVr4CrsRefClmPmt1) {
		this.wsVr4CrsRefClmPmt1 = Trunc.toUnsignedNumeric(wsVr4CrsRefClmPmt1, Len.WS_VR4_CRS_REF_CLM_PMT1);
	}

	public short getWsVr4CrsRefClmPmt1() {
		return NumericDisplay.asShort(this.wsVr4CrsRefClmPmt1);
	}

	public void setWsVr4CrsRefClmPmt2Formatted(String wsVr4CrsRefClmPmt2) {
		this.wsVr4CrsRefClmPmt2 = Trunc.toUnsignedNumeric(wsVr4CrsRefClmPmt2, Len.WS_VR4_CRS_REF_CLM_PMT2);
	}

	public short getWsVr4CrsRefClmPmt2() {
		return NumericDisplay.asShort(this.wsVr4CrsRefClmPmt2);
	}

	public String getCsrProgramLineAreaFormatted() {
		return MarshalByteExt.bufferToStr(getCsrProgramLineAreaBytes());
	}

	/**Original name: CSR-PROGRAM-LINE-AREA<br>
	 * <pre>* ------------------------------------------------------ **
	 * *          TABLE AREAS                                   **
	 * *  THIS CONTAINS TABLES USED IN ADJUSTMENTS, COMBINING   **
	 * *  LIKE CAS SEGMENTS, AND ROLLING UP INPATIENT LINES.    **
	 * * ------------------------------------------------------ **</pre>*/
	public byte[] getCsrProgramLineAreaBytes() {
		byte[] buffer = new byte[Len.CSR_PROGRAM_LINE_AREA];
		return getCsrProgramLineAreaBytes(buffer, 1);
	}

	public byte[] getCsrProgramLineAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nf07areaofCsrProgramLineArea.getPmtCommonInfo().getPmtCommonInfoBytes(buffer, position);
		position += CsrPmtCommonInfo.Len.PMT_COMMON_INFO;
		nf07areaofCsrProgramLineArea.getCalcInformation().getCalcInformationBytes(buffer, position);
		position += CsrCalcInformation.Len.CALC_INFORMATION;
		for (int idx = 1; idx <= Nf07areaofCsrProgramLineArea.XREF_AREA_MAXOCCURS; idx++) {
			nf07areaofCsrProgramLineArea.getXrefArea(idx).getXrefAreaBytes(buffer, position);
			position += CsrXrefArea.Len.XREF_AREA;
		}
		MarshalByte.writeString(buffer, position, nf07areaofCsrProgramLineArea.xrefUnbundleLine, Nf07areaofCsrProgramLineArea.Len.XREF_UNBUNDLE_LINE);
		position += Nf07areaofCsrProgramLineArea.Len.XREF_UNBUNDLE_LINE;
		nf07areaofCsrProgramLineArea.getXrefIncd().getXrefIncdBytes(buffer, position);
		position += CsrXrefIncd.Len.XREF_INCD;
		nf07areaofCsrProgramLineArea.getPmtCasInfo().getPmtCasInfoBytes(buffer, position);
		position += CsrPmtCasInfo.Len.PMT_CAS_INFO;
		MarshalByte.writeChar(buffer, position, nf07areaofCsrProgramLineArea.getArcInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, nf07areaofCsrProgramLineArea.getVbrIn());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nf07areaofCsrProgramLineArea.getMrktPkgCd(), Nf07areaofCsrProgramLineArea.Len.MRKT_PKG_CD);
		position += Nf07areaofCsrProgramLineArea.Len.MRKT_PKG_CD;
		MarshalByte.writeString(buffer, position, nf07areaofCsrProgramLineArea.getFlr1(), Nf07areaofCsrProgramLineArea.Len.FLR1);
		return buffer;
	}

	public String getVsrProgramLineAreaFormatted() {
		return MarshalByteExt.bufferToStr(getVsrProgramLineAreaBytes());
	}

	/**Original name: VSR-PROGRAM-LINE-AREA<br>*/
	public byte[] getVsrProgramLineAreaBytes() {
		byte[] buffer = new byte[Len.VSR_PROGRAM_LINE_AREA];
		return getVsrProgramLineAreaBytes(buffer, 1);
	}

	public byte[] getVsrProgramLineAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nf07areaofVsrProgramLineArea.getPmtCommonInfo().getPmtCommonInfoBytes(buffer, position);
		position += CsrPmtCommonInfo.Len.PMT_COMMON_INFO;
		nf07areaofVsrProgramLineArea.getCalcInformation().getCalcInformationBytes(buffer, position);
		position += CsrCalcInformation.Len.CALC_INFORMATION;
		for (int idx = 1; idx <= Nf07areaofVsrProgramLineArea.XREF_AREA_MAXOCCURS; idx++) {
			nf07areaofVsrProgramLineArea.getXrefArea(idx).getXrefAreaBytes(buffer, position);
			position += CsrXrefArea.Len.XREF_AREA;
		}
		MarshalByte.writeString(buffer, position, nf07areaofVsrProgramLineArea.xrefUnbundleLine, Nf07areaofVsrProgramLineArea.Len.XREF_UNBUNDLE_LINE);
		position += Nf07areaofVsrProgramLineArea.Len.XREF_UNBUNDLE_LINE;
		nf07areaofVsrProgramLineArea.getXrefIncd().getXrefIncdBytes(buffer, position);
		position += CsrXrefIncd.Len.XREF_INCD;
		nf07areaofVsrProgramLineArea.getPmtCasInfo().getPmtCasInfoBytes(buffer, position);
		position += CsrPmtCasInfo.Len.PMT_CAS_INFO;
		MarshalByte.writeChar(buffer, position, nf07areaofVsrProgramLineArea.getArcInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, nf07areaofVsrProgramLineArea.getVbrIn());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nf07areaofVsrProgramLineArea.getMrktPkgCd(), Nf07areaofVsrProgramLineArea.Len.MRKT_PKG_CD);
		position += Nf07areaofVsrProgramLineArea.Len.MRKT_PKG_CD;
		MarshalByte.writeString(buffer, position, nf07areaofVsrProgramLineArea.getFlr1(), Nf07areaofVsrProgramLineArea.Len.FLR1);
		return buffer;
	}

	public void setSecond1(double second1) {
		this.second1 = second1;
	}

	public void setSecond1FromBuffer(byte[] buffer) {
		second1 = MarshalByte.readDouble(buffer, 1);
	}

	public double getSecond1() {
		return this.second1;
	}

	public String getSecond1Formatted() {
		return AfSystem.formatIndicators.toString(getSecond1(), "g");
	}

	public String getSecond1AsString() {
		return getSecond1Formatted();
	}

	public void setSecond2(double second2) {
		this.second2 = second2;
	}

	public void setSecond2FromBuffer(byte[] buffer) {
		second2 = MarshalByte.readDouble(buffer, 1);
	}

	public double getSecond2() {
		return this.second2;
	}

	public String getSecond2Formatted() {
		return AfSystem.formatIndicators.toString(getSecond2(), "g");
	}

	public String getSecond2AsString() {
		return getSecond2Formatted();
	}

	public void setFc(String fc) {
		this.fc = Functions.subString(fc, Len.FC);
	}

	public String getFc() {
		return this.fc;
	}

	public String getFcFormatted() {
		return Functions.padBlanks(getFc(), Len.FC);
	}

	public String getWsSqlcodeUnpackedFormatted() {
		return MarshalByteExt.bufferToStr(getWsSqlcodeUnpackedBytes());
	}

	/**Original name: WS-SQLCODE-UNPACKED<br>
	 * <pre>***** END OF CHECKPOINT DATE TIME FIELDS
	 * ------------------------ DB2 WORKING STORAGE AREA
	 * *   -----------------------------------------------------------
	 * *       THIS IS THE KEY AREA FOR THE CHECKPOINT DB2 TABLE.
	 * *   -----------------------------------------------------------
	 * *   THIS IS FOR THE DATA RETURNED.</pre>*/
	public byte[] getWsSqlcodeUnpackedBytes() {
		byte[] buffer = new byte[Len.WS_SQLCODE_UNPACKED];
		return getWsSqlcodeUnpackedBytes(buffer, 1);
	}

	public byte[] getWsSqlcodeUnpackedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, wsSqlcodeSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, wsSqlcode, Len.WS_SQLCODE);
		return buffer;
	}

	public void setWsSqlcodeSign(char wsSqlcodeSign) {
		this.wsSqlcodeSign = wsSqlcodeSign;
	}

	public void setWsSqlcodeSignFormatted(String wsSqlcodeSign) {
		setWsSqlcodeSign(Functions.charAt(wsSqlcodeSign, Types.CHAR_SIZE));
	}

	public char getWsSqlcodeSign() {
		return this.wsSqlcodeSign;
	}

	public void setWsSqlcode(int wsSqlcode) {
		this.wsSqlcode = NumericDisplay.asString(wsSqlcode, Len.WS_SQLCODE);
	}

	public int getWsSqlcode() {
		return NumericDisplay.asInt(this.wsSqlcode);
	}

	public void setWsJobSeqPacked(short wsJobSeqPacked) {
		this.wsJobSeqPacked = wsJobSeqPacked;
	}

	public short getWsJobSeqPacked() {
		return this.wsJobSeqPacked;
	}

	public void setErrorTextLen(int errorTextLen) {
		this.errorTextLen = errorTextLen;
	}

	public void setErrorTextLenFromBuffer(byte[] buffer) {
		errorTextLen = MarshalByte.readBinaryInt(buffer, 1);
	}

	public int getErrorTextLen() {
		return this.errorTextLen;
	}

	public String getErrorTextLenFormatted() {
		return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.BINARY)).format(getErrorTextLen()).toString();
	}

	public void setSqlCode(long sqlCode) {
		this.sqlCode = PicFormatter.display("Z(9)9-").format(sqlCode).toString();
	}

	public long getSqlCode() {
		return PicParser.display("Z(9)9-").parseLong(this.sqlCode);
	}

	public String getSqlCodeFormatted() {
		return this.sqlCode;
	}

	public String getSqlCodeAsString() {
		return getSqlCodeFormatted();
	}

	public void setDb2ErrKey(String db2ErrKey) {
		this.db2ErrKey = Functions.subString(db2ErrKey, Len.DB2_ERR_KEY);
	}

	public String getDb2ErrKey() {
		return this.db2ErrKey;
	}

	public String getDb2ErrKeyFormatted() {
		return Functions.padBlanks(getDb2ErrKey(), Len.DB2_ERR_KEY);
	}

	public void setDb2ErrTable(String db2ErrTable) {
		this.db2ErrTable = Functions.subString(db2ErrTable, Len.DB2_ERR_TABLE);
	}

	public String getDb2ErrTable() {
		return this.db2ErrTable;
	}

	public String getDb2ErrTableFormatted() {
		return Functions.padBlanks(getDb2ErrTable(), Len.DB2_ERR_TABLE);
	}

	public void setDb2ErrLastCall(String db2ErrLastCall) {
		this.db2ErrLastCall = Functions.subString(db2ErrLastCall, Len.DB2_ERR_LAST_CALL);
	}

	public String getDb2ErrLastCall() {
		return this.db2ErrLastCall;
	}

	public String getDb2ErrLastCallFormatted() {
		return Functions.padBlanks(getDb2ErrLastCall(), Len.DB2_ERR_LAST_CALL);
	}

	public void setDb2ErrPara(String db2ErrPara) {
		this.db2ErrPara = Functions.subString(db2ErrPara, Len.DB2_ERR_PARA);
	}

	public String getDb2ErrPara() {
		return this.db2ErrPara;
	}

	public String getDb2ErrParaFormatted() {
		return Functions.padBlanks(getDb2ErrPara(), Len.DB2_ERR_PARA);
	}

	public CalcFields getCalcFields() {
		return calcFields;
	}

	public DataSavedFromTable3086 getDataSavedFromTable3086() {
		return dataSavedFromTable3086;
	}

	public Dcls02652sa getDcls02652sa() {
		return dcls02652sa;
	}

	public Dcls02682sa getDcls02682sa() {
		return dcls02682sa;
	}

	public Dcls02713sa getDcls02713sa() {
		return dcls02713sa;
	}

	public Dcls02717sa getDcls02717sa() {
		return dcls02717sa;
	}

	public Dcls02718sa getDcls02718sa() {
		return dcls02718sa;
	}

	public Dcls02719sa getDcls02719sa() {
		return dcls02719sa;
	}

	public Dcls02800sa getDcls02800sa() {
		return dcls02800sa;
	}

	public Dcls02809sa getDcls02809sa() {
		return dcls02809sa;
	}

	public Dcls02813sa getDcls02813sa() {
		return dcls02813sa;
	}

	public Dcls02952sa getDcls02952sa() {
		return dcls02952sa;
	}

	public Dcls02993sa getDcls02993sa() {
		return dcls02993sa;
	}

	public Dcls03086sa getDcls03086sa() {
		return dcls03086sa;
	}

	public Dcls04315sa getDcls04315sa() {
		return dcls04315sa;
	}

	public ErrorMessage getErrorMessage() {
		return errorMessage;
	}

	public FormattedTime getFormattedTime() {
		return formattedTime;
	}

	public Nf07areaofCsrProgramLineArea getNf07areaofCsrProgramLineArea() {
		return nf07areaofCsrProgramLineArea;
	}

	public Nf07areaofVsrProgramLineArea getNf07areaofVsrProgramLineArea() {
		return nf07areaofVsrProgramLineArea;
	}

	public Picstr getPicstr() {
		return picstr;
	}

	public ProgramCounters getProgramCounters() {
		return programCounters;
	}

	public ProgramHoldAreas getProgramHoldAreas() {
		return programHoldAreas;
	}

	public ProgramSubscripts getProgramSubscripts() {
		return programSubscripts;
	}

	public ProgramSwitches getProgramSwitches() {
		return programSwitches;
	}

	public S02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Nf0735ml getS02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Nf0735ml() {
		return s02652saS02682saS02713saS02800saS02809saS02813saS02993saS043Nf0735ml;
	}

	public S02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Nf0735ml getS02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Nf0735ml() {
		return s02652saS02682saS02713saS02800saS02809saS02993saS04315saS043Nf0735ml;
	}

	public Timestp getTimestp() {
		return timestp;
	}

	@Override
	public String getTrig837BillProvNpiId() {
		return trigRecordLayout.getTrig837BillProvNpiId();
	}

	@Override
	public void setTrig837BillProvNpiId(String trig837BillProvNpiId) {
		this.trigRecordLayout.setTrig837BillProvNpiId(trig837BillProvNpiId);
	}

	@Override
	public String getTrig837PerfProvNpiId() {
		return trigRecordLayout.getTrig837PerfProvNpiId();
	}

	@Override
	public void setTrig837PerfProvNpiId(String trig837PerfProvNpiId) {
		this.trigRecordLayout.setTrig837PerfProvNpiId(trig837PerfProvNpiId);
	}

	@Override
	public char getTrigAdjRespCd() {
		return trigRecordLayout.getTrigAdjRespCd();
	}

	@Override
	public void setTrigAdjRespCd(char trigAdjRespCd) {
		this.trigRecordLayout.setTrigAdjRespCd(trigAdjRespCd);
	}

	@Override
	public String getTrigAdjTrckCd() {
		return trigRecordLayout.getTrigAdjTrckCd();
	}

	@Override
	public void setTrigAdjTrckCd(String trigAdjTrckCd) {
		this.trigRecordLayout.setTrigAdjTrckCd(trigAdjTrckCd);
	}

	@Override
	public char getTrigAdjTypCd() {
		return trigRecordLayout.getTrigAdjTypCd();
	}

	@Override
	public void setTrigAdjTypCd(char trigAdjTypCd) {
		this.trigRecordLayout.setTrigAdjTypCd(trigAdjTypCd);
	}

	@Override
	public char getTrigAdjdProvStatCd() {
		return trigRecordLayout.getTrigAdjdProvStatCd();
	}

	@Override
	public void setTrigAdjdProvStatCd(char trigAdjdProvStatCd) {
		this.trigRecordLayout.setTrigAdjdProvStatCd(trigAdjdProvStatCd);
	}

	@Override
	public String getTrigAlphPrfxCd() {
		return trigRecordLayout.getTrigAlphPrfxCd();
	}

	@Override
	public void setTrigAlphPrfxCd(String trigAlphPrfxCd) {
		this.trigRecordLayout.setTrigAlphPrfxCd(trigAlphPrfxCd);
	}

	@Override
	public String getTrigAsgCd() {
		return trigRecordLayout.getTrigAsgCd();
	}

	@Override
	public void setTrigAsgCd(String trigAsgCd) {
		this.trigRecordLayout.setTrigAsgCd(trigAsgCd);
	}

	@Override
	public String getTrigBaseCnArngCd() {
		return trigRecordLayout.getTrigBaseCnArngCd();
	}

	@Override
	public void setTrigBaseCnArngCd(String trigBaseCnArngCd) {
		this.trigRecordLayout.setTrigBaseCnArngCd(trigBaseCnArngCd);
	}

	@Override
	public String getTrigBillFrmDt() {
		return trigRecordLayout.getTrigBillFrmDt();
	}

	@Override
	public void setTrigBillFrmDt(String trigBillFrmDt) {
		this.trigRecordLayout.setTrigBillFrmDt(trigBillFrmDt);
	}

	@Override
	public String getTrigBillProvIdQlfCd() {
		return trigRecordLayout.getTrigBillProvIdQlfCd();
	}

	@Override
	public void setTrigBillProvIdQlfCd(String trigBillProvIdQlfCd) {
		this.trigRecordLayout.setTrigBillProvIdQlfCd(trigBillProvIdQlfCd);
	}

	@Override
	public String getTrigBillProvSpecCd() {
		return trigRecordLayout.getTrigBillProvSpecCd();
	}

	@Override
	public void setTrigBillProvSpecCd(String trigBillProvSpecCd) {
		this.trigRecordLayout.setTrigBillProvSpecCd(trigBillProvSpecCd);
	}

	@Override
	public String getTrigBillThrDt() {
		return trigRecordLayout.getTrigBillThrDt();
	}

	@Override
	public void setTrigBillThrDt(String trigBillThrDt) {
		this.trigRecordLayout.setTrigBillThrDt(trigBillThrDt);
	}

	@Override
	public char getTrigClaimLobCd() {
		return trigRecordLayout.getTrigClaimLobCd();
	}

	@Override
	public void setTrigClaimLobCd(char trigClaimLobCd) {
		this.trigRecordLayout.setTrigClaimLobCd(trigClaimLobCd);
	}

	@Override
	public String getTrigClmCntlId() {
		return trigRecordLayout.getTrigClmCntlId();
	}

	@Override
	public void setTrigClmCntlId(String trigClmCntlId) {
		this.trigRecordLayout.setTrigClmCntlId(trigClmCntlId);
	}

	@Override
	public char getTrigClmCntlSfx() {
		return trigRecordLayout.getTrigClmCntlSfx();
	}

	@Override
	public void setTrigClmCntlSfx(char trigClmCntlSfx) {
		this.trigRecordLayout.setTrigClmCntlSfx(trigClmCntlSfx);
	}

	@Override
	public char getTrigClmckAdjStatCd() {
		return trigRecordLayout.getTrigClmckAdjStatCd();
	}

	@Override
	public void setTrigClmckAdjStatCd(char trigClmckAdjStatCd) {
		this.trigRecordLayout.setTrigClmckAdjStatCd(trigClmckAdjStatCd);
	}

	@Override
	public String getTrigCorpRcvDt() {
		return trigRecordLayout.getTrigCorpRcvDt();
	}

	@Override
	public void setTrigCorpRcvDt(String trigCorpRcvDt) {
		this.trigRecordLayout.setTrigCorpRcvDt(trigCorpRcvDt);
	}

	@Override
	public String getTrigCorrPrioritySubId() {
		return trigRecordLayout.getTrigCorrPrioritySubId();
	}

	@Override
	public void setTrigCorrPrioritySubId(String trigCorrPrioritySubId) {
		this.trigRecordLayout.setTrigCorrPrioritySubId(trigCorrPrioritySubId);
	}

	@Override
	public String getTrigDateCoverageLapsed() {
		return trigRecordLayout.getTrigDateCoverageLapsed();
	}

	@Override
	public void setTrigDateCoverageLapsed(String trigDateCoverageLapsed) {
		this.trigRecordLayout.setTrigDateCoverageLapsed(trigDateCoverageLapsed);
	}

	@Override
	public String getTrigDrgCd() {
		return trigRecordLayout.getTrigDrgCd();
	}

	@Override
	public void setTrigDrgCd(String trigDrgCd) {
		this.trigRecordLayout.setTrigDrgCd(trigDrgCd);
	}

	@Override
	public char getTrigEftAccountType() {
		return trigRecordLayout.getTrigEftAccountType();
	}

	@Override
	public void setTrigEftAccountType(char trigEftAccountType) {
		this.trigRecordLayout.setTrigEftAccountType(trigEftAccountType);
	}

	@Override
	public String getTrigEftAcct() {
		return trigRecordLayout.getTrigEftAcct();
	}

	@Override
	public void setTrigEftAcct(String trigEftAcct) {
		this.trigRecordLayout.setTrigEftAcct(trigEftAcct);
	}

	@Override
	public char getTrigEftInd() {
		return trigRecordLayout.getTrigEftInd();
	}

	@Override
	public void setTrigEftInd(char trigEftInd) {
		this.trigRecordLayout.setTrigEftInd(trigEftInd);
	}

	@Override
	public String getTrigEftTrans() {
		return trigRecordLayout.getTrigEftTrans();
	}

	@Override
	public void setTrigEftTrans(String trigEftTrans) {
		this.trigRecordLayout.setTrigEftTrans(trigEftTrans);
	}

	@Override
	public String getTrigEnrClCd() {
		return trigRecordLayout.getTrigEnrClCd();
	}

	@Override
	public void setTrigEnrClCd(String trigEnrClCd) {
		this.trigRecordLayout.setTrigEnrClCd(trigEnrClCd);
	}

	@Override
	public String getTrigFinCd() {
		return trigRecordLayout.getTrigFinCd();
	}

	@Override
	public void setTrigFinCd(String trigFinCd) {
		this.trigRecordLayout.setTrigFinCd(trigFinCd);
	}

	@Override
	public String getTrigGlOfstOrigCd() {
		return trigRecordLayout.getTrigGlOfstOrigCd();
	}

	@Override
	public void setTrigGlOfstOrigCd(String trigGlOfstOrigCd) {
		this.trigRecordLayout.setTrigGlOfstOrigCd(trigGlOfstOrigCd);
	}

	@Override
	public String getTrigGmisIndicator() {
		return trigRecordLayout.getTrigGmisIndicator();
	}

	@Override
	public void setTrigGmisIndicator(String trigGmisIndicator) {
		this.trigRecordLayout.setTrigGmisIndicator(trigGmisIndicator);
	}

	@Override
	public String getTrigGrpId() {
		return trigRecordLayout.getTrigGrpId();
	}

	@Override
	public void setTrigGrpId(String trigGrpId) {
		this.trigRecordLayout.setTrigGrpId(trigGrpId);
	}

	@Override
	public String getTrigHipaaVersionFormatId() {
		return trigRecordLayout.getTrigHipaaVersionFormatId();
	}

	@Override
	public void setTrigHipaaVersionFormatId(String trigHipaaVersionFormatId) {
		this.trigRecordLayout.setTrigHipaaVersionFormatId(trigHipaaVersionFormatId);
	}

	@Override
	public String getTrigHistBillProvNpiId() {
		return trigRecordLayout.getTrigHistBillProvNpiId();
	}

	@Override
	public void setTrigHistBillProvNpiId(String trigHistBillProvNpiId) {
		this.trigRecordLayout.setTrigHistBillProvNpiId(trigHistBillProvNpiId);
	}

	@Override
	public char getTrigHistLoadCd() {
		return trigRecordLayout.getTrigHistLoadCd();
	}

	@Override
	public void setTrigHistLoadCd(char trigHistLoadCd) {
		this.trigRecordLayout.setTrigHistLoadCd(trigHistLoadCd);
	}

	@Override
	public String getTrigHistPerfProvNpiId() {
		return trigRecordLayout.getTrigHistPerfProvNpiId();
	}

	@Override
	public void setTrigHistPerfProvNpiId(String trigHistPerfProvNpiId) {
		this.trigRecordLayout.setTrigHistPerfProvNpiId(trigHistPerfProvNpiId);
	}

	@Override
	public String getTrigInsId() {
		return trigRecordLayout.getTrigInsId();
	}

	@Override
	public void setTrigInsId(String trigInsId) {
		this.trigRecordLayout.setTrigInsId(trigInsId);
	}

	@Override
	public char getTrigItsCk() {
		return trigRecordLayout.getTrigItsCk();
	}

	@Override
	public void setTrigItsCk(char trigItsCk) {
		this.trigRecordLayout.setTrigItsCk(trigItsCk);
	}

	@Override
	public char getTrigItsClmTypCd() {
		return trigRecordLayout.getTrigItsClmTypCd();
	}

	@Override
	public void setTrigItsClmTypCd(char trigItsClmTypCd) {
		this.trigRecordLayout.setTrigItsClmTypCd(trigItsClmTypCd);
	}

	@Override
	public String getTrigItsInsId() {
		return trigRecordLayout.getTrigItsInsId();
	}

	@Override
	public void setTrigItsInsId(String trigItsInsId) {
		this.trigRecordLayout.setTrigItsInsId(trigItsInsId);
	}

	@Override
	public String getTrigKcapsTeamNm() {
		return trigRecordLayout.getTrigKcapsTeamNm();
	}

	@Override
	public void setTrigKcapsTeamNm(String trigKcapsTeamNm) {
		this.trigRecordLayout.setTrigKcapsTeamNm(trigKcapsTeamNm);
	}

	@Override
	public String getTrigKcapsUseId() {
		return trigRecordLayout.getTrigKcapsUseId();
	}

	@Override
	public void setTrigKcapsUseId(String trigKcapsUseId) {
		this.trigRecordLayout.setTrigKcapsUseId(trigKcapsUseId);
	}

	@Override
	public String getTrigLocalBillProvId() {
		return trigRecordLayout.getTrigLocalBillProvId();
	}

	@Override
	public void setTrigLocalBillProvId(String trigLocalBillProvId) {
		this.trigRecordLayout.setTrigLocalBillProvId(trigLocalBillProvId);
	}

	@Override
	public char getTrigLocalBillProvLobCd() {
		return trigRecordLayout.getTrigLocalBillProvLobCd();
	}

	@Override
	public void setTrigLocalBillProvLobCd(char trigLocalBillProvLobCd) {
		this.trigRecordLayout.setTrigLocalBillProvLobCd(trigLocalBillProvLobCd);
	}

	@Override
	public String getTrigLocalPerfProvId() {
		return trigRecordLayout.getTrigLocalPerfProvId();
	}

	@Override
	public void setTrigLocalPerfProvId(String trigLocalPerfProvId) {
		this.trigRecordLayout.setTrigLocalPerfProvId(trigLocalPerfProvId);
	}

	@Override
	public char getTrigLocalPerfProvLobCd() {
		return trigRecordLayout.getTrigLocalPerfProvLobCd();
	}

	@Override
	public void setTrigLocalPerfProvLobCd(char trigLocalPerfProvLobCd) {
		this.trigRecordLayout.setTrigLocalPerfProvLobCd(trigLocalPerfProvLobCd);
	}

	@Override
	public String getTrigMemberId() {
		return trigRecordLayout.getTrigMemberId();
	}

	@Override
	public void setTrigMemberId(String trigMemberId) {
		this.trigRecordLayout.setTrigMemberId(trigMemberId);
	}

	@Override
	public String getTrigMrktPkgCd() {
		return trigRecordLayout.getTrigMrktPkgCd();
	}

	@Override
	public void setTrigMrktPkgCd(String trigMrktPkgCd) {
		this.trigRecordLayout.setTrigMrktPkgCd(trigMrktPkgCd);
	}

	@Override
	public char getTrigNpiCd() {
		return programHoldAreas.getTrigNpiCd();
	}

	@Override
	public void setTrigNpiCd(char trigNpiCd) {
		this.programHoldAreas.setTrigNpiCd(trigNpiCd);
	}

	@Override
	public String getTrigNtwrkCd() {
		return trigRecordLayout.getTrigNtwrkCd();
	}

	@Override
	public void setTrigNtwrkCd(String trigNtwrkCd) {
		this.trigRecordLayout.setTrigNtwrkCd(trigNtwrkCd);
	}

	@Override
	public String getTrigOiPayNm() {
		return trigRecordLayout.getTrigOiPayNm();
	}

	@Override
	public void setTrigOiPayNm(String trigOiPayNm) {
		this.trigRecordLayout.setTrigOiPayNm(trigOiPayNm);
	}

	@Override
	public String getTrigPatActMedRecId() {
		return trigRecordLayout.getTrigPatActMedRecId();
	}

	@Override
	public void setTrigPatActMedRecId(String trigPatActMedRecId) {
		this.trigRecordLayout.setTrigPatActMedRecId(trigPatActMedRecId);
	}

	@Override
	public String getTrigPgmAreaCd() {
		return trigRecordLayout.getTrigPgmAreaCd();
	}

	@Override
	public void setTrigPgmAreaCd(String trigPgmAreaCd) {
		this.trigRecordLayout.setTrigPgmAreaCd(trigPgmAreaCd);
	}

	@Override
	public String getTrigPmtAdjdDt() {
		return trigRecordLayout.getTrigPmtAdjdDt();
	}

	@Override
	public void setTrigPmtAdjdDt(String trigPmtAdjdDt) {
		this.trigRecordLayout.setTrigPmtAdjdDt(trigPmtAdjdDt);
	}

	@Override
	public char getTrigPmtBkProdCd() {
		return trigRecordLayout.getTrigPmtBkProdCd();
	}

	@Override
	public void setTrigPmtBkProdCd(char trigPmtBkProdCd) {
		this.trigRecordLayout.setTrigPmtBkProdCd(trigPmtBkProdCd);
	}

	@Override
	public String getTrigPmtFrmServDt() {
		return trigRecordLayout.getTrigPmtFrmServDt();
	}

	@Override
	public void setTrigPmtFrmServDt(String trigPmtFrmServDt) {
		this.trigRecordLayout.setTrigPmtFrmServDt(trigPmtFrmServDt);
	}

	@Override
	public char getTrigPmtOiIn() {
		return trigRecordLayout.getTrigPmtOiIn();
	}

	@Override
	public void setTrigPmtOiIn(char trigPmtOiIn) {
		this.trigRecordLayout.setTrigPmtOiIn(trigPmtOiIn);
	}

	@Override
	public String getTrigPmtThrServDt() {
		return trigRecordLayout.getTrigPmtThrServDt();
	}

	@Override
	public void setTrigPmtThrServDt(String trigPmtThrServDt) {
		this.trigRecordLayout.setTrigPmtThrServDt(trigPmtThrServDt);
	}

	@Override
	public String getTrigPrimCnArngCd() {
		return trigRecordLayout.getTrigPrimCnArngCd();
	}

	@Override
	public void setTrigPrimCnArngCd(String trigPrimCnArngCd) {
		this.trigRecordLayout.setTrigPrimCnArngCd(trigPrimCnArngCd);
	}

	@Override
	public String getTrigPrmptPayDayCd() {
		return trigRecordLayout.getTrigPrmptPayDayCd();
	}

	@Override
	public void setTrigPrmptPayDayCd(String trigPrmptPayDayCd) {
		this.trigRecordLayout.setTrigPrmptPayDayCd(trigPrmptPayDayCd);
	}

	@Override
	public char getTrigPrmptPayOvrdCd() {
		return trigRecordLayout.getTrigPrmptPayOvrdCd();
	}

	@Override
	public void setTrigPrmptPayOvrdCd(char trigPrmptPayOvrdCd) {
		this.trigRecordLayout.setTrigPrmptPayOvrdCd(trigPrmptPayOvrdCd);
	}

	@Override
	public String getTrigProdInd() {
		return trigRecordLayout.getTrigProdInd();
	}

	@Override
	public void setTrigProdInd(String trigProdInd) {
		this.trigRecordLayout.setTrigProdInd(trigProdInd);
	}

	@Override
	public String getTrigProvUnwrpDt() {
		return trigRecordLayout.getTrigProvUnwrpDt();
	}

	@Override
	public void setTrigProvUnwrpDt(String trigProvUnwrpDt) {
		this.trigRecordLayout.setTrigProvUnwrpDt(trigProvUnwrpDt);
	}

	@Override
	public String getTrigRateCd() {
		return trigRecordLayout.getTrigRateCd();
	}

	@Override
	public void setTrigRateCd(String trigRateCd) {
		this.trigRecordLayout.setTrigRateCd(trigRateCd);
	}

	public TrigRecordLayout getTrigRecordLayout() {
		return trigRecordLayout;
	}

	@Override
	public char getTrigStatAdjPrevPmt() {
		return trigRecordLayout.getTrigStatAdjPrevPmt();
	}

	@Override
	public void setTrigStatAdjPrevPmt(char trigStatAdjPrevPmt) {
		this.trigRecordLayout.setTrigStatAdjPrevPmt(trigStatAdjPrevPmt);
	}

	@Override
	public String getTrigTypGrpCd() {
		return trigRecordLayout.getTrigTypGrpCd();
	}

	@Override
	public void setTrigTypGrpCd(String trigTypGrpCd) {
		this.trigRecordLayout.setTrigTypGrpCd(trigTypGrpCd);
	}

	@Override
	public char getTrigVbrIn() {
		return trigRecordLayout.getTrigVbrIn();
	}

	@Override
	public void setTrigVbrIn(char trigVbrIn) {
		this.trigRecordLayout.setTrigVbrIn(trigVbrIn);
	}

	@Override
	public char getTrigVoidCd() {
		return trigRecordLayout.getTrigVoidCd();
	}

	@Override
	public void setTrigVoidCd(char trigVoidCd) {
		this.trigRecordLayout.setTrigVoidCd(trigVoidCd);
	}

	@Override
	public AfDecimal getWsAccruedPrmptPayIntAm() {
		return programHoldAreas.getWsAccruedPrmptPayIntAm();
	}

	@Override
	public void setWsAccruedPrmptPayIntAm(AfDecimal wsAccruedPrmptPayIntAm) {
		this.programHoldAreas.setWsAccruedPrmptPayIntAm(wsAccruedPrmptPayIntAm.copy());
	}

	public WsDateFieldsNf0735ml getWsDateFields() {
		return wsDateFields;
	}

	public WsHoldTrigHeaderArea getWsHoldTrigHeaderArea() {
		return wsHoldTrigHeaderArea;
	}

	public WsSelectFields getWsSelectFields() {
		return wsSelectFields;
	}

	@Override
	public AfDecimal getWsTrigAltDrgAlwAm() {
		return programHoldAreas.getWsTrigAltDrgAlwAm();
	}

	@Override
	public void setWsTrigAltDrgAlwAm(AfDecimal wsTrigAltDrgAlwAm) {
		this.programHoldAreas.setWsTrigAltDrgAlwAm(wsTrigAltDrgAlwAm.copy());
	}

	@Override
	public AfDecimal getWsTrigClmPdAm() {
		return programHoldAreas.getWsTrigClmPdAm();
	}

	@Override
	public void setWsTrigClmPdAm(AfDecimal wsTrigClmPdAm) {
		this.programHoldAreas.setWsTrigClmPdAm(wsTrigClmPdAm.copy());
	}

	@Override
	public short getWsTrigGlSoteOrigCd() {
		return programHoldAreas.getWsTrigGlSoteOrigCd();
	}

	@Override
	public void setWsTrigGlSoteOrigCd(short wsTrigGlSoteOrigCd) {
		this.programHoldAreas.setWsTrigGlSoteOrigCd(wsTrigGlSoteOrigCd);
	}

	@Override
	public short getWsTrigLstFnlPmtPtId() {
		return programHoldAreas.getWsTrigLstFnlPmtPtId();
	}

	@Override
	public void setWsTrigLstFnlPmtPtId(short wsTrigLstFnlPmtPtId) {
		this.programHoldAreas.setWsTrigLstFnlPmtPtId(wsTrigLstFnlPmtPtId);
	}

	@Override
	public AfDecimal getWsTrigOverDrgAlwAm() {
		return programHoldAreas.getWsTrigOverDrgAlwAm();
	}

	@Override
	public void setWsTrigOverDrgAlwAm(AfDecimal wsTrigOverDrgAlwAm) {
		this.programHoldAreas.setWsTrigOverDrgAlwAm(wsTrigOverDrgAlwAm.copy());
	}

	@Override
	public short getWsTrigPmtId() {
		return programHoldAreas.getWsTrigPmtId();
	}

	@Override
	public void setWsTrigPmtId(short wsTrigPmtId) {
		this.programHoldAreas.setWsTrigPmtId(wsTrigPmtId);
	}

	@Override
	public AfDecimal getWsTrigSchdlDrgAlwAm() {
		return programHoldAreas.getWsTrigSchdlDrgAlwAm();
	}

	@Override
	public void setWsTrigSchdlDrgAlwAm(AfDecimal wsTrigSchdlDrgAlwAm) {
		this.programHoldAreas.setWsTrigSchdlDrgAlwAm(wsTrigSchdlDrgAlwAm.copy());
	}

	public WsVr4CrsRefCliUse getWsVr4CrsRefCliUse() {
		return wsVr4CrsRefCliUse;
	}

	public WsXr4CrsRefCliUse getWsXr4CrsRefCliUse() {
		return wsXr4CrsRefCliUse;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_XR4_CRS_REF_CLM_PMT1 = 1;
		public static final int WS_XR4_CRS_REF_CLM_PMT2 = 1;
		public static final int WS_VR4_CRS_REF_CLM_PMT1 = 1;
		public static final int WS_VR4_CRS_REF_CLM_PMT2 = 1;
		public static final int WS_SQLCODE = 9;
		public static final int SQL_CODE = 11;
		public static final int DB2_ERR_KEY = 80;
		public static final int DB2_ERR_TABLE = 80;
		public static final int DB2_ERR_LAST_CALL = 80;
		public static final int DB2_ERR_PARA = 80;
		public static final int WS_PGM_NAME = 8;
		public static final int CSR_PROGRAM_LINE_AREA = CsrPmtCommonInfo.Len.PMT_COMMON_INFO + CsrCalcInformation.Len.CALC_INFORMATION
				+ Nf07areaofCsrProgramLineArea.XREF_AREA_MAXOCCURS * CsrXrefArea.Len.XREF_AREA + Nf07areaofCsrProgramLineArea.Len.XREF_UNBUNDLE_LINE
				+ CsrXrefIncd.Len.XREF_INCD + CsrPmtCasInfo.Len.PMT_CAS_INFO + Nf07areaofCsrProgramLineArea.Len.ARC_IND
				+ Nf07areaofCsrProgramLineArea.Len.VBR_IN + Nf07areaofCsrProgramLineArea.Len.MRKT_PKG_CD + Nf07areaofCsrProgramLineArea.Len.FLR1;
		public static final int WS_XR4_CRS_REF_CLM_PMT_USE = WS_XR4_CRS_REF_CLM_PMT1 + WS_XR4_CRS_REF_CLM_PMT2;
		public static final int VSR_PROGRAM_LINE_AREA = CsrPmtCommonInfo.Len.PMT_COMMON_INFO + CsrCalcInformation.Len.CALC_INFORMATION
				+ Nf07areaofVsrProgramLineArea.XREF_AREA_MAXOCCURS * CsrXrefArea.Len.XREF_AREA + Nf07areaofVsrProgramLineArea.Len.XREF_UNBUNDLE_LINE
				+ CsrXrefIncd.Len.XREF_INCD + CsrPmtCasInfo.Len.PMT_CAS_INFO + Nf07areaofVsrProgramLineArea.Len.ARC_IND
				+ Nf07areaofVsrProgramLineArea.Len.VBR_IN + Nf07areaofVsrProgramLineArea.Len.MRKT_PKG_CD + Nf07areaofVsrProgramLineArea.Len.FLR1;
		public static final int WS_VR4_CRS_REF_CLM_PMT_USE = WS_VR4_CRS_REF_CLM_PMT1 + WS_VR4_CRS_REF_CLM_PMT2;
		public static final int FC = 12;
		public static final int WS_SQLCODE_SIGN = 1;
		public static final int WS_SQLCODE_UNPACKED = WS_SQLCODE_SIGN + WS_SQLCODE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
