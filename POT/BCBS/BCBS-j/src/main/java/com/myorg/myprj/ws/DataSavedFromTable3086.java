/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: DATA-SAVED-FROM-TABLE-3086<br>
 * Variable: DATA-SAVED-FROM-TABLE-3086 from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DataSavedFromTable3086 {

	//==== PROPERTIES ====
	//Original name: WS-APPL-TRM-CD
	private char wsApplTrmCd = Types.SPACE_CHAR;
	//Original name: WS-RSTRT-HIST-IN
	private char wsRstrtHistIn = Types.SPACE_CHAR;
	//Original name: WS-ROW-FREQ-CMT-CT
	private int wsRowFreqCmtCt = 0;
	//Original name: EXPANDED-CMT-TM-SS
	private String expandedCmtTmSs = "00";
	//Original name: WS-CMT-TM-INT-TM
	private WsCmtTmIntTm wsCmtTmIntTm = new WsCmtTmIntTm();

	//==== METHODS ====
	public void setWsApplTrmCd(char wsApplTrmCd) {
		this.wsApplTrmCd = wsApplTrmCd;
	}

	public char getWsApplTrmCd() {
		return this.wsApplTrmCd;
	}

	public void setWsRstrtHistIn(char wsRstrtHistIn) {
		this.wsRstrtHistIn = wsRstrtHistIn;
	}

	public char getWsRstrtHistIn() {
		return this.wsRstrtHistIn;
	}

	public void setWsRowFreqCmtCt(int wsRowFreqCmtCt) {
		this.wsRowFreqCmtCt = wsRowFreqCmtCt;
	}

	public int getWsRowFreqCmtCt() {
		return this.wsRowFreqCmtCt;
	}

	public void setExpandedCmtTmSsFormatted(String expandedCmtTmSs) {
		this.expandedCmtTmSs = Trunc.toUnsignedNumeric(expandedCmtTmSs, Len.EXPANDED_CMT_TM_SS);
	}

	public short getExpandedCmtTmSs() {
		return NumericDisplay.asShort(this.expandedCmtTmSs);
	}

	public WsCmtTmIntTm getWsCmtTmIntTm() {
		return wsCmtTmIntTm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_ROW_FREQ_CMT_CT = 9;
		public static final int EXPANDED_CMT_TM_SS = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
