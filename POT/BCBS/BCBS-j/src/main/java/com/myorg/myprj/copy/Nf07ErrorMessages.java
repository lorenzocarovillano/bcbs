/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

/**Original name: NF07-ERROR-MESSAGES<br>
 * Variable: NF07-ERROR-MESSAGES from copybook NF07MSGS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Nf07ErrorMessages {

	//==== PROPERTIES ====
	//Original name: VOID-NOT-FOUND-MSG
	private String voidNotFoundMsg = "VOID FOR PREVIOUS PAYMENT IS MISSING";
	//Original name: ORIG-IS-ADJ-MSG
	private String origIsAdjMsg = "ORIG PAYMENT CONTAINS AN ADJUSTMENT TYPE";

	//==== METHODS ====
	public String getVoidNotFoundMsg() {
		return this.voidNotFoundMsg;
	}

	public String getOrigIsAdjMsg() {
		return this.origIsAdjMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FATAL_MSG_DOLLARS = 13;
		public static final int WARNING_MSG_DOLLARS = 13;
		public static final int OVERRIDE_MSG_DOLLARS = 13;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
