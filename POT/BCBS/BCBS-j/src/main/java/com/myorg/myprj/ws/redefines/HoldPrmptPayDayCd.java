/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: HOLD-PRMPT-PAY-DAY-CD<br>
 * Variable: HOLD-PRMPT-PAY-DAY-CD from program NF0533ML<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class HoldPrmptPayDayCd extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public HoldPrmptPayDayCd() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.HOLD_PRMPT_PAY_DAY_CD;
	}

	public void setHoldPrmptPayDayCd(String holdPrmptPayDayCd) {
		writeString(Pos.HOLD_PRMPT_PAY_DAY_CD, holdPrmptPayDayCd, Len.HOLD_PRMPT_PAY_DAY_CD);
	}

	/**Original name: HOLD-PRMPT-PAY-DAY-CD<br>*/
	public String getHoldPrmptPayDayCd() {
		return readString(Pos.HOLD_PRMPT_PAY_DAY_CD, Len.HOLD_PRMPT_PAY_DAY_CD);
	}

	public String getHoldPrmptPayDayCdFormatted() {
		return Functions.padBlanks(getHoldPrmptPayDayCd(), Len.HOLD_PRMPT_PAY_DAY_CD);
	}

	/**Original name: HOLD-PRMPT-PAY-DAY-CD-9<br>*/
	public short getHoldPrmptPayDayCd9() {
		return readNumDispUnsignedShort(Pos.HOLD_PRMPT_PAY_DAY_CD9, Len.HOLD_PRMPT_PAY_DAY_CD9);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int HOLD_PRMPT_PAY_DAY_CD = 1;
		public static final int HOLD_PRMPT_PAY_DAY_CD9 = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int HOLD_PRMPT_PAY_DAY_CD = 2;
		public static final int HOLD_PRMPT_PAY_DAY_CD9 = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
