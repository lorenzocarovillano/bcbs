/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: GREGORN<br>
 * Variable: GREGORN from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Gregorn {

	//==== PROPERTIES ====
	//Original name: GREGORN-CC
	private String gregornCc = "00";
	//Original name: GREGORN-YY
	private String gregornYy = "00";
	//Original name: GREGORN-MM
	private String gregornMm = "00";
	//Original name: GREGORN-DD
	private String gregornDd = "00";
	//Original name: R-GREGORN-TIME
	private RGregornTime rGregornTime = new RGregornTime();

	//==== CONSTRUCTORS ====
	public Gregorn() {
		init();
	}

	//==== METHODS ====
	private void init() {
		rGregornTime.setGregornTime(0);
	}

	public void setGregornFormatted(String data) {
		byte[] buffer = new byte[Len.GREGORN];
		MarshalByte.writeString(buffer, 1, data, Len.GREGORN);
		setGregornBytes(buffer, 1);
	}

	public void setGregornBytes(byte[] buffer, int offset) {
		int position = offset;
		setGregornDateBytes(buffer, position);
		position += Len.GREGORN_DATE;
		rGregornTime.setrGregornTimeBytes(buffer, position);
	}

	public void setGregornDateBytes(byte[] buffer, int offset) {
		int position = offset;
		gregornCc = MarshalByte.readFixedString(buffer, position, Len.GREGORN_CC);
		position += Len.GREGORN_CC;
		gregornYy = MarshalByte.readFixedString(buffer, position, Len.GREGORN_YY);
		position += Len.GREGORN_YY;
		gregornMm = MarshalByte.readFixedString(buffer, position, Len.GREGORN_MM);
		position += Len.GREGORN_MM;
		gregornDd = MarshalByte.readFixedString(buffer, position, Len.GREGORN_DD);
	}

	public RGregornTime getrGregornTime() {
		return rGregornTime;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int GREGORN_CC = 2;
		public static final int GREGORN_YY = 2;
		public static final int GREGORN_MM = 2;
		public static final int GREGORN_DD = 2;
		public static final int GREGORN_DATE = GREGORN_CC + GREGORN_YY + GREGORN_MM + GREGORN_DD;
		public static final int GREGORN = GREGORN_DATE + RGregornTime.Len.R_GREGORN_TIME;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
