/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: REM-TAPE-RA-ADDR-NAME<br>
 * Variable: REM-TAPE-RA-ADDR-NAME from copybook NF07RMIT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class RemTapeRaAddrName {

	//==== PROPERTIES ====
	//Original name: REM-TAPE-RA-ADDR-NAME-LAST
	private String last = DefaultValues.stringVal(Len.LAST);
	//Original name: REM-TAPE-RA-ADDR-NAME-FIRST
	private String first = DefaultValues.stringVal(Len.FIRST);
	//Original name: REM-TAPE-RA-ADDR-NAME-MID
	private String mid = DefaultValues.stringVal(Len.MID);

	//==== METHODS ====
	public void setTapeRaAddrNameFormatted(String data) {
		byte[] buffer = new byte[Len.TAPE_RA_ADDR_NAME];
		MarshalByte.writeString(buffer, 1, data, Len.TAPE_RA_ADDR_NAME);
		setTapeRaAddrNameBytes(buffer, 1);
	}

	public void setTapeRaAddrNameBytes(byte[] buffer, int offset) {
		int position = offset;
		last = MarshalByte.readString(buffer, position, Len.LAST);
		position += Len.LAST;
		first = MarshalByte.readString(buffer, position, Len.FIRST);
		position += Len.FIRST;
		mid = MarshalByte.readString(buffer, position, Len.MID);
	}

	public byte[] getTapeRaAddrNameBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, last, Len.LAST);
		position += Len.LAST;
		MarshalByte.writeString(buffer, position, first, Len.FIRST);
		position += Len.FIRST;
		MarshalByte.writeString(buffer, position, mid, Len.MID);
		return buffer;
	}

	public void initTapeRaAddrNameSpaces() {
		last = "";
		first = "";
		mid = "";
	}

	public void setLast(String last) {
		this.last = Functions.subString(last, Len.LAST);
	}

	public String getLast() {
		return this.last;
	}

	public void setFirst(String first) {
		this.first = Functions.subString(first, Len.FIRST);
	}

	public String getFirst() {
		return this.first;
	}

	public void setMid(String mid) {
		this.mid = Functions.subString(mid, Len.MID);
	}

	public String getMid() {
		return this.mid;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LAST = 35;
		public static final int FIRST = 25;
		public static final int MID = 15;
		public static final int TAPE_RA_ADDR_NAME = LAST + FIRST + MID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
