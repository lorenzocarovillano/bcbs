/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.occurs.CsrXrefArea;

/**Original name: NF07AREA<br>
 * Variable: NF07AREA from copybook NF07AREA<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Nf07areaofVsrProgramLineArea {

	//==== PROPERTIES ====
	public static final int XREF_AREA_MAXOCCURS = 6;
	//Original name: VSR-PMT-COMMON-INFO
	private CsrPmtCommonInfo pmtCommonInfo = new CsrPmtCommonInfo();
	//Original name: VSR-CALC-INFORMATION
	private CsrCalcInformation calcInformation = new CsrCalcInformation();
	//Original name: VSR-XREF-AREA
	private CsrXrefArea[] xrefArea = new CsrXrefArea[XREF_AREA_MAXOCCURS];
	//Original name: VSR-XREF-UNBUNDLE-LINE
	public String xrefUnbundleLine = "000";
	//Original name: VSR-XREF-INCD
	private CsrXrefIncd xrefIncd = new CsrXrefIncd();
	//Original name: VSR-PMT-CAS-INFO
	private CsrPmtCasInfo pmtCasInfo = new CsrPmtCasInfo();
	//Original name: VSR-ARC-IND
	private char arcInd = Types.SPACE_CHAR;
	//Original name: VSR-VBR-IN
	private char vbrIn = Types.SPACE_CHAR;
	//Original name: VSR-MRKT-PKG-CD
	private String mrktPkgCd = "";
	//Original name: FILLER-VSR-PROGRAM-LINE-AREA
	private String flr1 = "";

	//==== CONSTRUCTORS ====
	public Nf07areaofVsrProgramLineArea() {
		init();
	}

	//==== METHODS ====
	private void init() {
		for (int xrefAreaIdx = 1; xrefAreaIdx <= XREF_AREA_MAXOCCURS; xrefAreaIdx++) {
			xrefArea[xrefAreaIdx - 1] = new CsrXrefArea();
		}
	}

	public void setArcInd(char arcInd) {
		this.arcInd = arcInd;
	}

	public void setArcIndFormatted(String arcInd) {
		setArcInd(Functions.charAt(arcInd, Types.CHAR_SIZE));
	}

	public char getArcInd() {
		return this.arcInd;
	}

	public void setVbrIn(char vbrIn) {
		this.vbrIn = vbrIn;
	}

	public char getVbrIn() {
		return this.vbrIn;
	}

	public void setMrktPkgCd(String mrktPkgCd) {
		this.mrktPkgCd = Functions.subString(mrktPkgCd, Len.MRKT_PKG_CD);
	}

	public String getMrktPkgCd() {
		return this.mrktPkgCd;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public CsrCalcInformation getCalcInformation() {
		return calcInformation;
	}

	public CsrPmtCasInfo getPmtCasInfo() {
		return pmtCasInfo;
	}

	public CsrPmtCommonInfo getPmtCommonInfo() {
		return pmtCommonInfo;
	}

	public CsrXrefArea getXrefArea(int idx) {
		return xrefArea[idx - 1];
	}

	public CsrXrefIncd getXrefIncd() {
		return xrefIncd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XREF_UNBUNDLE_LINE = 3;
		public static final int MRKT_PKG_CD = 30;
		public static final int ARC_IND = 1;
		public static final int VBR_IN = 1;
		public static final int FLR1 = 25;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
