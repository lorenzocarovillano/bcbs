/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS03086sa;

/**
 * Data Access Object(DAO) for table [S03086SA]
 * 
 */
public class S03086saDao extends BaseSqlDao<IS03086sa> {

	public S03086saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS03086sa> getToClass() {
		return IS03086sa.class;
	}

	public IS03086sa selectRec(String jobName, String stepName, short jobSeqPacked, IS03086sa iS03086sa) {
		return buildQuery("selectRec").bind("jobName", jobName).bind("stepName", stepName).bind("jobSeqPacked", jobSeqPacked).singleResult(iS03086sa);
	}

	public DbAccessStatus updateRec(IS03086sa iS03086sa) {
		return buildQuery("updateRec").bind(iS03086sa).executeUpdate();
	}

	public DbAccessStatus updateRec1(IS03086sa iS03086sa) {
		return buildQuery("updateRec1").bind(iS03086sa).executeUpdate();
	}

	public IS03086sa selectRec1(String name, String step, short seqPacked, IS03086sa iS03086sa) {
		return buildQuery("selectRec1").bind("name", name).bind("step", step).bind("seqPacked", seqPacked).singleResult(iS03086sa);
	}

	public IS03086sa selectRec2(String name, String step, short seqPacked, IS03086sa iS03086sa) {
		return buildQuery("selectRec2").bind("name", name).bind("step", step).bind("seqPacked", seqPacked).singleResult(iS03086sa);
	}
}
