/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.myorg.myprj.ws.redefines.HoldPrmptPayDayCd;

/**Original name: WS-PROMPT-PAY-WORK-FIELDS<br>
 * Variable: WS-PROMPT-PAY-WORK-FIELDS from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsPromptPayWorkFields {

	//==== PROPERTIES ====
	//Original name: TOTAL-CHECK-AMOUNT
	private AfDecimal totalCheckAmount = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: SUBSCRIBER-INTEREST
	private AfDecimal subscriberInterest = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: ACCRUED-SUB-INTEREST
	private AfDecimal accruedSubInterest = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: PROMPT-PAY-DAYS
	private int promptPayDays = DefaultValues.INT_VAL;
	//Original name: CLAIM-AGE
	private int claimAge = DefaultValues.INT_VAL;
	//Original name: HOLD-PRMPT-PAY-DAY-CD
	private HoldPrmptPayDayCd holdPrmptPayDayCd = new HoldPrmptPayDayCd();
	//Original name: LATE-PAYMENT-INTEREST
	private AfDecimal latePaymentInterest = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: LATE-DENIAL-INTEREST
	private AfDecimal lateDenialInterest = new AfDecimal(DefaultValues.DEC_VAL, 9, 2);
	//Original name: CLAIM-LINE-PAYMENTS
	private AfDecimal claimLinePayments = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: CLAIM-LINE-DENIALS
	private AfDecimal claimLineDenials = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: CLAIM-LINE-INTEREST
	private AfDecimal claimLineInterest = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);

	//==== METHODS ====
	public void setTotalCheckAmount(AfDecimal totalCheckAmount) {
		this.totalCheckAmount.assign(totalCheckAmount);
	}

	public AfDecimal getTotalCheckAmount() {
		return this.totalCheckAmount.copy();
	}

	public void setSubscriberInterest(AfDecimal subscriberInterest) {
		this.subscriberInterest.assign(subscriberInterest);
	}

	public AfDecimal getSubscriberInterest() {
		return this.subscriberInterest.copy();
	}

	public void setAccruedSubInterest(AfDecimal accruedSubInterest) {
		this.accruedSubInterest.assign(accruedSubInterest);
	}

	public AfDecimal getAccruedSubInterest() {
		return this.accruedSubInterest.copy();
	}

	public void setPromptPayDays(int promptPayDays) {
		this.promptPayDays = promptPayDays;
	}

	public int getPromptPayDays() {
		return this.promptPayDays;
	}

	public void setClaimAge(int claimAge) {
		this.claimAge = claimAge;
	}

	public int getClaimAge() {
		return this.claimAge;
	}

	public void setLatePaymentInterest(AfDecimal latePaymentInterest) {
		this.latePaymentInterest.assign(latePaymentInterest);
	}

	public AfDecimal getLatePaymentInterest() {
		return this.latePaymentInterest.copy();
	}

	public void setLateDenialInterest(AfDecimal lateDenialInterest) {
		this.lateDenialInterest.assign(lateDenialInterest);
	}

	public AfDecimal getLateDenialInterest() {
		return this.lateDenialInterest.copy();
	}

	public void setClaimLinePayments(AfDecimal claimLinePayments) {
		this.claimLinePayments.assign(claimLinePayments);
	}

	public AfDecimal getClaimLinePayments() {
		return this.claimLinePayments.copy();
	}

	public void setClaimLineDenials(AfDecimal claimLineDenials) {
		this.claimLineDenials.assign(claimLineDenials);
	}

	public AfDecimal getClaimLineDenials() {
		return this.claimLineDenials.copy();
	}

	public void setClaimLineInterest(AfDecimal claimLineInterest) {
		this.claimLineInterest.assign(claimLineInterest);
	}

	public AfDecimal getClaimLineInterest() {
		return this.claimLineInterest.copy();
	}

	public HoldPrmptPayDayCd getHoldPrmptPayDayCd() {
		return holdPrmptPayDayCd;
	}
}
