/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.myorg.myprj.commons.data.to.IS02800ca;

/**Original name: DB2-CAS-CALC1-INFO<br>
 * Variable: DB2-CAS-CALC1-INFO from copybook NF05CURS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Db2CasCalc1Info implements IS02800ca {

	//==== PROPERTIES ====
	//Original name: DB2-CAS-NCOV-AM
	private AfDecimal ncovAm = new AfDecimal(DefaultValues.DEC_VAL, 15, 2);
	//Original name: DB2-CAS-EOB-DED-AM
	private AfDecimal eobDedAm = new AfDecimal(DefaultValues.DEC_VAL, 15, 2);
	//Original name: DB2-CAS-EOB-COINS-AM
	private AfDecimal eobCoinsAm = new AfDecimal(DefaultValues.DEC_VAL, 15, 2);
	//Original name: DB2-CAS-EOB-COPAY-AM
	private AfDecimal eobCopayAm = new AfDecimal(DefaultValues.DEC_VAL, 15, 2);
	//Original name: DB2-CAS-EOB-PENALTY-AM
	private AfDecimal eobPenaltyAm = new AfDecimal(DefaultValues.DEC_VAL, 15, 2);

	//==== METHODS ====
	public void setCasCalc1InfoBytes(byte[] buffer, int offset) {
		int position = offset;
		ncovAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.NCOV_AM, Len.Fract.NCOV_AM));
		position += Len.NCOV_AM;
		eobDedAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.EOB_DED_AM, Len.Fract.EOB_DED_AM));
		position += Len.EOB_DED_AM;
		eobCoinsAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.EOB_COINS_AM, Len.Fract.EOB_COINS_AM));
		position += Len.EOB_COINS_AM;
		eobCopayAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.EOB_COPAY_AM, Len.Fract.EOB_COPAY_AM));
		position += Len.EOB_COPAY_AM;
		eobPenaltyAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.EOB_PENALTY_AM, Len.Fract.EOB_PENALTY_AM));
	}

	public byte[] getCasCalc1InfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeDecimalAsPacked(buffer, position, ncovAm.copy());
		position += Len.NCOV_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, eobDedAm.copy());
		position += Len.EOB_DED_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, eobCoinsAm.copy());
		position += Len.EOB_COINS_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, eobCopayAm.copy());
		position += Len.EOB_COPAY_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, eobPenaltyAm.copy());
		return buffer;
	}

	@Override
	public void setNcovAm(AfDecimal ncovAm) {
		this.ncovAm.assign(ncovAm);
	}

	@Override
	public AfDecimal getNcovAm() {
		return this.ncovAm.copy();
	}

	@Override
	public void setEobDedAm(AfDecimal eobDedAm) {
		this.eobDedAm.assign(eobDedAm);
	}

	@Override
	public AfDecimal getEobDedAm() {
		return this.eobDedAm.copy();
	}

	@Override
	public void setEobCoinsAm(AfDecimal eobCoinsAm) {
		this.eobCoinsAm.assign(eobCoinsAm);
	}

	@Override
	public AfDecimal getEobCoinsAm() {
		return this.eobCoinsAm.copy();
	}

	@Override
	public void setEobCopayAm(AfDecimal eobCopayAm) {
		this.eobCopayAm.assign(eobCopayAm);
	}

	@Override
	public AfDecimal getEobCopayAm() {
		return this.eobCopayAm.copy();
	}

	@Override
	public void setEobPenaltyAm(AfDecimal eobPenaltyAm) {
		this.eobPenaltyAm.assign(eobPenaltyAm);
	}

	@Override
	public AfDecimal getEobPenaltyAm() {
		return this.eobPenaltyAm.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NCOV_AM = 8;
		public static final int EOB_DED_AM = 8;
		public static final int EOB_COINS_AM = 8;
		public static final int EOB_COPAY_AM = 8;
		public static final int EOB_PENALTY_AM = 8;
		public static final int CAS_CALC1_INFO = NCOV_AM + EOB_DED_AM + EOB_COINS_AM + EOB_COPAY_AM + EOB_PENALTY_AM;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int NCOV_AM = 13;
			public static final int EOB_DED_AM = 13;
			public static final int EOB_COINS_AM = 13;
			public static final int EOB_COPAY_AM = 13;
			public static final int EOB_PENALTY_AM = 13;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int NCOV_AM = 2;
			public static final int EOB_DED_AM = 2;
			public static final int EOB_COINS_AM = 2;
			public static final int EOB_COPAY_AM = 2;
			public static final int EOB_PENALTY_AM = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
