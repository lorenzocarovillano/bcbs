/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: CSR-ITS-CLM-TYP-CD<br>
 * Variable: CSR-ITS-CLM-TYP-CD from copybook NF07AREA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CsrItsClmTypCd {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char HOST = 'S';
	public static final char HOME = 'M';
	public static final char BCBSK = 'K';

	//==== METHODS ====
	public void setClmTypCd(char clmTypCd) {
		this.value = clmTypCd;
	}

	public char getClmTypCd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
