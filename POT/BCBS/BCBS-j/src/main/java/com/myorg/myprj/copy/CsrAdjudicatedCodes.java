/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CSR-ADJUDICATED-CODES<br>
 * Variable: CSR-ADJUDICATED-CODES from copybook NF07AREA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CsrAdjudicatedCodes {

	//==== PROPERTIES ====
	//Original name: CSR-PROC-CD
	private String cd = "";
	//Original name: CSR-PROC-MOD1-CD
	private String mod1Cd = "";
	//Original name: CSR-PROC-MOD2-CD
	private String mod2Cd = "";
	//Original name: CSR-PROC-MOD3-CD
	private String mod3Cd = "";
	//Original name: CSR-PROC-MOD4-CD
	private String mod4Cd = "";

	//==== METHODS ====
	public byte[] getAdjudicatedCodesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cd, Len.CD);
		position += Len.CD;
		MarshalByte.writeString(buffer, position, mod1Cd, Len.MOD1_CD);
		position += Len.MOD1_CD;
		MarshalByte.writeString(buffer, position, mod2Cd, Len.MOD2_CD);
		position += Len.MOD2_CD;
		MarshalByte.writeString(buffer, position, mod3Cd, Len.MOD3_CD);
		position += Len.MOD3_CD;
		MarshalByte.writeString(buffer, position, mod4Cd, Len.MOD4_CD);
		return buffer;
	}

	public void initAdjudicatedCodesSpaces() {
		cd = "";
		mod1Cd = "";
		mod2Cd = "";
		mod3Cd = "";
		mod4Cd = "";
	}

	public void setCd(String cd) {
		this.cd = Functions.subString(cd, Len.CD);
	}

	public String getCd() {
		return this.cd;
	}

	public void setMod1Cd(String mod1Cd) {
		this.mod1Cd = Functions.subString(mod1Cd, Len.MOD1_CD);
	}

	public String getMod1Cd() {
		return this.mod1Cd;
	}

	public void setMod2Cd(String mod2Cd) {
		this.mod2Cd = Functions.subString(mod2Cd, Len.MOD2_CD);
	}

	public String getMod2Cd() {
		return this.mod2Cd;
	}

	public void setMod3Cd(String mod3Cd) {
		this.mod3Cd = Functions.subString(mod3Cd, Len.MOD3_CD);
	}

	public String getMod3Cd() {
		return this.mod3Cd;
	}

	public void setMod4Cd(String mod4Cd) {
		this.mod4Cd = Functions.subString(mod4Cd, Len.MOD4_CD);
	}

	public String getMod4Cd() {
		return this.mod4Cd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CD = 5;
		public static final int MOD1_CD = 2;
		public static final int MOD2_CD = 2;
		public static final int MOD3_CD = 2;
		public static final int MOD4_CD = 2;
		public static final int ADJUDICATED_CODES = CD + MOD1_CD + MOD2_CD + MOD3_CD + MOD4_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
