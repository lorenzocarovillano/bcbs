/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.data.fto;

import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.io.file.FileRecord;
import com.myorg.myprj.copy.TriggerRecordLayout;

/**Original name: TRIGGER-IN<br>
 * File: TRIGGER-IN from program NF0533ML<br>
 * Generated as a class for rule FTO.<br>*/
public class TriggerInTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: TRIGGER-RECORD-LAYOUT
	private TriggerRecordLayout triggerRecordLayout = new TriggerRecordLayout();

	//==== METHODS ====
	@Override
	public void getData(byte[] destination, int offset) {
		triggerRecordLayout.getTriggerRecordLayoutBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		triggerRecordLayout.setTriggerRecordLayoutBytes(data, offset);
	}

	@Override
	public int getLength() {
		return TriggerRecordLayout.Len.TRIGGER_RECORD_LAYOUT;
	}

	public TriggerRecordLayout getTriggerRecordLayout() {
		return triggerRecordLayout;
	}

	@Override
	public IBuffer copy() {
		TriggerInTO copyTO = new TriggerInTO();
		copyTO.assign(this);
		return copyTO;
	}
}
