/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [S02814SA]
 * 
 */
public interface IS02814sa extends BaseSqlTo {

	/**
	 * Host Variable CLM-CNTL-ID
	 * 
	 */
	String getClmCntlId();

	void setClmCntlId(String clmCntlId);

	/**
	 * Host Variable CLM-CNTL-SFX-ID
	 * 
	 */
	char getClmCntlSfxId();

	void setClmCntlSfxId(char clmCntlSfxId);

	/**
	 * Host Variable CLM-PMT-ID
	 * 
	 */
	short getClmPmtId();

	void setClmPmtId(short clmPmtId);

	/**
	 * Host Variable EVNT-LOG-STAT-CD
	 * 
	 */
	String getEvntLogStatCd();

	void setEvntLogStatCd(String evntLogStatCd);

	/**
	 * Host Variable EVNT-LOG-BUS-DT
	 * 
	 */
	String getEvntLogBusDt();

	void setEvntLogBusDt(String evntLogBusDt);

	/**
	 * Host Variable EVNT-LOG-SYST-DT
	 * 
	 */
	String getEvntLogSystDt();

	void setEvntLogSystDt(String evntLogSystDt);

	/**
	 * Host Variable DB2-PMT-KCAPS-TEAM-NM
	 * 
	 */
	String getDb2PmtKcapsTeamNm();

	void setDb2PmtKcapsTeamNm(String db2PmtKcapsTeamNm);

	/**
	 * Host Variable DB2-PMT-KCAPS-USE-ID
	 * 
	 */
	String getDb2PmtKcapsUseId();

	void setDb2PmtKcapsUseId(String db2PmtKcapsUseId);

	/**
	 * Host Variable KCAPS-TEAM-NM
	 * 
	 */
	String getKcapsTeamNm();

	void setKcapsTeamNm(String kcapsTeamNm);

	/**
	 * Host Variable KCAPS-USE-ID
	 * 
	 */
	String getKcapsUseId();

	void setKcapsUseId(String kcapsUseId);
};
