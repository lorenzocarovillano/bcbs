/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.commons.data.to.IS02809sa;
import com.myorg.myprj.commons.data.to.IS02815sa;

/**Original name: DB2-S02801-INFO<br>
 * Variable: DB2-S02801-INFO from copybook NF05CURS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Db2S02801Info implements IS02809sa, IS02815sa {

	//==== PROPERTIES ====
	//Original name: DB2-PROD-IND
	private String prodInd = DefaultValues.stringVal(Len.PROD_IND);
	//Original name: DB2-CLM-CNTL-ID
	private String clmCntlId = DefaultValues.stringVal(Len.CLM_CNTL_ID);
	//Original name: DB2-CLM-CNTL-SFX-ID
	private char clmCntlSfxId = DefaultValues.CHAR_VAL;
	//Original name: DB2-CLM-PMT-ID
	private short clmPmtId = DefaultValues.SHORT_VAL;
	//Original name: DB2-CLI-ID
	private short cliId = DefaultValues.SHORT_VAL;
	//Original name: DB2-FNL-BUS-DT
	private String fnlBusDt = DefaultValues.stringVal(Len.FNL_BUS_DT);
	//Original name: DB2-STAT-CATG-CD
	private String statCatgCd = DefaultValues.stringVal(Len.STAT_CATG_CD);

	//==== METHODS ====
	public void setS02801InfoBytes(byte[] buffer, int offset) {
		int position = offset;
		prodInd = MarshalByte.readString(buffer, position, Len.PROD_IND);
		position += Len.PROD_IND;
		clmCntlId = MarshalByte.readString(buffer, position, Len.CLM_CNTL_ID);
		position += Len.CLM_CNTL_ID;
		clmCntlSfxId = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		clmPmtId = MarshalByte.readPackedAsShort(buffer, position, Len.Int.CLM_PMT_ID, 0);
		position += Len.CLM_PMT_ID;
		cliId = MarshalByte.readPackedAsShort(buffer, position, Len.Int.CLI_ID, 0);
		position += Len.CLI_ID;
		fnlBusDt = MarshalByte.readString(buffer, position, Len.FNL_BUS_DT);
		position += Len.FNL_BUS_DT;
		statCatgCd = MarshalByte.readString(buffer, position, Len.STAT_CATG_CD);
	}

	public byte[] getS02801InfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, prodInd, Len.PROD_IND);
		position += Len.PROD_IND;
		MarshalByte.writeString(buffer, position, clmCntlId, Len.CLM_CNTL_ID);
		position += Len.CLM_CNTL_ID;
		MarshalByte.writeChar(buffer, position, clmCntlSfxId);
		position += Types.CHAR_SIZE;
		MarshalByte.writeShortAsPacked(buffer, position, clmPmtId, Len.Int.CLM_PMT_ID, 0);
		position += Len.CLM_PMT_ID;
		MarshalByte.writeShortAsPacked(buffer, position, cliId, Len.Int.CLI_ID, 0);
		position += Len.CLI_ID;
		MarshalByte.writeString(buffer, position, fnlBusDt, Len.FNL_BUS_DT);
		position += Len.FNL_BUS_DT;
		MarshalByte.writeString(buffer, position, statCatgCd, Len.STAT_CATG_CD);
		return buffer;
	}

	public void setProdInd(String prodInd) {
		this.prodInd = Functions.subString(prodInd, Len.PROD_IND);
	}

	public String getProdInd() {
		return this.prodInd;
	}

	public void setClmCntlId(String clmCntlId) {
		this.clmCntlId = Functions.subString(clmCntlId, Len.CLM_CNTL_ID);
	}

	public String getClmCntlId() {
		return this.clmCntlId;
	}

	public String getClmCntlIdFormatted() {
		return Functions.padBlanks(getClmCntlId(), Len.CLM_CNTL_ID);
	}

	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.clmCntlSfxId = clmCntlSfxId;
	}

	public char getClmCntlSfxId() {
		return this.clmCntlSfxId;
	}

	public void setClmPmtId(short clmPmtId) {
		this.clmPmtId = clmPmtId;
	}

	public short getClmPmtId() {
		return this.clmPmtId;
	}

	public void setCliId(short cliId) {
		this.cliId = cliId;
	}

	public short getCliId() {
		return this.cliId;
	}

	public void setFnlBusDt(String fnlBusDt) {
		this.fnlBusDt = Functions.subString(fnlBusDt, Len.FNL_BUS_DT);
	}

	public String getFnlBusDt() {
		return this.fnlBusDt;
	}

	public void setStatCatgCd(String statCatgCd) {
		this.statCatgCd = Functions.subString(statCatgCd, Len.STAT_CATG_CD);
	}

	public String getStatCatgCd() {
		return this.statCatgCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_CNTL_ID = 12;
		public static final int PROD_IND = 3;
		public static final int FNL_BUS_DT = 10;
		public static final int STAT_CATG_CD = 3;
		public static final int CLM_CNTL_SFX_ID = 1;
		public static final int CLM_PMT_ID = 2;
		public static final int CLI_ID = 2;
		public static final int S02801_INFO = PROD_IND + CLM_CNTL_ID + CLM_CNTL_SFX_ID + CLM_PMT_ID + CLI_ID + FNL_BUS_DT + STAT_CATG_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int CLM_PMT_ID = 2;
			public static final int CLI_ID = 3;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
