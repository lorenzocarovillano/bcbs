/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: NF05-NATION-PROGRAM-INDC<br>
 * Variable: NF05-NATION-PROGRAM-INDC from copybook NF05EOB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Nf05NationProgramIndc {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char IPDR_ACCOUNT = 'N';
	public static final char FEP = 'F';
	public static final char CENTRAL_CERT_ACCOUNT = 'C';
	public static final char HOST_PLAN_RECIPROCITY = 'R';
	public static final char INTER_PLAN_BANK = 'I';
	public static final char NOT_NATIONAL_PROGRAM = ' ';
	public static final char NATION_PROGRAM_NON_IPDR = 'L';
	public static final char PSI = 'P';
	public static final char NASCO = 'S';
	public static final char GEHA = 'G';
	public static final char WALMART = 'W';

	//==== METHODS ====
	public void setNf05NationProgramIndc(char nf05NationProgramIndc) {
		this.value = nf05NationProgramIndc;
	}

	public char getNf05NationProgramIndc() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
