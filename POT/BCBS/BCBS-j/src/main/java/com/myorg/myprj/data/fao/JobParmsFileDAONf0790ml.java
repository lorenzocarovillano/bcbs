/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.data.fao;

import com.bphx.ctu.af.io.file.AbstractInputOutputDAO;
import com.bphx.ctu.af.io.file.FileAccessStatus;
import com.myorg.myprj.data.fto.JobParmsFileTO;

/**Original name: JOB-PARMS-FILE/JOBPARMS/JOBPARMS[NF0790ML]<br>*/
public class JobParmsFileDAONf0790ml extends AbstractInputOutputDAO<JobParmsFileTO> {

	//==== PROPERTIES ====
	public static final int RECORD_SIZE = 80;

	//==== CONSTRUCTORS ====
	public JobParmsFileDAONf0790ml(FileAccessStatus fileStatus) {
		super(fileStatus);
	}

	//==== METHODS ====
	@Override
	public String getName() {
		return "JOBPARMS";
	}

	@Override
	public int getRecordSize() {
		return RECORD_SIZE;
	}

	@Override
	public JobParmsFileTO createTo() {
		return new JobParmsFileTO();
	}

	@Override
	public JobParmsFileTO read() {
		JobParmsFileTO jobParmsFileTO = new JobParmsFileTO();
		return read(jobParmsFileTO);
	}
}
