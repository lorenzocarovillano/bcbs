/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS02952sa;

/**
 * Data Access Object(DAO) for table [S02952SA]
 * 
 */
public class S02952saDao extends BaseSqlDao<IS02952sa> {

	public S02952saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02952sa> getToClass() {
		return IS02952sa.class;
	}

	public IS02952sa selectRec(String cntlId, char cntlSfxId, short pmtId, IS02952sa iS02952sa) {
		return buildQuery("selectRec").bind("cntlId", cntlId).bind("cntlSfxId", String.valueOf(cntlSfxId)).bind("pmtId", pmtId)
				.singleResult(iS02952sa);
	}

	public IS02952sa selectRec1(String cntlId, char cntlSfxId, short pmtId, IS02952sa iS02952sa) {
		return buildQuery("selectRec1").bind("cntlId", cntlId).bind("cntlSfxId", String.valueOf(cntlSfxId)).bind("pmtId", pmtId)
				.singleResult(iS02952sa);
	}

	public IS02952sa selectRec2(String cntlId, char cntlSfxId, short pmtId, IS02952sa iS02952sa) {
		return buildQuery("selectRec2").bind("cntlId", cntlId).bind("cntlSfxId", String.valueOf(cntlSfxId)).bind("pmtId", pmtId)
				.singleResult(iS02952sa);
	}

	public IS02952sa selectRec3(String cntlId, char cntlSfxId, short pmtId, IS02952sa iS02952sa) {
		return buildQuery("selectRec3").bind("cntlId", cntlId).bind("cntlSfxId", String.valueOf(cntlSfxId)).bind("pmtId", pmtId)
				.singleResult(iS02952sa);
	}
}
