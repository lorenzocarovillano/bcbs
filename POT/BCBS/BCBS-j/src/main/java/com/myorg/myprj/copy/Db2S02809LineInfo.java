/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DB2-S02809-LINE-INFO<br>
 * Variable: DB2-S02809-LINE-INFO from copybook NF05CURS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Db2S02809LineInfo {

	//==== PROPERTIES ====
	//Original name: DB2-LI-FRM-SERV-DT
	private String frmServDt = DefaultValues.stringVal(Len.FRM_SERV_DT);
	//Original name: DB2-LI-THR-SERV-DT
	private String thrServDt = DefaultValues.stringVal(Len.THR_SERV_DT);
	//Original name: DB2-LI-BEN-TYP-CD
	private String benTypCd = DefaultValues.stringVal(Len.BEN_TYP_CD);
	//Original name: DB2-LI-EXMN-ACTN-CD
	private char exmnActnCd = DefaultValues.CHAR_VAL;
	//Original name: DB2-LI-RMRK-CD
	private String rmrkCd = DefaultValues.stringVal(Len.RMRK_CD);
	//Original name: DB2-LI-EXPLN-CD
	private char explnCd = DefaultValues.CHAR_VAL;
	//Original name: DB2-LI-RVW-BY-CD
	private char rvwByCd = DefaultValues.CHAR_VAL;
	//Original name: DB2-LI-ADJD-OVRD1-CD
	private String adjdOvrd1Cd = DefaultValues.stringVal(Len.ADJD_OVRD1_CD);
	//Original name: DB2-LI-ADJD-OVRD2-CD
	private String adjdOvrd2Cd = DefaultValues.stringVal(Len.ADJD_OVRD2_CD);
	//Original name: DB2-LI-ADJD-OVRD3-CD
	private String adjdOvrd3Cd = DefaultValues.stringVal(Len.ADJD_OVRD3_CD);
	//Original name: DB2-LI-ADJD-OVRD4-CD
	private String adjdOvrd4Cd = DefaultValues.stringVal(Len.ADJD_OVRD4_CD);
	//Original name: DB2-LI-ADJD-OVRD5-CD
	private String adjdOvrd5Cd = DefaultValues.stringVal(Len.ADJD_OVRD5_CD);
	//Original name: DB2-LI-ADJD-OVRD6-CD
	private String adjdOvrd6Cd = DefaultValues.stringVal(Len.ADJD_OVRD6_CD);
	//Original name: DB2-LI-ADJD-OVRD7-CD
	private String adjdOvrd7Cd = DefaultValues.stringVal(Len.ADJD_OVRD7_CD);
	//Original name: DB2-LI-ADJD-OVRD8-CD
	private String adjdOvrd8Cd = DefaultValues.stringVal(Len.ADJD_OVRD8_CD);
	//Original name: DB2-LI-ADJD-OVRD9-CD
	private String adjdOvrd9Cd = DefaultValues.stringVal(Len.ADJD_OVRD9_CD);
	//Original name: DB2-LI-ADJD-OVRD10-CD
	private String adjdOvrd10Cd = DefaultValues.stringVal(Len.ADJD_OVRD10_CD);
	//Original name: DB2-LI-POS-CD
	private String posCd = DefaultValues.stringVal(Len.POS_CD);
	//Original name: DB2-LI-TS-CD
	private String tsCd = DefaultValues.stringVal(Len.TS_CD);
	//Original name: DB2-LI-CHG-AM
	private AfDecimal chgAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: DB2-LI-PAT-RESP-AM
	private AfDecimal patRespAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: DB2-LI-PWO-AM
	private AfDecimal pwoAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: DB2-LI-DED-CRVR-CD
	private char dedCrvrCd = DefaultValues.CHAR_VAL;
	//Original name: DB2-LI-COP-CRVR-CD
	private char copCrvrCd = DefaultValues.CHAR_VAL;
	//Original name: DB2-LI-RFR-OPT-CD
	private char rfrOptCd = DefaultValues.CHAR_VAL;
	//Original name: DB2-LI-COV-PMT-LVL-STT-CD
	private String covPmtLvlSttCd = DefaultValues.stringVal(Len.COV_PMT_LVL_STT_CD);
	//Original name: DB2-LI-ALW-CHG-AM
	private AfDecimal alwChgAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: DB2-LI-SRO-RMRK-CD
	private String sroRmrkCd = DefaultValues.stringVal(Len.SRO_RMRK_CD);
	//Original name: DB2-LI-ALCT-OI-COV-AM
	private AfDecimal alctOiCovAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: DB2-LI-ALCT-OI-PD-AM
	private AfDecimal alctOiPdAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: DB2-LI-ALCT-OI-SAVE-AM
	private AfDecimal alctOiSaveAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: DB2-LI-NCOV-AM
	private AfDecimal ncovAm = new AfDecimal(DefaultValues.DEC_VAL, 15, 2);
	//Original name: DB2-LI-PROC-CD
	private String procCd = DefaultValues.stringVal(Len.PROC_CD);

	//==== METHODS ====
	public void setS02809LineInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		frmServDt = MarshalByte.readString(buffer, position, Len.FRM_SERV_DT);
		position += Len.FRM_SERV_DT;
		thrServDt = MarshalByte.readString(buffer, position, Len.THR_SERV_DT);
		position += Len.THR_SERV_DT;
		benTypCd = MarshalByte.readString(buffer, position, Len.BEN_TYP_CD);
		position += Len.BEN_TYP_CD;
		exmnActnCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rmrkCd = MarshalByte.readString(buffer, position, Len.RMRK_CD);
		position += Len.RMRK_CD;
		explnCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rvwByCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		adjdOvrd1Cd = MarshalByte.readString(buffer, position, Len.ADJD_OVRD1_CD);
		position += Len.ADJD_OVRD1_CD;
		adjdOvrd2Cd = MarshalByte.readString(buffer, position, Len.ADJD_OVRD2_CD);
		position += Len.ADJD_OVRD2_CD;
		adjdOvrd3Cd = MarshalByte.readString(buffer, position, Len.ADJD_OVRD3_CD);
		position += Len.ADJD_OVRD3_CD;
		adjdOvrd4Cd = MarshalByte.readString(buffer, position, Len.ADJD_OVRD4_CD);
		position += Len.ADJD_OVRD4_CD;
		adjdOvrd5Cd = MarshalByte.readString(buffer, position, Len.ADJD_OVRD5_CD);
		position += Len.ADJD_OVRD5_CD;
		adjdOvrd6Cd = MarshalByte.readString(buffer, position, Len.ADJD_OVRD6_CD);
		position += Len.ADJD_OVRD6_CD;
		adjdOvrd7Cd = MarshalByte.readString(buffer, position, Len.ADJD_OVRD7_CD);
		position += Len.ADJD_OVRD7_CD;
		adjdOvrd8Cd = MarshalByte.readString(buffer, position, Len.ADJD_OVRD8_CD);
		position += Len.ADJD_OVRD8_CD;
		adjdOvrd9Cd = MarshalByte.readString(buffer, position, Len.ADJD_OVRD9_CD);
		position += Len.ADJD_OVRD9_CD;
		adjdOvrd10Cd = MarshalByte.readString(buffer, position, Len.ADJD_OVRD10_CD);
		position += Len.ADJD_OVRD10_CD;
		posCd = MarshalByte.readString(buffer, position, Len.POS_CD);
		position += Len.POS_CD;
		tsCd = MarshalByte.readString(buffer, position, Len.TS_CD);
		position += Len.TS_CD;
		chgAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.CHG_AM, Len.Fract.CHG_AM));
		position += Len.CHG_AM;
		patRespAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PAT_RESP_AM, Len.Fract.PAT_RESP_AM));
		position += Len.PAT_RESP_AM;
		pwoAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PWO_AM, Len.Fract.PWO_AM));
		position += Len.PWO_AM;
		dedCrvrCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		copCrvrCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rfrOptCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		covPmtLvlSttCd = MarshalByte.readString(buffer, position, Len.COV_PMT_LVL_STT_CD);
		position += Len.COV_PMT_LVL_STT_CD;
		alwChgAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ALW_CHG_AM, Len.Fract.ALW_CHG_AM));
		position += Len.ALW_CHG_AM;
		sroRmrkCd = MarshalByte.readString(buffer, position, Len.SRO_RMRK_CD);
		position += Len.SRO_RMRK_CD;
		alctOiCovAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ALCT_OI_COV_AM, Len.Fract.ALCT_OI_COV_AM));
		position += Len.ALCT_OI_COV_AM;
		alctOiPdAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ALCT_OI_PD_AM, Len.Fract.ALCT_OI_PD_AM));
		position += Len.ALCT_OI_PD_AM;
		alctOiSaveAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ALCT_OI_SAVE_AM, Len.Fract.ALCT_OI_SAVE_AM));
		position += Len.ALCT_OI_SAVE_AM;
		ncovAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.NCOV_AM, Len.Fract.NCOV_AM));
		position += Len.NCOV_AM;
		procCd = MarshalByte.readString(buffer, position, Len.PROC_CD);
	}

	public byte[] getS02809LineInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, frmServDt, Len.FRM_SERV_DT);
		position += Len.FRM_SERV_DT;
		MarshalByte.writeString(buffer, position, thrServDt, Len.THR_SERV_DT);
		position += Len.THR_SERV_DT;
		MarshalByte.writeString(buffer, position, benTypCd, Len.BEN_TYP_CD);
		position += Len.BEN_TYP_CD;
		MarshalByte.writeChar(buffer, position, exmnActnCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rmrkCd, Len.RMRK_CD);
		position += Len.RMRK_CD;
		MarshalByte.writeChar(buffer, position, explnCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, rvwByCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, adjdOvrd1Cd, Len.ADJD_OVRD1_CD);
		position += Len.ADJD_OVRD1_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd2Cd, Len.ADJD_OVRD2_CD);
		position += Len.ADJD_OVRD2_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd3Cd, Len.ADJD_OVRD3_CD);
		position += Len.ADJD_OVRD3_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd4Cd, Len.ADJD_OVRD4_CD);
		position += Len.ADJD_OVRD4_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd5Cd, Len.ADJD_OVRD5_CD);
		position += Len.ADJD_OVRD5_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd6Cd, Len.ADJD_OVRD6_CD);
		position += Len.ADJD_OVRD6_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd7Cd, Len.ADJD_OVRD7_CD);
		position += Len.ADJD_OVRD7_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd8Cd, Len.ADJD_OVRD8_CD);
		position += Len.ADJD_OVRD8_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd9Cd, Len.ADJD_OVRD9_CD);
		position += Len.ADJD_OVRD9_CD;
		MarshalByte.writeString(buffer, position, adjdOvrd10Cd, Len.ADJD_OVRD10_CD);
		position += Len.ADJD_OVRD10_CD;
		MarshalByte.writeString(buffer, position, posCd, Len.POS_CD);
		position += Len.POS_CD;
		MarshalByte.writeString(buffer, position, tsCd, Len.TS_CD);
		position += Len.TS_CD;
		MarshalByte.writeDecimalAsPacked(buffer, position, chgAm.copy());
		position += Len.CHG_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, patRespAm.copy());
		position += Len.PAT_RESP_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, pwoAm.copy());
		position += Len.PWO_AM;
		MarshalByte.writeChar(buffer, position, dedCrvrCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, copCrvrCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, rfrOptCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, covPmtLvlSttCd, Len.COV_PMT_LVL_STT_CD);
		position += Len.COV_PMT_LVL_STT_CD;
		MarshalByte.writeDecimalAsPacked(buffer, position, alwChgAm.copy());
		position += Len.ALW_CHG_AM;
		MarshalByte.writeString(buffer, position, sroRmrkCd, Len.SRO_RMRK_CD);
		position += Len.SRO_RMRK_CD;
		MarshalByte.writeDecimalAsPacked(buffer, position, alctOiCovAm.copy());
		position += Len.ALCT_OI_COV_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, alctOiPdAm.copy());
		position += Len.ALCT_OI_PD_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, alctOiSaveAm.copy());
		position += Len.ALCT_OI_SAVE_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, ncovAm.copy());
		position += Len.NCOV_AM;
		MarshalByte.writeString(buffer, position, procCd, Len.PROC_CD);
		return buffer;
	}

	public void setFrmServDt(String frmServDt) {
		this.frmServDt = Functions.subString(frmServDt, Len.FRM_SERV_DT);
	}

	public String getFrmServDt() {
		return this.frmServDt;
	}

	public String getFrmServDtFormatted() {
		return Functions.padBlanks(getFrmServDt(), Len.FRM_SERV_DT);
	}

	public void setThrServDt(String thrServDt) {
		this.thrServDt = Functions.subString(thrServDt, Len.THR_SERV_DT);
	}

	public String getThrServDt() {
		return this.thrServDt;
	}

	public String getThrServDtFormatted() {
		return Functions.padBlanks(getThrServDt(), Len.THR_SERV_DT);
	}

	public void setBenTypCd(String benTypCd) {
		this.benTypCd = Functions.subString(benTypCd, Len.BEN_TYP_CD);
	}

	public String getBenTypCd() {
		return this.benTypCd;
	}

	public void setExmnActnCd(char exmnActnCd) {
		this.exmnActnCd = exmnActnCd;
	}

	public char getExmnActnCd() {
		return this.exmnActnCd;
	}

	public void setRmrkCd(String rmrkCd) {
		this.rmrkCd = Functions.subString(rmrkCd, Len.RMRK_CD);
	}

	public String getRmrkCd() {
		return this.rmrkCd;
	}

	public void setExplnCd(char explnCd) {
		this.explnCd = explnCd;
	}

	public char getExplnCd() {
		return this.explnCd;
	}

	public void setRvwByCd(char rvwByCd) {
		this.rvwByCd = rvwByCd;
	}

	public char getRvwByCd() {
		return this.rvwByCd;
	}

	public void setAdjdOvrd1Cd(String adjdOvrd1Cd) {
		this.adjdOvrd1Cd = Functions.subString(adjdOvrd1Cd, Len.ADJD_OVRD1_CD);
	}

	public String getAdjdOvrd1Cd() {
		return this.adjdOvrd1Cd;
	}

	public void setAdjdOvrd2Cd(String adjdOvrd2Cd) {
		this.adjdOvrd2Cd = Functions.subString(adjdOvrd2Cd, Len.ADJD_OVRD2_CD);
	}

	public String getAdjdOvrd2Cd() {
		return this.adjdOvrd2Cd;
	}

	public void setAdjdOvrd3Cd(String adjdOvrd3Cd) {
		this.adjdOvrd3Cd = Functions.subString(adjdOvrd3Cd, Len.ADJD_OVRD3_CD);
	}

	public String getAdjdOvrd3Cd() {
		return this.adjdOvrd3Cd;
	}

	public void setAdjdOvrd4Cd(String adjdOvrd4Cd) {
		this.adjdOvrd4Cd = Functions.subString(adjdOvrd4Cd, Len.ADJD_OVRD4_CD);
	}

	public String getAdjdOvrd4Cd() {
		return this.adjdOvrd4Cd;
	}

	public void setAdjdOvrd5Cd(String adjdOvrd5Cd) {
		this.adjdOvrd5Cd = Functions.subString(adjdOvrd5Cd, Len.ADJD_OVRD5_CD);
	}

	public String getAdjdOvrd5Cd() {
		return this.adjdOvrd5Cd;
	}

	public void setAdjdOvrd6Cd(String adjdOvrd6Cd) {
		this.adjdOvrd6Cd = Functions.subString(adjdOvrd6Cd, Len.ADJD_OVRD6_CD);
	}

	public String getAdjdOvrd6Cd() {
		return this.adjdOvrd6Cd;
	}

	public void setAdjdOvrd7Cd(String adjdOvrd7Cd) {
		this.adjdOvrd7Cd = Functions.subString(adjdOvrd7Cd, Len.ADJD_OVRD7_CD);
	}

	public String getAdjdOvrd7Cd() {
		return this.adjdOvrd7Cd;
	}

	public void setAdjdOvrd8Cd(String adjdOvrd8Cd) {
		this.adjdOvrd8Cd = Functions.subString(adjdOvrd8Cd, Len.ADJD_OVRD8_CD);
	}

	public String getAdjdOvrd8Cd() {
		return this.adjdOvrd8Cd;
	}

	public void setAdjdOvrd9Cd(String adjdOvrd9Cd) {
		this.adjdOvrd9Cd = Functions.subString(adjdOvrd9Cd, Len.ADJD_OVRD9_CD);
	}

	public String getAdjdOvrd9Cd() {
		return this.adjdOvrd9Cd;
	}

	public void setAdjdOvrd10Cd(String adjdOvrd10Cd) {
		this.adjdOvrd10Cd = Functions.subString(adjdOvrd10Cd, Len.ADJD_OVRD10_CD);
	}

	public String getAdjdOvrd10Cd() {
		return this.adjdOvrd10Cd;
	}

	public void setPosCd(String posCd) {
		this.posCd = Functions.subString(posCd, Len.POS_CD);
	}

	public String getPosCd() {
		return this.posCd;
	}

	public void setTsCd(String tsCd) {
		this.tsCd = Functions.subString(tsCd, Len.TS_CD);
	}

	public String getTsCd() {
		return this.tsCd;
	}

	public void setChgAm(AfDecimal chgAm) {
		this.chgAm.assign(chgAm);
	}

	public AfDecimal getChgAm() {
		return this.chgAm.copy();
	}

	public void setPatRespAm(AfDecimal patRespAm) {
		this.patRespAm.assign(patRespAm);
	}

	public AfDecimal getPatRespAm() {
		return this.patRespAm.copy();
	}

	public void setPwoAm(AfDecimal pwoAm) {
		this.pwoAm.assign(pwoAm);
	}

	public AfDecimal getPwoAm() {
		return this.pwoAm.copy();
	}

	public void setDedCrvrCd(char dedCrvrCd) {
		this.dedCrvrCd = dedCrvrCd;
	}

	public char getDedCrvrCd() {
		return this.dedCrvrCd;
	}

	public void setCopCrvrCd(char copCrvrCd) {
		this.copCrvrCd = copCrvrCd;
	}

	public char getCopCrvrCd() {
		return this.copCrvrCd;
	}

	public void setRfrOptCd(char rfrOptCd) {
		this.rfrOptCd = rfrOptCd;
	}

	public char getRfrOptCd() {
		return this.rfrOptCd;
	}

	public void setCovPmtLvlSttCd(String covPmtLvlSttCd) {
		this.covPmtLvlSttCd = Functions.subString(covPmtLvlSttCd, Len.COV_PMT_LVL_STT_CD);
	}

	public String getCovPmtLvlSttCd() {
		return this.covPmtLvlSttCd;
	}

	public void setAlwChgAm(AfDecimal alwChgAm) {
		this.alwChgAm.assign(alwChgAm);
	}

	public AfDecimal getAlwChgAm() {
		return this.alwChgAm.copy();
	}

	public void setSroRmrkCd(String sroRmrkCd) {
		this.sroRmrkCd = Functions.subString(sroRmrkCd, Len.SRO_RMRK_CD);
	}

	public String getSroRmrkCd() {
		return this.sroRmrkCd;
	}

	public void setAlctOiCovAm(AfDecimal alctOiCovAm) {
		this.alctOiCovAm.assign(alctOiCovAm);
	}

	public AfDecimal getAlctOiCovAm() {
		return this.alctOiCovAm.copy();
	}

	public void setAlctOiPdAm(AfDecimal alctOiPdAm) {
		this.alctOiPdAm.assign(alctOiPdAm);
	}

	public AfDecimal getAlctOiPdAm() {
		return this.alctOiPdAm.copy();
	}

	public void setAlctOiSaveAm(AfDecimal alctOiSaveAm) {
		this.alctOiSaveAm.assign(alctOiSaveAm);
	}

	public AfDecimal getAlctOiSaveAm() {
		return this.alctOiSaveAm.copy();
	}

	public void setNcovAm(AfDecimal ncovAm) {
		this.ncovAm.assign(ncovAm);
	}

	public AfDecimal getNcovAm() {
		return this.ncovAm.copy();
	}

	public void setProcCd(String procCd) {
		this.procCd = Functions.subString(procCd, Len.PROC_CD);
	}

	public String getProcCd() {
		return this.procCd;
	}

	public String getProcCdFormatted() {
		return Functions.padBlanks(getProcCd(), Len.PROC_CD);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FRM_SERV_DT = 10;
		public static final int THR_SERV_DT = 10;
		public static final int BEN_TYP_CD = 3;
		public static final int RMRK_CD = 3;
		public static final int ADJD_OVRD1_CD = 3;
		public static final int ADJD_OVRD2_CD = 3;
		public static final int ADJD_OVRD3_CD = 3;
		public static final int ADJD_OVRD4_CD = 3;
		public static final int ADJD_OVRD5_CD = 3;
		public static final int ADJD_OVRD6_CD = 3;
		public static final int ADJD_OVRD7_CD = 3;
		public static final int ADJD_OVRD8_CD = 3;
		public static final int ADJD_OVRD9_CD = 3;
		public static final int ADJD_OVRD10_CD = 3;
		public static final int POS_CD = 2;
		public static final int TS_CD = 2;
		public static final int PROC_CD = 5;
		public static final int COV_PMT_LVL_STT_CD = 3;
		public static final int SRO_RMRK_CD = 2;
		public static final int EXMN_ACTN_CD = 1;
		public static final int EXPLN_CD = 1;
		public static final int RVW_BY_CD = 1;
		public static final int CHG_AM = 6;
		public static final int PAT_RESP_AM = 6;
		public static final int PWO_AM = 6;
		public static final int DED_CRVR_CD = 1;
		public static final int COP_CRVR_CD = 1;
		public static final int RFR_OPT_CD = 1;
		public static final int ALW_CHG_AM = 6;
		public static final int ALCT_OI_COV_AM = 6;
		public static final int ALCT_OI_PD_AM = 6;
		public static final int ALCT_OI_SAVE_AM = 6;
		public static final int NCOV_AM = 8;
		public static final int S02809_LINE_INFO = FRM_SERV_DT + THR_SERV_DT + BEN_TYP_CD + EXMN_ACTN_CD + RMRK_CD + EXPLN_CD + RVW_BY_CD
				+ ADJD_OVRD1_CD + ADJD_OVRD2_CD + ADJD_OVRD3_CD + ADJD_OVRD4_CD + ADJD_OVRD5_CD + ADJD_OVRD6_CD + ADJD_OVRD7_CD + ADJD_OVRD8_CD
				+ ADJD_OVRD9_CD + ADJD_OVRD10_CD + POS_CD + TS_CD + CHG_AM + PAT_RESP_AM + PWO_AM + DED_CRVR_CD + COP_CRVR_CD + RFR_OPT_CD
				+ COV_PMT_LVL_STT_CD + ALW_CHG_AM + SRO_RMRK_CD + ALCT_OI_COV_AM + ALCT_OI_PD_AM + ALCT_OI_SAVE_AM + NCOV_AM + PROC_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int CHG_AM = 9;
			public static final int PAT_RESP_AM = 9;
			public static final int PWO_AM = 9;
			public static final int ALW_CHG_AM = 9;
			public static final int ALCT_OI_COV_AM = 9;
			public static final int ALCT_OI_PD_AM = 9;
			public static final int ALCT_OI_SAVE_AM = 9;
			public static final int NCOV_AM = 13;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int CHG_AM = 2;
			public static final int PAT_RESP_AM = 2;
			public static final int PWO_AM = 2;
			public static final int ALW_CHG_AM = 2;
			public static final int ALCT_OI_COV_AM = 2;
			public static final int ALCT_OI_PD_AM = 2;
			public static final int ALCT_OI_SAVE_AM = 2;
			public static final int NCOV_AM = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
