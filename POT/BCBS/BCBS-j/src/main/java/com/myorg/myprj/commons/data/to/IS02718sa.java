/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [S02718SA]
 * 
 */
public interface IS02718sa extends BaseSqlTo {

	/**
	 * Host Variable SV201-PROD-SRV-ID
	 * 
	 */
	String getSv201ProdSrvId();

	void setSv201ProdSrvId(String sv201ProdSrvId);

	/**
	 * Host Variable PROD-SRV-ID-QLF-CD
	 * 
	 */
	String getProdSrvIdQlfCd();

	void setProdSrvIdQlfCd(String prodSrvIdQlfCd);

	/**
	 * Host Variable SV2022-PROD-SRV-ID
	 * 
	 */
	String getSv2022ProdSrvId();

	void setSv2022ProdSrvId(String sv2022ProdSrvId);

	/**
	 * Host Variable SV2023-PROC-MOD-CD
	 * 
	 */
	String getSv2023ProcModCd();

	void setSv2023ProcModCd(String sv2023ProcModCd);

	/**
	 * Host Variable SV2024-PROC-MOD-CD
	 * 
	 */
	String getSv2024ProcModCd();

	void setSv2024ProcModCd(String sv2024ProcModCd);

	/**
	 * Host Variable SV2025-PROC-MOD-CD
	 * 
	 */
	String getSv2025ProcModCd();

	void setSv2025ProcModCd(String sv2025ProcModCd);

	/**
	 * Host Variable SV2026-PROC-MOD-CD
	 * 
	 */
	String getSv2026ProcModCd();

	void setSv2026ProcModCd(String sv2026ProcModCd);

	/**
	 * Host Variable SV203-MNTRY-AM
	 * 
	 */
	AfDecimal getSv203MntryAm();

	void setSv203MntryAm(AfDecimal sv203MntryAm);

	/**
	 * Host Variable QNTY-CT
	 * 
	 */
	AfDecimal getQntyCt();

	void setQntyCt(AfDecimal qntyCt);
};
