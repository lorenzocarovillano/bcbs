/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.ws.enums.CsrItsClmTypCd;

/**Original name: CSR-ITS-INFORMATION<br>
 * Variable: CSR-ITS-INFORMATION from copybook NF07AREA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CsrItsInformation {

	//==== PROPERTIES ====
	//Original name: CSR-ITS-CLM-TYP-CD
	private CsrItsClmTypCd clmTypCd = new CsrItsClmTypCd();
	//Original name: CSR-ITS-CK
	private char ck = Types.SPACE_CHAR;
	//Original name: CSR-ITS-INS-PREFIX
	private String insPrefix = "";
	//Original name: CSR-ITS-INS-NUMBER
	private String insNumber = "";

	//==== METHODS ====
	public byte[] getItsInformationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, clmTypCd.getClmTypCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ck);
		position += Types.CHAR_SIZE;
		getInsIdBytes(buffer, position);
		return buffer;
	}

	public void initItsInformationSpaces() {
		clmTypCd.setClmTypCd(Types.SPACE_CHAR);
		ck = Types.SPACE_CHAR;
		initInsIdSpaces();
	}

	public void setCk(char ck) {
		this.ck = ck;
	}

	public char getCk() {
		return this.ck;
	}

	public void setInsIdFormatted(String data) {
		byte[] buffer = new byte[Len.INS_ID];
		MarshalByte.writeString(buffer, 1, data, Len.INS_ID);
		setInsIdBytes(buffer, 1);
	}

	public void setInsIdBytes(byte[] buffer, int offset) {
		int position = offset;
		insPrefix = MarshalByte.readString(buffer, position, Len.INS_PREFIX);
		position += Len.INS_PREFIX;
		insNumber = MarshalByte.readString(buffer, position, Len.INS_NUMBER);
	}

	public byte[] getInsIdBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, insPrefix, Len.INS_PREFIX);
		position += Len.INS_PREFIX;
		MarshalByte.writeString(buffer, position, insNumber, Len.INS_NUMBER);
		return buffer;
	}

	public void initInsIdSpaces() {
		insPrefix = "";
		insNumber = "";
	}

	public void setInsPrefix(String insPrefix) {
		this.insPrefix = Functions.subString(insPrefix, Len.INS_PREFIX);
	}

	public String getInsPrefix() {
		return this.insPrefix;
	}

	public void setInsNumber(String insNumber) {
		this.insNumber = Functions.subString(insNumber, Len.INS_NUMBER);
	}

	public String getInsNumber() {
		return this.insNumber;
	}

	public CsrItsClmTypCd getClmTypCd() {
		return clmTypCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int INS_PREFIX = 3;
		public static final int INS_NUMBER = 14;
		public static final int INS_ID = INS_PREFIX + INS_NUMBER;
		public static final int CK = 1;
		public static final int ITS_INFORMATION = CsrItsClmTypCd.Len.VALUE + CK + INS_ID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
