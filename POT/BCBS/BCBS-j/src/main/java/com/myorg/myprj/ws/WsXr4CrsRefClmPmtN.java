/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-XR4-CRS-REF-CLM-PMT-N<br>
 * Variable: WS-XR4-CRS-REF-CLM-PMT-N from program NF0735ML<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsXr4CrsRefClmPmtN {

	//==== PROPERTIES ====
	//Original name: WS-XR4-CRS-REF-CLM-PMT-P2
	private String p2 = DefaultValues.stringVal(Len.P2);
	//Original name: WS-XR4-CRS-REF-CLM-PMT-P1
	private String p1 = DefaultValues.stringVal(Len.P1);

	//==== METHODS ====
	public void setWsXr4CrsRefClmPmtId(String wsXr4CrsRefClmPmtId) {
		int position = 1;
		byte[] buffer = getWsXr4CrsRefClmPmtNBytes();
		MarshalByte.writeString(buffer, position, wsXr4CrsRefClmPmtId, Len.WS_XR4_CRS_REF_CLM_PMT_ID);
		setWsXr4CrsRefClmPmtNBytes(buffer);
	}

	/**Original name: WS-XR4-CRS-REF-CLM-PMT-ID<br>*/
	public String getWsXr4CrsRefClmPmtId() {
		int position = 1;
		return MarshalByte.readString(getWsXr4CrsRefClmPmtNBytes(), position, Len.WS_XR4_CRS_REF_CLM_PMT_ID);
	}

	public void setWsXr4CrsRefClmPmtNBytes(byte[] buffer) {
		setWsXr4CrsRefClmPmtNBytes(buffer, 1);
	}

	/**Original name: WS-XR4-CRS-REF-CLM-PMT-N<br>*/
	public byte[] getWsXr4CrsRefClmPmtNBytes() {
		byte[] buffer = new byte[Len.WS_XR4_CRS_REF_CLM_PMT_N];
		return getWsXr4CrsRefClmPmtNBytes(buffer, 1);
	}

	public void setWsXr4CrsRefClmPmtNBytes(byte[] buffer, int offset) {
		int position = offset;
		p2 = MarshalByte.readFixedString(buffer, position, Len.P2);
		position += Len.P2;
		p1 = MarshalByte.readFixedString(buffer, position, Len.P1);
	}

	public byte[] getWsXr4CrsRefClmPmtNBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, p2, Len.P2);
		position += Len.P2;
		MarshalByte.writeString(buffer, position, p1, Len.P1);
		return buffer;
	}

	public String getP2Formatted() {
		return this.p2;
	}

	public short getP1() {
		return NumericDisplay.asShort(this.p1);
	}

	public String getP1Formatted() {
		return this.p1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int P2 = 1;
		public static final int P1 = 1;
		public static final int WS_XR4_CRS_REF_CLM_PMT_N = P2 + P1;
		public static final int WS_XR4_CRS_REF_CLM_PMT_ID = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_XR4_CRS_REF_CLM_PMT_ID = 2;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
