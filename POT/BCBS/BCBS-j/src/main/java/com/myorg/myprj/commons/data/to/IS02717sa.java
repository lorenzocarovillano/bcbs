/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [S02717SA]
 * 
 */
public interface IS02717sa extends BaseSqlTo {

	/**
	 * Host Variable PROD-SRV-ID
	 * 
	 */
	String getProdSrvId();

	void setProdSrvId(String prodSrvId);

	/**
	 * Host Variable PROD-SRV-ID-QLF-CD
	 * 
	 */
	String getProdSrvIdQlfCd();

	void setProdSrvIdQlfCd(String prodSrvIdQlfCd);

	/**
	 * Host Variable SV1013-PROC-MOD-CD
	 * 
	 */
	String getSv1013ProcModCd();

	void setSv1013ProcModCd(String sv1013ProcModCd);

	/**
	 * Host Variable SV1014-PROC-MOD-CD
	 * 
	 */
	String getSv1014ProcModCd();

	void setSv1014ProcModCd(String sv1014ProcModCd);

	/**
	 * Host Variable SV1015-PROC-MOD-CD
	 * 
	 */
	String getSv1015ProcModCd();

	void setSv1015ProcModCd(String sv1015ProcModCd);

	/**
	 * Host Variable SV1016-PROC-MOD-CD
	 * 
	 */
	String getSv1016ProcModCd();

	void setSv1016ProcModCd(String sv1016ProcModCd);

	/**
	 * Host Variable SV102-MNTRY-AM
	 * 
	 */
	AfDecimal getSv102MntryAm();

	void setSv102MntryAm(AfDecimal sv102MntryAm);

	/**
	 * Host Variable QNTY-CT
	 * 
	 */
	AfDecimal getQntyCt();

	void setQntyCt(AfDecimal qntyCt);
};
