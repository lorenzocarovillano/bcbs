/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-RESTART-JOB-PARMS<br>
 * Variable: WS-RESTART-JOB-PARMS from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsRestartJobParms {

	//==== PROPERTIES ====
	//Original name: WS-RESTART-JOB-NAME
	private String restartJobName = "";
	//Original name: FILLER-WS-RESTART-JOB-PARMS
	private char flr1 = Types.SPACE_CHAR;
	//Original name: WS-RESTART-STEP-NAME
	private String restartStepName = "";
	//Original name: FILLER-WS-RESTART-JOB-PARMS-1
	private char flr2 = Types.SPACE_CHAR;
	//Original name: WS-RESTART-JOB-SEQ
	private String restartJobSeq = "000000000";
	//Original name: FILLER-WS-RESTART-JOB-PARMS-2
	private char flr3 = Types.SPACE_CHAR;
	//Original name: WS-TEST-FLAG
	private char testFlag = Types.SPACE_CHAR;
	//Original name: FILLER-WS-RESTART-JOB-PARMS-3
	private String flr4 = "";

	//==== METHODS ====
	public String getWsRestartJobParmsFormatted() {
		return MarshalByteExt.bufferToStr(getWsRestartJobParmsBytes());
	}

	public void setWsRestartJobParmsBytes(byte[] buffer) {
		setWsRestartJobParmsBytes(buffer, 1);
	}

	public byte[] getWsRestartJobParmsBytes() {
		byte[] buffer = new byte[Len.WS_RESTART_JOB_PARMS];
		return getWsRestartJobParmsBytes(buffer, 1);
	}

	public void setWsRestartJobParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		restartJobName = MarshalByte.readString(buffer, position, Len.RESTART_JOB_NAME);
		position += Len.RESTART_JOB_NAME;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		restartStepName = MarshalByte.readString(buffer, position, Len.RESTART_STEP_NAME);
		position += Len.RESTART_STEP_NAME;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		restartJobSeq = MarshalByte.readFixedString(buffer, position, Len.RESTART_JOB_SEQ);
		position += Len.RESTART_JOB_SEQ;
		flr3 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		testFlag = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		flr4 = MarshalByte.readString(buffer, position, Len.FLR4);
	}

	public byte[] getWsRestartJobParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, restartJobName, Len.RESTART_JOB_NAME);
		position += Len.RESTART_JOB_NAME;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, restartStepName, Len.RESTART_STEP_NAME);
		position += Len.RESTART_STEP_NAME;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, restartJobSeq, Len.RESTART_JOB_SEQ);
		position += Len.RESTART_JOB_SEQ;
		MarshalByte.writeChar(buffer, position, flr3);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, testFlag);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		return buffer;
	}

	public void setRestartJobName(String restartJobName) {
		this.restartJobName = Functions.subString(restartJobName, Len.RESTART_JOB_NAME);
	}

	public String getRestartJobName() {
		return this.restartJobName;
	}

	public String getRestartJobNameFormatted() {
		return Functions.padBlanks(getRestartJobName(), Len.RESTART_JOB_NAME);
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setRestartStepName(String restartStepName) {
		this.restartStepName = Functions.subString(restartStepName, Len.RESTART_STEP_NAME);
	}

	public String getRestartStepName() {
		return this.restartStepName;
	}

	public String getRestartStepNameFormatted() {
		return Functions.padBlanks(getRestartStepName(), Len.RESTART_STEP_NAME);
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public int getRestartJobSeq() {
		return NumericDisplay.asInt(this.restartJobSeq);
	}

	public String getRestartJobSeqFormatted() {
		return this.restartJobSeq;
	}

	public String getRestartJobSeqAsString() {
		return getRestartJobSeqFormatted();
	}

	public void setFlr3(char flr3) {
		this.flr3 = flr3;
	}

	public char getFlr3() {
		return this.flr3;
	}

	public void setTestFlag(char testFlag) {
		this.testFlag = testFlag;
	}

	public char getTestFlag() {
		return this.testFlag;
	}

	public void setFlr4(String flr4) {
		this.flr4 = Functions.subString(flr4, Len.FLR4);
	}

	public String getFlr4() {
		return this.flr4;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RESTART_JOB_SEQ = 9;
		public static final int RESTART_JOB_NAME = 8;
		public static final int RESTART_STEP_NAME = 8;
		public static final int FLR4 = 51;
		public static final int FLR1 = 1;
		public static final int TEST_FLAG = 1;
		public static final int WS_RESTART_JOB_PARMS = RESTART_JOB_NAME + RESTART_STEP_NAME + RESTART_JOB_SEQ + TEST_FLAG + 3 * FLR1 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
