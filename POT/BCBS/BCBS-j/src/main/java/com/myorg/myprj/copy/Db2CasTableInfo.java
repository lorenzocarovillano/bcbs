/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.commons.data.to.IS02800sa;

/**Original name: DB2-CAS-TABLE-INFO<br>
 * Variable: DB2-CAS-TABLE-INFO from copybook NF05CURS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Db2CasTableInfo implements IS02800sa {

	//==== PROPERTIES ====
	//Original name: DB2-CLM-ADJ-GRP-CD
	private String clmAdjGrpCd = DefaultValues.stringVal(Len.CLM_ADJ_GRP_CD);
	//Original name: DB2-CLM-ADJ-RSN-CD
	private String clmAdjRsnCd = DefaultValues.stringVal(Len.CLM_ADJ_RSN_CD);
	//Original name: DB2-MNTRY-AM
	private AfDecimal mntryAm = new AfDecimal(DefaultValues.DEC_VAL, 13, 2);

	//==== METHODS ====
	public void setCasTableInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		clmAdjGrpCd = MarshalByte.readString(buffer, position, Len.CLM_ADJ_GRP_CD);
		position += Len.CLM_ADJ_GRP_CD;
		clmAdjRsnCd = MarshalByte.readString(buffer, position, Len.CLM_ADJ_RSN_CD);
		position += Len.CLM_ADJ_RSN_CD;
		mntryAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.MNTRY_AM, Len.Fract.MNTRY_AM));
	}

	public byte[] getCasTableInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clmAdjGrpCd, Len.CLM_ADJ_GRP_CD);
		position += Len.CLM_ADJ_GRP_CD;
		MarshalByte.writeString(buffer, position, clmAdjRsnCd, Len.CLM_ADJ_RSN_CD);
		position += Len.CLM_ADJ_RSN_CD;
		MarshalByte.writeDecimalAsPacked(buffer, position, mntryAm.copy());
		return buffer;
	}

	@Override
	public void setClmAdjGrpCd(String clmAdjGrpCd) {
		this.clmAdjGrpCd = Functions.subString(clmAdjGrpCd, Len.CLM_ADJ_GRP_CD);
	}

	@Override
	public String getClmAdjGrpCd() {
		return this.clmAdjGrpCd;
	}

	@Override
	public void setClmAdjRsnCd(String clmAdjRsnCd) {
		this.clmAdjRsnCd = Functions.subString(clmAdjRsnCd, Len.CLM_ADJ_RSN_CD);
	}

	@Override
	public String getClmAdjRsnCd() {
		return this.clmAdjRsnCd;
	}

	@Override
	public void setMntryAm(AfDecimal mntryAm) {
		this.mntryAm.assign(mntryAm);
	}

	@Override
	public AfDecimal getMntryAm() {
		return this.mntryAm.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_ADJ_GRP_CD = 2;
		public static final int CLM_ADJ_RSN_CD = 5;
		public static final int MNTRY_AM = 7;
		public static final int CAS_TABLE_INFO = CLM_ADJ_GRP_CD + CLM_ADJ_RSN_CD + MNTRY_AM;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int MNTRY_AM = 11;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int MNTRY_AM = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
