/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

/**Original name: VOID-CAS-SUBS<br>
 * Variable: VOID-CAS-SUBS from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class VoidCasSubs {

	//==== PROPERTIES ====
	//Original name: CASCO-VSUB
	private short cascoVsub = ((short) 0);
	//Original name: CASPR-VSUB
	private short casprVsub = ((short) 0);
	//Original name: CASOA-VSUB
	private short casoaVsub = ((short) 0);
	//Original name: CASPI-VSUB
	private short caspiVsub = ((short) 0);

	//==== METHODS ====
	public void setCascoVsub(short cascoVsub) {
		this.cascoVsub = cascoVsub;
	}

	public short getCascoVsub() {
		return this.cascoVsub;
	}

	public void setCasprVsub(short casprVsub) {
		this.casprVsub = casprVsub;
	}

	public short getCasprVsub() {
		return this.casprVsub;
	}

	public void setCasoaVsub(short casoaVsub) {
		this.casoaVsub = casoaVsub;
	}

	public short getCasoaVsub() {
		return this.casoaVsub;
	}

	public void setCaspiVsub(short caspiVsub) {
		this.caspiVsub = caspiVsub;
	}

	public short getCaspiVsub() {
		return this.caspiVsub;
	}
}
