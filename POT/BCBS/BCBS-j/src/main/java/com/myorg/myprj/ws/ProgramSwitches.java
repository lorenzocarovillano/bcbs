/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: PROGRAM-SWITCHES<br>
 * Variable: PROGRAM-SWITCHES from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ProgramSwitches {

	//==== PROPERTIES ====
	//Original name: CALC-EOF-SW
	private short calcEofSw = ((short) 0);
	//Original name: INTG-EOF-SW
	private String intgEofSw = "0";
	public static final String INTG_EOF = "1";
	//Original name: FETCH-DONE-SW
	private String fetchDoneSw = "0";
	public static final String FETCH_DONE = "1";
	//Original name: FETCH-VOID-DONE-SW
	private String fetchVoidDoneSw = "0";
	public static final String FETCH_VOID_DONE = "1";
	//Original name: TRIGGER-EMPTY-SW
	private String triggerEmptySw = "0";
	public static final String TRIGGER_EMPTY = "1";
	//Original name: CLAIM-CURSOR-EOF-SW
	private String claimCursorEofSw = "0";
	public static final String CLAIM_CURSOR_EOF = "1";
	//Original name: VOID-EOF-SW
	private char voidEofSw = 'N';
	public static final char VOID_EOF = 'Y';
	//Original name: LAST-ROW-THIS-LINE-SW
	private char lastRowThisLineSw = 'N';
	public static final char LAST_ROW_THIS_LINE = 'Y';
	//Original name: BYPASS-2153-SW
	private char bypass2153Sw = 'N';
	public static final char BYPASS2153_NO = 'N';
	//Original name: BYPASS-2612-SW
	private char bypass2612Sw = 'N';
	public static final char BYPASS2612_NO = 'N';
	//Original name: WS-FIRST-2158-SW
	private char wsFirst2158Sw = 'N';
	public static final char NEXT2158 = 'Y';
	//Original name: WS-FIRST-2626-SW
	private char wsFirst2626Sw = 'N';
	public static final char NEXT2626 = 'Y';
	//Original name: WS-PROFESSIONAL-DATA-FOUND-SW
	private char wsProfessionalDataFoundSw = 'N';
	public static final char PROFESSIONAL_DATA_FOUND = 'Y';
	//Original name: WS-INSTITUTIONAL-DATA-FOUND-SW
	private char wsInstitutionalDataFoundSw = 'N';
	public static final char INSTITUTIONAL_DATA_FOUND = 'Y';
	//Original name: WS-HIST-BUNDLE-IND
	private char wsHistBundleInd = ' ';
	//Original name: WS-LI-ID-UNPACKED
	private String wsLiIdUnpacked = "000";
	//Original name: WS-EOB-AM
	private AfDecimal wsEobAm = new AfDecimal("0", 11, 2);
	//Original name: WS-SAVED-CAS
	private WsSavedCas wsSavedCas = new WsSavedCas();

	//==== METHODS ====
	public void setCalcEofSw(short calcEofSw) {
		this.calcEofSw = calcEofSw;
	}

	public short getCalcEofSw() {
		return this.calcEofSw;
	}

	public void setIntgEofSw(short intgEofSw) {
		this.intgEofSw = NumericDisplay.asString(intgEofSw, Len.INTG_EOF_SW);
	}

	public short getIntgEofSw() {
		return NumericDisplay.asShort(this.intgEofSw);
	}

	public String getIntgEofSwFormatted() {
		return this.intgEofSw;
	}

	public boolean isIntgEof() {
		return getIntgEofSwFormatted().equals(INTG_EOF);
	}

	public void setFetchDoneSw(short fetchDoneSw) {
		this.fetchDoneSw = NumericDisplay.asString(fetchDoneSw, Len.FETCH_DONE_SW);
	}

	public short getFetchDoneSw() {
		return NumericDisplay.asShort(this.fetchDoneSw);
	}

	public String getFetchDoneSwFormatted() {
		return this.fetchDoneSw;
	}

	public boolean isFetchDone() {
		return getFetchDoneSwFormatted().equals(FETCH_DONE);
	}

	public void setFetchVoidDoneSw(short fetchVoidDoneSw) {
		this.fetchVoidDoneSw = NumericDisplay.asString(fetchVoidDoneSw, Len.FETCH_VOID_DONE_SW);
	}

	public short getFetchVoidDoneSw() {
		return NumericDisplay.asShort(this.fetchVoidDoneSw);
	}

	public String getFetchVoidDoneSwFormatted() {
		return this.fetchVoidDoneSw;
	}

	public boolean isFetchVoidDone() {
		return getFetchVoidDoneSwFormatted().equals(FETCH_VOID_DONE);
	}

	public void setTriggerEmptySw(short triggerEmptySw) {
		this.triggerEmptySw = NumericDisplay.asString(triggerEmptySw, Len.TRIGGER_EMPTY_SW);
	}

	public short getTriggerEmptySw() {
		return NumericDisplay.asShort(this.triggerEmptySw);
	}

	public String getTriggerEmptySwFormatted() {
		return this.triggerEmptySw;
	}

	public boolean isTriggerEmpty() {
		return getTriggerEmptySwFormatted().equals(TRIGGER_EMPTY);
	}

	public void setClaimCursorEofSw(short claimCursorEofSw) {
		this.claimCursorEofSw = NumericDisplay.asString(claimCursorEofSw, Len.CLAIM_CURSOR_EOF_SW);
	}

	public short getClaimCursorEofSw() {
		return NumericDisplay.asShort(this.claimCursorEofSw);
	}

	public String getClaimCursorEofSwFormatted() {
		return this.claimCursorEofSw;
	}

	public boolean isClaimCursorEof() {
		return getClaimCursorEofSwFormatted().equals(CLAIM_CURSOR_EOF);
	}

	public void setVoidEofSw(char voidEofSw) {
		this.voidEofSw = voidEofSw;
	}

	public void setVoidEofSwFormatted(String voidEofSw) {
		setVoidEofSw(Functions.charAt(voidEofSw, Types.CHAR_SIZE));
	}

	public char getVoidEofSw() {
		return this.voidEofSw;
	}

	public boolean isVoidEof() {
		return voidEofSw == VOID_EOF;
	}

	public void setLastRowThisLineSw(char lastRowThisLineSw) {
		this.lastRowThisLineSw = lastRowThisLineSw;
	}

	public void setLastRowThisLineSwFormatted(String lastRowThisLineSw) {
		setLastRowThisLineSw(Functions.charAt(lastRowThisLineSw, Types.CHAR_SIZE));
	}

	public char getLastRowThisLineSw() {
		return this.lastRowThisLineSw;
	}

	public boolean isLastRowThisLine() {
		return lastRowThisLineSw == LAST_ROW_THIS_LINE;
	}

	public void setBypass2153Sw(char bypass2153Sw) {
		this.bypass2153Sw = bypass2153Sw;
	}

	public void setBypass2153SwFormatted(String bypass2153Sw) {
		setBypass2153Sw(Functions.charAt(bypass2153Sw, Types.CHAR_SIZE));
	}

	public char getBypass2153Sw() {
		return this.bypass2153Sw;
	}

	public boolean isBypass2153No() {
		return bypass2153Sw == BYPASS2153_NO;
	}

	public void setBypass2612Sw(char bypass2612Sw) {
		this.bypass2612Sw = bypass2612Sw;
	}

	public void setBypass2612SwFormatted(String bypass2612Sw) {
		setBypass2612Sw(Functions.charAt(bypass2612Sw, Types.CHAR_SIZE));
	}

	public char getBypass2612Sw() {
		return this.bypass2612Sw;
	}

	public boolean isBypass2612No() {
		return bypass2612Sw == BYPASS2612_NO;
	}

	public void setWsFirst2158Sw(char wsFirst2158Sw) {
		this.wsFirst2158Sw = wsFirst2158Sw;
	}

	public void setWsFirst2158SwFormatted(String wsFirst2158Sw) {
		setWsFirst2158Sw(Functions.charAt(wsFirst2158Sw, Types.CHAR_SIZE));
	}

	public char getWsFirst2158Sw() {
		return this.wsFirst2158Sw;
	}

	public void setWsFirst2626Sw(char wsFirst2626Sw) {
		this.wsFirst2626Sw = wsFirst2626Sw;
	}

	public void setWsFirst2626SwFormatted(String wsFirst2626Sw) {
		setWsFirst2626Sw(Functions.charAt(wsFirst2626Sw, Types.CHAR_SIZE));
	}

	public char getWsFirst2626Sw() {
		return this.wsFirst2626Sw;
	}

	public void setWsProfessionalDataFoundSw(char wsProfessionalDataFoundSw) {
		this.wsProfessionalDataFoundSw = wsProfessionalDataFoundSw;
	}

	public void setWsProfessionalDataFoundSwFormatted(String wsProfessionalDataFoundSw) {
		setWsProfessionalDataFoundSw(Functions.charAt(wsProfessionalDataFoundSw, Types.CHAR_SIZE));
	}

	public char getWsProfessionalDataFoundSw() {
		return this.wsProfessionalDataFoundSw;
	}

	public boolean isProfessionalDataFound() {
		return wsProfessionalDataFoundSw == PROFESSIONAL_DATA_FOUND;
	}

	public void setWsInstitutionalDataFoundSw(char wsInstitutionalDataFoundSw) {
		this.wsInstitutionalDataFoundSw = wsInstitutionalDataFoundSw;
	}

	public void setWsInstitutionalDataFoundSwFormatted(String wsInstitutionalDataFoundSw) {
		setWsInstitutionalDataFoundSw(Functions.charAt(wsInstitutionalDataFoundSw, Types.CHAR_SIZE));
	}

	public char getWsInstitutionalDataFoundSw() {
		return this.wsInstitutionalDataFoundSw;
	}

	public boolean isInstitutionalDataFound() {
		return wsInstitutionalDataFoundSw == INSTITUTIONAL_DATA_FOUND;
	}

	public void setWsHistBundleInd(char wsHistBundleInd) {
		this.wsHistBundleInd = wsHistBundleInd;
	}

	public char getWsHistBundleInd() {
		return this.wsHistBundleInd;
	}

	public void setWsLiIdUnpacked(short wsLiIdUnpacked) {
		this.wsLiIdUnpacked = NumericDisplay.asString(wsLiIdUnpacked, Len.WS_LI_ID_UNPACKED);
	}

	public short getWsLiIdUnpacked() {
		return NumericDisplay.asShort(this.wsLiIdUnpacked);
	}

	public String getWsLiIdUnpackedFormatted() {
		return this.wsLiIdUnpacked;
	}

	public String getWsLiIdUnpackedAsString() {
		return getWsLiIdUnpackedFormatted();
	}

	public void setWsEobAm(AfDecimal wsEobAm) {
		this.wsEobAm.assign(wsEobAm);
	}

	public AfDecimal getWsEobAm() {
		return this.wsEobAm.copy();
	}

	public WsSavedCas getWsSavedCas() {
		return wsSavedCas;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FETCH_DONE_SW = 1;
		public static final int CLAIM_CURSOR_EOF_SW = 1;
		public static final int FETCH_VOID_DONE_SW = 1;
		public static final int TRIGGER_EMPTY_SW = 1;
		public static final int INTG_EOF_SW = 1;
		public static final int CALC_EOF_SW = 1;
		public static final int WS_LI_ID_UNPACKED = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
