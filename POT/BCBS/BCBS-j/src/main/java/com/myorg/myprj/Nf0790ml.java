/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.DbService;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.io.file.FileAccessStatus;
import com.bphx.ctu.af.io.file.OpenMode;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.bphx.ctu.af.util.display.DisplayUtil;
import com.modernsystems.batch.DdCard;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.programs.Programs;
import com.myorg.myprj.commons.data.dao.S03085saDao;
import com.myorg.myprj.commons.data.dao.S03086saDao;
import com.myorg.myprj.copy.Db2StatErrMsg;
import com.myorg.myprj.copy.Sqlca;
import com.myorg.myprj.data.fao.JobParmsFileDAONf0790ml;
import com.myorg.myprj.data.fto.JobParmsFileTO;
import com.myorg.myprj.ws.Nf0790mlData;

/**Original name: NF0790ML<br>
 * <pre>DATE-WRITTEN. MARCH 2003.
 * AUTHOR.       CLAIMS TEAM.
 * DATE-COMPILED.
 * *****************************************************************
 *  THIS PROGRAM WILL DETERMINE WHETHER A JOB IS IN RESTART
 *  MODE OR NOT.
 *  THIS PROGRAM SHOULD BE THE VERY FIRST STEP IN THE JOB.
 *  IT SHOULD CHECK THE RESTART PARM FOR THE PROGRAM(S) THAT
 *  ACCESS DB2.
 *  IF A PROGRAM IS IN RESTART MODE, THEN THIS PROGRAM WILL
 *  ABEND.  THIS COULD HAVE BEEN CAUSED IF THE JOB HAD
 *  ORIGINALLY ABENDED (WHICH WOULD HAVE SET THE RESTART PARM
 *  TO YES) AND THEN RUN HISTORY WAS DELETED AND THE JOB WAS
 *  RESTARTED FROM THE TOP.
 *  IF THE RESTART WAS ALLOWED TO PROCEED WHEN THE RESTART PARM
 *  WAS SET TO YES, THEN SOME RECORDS COULD BE BY-PASSED
 *  INSTEAD OF BEING PULLED FOR PROCESSING.
 *  THIS PROGRAM IS ONLY INTENDED TO CALL ATTENTION TO THE FACT
 *  THAT THE RESTART PARM NEEDS TO BE RESET.
 * *****************************************************************
 *             M O D I F I C A T I O N    L O G                    *
 * *****************************************************************
 *  XX/XX/XXXX  PROJ/PAR XXXXXX
 * *****************************************************************
 * LCAROVILLANO SOURCE-COMPUTER.   IBM-370 WITH DEBUGGING MODE.
 * LCAROVILLANO SOURCE-COMPUTER.   IBM-370.
 * LCAROVILLANO OBJECT-COMPUTER.   IBM-370.</pre>*/
public class Nf0790ml extends BatchProgram {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private S03085saDao s03085saDao = new S03085saDao(dbAccessStatus);
	private S03086saDao s03086saDao = new S03086saDao(dbAccessStatus);
	public JobParmsFileTO jobParmsFileTO = new JobParmsFileTO();
	public JobParmsFileDAONf0790ml jobParmsFileDAONf0790ml = new JobParmsFileDAONf0790ml(new FileAccessStatus());
	//Original name: WORKING-STORAGE
	private Nf0790mlData ws = new Nf0790mlData();

	//==== METHODS ====
	/**Original name: 0000-START-PROCESS<br>
	 * <pre>----------------------------------------------------------------</pre>*/
	public long execute() {
		// COB_CODE: MOVE WS-PGM-NAME TO DB2-ERR-PROG.
		ws.getNudberdd().getDb2StatErrMsg().setErrProg(ws.getWsPgmName());
		// COB_CODE: DISPLAY '---------------------------------------------'.
		DisplayUtil.sysout.write("---------------------------------------------");
		// COB_CODE: DISPLAY '        ' WS-PGM-NAME ' BEGINNING   '.
		DisplayUtil.sysout.write("        ", ws.getWsPgmNameFormatted(), " BEGINNING   ");
		// COB_CODE: DISPLAY '---------------------------------------------'.
		DisplayUtil.sysout.write("---------------------------------------------");
		// COB_CODE: OPEN INPUT  JOB-PARMS-FILE.
		jobParmsFileDAONf0790ml.open(OpenMode.READ, "Nf0790ml");
		// COB_CODE: PERFORM 9800-READ-JOB-PARMS THRU 9800-EXIT.
		readJobParms();
		// COB_CODE: IF EOF-PARMS-FILE = 'Y'
		//               PERFORM 9999-CALL-ABEND THRU 9999-EXIT.
		if (ws.getEofParmsFile() == 'Y') {
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
			// COB_CODE: DISPLAY '      ' WS-PGM-NAME ' ABENDING'
			DisplayUtil.sysout.write("      ", ws.getWsPgmNameFormatted(), " ABENDING");
			// COB_CODE: DISPLAY '  JOB PARMS FILE IS EMPTY     '
			DisplayUtil.sysout.write("  JOB PARMS FILE IS EMPTY     ");
			// COB_CODE: DISPLAY '******************************'
			DisplayUtil.sysout.write("******************************");
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: PERFORM 9999-CALL-ABEND THRU 9999-EXIT.
			callAbend();
		}
		// COB_CODE: PERFORM 1000-PROCESS-JOB-PARMS THRU 1000-EXIT
		//               UNTIL EOF-PARMS-FILE = 'Y'.
		while (!(ws.getEofParmsFile() == 'Y')) {
			processJobParms();
		}
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '---------------------------------------------'.
		DisplayUtil.sysout.write("---------------------------------------------");
		// COB_CODE: DISPLAY '        '  WS-PGM-NAME ' ENDING      '.
		DisplayUtil.sysout.write("        ", ws.getWsPgmNameFormatted(), " ENDING      ");
		// COB_CODE: DISPLAY '---------------------------------------------'.
		DisplayUtil.sysout.write("---------------------------------------------");
		// COB_CODE: CLOSE JOB-PARMS-FILE.
		jobParmsFileDAONf0790ml.close();
		// COB_CODE: IF ERROR-FOUND-SW = 'Y'
		//               PERFORM 9999-CALL-ABEND.
		if (ws.getErrorFoundSw() == 'Y') {
			// COB_CODE: PERFORM 9999-CALL-ABEND.
			callAbend();
		}
		// COB_CODE: MOVE ZEROES TO RETURN-CODE.
		Session.setReturnCode(0);
		// COB_CODE: GOBACK.
		//last return statement was skipped
		return 0;
	}

	public static Nf0790ml getInstance() {
		return ((Nf0790ml) Programs.getInstance(Nf0790ml.class));
	}

	/**Original name: 1000-PROCESS-JOB-PARMS<br>
	 * <pre>----------------------------------------------------------------
	 *     DISPLAY '1000-PROCESS-JOB-PARMS'.</pre>*/
	private void processJobParms() {
		// COB_CODE: MOVE 'N' TO PARM-ERROR-FOUND-SW.
		ws.setParmErrorFoundSwFormatted("N");
		// COB_CODE: MOVE 'N' TO RESTART-DISPLAY-DONE-SW.
		ws.setRestartDisplayDoneSwFormatted("N");
		// COB_CODE: MOVE WS-JOB-SEQ TO WS-JOB-SEQ-PACKED.
		ws.setWsJobSeqPacked(jobParmsFileTO.getSeq());
		// COB_CODE: PERFORM 9010-SELECT-TA03085-CONTROL.
		selectTa03085Control();
		// COB_CODE: PERFORM 9020-SELECT-TA03086-RESTART.
		selectTa03086Restart();
		// COB_CODE: IF RSTRT-IN OF DCLS03085SA = 'Y'
		//                       'FROM THE BEGINNING OF THE JOB.'.
		if (ws.getDcls03085sa().getRstrtIn() == 'Y') {
			// COB_CODE: MOVE 'Y' TO PARM-ERROR-FOUND-SW
			ws.setParmErrorFoundSwFormatted("Y");
			// COB_CODE: PERFORM 9700-DISPLAY-RESTART-INFO
			rng9700DisplayRestartInfo();
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY '         !!!   ERROR   !!!           '
			DisplayUtil.sysout.write("         !!!   ERROR   !!!           ");
			// COB_CODE: DISPLAY '         !!!   ERROR   !!!           '
			DisplayUtil.sysout.write("         !!!   ERROR   !!!           ");
			// COB_CODE: DISPLAY '         !!!   ERROR   !!!           '
			DisplayUtil.sysout.write("         !!!   ERROR   !!!           ");
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY 'JOB NAME: ' WS-JOB-NAME
			//             '  JOB STEP: ' WS-JOB-STEP
			//             '  JOB SEQ: '  WS-JOB-SEQ
			DisplayUtil.sysout.write(new String[] { "JOB NAME: ", jobParmsFileTO.getNameFormatted(), "  JOB STEP: ",
					jobParmsFileTO.getStepFormatted(), "  JOB SEQ: ", jobParmsFileTO.getSeqAsString() });
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY 'RESTART INDICATOR IS SET TO YES.'
			DisplayUtil.sysout.write("RESTART INDICATOR IS SET TO YES.");
			// COB_CODE: DISPLAY 'IT MUST BE RESET TO AN "N" BEFORE STARTING '
			//                   'FROM THE BEGINNING OF THE JOB.'.
			DisplayUtil.sysout.write("IT MUST BE RESET TO AN \"N\" BEFORE STARTING ", "FROM THE BEGINNING OF THE JOB.");
		}
		// COB_CODE: IF APPL-TRM-CD OF DCLS03086SA = ' '
		//               NEXT SENTENCE
		//           ELSE
		//                       'FROM THE BEGINNING OF THE JOB.'.
		if (!(ws.getDcls03086sa().getApplTrmCd() == ' ')) {
			// COB_CODE: MOVE 'Y' TO PARM-ERROR-FOUND-SW
			ws.setParmErrorFoundSwFormatted("Y");
			// COB_CODE: PERFORM 9700-DISPLAY-RESTART-INFO
			rng9700DisplayRestartInfo();
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY '         !!!   ERROR   !!!           '
			DisplayUtil.sysout.write("         !!!   ERROR   !!!           ");
			// COB_CODE: DISPLAY '         !!!   ERROR   !!!           '
			DisplayUtil.sysout.write("         !!!   ERROR   !!!           ");
			// COB_CODE: DISPLAY '         !!!   ERROR   !!!           '
			DisplayUtil.sysout.write("         !!!   ERROR   !!!           ");
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY 'JOB NAME: ' WS-JOB-NAME
			//             '  JOB STEP: ' WS-JOB-STEP
			//             '  JOB SEQ: '  WS-JOB-SEQ
			DisplayUtil.sysout.write(new String[] { "JOB NAME: ", jobParmsFileTO.getNameFormatted(), "  JOB STEP: ",
					jobParmsFileTO.getStepFormatted(), "  JOB SEQ: ", jobParmsFileTO.getSeqAsString() });
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY 'APPLICATION TERMINATION CODE WAS SET.'
			DisplayUtil.sysout.write("APPLICATION TERMINATION CODE WAS SET.");
			// COB_CODE: DISPLAY 'IT MUST BE RESET TO SPACES BEFORE STARTING '
			//                   'FROM THE BEGINNING OF THE JOB.'.
			DisplayUtil.sysout.write("IT MUST BE RESET TO SPACES BEFORE STARTING ", "FROM THE BEGINNING OF THE JOB.");
		}
		// COB_CODE: IF  RSTRT-IN OF DCLS03085SA = 'N'
		//           AND APPL-TRM-CD OF DCLS03086SA = ' '
		//                       '-----------'.
		if (ws.getDcls03085sa().getRstrtIn() == 'N' && ws.getDcls03086sa().getApplTrmCd() == ' ') {
			// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY '-----------------------------------------'
			//                   '-----------'
			DisplayUtil.sysout.write("-----------------------------------------", "-----------");
			// COB_CODE: DISPLAY 'JOB NAME: ' WS-JOB-NAME
			//             '  JOB STEP: ' WS-JOB-STEP
			//             '  JOB SEQ: '  WS-JOB-SEQ
			DisplayUtil.sysout.write(new String[] { "JOB NAME: ", jobParmsFileTO.getNameFormatted(), "  JOB STEP: ",
					jobParmsFileTO.getStepFormatted(), "  JOB SEQ: ", jobParmsFileTO.getSeqAsString() });
			// COB_CODE: DISPLAY '                RESTART PARMS ARE OK'
			DisplayUtil.sysout.write("                RESTART PARMS ARE OK");
			// COB_CODE: DISPLAY '          JOB CAN BE STARTED FROM THE TOP'
			DisplayUtil.sysout.write("          JOB CAN BE STARTED FROM THE TOP");
			// COB_CODE: DISPLAY '-----------------------------------------'
			//                   '-----------'.
			DisplayUtil.sysout.write("-----------------------------------------", "-----------");
		}
		// COB_CODE: IF PARM-ERROR-FOUND-SW = 'Y'
		//               MOVE 'Y' TO ERROR-FOUND-SW.
		if (ws.getParmErrorFoundSw() == 'Y') {
			// COB_CODE: MOVE 'Y' TO ERROR-FOUND-SW.
			ws.setErrorFoundSwFormatted("Y");
		}
		// COB_CODE: PERFORM 9800-READ-JOB-PARMS THRU 9800-EXIT.
		readJobParms();
	}

	/**Original name: 9010-SELECT-TA03085-CONTROL<br>
	 * <pre>----------------------------------------------------------------
	 *     DISPLAY '9010-SELECT-TA03085-CONTROL'.</pre>*/
	private void selectTa03085Control() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//              SELECT JOB_NM
		//                  ,  JBSTP_NM
		//                  ,  JBSTP_SEQ_ID
		//                  ,  RSTRT_IN
		//                  ,  JBSTP_STRT_TS
		//                  ,  JBSTP_END_TS
		//                  ,  JBSTP_RUN_CT
		//                  ,  INFO_CHG_ID
		//                  ,  INFO_CHG_TS
		//              INTO  :DCLS03085SA.JOB-NM
		//                  , :DCLS03085SA.JBSTP-NM
		//                  , :DCLS03085SA.JBSTP-SEQ-ID
		//                  , :DCLS03085SA.RSTRT-IN
		//                  , :DCLS03085SA.JBSTP-STRT-TS
		//                  , :DCLS03085SA.JBSTP-END-TS
		//                  , :DCLS03085SA.JBSTP-RUN-CT
		//                  , :DCLS03085SA.INFO-CHG-ID
		//                  , :DCLS03085SA.INFO-CHG-TS
		//              FROM S03085SA
		//                  WHERE JOB_NM       = :WS-JOB-NAME
		//                    AND JBSTP_NM     = :WS-JOB-STEP
		//                    AND JBSTP_SEQ_ID = :WS-JOB-SEQ-PACKED
		//           END-EXEC.
		s03085saDao.selectRec1(jobParmsFileTO.getName(), jobParmsFileTO.getStep(), ws.getWsJobSeqPacked(), ws.getDcls03085sa());
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9999-CALL-ABEND THRU 9999-EXIT
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: DISPLAY '     ' WS-PGM-NAME ' ABENDING  '
			DisplayUtil.sysout.write("     ", ws.getWsPgmNameFormatted(), " ABENDING  ");
			// COB_CODE: DISPLAY 'ERROR SELECTING S03085SA TABLE '
			DisplayUtil.sysout.write("ERROR SELECTING S03085SA TABLE ");
			// COB_CODE: DISPLAY 'FOR RESTART CONTROL INFORMATION'
			DisplayUtil.sysout.write("FOR RESTART CONTROL INFORMATION");
			// COB_CODE: DISPLAY '     '
			//                WS-JOB-NAME ' ' WS-JOB-STEP ' ' WS-JOB-SEQ
			DisplayUtil.sysout.write(new String[] { "     ", jobParmsFileTO.getNameFormatted(), " ", jobParmsFileTO.getStepFormatted(), " ",
					jobParmsFileTO.getSeqAsString() });
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: PERFORM 9998-DISPLAY-SQLCODE THRU 9998-EXIT
			displaySqlcode();
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '&'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Db2StatErrMsg.Len.ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "&"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "&"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "&"));
			ws.getNudberdd().getDb2StatErrMsg().setErrKey(concatUtil.replaceInString(ws.getNudberdd().getDb2StatErrMsg().getErrKeyFormatted()));
			// COB_CODE: MOVE 'S03085SA'               TO DB2-ERR-TABLE
			ws.getNudberdd().getDb2StatErrMsg().setErrTable("S03085SA");
			// COB_CODE: MOVE 'SELECT S03085SA'        TO DB2-ERR-LAST-CALL
			ws.getNudberdd().getDb2StatErrMsg().setErrLastCall("SELECT S03085SA");
			// COB_CODE: MOVE '9010-SELECT-TA03085-CONTROL' TO DB2-ERR-PARA
			ws.getNudberdd().getDb2StatErrMsg().setErrPara("9010-SELECT-TA03085-CONTROL");
			// COB_CODE: PERFORM DB2-STAT-ERR THRU DB2-STAT-ERR-EXIT
			db2StatErr();
			// COB_CODE: PERFORM 9999-CALL-ABEND THRU 9999-EXIT
			callAbend();
			break;
		}
	}

	/**Original name: 9020-SELECT-TA03086-RESTART<br>
	 * <pre>----------------------------------------------------------------
	 *     DISPLAY '9020-SELECT-TA03086-RESTART'.</pre>*/
	private void selectTa03086Restart() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//              SELECT JOB_NM
		//                  ,  JBSTP_NM
		//                  ,  JBSTP_SEQ_ID
		//                  ,  INFO_CHG_TS
		//                  ,  APPL_TRM_CD
		//                  ,  RSTRT_HIST_IN
		//                  ,  ROW_FREQ_CMT_CT
		//                  ,  CMT_TM_INT_TM
		//                  ,  CMT_CT
		//                  ,  CMT_TS
		//                  ,  RSTRT1_TX
		//                  ,  RSTRT2_TX
		//                  ,  INFO_CHG_ID
		//              INTO  :DCLS03086SA.JOB-NM
		//                  , :DCLS03086SA.JBSTP-NM
		//                  , :DCLS03086SA.JBSTP-SEQ-ID
		//                  , :DCLS03086SA.INFO-CHG-TS
		//                  , :DCLS03086SA.APPL-TRM-CD
		//                  , :DCLS03086SA.RSTRT-HIST-IN
		//                  , :DCLS03086SA.ROW-FREQ-CMT-CT
		//                  , :DCLS03086SA.CMT-TM-INT-TM
		//                  , :DCLS03086SA.CMT-CT
		//                  , :DCLS03086SA.CMT-TS
		//                  , :DCLS03086SA.RSTRT1-TX
		//                  , :DCLS03086SA.RSTRT2-TX
		//                  , :DCLS03086SA.INFO-CHG-ID
		//              FROM S03086SA
		//                  WHERE JOB_NM       = :WS-JOB-NAME
		//                    AND JBSTP_NM     = :WS-JOB-STEP
		//                    AND JBSTP_SEQ_ID = :WS-JOB-SEQ-PACKED
		//           END-EXEC.
		s03086saDao.selectRec2(jobParmsFileTO.getName(), jobParmsFileTO.getStep(), ws.getWsJobSeqPacked(), ws.getDcls03086sa());
		// COB_CODE: EVALUATE SQLCODE
		//               WHEN 0
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9999-CALL-ABEND THRU 9999-EXIT
		//           END-EVALUATE.
		switch (sqlca.getSqlcode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: DISPLAY ' '
			DisplayUtil.sysout.write(" ");
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: DISPLAY '     ' WS-PGM-NAME ' ABENDING  '
			DisplayUtil.sysout.write("     ", ws.getWsPgmNameFormatted(), " ABENDING  ");
			// COB_CODE: DISPLAY 'ERROR SELECTING S03086SA TABLE '
			DisplayUtil.sysout.write("ERROR SELECTING S03086SA TABLE ");
			// COB_CODE: DISPLAY 'FOR RESTART INFORMATION.       '
			DisplayUtil.sysout.write("FOR RESTART INFORMATION.       ");
			// COB_CODE: DISPLAY '     '
			//                WS-JOB-NAME ' ' WS-JOB-STEP ' ' WS-JOB-SEQ
			DisplayUtil.sysout.write(new String[] { "     ", jobParmsFileTO.getNameFormatted(), " ", jobParmsFileTO.getStepFormatted(), " ",
					jobParmsFileTO.getSeqAsString() });
			// COB_CODE: DISPLAY '*******************************'
			DisplayUtil.sysout.write("*******************************");
			// COB_CODE: PERFORM 9998-DISPLAY-SQLCODE THRU 9998-EXIT
			displaySqlcode();
			// COB_CODE: STRING WS-JOB-NAME, ' '
			//                , WS-JOB-STEP, ' '
			//                , WS-JOB-SEQ
			//                DELIMITED BY '&'    INTO DB2-ERR-KEY
			concatUtil = ConcatUtil.buildString(Db2StatErrMsg.Len.ERR_KEY, Functions.substringBefore(jobParmsFileTO.getNameFormatted(), "&"), " ",
					Functions.substringBefore(jobParmsFileTO.getStepFormatted(), "&"), " ",
					Functions.substringBefore(jobParmsFileTO.getSeqAsString(), "&"));
			ws.getNudberdd().getDb2StatErrMsg().setErrKey(concatUtil.replaceInString(ws.getNudberdd().getDb2StatErrMsg().getErrKeyFormatted()));
			// COB_CODE: MOVE 'S03086SA'                TO DB2-ERR-TABLE
			ws.getNudberdd().getDb2StatErrMsg().setErrTable("S03086SA");
			// COB_CODE: MOVE 'SELECT S03086SA'         TO DB2-ERR-LAST-CALL
			ws.getNudberdd().getDb2StatErrMsg().setErrLastCall("SELECT S03086SA");
			// COB_CODE: MOVE '9020-SELECT-TA03086-RESTART' TO DB2-ERR-PARA
			ws.getNudberdd().getDb2StatErrMsg().setErrPara("9020-SELECT-TA03086-RESTART");
			// COB_CODE: PERFORM DB2-STAT-ERR THRU DB2-STAT-ERR-EXIT
			db2StatErr();
			// COB_CODE: PERFORM 9999-CALL-ABEND THRU 9999-EXIT
			callAbend();
			break;
		}
	}

	/**Original name: 9700-DISPLAY-RESTART-INFO<br>
	 * <pre>----------------------------------------------------------------
	 *     DISPLAY '9700-DISPLAY-RESTART-INFO'.</pre>*/
	private String displayRestartInfo() {
		// COB_CODE: IF RESTART-DISPLAY-DONE-SW = 'Y'
		//               GO TO 9700-EXIT.
		if (ws.getRestartDisplayDoneSw() == 'Y') {
			// COB_CODE: GO TO 9700-EXIT.
			return "9700-EXIT";
		}
		// COB_CODE: DISPLAY  ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY  'RESTART CONTROL INFORMATION FROM TA03085: '.
		DisplayUtil.sysout.write("RESTART CONTROL INFORMATION FROM TA03085: ");
		// COB_CODE: DISPLAY  '  JOB-NM          ' JOB-NM        OF DCLS03085SA.
		DisplayUtil.sysout.write("  JOB-NM          ", ws.getDcls03085sa().getJobNmFormatted());
		// COB_CODE: DISPLAY  '  JBSTP-NM        ' JBSTP-NM      OF DCLS03085SA.
		DisplayUtil.sysout.write("  JBSTP-NM        ", ws.getDcls03085sa().getJbstpNmFormatted());
		// COB_CODE: DISPLAY  '  JBSTP-SEQ-ID    ' JBSTP-SEQ-ID  OF DCLS03085SA.
		DisplayUtil.sysout.write("  JBSTP-SEQ-ID    ", ws.getDcls03085sa().getJbstpSeqIdAsString());
		// COB_CODE: IF RSTRT-IN OF DCLS03085SA = 'Y'
		//                  '                            <<<<< NOTE <<<<<<<<<<<'
		//           ELSE
		//                  '  RSTRT-IN        ' RSTRT-IN      OF DCLS03085SA.
		if (ws.getDcls03085sa().getRstrtIn() == 'Y') {
			// COB_CODE: DISPLAY
			//              '  RSTRT-IN        ' RSTRT-IN      OF DCLS03085SA
			//              '                            <<<<< NOTE <<<<<<<<<<<'
			DisplayUtil.sysout.write("  RSTRT-IN        ", String.valueOf(ws.getDcls03085sa().getRstrtIn()),
					"                            <<<<< NOTE <<<<<<<<<<<");
		} else {
			// COB_CODE: DISPLAY
			//              '  RSTRT-IN        ' RSTRT-IN      OF DCLS03085SA.
			DisplayUtil.sysout.write("  RSTRT-IN        ", String.valueOf(ws.getDcls03085sa().getRstrtIn()));
		}
		// COB_CODE: DISPLAY  '  JBSTP-STRT-TS   ' JBSTP-STRT-TS OF DCLS03085SA.
		DisplayUtil.sysout.write("  JBSTP-STRT-TS   ", ws.getDcls03085sa().getJbstpStrtTsFormatted());
		// COB_CODE: DISPLAY  '  JBSTP-END-TS    ' JBSTP-END-TS  OF DCLS03085SA.
		DisplayUtil.sysout.write("  JBSTP-END-TS    ", ws.getDcls03085sa().getJbstpEndTsFormatted());
		// COB_CODE: DISPLAY  '  JBSTP-RUN-CT    ' JBSTP-RUN-CT  OF DCLS03085SA.
		DisplayUtil.sysout.write("  JBSTP-RUN-CT    ", ws.getDcls03085sa().getJbstpRunCtAsString());
		// COB_CODE: DISPLAY  '  INFO-CHG-ID     ' INFO-CHG-ID   OF DCLS03085SA.
		DisplayUtil.sysout.write("  INFO-CHG-ID     ", ws.getDcls03085sa().getInfoChgIdFormatted());
		// COB_CODE: DISPLAY  '  INFO-CHG-TS     ' INFO-CHG-TS   OF DCLS03085SA.
		DisplayUtil.sysout.write("  INFO-CHG-TS     ", ws.getDcls03085sa().getInfoChgTsFormatted());
		// COB_CODE: DISPLAY  ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY  'RESTART INFORMATION FROM TA03086: '.
		DisplayUtil.sysout.write("RESTART INFORMATION FROM TA03086: ");
		// COB_CODE: DISPLAY  '  JOB-NM          ' JOB-NM          OF DCLS03086SA.
		DisplayUtil.sysout.write("  JOB-NM          ", ws.getDcls03086sa().getJobNmFormatted());
		// COB_CODE: DISPLAY  '  JOBSTP-NM       ' JBSTP-NM        OF DCLS03086SA.
		DisplayUtil.sysout.write("  JOBSTP-NM       ", ws.getDcls03086sa().getJbstpNmFormatted());
		// COB_CODE: DISPLAY  '  JBSTP-SEQ-ID    ' JBSTP-SEQ-ID    OF DCLS03086SA.
		DisplayUtil.sysout.write("  JBSTP-SEQ-ID    ", ws.getDcls03086sa().getJbstpSeqIdAsString());
		// COB_CODE: DISPLAY  '  INFO-CHG-TS     ' INFO-CHG-TS     OF DCLS03086SA.
		DisplayUtil.sysout.write("  INFO-CHG-TS     ", ws.getDcls03086sa().getInfoChgTsFormatted());
		// COB_CODE: IF APPL-TRM-CD OF DCLS03086SA = SPACES
		//                  '  APPL-TRM-CD     ' APPL-TRM-CD     OF DCLS03086SA
		//           ELSE
		//                  '                            <<<<< NOTE <<<<<<<<<<<'.
		if (Conditions.eq(ws.getDcls03086sa().getApplTrmCd(), Types.SPACE_CHAR)) {
			// COB_CODE: DISPLAY
			//              '  APPL-TRM-CD     ' APPL-TRM-CD     OF DCLS03086SA
			DisplayUtil.sysout.write("  APPL-TRM-CD     ", String.valueOf(ws.getDcls03086sa().getApplTrmCd()));
		} else {
			// COB_CODE: DISPLAY
			//              '  APPL-TRM-CD     ' APPL-TRM-CD     OF DCLS03086SA
			//              '                            <<<<< NOTE <<<<<<<<<<<'.
			DisplayUtil.sysout.write("  APPL-TRM-CD     ", String.valueOf(ws.getDcls03086sa().getApplTrmCd()),
					"                            <<<<< NOTE <<<<<<<<<<<");
		}
		// COB_CODE: DISPLAY  '  RSTRT-HIST-IN   ' RSTRT-HIST-IN   OF DCLS03086SA.
		DisplayUtil.sysout.write("  RSTRT-HIST-IN   ", String.valueOf(ws.getDcls03086sa().getRstrtHistIn()));
		// COB_CODE: DISPLAY  '  ROW-FREQ-CMT-CT ' ROW-FREQ-CMT-CT OF DCLS03086SA.
		DisplayUtil.sysout.write("  ROW-FREQ-CMT-CT ", ws.getDcls03086sa().getRowFreqCmtCtAsString());
		// COB_CODE: DISPLAY  '  CMT-TM-INT-TM   ' CMT-TM-INT-TM   OF DCLS03086SA.
		DisplayUtil.sysout.write("  CMT-TM-INT-TM   ", ws.getDcls03086sa().getCmtTmIntTmFormatted());
		// COB_CODE: DISPLAY  '  CMT-CT          ' CMT-CT          OF DCLS03086SA.
		DisplayUtil.sysout.write("  CMT-CT          ", ws.getDcls03086sa().getCmtCtAsString());
		// COB_CODE: DISPLAY  '  CMT-TS          ' CMT-TS          OF DCLS03086SA.
		DisplayUtil.sysout.write("  CMT-TS          ", ws.getDcls03086sa().getCmtTsFormatted());
		// COB_CODE: DISPLAY  '  INFO-CHG-ID     ' INFO-CHG-ID     OF DCLS03086SA.
		DisplayUtil.sysout.write("  INFO-CHG-ID     ", ws.getDcls03086sa().getInfoChgIdFormatted());
		// COB_CODE: DISPLAY  '  RSTRT1-TX       ' RSTRT1-TX       OF DCLS03086SA.
		DisplayUtil.sysout.write("  RSTRT1-TX       ", ws.getDcls03086sa().getRstrt1TxFormatted());
		// COB_CODE: DISPLAY  '  RSTRT2-TX       ' RSTRT2-TX       OF DCLS03086SA.
		DisplayUtil.sysout.write("  RSTRT2-TX       ", ws.getDcls03086sa().getRstrt2TxFormatted());
		// COB_CODE: MOVE 'Y' TO RESTART-DISPLAY-DONE-SW.
		ws.setRestartDisplayDoneSwFormatted("Y");
		return "";
	}

	/**Original name: 9800-READ-JOB-PARMS<br>
	 * <pre>----------------------------------------------------------------
	 *     DISPLAY '9800-READ-JOB-PARMS'.</pre>*/
	private void readJobParms() {
		// COB_CODE: READ JOB-PARMS-FILE INTO WS-JOB-PARMS
		//               AT END
		//                  MOVE 'Y' TO EOF-PARMS-FILE.
		jobParmsFileTO = jobParmsFileDAONf0790ml.read(jobParmsFileTO);
		if (jobParmsFileDAONf0790ml.getFileStatus().isEOF()) {
			// COB_CODE: MOVE 'Y' TO EOF-PARMS-FILE.
			ws.setEofParmsFileFormatted("Y");
		}
	}

	/**Original name: 9998-DISPLAY-SQLCODE<br>
	 * <pre>----------------------------------------------------------------</pre>*/
	private void displaySqlcode() {
		// COB_CODE: IF SQLCODE < 0
		//               MOVE '-' TO WS-SQLCODE-SIGN
		//           ELSE
		//               MOVE '+' TO WS-SQLCODE-SIGN.
		if (sqlca.getSqlcode() < 0) {
			// COB_CODE: MOVE '-' TO WS-SQLCODE-SIGN
			ws.setWsSqlcodeSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+' TO WS-SQLCODE-SIGN.
			ws.setWsSqlcodeSignFormatted("+");
		}
		// COB_CODE: MOVE SQLCODE TO WS-SQLCODE.
		ws.setWsSqlcode(TruncAbs.toInt(sqlca.getSqlcode(), 9));
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
		// COB_CODE: DISPLAY '***>>> SQLCODE = ' WS-SQLCODE-UNPACKED
		//                   '    SQLSTATE = ' SQLSTATE.
		DisplayUtil.sysout.write("***>>> SQLCODE = ", ws.getWsSqlcodeUnpackedFormatted(), "    SQLSTATE = ", sqlca.getSqlstateFormatted());
		// COB_CODE: DISPLAY '***>>> SQLERRMC = ' SQLERRMC.
		DisplayUtil.sysout.write("***>>> SQLERRMC = ", sqlca.getSqlerrmcFormatted());
		// COB_CODE: DISPLAY '***>>> NOTE: SQLERRMC = 00C90088 IS A DEADLOCK'.
		DisplayUtil.sysout.write("***>>> NOTE: SQLERRMC = 00C90088 IS A DEADLOCK");
		// COB_CODE: DISPLAY '***>>>       SQLERRMC = 00C9008E IS A TIMEOUT'.
		DisplayUtil.sysout.write("***>>>       SQLERRMC = 00C9008E IS A TIMEOUT");
		// COB_CODE: DISPLAY ' '.
		DisplayUtil.sysout.write(" ");
	}

	/**Original name: 9999-CALL-ABEND<br>
	 * <pre>----------------------------------------------------------------</pre>*/
	private void callAbend() {
		// COB_CODE: EXEC SQL
		//              ROLLBACK
		//           END-EXEC.
		DbService.getCurrent().rollback(dbAccessStatus);
		// COB_CODE: CALL 'ABEND'.
		DynamicCall.invoke("ABEND");
	}

	/**Original name: DB2-STAT-ERR<br>
	 * <pre>----------------------------------------------------------------
	 *  DATA BASE ERROR PARAGRAPH
	 * ----------------------------------------------------------------
	 *   COPYLIB MEMBER NUDBERPB IS USED IN THE PROCEDURE DIVISION OF
	 *   DB2 PROGRAMS RUN AS BMPS, BATCH OR TSO FOREGROUND.  IT FORMATS
	 *   COPYLIB MEMBER NUDBERDD WHEN A DB2 STATUS ERROR OCCURS.
	 * *****************************************************************</pre>*/
	private void db2StatErr() {
		GenericParam db2MsgLg = null;
		// COB_CODE: CALL 'DSNTIAR' USING SQLCA,
		//                                DB2-ERR-MSG-DATA,
		//                                DB2-MSG-LG.
		db2MsgLg = new GenericParam(MarshalByteExt.binShortToBuffer(ws.getNudberdd().getDb2StatErrMsg().getMsgLg()));
		DynamicCall.invoke("DSNTIAR", sqlca, ws.getNudberdd().getDb2StatErrMsg().getErrMsgData(), db2MsgLg);
		ws.getNudberdd().getDb2StatErrMsg().setMsgLgFromBuffer(db2MsgLg.getByteData());
		// COB_CODE: DISPLAY '*************************************************'.
		DisplayUtil.sysout.write("*************************************************");
		// COB_CODE: DISPLAY '            UNSUCCESSFUL DATABASE CALL           '.
		DisplayUtil.sysout.write("            UNSUCCESSFUL DATABASE CALL           ");
		// COB_CODE: DISPLAY '          PROGRAM TERMINATION IN PROGRESS.       '.
		DisplayUtil.sysout.write("          PROGRAM TERMINATION IN PROGRESS.       ");
		// COB_CODE: DISPLAY '                                                 '.
		DisplayUtil.sysout.write("                                                 ");
		// COB_CODE: DISPLAY       'PROGRAM NUMBER:       ' DB2-ERR-PROG.
		DisplayUtil.sysout.write("PROGRAM NUMBER:       ", ws.getNudberdd().getDb2StatErrMsg().getErrProgFormatted());
		// COB_CODE: DISPLAY       'PARAGRAPH EXECUTING:  ' DB2-ERR-PARA.
		DisplayUtil.sysout.write("PARAGRAPH EXECUTING:  ", ws.getNudberdd().getDb2StatErrMsg().getErrParaFormatted());
		// COB_CODE: DISPLAY       'TYPE OF CALL:         ' DB2-ERR-LAST-CALL.
		DisplayUtil.sysout.write("TYPE OF CALL:         ", ws.getNudberdd().getDb2StatErrMsg().getErrLastCallFormatted());
		// COB_CODE: DISPLAY       'TABLE NAME:           ' DB2-ERR-TABLE.
		DisplayUtil.sysout.write("TABLE NAME:           ", ws.getNudberdd().getDb2StatErrMsg().getErrTableFormatted());
		// COB_CODE: DISPLAY       'KEY DATA:             ' DB2-ERR-KEY.
		DisplayUtil.sysout.write("KEY DATA:             ", ws.getNudberdd().getDb2StatErrMsg().getErrKeyFormatted());
		// COB_CODE: DISPLAY       'ERROR MSG:            ' DB2-ERR-MSG(1).
		DisplayUtil.sysout.write("ERROR MSG:            ", ws.getNudberdd().getDb2StatErrMsg().getErrMsgData().getGFormatted(1));
		// COB_CODE: DISPLAY       '                      ' DB2-ERR-MSG(2).
		DisplayUtil.sysout.write("                      ", ws.getNudberdd().getDb2StatErrMsg().getErrMsgData().getGFormatted(2));
		// COB_CODE: DISPLAY       '                      ' DB2-ERR-MSG(3).
		DisplayUtil.sysout.write("                      ", ws.getNudberdd().getDb2StatErrMsg().getErrMsgData().getGFormatted(3));
		// COB_CODE: DISPLAY       '                      ' DB2-ERR-MSG(4).
		DisplayUtil.sysout.write("                      ", ws.getNudberdd().getDb2StatErrMsg().getErrMsgData().getGFormatted(4));
		// COB_CODE: DISPLAY       '                      ' DB2-ERR-MSG(5).
		DisplayUtil.sysout.write("                      ", ws.getNudberdd().getDb2StatErrMsg().getErrMsgData().getGFormatted(5));
		// COB_CODE: DISPLAY       '                      ' DB2-ERR-MSG(6).
		DisplayUtil.sysout.write("                      ", ws.getNudberdd().getDb2StatErrMsg().getErrMsgData().getGFormatted(6));
		// COB_CODE: DISPLAY       '                      ' DB2-ERR-MSG(7).
		DisplayUtil.sysout.write("                      ", ws.getNudberdd().getDb2StatErrMsg().getErrMsgData().getGFormatted(7));
		// COB_CODE: DISPLAY       '                      ' DB2-ERR-MSG(8).
		DisplayUtil.sysout.write("                      ", ws.getNudberdd().getDb2StatErrMsg().getErrMsgData().getGFormatted(8));
		// COB_CODE: DISPLAY       '                      ' DB2-ERR-MSG(9).
		DisplayUtil.sysout.write("                      ", ws.getNudberdd().getDb2StatErrMsg().getErrMsgData().getGFormatted(9));
		// COB_CODE: DISPLAY       '                      ' DB2-ERR-MSG(10).
		DisplayUtil.sysout.write("                      ", ws.getNudberdd().getDb2StatErrMsg().getErrMsgData().getGFormatted(10));
		// COB_CODE: DISPLAY '*************************************************'.
		DisplayUtil.sysout.write("*************************************************");
		// COB_CODE: EXIT.
		//exit
	}

	/**Original name: RNG_9700-DISPLAY-RESTART-INFO<br>*/
	private void rng9700DisplayRestartInfo() {
		String retcode = "";
		boolean goto9700Exit = false;
		boolean gotoExitlabel = false;
		retcode = displayRestartInfo();
		if (!retcode.equals("9700-EXIT")) {
			gotoExitlabel = true;
		}
		if (!gotoExitlabel) {
			goto9700Exit = false;
			readJobParms();
			displaySqlcode();
			callAbend();
			db2StatErr();
			throw new ReturnException();
		}
	}

	@Override
	public DdCard[] getDefaultDdCards() {
		return new DdCard[] { new DdCard("JOBPARMS", 80) };
	}
}
