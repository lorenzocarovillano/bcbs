/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: NF05-GROUP-AND-ACCOUNT-3<br>
 * Variable: NF05-GROUP-AND-ACCOUNT-3 from copybook NF05EOB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Nf05GroupAndAccount3 {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.VALUE);
	public static final String NF05_GROUP_MPN = "MPN";

	//==== METHODS ====
	public void setAccount3(String account3) {
		this.value = Functions.subString(account3, Len.VALUE);
	}

	public String getAccount3() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
