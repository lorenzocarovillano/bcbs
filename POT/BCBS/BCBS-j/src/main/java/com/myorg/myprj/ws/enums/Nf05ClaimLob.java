/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: NF05-CLAIM-LOB<br>
 * Variable: NF05-CLAIM-LOB from copybook NF05EOB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Nf05ClaimLob {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char CLAIM_LOB_VALUES_1_MIN = '1';
	public static final char CLAIM_LOB_VALUES_1_MAX = '8';
	public static final char CLAIM_LOB_VALUES_2 = 'D';
	public static final char CLAIM_LOB_VALUES_3 = 'R';
	public static final char CROSS_INPAT = '1';
	public static final char CROSS_OUTPAT = '2';
	public static final char SHIELD = '3';
	public static final char ADMISSION_NOTICE = '4';
	public static final char DENTAL = '5';
	public static final char ORTHO = '6';
	public static final char PLAN65_CROSS_INPAT = '7';
	public static final char PLAN65_CROSS_OUTPAT = '8';
	public static final char PLAN65_SHIELD = '8';
	public static final char PLAN65_PLUS = '8';
	public static final char DRUGS = 'D';
	public static final char REFERRAL = 'R';

	//==== METHODS ====
	public void setNf05ClaimLob(char nf05ClaimLob) {
		this.value = nf05ClaimLob;
	}

	public char getNf05ClaimLob() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
