/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: FILLER-WS-DATE-FIELDS<br>
 * Variable: FILLER-WS-DATE-FIELDS from program NF0533ML<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class FillerWsDateFields {

	//==== PROPERTIES ====
	//Original name: WS-BUSINESS-CCYY-N
	private String ccyyN = DefaultValues.stringVal(Len.CCYY_N);
	//Original name: WS-BUSINESS-MM-N
	private String mmN = DefaultValues.stringVal(Len.MM_N);
	//Original name: WS-BUSINESS-DD-N
	private String ddN = DefaultValues.stringVal(Len.DD_N);

	//==== METHODS ====
	/**Original name: WS-BUSINESS-DATE-9<br>*/
	public int getWsBusinessDate9() {
		int position = 1;
		return MarshalByte.readInt(getFillerWsDateFieldsBytes(), position, Len.Int.WS_BUSINESS_DATE9, SignType.NO_SIGN);
	}

	public String getWsBusinessDate9Formatted() {
		int position = 1;
		return MarshalByte.readFixedString(getFillerWsDateFieldsBytes(), position, Len.WS_BUSINESS_DATE9);
	}

	public String getWsBusinessDate9AsString() {
		return getWsBusinessDate9Formatted();
	}

	/**Original name: FILLER-WS-DATE-FIELDS<br>*/
	public byte[] getFillerWsDateFieldsBytes() {
		byte[] buffer = new byte[Len.FILLER_WS_DATE_FIELDS];
		return getFillerWsDateFieldsBytes(buffer, 1);
	}

	public byte[] getFillerWsDateFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ccyyN, Len.CCYY_N);
		position += Len.CCYY_N;
		MarshalByte.writeString(buffer, position, mmN, Len.MM_N);
		position += Len.MM_N;
		MarshalByte.writeString(buffer, position, ddN, Len.DD_N);
		return buffer;
	}

	public void setCcyyNFromBuffer(byte[] buffer) {
		ccyyN = MarshalByte.readFixedString(buffer, 1, Len.CCYY_N);
	}

	public void setMmNFormatted(String mmN) {
		this.mmN = Trunc.toUnsignedNumeric(mmN, Len.MM_N);
	}

	public short getMmN() {
		return NumericDisplay.asShort(this.mmN);
	}

	public void setDdNFormatted(String ddN) {
		this.ddN = Trunc.toUnsignedNumeric(ddN, Len.DD_N);
	}

	public short getDdN() {
		return NumericDisplay.asShort(this.ddN);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CCYY_N = 4;
		public static final int MM_N = 2;
		public static final int DD_N = 2;
		public static final int FILLER_WS_DATE_FIELDS = CCYY_N + MM_N + DD_N;
		public static final int WS_BUSINESS_DATE9 = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_BUSINESS_DATE9 = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
