/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: PROV-PASSED-INDIC-INPUT<br>
 * Variable: PROV-PASSED-INDIC-INPUT from copybook NU02PROV<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ProvPassedIndicInput {

	//==== PROPERTIES ====
	//Original name: PROV-ID
	private String id = DefaultValues.stringVal(Len.ID);
	//Original name: PROV-LOB-CD
	private char lobCd = DefaultValues.CHAR_VAL;
	//Original name: PROV-CN-ARNG-CD
	private String cnArngCd = DefaultValues.stringVal(Len.CN_ARNG_CD);
	//Original name: PROV-AD-CD
	private String adCd = DefaultValues.stringVal(Len.AD_CD);
	//Original name: PROV-XREF-NUM-1-TYPE
	private String xrefNum1Type = DefaultValues.stringVal(Len.XREF_NUM1_TYPE);
	//Original name: PROV-XREF-NUM-2-TYPE
	private String xrefNum2Type = DefaultValues.stringVal(Len.XREF_NUM2_TYPE);

	//==== METHODS ====
	public void setProvPassedIndicInputBytes(byte[] buffer, int offset) {
		int position = offset;
		id = MarshalByte.readString(buffer, position, Len.ID);
		position += Len.ID;
		lobCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cnArngCd = MarshalByte.readString(buffer, position, Len.CN_ARNG_CD);
		position += Len.CN_ARNG_CD;
		adCd = MarshalByte.readString(buffer, position, Len.AD_CD);
		position += Len.AD_CD;
		setXrefNumTypesBytes(buffer, position);
	}

	public byte[] getProvPassedIndicInputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, id, Len.ID);
		position += Len.ID;
		MarshalByte.writeChar(buffer, position, lobCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cnArngCd, Len.CN_ARNG_CD);
		position += Len.CN_ARNG_CD;
		MarshalByte.writeString(buffer, position, adCd, Len.AD_CD);
		position += Len.AD_CD;
		getXrefNumTypesBytes(buffer, position);
		return buffer;
	}

	public void initProvPassedIndicInputSpaces() {
		id = "";
		lobCd = Types.SPACE_CHAR;
		cnArngCd = "";
		adCd = "";
		initXrefNumTypesSpaces();
	}

	public void setId(String id) {
		this.id = Functions.subString(id, Len.ID);
	}

	public String getId() {
		return this.id;
	}

	public String getIdFormatted() {
		return Functions.padBlanks(getId(), Len.ID);
	}

	public void setLobCd(char lobCd) {
		this.lobCd = lobCd;
	}

	public void setLobCdFormatted(String lobCd) {
		setLobCd(Functions.charAt(lobCd, Types.CHAR_SIZE));
	}

	public char getLobCd() {
		return this.lobCd;
	}

	public void setCnArngCd(String cnArngCd) {
		this.cnArngCd = Functions.subString(cnArngCd, Len.CN_ARNG_CD);
	}

	public String getCnArngCd() {
		return this.cnArngCd;
	}

	public String getCnArngCdFormatted() {
		return Functions.padBlanks(getCnArngCd(), Len.CN_ARNG_CD);
	}

	public void setAdCd(String adCd) {
		this.adCd = Functions.subString(adCd, Len.AD_CD);
	}

	public String getAdCd() {
		return this.adCd;
	}

	public String getAdCdFormatted() {
		return Functions.padBlanks(getAdCd(), Len.AD_CD);
	}

	public void setXrefNumTypesBytes(byte[] buffer, int offset) {
		int position = offset;
		xrefNum1Type = MarshalByte.readString(buffer, position, Len.XREF_NUM1_TYPE);
		position += Len.XREF_NUM1_TYPE;
		xrefNum2Type = MarshalByte.readString(buffer, position, Len.XREF_NUM2_TYPE);
	}

	public byte[] getXrefNumTypesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xrefNum1Type, Len.XREF_NUM1_TYPE);
		position += Len.XREF_NUM1_TYPE;
		MarshalByte.writeString(buffer, position, xrefNum2Type, Len.XREF_NUM2_TYPE);
		return buffer;
	}

	public void initXrefNumTypesSpaces() {
		xrefNum1Type = "";
		xrefNum2Type = "";
	}

	public void setXrefNum1Type(String xrefNum1Type) {
		this.xrefNum1Type = Functions.subString(xrefNum1Type, Len.XREF_NUM1_TYPE);
	}

	public String getXrefNum1Type() {
		return this.xrefNum1Type;
	}

	public String getXrefNum1TypeFormatted() {
		return Functions.padBlanks(getXrefNum1Type(), Len.XREF_NUM1_TYPE);
	}

	public void setXrefNum2Type(String xrefNum2Type) {
		this.xrefNum2Type = Functions.subString(xrefNum2Type, Len.XREF_NUM2_TYPE);
	}

	public String getXrefNum2Type() {
		return this.xrefNum2Type;
	}

	public String getXrefNum2TypeFormatted() {
		return Functions.padBlanks(getXrefNum2Type(), Len.XREF_NUM2_TYPE);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ID = 10;
		public static final int LOB_CD = 1;
		public static final int CN_ARNG_CD = 5;
		public static final int AD_CD = 5;
		public static final int XREF_NUM1_TYPE = 5;
		public static final int XREF_NUM2_TYPE = 5;
		public static final int XREF_NUM_TYPES = XREF_NUM1_TYPE + XREF_NUM2_TYPE;
		public static final int PROV_PASSED_INDIC_INPUT = ID + LOB_CD + CN_ARNG_CD + AD_CD + XREF_NUM_TYPES;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
