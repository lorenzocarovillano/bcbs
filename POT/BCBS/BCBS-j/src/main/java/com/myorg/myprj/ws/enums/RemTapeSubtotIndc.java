/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: REM-TAPE-SUBTOT-INDC<br>
 * Variable: REM-TAPE-SUBTOT-INDC from copybook NF07RMIT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class RemTapeSubtotIndc {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char PAGE_CTR = 'A';
	public static final char PIP_BEGIN = 'B';
	public static final char LEVEL_PAY = 'C';
	public static final char DETAIL_LINE = 'D';
	public static final char PIP_END = 'E';
	public static final char CAPITATION_LINE = 'Q';
	public static final char SUBTOT_LINE = 'S';
	public static final char TOTAL_LINE = 'T';
	public static final char PEND_LINE = 'Y';
	public static final char WO_LINE = 'Z';
	private static final char[] LINE_CTR = new char[] { 'B', 'C', 'E', 'S' };

	//==== METHODS ====
	public void setTapeSubtotIndc(char tapeSubtotIndc) {
		this.value = tapeSubtotIndc;
	}

	public void setTapeSubtotIndcFormatted(String tapeSubtotIndc) {
		setTapeSubtotIndc(Functions.charAt(tapeSubtotIndc, Types.CHAR_SIZE));
	}

	public char getTapeSubtotIndc() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALUE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
