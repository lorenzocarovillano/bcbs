/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.myorg.myprj.commons.data.to.IS02814sa;

/**
 * Data Access Object(DAO) for table [S02814SA]
 * 
 */
public class S02814saDao extends BaseSqlDao<IS02814sa> {

	public S02814saDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IS02814sa> getToClass() {
		return IS02814sa.class;
	}

	public DbAccessStatus insertRec(IS02814sa iS02814sa) {
		return buildQuery("insertRec").bind(iS02814sa).executeInsert();
	}

	public DbAccessStatus insertRec1(IS02814sa iS02814sa) {
		return buildQuery("insertRec1").bind(iS02814sa).executeInsert();
	}
}
