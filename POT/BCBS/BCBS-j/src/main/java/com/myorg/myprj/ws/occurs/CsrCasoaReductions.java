/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: CSR-CASOA-REDUCTIONS<br>
 * Variables: CSR-CASOA-REDUCTIONS from copybook NF07AREA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class CsrCasoaReductions {

	//==== PROPERTIES ====
	//Original name: CSR-CASOA-CLM-ADJ-RSN-CD
	private String clmAdjRsnCd = "";
	//Original name: CSR-CASOA-MNTRY-AM
	private AfDecimal mntryAm = new AfDecimal("0", 11, 2);
	//Original name: CSR-CASOA-QNTY-CT
	private AfDecimal qntyCt = new AfDecimal("0", 7, 2);

	//==== METHODS ====
	public byte[] getCasoaReductionsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clmAdjRsnCd, Len.CLM_ADJ_RSN_CD);
		position += Len.CLM_ADJ_RSN_CD;
		MarshalByte.writeDecimalAsPacked(buffer, position, mntryAm.copy());
		position += Len.MNTRY_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, qntyCt.copy());
		return buffer;
	}

	public void setClmAdjRsnCd(String clmAdjRsnCd) {
		this.clmAdjRsnCd = Functions.subString(clmAdjRsnCd, Len.CLM_ADJ_RSN_CD);
	}

	public String getClmAdjRsnCd() {
		return this.clmAdjRsnCd;
	}

	public void setMntryAm(AfDecimal mntryAm) {
		this.mntryAm.assign(mntryAm);
	}

	public AfDecimal getMntryAm() {
		return this.mntryAm.copy();
	}

	public void setQntyCt(AfDecimal qntyCt) {
		this.qntyCt.assign(qntyCt);
	}

	public AfDecimal getQntyCt() {
		return this.qntyCt.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_ADJ_RSN_CD = 5;
		public static final int MNTRY_AM = 6;
		public static final int QNTY_CT = 4;
		public static final int CASOA_REDUCTIONS = CLM_ADJ_RSN_CD + MNTRY_AM + QNTY_CT;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int MNTRY_AM = 9;
			public static final int QNTY_CT = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int MNTRY_AM = 2;
			public static final int QNTY_CT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
