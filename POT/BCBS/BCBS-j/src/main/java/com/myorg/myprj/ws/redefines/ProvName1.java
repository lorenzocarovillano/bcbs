/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: PROV-NAME-1<br>
 * Variable: PROV-NAME-1 from program NF0533ML<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class ProvName1 extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public ProvName1() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.PROV_NAME1;
	}

	public void setProvName1Bytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.PROV_NAME1, Pos.PROV_NAME1);
	}

	public byte[] getProvName1Bytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.PROV_NAME1, Pos.PROV_NAME1);
		return buffer;
	}

	public void initProvName1Spaces() {
		fill(Pos.PROV_NAME1, Len.PROV_NAME1, Types.SPACE_CHAR);
	}

	/**Original name: PROV-INST-NAME<br>*/
	public String getInstName() {
		return readString(Pos.INST_NAME, Len.INST_NAME);
	}

	/**Original name: PROV-L-NAME<br>*/
	public String getlName() {
		return readString(Pos.L_NAME, Len.L_NAME);
	}

	/**Original name: PROV-F-NAME<br>*/
	public String getfName() {
		return readString(Pos.F_NAME, Len.F_NAME);
	}

	/**Original name: PROV-M-INIT<br>*/
	public char getmInit() {
		return readChar(Pos.M_INIT);
	}

	/**Original name: PROV-SUPL-NAME<br>*/
	public String getSuplName() {
		return readString(Pos.SUPL_NAME, Len.SUPL_NAME);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int PROV_NAME1 = 1;
		public static final int ADMN_NAME = PROV_NAME1;
		public static final int FLR1 = ADMN_NAME + Len.ADMN_NAME;
		public static final int INST_NAME = FLR1 + Len.FLR1;
		public static final int PROV_NAME2 = 1;
		public static final int L_NAME = PROV_NAME2;
		public static final int F_NAME = L_NAME + Len.L_NAME;
		public static final int M_NAME = F_NAME + Len.F_NAME;
		public static final int M_INIT = M_NAME;
		public static final int M_REST = M_INIT + Len.M_INIT;
		public static final int TITLE = M_REST + Len.M_REST;
		public static final int FLR2 = TITLE + Len.TITLE;
		public static final int PROV_NAME3 = 1;
		public static final int FLR3 = PROV_NAME3;
		public static final int SUPL_NAME = FLR3 + Len.FLR3;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int ADMN_NAME = 30;
		public static final int FLR1 = 5;
		public static final int L_NAME = 35;
		public static final int F_NAME = 25;
		public static final int M_INIT = 1;
		public static final int M_REST = 14;
		public static final int TITLE = 4;
		public static final int FLR3 = 35;
		public static final int INST_NAME = 45;
		public static final int PROV_NAME1 = ADMN_NAME + INST_NAME + FLR1;
		public static final int SUPL_NAME = 45;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
