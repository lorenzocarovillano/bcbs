/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-EXP-WORK-ITEMS<br>
 * Variable: WS-EXP-WORK-ITEMS from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsExpWorkItems {

	//==== PROPERTIES ====
	//Original name: WS-GL-OFST-ORIG-CD
	private String wsGlOfstOrigCd = DefaultValues.stringVal(Len.WS_GL_OFST_ORIG_CD);
	//Original name: WS-GL-SOTE-ORIG-CD
	private short wsGlSoteOrigCd = DefaultValues.SHORT_VAL;
	//Original name: WS-GL-OFST-VOID-CD
	private String wsGlOfstVoidCd = DefaultValues.stringVal(Len.WS_GL_OFST_VOID_CD);
	//Original name: WS-GL-SOTE-VOID-CD
	private short wsGlSoteVoidCd = DefaultValues.SHORT_VAL;
	//Original name: WS-LINE-NUM
	private short wsLineNum = DefaultValues.SHORT_VAL;
	//Original name: PREV-CLM-PMT-ID
	private short prevClmPmtId = DefaultValues.SHORT_VAL;
	//Original name: WS-END-OF-LINES
	private char wsEndOfLines = 'N';

	//==== METHODS ====
	public void setWsGlOfstOrigCd(String wsGlOfstOrigCd) {
		this.wsGlOfstOrigCd = Functions.subString(wsGlOfstOrigCd, Len.WS_GL_OFST_ORIG_CD);
	}

	public String getWsGlOfstOrigCd() {
		return this.wsGlOfstOrigCd;
	}

	public void setWsGlSoteOrigCd(short wsGlSoteOrigCd) {
		this.wsGlSoteOrigCd = wsGlSoteOrigCd;
	}

	public short getWsGlSoteOrigCd() {
		return this.wsGlSoteOrigCd;
	}

	public void setWsGlOfstVoidCd(String wsGlOfstVoidCd) {
		this.wsGlOfstVoidCd = Functions.subString(wsGlOfstVoidCd, Len.WS_GL_OFST_VOID_CD);
	}

	public String getWsGlOfstVoidCd() {
		return this.wsGlOfstVoidCd;
	}

	public void setWsGlSoteVoidCd(short wsGlSoteVoidCd) {
		this.wsGlSoteVoidCd = wsGlSoteVoidCd;
	}

	public short getWsGlSoteVoidCd() {
		return this.wsGlSoteVoidCd;
	}

	public void setWsLineNum(short wsLineNum) {
		this.wsLineNum = wsLineNum;
	}

	public short getWsLineNum() {
		return this.wsLineNum;
	}

	public void setPrevClmPmtId(short prevClmPmtId) {
		this.prevClmPmtId = prevClmPmtId;
	}

	public short getPrevClmPmtId() {
		return this.prevClmPmtId;
	}

	public void setWsEndOfLines(char wsEndOfLines) {
		this.wsEndOfLines = wsEndOfLines;
	}

	public void setWsEndOfLinesFormatted(String wsEndOfLines) {
		setWsEndOfLines(Functions.charAt(wsEndOfLines, Types.CHAR_SIZE));
	}

	public char getWsEndOfLines() {
		return this.wsEndOfLines;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_GL_OFST_VOID_CD = 2;
		public static final int WS_GL_OFST_ORIG_CD = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
