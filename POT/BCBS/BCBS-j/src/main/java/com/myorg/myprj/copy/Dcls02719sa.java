/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.commons.data.to.IS02719sa;

/**Original name: DCLS02719SA<br>
 * Variable: DCLS02719SA from copybook S02719SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02719sa implements IS02719sa {

	//==== PROPERTIES ====
	//Original name: PROD-SRV-ID-QLF-CD
	private String prodSrvIdQlfCd = DefaultValues.stringVal(Len.PROD_SRV_ID_QLF_CD);
	//Original name: PROD-SRV-ID
	private String prodSrvId = DefaultValues.stringVal(Len.PROD_SRV_ID);
	//Original name: SV3013-PROC-MOD-CD
	private String sv3013ProcModCd = DefaultValues.stringVal(Len.SV3013_PROC_MOD_CD);
	//Original name: SV3014-PROC-MOD-CD
	private String sv3014ProcModCd = DefaultValues.stringVal(Len.SV3014_PROC_MOD_CD);
	//Original name: SV3015-PROC-MOD-CD
	private String sv3015ProcModCd = DefaultValues.stringVal(Len.SV3015_PROC_MOD_CD);
	//Original name: SV3016-PROC-MOD-CD
	private String sv3016ProcModCd = DefaultValues.stringVal(Len.SV3016_PROC_MOD_CD);
	//Original name: MNTRY-AM
	private AfDecimal mntryAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: QNTY-CT
	private AfDecimal qntyCt = new AfDecimal(DefaultValues.DEC_VAL, 7, 2);

	//==== METHODS ====
	@Override
	public void setProdSrvIdQlfCd(String prodSrvIdQlfCd) {
		this.prodSrvIdQlfCd = Functions.subString(prodSrvIdQlfCd, Len.PROD_SRV_ID_QLF_CD);
	}

	@Override
	public String getProdSrvIdQlfCd() {
		return this.prodSrvIdQlfCd;
	}

	@Override
	public void setProdSrvId(String prodSrvId) {
		this.prodSrvId = Functions.subString(prodSrvId, Len.PROD_SRV_ID);
	}

	@Override
	public String getProdSrvId() {
		return this.prodSrvId;
	}

	@Override
	public void setSv3013ProcModCd(String sv3013ProcModCd) {
		this.sv3013ProcModCd = Functions.subString(sv3013ProcModCd, Len.SV3013_PROC_MOD_CD);
	}

	@Override
	public String getSv3013ProcModCd() {
		return this.sv3013ProcModCd;
	}

	@Override
	public void setSv3014ProcModCd(String sv3014ProcModCd) {
		this.sv3014ProcModCd = Functions.subString(sv3014ProcModCd, Len.SV3014_PROC_MOD_CD);
	}

	@Override
	public String getSv3014ProcModCd() {
		return this.sv3014ProcModCd;
	}

	@Override
	public void setSv3015ProcModCd(String sv3015ProcModCd) {
		this.sv3015ProcModCd = Functions.subString(sv3015ProcModCd, Len.SV3015_PROC_MOD_CD);
	}

	@Override
	public String getSv3015ProcModCd() {
		return this.sv3015ProcModCd;
	}

	@Override
	public void setSv3016ProcModCd(String sv3016ProcModCd) {
		this.sv3016ProcModCd = Functions.subString(sv3016ProcModCd, Len.SV3016_PROC_MOD_CD);
	}

	@Override
	public String getSv3016ProcModCd() {
		return this.sv3016ProcModCd;
	}

	@Override
	public void setMntryAm(AfDecimal mntryAm) {
		this.mntryAm.assign(mntryAm);
	}

	@Override
	public AfDecimal getMntryAm() {
		return this.mntryAm.copy();
	}

	@Override
	public void setQntyCt(AfDecimal qntyCt) {
		this.qntyCt.assign(qntyCt);
	}

	@Override
	public AfDecimal getQntyCt() {
		return this.qntyCt.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROD_SRV_ID_QLF_CD = 2;
		public static final int PROD_SRV_ID = 48;
		public static final int SV3013_PROC_MOD_CD = 2;
		public static final int SV3014_PROC_MOD_CD = 2;
		public static final int SV3015_PROC_MOD_CD = 2;
		public static final int SV3016_PROC_MOD_CD = 2;
		public static final int CLM_CNTL_ID = 12;
		public static final int LOOP_ID = 6;
		public static final int SEG_ID = 3;
		public static final int SV3017_DS_TX = 80;
		public static final int FAC_VL_CD = 2;
		public static final int SV3041_OL_CT_DT_CD = 3;
		public static final int SV3042_OL_CT_DT_CD = 3;
		public static final int SV3043_OL_CT_DT_CD = 3;
		public static final int SV3044_OL_CT_DT_CD = 3;
		public static final int SV3045_OL_CT_DT_CD = 3;
		public static final int SV3111_CMP_DGPT_CD = 2;
		public static final int SV3112_CMP_DGPT_CD = 2;
		public static final int SV3113_CMP_DGPT_CD = 2;
		public static final int SV3114_CMP_DGPT_CD = 2;
		public static final int INFO_CHG_ID = 8;
		public static final int INFO_CHG_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
