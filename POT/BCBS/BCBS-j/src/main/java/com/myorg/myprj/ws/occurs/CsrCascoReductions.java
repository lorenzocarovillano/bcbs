/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: CSR-CASCO-REDUCTIONS<br>
 * Variables: CSR-CASCO-REDUCTIONS from copybook NF07AREA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class CsrCascoReductions {

	//==== PROPERTIES ====
	//Original name: CSR-CASCO-CLM-ADJ-RSN-CD
	private String clmAdjRsnCd = "";
	//Original name: CSR-CASCO-MNTRY-AM
	private AfDecimal mntryAm = new AfDecimal("0", 11, 2);
	//Original name: CSR-CASCO-QNTY-CT
	private AfDecimal qntyCt = new AfDecimal("0", 7, 2);
	//Original name: CSR-CASCO-EOB-AM
	private AfDecimal eobAm = new AfDecimal("0", 11, 2);

	//==== METHODS ====
	public byte[] getCascoReductionsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clmAdjRsnCd, Len.CLM_ADJ_RSN_CD);
		position += Len.CLM_ADJ_RSN_CD;
		MarshalByte.writeDecimalAsPacked(buffer, position, mntryAm.copy());
		position += Len.MNTRY_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, qntyCt.copy());
		position += Len.QNTY_CT;
		MarshalByte.writeDecimalAsPacked(buffer, position, eobAm.copy());
		return buffer;
	}

	public void setClmAdjRsnCd(String clmAdjRsnCd) {
		this.clmAdjRsnCd = Functions.subString(clmAdjRsnCd, Len.CLM_ADJ_RSN_CD);
	}

	public String getClmAdjRsnCd() {
		return this.clmAdjRsnCd;
	}

	public void setMntryAm(AfDecimal mntryAm) {
		this.mntryAm.assign(mntryAm);
	}

	public AfDecimal getMntryAm() {
		return this.mntryAm.copy();
	}

	public void setQntyCt(AfDecimal qntyCt) {
		this.qntyCt.assign(qntyCt);
	}

	public AfDecimal getQntyCt() {
		return this.qntyCt.copy();
	}

	public void setEobAm(AfDecimal eobAm) {
		this.eobAm.assign(eobAm);
	}

	public AfDecimal getEobAm() {
		return this.eobAm.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_ADJ_RSN_CD = 5;
		public static final int MNTRY_AM = 6;
		public static final int QNTY_CT = 4;
		public static final int EOB_AM = 6;
		public static final int CASCO_REDUCTIONS = CLM_ADJ_RSN_CD + MNTRY_AM + QNTY_CT + EOB_AM;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int MNTRY_AM = 9;
			public static final int QNTY_CT = 5;
			public static final int EOB_AM = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int MNTRY_AM = 2;
			public static final int QNTY_CT = 2;
			public static final int EOB_AM = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
