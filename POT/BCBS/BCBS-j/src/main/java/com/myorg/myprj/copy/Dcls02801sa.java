/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.myorg.myprj.commons.data.to.IS02801sa;

/**Original name: DCLS02801SA<br>
 * Variable: DCLS02801SA from copybook S02801SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls02801sa implements IS02801sa {

	//==== PROPERTIES ====
	//Original name: CLM-CNTL-ID
	private String clmCntlId = DefaultValues.stringVal(Len.CLM_CNTL_ID);
	//Original name: CLM-CNTL-SFX-ID
	private char clmCntlSfxId = DefaultValues.CHAR_VAL;
	//Original name: CLM-PMT-ID
	private short clmPmtId = DefaultValues.SHORT_VAL;
	//Original name: HCC-STAT-CATG-CD
	private String hccStatCatgCd = DefaultValues.stringVal(Len.HCC_STAT_CATG_CD);
	//Original name: FNL-BUS-DT
	private String fnlBusDt = DefaultValues.stringVal(Len.FNL_BUS_DT);

	//==== METHODS ====
	@Override
	public void setClmCntlId(String clmCntlId) {
		this.clmCntlId = Functions.subString(clmCntlId, Len.CLM_CNTL_ID);
	}

	@Override
	public String getClmCntlId() {
		return this.clmCntlId;
	}

	@Override
	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.clmCntlSfxId = clmCntlSfxId;
	}

	@Override
	public char getClmCntlSfxId() {
		return this.clmCntlSfxId;
	}

	@Override
	public void setClmPmtId(short clmPmtId) {
		this.clmPmtId = clmPmtId;
	}

	@Override
	public short getClmPmtId() {
		return this.clmPmtId;
	}

	@Override
	public void setHccStatCatgCd(String hccStatCatgCd) {
		this.hccStatCatgCd = Functions.subString(hccStatCatgCd, Len.HCC_STAT_CATG_CD);
	}

	@Override
	public String getHccStatCatgCd() {
		return this.hccStatCatgCd;
	}

	public String getHccStatCatgCdFormatted() {
		return Functions.padBlanks(getHccStatCatgCd(), Len.HCC_STAT_CATG_CD);
	}

	@Override
	public void setFnlBusDt(String fnlBusDt) {
		this.fnlBusDt = Functions.subString(fnlBusDt, Len.FNL_BUS_DT);
	}

	@Override
	public String getFnlBusDt() {
		return this.fnlBusDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLM_CNTL_ID = 12;
		public static final int HCC_STAT_CATG_CD = 3;
		public static final int FNL_BUS_DT = 10;
		public static final int INFO_CHG_TS = 26;
		public static final int INFO_SSYST_ID = 4;
		public static final int INFO_INSRT_PGM_NM = 8;
		public static final int INFO_CHG_ID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
