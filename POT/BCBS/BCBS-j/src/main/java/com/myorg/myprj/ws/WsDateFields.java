/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.ws;

import com.myorg.myprj.ws.redefines.WsCorpDateN;

/**Original name: WS-DATE-FIELDS<br>
 * Variable: WS-DATE-FIELDS from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsDateFields {

	//==== PROPERTIES ====
	//Original name: WS-BUSINESS-DATE
	private WsBusinessDate wsBusinessDate = new WsBusinessDate();
	//Original name: FILLER-WS-DATE-FIELDS
	private FillerWsDateFields fillerWsDateFields = new FillerWsDateFields();
	//Original name: FILLER-WS-DATE-FIELDS-1
	private FillerWsDateFields1 fillerWsDateFields1 = new FillerWsDateFields1();
	//Original name: WS-CORP-DATE-DB2
	private WsCorpDateDb2 wsCorpDateDb2 = new WsCorpDateDb2();
	//Original name: WS-CORP-DATE-N
	private WsCorpDateN wsCorpDateN = new WsCorpDateN();

	//==== METHODS ====
	public FillerWsDateFields1 getFillerWsDateFields1() {
		return fillerWsDateFields1;
	}

	public FillerWsDateFields getFillerWsDateFields() {
		return fillerWsDateFields;
	}

	public WsBusinessDate getWsBusinessDate() {
		return wsBusinessDate;
	}

	public WsCorpDateDb2 getWsCorpDateDb2() {
		return wsCorpDateDb2;
	}

	public WsCorpDateN getWsCorpDateN() {
		return wsCorpDateN;
	}
}
