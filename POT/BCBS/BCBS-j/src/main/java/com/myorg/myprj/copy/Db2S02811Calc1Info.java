/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DB2-S02811-CALC1-INFO<br>
 * Variable: DB2-S02811-CALC1-INFO from copybook NF05CURS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Db2S02811Calc1Info {

	//==== PROPERTIES ====
	//Original name: DB2-C1-CALC-TYP-CD
	private char calcTypCd = DefaultValues.CHAR_VAL;
	//Original name: DB2-C1-CALC-EXPLN-CD
	private char calcExplnCd = DefaultValues.CHAR_VAL;
	//Original name: DB2-C1-PAY-CD
	private char payCd = DefaultValues.CHAR_VAL;
	//Original name: DB2-C1-COV-LOB-CD
	private char covLobCd = DefaultValues.CHAR_VAL;
	//Original name: DB2-C1-ALW-CHG-AM
	private AfDecimal alwChgAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: DB2-C1-PD-AM
	private AfDecimal pdAm = new AfDecimal(DefaultValues.DEC_VAL, 11, 2);
	//Original name: DB2-C1-CORP-CD
	private char corpCd = DefaultValues.CHAR_VAL;
	//Original name: DB2-C1-PROD-CD
	private String prodCd = DefaultValues.stringVal(Len.PROD_CD);
	//Original name: DB2-C1-TYP-BEN-CD
	private String typBenCd = DefaultValues.stringVal(Len.TYP_BEN_CD);
	//Original name: DB2-C1-SPECI-BEN-CD
	private String speciBenCd = DefaultValues.stringVal(Len.SPECI_BEN_CD);
	//Original name: DB2-C1-GL-ACCT-ID
	private String glAcctId = DefaultValues.stringVal(Len.GL_ACCT_ID);
	//Original name: DB2-C1-INS-TC-CD
	private String insTcCd = DefaultValues.stringVal(Len.INS_TC_CD);

	//==== METHODS ====
	public void setS02811Calc1InfoBytes(byte[] buffer, int offset) {
		int position = offset;
		calcTypCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		calcExplnCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		payCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		covLobCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		alwChgAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ALW_CHG_AM, Len.Fract.ALW_CHG_AM));
		position += Len.ALW_CHG_AM;
		pdAm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PD_AM, Len.Fract.PD_AM));
		position += Len.PD_AM;
		corpCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		prodCd = MarshalByte.readString(buffer, position, Len.PROD_CD);
		position += Len.PROD_CD;
		typBenCd = MarshalByte.readString(buffer, position, Len.TYP_BEN_CD);
		position += Len.TYP_BEN_CD;
		speciBenCd = MarshalByte.readString(buffer, position, Len.SPECI_BEN_CD);
		position += Len.SPECI_BEN_CD;
		glAcctId = MarshalByte.readString(buffer, position, Len.GL_ACCT_ID);
		position += Len.GL_ACCT_ID;
		insTcCd = MarshalByte.readString(buffer, position, Len.INS_TC_CD);
	}

	public byte[] getS02811Calc1InfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, calcTypCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, calcExplnCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, payCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, covLobCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeDecimalAsPacked(buffer, position, alwChgAm.copy());
		position += Len.ALW_CHG_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, pdAm.copy());
		position += Len.PD_AM;
		MarshalByte.writeChar(buffer, position, corpCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, prodCd, Len.PROD_CD);
		position += Len.PROD_CD;
		MarshalByte.writeString(buffer, position, typBenCd, Len.TYP_BEN_CD);
		position += Len.TYP_BEN_CD;
		MarshalByte.writeString(buffer, position, speciBenCd, Len.SPECI_BEN_CD);
		position += Len.SPECI_BEN_CD;
		MarshalByte.writeString(buffer, position, glAcctId, Len.GL_ACCT_ID);
		position += Len.GL_ACCT_ID;
		MarshalByte.writeString(buffer, position, insTcCd, Len.INS_TC_CD);
		return buffer;
	}

	public void setCalcTypCd(char calcTypCd) {
		this.calcTypCd = calcTypCd;
	}

	public char getCalcTypCd() {
		return this.calcTypCd;
	}

	public void setCalcExplnCd(char calcExplnCd) {
		this.calcExplnCd = calcExplnCd;
	}

	public char getCalcExplnCd() {
		return this.calcExplnCd;
	}

	public void setPayCd(char payCd) {
		this.payCd = payCd;
	}

	public char getPayCd() {
		return this.payCd;
	}

	public void setCovLobCd(char covLobCd) {
		this.covLobCd = covLobCd;
	}

	public char getCovLobCd() {
		return this.covLobCd;
	}

	public void setAlwChgAm(AfDecimal alwChgAm) {
		this.alwChgAm.assign(alwChgAm);
	}

	public AfDecimal getAlwChgAm() {
		return this.alwChgAm.copy();
	}

	public void setPdAm(AfDecimal pdAm) {
		this.pdAm.assign(pdAm);
	}

	public AfDecimal getPdAm() {
		return this.pdAm.copy();
	}

	public void setCorpCd(char corpCd) {
		this.corpCd = corpCd;
	}

	public char getCorpCd() {
		return this.corpCd;
	}

	public void setProdCd(String prodCd) {
		this.prodCd = Functions.subString(prodCd, Len.PROD_CD);
	}

	public String getProdCd() {
		return this.prodCd;
	}

	public void setTypBenCd(String typBenCd) {
		this.typBenCd = Functions.subString(typBenCd, Len.TYP_BEN_CD);
	}

	public String getTypBenCd() {
		return this.typBenCd;
	}

	public void setSpeciBenCd(String speciBenCd) {
		this.speciBenCd = Functions.subString(speciBenCd, Len.SPECI_BEN_CD);
	}

	public String getSpeciBenCd() {
		return this.speciBenCd;
	}

	public void setGlAcctId(String glAcctId) {
		this.glAcctId = Functions.subString(glAcctId, Len.GL_ACCT_ID);
	}

	public String getGlAcctId() {
		return this.glAcctId;
	}

	public void setInsTcCd(String insTcCd) {
		this.insTcCd = Functions.subString(insTcCd, Len.INS_TC_CD);
	}

	public String getInsTcCd() {
		return this.insTcCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROD_CD = 2;
		public static final int TYP_BEN_CD = 3;
		public static final int SPECI_BEN_CD = 4;
		public static final int GL_ACCT_ID = 8;
		public static final int INS_TC_CD = 2;
		public static final int CALC_TYP_CD = 1;
		public static final int CALC_EXPLN_CD = 1;
		public static final int PAY_CD = 1;
		public static final int COV_LOB_CD = 1;
		public static final int ALW_CHG_AM = 6;
		public static final int PD_AM = 6;
		public static final int CORP_CD = 1;
		public static final int S02811_CALC1_INFO = CALC_TYP_CD + CALC_EXPLN_CD + PAY_CD + COV_LOB_CD + ALW_CHG_AM + PD_AM + CORP_CD + PROD_CD
				+ TYP_BEN_CD + SPECI_BEN_CD + GL_ACCT_ID + INS_TC_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int ALW_CHG_AM = 9;
			public static final int PD_AM = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int ALW_CHG_AM = 2;
			public static final int PD_AM = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
