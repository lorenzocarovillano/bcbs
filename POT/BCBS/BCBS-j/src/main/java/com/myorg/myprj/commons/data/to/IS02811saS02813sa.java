/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [S02811SA, S02813SA]
 * 
 */
public interface IS02811saS02813sa extends BaseSqlTo {

	/**
	 * Host Variable HOLD-CALC-TYP-CD
	 * 
	 */
	char getCalcTypCd();

	void setCalcTypCd(char calcTypCd);

	/**
	 * Host Variable HOLD-COV-LOB-CD
	 * 
	 */
	char getCovLobCd();

	void setCovLobCd(char covLobCd);

	/**
	 * Host Variable HOLD-CALC-EXPLN-CD
	 * 
	 */
	char getCalcExplnCd();

	void setCalcExplnCd(char calcExplnCd);

	/**
	 * Host Variable HOLD-PAY-CD
	 * 
	 */
	char getPayCd();

	void setPayCd(char payCd);

	/**
	 * Host Variable HOLD-ALW-CHG-AM
	 * 
	 */
	AfDecimal getAlwChgAm();

	void setAlwChgAm(AfDecimal alwChgAm);

	/**
	 * Host Variable HOLD-PD-AM
	 * 
	 */
	AfDecimal getPdAm();

	void setPdAm(AfDecimal pdAm);

	/**
	 * Host Variable HOLD-GRP-ID
	 * 
	 */
	String getGrpId();

	void setGrpId(String grpId);

	/**
	 * Host Variable HOLD-CORP-CD
	 * 
	 */
	char getCorpCd();

	void setCorpCd(char corpCd);

	/**
	 * Host Variable HOLD-PROD-CD
	 * 
	 */
	String getProdCd();

	void setProdCd(String prodCd);

	/**
	 * Host Variable HOLD-TYP-BEN-CD
	 * 
	 */
	String getTypBenCd();

	void setTypBenCd(String typBenCd);

	/**
	 * Host Variable HOLD-SPECI-BEN-CD
	 * 
	 */
	String getSpeciBenCd();

	void setSpeciBenCd(String speciBenCd);

	/**
	 * Host Variable HOLD-GL-ACCT-ID
	 * 
	 */
	String getGlAcctId();

	void setGlAcctId(String glAcctId);

	/**
	 * Host Variable HOLD-INS-TC-CD
	 * 
	 */
	String getInsTcCd();

	void setInsTcCd(String insTcCd);
};
