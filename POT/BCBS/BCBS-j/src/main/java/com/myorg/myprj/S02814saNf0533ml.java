/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.myorg.myprj;

import com.modernsystems.jdbc.FieldNotMappedException;
import com.myorg.myprj.commons.data.to.IS02814sa;
import com.myorg.myprj.ws.Nf0533mlData;

/**Original name: S02814saNf0533ml<br>*/
public class S02814saNf0533ml implements IS02814sa {

	//==== PROPERTIES ====
	private Nf0533mlData ws;

	//==== CONSTRUCTORS ====
	public S02814saNf0533ml(Nf0533mlData ws) {
		this.ws = ws;
	}

	//==== METHODS ====
	@Override
	public String getClmCntlId() {
		return ws.getDcls02814sa().getClmCntlId();
	}

	@Override
	public void setClmCntlId(String clmCntlId) {
		this.ws.getDcls02814sa().setClmCntlId(clmCntlId);
	}

	@Override
	public char getClmCntlSfxId() {
		return ws.getDcls02814sa().getClmCntlSfxId();
	}

	@Override
	public void setClmCntlSfxId(char clmCntlSfxId) {
		this.ws.getDcls02814sa().setClmCntlSfxId(clmCntlSfxId);
	}

	@Override
	public short getClmPmtId() {
		return ws.getDcls02814sa().getClmPmtId();
	}

	@Override
	public void setClmPmtId(short clmPmtId) {
		this.ws.getDcls02814sa().setClmPmtId(clmPmtId);
	}

	@Override
	public String getDb2PmtKcapsTeamNm() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsTeamNm();
	}

	@Override
	public void setDb2PmtKcapsTeamNm(String db2PmtKcapsTeamNm) {
		this.ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtKcapsTeamNm(db2PmtKcapsTeamNm);
	}

	@Override
	public String getDb2PmtKcapsUseId() {
		return ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().getPmtKcapsUseId();
	}

	@Override
	public void setDb2PmtKcapsUseId(String db2PmtKcapsUseId) {
		this.ws.getEobDb2CursorArea().getS02813PmtOrClaimInfo().setPmtKcapsUseId(db2PmtKcapsUseId);
	}

	@Override
	public String getEvntLogBusDt() {
		return ws.getDcls02814sa().getEvntLogBusDt();
	}

	@Override
	public void setEvntLogBusDt(String evntLogBusDt) {
		this.ws.getDcls02814sa().setEvntLogBusDt(evntLogBusDt);
	}

	@Override
	public String getEvntLogStatCd() {
		return ws.getDcls02814sa().getEvntLogStatCd();
	}

	@Override
	public void setEvntLogStatCd(String evntLogStatCd) {
		this.ws.getDcls02814sa().setEvntLogStatCd(evntLogStatCd);
	}

	@Override
	public String getEvntLogSystDt() {
		return ws.getDcls02814sa().getEvntLogSystDt();
	}

	@Override
	public void setEvntLogSystDt(String evntLogSystDt) {
		this.ws.getDcls02814sa().setEvntLogSystDt(evntLogSystDt);
	}

	@Override
	public String getKcapsTeamNm() {
		throw new FieldNotMappedException("kcapsTeamNm");
	}

	@Override
	public void setKcapsTeamNm(String kcapsTeamNm) {
		throw new FieldNotMappedException("kcapsTeamNm");
	}

	@Override
	public String getKcapsUseId() {
		throw new FieldNotMappedException("kcapsUseId");
	}

	@Override
	public void setKcapsUseId(String kcapsUseId) {
		throw new FieldNotMappedException("kcapsUseId");
	}
}
