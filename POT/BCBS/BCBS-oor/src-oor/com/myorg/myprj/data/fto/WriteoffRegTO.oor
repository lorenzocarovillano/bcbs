package com.myorg.myprj.data.fto;

import java.lang.Override;

import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;

import com.myorg.myprj.copy.DetailPartOfRecord;
import com.myorg.myprj.copy.HeaderPartOfRecord;
import com.myorg.myprj.ws.enums.RemLineOfBusiness;
import com.myorg.myprj.ws.enums.RemPrintNoPrint;
import com.myorg.myprj.ws.enums.RemTypeOfRemit;

/**Original name: WRITEOFF-REG<br>
 * File: WRITEOFF-REG from program NF0533ML<br>
 * Generated as a class for rule FTO.<br>*/
class WriteoffRegTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: HEADER-PART-OF-RECORD
	private HeaderPartOfRecord headerPartOfRecord := new HeaderPartOfRecord();
	//Original name: DETAIL-PART-OF-RECORD
	private DetailPartOfRecord detailPartOfRecord := new DetailPartOfRecord();
	//Original name: REM-LINE-OF-BUSINESS
	private RemLineOfBusiness remLineOfBusiness := new RemLineOfBusiness();
	//Original name: REM-PRINT-NO-PRINT
	private RemPrintNoPrint remPrintNoPrint := new RemPrintNoPrint();
	//Original name: REM-TYPE-OF-REMIT
	private RemTypeOfRemit remTypeOfRemit := new RemTypeOfRemit();


	//==== METHODS ====
	/**Original name: REMITT-TAPE-RECORD<br>
	 * <pre> *---------------------
	 *  *             NF07RMIT                RECORD LENGTH:650
	 *  *   REMITTANCE ADVICE FILE LAYOUT
	 *  *   COPYBOOK   :  NFREMIT
	 *  *   LAST UPDATE:  07/97 EXPAND PROVIDER NBR, ADDRESS, ETC
	 *  *                 07/97 ADD TOOTH SVC CODE & ID FIELDS
	 *  *                 06/99 ADD A-INVALID-SW
	 *  *                 05/00 GMIS: RENAME REM-DETL-DENTAL-EXPL TO
	 *  *                       REM-DETL-REASON-EXPL.
	 *  *                 12/00 ADD: PROMPT PAY INTEREST FIELD
	 *  *                       AND RECEIPT DATE.
	 *  *                 02/02 CHANGES FOR THE HIPAA PROJECT
	 *  *                 09/04 CHANGES FOR AUTO DEDUCT MANAGEMENT RPT
	 *  *                 11/06 ADD FIELDS FOR NPI NUMBERS.
	 *  *   LENGTH     :  650
	 *  *---------------------------------</pre>*/
	public []byte getRemittTapeRecordBytes() {
		[]byte buffer := new [Len.REMITT_TAPE_RECORD]byte;
		return getRemittTapeRecordBytes(buffer, 1);
	}

	public void setRemittTapeRecordBytes([]byte buffer, integer offset) {
		integer position := offset;
		headerPartOfRecord.setHeaderPartOfRecordBytes(buffer, position);
		position +:= HeaderPartOfRecord.Len.HEADER_PART_OF_RECORD;
		detailPartOfRecord.setDetailPartOfRecordBytes(buffer, position);
		position +:= DetailPartOfRecord.Len.DETAIL_PART_OF_RECORD;
		setTrailerAreaBytes(buffer, position);
	}

	public []byte getRemittTapeRecordBytes([]byte buffer, integer offset) {
		integer position := offset;
		headerPartOfRecord.getHeaderPartOfRecordBytes(buffer, position);
		position +:= HeaderPartOfRecord.Len.HEADER_PART_OF_RECORD;
		detailPartOfRecord.getDetailPartOfRecordBytes(buffer, position);
		position +:= DetailPartOfRecord.Len.DETAIL_PART_OF_RECORD;
		getTrailerAreaBytes(buffer, position);
		return buffer;
	}

	public void initRemittTapeRecordSpaces() {
		headerPartOfRecord.initHeaderPartOfRecordSpaces();
		detailPartOfRecord.initDetailPartOfRecordSpaces();
		initTrailerAreaSpaces();
	}

	public void setTrailerAreaBytes([]byte buffer, integer offset) {
		integer position := offset;
		remLineOfBusiness.setRemLineOfBusiness(MarshalByte.readString(buffer, position, RemLineOfBusiness.Len.VALUE));
		position +:= RemLineOfBusiness.Len.VALUE;
		remPrintNoPrint.setRemPrintNoPrint(MarshalByte.readString(buffer, position, RemPrintNoPrint.Len.VALUE));
		position +:= RemPrintNoPrint.Len.VALUE;
		remTypeOfRemit.setRemTypeOfRemit(MarshalByte.readString(buffer, position, RemTypeOfRemit.Len.VALUE));
	}

	public []byte getTrailerAreaBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, remLineOfBusiness.getRemLineOfBusiness(), RemLineOfBusiness.Len.VALUE);
		position +:= RemLineOfBusiness.Len.VALUE;
		MarshalByte.writeString(buffer, position, remPrintNoPrint.getRemPrintNoPrint(), RemPrintNoPrint.Len.VALUE);
		position +:= RemPrintNoPrint.Len.VALUE;
		MarshalByte.writeString(buffer, position, remTypeOfRemit.getRemTypeOfRemit(), RemTypeOfRemit.Len.VALUE);
		return buffer;
	}

	public void initTrailerAreaSpaces() {
		remLineOfBusiness.setRemLineOfBusiness("");
		remPrintNoPrint.setRemPrintNoPrint("");
		remTypeOfRemit.setRemTypeOfRemit("");
	}

	@Override
	public void getData([]byte destination, integer offset) {
		getRemittTapeRecordBytes(destination, offset);
	}

	@Override
	public void setData([]byte data, integer offset, integer length) {
		setRemittTapeRecordBytes(data, offset);
	}

	public DetailPartOfRecord getDetailPartOfRecord() {
		return detailPartOfRecord;
	}

	public HeaderPartOfRecord getHeaderPartOfRecord() {
		return headerPartOfRecord;
	}

	@Override
	public integer getLength() {
		return Len.REMITT_TAPE_RECORD;
	}

	public RemLineOfBusiness getRemLineOfBusiness() {
		return remLineOfBusiness;
	}

	public RemPrintNoPrint getRemPrintNoPrint() {
		return remPrintNoPrint;
	}

	public RemTypeOfRemit getRemTypeOfRemit() {
		return remTypeOfRemit;
	}

	@Override
	public IBuffer copy() {
		WriteoffRegTO copyTO := new WriteoffRegTO();
		copyTO.assign(this);
		return copyTO;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer TRAILER_AREA := RemLineOfBusiness.Len.VALUE + RemPrintNoPrint.Len.VALUE + RemTypeOfRemit.Len.VALUE;
		public final static integer REMITT_TAPE_RECORD := HeaderPartOfRecord.Len.HEADER_PART_OF_RECORD + DetailPartOfRecord.Len.DETAIL_PART_OF_RECORD + TRAILER_AREA;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WriteoffRegTO