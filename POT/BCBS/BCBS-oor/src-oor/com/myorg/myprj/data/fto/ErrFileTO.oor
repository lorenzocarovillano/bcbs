package com.myorg.myprj.data.fto;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.copy.ErrLocalBillProvInfo;

/**Original name: ERR-FILE<br>
 * File: ERR-FILE from program NF0533ML<br>
 * Generated as a class for rule FTO.<br>*/
class ErrFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: ERR-HIST-NPI-BILL-PROV-ID
	private string errHistNpiBillProvId := DefaultValues.stringVal(Len.ERR_HIST_NPI_BILL_PROV_ID);
	//Original name: FILLER-ERROR-FILE-LAYOUT
	private char flr1 := DefaultValues.CHAR_VAL;
	//Original name: ERR-LOCAL-BILL-PROV-INFO
	private ErrLocalBillProvInfo errLocalBillProvInfo := new ErrLocalBillProvInfo();
	//Original name: FILLER-ERROR-FILE-LAYOUT-1
	private char flr2 := DefaultValues.CHAR_VAL;
	//Original name: ERR-CLM-CNTL-ID
	private string errClmCntlId := DefaultValues.stringVal(Len.ERR_CLM_CNTL_ID);
	//Original name: ERR-CLM-CNTL-SFX
	private char errClmCntlSfx := DefaultValues.CHAR_VAL;
	//Original name: FILLER-ERROR-FILE-LAYOUT-2
	private char flr3 := DefaultValues.CHAR_VAL;
	//Original name: ERR-CLM-PMT-ID
	private string errClmPmtId := DefaultValues.stringVal(Len.ERR_CLM_PMT_ID);
	//Original name: FILLER-ERROR-FILE-LAYOUT-3
	private char flr4 := DefaultValues.CHAR_VAL;
	//Original name: ERR-INS-ID
	private string errInsId := DefaultValues.stringVal(Len.ERR_INS_ID);
	//Original name: FILLER-ERROR-FILE-LAYOUT-4
	private char flr5 := DefaultValues.CHAR_VAL;
	//Original name: ERR-MSG-TEXT
	private string errMsgText := DefaultValues.stringVal(Len.ERR_MSG_TEXT);
	//Original name: FILLER-ERROR-FILE-LAYOUT-5
	private char flr6 := DefaultValues.CHAR_VAL;
	//Original name: ERR-PGM-NAME
	private string errPgmName := DefaultValues.stringVal(Len.ERR_PGM_NAME);
	//Original name: ERR-PMT-ADJD-DT
	private string errPmtAdjdDt := DefaultValues.stringVal(Len.ERR_PMT_ADJD_DT);
	//Original name: ERR-INFO-SSYST-ID
	private string errInfoSsystId := DefaultValues.stringVal(Len.ERR_INFO_SSYST_ID);
	//Original name: ERR-ASG-CD
	private string errAsgCd := DefaultValues.stringVal(Len.ERR_ASG_CD);
	//Original name: ERR-KCAPS-TEAM-NM
	private string errKcapsTeamNm := DefaultValues.stringVal(Len.ERR_KCAPS_TEAM_NM);
	//Original name: ERR-KCAPS-USE-ID
	private string errKcapsUseId := DefaultValues.stringVal(Len.ERR_KCAPS_USE_ID);
	//Original name: ERR-HIST-LOAD-CD
	private char errHistLoadCd := DefaultValues.CHAR_VAL;
	//Original name: ERR-PMT-BK-PROD-CD
	private char errPmtBkProdCd := DefaultValues.CHAR_VAL;
	//Original name: ERR-PROD-IND
	private string errProdInd := DefaultValues.stringVal(Len.ERR_PROD_IND);
	//Original name: ERR-ADJ-TYP-CD
	private char errAdjTypCd := DefaultValues.CHAR_VAL;
	//Original name: ERR-ITS-CLM-TYP-CD
	private char errItsClmTypCd := DefaultValues.CHAR_VAL;
	//Original name: ERR-CLM-PD-AM
	private decimal(11,2) errClmPdAm := DefaultValues.DEC_VAL;
	//Original name: FILLER-ERROR-FILE-LAYOUT-6
	private string flr7 := DefaultValues.stringVal(Len.FLR7);


	//==== METHODS ====
	/**Original name: ERROR-FILE-LAYOUT<br>
	 * <pre>     INPUT FILE USED TO CREATE CHECKWRITER ERROR REPORT
	 *   --------------------------------------------------</pre>*/
	public []byte getErrorFileLayoutBytes() {
		[]byte buffer := new [Len.ERROR_FILE_LAYOUT]byte;
		return getErrorFileLayoutBytes(buffer, 1);
	}

	public void setErrorFileLayoutBytes([]byte buffer, integer offset) {
		integer position := offset;
		errHistNpiBillProvId := MarshalByte.readString(buffer, position, Len.ERR_HIST_NPI_BILL_PROV_ID);
		position +:= Len.ERR_HIST_NPI_BILL_PROV_ID;
		flr1 := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		errLocalBillProvInfo.setErrLocalBillProvInfoBytes(buffer, position);
		position +:= ErrLocalBillProvInfo.Len.ERR_LOCAL_BILL_PROV_INFO;
		flr2 := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		errClmCntlId := MarshalByte.readString(buffer, position, Len.ERR_CLM_CNTL_ID);
		position +:= Len.ERR_CLM_CNTL_ID;
		errClmCntlSfx := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		flr3 := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		errClmPmtId := MarshalByte.readFixedString(buffer, position, Len.ERR_CLM_PMT_ID);
		position +:= Len.ERR_CLM_PMT_ID;
		flr4 := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		errInsId := MarshalByte.readString(buffer, position, Len.ERR_INS_ID);
		position +:= Len.ERR_INS_ID;
		flr5 := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		errMsgText := MarshalByte.readString(buffer, position, Len.ERR_MSG_TEXT);
		position +:= Len.ERR_MSG_TEXT;
		flr6 := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		errPgmName := MarshalByte.readString(buffer, position, Len.ERR_PGM_NAME);
		position +:= Len.ERR_PGM_NAME;
		errPmtAdjdDt := MarshalByte.readString(buffer, position, Len.ERR_PMT_ADJD_DT);
		position +:= Len.ERR_PMT_ADJD_DT;
		errInfoSsystId := MarshalByte.readString(buffer, position, Len.ERR_INFO_SSYST_ID);
		position +:= Len.ERR_INFO_SSYST_ID;
		errAsgCd := MarshalByte.readString(buffer, position, Len.ERR_ASG_CD);
		position +:= Len.ERR_ASG_CD;
		errKcapsTeamNm := MarshalByte.readString(buffer, position, Len.ERR_KCAPS_TEAM_NM);
		position +:= Len.ERR_KCAPS_TEAM_NM;
		errKcapsUseId := MarshalByte.readString(buffer, position, Len.ERR_KCAPS_USE_ID);
		position +:= Len.ERR_KCAPS_USE_ID;
		errHistLoadCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		errPmtBkProdCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		errProdInd := MarshalByte.readString(buffer, position, Len.ERR_PROD_IND);
		position +:= Len.ERR_PROD_IND;
		errAdjTypCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		errItsClmTypCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		errClmPdAm := MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ERR_CLM_PD_AM, Len.Fract.ERR_CLM_PD_AM);
		position +:= Len.ERR_CLM_PD_AM;
		flr7 := MarshalByte.readString(buffer, position, Len.FLR7);
	}

	public []byte getErrorFileLayoutBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, errHistNpiBillProvId, Len.ERR_HIST_NPI_BILL_PROV_ID);
		position +:= Len.ERR_HIST_NPI_BILL_PROV_ID;
		MarshalByte.writeChar(buffer, position, flr1);
		position +:= Types.CHAR_SIZE;
		errLocalBillProvInfo.getErrLocalBillProvInfoBytes(buffer, position);
		position +:= ErrLocalBillProvInfo.Len.ERR_LOCAL_BILL_PROV_INFO;
		MarshalByte.writeChar(buffer, position, flr2);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, errClmCntlId, Len.ERR_CLM_CNTL_ID);
		position +:= Len.ERR_CLM_CNTL_ID;
		MarshalByte.writeChar(buffer, position, errClmCntlSfx);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, flr3);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, errClmPmtId, Len.ERR_CLM_PMT_ID);
		position +:= Len.ERR_CLM_PMT_ID;
		MarshalByte.writeChar(buffer, position, flr4);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, errInsId, Len.ERR_INS_ID);
		position +:= Len.ERR_INS_ID;
		MarshalByte.writeChar(buffer, position, flr5);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, errMsgText, Len.ERR_MSG_TEXT);
		position +:= Len.ERR_MSG_TEXT;
		MarshalByte.writeChar(buffer, position, flr6);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, errPgmName, Len.ERR_PGM_NAME);
		position +:= Len.ERR_PGM_NAME;
		MarshalByte.writeString(buffer, position, errPmtAdjdDt, Len.ERR_PMT_ADJD_DT);
		position +:= Len.ERR_PMT_ADJD_DT;
		MarshalByte.writeString(buffer, position, errInfoSsystId, Len.ERR_INFO_SSYST_ID);
		position +:= Len.ERR_INFO_SSYST_ID;
		MarshalByte.writeString(buffer, position, errAsgCd, Len.ERR_ASG_CD);
		position +:= Len.ERR_ASG_CD;
		MarshalByte.writeString(buffer, position, errKcapsTeamNm, Len.ERR_KCAPS_TEAM_NM);
		position +:= Len.ERR_KCAPS_TEAM_NM;
		MarshalByte.writeString(buffer, position, errKcapsUseId, Len.ERR_KCAPS_USE_ID);
		position +:= Len.ERR_KCAPS_USE_ID;
		MarshalByte.writeChar(buffer, position, errHistLoadCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, errPmtBkProdCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, errProdInd, Len.ERR_PROD_IND);
		position +:= Len.ERR_PROD_IND;
		MarshalByte.writeChar(buffer, position, errAdjTypCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, errItsClmTypCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeDecimalAsPacked(buffer, position, errClmPdAm);
		position +:= Len.ERR_CLM_PD_AM;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		return buffer;
	}

	public void initErrorFileLayoutSpaces() {
		errHistNpiBillProvId := "";
		flr1 := Types.SPACE_CHAR;
		errLocalBillProvInfo.initErrLocalBillProvInfoSpaces();
		flr2 := Types.SPACE_CHAR;
		errClmCntlId := "";
		errClmCntlSfx := Types.SPACE_CHAR;
		flr3 := Types.SPACE_CHAR;
		errClmPmtId := "";
		flr4 := Types.SPACE_CHAR;
		errInsId := "";
		flr5 := Types.SPACE_CHAR;
		errMsgText := "";
		flr6 := Types.SPACE_CHAR;
		errPgmName := "";
		errPmtAdjdDt := "";
		errInfoSsystId := "";
		errAsgCd := "";
		errKcapsTeamNm := "";
		errKcapsUseId := "";
		errHistLoadCd := Types.SPACE_CHAR;
		errPmtBkProdCd := Types.SPACE_CHAR;
		errProdInd := "";
		errAdjTypCd := Types.SPACE_CHAR;
		errItsClmTypCd := Types.SPACE_CHAR;
		errClmPdAm := OOR_NaN;
		flr7 := "";
	}

	public void setErrHistNpiBillProvId(string errHistNpiBillProvId) {
		this.errHistNpiBillProvId:=Functions.subString(errHistNpiBillProvId, Len.ERR_HIST_NPI_BILL_PROV_ID);
	}

	public string getErrHistNpiBillProvId() {
		return this.errHistNpiBillProvId;
	}

	public void setFlr1(char flr1) {
		this.flr1:=flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setFlr2(char flr2) {
		this.flr2:=flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setErrClmCntlId(string errClmCntlId) {
		this.errClmCntlId:=Functions.subString(errClmCntlId, Len.ERR_CLM_CNTL_ID);
	}

	public string getErrClmCntlId() {
		return this.errClmCntlId;
	}

	public void setErrClmCntlSfx(char errClmCntlSfx) {
		this.errClmCntlSfx:=errClmCntlSfx;
	}

	public char getErrClmCntlSfx() {
		return this.errClmCntlSfx;
	}

	public void setFlr3(char flr3) {
		this.flr3:=flr3;
	}

	public char getFlr3() {
		return this.flr3;
	}

	public void setErrClmPmtId(short errClmPmtId) {
		this.errClmPmtId := NumericDisplay.asString(errClmPmtId, Len.ERR_CLM_PMT_ID);
	}

	public short getErrClmPmtId() {
		return NumericDisplay.asShort(this.errClmPmtId);
	}

	public void setFlr4(char flr4) {
		this.flr4:=flr4;
	}

	public char getFlr4() {
		return this.flr4;
	}

	public void setErrInsId(string errInsId) {
		this.errInsId:=Functions.subString(errInsId, Len.ERR_INS_ID);
	}

	public string getErrInsId() {
		return this.errInsId;
	}

	public void setFlr5(char flr5) {
		this.flr5:=flr5;
	}

	public char getFlr5() {
		return this.flr5;
	}

	public void setErrMsgText(string errMsgText) {
		this.errMsgText:=Functions.subString(errMsgText, Len.ERR_MSG_TEXT);
	}

	public string getErrMsgText() {
		return this.errMsgText;
	}

	public void setFlr6(char flr6) {
		this.flr6:=flr6;
	}

	public char getFlr6() {
		return this.flr6;
	}

	public void setErrPgmName(string errPgmName) {
		this.errPgmName:=Functions.subString(errPgmName, Len.ERR_PGM_NAME);
	}

	public string getErrPgmName() {
		return this.errPgmName;
	}

	public void setErrPmtAdjdDt(string errPmtAdjdDt) {
		this.errPmtAdjdDt:=Functions.subString(errPmtAdjdDt, Len.ERR_PMT_ADJD_DT);
	}

	public string getErrPmtAdjdDt() {
		return this.errPmtAdjdDt;
	}

	public void setErrInfoSsystId(string errInfoSsystId) {
		this.errInfoSsystId:=Functions.subString(errInfoSsystId, Len.ERR_INFO_SSYST_ID);
	}

	public string getErrInfoSsystId() {
		return this.errInfoSsystId;
	}

	public void setErrAsgCd(string errAsgCd) {
		this.errAsgCd:=Functions.subString(errAsgCd, Len.ERR_ASG_CD);
	}

	public string getErrAsgCd() {
		return this.errAsgCd;
	}

	public void setErrKcapsTeamNm(string errKcapsTeamNm) {
		this.errKcapsTeamNm:=Functions.subString(errKcapsTeamNm, Len.ERR_KCAPS_TEAM_NM);
	}

	public string getErrKcapsTeamNm() {
		return this.errKcapsTeamNm;
	}

	public void setErrKcapsUseId(string errKcapsUseId) {
		this.errKcapsUseId:=Functions.subString(errKcapsUseId, Len.ERR_KCAPS_USE_ID);
	}

	public string getErrKcapsUseId() {
		return this.errKcapsUseId;
	}

	public void setErrHistLoadCd(char errHistLoadCd) {
		this.errHistLoadCd:=errHistLoadCd;
	}

	public char getErrHistLoadCd() {
		return this.errHistLoadCd;
	}

	public void setErrPmtBkProdCd(char errPmtBkProdCd) {
		this.errPmtBkProdCd:=errPmtBkProdCd;
	}

	public char getErrPmtBkProdCd() {
		return this.errPmtBkProdCd;
	}

	public void setErrProdInd(string errProdInd) {
		this.errProdInd:=Functions.subString(errProdInd, Len.ERR_PROD_IND);
	}

	public string getErrProdInd() {
		return this.errProdInd;
	}

	public void setErrAdjTypCd(char errAdjTypCd) {
		this.errAdjTypCd:=errAdjTypCd;
	}

	public char getErrAdjTypCd() {
		return this.errAdjTypCd;
	}

	public void setErrItsClmTypCd(char errItsClmTypCd) {
		this.errItsClmTypCd:=errItsClmTypCd;
	}

	public char getErrItsClmTypCd() {
		return this.errItsClmTypCd;
	}

	public void setErrClmPdAm(decimal(11,2) errClmPdAm) {
		this.errClmPdAm:=errClmPdAm;
	}

	public decimal(11,2) getErrClmPdAm() {
		return this.errClmPdAm;
	}

	public void setFlr7(string flr7) {
		this.flr7:=Functions.subString(flr7, Len.FLR7);
	}

	public string getFlr7() {
		return this.flr7;
	}

	@Override
	public void getData([]byte destination, integer offset) {
		getErrorFileLayoutBytes(destination, offset);
	}

	@Override
	public void setData([]byte data, integer offset, integer length) {
		setErrorFileLayoutBytes(data, offset);
	}

	public ErrLocalBillProvInfo getErrLocalBillProvInfo() {
		return errLocalBillProvInfo;
	}

	@Override
	public integer getLength() {
		return Len.ERROR_FILE_LAYOUT;
	}

	@Override
	public IBuffer copy() {
		ErrFileTO copyTO := new ErrFileTO();
		copyTO.assign(this);
		return copyTO;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ERR_HIST_NPI_BILL_PROV_ID := 10;
		public final static integer FLR1 := 1;
		public final static integer ERR_CLM_CNTL_ID := 12;
		public final static integer ERR_CLM_CNTL_SFX := 1;
		public final static integer ERR_CLM_PMT_ID := 2;
		public final static integer ERR_INS_ID := 12;
		public final static integer ERR_MSG_TEXT := 50;
		public final static integer ERR_PGM_NAME := 8;
		public final static integer ERR_PMT_ADJD_DT := 10;
		public final static integer ERR_INFO_SSYST_ID := 4;
		public final static integer ERR_ASG_CD := 2;
		public final static integer ERR_KCAPS_TEAM_NM := 5;
		public final static integer ERR_KCAPS_USE_ID := 3;
		public final static integer ERR_HIST_LOAD_CD := 1;
		public final static integer ERR_PMT_BK_PROD_CD := 1;
		public final static integer ERR_PROD_IND := 3;
		public final static integer ERR_ADJ_TYP_CD := 1;
		public final static integer ERR_ITS_CLM_TYP_CD := 1;
		public final static integer ERR_CLM_PD_AM := 6;
		public final static integer FLR7 := 41;
		public final static integer ERROR_FILE_LAYOUT := ERR_HIST_NPI_BILL_PROV_ID + ErrLocalBillProvInfo.Len.ERR_LOCAL_BILL_PROV_INFO + ERR_CLM_CNTL_ID + ERR_CLM_CNTL_SFX + ERR_CLM_PMT_ID + ERR_INS_ID + ERR_MSG_TEXT + ERR_PGM_NAME + ERR_PMT_ADJD_DT + ERR_INFO_SSYST_ID + ERR_ASG_CD + ERR_KCAPS_TEAM_NM + ERR_KCAPS_USE_ID + ERR_HIST_LOAD_CD + ERR_PMT_BK_PROD_CD + ERR_PROD_IND + ERR_ADJ_TYP_CD + ERR_ITS_CLM_TYP_CD + ERR_CLM_PD_AM + 6 * FLR1 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer ERR_CLM_PD_AM := 9;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer ERR_CLM_PD_AM := 2;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//ErrFileTO