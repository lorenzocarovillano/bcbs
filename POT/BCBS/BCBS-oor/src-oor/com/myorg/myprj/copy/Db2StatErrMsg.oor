package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

import com.myorg.myprj.ws.Db2ErrMsgData;

/**Original name: DB2-STAT-ERR-MSG<br>
 * Variable: DB2-STAT-ERR-MSG from copybook NUDBERDD<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Db2StatErrMsg {

	//==== PROPERTIES ====
	//Original name: DB2-ERR-PROG
	private string errProg := "NF0790ML";
	//Original name: DB2-ERR-PARA
	private string errPara := "";
	//Original name: DB2-ERR-LAST-CALL
	private string errLastCall := "";
	//Original name: DB2-ERR-TABLE
	private string errTable := "";
	//Original name: DB2-ERR-KEY
	private string errKey := "";
	//Original name: DB2-ERR-MSG-DATA
	private Db2ErrMsgData errMsgData := new Db2ErrMsgData();
	//Original name: DB2-MSG-LG
	private short msgLg := (short)79;


	//==== METHODS ====
	public void setErrProg(string errProg) {
		this.errProg:=Functions.subString(errProg, Len.ERR_PROG);
	}

	public string getErrProg() {
		return this.errProg;
	}

	public string getErrProgFormatted() {
		return Functions.padBlanks(getErrProg(), Len.ERR_PROG);
	}

	public void setErrPara(string errPara) {
		this.errPara:=Functions.subString(errPara, Len.ERR_PARA);
	}

	public string getErrPara() {
		return this.errPara;
	}

	public string getErrParaFormatted() {
		return Functions.padBlanks(getErrPara(), Len.ERR_PARA);
	}

	public void setErrLastCall(string errLastCall) {
		this.errLastCall:=Functions.subString(errLastCall, Len.ERR_LAST_CALL);
	}

	public string getErrLastCall() {
		return this.errLastCall;
	}

	public string getErrLastCallFormatted() {
		return Functions.padBlanks(getErrLastCall(), Len.ERR_LAST_CALL);
	}

	public void setErrTable(string errTable) {
		this.errTable:=Functions.subString(errTable, Len.ERR_TABLE);
	}

	public string getErrTable() {
		return this.errTable;
	}

	public string getErrTableFormatted() {
		return Functions.padBlanks(getErrTable(), Len.ERR_TABLE);
	}

	public void setErrKey(string errKey) {
		this.errKey:=Functions.subString(errKey, Len.ERR_KEY);
	}

	public string getErrKey() {
		return this.errKey;
	}

	public string getErrKeyFormatted() {
		return Functions.padBlanks(getErrKey(), Len.ERR_KEY);
	}

	public void setMsgLg(short msgLg) {
		this.msgLg:=msgLg;
	}

	public void setMsgLgFromBuffer([]byte buffer) {
		msgLg := MarshalByte.readBinaryShort(buffer, 1);
	}

	public short getMsgLg() {
		return this.msgLg;
	}

	public string getMsgLgFormatted() {
		return PicFormatter.display(new PicParams("S9(4)").setUsage(PicUsage.BINARY)).format(getMsgLg()).toString();
	}

	public Db2ErrMsgData getErrMsgData() {
		return errMsgData;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ERR_PROG := 8;
		public final static integer ERR_KEY := 30;
		public final static integer ERR_TABLE := 27;
		public final static integer ERR_LAST_CALL := 20;
		public final static integer ERR_PARA := 30;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Db2StatErrMsg