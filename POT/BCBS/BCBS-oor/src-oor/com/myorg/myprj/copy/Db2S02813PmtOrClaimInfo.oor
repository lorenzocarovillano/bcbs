package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DB2-S02813-PMT-OR-CLAIM-INFO<br>
 * Variable: DB2-S02813-PMT-OR-CLAIM-INFO from copybook NF05CURS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Db2S02813PmtOrClaimInfo {

	//==== PROPERTIES ====
	//Original name: DB2-PMT-ALPH-PRFX-CD
	private string pmtAlphPrfxCd := DefaultValues.stringVal(Len.PMT_ALPH_PRFX_CD);
	//Original name: DB2-PMT-INS-ID
	private string pmtInsId := DefaultValues.stringVal(Len.PMT_INS_ID);
	//Original name: DB2-PMT-ALT-INS-ID
	private string pmtAltInsId := DefaultValues.stringVal(Len.PMT_ALT_INS_ID);
	//Original name: DB2-PMT-MEM-ID
	private string pmtMemId := DefaultValues.stringVal(Len.PMT_MEM_ID);
	//Original name: DB2-CLM-SYST-VER-ID
	private string clmSystVerId := DefaultValues.stringVal(Len.CLM_SYST_VER_ID);
	//Original name: DB2-PMT-CLM-SYST-VER-ID
	private string pmtClmSystVerId := DefaultValues.stringVal(Len.PMT_CLM_SYST_VER_ID);
	//Original name: DB2-PMT-CORP-RCV-DT
	private string pmtCorpRcvDt := DefaultValues.stringVal(Len.PMT_CORP_RCV_DT);
	//Original name: DB2-PMT-ADJD-DT
	private string pmtAdjdDt := DefaultValues.stringVal(Len.PMT_ADJD_DT);
	//Original name: DB2-PMT-BILL-FRM-DT
	private string pmtBillFrmDt := DefaultValues.stringVal(Len.PMT_BILL_FRM_DT);
	//Original name: DB2-PMT-BILL-THR-DT
	private string pmtBillThrDt := DefaultValues.stringVal(Len.PMT_BILL_THR_DT);
	//Original name: DB2-PMT-ASG-CD
	private string pmtAsgCd := DefaultValues.stringVal(Len.PMT_ASG_CD);
	//Original name: DB2-PMT-ADJ-TYP-CD
	private char pmtAdjTypCd := DefaultValues.CHAR_VAL;
	//Original name: DB2-PMT-CLM-PD-AM
	private decimal(11,2) pmtClmPdAm := DefaultValues.DEC_VAL;
	//Original name: DB2-PMT-KCAPS-TEAM-NM
	private string pmtKcapsTeamNm := DefaultValues.stringVal(Len.PMT_KCAPS_TEAM_NM);
	//Original name: DB2-PMT-KCAPS-USE-ID
	private string pmtKcapsUseId := DefaultValues.stringVal(Len.PMT_KCAPS_USE_ID);
	//Original name: DB2-PMT-HIST-LOAD-CD
	private char pmtHistLoadCd := DefaultValues.CHAR_VAL;
	//Original name: DB2-PMT-PMT-BK-PROD-CD
	private char pmtPmtBkProdCd := DefaultValues.CHAR_VAL;
	//Original name: DB2-PMT-DRG
	private string pmtDrg := DefaultValues.stringVal(Len.PMT_DRG);
	//Original name: DB2-PMT-PAT-ACT-ID
	private string pmtPatActId := DefaultValues.stringVal(Len.PMT_PAT_ACT_ID);
	//Original name: DB2-PMT-PGM-AREA-CD
	private string pmtPgmAreaCd := DefaultValues.stringVal(Len.PMT_PGM_AREA_CD);
	//Original name: DB2-PMT-FIN-CD
	private string pmtFinCd := DefaultValues.stringVal(Len.PMT_FIN_CD);
	//Original name: DB2-PMT-ADJD-PROV-STAT-CD
	private char pmtAdjdProvStatCd := DefaultValues.CHAR_VAL;
	//Original name: DB2-PMT-PRMPT-PAY-DAY
	private string pmtPrmptPayDay := DefaultValues.stringVal(Len.PMT_PRMPT_PAY_DAY);
	//Original name: DB2-PMT-PRMPT-PAY-OVRD
	private char pmtPrmptPayOvrd := DefaultValues.CHAR_VAL;
	//Original name: DB2-PMT-BAL-BILL-AM
	private decimal(11,2) pmtBalBillAm := DefaultValues.DEC_VAL;
	//Original name: DB2-PMT-ITS-CLM-TYP-CD
	private char pmtItsClmTypCd := DefaultValues.CHAR_VAL;
	//Original name: DB2-PMT-GRP-ID
	private string pmtGrpId := DefaultValues.stringVal(Len.PMT_GRP_ID);
	//Original name: DB2-PMT-NPI-CD
	private char pmtNpiCd := DefaultValues.CHAR_VAL;
	//Original name: DB2-PMT-LOB-CD
	private char pmtLobCd := DefaultValues.CHAR_VAL;
	//Original name: DB2-PMT-TYP-GRP-CD
	private string pmtTypGrpCd := DefaultValues.stringVal(Len.PMT_TYP_GRP_CD);
	//Original name: DB2-PMT-BEN-PLN-ID
	private string pmtBenPlnId := DefaultValues.stringVal(Len.PMT_BEN_PLN_ID);
	//Original name: DB2-PMT-IRC-CD
	private string pmtIrcCd := DefaultValues.stringVal(Len.PMT_IRC_CD);
	//Original name: DB2-PMT-NTWRK-CD
	private string pmtNtwrkCd := DefaultValues.stringVal(Len.PMT_NTWRK_CD);
	//Original name: DB2-PMT-ADDNL-CN-ARNG
	private string pmtAddnlCnArng := DefaultValues.stringVal(Len.PMT_ADDNL_CN_ARNG);
	//Original name: DB2-PMT-PRIM-CN-ARNG-CD
	private string pmtPrimCnArngCd := DefaultValues.stringVal(Len.PMT_PRIM_CN_ARNG_CD);
	//Original name: DB2-PMT-BASE-CN-ARNG-CD
	private string pmtBaseCnArngCd := DefaultValues.stringVal(Len.PMT_BASE_CN_ARNG_CD);
	//Original name: DB2-PMT-ELIG-INST-REIMB
	private string pmtEligInstReimb := DefaultValues.stringVal(Len.PMT_ELIG_INST_REIMB);
	//Original name: DB2-PMT-ELIG-PROF-REIMB
	private string pmtEligProfReimb := DefaultValues.stringVal(Len.PMT_ELIG_PROF_REIMB);
	//Original name: DB2-PMT-PRC-INST-REIMB
	private string pmtPrcInstReimb := DefaultValues.stringVal(Len.PMT_PRC_INST_REIMB);
	//Original name: DB2-PMT-PRC-PROF-REIMB
	private string pmtPrcProfReimb := DefaultValues.stringVal(Len.PMT_PRC_PROF_REIMB);
	//Original name: DB2-PMT-ENR-CL-CD
	private string pmtEnrClCd := DefaultValues.stringVal(Len.PMT_ENR_CL_CD);
	//Original name: DB2-PMT-POS-CD
	private string pmtPosCd := DefaultValues.stringVal(Len.PMT_POS_CD);
	//Original name: DB2-PMT-TS-CD
	private string pmtTsCd := DefaultValues.stringVal(Len.PMT_TS_CD);
	//Original name: DB2-PMT-RATE-CD
	private string pmtRateCd := DefaultValues.stringVal(Len.PMT_RATE_CD);
	//Original name: DB2-PMT-OI-CD
	private char pmtOiCd := DefaultValues.CHAR_VAL;
	//Original name: DB2-VOID-CD
	private char voidCd := DefaultValues.CHAR_VAL;
	//Original name: DB2-PMT-PROV-TC-CD
	private string pmtProvTcCd := DefaultValues.stringVal(Len.PMT_PROV_TC_CD);
	//Original name: DB2-PMT-CK-ID
	private string pmtCkId := DefaultValues.stringVal(Len.PMT_CK_ID);
	//Original name: DB2-ACR-PRMT-PA-INT-AM
	private decimal(11,2) acrPrmtPaIntAm := DefaultValues.DEC_VAL;
	//Original name: DB2-CLM-INS-LN-CD
	private string clmInsLnCd := DefaultValues.stringVal(Len.CLM_INS_LN_CD);
	//Original name: DB2-PMT-MDGP-PLN-CD
	private char pmtMdgpPlnCd := DefaultValues.CHAR_VAL;
	//Original name: DB2-PMT-CLM-CORP-CODE
	private char pmtClmCorpCode := DefaultValues.CHAR_VAL;
	//Original name: DB2-PMT-MSTR-PLCY-ID
	private string pmtMstrPlcyId := DefaultValues.stringVal(Len.PMT_MSTR_PLCY_ID);
	//Original name: DB2-PA-ID-TO-ACUM-ID
	private string paIdToAcumId := DefaultValues.stringVal(Len.PA_ID_TO_ACUM_ID);
	//Original name: DB2-MEM-ID-TO-ACUM-ID
	private string memIdToAcumId := DefaultValues.stringVal(Len.MEM_ID_TO_ACUM_ID);
	//Original name: DB2-VBR-IN
	private char vbrIn := DefaultValues.CHAR_VAL;
	//Original name: DB2-SPEC-DRUG-CPN-IN
	private char specDrugCpnIn := DefaultValues.CHAR_VAL;


	//==== METHODS ====
	public void setS02813PmtOrClaimInfoBytes([]byte buffer, integer offset) {
		integer position := offset;
		pmtAlphPrfxCd := MarshalByte.readString(buffer, position, Len.PMT_ALPH_PRFX_CD);
		position +:= Len.PMT_ALPH_PRFX_CD;
		pmtInsId := MarshalByte.readString(buffer, position, Len.PMT_INS_ID);
		position +:= Len.PMT_INS_ID;
		pmtAltInsId := MarshalByte.readString(buffer, position, Len.PMT_ALT_INS_ID);
		position +:= Len.PMT_ALT_INS_ID;
		pmtMemId := MarshalByte.readString(buffer, position, Len.PMT_MEM_ID);
		position +:= Len.PMT_MEM_ID;
		clmSystVerId := MarshalByte.readString(buffer, position, Len.CLM_SYST_VER_ID);
		position +:= Len.CLM_SYST_VER_ID;
		pmtClmSystVerId := MarshalByte.readString(buffer, position, Len.PMT_CLM_SYST_VER_ID);
		position +:= Len.PMT_CLM_SYST_VER_ID;
		pmtCorpRcvDt := MarshalByte.readString(buffer, position, Len.PMT_CORP_RCV_DT);
		position +:= Len.PMT_CORP_RCV_DT;
		pmtAdjdDt := MarshalByte.readString(buffer, position, Len.PMT_ADJD_DT);
		position +:= Len.PMT_ADJD_DT;
		pmtBillFrmDt := MarshalByte.readString(buffer, position, Len.PMT_BILL_FRM_DT);
		position +:= Len.PMT_BILL_FRM_DT;
		pmtBillThrDt := MarshalByte.readString(buffer, position, Len.PMT_BILL_THR_DT);
		position +:= Len.PMT_BILL_THR_DT;
		pmtAsgCd := MarshalByte.readString(buffer, position, Len.PMT_ASG_CD);
		position +:= Len.PMT_ASG_CD;
		pmtAdjTypCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pmtClmPdAm := MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PMT_CLM_PD_AM, Len.Fract.PMT_CLM_PD_AM);
		position +:= Len.PMT_CLM_PD_AM;
		pmtKcapsTeamNm := MarshalByte.readString(buffer, position, Len.PMT_KCAPS_TEAM_NM);
		position +:= Len.PMT_KCAPS_TEAM_NM;
		pmtKcapsUseId := MarshalByte.readString(buffer, position, Len.PMT_KCAPS_USE_ID);
		position +:= Len.PMT_KCAPS_USE_ID;
		pmtHistLoadCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pmtPmtBkProdCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pmtDrg := MarshalByte.readString(buffer, position, Len.PMT_DRG);
		position +:= Len.PMT_DRG;
		pmtPatActId := MarshalByte.readString(buffer, position, Len.PMT_PAT_ACT_ID);
		position +:= Len.PMT_PAT_ACT_ID;
		pmtPgmAreaCd := MarshalByte.readString(buffer, position, Len.PMT_PGM_AREA_CD);
		position +:= Len.PMT_PGM_AREA_CD;
		pmtFinCd := MarshalByte.readString(buffer, position, Len.PMT_FIN_CD);
		position +:= Len.PMT_FIN_CD;
		pmtAdjdProvStatCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pmtPrmptPayDay := MarshalByte.readString(buffer, position, Len.PMT_PRMPT_PAY_DAY);
		position +:= Len.PMT_PRMPT_PAY_DAY;
		pmtPrmptPayOvrd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pmtBalBillAm := MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PMT_BAL_BILL_AM, Len.Fract.PMT_BAL_BILL_AM);
		position +:= Len.PMT_BAL_BILL_AM;
		pmtItsClmTypCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pmtGrpId := MarshalByte.readString(buffer, position, Len.PMT_GRP_ID);
		position +:= Len.PMT_GRP_ID;
		pmtNpiCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pmtLobCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pmtTypGrpCd := MarshalByte.readString(buffer, position, Len.PMT_TYP_GRP_CD);
		position +:= Len.PMT_TYP_GRP_CD;
		pmtBenPlnId := MarshalByte.readString(buffer, position, Len.PMT_BEN_PLN_ID);
		position +:= Len.PMT_BEN_PLN_ID;
		pmtIrcCd := MarshalByte.readString(buffer, position, Len.PMT_IRC_CD);
		position +:= Len.PMT_IRC_CD;
		pmtNtwrkCd := MarshalByte.readString(buffer, position, Len.PMT_NTWRK_CD);
		position +:= Len.PMT_NTWRK_CD;
		pmtAddnlCnArng := MarshalByte.readString(buffer, position, Len.PMT_ADDNL_CN_ARNG);
		position +:= Len.PMT_ADDNL_CN_ARNG;
		pmtPrimCnArngCd := MarshalByte.readString(buffer, position, Len.PMT_PRIM_CN_ARNG_CD);
		position +:= Len.PMT_PRIM_CN_ARNG_CD;
		pmtBaseCnArngCd := MarshalByte.readString(buffer, position, Len.PMT_BASE_CN_ARNG_CD);
		position +:= Len.PMT_BASE_CN_ARNG_CD;
		pmtEligInstReimb := MarshalByte.readString(buffer, position, Len.PMT_ELIG_INST_REIMB);
		position +:= Len.PMT_ELIG_INST_REIMB;
		pmtEligProfReimb := MarshalByte.readString(buffer, position, Len.PMT_ELIG_PROF_REIMB);
		position +:= Len.PMT_ELIG_PROF_REIMB;
		pmtPrcInstReimb := MarshalByte.readString(buffer, position, Len.PMT_PRC_INST_REIMB);
		position +:= Len.PMT_PRC_INST_REIMB;
		pmtPrcProfReimb := MarshalByte.readString(buffer, position, Len.PMT_PRC_PROF_REIMB);
		position +:= Len.PMT_PRC_PROF_REIMB;
		pmtEnrClCd := MarshalByte.readString(buffer, position, Len.PMT_ENR_CL_CD);
		position +:= Len.PMT_ENR_CL_CD;
		pmtPosCd := MarshalByte.readString(buffer, position, Len.PMT_POS_CD);
		position +:= Len.PMT_POS_CD;
		pmtTsCd := MarshalByte.readString(buffer, position, Len.PMT_TS_CD);
		position +:= Len.PMT_TS_CD;
		pmtRateCd := MarshalByte.readString(buffer, position, Len.PMT_RATE_CD);
		position +:= Len.PMT_RATE_CD;
		pmtOiCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		voidCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pmtProvTcCd := MarshalByte.readString(buffer, position, Len.PMT_PROV_TC_CD);
		position +:= Len.PMT_PROV_TC_CD;
		pmtCkId := MarshalByte.readString(buffer, position, Len.PMT_CK_ID);
		position +:= Len.PMT_CK_ID;
		acrPrmtPaIntAm := MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ACR_PRMT_PA_INT_AM, Len.Fract.ACR_PRMT_PA_INT_AM);
		position +:= Len.ACR_PRMT_PA_INT_AM;
		clmInsLnCd := MarshalByte.readString(buffer, position, Len.CLM_INS_LN_CD);
		position +:= Len.CLM_INS_LN_CD;
		pmtMdgpPlnCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pmtClmCorpCode := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pmtMstrPlcyId := MarshalByte.readString(buffer, position, Len.PMT_MSTR_PLCY_ID);
		position +:= Len.PMT_MSTR_PLCY_ID;
		paIdToAcumId := MarshalByte.readString(buffer, position, Len.PA_ID_TO_ACUM_ID);
		position +:= Len.PA_ID_TO_ACUM_ID;
		memIdToAcumId := MarshalByte.readString(buffer, position, Len.MEM_ID_TO_ACUM_ID);
		position +:= Len.MEM_ID_TO_ACUM_ID;
		vbrIn := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		specDrugCpnIn := MarshalByte.readChar(buffer, position);
	}

	public []byte getS02813PmtOrClaimInfoBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, pmtAlphPrfxCd, Len.PMT_ALPH_PRFX_CD);
		position +:= Len.PMT_ALPH_PRFX_CD;
		MarshalByte.writeString(buffer, position, pmtInsId, Len.PMT_INS_ID);
		position +:= Len.PMT_INS_ID;
		MarshalByte.writeString(buffer, position, pmtAltInsId, Len.PMT_ALT_INS_ID);
		position +:= Len.PMT_ALT_INS_ID;
		MarshalByte.writeString(buffer, position, pmtMemId, Len.PMT_MEM_ID);
		position +:= Len.PMT_MEM_ID;
		MarshalByte.writeString(buffer, position, clmSystVerId, Len.CLM_SYST_VER_ID);
		position +:= Len.CLM_SYST_VER_ID;
		MarshalByte.writeString(buffer, position, pmtClmSystVerId, Len.PMT_CLM_SYST_VER_ID);
		position +:= Len.PMT_CLM_SYST_VER_ID;
		MarshalByte.writeString(buffer, position, pmtCorpRcvDt, Len.PMT_CORP_RCV_DT);
		position +:= Len.PMT_CORP_RCV_DT;
		MarshalByte.writeString(buffer, position, pmtAdjdDt, Len.PMT_ADJD_DT);
		position +:= Len.PMT_ADJD_DT;
		MarshalByte.writeString(buffer, position, pmtBillFrmDt, Len.PMT_BILL_FRM_DT);
		position +:= Len.PMT_BILL_FRM_DT;
		MarshalByte.writeString(buffer, position, pmtBillThrDt, Len.PMT_BILL_THR_DT);
		position +:= Len.PMT_BILL_THR_DT;
		MarshalByte.writeString(buffer, position, pmtAsgCd, Len.PMT_ASG_CD);
		position +:= Len.PMT_ASG_CD;
		MarshalByte.writeChar(buffer, position, pmtAdjTypCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeDecimalAsPacked(buffer, position, pmtClmPdAm);
		position +:= Len.PMT_CLM_PD_AM;
		MarshalByte.writeString(buffer, position, pmtKcapsTeamNm, Len.PMT_KCAPS_TEAM_NM);
		position +:= Len.PMT_KCAPS_TEAM_NM;
		MarshalByte.writeString(buffer, position, pmtKcapsUseId, Len.PMT_KCAPS_USE_ID);
		position +:= Len.PMT_KCAPS_USE_ID;
		MarshalByte.writeChar(buffer, position, pmtHistLoadCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, pmtPmtBkProdCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pmtDrg, Len.PMT_DRG);
		position +:= Len.PMT_DRG;
		MarshalByte.writeString(buffer, position, pmtPatActId, Len.PMT_PAT_ACT_ID);
		position +:= Len.PMT_PAT_ACT_ID;
		MarshalByte.writeString(buffer, position, pmtPgmAreaCd, Len.PMT_PGM_AREA_CD);
		position +:= Len.PMT_PGM_AREA_CD;
		MarshalByte.writeString(buffer, position, pmtFinCd, Len.PMT_FIN_CD);
		position +:= Len.PMT_FIN_CD;
		MarshalByte.writeChar(buffer, position, pmtAdjdProvStatCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pmtPrmptPayDay, Len.PMT_PRMPT_PAY_DAY);
		position +:= Len.PMT_PRMPT_PAY_DAY;
		MarshalByte.writeChar(buffer, position, pmtPrmptPayOvrd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeDecimalAsPacked(buffer, position, pmtBalBillAm);
		position +:= Len.PMT_BAL_BILL_AM;
		MarshalByte.writeChar(buffer, position, pmtItsClmTypCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pmtGrpId, Len.PMT_GRP_ID);
		position +:= Len.PMT_GRP_ID;
		MarshalByte.writeChar(buffer, position, pmtNpiCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, pmtLobCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pmtTypGrpCd, Len.PMT_TYP_GRP_CD);
		position +:= Len.PMT_TYP_GRP_CD;
		MarshalByte.writeString(buffer, position, pmtBenPlnId, Len.PMT_BEN_PLN_ID);
		position +:= Len.PMT_BEN_PLN_ID;
		MarshalByte.writeString(buffer, position, pmtIrcCd, Len.PMT_IRC_CD);
		position +:= Len.PMT_IRC_CD;
		MarshalByte.writeString(buffer, position, pmtNtwrkCd, Len.PMT_NTWRK_CD);
		position +:= Len.PMT_NTWRK_CD;
		MarshalByte.writeString(buffer, position, pmtAddnlCnArng, Len.PMT_ADDNL_CN_ARNG);
		position +:= Len.PMT_ADDNL_CN_ARNG;
		MarshalByte.writeString(buffer, position, pmtPrimCnArngCd, Len.PMT_PRIM_CN_ARNG_CD);
		position +:= Len.PMT_PRIM_CN_ARNG_CD;
		MarshalByte.writeString(buffer, position, pmtBaseCnArngCd, Len.PMT_BASE_CN_ARNG_CD);
		position +:= Len.PMT_BASE_CN_ARNG_CD;
		MarshalByte.writeString(buffer, position, pmtEligInstReimb, Len.PMT_ELIG_INST_REIMB);
		position +:= Len.PMT_ELIG_INST_REIMB;
		MarshalByte.writeString(buffer, position, pmtEligProfReimb, Len.PMT_ELIG_PROF_REIMB);
		position +:= Len.PMT_ELIG_PROF_REIMB;
		MarshalByte.writeString(buffer, position, pmtPrcInstReimb, Len.PMT_PRC_INST_REIMB);
		position +:= Len.PMT_PRC_INST_REIMB;
		MarshalByte.writeString(buffer, position, pmtPrcProfReimb, Len.PMT_PRC_PROF_REIMB);
		position +:= Len.PMT_PRC_PROF_REIMB;
		MarshalByte.writeString(buffer, position, pmtEnrClCd, Len.PMT_ENR_CL_CD);
		position +:= Len.PMT_ENR_CL_CD;
		MarshalByte.writeString(buffer, position, pmtPosCd, Len.PMT_POS_CD);
		position +:= Len.PMT_POS_CD;
		MarshalByte.writeString(buffer, position, pmtTsCd, Len.PMT_TS_CD);
		position +:= Len.PMT_TS_CD;
		MarshalByte.writeString(buffer, position, pmtRateCd, Len.PMT_RATE_CD);
		position +:= Len.PMT_RATE_CD;
		MarshalByte.writeChar(buffer, position, pmtOiCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, voidCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pmtProvTcCd, Len.PMT_PROV_TC_CD);
		position +:= Len.PMT_PROV_TC_CD;
		MarshalByte.writeString(buffer, position, pmtCkId, Len.PMT_CK_ID);
		position +:= Len.PMT_CK_ID;
		MarshalByte.writeDecimalAsPacked(buffer, position, acrPrmtPaIntAm);
		position +:= Len.ACR_PRMT_PA_INT_AM;
		MarshalByte.writeString(buffer, position, clmInsLnCd, Len.CLM_INS_LN_CD);
		position +:= Len.CLM_INS_LN_CD;
		MarshalByte.writeChar(buffer, position, pmtMdgpPlnCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, pmtClmCorpCode);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pmtMstrPlcyId, Len.PMT_MSTR_PLCY_ID);
		position +:= Len.PMT_MSTR_PLCY_ID;
		MarshalByte.writeString(buffer, position, paIdToAcumId, Len.PA_ID_TO_ACUM_ID);
		position +:= Len.PA_ID_TO_ACUM_ID;
		MarshalByte.writeString(buffer, position, memIdToAcumId, Len.MEM_ID_TO_ACUM_ID);
		position +:= Len.MEM_ID_TO_ACUM_ID;
		MarshalByte.writeChar(buffer, position, vbrIn);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, specDrugCpnIn);
		return buffer;
	}

	public void setPmtAlphPrfxCd(string pmtAlphPrfxCd) {
		this.pmtAlphPrfxCd:=Functions.subString(pmtAlphPrfxCd, Len.PMT_ALPH_PRFX_CD);
	}

	public string getPmtAlphPrfxCd() {
		return this.pmtAlphPrfxCd;
	}

	public void setPmtInsId(string pmtInsId) {
		this.pmtInsId:=Functions.subString(pmtInsId, Len.PMT_INS_ID);
	}

	public string getPmtInsId() {
		return this.pmtInsId;
	}

	public void setPmtAltInsId(string pmtAltInsId) {
		this.pmtAltInsId:=Functions.subString(pmtAltInsId, Len.PMT_ALT_INS_ID);
	}

	public string getPmtAltInsId() {
		return this.pmtAltInsId;
	}

	public void setPmtMemId(string pmtMemId) {
		this.pmtMemId:=Functions.subString(pmtMemId, Len.PMT_MEM_ID);
	}

	public string getPmtMemId() {
		return this.pmtMemId;
	}

	public void setClmSystVerId(string clmSystVerId) {
		this.clmSystVerId:=Functions.subString(clmSystVerId, Len.CLM_SYST_VER_ID);
	}

	public string getClmSystVerId() {
		return this.clmSystVerId;
	}

	public void setPmtClmSystVerId(string pmtClmSystVerId) {
		this.pmtClmSystVerId:=Functions.subString(pmtClmSystVerId, Len.PMT_CLM_SYST_VER_ID);
	}

	public string getPmtClmSystVerId() {
		return this.pmtClmSystVerId;
	}

	public void setPmtCorpRcvDt(string pmtCorpRcvDt) {
		this.pmtCorpRcvDt:=Functions.subString(pmtCorpRcvDt, Len.PMT_CORP_RCV_DT);
	}

	public string getPmtCorpRcvDt() {
		return this.pmtCorpRcvDt;
	}

	public string getPmtCorpRcvDtFormatted() {
		return Functions.padBlanks(getPmtCorpRcvDt(), Len.PMT_CORP_RCV_DT);
	}

	public void setPmtAdjdDt(string pmtAdjdDt) {
		this.pmtAdjdDt:=Functions.subString(pmtAdjdDt, Len.PMT_ADJD_DT);
	}

	public string getPmtAdjdDt() {
		return this.pmtAdjdDt;
	}

	public string getPmtAdjdDtFormatted() {
		return Functions.padBlanks(getPmtAdjdDt(), Len.PMT_ADJD_DT);
	}

	public void setPmtBillFrmDt(string pmtBillFrmDt) {
		this.pmtBillFrmDt:=Functions.subString(pmtBillFrmDt, Len.PMT_BILL_FRM_DT);
	}

	public string getPmtBillFrmDt() {
		return this.pmtBillFrmDt;
	}

	public string getPmtBillFrmDtFormatted() {
		return Functions.padBlanks(getPmtBillFrmDt(), Len.PMT_BILL_FRM_DT);
	}

	public void setPmtBillThrDt(string pmtBillThrDt) {
		this.pmtBillThrDt:=Functions.subString(pmtBillThrDt, Len.PMT_BILL_THR_DT);
	}

	public string getPmtBillThrDt() {
		return this.pmtBillThrDt;
	}

	public string getPmtBillThrDtFormatted() {
		return Functions.padBlanks(getPmtBillThrDt(), Len.PMT_BILL_THR_DT);
	}

	public void setPmtAsgCd(string pmtAsgCd) {
		this.pmtAsgCd:=Functions.subString(pmtAsgCd, Len.PMT_ASG_CD);
	}

	public string getPmtAsgCd() {
		return this.pmtAsgCd;
	}

	public void setPmtAdjTypCd(char pmtAdjTypCd) {
		this.pmtAdjTypCd:=pmtAdjTypCd;
	}

	public char getPmtAdjTypCd() {
		return this.pmtAdjTypCd;
	}

	public void setPmtClmPdAm(decimal(11,2) pmtClmPdAm) {
		this.pmtClmPdAm:=pmtClmPdAm;
	}

	public decimal(11,2) getPmtClmPdAm() {
		return this.pmtClmPdAm;
	}

	public void setPmtKcapsTeamNm(string pmtKcapsTeamNm) {
		this.pmtKcapsTeamNm:=Functions.subString(pmtKcapsTeamNm, Len.PMT_KCAPS_TEAM_NM);
	}

	public string getPmtKcapsTeamNm() {
		return this.pmtKcapsTeamNm;
	}

	public void setPmtKcapsUseId(string pmtKcapsUseId) {
		this.pmtKcapsUseId:=Functions.subString(pmtKcapsUseId, Len.PMT_KCAPS_USE_ID);
	}

	public string getPmtKcapsUseId() {
		return this.pmtKcapsUseId;
	}

	public void setPmtHistLoadCd(char pmtHistLoadCd) {
		this.pmtHistLoadCd:=pmtHistLoadCd;
	}

	public char getPmtHistLoadCd() {
		return this.pmtHistLoadCd;
	}

	public void setPmtPmtBkProdCd(char pmtPmtBkProdCd) {
		this.pmtPmtBkProdCd:=pmtPmtBkProdCd;
	}

	public char getPmtPmtBkProdCd() {
		return this.pmtPmtBkProdCd;
	}

	public void setPmtDrg(string pmtDrg) {
		this.pmtDrg:=Functions.subString(pmtDrg, Len.PMT_DRG);
	}

	public string getPmtDrg() {
		return this.pmtDrg;
	}

	public void setPmtPatActId(string pmtPatActId) {
		this.pmtPatActId:=Functions.subString(pmtPatActId, Len.PMT_PAT_ACT_ID);
	}

	public string getPmtPatActId() {
		return this.pmtPatActId;
	}

	public void setPmtPgmAreaCd(string pmtPgmAreaCd) {
		this.pmtPgmAreaCd:=Functions.subString(pmtPgmAreaCd, Len.PMT_PGM_AREA_CD);
	}

	public string getPmtPgmAreaCd() {
		return this.pmtPgmAreaCd;
	}

	public void setPmtFinCd(string pmtFinCd) {
		this.pmtFinCd:=Functions.subString(pmtFinCd, Len.PMT_FIN_CD);
	}

	public string getPmtFinCd() {
		return this.pmtFinCd;
	}

	public void setPmtAdjdProvStatCd(char pmtAdjdProvStatCd) {
		this.pmtAdjdProvStatCd:=pmtAdjdProvStatCd;
	}

	public char getPmtAdjdProvStatCd() {
		return this.pmtAdjdProvStatCd;
	}

	public void setPmtPrmptPayDay(string pmtPrmptPayDay) {
		this.pmtPrmptPayDay:=Functions.subString(pmtPrmptPayDay, Len.PMT_PRMPT_PAY_DAY);
	}

	public string getPmtPrmptPayDay() {
		return this.pmtPrmptPayDay;
	}

	public void setPmtPrmptPayOvrd(char pmtPrmptPayOvrd) {
		this.pmtPrmptPayOvrd:=pmtPrmptPayOvrd;
	}

	public char getPmtPrmptPayOvrd() {
		return this.pmtPrmptPayOvrd;
	}

	public void setPmtBalBillAm(decimal(11,2) pmtBalBillAm) {
		this.pmtBalBillAm:=pmtBalBillAm;
	}

	public decimal(11,2) getPmtBalBillAm() {
		return this.pmtBalBillAm;
	}

	public void setPmtItsClmTypCd(char pmtItsClmTypCd) {
		this.pmtItsClmTypCd:=pmtItsClmTypCd;
	}

	public char getPmtItsClmTypCd() {
		return this.pmtItsClmTypCd;
	}

	public void setPmtGrpId(string pmtGrpId) {
		this.pmtGrpId:=Functions.subString(pmtGrpId, Len.PMT_GRP_ID);
	}

	public string getPmtGrpId() {
		return this.pmtGrpId;
	}

	public string getPmtGrpIdFormatted() {
		return Functions.padBlanks(getPmtGrpId(), Len.PMT_GRP_ID);
	}

	public void setPmtNpiCd(char pmtNpiCd) {
		this.pmtNpiCd:=pmtNpiCd;
	}

	public char getPmtNpiCd() {
		return this.pmtNpiCd;
	}

	public void setPmtLobCd(char pmtLobCd) {
		this.pmtLobCd:=pmtLobCd;
	}

	public char getPmtLobCd() {
		return this.pmtLobCd;
	}

	public void setPmtTypGrpCd(string pmtTypGrpCd) {
		this.pmtTypGrpCd:=Functions.subString(pmtTypGrpCd, Len.PMT_TYP_GRP_CD);
	}

	public string getPmtTypGrpCd() {
		return this.pmtTypGrpCd;
	}

	public void setPmtBenPlnId(string pmtBenPlnId) {
		this.pmtBenPlnId:=Functions.subString(pmtBenPlnId, Len.PMT_BEN_PLN_ID);
	}

	public string getPmtBenPlnId() {
		return this.pmtBenPlnId;
	}

	public void setPmtIrcCd(string pmtIrcCd) {
		this.pmtIrcCd:=Functions.subString(pmtIrcCd, Len.PMT_IRC_CD);
	}

	public string getPmtIrcCd() {
		return this.pmtIrcCd;
	}

	public void setPmtNtwrkCd(string pmtNtwrkCd) {
		this.pmtNtwrkCd:=Functions.subString(pmtNtwrkCd, Len.PMT_NTWRK_CD);
	}

	public string getPmtNtwrkCd() {
		return this.pmtNtwrkCd;
	}

	public void setPmtAddnlCnArng(string pmtAddnlCnArng) {
		this.pmtAddnlCnArng:=Functions.subString(pmtAddnlCnArng, Len.PMT_ADDNL_CN_ARNG);
	}

	public string getPmtAddnlCnArng() {
		return this.pmtAddnlCnArng;
	}

	public void setPmtPrimCnArngCd(string pmtPrimCnArngCd) {
		this.pmtPrimCnArngCd:=Functions.subString(pmtPrimCnArngCd, Len.PMT_PRIM_CN_ARNG_CD);
	}

	public string getPmtPrimCnArngCd() {
		return this.pmtPrimCnArngCd;
	}

	public void setPmtBaseCnArngCd(string pmtBaseCnArngCd) {
		this.pmtBaseCnArngCd:=Functions.subString(pmtBaseCnArngCd, Len.PMT_BASE_CN_ARNG_CD);
	}

	public string getPmtBaseCnArngCd() {
		return this.pmtBaseCnArngCd;
	}

	public void setPmtEligInstReimb(string pmtEligInstReimb) {
		this.pmtEligInstReimb:=Functions.subString(pmtEligInstReimb, Len.PMT_ELIG_INST_REIMB);
	}

	public string getPmtEligInstReimb() {
		return this.pmtEligInstReimb;
	}

	public void setPmtEligProfReimb(string pmtEligProfReimb) {
		this.pmtEligProfReimb:=Functions.subString(pmtEligProfReimb, Len.PMT_ELIG_PROF_REIMB);
	}

	public string getPmtEligProfReimb() {
		return this.pmtEligProfReimb;
	}

	public void setPmtPrcInstReimb(string pmtPrcInstReimb) {
		this.pmtPrcInstReimb:=Functions.subString(pmtPrcInstReimb, Len.PMT_PRC_INST_REIMB);
	}

	public string getPmtPrcInstReimb() {
		return this.pmtPrcInstReimb;
	}

	public void setPmtPrcProfReimb(string pmtPrcProfReimb) {
		this.pmtPrcProfReimb:=Functions.subString(pmtPrcProfReimb, Len.PMT_PRC_PROF_REIMB);
	}

	public string getPmtPrcProfReimb() {
		return this.pmtPrcProfReimb;
	}

	public void setPmtEnrClCd(string pmtEnrClCd) {
		this.pmtEnrClCd:=Functions.subString(pmtEnrClCd, Len.PMT_ENR_CL_CD);
	}

	public string getPmtEnrClCd() {
		return this.pmtEnrClCd;
	}

	public void setPmtPosCd(string pmtPosCd) {
		this.pmtPosCd:=Functions.subString(pmtPosCd, Len.PMT_POS_CD);
	}

	public string getPmtPosCd() {
		return this.pmtPosCd;
	}

	public void setPmtTsCd(string pmtTsCd) {
		this.pmtTsCd:=Functions.subString(pmtTsCd, Len.PMT_TS_CD);
	}

	public string getPmtTsCd() {
		return this.pmtTsCd;
	}

	public void setPmtRateCd(string pmtRateCd) {
		this.pmtRateCd:=Functions.subString(pmtRateCd, Len.PMT_RATE_CD);
	}

	public string getPmtRateCd() {
		return this.pmtRateCd;
	}

	public void setPmtOiCd(char pmtOiCd) {
		this.pmtOiCd:=pmtOiCd;
	}

	public char getPmtOiCd() {
		return this.pmtOiCd;
	}

	public void setVoidCd(char voidCd) {
		this.voidCd:=voidCd;
	}

	public char getVoidCd() {
		return this.voidCd;
	}

	public void setPmtProvTcCd(string pmtProvTcCd) {
		this.pmtProvTcCd:=Functions.subString(pmtProvTcCd, Len.PMT_PROV_TC_CD);
	}

	public string getPmtProvTcCd() {
		return this.pmtProvTcCd;
	}

	public void setPmtCkId(string pmtCkId) {
		this.pmtCkId:=Functions.subString(pmtCkId, Len.PMT_CK_ID);
	}

	public string getPmtCkId() {
		return this.pmtCkId;
	}

	public string getPmtCkIdFormatted() {
		return Functions.padBlanks(getPmtCkId(), Len.PMT_CK_ID);
	}

	public void setAcrPrmtPaIntAm(decimal(11,2) acrPrmtPaIntAm) {
		this.acrPrmtPaIntAm:=acrPrmtPaIntAm;
	}

	public decimal(11,2) getAcrPrmtPaIntAm() {
		return this.acrPrmtPaIntAm;
	}

	public void setClmInsLnCd(string clmInsLnCd) {
		this.clmInsLnCd:=Functions.subString(clmInsLnCd, Len.CLM_INS_LN_CD);
	}

	public string getClmInsLnCd() {
		return this.clmInsLnCd;
	}

	public void setPmtMdgpPlnCd(char pmtMdgpPlnCd) {
		this.pmtMdgpPlnCd:=pmtMdgpPlnCd;
	}

	public char getPmtMdgpPlnCd() {
		return this.pmtMdgpPlnCd;
	}

	public void setPmtClmCorpCode(char pmtClmCorpCode) {
		this.pmtClmCorpCode:=pmtClmCorpCode;
	}

	public char getPmtClmCorpCode() {
		return this.pmtClmCorpCode;
	}

	public void setPmtMstrPlcyId(string pmtMstrPlcyId) {
		this.pmtMstrPlcyId:=Functions.subString(pmtMstrPlcyId, Len.PMT_MSTR_PLCY_ID);
	}

	public string getPmtMstrPlcyId() {
		return this.pmtMstrPlcyId;
	}

	public void setPaIdToAcumId(string paIdToAcumId) {
		this.paIdToAcumId:=Functions.subString(paIdToAcumId, Len.PA_ID_TO_ACUM_ID);
	}

	public string getPaIdToAcumId() {
		return this.paIdToAcumId;
	}

	public void setMemIdToAcumId(string memIdToAcumId) {
		this.memIdToAcumId:=Functions.subString(memIdToAcumId, Len.MEM_ID_TO_ACUM_ID);
	}

	public string getMemIdToAcumId() {
		return this.memIdToAcumId;
	}

	public void setVbrIn(char vbrIn) {
		this.vbrIn:=vbrIn;
	}

	public char getVbrIn() {
		return this.vbrIn;
	}

	public void setSpecDrugCpnIn(char specDrugCpnIn) {
		this.specDrugCpnIn:=specDrugCpnIn;
	}

	public char getSpecDrugCpnIn() {
		return this.specDrugCpnIn;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer PMT_ALPH_PRFX_CD := 3;
		public final static integer PMT_INS_ID := 12;
		public final static integer PMT_ALT_INS_ID := 12;
		public final static integer PMT_MEM_ID := 14;
		public final static integer CLM_SYST_VER_ID := 6;
		public final static integer PMT_CLM_SYST_VER_ID := 6;
		public final static integer PMT_CORP_RCV_DT := 10;
		public final static integer PMT_ADJD_DT := 10;
		public final static integer PMT_BILL_FRM_DT := 10;
		public final static integer PMT_BILL_THR_DT := 10;
		public final static integer PMT_ASG_CD := 2;
		public final static integer PMT_KCAPS_TEAM_NM := 5;
		public final static integer PMT_KCAPS_USE_ID := 3;
		public final static integer PMT_DRG := 4;
		public final static integer PMT_PAT_ACT_ID := 25;
		public final static integer PMT_PGM_AREA_CD := 3;
		public final static integer PMT_FIN_CD := 3;
		public final static integer PMT_PRMPT_PAY_DAY := 2;
		public final static integer PMT_GRP_ID := 9;
		public final static integer PMT_TYP_GRP_CD := 3;
		public final static integer PMT_BEN_PLN_ID := 12;
		public final static integer PMT_IRC_CD := 2;
		public final static integer PMT_NTWRK_CD := 3;
		public final static integer PMT_PRIM_CN_ARNG_CD := 5;
		public final static integer PMT_ENR_CL_CD := 3;
		public final static integer PMT_RATE_CD := 2;
		public final static integer PMT_CK_ID := 15;
		public final static integer CLM_INS_LN_CD := 3;
		public final static integer PA_ID_TO_ACUM_ID := 12;
		public final static integer MEM_ID_TO_ACUM_ID := 14;
		public final static integer PMT_ADDNL_CN_ARNG := 5;
		public final static integer PMT_BASE_CN_ARNG_CD := 5;
		public final static integer PMT_ELIG_INST_REIMB := 2;
		public final static integer PMT_ELIG_PROF_REIMB := 2;
		public final static integer PMT_PRC_INST_REIMB := 2;
		public final static integer PMT_PRC_PROF_REIMB := 2;
		public final static integer PMT_POS_CD := 2;
		public final static integer PMT_TS_CD := 2;
		public final static integer PMT_PROV_TC_CD := 3;
		public final static integer PMT_MSTR_PLCY_ID := 12;
		public final static integer PMT_ADJ_TYP_CD := 1;
		public final static integer PMT_CLM_PD_AM := 6;
		public final static integer PMT_HIST_LOAD_CD := 1;
		public final static integer PMT_PMT_BK_PROD_CD := 1;
		public final static integer PMT_ADJD_PROV_STAT_CD := 1;
		public final static integer PMT_PRMPT_PAY_OVRD := 1;
		public final static integer PMT_BAL_BILL_AM := 6;
		public final static integer PMT_ITS_CLM_TYP_CD := 1;
		public final static integer PMT_NPI_CD := 1;
		public final static integer PMT_LOB_CD := 1;
		public final static integer PMT_OI_CD := 1;
		public final static integer VOID_CD := 1;
		public final static integer ACR_PRMT_PA_INT_AM := 6;
		public final static integer PMT_MDGP_PLN_CD := 1;
		public final static integer PMT_CLM_CORP_CODE := 1;
		public final static integer VBR_IN := 1;
		public final static integer SPEC_DRUG_CPN_IN := 1;
		public final static integer S02813_PMT_OR_CLAIM_INFO := PMT_ALPH_PRFX_CD + PMT_INS_ID + PMT_ALT_INS_ID + PMT_MEM_ID + CLM_SYST_VER_ID + PMT_CLM_SYST_VER_ID + PMT_CORP_RCV_DT + PMT_ADJD_DT + PMT_BILL_FRM_DT + PMT_BILL_THR_DT + PMT_ASG_CD + PMT_ADJ_TYP_CD + PMT_CLM_PD_AM + PMT_KCAPS_TEAM_NM + PMT_KCAPS_USE_ID + PMT_HIST_LOAD_CD + PMT_PMT_BK_PROD_CD + PMT_DRG + PMT_PAT_ACT_ID + PMT_PGM_AREA_CD + PMT_FIN_CD + PMT_ADJD_PROV_STAT_CD + PMT_PRMPT_PAY_DAY + PMT_PRMPT_PAY_OVRD + PMT_BAL_BILL_AM + PMT_ITS_CLM_TYP_CD + PMT_GRP_ID + PMT_NPI_CD + PMT_LOB_CD + PMT_TYP_GRP_CD + PMT_BEN_PLN_ID + PMT_IRC_CD + PMT_NTWRK_CD + PMT_ADDNL_CN_ARNG + PMT_PRIM_CN_ARNG_CD + PMT_BASE_CN_ARNG_CD + PMT_ELIG_INST_REIMB + PMT_ELIG_PROF_REIMB + PMT_PRC_INST_REIMB + PMT_PRC_PROF_REIMB + PMT_ENR_CL_CD + PMT_POS_CD + PMT_TS_CD + PMT_RATE_CD + PMT_OI_CD + VOID_CD + PMT_PROV_TC_CD + PMT_CK_ID + ACR_PRMT_PA_INT_AM + CLM_INS_LN_CD + PMT_MDGP_PLN_CD + PMT_CLM_CORP_CODE + PMT_MSTR_PLCY_ID + PA_ID_TO_ACUM_ID + MEM_ID_TO_ACUM_ID + VBR_IN + SPEC_DRUG_CPN_IN;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer PMT_CLM_PD_AM := 9;
			public final static integer PMT_BAL_BILL_AM := 9;
			public final static integer ACR_PRMT_PA_INT_AM := 9;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer PMT_CLM_PD_AM := 2;
			public final static integer PMT_BAL_BILL_AM := 2;
			public final static integer ACR_PRMT_PA_INT_AM := 2;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//Db2S02813PmtOrClaimInfo