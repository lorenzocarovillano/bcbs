package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.copy.Dcls03085sa;
import com.myorg.myprj.copy.Dcls03086sa;
import com.myorg.myprj.copy.Nudberdd;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program NF0790ML<br>
 * Generated as a class for rule WS.<br>*/
public class Nf0790mlData {

	//==== PROPERTIES ====
	//Original name: WS-PGM-NAME
	private string wsPgmName := "NF0790ML";
	//Original name: EOF-PARMS-FILE
	private char eofParmsFile := 'N';
	//Original name: RESTART-DISPLAY-DONE-SW
	private char restartDisplayDoneSw := 'N';
	//Original name: PARM-ERROR-FOUND-SW
	private char parmErrorFoundSw := 'N';
	//Original name: ERROR-FOUND-SW
	private char errorFoundSw := 'N';
	//Original name: WS-SQLCODE-SIGN
	private char wsSqlcodeSign := Types.SPACE_CHAR;
	//Original name: WS-SQLCODE
	private string wsSqlcode := "000000000";
	//Original name: WS-JOB-SEQ-PACKED
	private short wsJobSeqPacked := (short)0;
	//Original name: NUDBERDD
	private Nudberdd nudberdd := new Nudberdd();
	//Original name: DCLS03085SA
	private Dcls03085sa dcls03085sa := new Dcls03085sa();
	//Original name: DCLS03086SA
	private Dcls03086sa dcls03086sa := new Dcls03086sa();


	//==== METHODS ====
	public string getWsPgmName() {
		return this.wsPgmName;
	}

	public string getWsPgmNameFormatted() {
		return Functions.padBlanks(getWsPgmName(), Len.WS_PGM_NAME);
	}

	public void setEofParmsFile(char eofParmsFile) {
		this.eofParmsFile:=eofParmsFile;
	}

	public void setEofParmsFileFormatted(string eofParmsFile) {
		setEofParmsFile(Functions.charAt(eofParmsFile, Types.CHAR_SIZE));
	}

	public char getEofParmsFile() {
		return this.eofParmsFile;
	}

	public void setRestartDisplayDoneSw(char restartDisplayDoneSw) {
		this.restartDisplayDoneSw:=restartDisplayDoneSw;
	}

	public void setRestartDisplayDoneSwFormatted(string restartDisplayDoneSw) {
		setRestartDisplayDoneSw(Functions.charAt(restartDisplayDoneSw, Types.CHAR_SIZE));
	}

	public char getRestartDisplayDoneSw() {
		return this.restartDisplayDoneSw;
	}

	public void setParmErrorFoundSw(char parmErrorFoundSw) {
		this.parmErrorFoundSw:=parmErrorFoundSw;
	}

	public void setParmErrorFoundSwFormatted(string parmErrorFoundSw) {
		setParmErrorFoundSw(Functions.charAt(parmErrorFoundSw, Types.CHAR_SIZE));
	}

	public char getParmErrorFoundSw() {
		return this.parmErrorFoundSw;
	}

	public void setErrorFoundSw(char errorFoundSw) {
		this.errorFoundSw:=errorFoundSw;
	}

	public void setErrorFoundSwFormatted(string errorFoundSw) {
		setErrorFoundSw(Functions.charAt(errorFoundSw, Types.CHAR_SIZE));
	}

	public char getErrorFoundSw() {
		return this.errorFoundSw;
	}

	public string getWsSqlcodeUnpackedFormatted() {
		return MarshalByteExt.bufferToStr(getWsSqlcodeUnpackedBytes());
	}

	/**Original name: WS-SQLCODE-UNPACKED<br>*/
	public []byte getWsSqlcodeUnpackedBytes() {
		[]byte buffer := new [Len.WS_SQLCODE_UNPACKED]byte;
		return getWsSqlcodeUnpackedBytes(buffer, 1);
	}

	public []byte getWsSqlcodeUnpackedBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, wsSqlcodeSign);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, wsSqlcode, Len.WS_SQLCODE);
		return buffer;
	}

	public void setWsSqlcodeSign(char wsSqlcodeSign) {
		this.wsSqlcodeSign:=wsSqlcodeSign;
	}

	public void setWsSqlcodeSignFormatted(string wsSqlcodeSign) {
		setWsSqlcodeSign(Functions.charAt(wsSqlcodeSign, Types.CHAR_SIZE));
	}

	public char getWsSqlcodeSign() {
		return this.wsSqlcodeSign;
	}

	public void setWsSqlcode(integer wsSqlcode) {
		this.wsSqlcode := NumericDisplay.asString(wsSqlcode, Len.WS_SQLCODE);
	}

	public integer getWsSqlcode() {
		return NumericDisplay.asInt(this.wsSqlcode);
	}

	public void setWsJobSeqPacked(short wsJobSeqPacked) {
		this.wsJobSeqPacked:=wsJobSeqPacked;
	}

	public short getWsJobSeqPacked() {
		return this.wsJobSeqPacked;
	}

	public Dcls03085sa getDcls03085sa() {
		return dcls03085sa;
	}

	public Dcls03086sa getDcls03086sa() {
		return dcls03086sa;
	}

	public Nudberdd getNudberdd() {
		return nudberdd;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WS_SQLCODE := 9;
		public final static integer WS_PGM_NAME := 8;
		public final static integer WS_SQLCODE_SIGN := 1;
		public final static integer WS_SQLCODE_UNPACKED := WS_SQLCODE_SIGN + WS_SQLCODE;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Nf0790mlData