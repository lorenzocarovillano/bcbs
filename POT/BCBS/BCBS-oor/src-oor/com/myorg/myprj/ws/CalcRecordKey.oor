package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: CALC-RECORD-KEY<br>
 * Variable: CALC-RECORD-KEY from program NF0735ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CalcRecordKey {

	//==== PROPERTIES ====
	//Original name: WS-CALC-CLM-CNTL-ID
	private string cntlId := "";
	//Original name: WS-CALC-CLM-SFX-ID
	private char sfxId := Types.SPACE_CHAR;
	//Original name: WS-CALC-CLM-PMT-NUM
	private short pmtNum := 0;
	//Original name: WS-CALC-CLM-LI-ID-PACKED
	private short liIdPacked := 0;


	//==== METHODS ====
	public void setCntlId(string cntlId) {
		this.cntlId:=Functions.subString(cntlId, Len.CNTL_ID);
	}

	public string getCntlId() {
		return this.cntlId;
	}

	public string getCntlIdFormatted() {
		return Functions.padBlanks(getCntlId(), Len.CNTL_ID);
	}

	public void setSfxId(char sfxId) {
		this.sfxId:=sfxId;
	}

	public char getSfxId() {
		return this.sfxId;
	}

	public void setPmtNum(short pmtNum) {
		this.pmtNum:=pmtNum;
	}

	public short getPmtNum() {
		return this.pmtNum;
	}

	public string getPmtNumFormatted() {
		return PicFormatter.display(new PicParams("S9(2)V").setUsage(PicUsage.PACKED)).format(getPmtNum()).toString();
	}

	public string getPmtNumAsString() {
		return getPmtNumFormatted();
	}

	public void setLiIdPacked(short liIdPacked) {
		this.liIdPacked:=liIdPacked;
	}

	public short getLiIdPacked() {
		return this.liIdPacked;
	}

	public string getLiIdPackedFormatted() {
		return PicFormatter.display(new PicParams("S9(3)").setUsage(PicUsage.PACKED)).format(getLiIdPacked()).toString();
	}

	public string getLiIdPackedAsString() {
		return getLiIdPackedFormatted();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer CNTL_ID := 12;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//CalcRecordKey