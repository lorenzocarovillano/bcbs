@echo off
rem -------------------------------------------------------------------------
rem OOWB compiler runer
rem -------------------------------------------------------------------------

rem Compiler command line path
rem -------------------------------------------------------------------------
set CMDLINE=C:\ctu-eclipse\plugins\com.bphx.odf.compiler.v32_2.6.0.202102012328\generator\bin.x86\CmdLineCompiler.exe
set WORKPATH=C:\ctu-eclipse\workspace\BCBS\BCBS-oor\bin-oor
set MANIFESTFILE=XBuild20210421_110120849_P0002.manifest
set SYSTEMFILEPATH=C:\ctu-eclipse\plugins\com.bphx.odf.compiler.v32_2.6.0.202102012328\generator\HibernateSupport.xml

rem These are the original command line args as called from OOWB. Uncomment if you want
rem -------------------------------------------------------------------------
rem set LOGPATH=..\.log\P0002
rem set OUTPATH=..\..\BCBS-j\src\main\java

rem Simplified running args, recommanded to run in bin-oor folder
rem -------------------------------------------------------------------------
set LOGPATH=_log
set OUTPATH=_out

echo ===============================================================================
echo.
echo   OOWB compiler runer...
echo    Running from OOWB returned code: 0
echo    Running from OOWB with:
echo      compiler ID: DEVELOPMENT
echo      compiler Version: 4100, Built on  at 
echo.
echo   CMDLINE: %CMDLINE%
echo   WORKPATH: %WORKPATH%
echo.
echo   LOGPATH: %LOGPATH%
echo   OUTPATH: %OUTPATH%
echo.
echo   SYSTEMFILEPATH: %SYSTEMFILEPATH%
echo   MANIFESTFILE: %MANIFESTFILE%
echo.
echo ===============================================================================
echo.
@echo on
%CMDLINE%  -T java -C client_build hibernate_support -L %LOGPATH% -O %OUTPATH% -W %WORKPATH% -M %MANIFESTFILE%
@echo off
PAUSE
