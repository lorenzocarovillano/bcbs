CREATE TABLE S02801SA
   ( CLM_CNTL_ID                    CHAR(12) NOT NULL,
     CLM_CNTL_SFX_ID                CHAR(1) NOT NULL,
     CLM_PMT_ID                     DECIMAL(2, 0) NOT NULL,
     INFO_CHG_TS                    TIMESTAMP NOT NULL,
     HCC_STAT_CATG_CD               CHAR(3) NOT NULL,
     FNL_BUS_DT                     DATE NOT NULL,
     INFO_SSYST_ID                  CHAR(4) NOT NULL,
     INFO_INSRT_PGM_NM              CHAR(8) NOT NULL,
     INFO_CHG_ID                    CHAR(8) NOT NULL
   )