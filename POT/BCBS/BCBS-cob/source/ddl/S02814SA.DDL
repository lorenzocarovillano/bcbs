CREATE TABLE S02814SA
   ( CLM_CNTL_ID                    CHAR(12) NOT NULL,
     CLM_CNTL_SFX_ID                CHAR(1) NOT NULL,
     CLM_PMT_ID                     DECIMAL(2, 0) NOT NULL,
     INFO_CHG_TS                    TIMESTAMP NOT NULL,
     EVNT_LOG_STAT_CD               CHAR(15) NOT NULL,
     EVNT_LOG_BUS_DT                DATE NOT NULL,
     EVNT_LOG_SYST_DT               DATE NOT NULL,
     INFO_SSYST_ID                  CHAR(4) NOT NULL,
     INFO_INSRT_PGM_NM              CHAR(8) NOT NULL,
     KCAPS_TEAM_NM                  CHAR(5) NOT NULL,
     KCAPS_USE_ID                   CHAR(3) NOT NULL,
     INFO_CHG_ID                    CHAR(8) NOT NULL
   )