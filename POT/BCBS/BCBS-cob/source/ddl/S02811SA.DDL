CREATE TABLE S02811SA
   ( CLM_CNTL_ID                    CHAR(12) NOT NULL,
     CLM_CNTL_SFX_ID                CHAR(1) NOT NULL,
     CLM_PMT_ID                     DECIMAL(2, 0) NOT NULL,
     CLI_ID                         DECIMAL(3, 0) NOT NULL,
     CALC_TYP_CD                    CHAR(1) NOT NULL,
     BEN_PRD_CD                     CHAR(1) NOT NULL,
     COV_LOB_CD                     CHAR(1) NOT NULL,
     CALC_EXPLN_CD                  CHAR(1) NOT NULL,
     PAY_CD                         CHAR(1) NOT NULL,
     INS_TC_CD                      CHAR(2) NOT NULL,
     WTHLD_DSC_CD                   CHAR(1) NOT NULL,
     ALW_CHG_AM                     DECIMAL(11, 2) NOT NULL,
     PD_AM                          DECIMAL(11, 2) NOT NULL,
     ALW_CHG_MAX_AM                 DECIMAL(11, 2) NOT NULL,
     INCNTV_AM                      DECIMAL(11, 2) NOT NULL,
     WTHLD_AM                       DECIMAL(11, 2) NOT NULL,
     COINS_PC                       DECIMAL(3, 0) NOT NULL,
     INCNTV_PC                      DECIMAL(7, 6) NOT NULL,
     CORP_CD                        CHAR(1) NOT NULL,
     PROD_CD                        CHAR(2) NOT NULL,
     TYP_BEN_CD                     CHAR(3) NOT NULL,
     SPECI_BEN_CD                   CHAR(4) NOT NULL,
     GL_ACCT_ID                     CHAR(8) NOT NULL,
     INFO_CHG_ID                    CHAR(8) NOT NULL,
     INFO_CHG_TS                    TIMESTAMP NOT NULL,
     COV_TYP_PRC_CD                 CHAR(1) NOT NULL,
     BEG_ALW_AM                     DECIMAL(11, 2) NOT NULL,
     INS_LN_CD                      CHAR(3) NOT NULL
   )