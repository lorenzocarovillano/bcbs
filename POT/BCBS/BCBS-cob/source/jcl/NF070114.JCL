//NF070114 JOB (D),MSGCLASS=8,PRTY=8,                                   00001                                                        
//             CLASS=9                                                  00003                                                        
//*                                                                     00002                                                        
//*********************************************************************                                                              
//*  THIS JOB CAN BE RESTARTED FROM THE TOP.                          *                                                              
//*                                                                   *                                                              
//*  THIS JOB IS PART OF THE CHECKWRITER PORTION OF THE CLAIMS        *                                                              
//*  PRODUCTION SYSTEM.                                               *                                                              
//*                                                                   *                                                              
//*  THIS IS A BCBS JOB.                                              *                                                              
//*                                                                   *                                                              
//*  IT EXECUTES PROC NF079114.                                       *                                                              
//*                                                                   *                                                              
//*  THE CORRESPONDING TEST JOBS ARE NZ0**60A.                        *                                                              
//*                                                                   *                                                              
//*  THIS JOB PROCESSES THE FIRST LOT OF THE BCBS CLAIMS, WHICH WAS   *                                                              
//*  CREATED FROM NF070110.                                           *                                                              
//*                                                                   *                                                              
//*  THIS JOB ACCESSES THE NECESSARY DB2 TABLES TO GATHER ALL THE     *                                                              
//*  NECESSARY INFORMATION TO PROPERLY REPORT EACH ADJUDICATED CLAIM  *                                                              
//*  THAT MUST BE REPORTED ON A REMIT.                                *                                                              
//*                                                                   *                                                              
//*  THIS JOB CREATES THE FOLLOWING OUTPUT FILES:                     *                                                              
//*    ORIGINAL PAYMENTS / REPAYS                                     *                                                              
//*    VOIDS                                                          *                                                              
//*    GMIS ORIGINAL PAYMENTS / REPAYS                                *                                                              
//*    GMIS VOIDS                                                     *                                                              
//*                                                                   *                                                              
//*********************************************************************                                                              
//*                                                                                                                                  
//JOBLIB   DD DSN=USER.CM30.PRODLIB,DISP=SHR                                                                                         
//         DD DSN=DSSP.DSNLOAD,DISP=SHR                                                                                              
//*                                                                                                                                  
//NF070114 EXEC PROC=NF079114,                                          00004                                                        
//             T='BC',                                                                                                               
//             LOB='BCS',                                                                                                            
//             ENV='',                                                                                                               
//             JOBNAME='NF078084',                                                                                                   
//             PERMLIB='USER.CM30.CARDLIB',                                                                                          
//             LOT='1'                                                                                                               
