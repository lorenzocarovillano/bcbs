      ******************************************************************
      * DCLGEN TABLE(S02814SA)                                         *
      *        LIBRARY(DSSP.ZQBS.COPYLIB(S02814SA))                    *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        APOST                                                   *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE S02814SA TABLE
           ( CLM_CNTL_ID                    CHAR(12) NOT NULL,
             CLM_CNTL_SFX_ID                CHAR(1) NOT NULL,
             CLM_PMT_ID                     DECIMAL(2, 0) NOT NULL,
             INFO_CHG_TS                    TIMESTAMP NOT NULL,
             EVNT_LOG_STAT_CD               CHAR(15) NOT NULL,
             EVNT_LOG_BUS_DT                DATE NOT NULL,
             EVNT_LOG_SYST_DT               DATE NOT NULL,
             INFO_SSYST_ID                  CHAR(4) NOT NULL,
             INFO_INSRT_PGM_NM              CHAR(8) NOT NULL,
             KCAPS_TEAM_NM                  CHAR(5) NOT NULL,
             KCAPS_USE_ID                   CHAR(3) NOT NULL,
             INFO_CHG_ID                    CHAR(8) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE S02814SA                           *
      ******************************************************************
       01  DCLS02814SA.
           10 CLM-CNTL-ID          PIC X(12).
           10 CLM-CNTL-SFX-ID      PIC X(1).
           10 CLM-PMT-ID           PIC S9(2)V USAGE COMP-3.
           10 INFO-CHG-TS          PIC X(26).
           10 EVNT-LOG-STAT-CD     PIC X(15).
           10 EVNT-LOG-BUS-DT      PIC X(10).
           10 EVNT-LOG-SYST-DT     PIC X(10).
           10 INFO-SSYST-ID        PIC X(4).
           10 INFO-INSRT-PGM-NM    PIC X(8).
           10 KCAPS-TEAM-NM        PIC X(5).
           10 KCAPS-USE-ID         PIC X(3).
           10 INFO-CHG-ID          PIC X(8).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 12      *
      ******************************************************************
