      ******************************************************************
      * DCLGEN TABLE(SR01453)                                          *
      *        LIBRARY(DSSA.ZQBS.COPYLIB(SR01453))                     *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        APOST                                                   *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE SR01453 TABLE
           ( PROV_ID                        CHAR(10) NOT NULL,
             PROV_LOB_CD                    CHAR(1) NOT NULL,
             COMM_PAY_IN                    CHAR(1) NOT NULL,
             SSN_ID                         CHAR(9) NOT NULL,
             BRTH_DT                        DATE,
             CNSLT_IN                       CHAR(1) NOT NULL,
             CNSLT_EFF_DT                   DATE,
             CNSLT_TERM_DT                  DATE,
             SEX_CD                         CHAR(1) NOT NULL,
             KS_LCNS_ID                     CHAR(20) NOT NULL,
             KS_LCNS_EXPR_DT                DATE,
             KS_LCNS_EFF_DT                 DATE,
             INFO_CHG_ID                    CHAR(8) NOT NULL,
             INFO_CHG_TS                    TIMESTAMP NOT NULL,
             PROV_PAGE_NO_ID                CHAR(7) NOT NULL,
             PROV_PAGE_AREA_CD              CHAR(3) NOT NULL,
             PROF_SCH_CD                    CHAR(3),
             MED_SCH_GRAD_YR_ID             CHAR(4),
             PROV_RES_NM_CD                 CHAR(4) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE SR01453                            *
      ******************************************************************
       01  DCLSR01453.
           10 PROV-ID              PIC X(10).
           10 PROV-LOB-CD          PIC X(1).
           10 COMM-PAY-IN          PIC X(1).
           10 SSN-ID               PIC X(9).
           10 BRTH-DT              PIC X(10).
           10 CNSLT-IN             PIC X(1).
           10 CNSLT-EFF-DT         PIC X(10).
           10 CNSLT-TERM-DT        PIC X(10).
           10 SEX-CD               PIC X(1).
           10 KS-LCNS-ID           PIC X(20).
           10 KS-LCNS-EXPR-DT      PIC X(10).
           10 KS-LCNS-EFF-DT       PIC X(10).
           10 INFO-CHG-ID          PIC X(8).
           10 INFO-CHG-TS          PIC X(26).
           10 PROV-PAGE-NO-ID      PIC X(7).
           10 PROV-PAGE-AREA-CD    PIC X(3).
           10 PROF-SCH-CD          PIC X(3).
           10 MED-SCH-GRAD-YR-ID   PIC X(4).
           10 PROV-RES-NM-CD       PIC X(4).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 20      *
      ******************************************************************
