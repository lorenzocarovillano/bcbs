      * COPYLIB MEMBER NUDBERDD IS USED IN THE DATA DIVISION OF A       
      * DB2 PROGRAM (ON-LINE OR BATCH) WHEN A STATUS ERROR OCCURS ON    
      * AN SQL CALL.  THE FIELDS PERTINENT TO THAT CALL ARE MOVED TO    
      * THIS HOLD AREA.  THE HOLD AREA IS THEN EITHER INSERTED USING    
      * COPYLIB MEMBER NUDBERPO OR DISPLAYED USING COPYLIB MEMBER       
      * NUDBERPB.  ADDITIONAL IMS FIELDS ARE ALSO INCLUDED FOR DB2      
      * PROGRAMS THAT EXECUTE IN THE IMS ENVIRONMENT.                   
      ******************************************************************
       01  DB2-STAT-ERR-MSG.                                            
           05  DB2-MSG-LL            PIC S9(04) VALUE +1000 USAGE COMP. 
           05  DB2-MSG-ZZ            PIC S9(04) VALUE +0 USAGE COMP.    
           05  DB2-ERR-PROG          PIC X(08) VALUE 'NUIQXXXX'.        
           05  DB2-ERR-PARA          PIC X(30) VALUE SPACES.            
           05  DB2-ERR-LAST-CALL     PIC X(20) VALUE SPACES.            
           05  DB2-ERR-TABLE         PIC X(27) VALUE SPACES.            
           05  DB2-ERR-KEY           PIC X(30) VALUE SPACES.            
           05  FILLER                PIC X(02) VALUE SPACE.             
           05  DB2-ERR-MSG-DATA.                                        
               10  DB2-ERR-MSG-LG    PIC S9(04) VALUE +790 USAGE COMP.  
               10  DB2-ERR-MSG OCCURS 10 TIMES INDEXED BY ERRMSG-INDEX  
                                     PIC X(79).                         
           05  DB2-MSG-LG            PIC S9(04) VALUE +79 USAGE COMP.   
           05  FILLER                PIC X(01) VALUE SPACE.             
           05  DB2-ERR-UID           PIC X(08) VALUE SPACES.            
           05  DB2-ERR-LTERM         PIC X(08) VALUE SPACES.            
           05  DB2-MSGLINE           PIC X(60) VALUE SPACES.            
           05  DB2-ODNUM             PIC X(08) VALUE SPACES.            
                                                                        
       01  XXX-ERR-MSG-DATA.                                            
           05  XXX-ERR-MSG-LG        PIC S9(04) COMP VALUE +624.        
           05  XXX-ERR-MSG           PIC X(78) OCCURS  8 TIMES.         
       01  XXX-MSG-LG                PIC S9(09) COMP VALUE +78.         
       01  DB2-SQLCODE               PIC -999.                          
                                                                        
