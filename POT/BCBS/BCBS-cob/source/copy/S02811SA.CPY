      ******************************************************************
      * DCLGEN TABLE(S02811SA)                                         *
      *        LIBRARY(DSSP.ZQBS.COPYLIB(S02811SA))                    *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        APOST                                                   *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE S02811SA TABLE
           ( CLM_CNTL_ID                    CHAR(12) NOT NULL,
             CLM_CNTL_SFX_ID                CHAR(1) NOT NULL,
             CLM_PMT_ID                     DECIMAL(2, 0) NOT NULL,
             CLI_ID                         DECIMAL(3, 0) NOT NULL,
             CALC_TYP_CD                    CHAR(1) NOT NULL,
             BEN_PRD_CD                     CHAR(1) NOT NULL,
             COV_LOB_CD                     CHAR(1) NOT NULL,
             CALC_EXPLN_CD                  CHAR(1) NOT NULL,
             PAY_CD                         CHAR(1) NOT NULL,
             INS_TC_CD                      CHAR(2) NOT NULL,
             WTHLD_DSC_CD                   CHAR(1) NOT NULL,
             ALW_CHG_AM                     DECIMAL(11, 2) NOT NULL,
             PD_AM                          DECIMAL(11, 2) NOT NULL,
             ALW_CHG_MAX_AM                 DECIMAL(11, 2) NOT NULL,
             INCNTV_AM                      DECIMAL(11, 2) NOT NULL,
             WTHLD_AM                       DECIMAL(11, 2) NOT NULL,
             COINS_PC                       DECIMAL(3, 0) NOT NULL,
             INCNTV_PC                      DECIMAL(7, 6) NOT NULL,
             CORP_CD                        CHAR(1) NOT NULL,
             PROD_CD                        CHAR(2) NOT NULL,
             TYP_BEN_CD                     CHAR(3) NOT NULL,
             SPECI_BEN_CD                   CHAR(4) NOT NULL,
             GL_ACCT_ID                     CHAR(8) NOT NULL,
             INFO_CHG_ID                    CHAR(8) NOT NULL,
             INFO_CHG_TS                    TIMESTAMP NOT NULL,
             COV_TYP_PRC_CD                 CHAR(1) NOT NULL,
             BEG_ALW_AM                     DECIMAL(11, 2) NOT NULL,
             INS_LN_CD                      CHAR(3) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE S02811SA                           *
      ******************************************************************
       01  DCLS02811SA.
           10 CLM-CNTL-ID          PIC X(12).
           10 CLM-CNTL-SFX-ID      PIC X(1).
           10 CLM-PMT-ID           PIC S9(2)V USAGE COMP-3.
           10 CLI-ID               PIC S9(3)V USAGE COMP-3.
           10 CALC-TYP-CD          PIC X(1).
           10 BEN-PRD-CD           PIC X(1).
           10 COV-LOB-CD           PIC X(1).
           10 CALC-EXPLN-CD        PIC X(1).
           10 PAY-CD               PIC X(1).
           10 INS-TC-CD            PIC X(2).
           10 WTHLD-DSC-CD         PIC X(1).
           10 ALW-CHG-AM           PIC S9(9)V9(2) USAGE COMP-3.
           10 PD-AM                PIC S9(9)V9(2) USAGE COMP-3.
           10 ALW-CHG-MAX-AM       PIC S9(9)V9(2) USAGE COMP-3.
           10 INCNTV-AM            PIC S9(9)V9(2) USAGE COMP-3.
           10 WTHLD-AM             PIC S9(9)V9(2) USAGE COMP-3.
           10 COINS-PC             PIC S9(3)V USAGE COMP-3.
           10 INCNTV-PC            PIC S9(1)V9(6) USAGE COMP-3.
           10 CORP-CD              PIC X(1).
           10 PROD-CD              PIC X(2).
           10 TYP-BEN-CD           PIC X(3).
           10 SPECI-BEN-CD         PIC X(4).
           10 GL-ACCT-ID           PIC X(8).
           10 INFO-CHG-ID          PIC X(8).
           10 INFO-CHG-TS          PIC X(26).
           10 COV-TYP-PRC-CD       PIC X(1).
           10 BEG-ALW-AM           PIC S9(9)V9(2) USAGE COMP-3.
           10 INS-LN-CD            PIC X(3).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 28      *
      ******************************************************************
