      ******************************************************************
      * DCLGEN TABLE(S02717SA)                                         *
      *        LIBRARY(DSSP.ZQBS.COPYLIB(S02717SA))                    *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        APOST                                                   *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE S02717SA TABLE
           ( CLM_CNTL_ID                    CHAR(12) NOT NULL,
             CLM_CNTL_SFX_ID                CHAR(1) NOT NULL,
             LOOP_ID                        CHAR(6) NOT NULL,
             LOOP_SEQ_CT                    DECIMAL(4, 0) NOT NULL,
             SEG_ID                         CHAR(3) NOT NULL,
             SEG_SEQ_CT                     DECIMAL(4, 0) NOT NULL,
             PROV_SBMT_LN_NO_ID             DECIMAL(3, 0) NOT NULL,
             PROD_SRV_ID_QLF_CD             CHAR(2) NOT NULL,
             PROD_SRV_ID                    CHAR(48) NOT NULL,
             SV1013_PROC_MOD_CD             CHAR(2) NOT NULL,
             SV1014_PROC_MOD_CD             CHAR(2) NOT NULL,
             SV1015_PROC_MOD_CD             CHAR(2) NOT NULL,
             SV1016_PROC_MOD_CD             CHAR(2) NOT NULL,
             SV1017_DS_TX                   CHAR(80) NOT NULL,
             SV102_MNTRY_AM                 DECIMAL(11, 2) NOT NULL,
             UNIT_BSS_MEAS_CD               CHAR(2) NOT NULL,
             QNTY_CT                        DECIMAL(7, 2) NOT NULL,
             FAC_VL_CD                      CHAR(2) NOT NULL,
             SV1071_DIAG_PT_CD              CHAR(2) NOT NULL,
             SV1072_DIAG_PT_CD              CHAR(2) NOT NULL,
             SV1073_DIAG_PT_CD              CHAR(2) NOT NULL,
             SV1074_DIAG_PT_CD              CHAR(2) NOT NULL,
             SV108_MNTRY_AM                 DECIMAL(11, 2) NOT NULL,
             SV109_YS_NO_CND_CD             CHAR(1) NOT NULL,
             SV111_YS_NO_CND_CD             CHAR(1) NOT NULL,
             SV112_YS_NO_CND_CD             CHAR(1) NOT NULL,
             COPAY_STAT_CD                  CHAR(1) NOT NULL,
             INFO_CHG_ID                    CHAR(8) NOT NULL,
             INFO_CHG_DT                    DATE NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE S02717SA                           *
      ******************************************************************
       01  DCLS02717SA.
           10 CLM-CNTL-ID          PIC X(12).
           10 CLM-CNTL-SFX-ID      PIC X(1).
           10 LOOP-ID              PIC X(6).
           10 LOOP-SEQ-CT          PIC S9(4)V USAGE COMP-3.
           10 SEG-ID               PIC X(3).
           10 SEG-SEQ-CT           PIC S9(4)V USAGE COMP-3.
           10 PROV-SBMT-LN-NO-ID   PIC S9(3)V USAGE COMP-3.
           10 PROD-SRV-ID-QLF-CD   PIC X(2).
           10 PROD-SRV-ID          PIC X(48).
           10 SV1013-PROC-MOD-CD   PIC X(2).
           10 SV1014-PROC-MOD-CD   PIC X(2).
           10 SV1015-PROC-MOD-CD   PIC X(2).
           10 SV1016-PROC-MOD-CD   PIC X(2).
           10 SV1017-DS-TX         PIC X(80).
           10 SV102-MNTRY-AM       PIC S9(9)V9(2) USAGE COMP-3.
           10 UNIT-BSS-MEAS-CD     PIC X(2).
           10 QNTY-CT              PIC S9(5)V9(2) USAGE COMP-3.
           10 FAC-VL-CD            PIC X(2).
           10 SV1071-DIAG-PT-CD    PIC X(2).
           10 SV1072-DIAG-PT-CD    PIC X(2).
           10 SV1073-DIAG-PT-CD    PIC X(2).
           10 SV1074-DIAG-PT-CD    PIC X(2).
           10 SV108-MNTRY-AM       PIC S9(9)V9(2) USAGE COMP-3.
           10 SV109-YS-NO-CND-CD   PIC X(1).
           10 SV111-YS-NO-CND-CD   PIC X(1).
           10 SV112-YS-NO-CND-CD   PIC X(1).
           10 COPAY-STAT-CD        PIC X(1).
           10 INFO-CHG-ID          PIC X(8).
           10 INFO-CHG-DT          PIC X(10).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 29      *
      ******************************************************************
