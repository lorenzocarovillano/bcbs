package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: DETAIL-PART-OF-RECORD<br>
 * Variable: DETAIL-PART-OF-RECORD from copybook NF07RMIT<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class DetailPartOfRecord {

	//==== PROPERTIES ====
	//Original name: REM-TAPE-GRP-ALP-PRFX-CD
	private string tapeGrpAlpPrfxCd := DefaultValues.stringVal(Len.TAPE_GRP_ALP_PRFX_CD);
	//Original name: REM-DETL-ID-NUM
	private string detlIdNum := DefaultValues.stringVal(Len.DETL_ID_NUM);
	//Original name: REM-DETL-SUBSCRIBER-DIGITS
	private string detlSubscriberDigits := DefaultValues.stringVal(Len.DETL_SUBSCRIBER_DIGITS);
	//Original name: REM-DETL-CLAIM-ID
	private string detlClaimId := DefaultValues.stringVal(Len.DETL_CLAIM_ID);
	//Original name: REM-DETL-CLAIM-ID-SUFFIX
	private char detlClaimIdSuffix := DefaultValues.CHAR_VAL;
	//Original name: REM-REL-PAYMENT
	private string relPayment := DefaultValues.stringVal(Len.REL_PAYMENT);
	//Original name: REM-LINE-NUM
	private string lineNum := DefaultValues.stringVal(Len.LINE_NUM);
	//Original name: REM-DETL-PATIENT-NAME
	private RemDetlPatientName detlPatientName := new RemDetlPatientName();
	//Original name: REM-DETL-PATIENT-ACCT
	private string detlPatientAcct := DefaultValues.stringVal(Len.DETL_PATIENT_ACCT);
	//Original name: REM-DETL-POS
	private string detlPos := DefaultValues.stringVal(Len.DETL_POS);
	//Original name: REM-DETL-OTLR
	private char detlOtlr := DefaultValues.CHAR_VAL;
	//Original name: REM-DETL-SERV-DATE
	private RemDetlServDate detlServDate := new RemDetlServDate();
	//Original name: REM-ADJU-DRG-PROC
	private RemAdjuDrgProc adjuDrgProc := new RemAdjuDrgProc();
	//Original name: REM-SUBM-DRG-PROC
	private RemSubmDrgProc submDrgProc := new RemSubmDrgProc();
	//Original name: REM-DETL-DAYS
	private string detlDays := DefaultValues.stringVal(Len.DETL_DAYS);
	//Original name: REM-DETL-TOTAL-CHARGE
	private decimal(9,2) detlTotalCharge := DefaultValues.DEC_VAL;
	//Original name: REM-DETL-CONTINUED
	private RemDetlContinued detlContinued := new RemDetlContinued();


	//==== METHODS ====
	public void setDetailPartOfRecordBytes([]byte buffer, integer offset) {
		integer position := offset;
		setDetlSubscriberIdBytes(buffer, position);
		position +:= Len.DETL_SUBSCRIBER_ID;
		detlSubscriberDigits := MarshalByte.readString(buffer, position, Len.DETL_SUBSCRIBER_DIGITS);
		position +:= Len.DETL_SUBSCRIBER_DIGITS;
		setDetlClaimKeyBytes(buffer, position);
		position +:= Len.DETL_CLAIM_KEY;
		relPayment := MarshalByte.readFixedString(buffer, position, Len.REL_PAYMENT);
		position +:= Len.REL_PAYMENT;
		lineNum := MarshalByte.readFixedString(buffer, position, Len.LINE_NUM);
		position +:= Len.LINE_NUM;
		detlPatientName.setDetlPatientNameBytes(buffer, position);
		position +:= RemDetlPatientName.Len.DETL_PATIENT_NAME;
		detlPatientAcct := MarshalByte.readString(buffer, position, Len.DETL_PATIENT_ACCT);
		position +:= Len.DETL_PATIENT_ACCT;
		detlPos := MarshalByte.readString(buffer, position, Len.DETL_POS);
		position +:= Len.DETL_POS;
		detlOtlr := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		detlServDate.setDetlServDateBytes(buffer, position);
		position +:= RemDetlServDate.Len.DETL_SERV_DATE;
		adjuDrgProc.setAdjuDrgProcBytes(buffer, position);
		position +:= RemAdjuDrgProc.Len.ADJU_DRG_PROC;
		submDrgProc.setSubmDrgProcBytes(buffer, position);
		position +:= RemSubmDrgProc.Len.SUBM_DRG_PROC;
		detlDays := MarshalByte.readFixedString(buffer, position, Len.DETL_DAYS);
		position +:= Len.DETL_DAYS;
		detlTotalCharge := MarshalByte.readDecimal(buffer, position, Len.Int.DETL_TOTAL_CHARGE, Len.Fract.DETL_TOTAL_CHARGE);
		position +:= Len.DETL_TOTAL_CHARGE;
		detlContinued.setDetlContinuedBytes(buffer, position);
	}

	public []byte getDetailPartOfRecordBytes([]byte buffer, integer offset) {
		integer position := offset;
		getDetlSubscriberIdBytes(buffer, position);
		position +:= Len.DETL_SUBSCRIBER_ID;
		MarshalByte.writeString(buffer, position, detlSubscriberDigits, Len.DETL_SUBSCRIBER_DIGITS);
		position +:= Len.DETL_SUBSCRIBER_DIGITS;
		getDetlClaimKeyBytes(buffer, position);
		position +:= Len.DETL_CLAIM_KEY;
		MarshalByte.writeString(buffer, position, relPayment, Len.REL_PAYMENT);
		position +:= Len.REL_PAYMENT;
		MarshalByte.writeString(buffer, position, lineNum, Len.LINE_NUM);
		position +:= Len.LINE_NUM;
		detlPatientName.getDetlPatientNameBytes(buffer, position);
		position +:= RemDetlPatientName.Len.DETL_PATIENT_NAME;
		MarshalByte.writeString(buffer, position, detlPatientAcct, Len.DETL_PATIENT_ACCT);
		position +:= Len.DETL_PATIENT_ACCT;
		MarshalByte.writeString(buffer, position, detlPos, Len.DETL_POS);
		position +:= Len.DETL_POS;
		MarshalByte.writeChar(buffer, position, detlOtlr);
		position +:= Types.CHAR_SIZE;
		detlServDate.getDetlServDateBytes(buffer, position);
		position +:= RemDetlServDate.Len.DETL_SERV_DATE;
		adjuDrgProc.getAdjuDrgProcBytes(buffer, position);
		position +:= RemAdjuDrgProc.Len.ADJU_DRG_PROC;
		submDrgProc.getSubmDrgProcBytes(buffer, position);
		position +:= RemSubmDrgProc.Len.SUBM_DRG_PROC;
		MarshalByte.writeString(buffer, position, detlDays, Len.DETL_DAYS);
		position +:= Len.DETL_DAYS;
		MarshalByte.writeDecimal(buffer, position, detlTotalCharge);
		position +:= Len.DETL_TOTAL_CHARGE;
		detlContinued.getDetlContinuedBytes(buffer, position);
		return buffer;
	}

	public void initDetailPartOfRecordSpaces() {
		initDetlSubscriberIdSpaces();
		detlSubscriberDigits := "";
		initDetlClaimKeySpaces();
		relPayment := "";
		lineNum := "";
		detlPatientName.initDetlPatientNameSpaces();
		detlPatientAcct := "";
		detlPos := "";
		detlOtlr := Types.SPACE_CHAR;
		detlServDate.initDetlServDateSpaces();
		adjuDrgProc.initAdjuDrgProcSpaces();
		submDrgProc.initSubmDrgProcSpaces();
		detlDays := "";
		detlTotalCharge := OOR_NaN;
	}

	public void setDetlSubscriberIdFormatted(string data) {
		[]byte buffer := new [Len.DETL_SUBSCRIBER_ID]byte;
		MarshalByte.writeString(buffer, 1, data, Len.DETL_SUBSCRIBER_ID);
		setDetlSubscriberIdBytes(buffer, 1);
	}

	public void setDetlSubscriberIdBytes([]byte buffer, integer offset) {
		integer position := offset;
		tapeGrpAlpPrfxCd := MarshalByte.readString(buffer, position, Len.TAPE_GRP_ALP_PRFX_CD);
		position +:= Len.TAPE_GRP_ALP_PRFX_CD;
		detlIdNum := MarshalByte.readString(buffer, position, Len.DETL_ID_NUM);
	}

	public []byte getDetlSubscriberIdBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, tapeGrpAlpPrfxCd, Len.TAPE_GRP_ALP_PRFX_CD);
		position +:= Len.TAPE_GRP_ALP_PRFX_CD;
		MarshalByte.writeString(buffer, position, detlIdNum, Len.DETL_ID_NUM);
		return buffer;
	}

	public void initDetlSubscriberIdSpaces() {
		tapeGrpAlpPrfxCd := "";
		detlIdNum := "";
	}

	public void setTapeGrpAlpPrfxCd(string tapeGrpAlpPrfxCd) {
		this.tapeGrpAlpPrfxCd:=Functions.subString(tapeGrpAlpPrfxCd, Len.TAPE_GRP_ALP_PRFX_CD);
	}

	public string getTapeGrpAlpPrfxCd() {
		return this.tapeGrpAlpPrfxCd;
	}

	public void setDetlIdNum(string detlIdNum) {
		this.detlIdNum:=Functions.subString(detlIdNum, Len.DETL_ID_NUM);
	}

	public string getDetlIdNum() {
		return this.detlIdNum;
	}

	public string getDetlIdNumFormatted() {
		return Functions.padBlanks(getDetlIdNum(), Len.DETL_ID_NUM);
	}

	public void setDetlSubscriberDigits(string detlSubscriberDigits) {
		this.detlSubscriberDigits:=Functions.subString(detlSubscriberDigits, Len.DETL_SUBSCRIBER_DIGITS);
	}

	public string getDetlSubscriberDigits() {
		return this.detlSubscriberDigits;
	}

	public void setDetlClaimKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		detlClaimId := MarshalByte.readString(buffer, position, Len.DETL_CLAIM_ID);
		position +:= Len.DETL_CLAIM_ID;
		detlClaimIdSuffix := MarshalByte.readChar(buffer, position);
	}

	public []byte getDetlClaimKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, detlClaimId, Len.DETL_CLAIM_ID);
		position +:= Len.DETL_CLAIM_ID;
		MarshalByte.writeChar(buffer, position, detlClaimIdSuffix);
		return buffer;
	}

	public void initDetlClaimKeySpaces() {
		detlClaimId := "";
		detlClaimIdSuffix := Types.SPACE_CHAR;
	}

	public void setDetlClaimId(string detlClaimId) {
		this.detlClaimId:=Functions.subString(detlClaimId, Len.DETL_CLAIM_ID);
	}

	public string getDetlClaimId() {
		return this.detlClaimId;
	}

	public void setDetlClaimIdSuffix(char detlClaimIdSuffix) {
		this.detlClaimIdSuffix:=detlClaimIdSuffix;
	}

	public char getDetlClaimIdSuffix() {
		return this.detlClaimIdSuffix;
	}

	public void setRelPayment(short relPayment) {
		this.relPayment := NumericDisplay.asString(relPayment, Len.REL_PAYMENT);
	}

	public void setRelPaymentFormatted(string relPayment) {
		this.relPayment:=Trunc.toUnsignedNumeric(relPayment, Len.REL_PAYMENT);
	}

	public short getRelPayment() {
		return NumericDisplay.asShort(this.relPayment);
	}

	public void setLineNum(short lineNum) {
		this.lineNum := NumericDisplay.asString(lineNum, Len.LINE_NUM);
	}

	public void setLineNumFormatted(string lineNum) {
		this.lineNum:=Trunc.toUnsignedNumeric(lineNum, Len.LINE_NUM);
	}

	public short getLineNum() {
		return NumericDisplay.asShort(this.lineNum);
	}

	public void setDetlPatientAcct(string detlPatientAcct) {
		this.detlPatientAcct:=Functions.subString(detlPatientAcct, Len.DETL_PATIENT_ACCT);
	}

	public string getDetlPatientAcct() {
		return this.detlPatientAcct;
	}

	public void setDetlPos(string detlPos) {
		this.detlPos:=Functions.subString(detlPos, Len.DETL_POS);
	}

	public string getDetlPos() {
		return this.detlPos;
	}

	public void setDetlOtlr(char detlOtlr) {
		this.detlOtlr:=detlOtlr;
	}

	public char getDetlOtlr() {
		return this.detlOtlr;
	}

	public void setDetlDaysFormatted(string detlDays) {
		this.detlDays:=Trunc.toUnsignedNumeric(detlDays, Len.DETL_DAYS);
	}

	public short getDetlDays() {
		return NumericDisplay.asShort(this.detlDays);
	}

	public void setDetlTotalCharge(decimal(9,2) detlTotalCharge) {
		this.detlTotalCharge:=detlTotalCharge;
	}

	public decimal(9,2) getDetlTotalCharge() {
		return this.detlTotalCharge;
	}

	public RemAdjuDrgProc getAdjuDrgProc() {
		return adjuDrgProc;
	}

	public RemDetlContinued getDetlContinued() {
		return detlContinued;
	}

	public RemDetlPatientName getDetlPatientName() {
		return detlPatientName;
	}

	public RemDetlServDate getDetlServDate() {
		return detlServDate;
	}

	public RemSubmDrgProc getSubmDrgProc() {
		return submDrgProc;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer TAPE_GRP_ALP_PRFX_CD := 3;
		public final static integer DETL_ID_NUM := 14;
		public final static integer DETL_SUBSCRIBER_ID := TAPE_GRP_ALP_PRFX_CD + DETL_ID_NUM;
		public final static integer DETL_SUBSCRIBER_DIGITS := 14;
		public final static integer DETL_CLAIM_ID := 12;
		public final static integer DETL_CLAIM_ID_SUFFIX := 1;
		public final static integer DETL_CLAIM_KEY := DETL_CLAIM_ID + DETL_CLAIM_ID_SUFFIX;
		public final static integer REL_PAYMENT := 4;
		public final static integer LINE_NUM := 3;
		public final static integer DETL_PATIENT_ACCT := 17;
		public final static integer DETL_POS := 2;
		public final static integer DETL_OTLR := 1;
		public final static integer DETL_DAYS := 3;
		public final static integer DETL_TOTAL_CHARGE := 9;
		public final static integer DETAIL_PART_OF_RECORD := DETL_SUBSCRIBER_ID + DETL_SUBSCRIBER_DIGITS + DETL_CLAIM_KEY + REL_PAYMENT + LINE_NUM + RemDetlPatientName.Len.DETL_PATIENT_NAME + DETL_PATIENT_ACCT + DETL_POS + DETL_OTLR + RemDetlServDate.Len.DETL_SERV_DATE + RemAdjuDrgProc.Len.ADJU_DRG_PROC + RemSubmDrgProc.Len.SUBM_DRG_PROC + DETL_DAYS + DETL_TOTAL_CHARGE + RemDetlContinued.Len.DETL_CONTINUED;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer DETL_TOTAL_CHARGE := 7;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer DETL_TOTAL_CHARGE := 2;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//DetailPartOfRecord