package com.myorg.myprj.copy;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

import com.myorg.myprj.commons.data.to.IS03085sa;

/**Original name: DCLS03085SA<br>
 * Variable: DCLS03085SA from copybook S03085SA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Dcls03085sa implements IS03085sa {

	//==== PROPERTIES ====
	//Original name: JOB-NM
	private string jobNm := DefaultValues.stringVal(Len.JOB_NM);
	//Original name: JBSTP-NM
	private string jbstpNm := DefaultValues.stringVal(Len.JBSTP_NM);
	//Original name: JBSTP-SEQ-ID
	private short jbstpSeqId := DefaultValues.SHORT_VAL;
	//Original name: RSTRT-IN
	private char rstrtIn := DefaultValues.CHAR_VAL;
	//Original name: JBSTP-STRT-TS
	private string jbstpStrtTs := DefaultValues.stringVal(Len.JBSTP_STRT_TS);
	//Original name: JBSTP-END-TS
	private string jbstpEndTs := DefaultValues.stringVal(Len.JBSTP_END_TS);
	//Original name: JBSTP-RUN-CT
	private short jbstpRunCt := DefaultValues.SHORT_VAL;
	//Original name: INFO-CHG-ID
	private string infoChgId := DefaultValues.stringVal(Len.INFO_CHG_ID);
	//Original name: INFO-CHG-TS
	private string infoChgTs := DefaultValues.stringVal(Len.INFO_CHG_TS);


	//==== METHODS ====
	@Override
	public void setJobNm(string jobNm) {
		this.jobNm:=Functions.subString(jobNm, Len.JOB_NM);
	}

	@Override
	public string getJobNm() {
		return this.jobNm;
	}

	public string getJobNmFormatted() {
		return Functions.padBlanks(getJobNm(), Len.JOB_NM);
	}

	@Override
	public void setJbstpNm(string jbstpNm) {
		this.jbstpNm:=Functions.subString(jbstpNm, Len.JBSTP_NM);
	}

	@Override
	public string getJbstpNm() {
		return this.jbstpNm;
	}

	public string getJbstpNmFormatted() {
		return Functions.padBlanks(getJbstpNm(), Len.JBSTP_NM);
	}

	@Override
	public void setJbstpSeqId(short jbstpSeqId) {
		this.jbstpSeqId:=jbstpSeqId;
	}

	@Override
	public short getJbstpSeqId() {
		return this.jbstpSeqId;
	}

	public string getJbstpSeqIdFormatted() {
		return PicFormatter.display(new PicParams("S9(3)V").setUsage(PicUsage.PACKED)).format(getJbstpSeqId()).toString();
	}

	public string getJbstpSeqIdAsString() {
		return getJbstpSeqIdFormatted();
	}

	@Override
	public void setRstrtIn(char rstrtIn) {
		this.rstrtIn:=rstrtIn;
	}

	@Override
	public char getRstrtIn() {
		return this.rstrtIn;
	}

	@Override
	public void setJbstpStrtTs(string jbstpStrtTs) {
		this.jbstpStrtTs:=Functions.subString(jbstpStrtTs, Len.JBSTP_STRT_TS);
	}

	@Override
	public string getJbstpStrtTs() {
		return this.jbstpStrtTs;
	}

	public string getJbstpStrtTsFormatted() {
		return Functions.padBlanks(getJbstpStrtTs(), Len.JBSTP_STRT_TS);
	}

	@Override
	public void setJbstpEndTs(string jbstpEndTs) {
		this.jbstpEndTs:=Functions.subString(jbstpEndTs, Len.JBSTP_END_TS);
	}

	@Override
	public string getJbstpEndTs() {
		return this.jbstpEndTs;
	}

	public string getJbstpEndTsFormatted() {
		return Functions.padBlanks(getJbstpEndTs(), Len.JBSTP_END_TS);
	}

	@Override
	public void setJbstpRunCt(short jbstpRunCt) {
		this.jbstpRunCt:=jbstpRunCt;
	}

	@Override
	public short getJbstpRunCt() {
		return this.jbstpRunCt;
	}

	public string getJbstpRunCtFormatted() {
		return PicFormatter.display(new PicParams("S9(3)V").setUsage(PicUsage.PACKED)).format(getJbstpRunCt()).toString();
	}

	public string getJbstpRunCtAsString() {
		return getJbstpRunCtFormatted();
	}

	@Override
	public void setInfoChgId(string infoChgId) {
		this.infoChgId:=Functions.subString(infoChgId, Len.INFO_CHG_ID);
	}

	@Override
	public string getInfoChgId() {
		return this.infoChgId;
	}

	public string getInfoChgIdFormatted() {
		return Functions.padBlanks(getInfoChgId(), Len.INFO_CHG_ID);
	}

	@Override
	public void setInfoChgTs(string infoChgTs) {
		this.infoChgTs:=Functions.subString(infoChgTs, Len.INFO_CHG_TS);
	}

	@Override
	public string getInfoChgTs() {
		return this.infoChgTs;
	}

	public string getInfoChgTsFormatted() {
		return Functions.padBlanks(getInfoChgTs(), Len.INFO_CHG_TS);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer JOB_NM := 8;
		public final static integer JBSTP_NM := 8;
		public final static integer JBSTP_STRT_TS := 26;
		public final static integer JBSTP_END_TS := 26;
		public final static integer INFO_CHG_ID := 8;
		public final static integer INFO_CHG_TS := 26;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Dcls03085sa