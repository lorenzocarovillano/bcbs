package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.ws.enums.SbrAssignedIndc;
import com.myorg.myprj.ws.enums.SbrHistoryLoadIndc;
import com.myorg.myprj.ws.enums.SbrItsClaimType;

/**Original name: SBR-HEADER-DATA<br>
 * Variable: SBR-HEADER-DATA from copybook NF05SUBR<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class SbrHeaderData {

	//==== PROPERTIES ====
	//Original name: SBR-ASSIGNED-INDC
	private SbrAssignedIndc assignedIndc := new SbrAssignedIndc();
	//Original name: SBR-HISTORY-LOAD-INDC
	private SbrHistoryLoadIndc historyLoadIndc := new SbrHistoryLoadIndc();
	//Original name: SBR-PIP-PROVIDER-INDC
	private char pipProviderIndc := DefaultValues.CHAR_VAL;
	public final static char PIP_PROVIDER := 'Y';
	//Original name: SBR-ITS-CLAIM-TYPE
	private SbrItsClaimType itsClaimType := new SbrItsClaimType();
	//Original name: SBR-EFT-INDC
	private char eftIndc := DefaultValues.CHAR_VAL;
	//Original name: FILLER-SBR-HEADER-DATA
	private string flr1 := DefaultValues.stringVal(Len.FLR1);
	//Original name: SBR-TYP-GRP-CD
	private string typGrpCd := DefaultValues.stringVal(Len.TYP_GRP_CD);
	private final static []string FEP := {"9"," 9","  9"};
	//Original name: SBR-FEP-INDICATOR
	private char fepIndicator := DefaultValues.CHAR_VAL;
	public final static char FEP_INDICATOR_YES := 'Y';
	//Original name: FILLER-SBR-HEADER-DATA-1
	private string flr2 := DefaultValues.stringVal(Len.FLR2);


	//==== METHODS ====
	public void setSbrHeaderDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		assignedIndc.setAssignedIndc(MarshalByte.readString(buffer, position, SbrAssignedIndc.Len.VALUE));
		position +:= SbrAssignedIndc.Len.VALUE;
		historyLoadIndc.setHistoryLoadIndc(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		pipProviderIndc := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		itsClaimType.setItsClaimType(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		eftIndc := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		flr1 := MarshalByte.readString(buffer, position, Len.FLR1);
		position +:= Len.FLR1;
		typGrpCd := MarshalByte.readString(buffer, position, Len.TYP_GRP_CD);
		position +:= Len.TYP_GRP_CD;
		fepIndicator := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		flr2 := MarshalByte.readString(buffer, position, Len.FLR2);
	}

	public []byte getSbrHeaderDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, assignedIndc.getAssignedIndc(), SbrAssignedIndc.Len.VALUE);
		position +:= SbrAssignedIndc.Len.VALUE;
		MarshalByte.writeChar(buffer, position, historyLoadIndc.getHistoryLoadIndc());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, pipProviderIndc);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, itsClaimType.getItsClaimType());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, eftIndc);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, typGrpCd, Len.TYP_GRP_CD);
		position +:= Len.TYP_GRP_CD;
		MarshalByte.writeChar(buffer, position, fepIndicator);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		return buffer;
	}

	public void setPipProviderIndc(char pipProviderIndc) {
		this.pipProviderIndc:=pipProviderIndc;
	}

	public char getPipProviderIndc() {
		return this.pipProviderIndc;
	}

	public void setEftIndc(char eftIndc) {
		this.eftIndc:=eftIndc;
	}

	public char getEftIndc() {
		return this.eftIndc;
	}

	public void setFlr1(string flr1) {
		this.flr1:=Functions.subString(flr1, Len.FLR1);
	}

	public string getFlr1() {
		return this.flr1;
	}

	public void setTypGrpCd(string typGrpCd) {
		this.typGrpCd:=Functions.subString(typGrpCd, Len.TYP_GRP_CD);
	}

	public string getTypGrpCd() {
		return this.typGrpCd;
	}

	public void setFepIndicator(char fepIndicator) {
		this.fepIndicator:=fepIndicator;
	}

	public char getFepIndicator() {
		return this.fepIndicator;
	}

	public void setFlr2(string flr2) {
		this.flr2:=Functions.subString(flr2, Len.FLR2);
	}

	public string getFlr2() {
		return this.flr2;
	}

	public SbrAssignedIndc getAssignedIndc() {
		return assignedIndc;
	}

	public SbrHistoryLoadIndc getHistoryLoadIndc() {
		return historyLoadIndc;
	}

	public SbrItsClaimType getItsClaimType() {
		return itsClaimType;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer PIP_PROVIDER_INDC := 1;
		public final static integer EFT_INDC := 1;
		public final static integer FLR1 := 5;
		public final static integer TYP_GRP_CD := 3;
		public final static integer FEP_INDICATOR := 1;
		public final static integer FLR2 := 17;
		public final static integer SBR_HEADER_DATA := SbrAssignedIndc.Len.VALUE + SbrHistoryLoadIndc.Len.VALUE + PIP_PROVIDER_INDC + SbrItsClaimType.Len.VALUE + EFT_INDC + TYP_GRP_CD + FEP_INDICATOR + FLR1 + FLR2;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//SbrHeaderData