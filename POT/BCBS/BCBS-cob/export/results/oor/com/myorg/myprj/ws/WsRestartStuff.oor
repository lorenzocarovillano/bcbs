package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.ws.enums.RestartIndicator;

/**Original name: WS-RESTART-STUFF<br>
 * Variable: WS-RESTART-STUFF from program NF0533ML<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsRestartStuff {

	//==== PROPERTIES ====
	//Original name: WS-RESTART-JOB-PARMS
	private WsRestartJobParms wsRestartJobParms := new WsRestartJobParms();
	//Original name: WS-RESTART-JOB-SEQ-PACKED
	private short wsRestartJobSeqPacked := (short)0;
	//Original name: RESTART-INDICATOR
	private RestartIndicator restartIndicator := new RestartIndicator();
	//Original name: RESTART-CLAIM-ID
	private string restartClaimId := DefaultValues.stringVal(Len.RESTART_CLAIM_ID);
	//Original name: RESTART-CLAIM-SFX
	private char restartClaimSfx := DefaultValues.CHAR_VAL;
	//Original name: RESTART-PMT-ID
	private string restartPmtId := DefaultValues.stringVal(Len.RESTART_PMT_ID);


	//==== METHODS ====
	public void setWsRestartJobSeqPacked(short wsRestartJobSeqPacked) {
		this.wsRestartJobSeqPacked:=wsRestartJobSeqPacked;
	}

	public short getWsRestartJobSeqPacked() {
		return this.wsRestartJobSeqPacked;
	}

	public void setWsRestartTxt1Formatted(string data) {
		[]byte buffer := new [Len.WS_RESTART_TXT1]byte;
		MarshalByte.writeString(buffer, 1, data, Len.WS_RESTART_TXT1);
		setWsRestartTxt1Bytes(buffer, 1);
	}

	public string getWsRestartTxt1Formatted() {
		return MarshalByteExt.bufferToStr(getWsRestartTxt1Bytes());
	}

	/**Original name: WS-RESTART-TXT1<br>*/
	public []byte getWsRestartTxt1Bytes() {
		[]byte buffer := new [Len.WS_RESTART_TXT1]byte;
		return getWsRestartTxt1Bytes(buffer, 1);
	}

	public void setWsRestartTxt1Bytes([]byte buffer, integer offset) {
		integer position := offset;
		restartIndicator.setRestartIndicator(MarshalByte.readString(buffer, position, RestartIndicator.Len.VALUE));
		position +:= RestartIndicator.Len.VALUE;
		setRestartKeyBytes(buffer, position);
	}

	public []byte getWsRestartTxt1Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, restartIndicator.getRestartIndicator(), RestartIndicator.Len.VALUE);
		position +:= RestartIndicator.Len.VALUE;
		getRestartKeyBytes(buffer, position);
		return buffer;
	}

	public string getRestartKeyFormatted() {
		return MarshalByteExt.bufferToStr(getRestartKeyBytes());
	}

	public void setRestartKeyBytes([]byte buffer) {
		setRestartKeyBytes(buffer, 1);
	}

	/**Original name: RESTART-KEY<br>*/
	public []byte getRestartKeyBytes() {
		[]byte buffer := new [Len.RESTART_KEY]byte;
		return getRestartKeyBytes(buffer, 1);
	}

	public void setRestartKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		restartClaimId := MarshalByte.readString(buffer, position, Len.RESTART_CLAIM_ID);
		position +:= Len.RESTART_CLAIM_ID;
		restartClaimSfx := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		restartPmtId := MarshalByte.readFixedString(buffer, position, Len.RESTART_PMT_ID);
	}

	public []byte getRestartKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, restartClaimId, Len.RESTART_CLAIM_ID);
		position +:= Len.RESTART_CLAIM_ID;
		MarshalByte.writeChar(buffer, position, restartClaimSfx);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, restartPmtId, Len.RESTART_PMT_ID);
		return buffer;
	}

	public void setRestartClaimId(string restartClaimId) {
		this.restartClaimId:=Functions.subString(restartClaimId, Len.RESTART_CLAIM_ID);
	}

	public string getRestartClaimId() {
		return this.restartClaimId;
	}

	public string getRestartClaimIdFormatted() {
		return Functions.padBlanks(getRestartClaimId(), Len.RESTART_CLAIM_ID);
	}

	public void setRestartClaimSfx(char restartClaimSfx) {
		this.restartClaimSfx:=restartClaimSfx;
	}

	public char getRestartClaimSfx() {
		return this.restartClaimSfx;
	}

	public string getRestartPmtIdFormatted() {
		return this.restartPmtId;
	}

	public string getRestartPmtIdAsString() {
		return getRestartPmtIdFormatted();
	}

	public RestartIndicator getRestartIndicator() {
		return restartIndicator;
	}

	public WsRestartJobParms getWsRestartJobParms() {
		return wsRestartJobParms;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer RESTART_CLAIM_ID := 12;
		public final static integer RESTART_PMT_ID := 2;
		public final static integer RESTART_CLAIM_SFX := 1;
		public final static integer RESTART_KEY := RESTART_CLAIM_ID + RESTART_CLAIM_SFX + RESTART_PMT_ID;
		public final static integer WS_RESTART_TXT1 := RestartIndicator.Len.VALUE + RESTART_KEY;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsRestartStuff