package com.myorg.myprj.copy;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.commons.data.to.IS02800sa;

/**Original name: DB2-CAS-TABLE-INFO<br>
 * Variable: DB2-CAS-TABLE-INFO from copybook NF05CURS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Db2CasTableInfo implements IS02800sa {

	//==== PROPERTIES ====
	//Original name: DB2-CLM-ADJ-GRP-CD
	private string clmAdjGrpCd := DefaultValues.stringVal(Len.CLM_ADJ_GRP_CD);
	//Original name: DB2-CLM-ADJ-RSN-CD
	private string clmAdjRsnCd := DefaultValues.stringVal(Len.CLM_ADJ_RSN_CD);
	//Original name: DB2-MNTRY-AM
	private decimal(13,2) mntryAm := DefaultValues.DEC_VAL;


	//==== METHODS ====
	public void setCasTableInfoBytes([]byte buffer, integer offset) {
		integer position := offset;
		clmAdjGrpCd := MarshalByte.readString(buffer, position, Len.CLM_ADJ_GRP_CD);
		position +:= Len.CLM_ADJ_GRP_CD;
		clmAdjRsnCd := MarshalByte.readString(buffer, position, Len.CLM_ADJ_RSN_CD);
		position +:= Len.CLM_ADJ_RSN_CD;
		mntryAm := MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.MNTRY_AM, Len.Fract.MNTRY_AM);
	}

	public []byte getCasTableInfoBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, clmAdjGrpCd, Len.CLM_ADJ_GRP_CD);
		position +:= Len.CLM_ADJ_GRP_CD;
		MarshalByte.writeString(buffer, position, clmAdjRsnCd, Len.CLM_ADJ_RSN_CD);
		position +:= Len.CLM_ADJ_RSN_CD;
		MarshalByte.writeDecimalAsPacked(buffer, position, mntryAm);
		return buffer;
	}

	@Override
	public void setClmAdjGrpCd(string clmAdjGrpCd) {
		this.clmAdjGrpCd:=Functions.subString(clmAdjGrpCd, Len.CLM_ADJ_GRP_CD);
	}

	@Override
	public string getClmAdjGrpCd() {
		return this.clmAdjGrpCd;
	}

	@Override
	public void setClmAdjRsnCd(string clmAdjRsnCd) {
		this.clmAdjRsnCd:=Functions.subString(clmAdjRsnCd, Len.CLM_ADJ_RSN_CD);
	}

	@Override
	public string getClmAdjRsnCd() {
		return this.clmAdjRsnCd;
	}

	@Override
	public void setMntryAm(decimal(13,2) mntryAm) {
		this.mntryAm:=mntryAm;
	}

	@Override
	public decimal(13,2) getMntryAm() {
		return this.mntryAm;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer CLM_ADJ_GRP_CD := 2;
		public final static integer CLM_ADJ_RSN_CD := 5;
		public final static integer MNTRY_AM := 7;
		public final static integer CAS_TABLE_INFO := CLM_ADJ_GRP_CD + CLM_ADJ_RSN_CD + MNTRY_AM;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer MNTRY_AM := 11;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer MNTRY_AM := 2;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//Db2CasTableInfo