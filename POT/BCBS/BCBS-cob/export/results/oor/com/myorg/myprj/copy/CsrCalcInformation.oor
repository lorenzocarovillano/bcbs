package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;

import com.myorg.myprj.ws.occurs.CsrCalcEntry;

/**Original name: CSR-CALC-INFORMATION<br>
 * Variable: CSR-CALC-INFORMATION from copybook NF07AREA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CsrCalcInformation {

	//==== PROPERTIES ====
	public final static integer CALC_ENTRY_MAXOCCURS := 9;
	//Original name: CSR-CALC-ENTRY
	private []CsrCalcEntry calcEntry := new [CALC_ENTRY_MAXOCCURS]CsrCalcEntry;
	/**Original name: CSR-LINE-CALC-EXPLN-CD<br>
	 * <pre>      ----------------------------------------------------
	 *       --         CALC SUMMARIZED FIELDS
	 *       -- LINE-CALC-EXPLN-CD = FIRST NON-BLANK EXPLN-CD FOUND
	 *       -- LINE-CALC-ALW-CHG-AM = FIRST NON-ZERO CALC ALW AMT
	 *       -- LINE-CALC-PD-AM = SUM OF ALL CALC PD-AM
	 *       ----------------------------------------------------</pre>*/
	private char lineCalcExplnCd := Types.SPACE_CHAR;
	//Original name: CSR-LINE-CALC-ALW-CHG-AM
	private decimal(11,2) lineCalcAlwChgAm := 0M;
	//Original name: CSR-LINE-CALC-PD-AM
	private decimal(11,2) lineCalcPdAm := 0M;

	//==== CONSTRUCTORS ====
	public CsrCalcInformation() {
		init();
	}

	//==== METHODS ====
	private void init() {
		for int calcEntryIdx in 1.. CALC_ENTRY_MAXOCCURS 
		do
			calcEntry[calcEntryIdx] := new CsrCalcEntry();
		enddo
	}

	public []byte getCalcInformationBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. CALC_ENTRY_MAXOCCURS 
		do
			calcEntry[idx].getCalcEntryBytes(buffer, position);
			position +:= CsrCalcEntry.Len.CALC_ENTRY;
		enddo
		MarshalByte.writeChar(buffer, position, lineCalcExplnCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeDecimalAsPacked(buffer, position, lineCalcAlwChgAm);
		position +:= Len.LINE_CALC_ALW_CHG_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, lineCalcPdAm);
		return buffer;
	}

	public void initCalcInformationSpaces() {
		for integer idx in 1.. CALC_ENTRY_MAXOCCURS 
		do
			calcEntry[idx].initCalcEntrySpaces();
		enddo
		lineCalcExplnCd := Types.SPACE_CHAR;
		lineCalcAlwChgAm := OOR_NaN;
		lineCalcPdAm := OOR_NaN;
	}

	public void setLineCalcExplnCd(char lineCalcExplnCd) {
		this.lineCalcExplnCd:=lineCalcExplnCd;
	}

	public char getLineCalcExplnCd() {
		return this.lineCalcExplnCd;
	}

	public void setLineCalcAlwChgAm(decimal(11,2) lineCalcAlwChgAm) {
		this.lineCalcAlwChgAm:=lineCalcAlwChgAm;
	}

	public decimal(11,2) getLineCalcAlwChgAm() {
		return this.lineCalcAlwChgAm;
	}

	public void setLineCalcPdAm(decimal(11,2) lineCalcPdAm) {
		this.lineCalcPdAm:=lineCalcPdAm;
	}

	public decimal(11,2) getLineCalcPdAm() {
		return this.lineCalcPdAm;
	}

	public CsrCalcEntry getCalcEntry(integer idx) {
		return calcEntry[idx];
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer LINE_CALC_EXPLN_CD := 1;
		public final static integer LINE_CALC_ALW_CHG_AM := 6;
		public final static integer LINE_CALC_PD_AM := 6;
		public final static integer CALC_INFORMATION := CsrCalcInformation.CALC_ENTRY_MAXOCCURS * CsrCalcEntry.Len.CALC_ENTRY + LINE_CALC_EXPLN_CD + LINE_CALC_ALW_CHG_AM + LINE_CALC_PD_AM;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer LINE_CALC_ALW_CHG_AM := 9;
			public final static integer LINE_CALC_PD_AM := 9;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer LINE_CALC_ALW_CHG_AM := 2;
			public final static integer LINE_CALC_PD_AM := 2;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//CsrCalcInformation