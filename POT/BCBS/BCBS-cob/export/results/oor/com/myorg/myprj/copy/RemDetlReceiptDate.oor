package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: REM-DETL-RECEIPT-DATE<br>
 * Variable: REM-DETL-RECEIPT-DATE from copybook NF07RMIT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class RemDetlReceiptDate {

	//==== PROPERTIES ====
	//Original name: REM-DETL-RECEIPT-CC
	private string cc := DefaultValues.stringVal(Len.CC);
	//Original name: REM-DETL-RECEIPT-YY
	private string yy := DefaultValues.stringVal(Len.YY);
	//Original name: REM-DETL-RECEIPT-MM
	private string mm := DefaultValues.stringVal(Len.MM);
	//Original name: REM-DETL-RECEIPT-DD
	private string dd := DefaultValues.stringVal(Len.DD);


	//==== METHODS ====
	public void setDetlReceiptDateBytes([]byte buffer, integer offset) {
		integer position := offset;
		cc := MarshalByte.readFixedString(buffer, position, Len.CC);
		position +:= Len.CC;
		yy := MarshalByte.readFixedString(buffer, position, Len.YY);
		position +:= Len.YY;
		mm := MarshalByte.readFixedString(buffer, position, Len.MM);
		position +:= Len.MM;
		dd := MarshalByte.readFixedString(buffer, position, Len.DD);
	}

	public []byte getDetlReceiptDateBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, cc, Len.CC);
		position +:= Len.CC;
		MarshalByte.writeString(buffer, position, yy, Len.YY);
		position +:= Len.YY;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position +:= Len.MM;
		MarshalByte.writeString(buffer, position, dd, Len.DD);
		return buffer;
	}

	public void setCcFormatted(string cc) {
		this.cc:=Trunc.toUnsignedNumeric(cc, Len.CC);
	}

	public short getCc() {
		return NumericDisplay.asShort(this.cc);
	}

	public void setYyFormatted(string yy) {
		this.yy:=Trunc.toUnsignedNumeric(yy, Len.YY);
	}

	public short getYy() {
		return NumericDisplay.asShort(this.yy);
	}

	public void setMmFormatted(string mm) {
		this.mm:=Trunc.toUnsignedNumeric(mm, Len.MM);
	}

	public short getMm() {
		return NumericDisplay.asShort(this.mm);
	}

	public void setDdFormatted(string dd) {
		this.dd:=Trunc.toUnsignedNumeric(dd, Len.DD);
	}

	public short getDd() {
		return NumericDisplay.asShort(this.dd);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer CC := 2;
		public final static integer YY := 2;
		public final static integer MM := 2;
		public final static integer DD := 2;
		public final static integer DETL_RECEIPT_DATE := CC + YY + MM + DD;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//RemDetlReceiptDate