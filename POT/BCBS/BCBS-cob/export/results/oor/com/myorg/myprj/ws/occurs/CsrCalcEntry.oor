package com.myorg.myprj.ws.occurs;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: CSR-CALC-ENTRY<br>
 * Variables: CSR-CALC-ENTRY from copybook NF07AREA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class CsrCalcEntry {

	//==== PROPERTIES ====
	//Original name: CSR-CALC-TYP-CD
	private char calcTypCd := Types.SPACE_CHAR;
	//Original name: CSR-CALC-COV-LOB-CD
	private char calcCovLobCd := Types.SPACE_CHAR;
	//Original name: CSR-CALC-EXPLN-CD
	private char calcExplnCd := Types.SPACE_CHAR;
	//Original name: CSR-CALC-PAY-CD
	private char calcPayCd := Types.SPACE_CHAR;
	//Original name: CSR-CALC-ALW-CHG-AM
	private decimal(11,2) calcAlwChgAm := 0M;
	//Original name: CSR-CALC-PD-AM
	private decimal(11,2) calcPdAm := 0M;
	//Original name: CSR-CALC-CORP-CD
	private char calcCorpCd := Types.SPACE_CHAR;
	//Original name: CSR-CALC-PROD-CD
	private string calcProdCd := "";
	//Original name: CSR-CALC-TYP-BEN-CD
	private string calcTypBenCd := "";
	//Original name: CSR-FILLER
	private string filler := "";
	//Original name: CSR-CALC-SPECI-BEN-CD4
	private char calcSpeciBenCd4 := Types.SPACE_CHAR;
	//Original name: CSR-CALC-GL-ACCT-ID
	private string calcGlAcctId := "";
	//Original name: CSR-CALC-INS-TC-CD
	private string calcInsTcCd := "";
	//Original name: CSR-CALC-ORIGINAL-LINE
	private string calcOriginalLine := "000";
	//Original name: CSR-CALC-ORIGINAL-TYPE
	private string calcOriginalType := "0";


	//==== METHODS ====
	public []byte getCalcEntryBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, calcTypCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, calcCovLobCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, calcExplnCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, calcPayCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeDecimalAsPacked(buffer, position, calcAlwChgAm);
		position +:= Len.CALC_ALW_CHG_AM;
		MarshalByte.writeDecimalAsPacked(buffer, position, calcPdAm);
		position +:= Len.CALC_PD_AM;
		MarshalByte.writeChar(buffer, position, calcCorpCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, calcProdCd, Len.CALC_PROD_CD);
		position +:= Len.CALC_PROD_CD;
		MarshalByte.writeString(buffer, position, calcTypBenCd, Len.CALC_TYP_BEN_CD);
		position +:= Len.CALC_TYP_BEN_CD;
		getCalcSpeciBenCdBytes(buffer, position);
		position +:= Len.CALC_SPECI_BEN_CD;
		MarshalByte.writeString(buffer, position, calcGlAcctId, Len.CALC_GL_ACCT_ID);
		position +:= Len.CALC_GL_ACCT_ID;
		MarshalByte.writeString(buffer, position, calcInsTcCd, Len.CALC_INS_TC_CD);
		position +:= Len.CALC_INS_TC_CD;
		MarshalByte.writeString(buffer, position, calcOriginalLine, Len.CALC_ORIGINAL_LINE);
		position +:= Len.CALC_ORIGINAL_LINE;
		MarshalByte.writeString(buffer, position, calcOriginalType, Len.CALC_ORIGINAL_TYPE);
		return buffer;
	}

	public void initCalcEntrySpaces() {
		calcTypCd := Types.SPACE_CHAR;
		calcCovLobCd := Types.SPACE_CHAR;
		calcExplnCd := Types.SPACE_CHAR;
		calcPayCd := Types.SPACE_CHAR;
		calcAlwChgAm := OOR_NaN;
		calcPdAm := OOR_NaN;
		calcCorpCd := Types.SPACE_CHAR;
		calcProdCd := "";
		calcTypBenCd := "";
		initCalcSpeciBenCdSpaces();
		calcGlAcctId := "";
		calcInsTcCd := "";
		calcOriginalLine := "";
		calcOriginalType := "";
	}

	public void setCalcTypCd(char calcTypCd) {
		this.calcTypCd:=calcTypCd;
	}

	public char getCalcTypCd() {
		return this.calcTypCd;
	}

	public void setCalcCovLobCd(char calcCovLobCd) {
		this.calcCovLobCd:=calcCovLobCd;
	}

	public char getCalcCovLobCd() {
		return this.calcCovLobCd;
	}

	public void setCalcExplnCd(char calcExplnCd) {
		this.calcExplnCd:=calcExplnCd;
	}

	public char getCalcExplnCd() {
		return this.calcExplnCd;
	}

	public void setCalcPayCd(char calcPayCd) {
		this.calcPayCd:=calcPayCd;
	}

	public char getCalcPayCd() {
		return this.calcPayCd;
	}

	public void setCalcAlwChgAm(decimal(11,2) calcAlwChgAm) {
		this.calcAlwChgAm:=calcAlwChgAm;
	}

	public decimal(11,2) getCalcAlwChgAm() {
		return this.calcAlwChgAm;
	}

	public void setCalcPdAm(decimal(11,2) calcPdAm) {
		this.calcPdAm:=calcPdAm;
	}

	public decimal(11,2) getCalcPdAm() {
		return this.calcPdAm;
	}

	public void setCalcCorpCd(char calcCorpCd) {
		this.calcCorpCd:=calcCorpCd;
	}

	public char getCalcCorpCd() {
		return this.calcCorpCd;
	}

	public void setCalcProdCd(string calcProdCd) {
		this.calcProdCd:=Functions.subString(calcProdCd, Len.CALC_PROD_CD);
	}

	public string getCalcProdCd() {
		return this.calcProdCd;
	}

	public void setCalcTypBenCd(string calcTypBenCd) {
		this.calcTypBenCd:=Functions.subString(calcTypBenCd, Len.CALC_TYP_BEN_CD);
	}

	public string getCalcTypBenCd() {
		return this.calcTypBenCd;
	}

	public void setCalcSpeciBenCdFormatted(string data) {
		[]byte buffer := new [Len.CALC_SPECI_BEN_CD]byte;
		MarshalByte.writeString(buffer, 1, data, Len.CALC_SPECI_BEN_CD);
		setCalcSpeciBenCdBytes(buffer, 1);
	}

	public void setCalcSpeciBenCdBytes([]byte buffer, integer offset) {
		integer position := offset;
		filler := MarshalByte.readString(buffer, position, Len.FILLER);
		position +:= Len.FILLER;
		calcSpeciBenCd4 := MarshalByte.readChar(buffer, position);
	}

	public []byte getCalcSpeciBenCdBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, filler, Len.FILLER);
		position +:= Len.FILLER;
		MarshalByte.writeChar(buffer, position, calcSpeciBenCd4);
		return buffer;
	}

	public void initCalcSpeciBenCdSpaces() {
		filler := "";
		calcSpeciBenCd4 := Types.SPACE_CHAR;
	}

	public void setFiller(string filler) {
		this.filler:=Functions.subString(filler, Len.FILLER);
	}

	public string getFiller() {
		return this.filler;
	}

	public void setCalcSpeciBenCd4(char calcSpeciBenCd4) {
		this.calcSpeciBenCd4:=calcSpeciBenCd4;
	}

	public char getCalcSpeciBenCd4() {
		return this.calcSpeciBenCd4;
	}

	public void setCalcGlAcctId(string calcGlAcctId) {
		this.calcGlAcctId:=Functions.subString(calcGlAcctId, Len.CALC_GL_ACCT_ID);
	}

	public string getCalcGlAcctId() {
		return this.calcGlAcctId;
	}

	public void setCalcInsTcCd(string calcInsTcCd) {
		this.calcInsTcCd:=Functions.subString(calcInsTcCd, Len.CALC_INS_TC_CD);
	}

	public string getCalcInsTcCd() {
		return this.calcInsTcCd;
	}

	public void setCalcOriginalLineFormatted(string calcOriginalLine) {
		this.calcOriginalLine:=Trunc.toUnsignedNumeric(calcOriginalLine, Len.CALC_ORIGINAL_LINE);
	}

	public short getCalcOriginalLine() {
		return NumericDisplay.asShort(this.calcOriginalLine);
	}

	public void setCalcOriginalTypeFormatted(string calcOriginalType) {
		this.calcOriginalType:=Trunc.toUnsignedNumeric(calcOriginalType, Len.CALC_ORIGINAL_TYPE);
	}

	public short getCalcOriginalType() {
		return NumericDisplay.asShort(this.calcOriginalType);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer CALC_ORIGINAL_LINE := 3;
		public final static integer CALC_ORIGINAL_TYPE := 1;
		public final static integer CALC_PROD_CD := 2;
		public final static integer CALC_TYP_BEN_CD := 3;
		public final static integer FILLER := 3;
		public final static integer CALC_GL_ACCT_ID := 8;
		public final static integer CALC_INS_TC_CD := 2;
		public final static integer CALC_TYP_CD := 1;
		public final static integer CALC_COV_LOB_CD := 1;
		public final static integer CALC_EXPLN_CD := 1;
		public final static integer CALC_PAY_CD := 1;
		public final static integer CALC_ALW_CHG_AM := 6;
		public final static integer CALC_PD_AM := 6;
		public final static integer CALC_CORP_CD := 1;
		public final static integer CALC_SPECI_BEN_CD4 := 1;
		public final static integer CALC_SPECI_BEN_CD := FILLER + CALC_SPECI_BEN_CD4;
		public final static integer CALC_ENTRY := CALC_TYP_CD + CALC_COV_LOB_CD + CALC_EXPLN_CD + CALC_PAY_CD + CALC_ALW_CHG_AM + CALC_PD_AM + CALC_CORP_CD + CALC_PROD_CD + CALC_TYP_BEN_CD + CALC_SPECI_BEN_CD + CALC_GL_ACCT_ID + CALC_INS_TC_CD + CALC_ORIGINAL_LINE + CALC_ORIGINAL_TYPE;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer CALC_ALW_CHG_AM := 9;
			public final static integer CALC_PD_AM := 9;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer CALC_ALW_CHG_AM := 2;
			public final static integer CALC_PD_AM := 2;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//CsrCalcEntry