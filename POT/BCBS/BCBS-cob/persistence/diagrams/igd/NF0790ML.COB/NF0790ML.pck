<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="NF0790ML" cbl:id="NF0790ML" xsi:id="NF0790ML" packageRef="NF0790ML.igd#NF0790ML" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="NF0790ML_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10004" deadCode="false" name="FIRST">
			<representations href="../../../cobol/NF0790ML.COB.cobModel#SC_1F10004"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10004" deadCode="false" name="0000-START-PROCESS">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_1F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10004" deadCode="true" name="0000-EXIT">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_8F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10004" deadCode="false" name="1000-PROCESS-JOB-PARMS">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_6F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10004" deadCode="true" name="1000-EXIT">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_7F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10004" deadCode="false" name="9010-SELECT-TA03085-CONTROL">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_9F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10004" deadCode="true" name="9010-EXIT">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_16F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10004" deadCode="false" name="9020-SELECT-TA03086-RESTART">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_10F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10004" deadCode="true" name="9020-EXIT">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_17F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10004" deadCode="false" name="9700-DISPLAY-RESTART-INFO">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_11F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10004" deadCode="true" name="9700-EXIT">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_18F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10004" deadCode="false" name="9800-READ-JOB-PARMS">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_2F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10004" deadCode="true" name="9800-EXIT">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_3F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10004" deadCode="false" name="9998-DISPLAY-SQLCODE">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_12F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10004" deadCode="true" name="9998-EXIT">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_13F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10004" deadCode="false" name="9999-CALL-ABEND">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_4F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10004" deadCode="true" name="9999-EXIT">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_5F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10004" deadCode="false" name="DB2-STAT-ERR">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_14F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10004" deadCode="true" name="DB2-STAT-ERR-EXIT">
				<representations href="../../../cobol/NF0790ML.COB.cobModel#P_15F10004"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_S03085SA" name="S03085SA">
			<representations href="../../../explorer/storage-explorer.xml.storage#S_1F10078"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_S03086SA" name="S03086SA">
			<representations href="../../../explorer/storage-explorer.xml.storage#S_1F10079"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="ABEND" name="ABEND" missing="true">
			<representations href="../../../../missing.xmi#IDITG4JYQR2NENBSFS1HXF4G532IP1OP3CABLGB2JCQTM0PVJNGHFM"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="DSNTIAR" name="DSNTIAR" missing="true">
			<representations href="../../../../missing.xmi#IDJNJ0WR4TJDOTKUDANZV2FIOS1JII1RMUDNTUX44RCJVFPJZW5YK"/>
		</children>
		<children xsi:type="cbl:VsamDataNode" id="V_JOBPARMS_NF0790ML" name="JOBPARMS[NF0790ML]">
			<representations href="../../../explorer/storage-explorer.xml.storage#JOBPARMS_NF0790ML"/>
		</children>
	</packageNode>
	<edges id="SC_1F10004P_1F10004" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10004" targetNode="P_1F10004"/>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10004_I" deadCode="false" sourceNode="P_1F10004" targetNode="P_2F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_6F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10004_O" deadCode="false" sourceNode="P_1F10004" targetNode="P_3F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_6F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10004_I" deadCode="false" sourceNode="P_1F10004" targetNode="P_4F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_14F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10004_O" deadCode="false" sourceNode="P_1F10004" targetNode="P_5F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_14F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10004_I" deadCode="false" sourceNode="P_1F10004" targetNode="P_6F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_15F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10004_O" deadCode="false" sourceNode="P_1F10004" targetNode="P_7F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_15F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10004_I" deadCode="false" sourceNode="P_1F10004" targetNode="P_4F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_22F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10004_I" deadCode="false" sourceNode="P_6F10004" targetNode="P_9F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_29F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10004_I" deadCode="false" sourceNode="P_6F10004" targetNode="P_10F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_30F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10004_I" deadCode="false" sourceNode="P_6F10004" targetNode="P_11F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_33F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10004_I" deadCode="false" sourceNode="P_6F10004" targetNode="P_11F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_47F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10004_I" deadCode="false" sourceNode="P_6F10004" targetNode="P_2F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_67F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10004_O" deadCode="false" sourceNode="P_6F10004" targetNode="P_3F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_67F10004"/>
	</edges>
	<edges id="P_6F10004P_7F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10004" targetNode="P_7F10004"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10004_I" deadCode="false" sourceNode="P_9F10004" targetNode="P_12F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_79F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10004_O" deadCode="false" sourceNode="P_9F10004" targetNode="P_13F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_79F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10004_I" deadCode="false" sourceNode="P_9F10004" targetNode="P_14F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_84F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10004_O" deadCode="false" sourceNode="P_9F10004" targetNode="P_15F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_84F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10004_I" deadCode="false" sourceNode="P_9F10004" targetNode="P_4F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_85F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10004_O" deadCode="false" sourceNode="P_9F10004" targetNode="P_5F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_85F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10004_I" deadCode="false" sourceNode="P_10F10004" targetNode="P_12F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_97F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10004_O" deadCode="false" sourceNode="P_10F10004" targetNode="P_13F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_97F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10004_I" deadCode="false" sourceNode="P_10F10004" targetNode="P_14F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_102F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10004_O" deadCode="false" sourceNode="P_10F10004" targetNode="P_15F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_102F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10004_I" deadCode="false" sourceNode="P_10F10004" targetNode="P_4F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_103F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10004_O" deadCode="false" sourceNode="P_10F10004" targetNode="P_5F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_103F10004"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_106F10004_GTP_1" deadCode="false" sourceNode="P_11F10004" targetNode="P_18F10004">
		<representations href="../../../cobol/NF0790ML.COB.cobModel#S_106F10004"/>
	</edges>
	<edges id="P_11F10004P_18F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_11F10004" targetNode="P_18F10004"/>
	<edges id="P_18F10004P_2F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10004" targetNode="P_2F10004"/>
	<edges id="P_2F10004P_3F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10004" targetNode="P_3F10004"/>
	<edges id="P_3F10004P_12F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_3F10004" targetNode="P_12F10004"/>
	<edges id="P_12F10004P_13F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10004" targetNode="P_13F10004"/>
	<edges id="P_13F10004P_4F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_13F10004" targetNode="P_4F10004"/>
	<edges id="P_4F10004P_5F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10004" targetNode="P_5F10004"/>
	<edges id="P_5F10004P_14F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_5F10004" targetNode="P_14F10004"/>
	<edges id="P_14F10004P_15F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10004" targetNode="P_15F10004"/>
	<edges xsi:type="cbl:DataEdge" id="NF0790ML_S_5F10004JOBPARMS_NF0790ML" deadCode="false" targetNode="P_1F10004" sourceNode="V_JOBPARMS_NF0790ML">
		<representations href="../../../cobol/../importantStmts.cobModel#S_5F10004"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="NF0790ML_S_20F10004JOBPARMS_NF0790ML" deadCode="false" targetNode="P_1F10004" sourceNode="V_JOBPARMS_NF0790ML">
		<representations href="../../../cobol/../importantStmts.cobModel#S_20F10004"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="NF0790ML_S_139F10004JOBPARMS_NF0790ML" deadCode="false" targetNode="P_2F10004" sourceNode="V_JOBPARMS_NF0790ML">
		<representations href="../../../cobol/../importantStmts.cobModel#S_139F10004"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_69F10004_POS1" deadCode="false" targetNode="P_9F10004" sourceNode="DB2_S03085SA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_69F10004"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_87F10004_POS1" deadCode="false" targetNode="P_10F10004" sourceNode="DB2_S03086SA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_87F10004"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_154F10004" deadCode="false" sourceNode="P_4F10004" targetNode="ABEND">
		<representations href="../../../cobol/../importantStmts.cobModel#S_154F10004"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_156F10004" deadCode="false" sourceNode="P_14F10004" targetNode="DSNTIAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_156F10004"></representations>
	</edges>
</Package>
